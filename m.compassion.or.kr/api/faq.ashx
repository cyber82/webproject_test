﻿<%@ WebHandler Language="C#" Class="api_sympathy" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_sympathy : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();
        if(t == "list") {
            this.GetList(context);
        }
    }

    void GetList(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var s_type = context.Request["s_type"].EmptyIfNull();
        var k_word = context.Request["k_word"].EmptyIfNull();
        var top = context.Request["top"].ValueIfNull("-1");


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_faq_list_f(page, rowsPerPage, s_type, k_word, "", Convert.ToInt32(top), "recruit").ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "keyword", "re_keyword", "top", "except" };
            Object[] op2 = new Object[] {page, rowsPerPage, s_type, k_word, "", Convert.ToInt32(top), "recruit" };
            var list = www6.selectSP("sp_faq_list_f", op1, op2).DataTableToList<sp_faq_list_fResult>();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }


}