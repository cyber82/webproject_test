﻿<%@ WebHandler Language="C#" Class="user_profile_image" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;
using System.Drawing;


public class user_profile_image : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var result = new JsonWriter() {
            success = true
        };


        if (!FrontLoginSession.HasCookie(context)) {
            result.success = false;
            result.message = "로그인이 필요한 서비스입니다.";
            return;
        }

        // 기존 업로드된 파일은 삭제
        /*
		var old_picture = FrontLoginSession.GetCookie(context).u_picture.EmptyIfNull();
		if(old_picture.IndexOf("http://") > -1 || old_picture.IndexOf("https://") > -1)
			old_picture = "";
		
		// profile image 
		data:image/png;base64,iVBORw0KG~== 형태
		*/

        string orientation = context.Request["orientation"];
        string base64 = context.Request["base64"];
        string path = context.Request["path"];
        var bin = base64.Split(',')[1];

        byte[] bytes = Convert.FromBase64String(bin);

        using (Stream stream = new MemoryStream(bytes)) {
            var image = Image.FromStream(stream);

            //image = image.Resize(300, 300 , Convert.ToInt32(orientation));
            // 이미지 사이즈 조절 부분 수정 문희원 2017-05-30
            var ratioX = (double)300.0 / image.Width;
            var ratioY = (double)300.0 / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);


            if (orientation != "1")
                newImage.RotateFlip(RotateFlipType.Rotate90FlipNone);

            //var path = "/upload/user-profile-image/";
            var fileName = string.Format("{0}.jpg" , Guid.NewGuid().ToString().Replace("-" , ""));

            var upload_result = new Uploader().Upload(context, true, path, fileName, newImage, "");

            if(upload_result.success) {
                result.data = upload_result.name;
            } else {
                result.success = false;
                ErrorLog.Write(context, 0, upload_result.ToJson());
            }

            JsonWriter.Write(result, context);


        }

        // impl me.
        //ErrorLog.Write(context, 0, base64);

        //JsonWriter.Write(result, context);

    }


}