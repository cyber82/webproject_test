﻿<%@ WebHandler Language="C#" Class="api_sponsor" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_sponsor : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest( HttpContext context ) {

		var t = context.Request["t"].EmptyIfNull();

		if(t == "sync") {
			this.Sync(context);
		} else if(t == "check") {
			this.Check(context);
			/*
	} else if(t == "report") {
		this.Report(context);

	} else if(t == "checkout") {        // 지정된 항목을 PayItem 에 담는다
		this.CheckOut(context);
		*/
		} else if(t == "update-userpic") {
			this.UpdateUserPic(context);
		} else if(t == "organization-list") {
			this.GetOrganization( context );
		}
	}



	void GetOrganization( HttpContext context ) {
		var sConID = context.Request["id"].EmptyIfNull();
		var sOrganizationName = context.Request["name"].EmptyIfNull();
		var ddlOrganizationType = context.Request["type"].EmptyIfNull();

		WWWService.Service _wwwService = new WWWService.Service();
		DataSet ds = _wwwService.listOrganizationInformation(sConID, sOrganizationName, ddlOrganizationType);

		JsonWriter result = new JsonWriter();
		result.data = ds;
		result.success = true;
		result.Write(context);
			
	}


	void UpdateUserPic( HttpContext context ) {
		var path = context.Request["path"].EmptyIfNull();
		new SponsorAction().UpdateUserPic(path).Write(context);
	}

	/*
		void CheckOut( HttpContext context ) {

			var type = context.Request["type"].EmptyIfNull();       // type = special , CDSP
			var frequency = context.Request["frequency"].ValueIfNull("일시");
			var group = context.Request["group"].EmptyIfNull();
			var codeId = context.Request["codeId"].EmptyIfNull();
			var codeName = context.Request["codeName"].EmptyIfNull();
			var amount = Convert.ToInt32(context.Request["amount"].ValueIfNull("0"));
			var childMasterId = context.Request["childMasterId"].EmptyIfNull();
			var campaignId = context.Request["campaignId"].EmptyIfNull();
			var relation_key = Convert.ToInt32(context.Request["relation_key"].ValueIfNull("-1"));

			PayItemSession.Entity.enumType enumType = PayItemSession.Entity.enumType.SPECIAL_FUNDING;
			if (type.ToLower() == "cdsp") {
				enumType = PayItemSession.Entity.enumType.CDSP;
				group = "CDSP";
				codeId = "DS";
				amount = 45000;

			}

			PayItemSession.SetCookie(context,
				new PayItemSession.Entity() { type = enumType,
					relation_key = relation_key ,
					childMasterId = childMasterId , 
					campaignId = campaignId,
					frequency = frequency ,
					group = group ,
					codeId = codeId,
					codeName = codeName,
					amount = amount });

			new JsonWriter() {
				success = true,
				data = (frequency == "일시" ? "/sponsor/pay/temporary/" : "/sponsor/pay/regular/"),
				action = "redirect"
			}.Write(context);

			//Response.Redirect();

		}

		void Report( HttpContext context ) {

			var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
			var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
			var AccountClassGroup = context.Request["group"].EmptyIfNull();
			var AccountClass = context.Request["code"].EmptyIfNull();

			new SpecialFundingAction().GetReportList(page, rowsPerPage, AccountClassGroup, AccountClass).Write(context);
		}
		*/

	void Check( HttpContext context ) {

		var ci = context.Request["ci"].EmptyIfNull();

		new SponsorAction().CheckSponsor(ci).Write(context);
	}

	void Sync( HttpContext context ) {

		var ci = context.Request["ci"].EmptyIfNull();
		var di = context.Request["di"].EmptyIfNull();
		var gender = context.Request["gender"].EmptyIfNull();
		var jumin = context.Request["jumin"].EmptyIfNull();     // 실명인증만 값이 있음

		new SponsorAction().Sync(ci, di, gender, jumin).Write(context);
	}
}