﻿<%@ WebHandler Language="C#" Class="api_popup" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;



public class api_popup : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public class popupEx : popup {
        public int rd;      // 몇일동안 열지 않을지 여부 날짜 
    }

    public override void OnRequest( HttpContext context ) {

        List<popupEx> result = new List<popupEx>();
        var actionResult = new JsonWriter();

        if(this.GetChildPopup(context).success) {
            result.Add((popupEx)GetChildPopup(context).data);

        }
        /*
        if(this.GetInformationPopup(context).success) {
            result.Add((popupEx)GetInformationPopup(context).data);
        }
        */
        /*
        if(this.GetOverseaPopup(context).success) {
            result.Add((popupEx)GetOverseaPopup(context).data);
        }
        */
        actionResult.success = true;
        actionResult.data = result;
        JsonWriter.Write(actionResult, context);

    }

    // 미주회원 팝업
    /*
    JsonWriter GetOverseaPopup( HttpContext context ) {
        JsonWriter result = new JsonWriter() {
            success = false
        };
        UserInfo sess = new UserInfo();

        if(sess.LocationType == "국내") {
            result.success = true;
        }

        // 팝업 data
        var userName = sess.UserName;
        var path = context.Server.MapPath("/popup/custom/american-member.html");
        using(StreamReader stream = File.OpenText(path)) {
            var popup_content = stream.ReadToEnd().Replace("{userName}", userName);

            result.data = (new popupEx() {
                p_id = 9000002,
                p_type = "layer",
                p_name = "해외",
                p_left = 300,
                p_top = 300,
                p_width = 300,
                p_height = 300,
                p_content = popup_content,
                rd = 14,
                p_begin = DateTime.Now,
                p_end = DateTime.Now,
                p_regdate = DateTime.Now,
                p_display = true

            });
        }
        return result;
    }
    */
    // 개인정보변경 팝업
    /*
    JsonWriter GetInformationPopup( HttpContext context ) {
        JsonWriter result = new JsonWriter() {
            success = false
        };
        UserInfo sess = new UserInfo();

        if(sess.LocationType == "국내") {
            result.success = true;
        }
        // 팝업 data
        var userName = sess.UserName;
        var path = context.Server.MapPath("/popup/custom/child.html");
        using(StreamReader stream = File.OpenText(path)) {
            var popup_content = stream.ReadToEnd().Replace("{userName}", userName);

            result.data = (new popupEx() {
                p_id = 9000001,
                p_type = "layer",
                p_name = "해외",
                p_left = 750,
                p_top = 130,
                p_width = 400,
                p_height = 544,
                p_content = popup_content,
                rd = 1,
                p_begin = DateTime.Now,
                p_end = DateTime.Now,
                p_regdate = DateTime.Now,
                p_display = true

            });

            return result;
        }
    }
    */
    
    // 어린이 생일 팝업
    JsonWriter GetChildPopup( HttpContext context ) {

        JsonWriter result = new JsonWriter() {
            success = false
        };

        // 로그인 사용자만 체크
        if(!UserInfo.IsLogin) {
            return result;
        }

        WWWService.Service _wwwService = new WWWService.Service();
        UserInfo sess = new UserInfo();

        // 후원아이디가 있는 경우만 체크
        if(string.IsNullOrEmpty(sess.SponsorID)) {
            return result;
        }

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        Object[] objSql3 = new object[1] { " SELECT * FROM   tCorrCopySvcRequest " +
                                               " WHERE CurrentUse = 'Y' AND (LetterRequestType != 'GN' OR CardRequestType != 'GN') " +
                                              " AND SponsorID = '" + sess.SponsorID.ToString() + "'" };

        DataSet ds3 = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql3, "Text", null, null);

        if(ds3.Tables[0].Rows.Count > 0)
            return result;

        DataSet dsChild = _wwwService.getSponChildList_Web(sess.SponsorID);

        if(dsChild.Tables[0].Rows.Count > 5)
            return result;

        foreach(DataRow dr in dsChild.Tables[0].Rows) {
            if(dr["FirstFundedDate"].ToString() == "" && dr["SponsorTypeEng"].ToString().IndexOf("COR") < 0)

                continue;

            else if(dr["CloseDate"].ToString() != "")
                continue;

            try {
                Object[] objSql2 = new object[1] { " SELECT MONTH(BirthDate) AS Month FROM tChildMaster " +
                                                       " WHERE 1=1 " +
                                                       " AND  (MONTH(GETDATE()) >= MONTH(DATEADD(M, -2, BirthDate)) OR  " +
                                                       "      (MONTH(BirthDate) = 1 OR MONTH(BirthDate) = 2)) " +
                                                       " AND  (MONTH(GETDATE()) <= MONTH(DATEADD(M, 0, BirthDate))) " +
                                                       " AND  ChildMasterID = '" + dr["ChildMasterID"].ToString() + "'" };

                DataSet ds2 = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "Text", null, null);

                if(ds2.Tables[0].Rows.Count == 0)
                    continue;

                else {
                    string year = (Convert.ToInt32(DateTime.Today.ToString("yyyy")) - 1).ToString();
                    string month = (Convert.ToInt32(ds2.Tables[0].Rows[0]["Month"]) + 2).ToString();

                    if(Convert.ToInt32(month) > 12)
                        month = (Convert.ToInt32(month) - 12).ToString();

                    if(month.ToString().Length == 1)
                        month = "0" + month;

                    string sBirthDate = year + "-" + month + "-01";

                    try {
                        Object[] objSql = new object[1] { " SELECT CorrID " +
                                                              " FROM   Corr_Master " +
                                                              " WHERE  SponsorID = '" + sess.SponsorID + "' " +
                                                              " AND    ChildMasterID = '" + dr["ChildMasterID"].ToString() + "' " +
                                                              " AND    CorrType IN ('SPNBDC', 'XCSBDC', 'WSBDC') " +
                                                              " AND    RegisterDate BETWEEN '" + sBirthDate + "' AND GETDATE() "};

                        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

                        if(ds.Tables[0].Rows.Count == 0) {
                            result.success = true;

                            // 팝업 data
                            var path = context.Server.MapPath("/popup/custom/child.html");
                            var userName = sess.UserName;

                            using(StreamReader stream = File.OpenText(path)) {
                                var popup_content = stream.ReadToEnd().Replace("{userName}", userName);

                                result.data = new popupEx() {
                                    p_id = 900001,
                                    p_type = "layer",
                                    p_name = "어린이",
                                    p_left = 600,
                                    p_top = 0,
                                    p_width = 500,
                                    p_height = 500,
                                    p_content = popup_content,
                                    rd = 14,
                                    p_begin = DateTime.Now,
                                    p_end = DateTime.Now,
                                    p_regdate = DateTime.Now,
                                    p_display = true

                                };

                            }

                            break;
                        }

                    } catch(Exception ex) {
                        ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
                        result.message = "편지 정보를 확인하는 도중 오류가 발생하였습니다.";


                    }
                }
            } catch(Exception ex) {
                ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
                result.message = "후원 어린이 생일 정보를 가져오는 도중 오류가 발생하였습니다.";

            }
        }


        return result;


    }


}