﻿<%@ WebHandler Language="C#" Class="api_newsletter" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_newsletter : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "add") {
            this.Add(context);
        }
    }

    void Add(HttpContext context) {


        JsonWriter result = new JsonWriter();
        result.success = true;


        // 파라미터 검색
        var name = context.Request["name"].EmptyIfNull();
        var phone = context.Request["phone"].EmptyIfNull();
        var zipcode = context.Request["zipcode"].EmptyIfNull();
        var addr1 = context.Request["addr1"].EmptyIfNull();
        var addr2 = context.Request["addr2"].EmptyIfNull();
        var hfAddressType = context.Request["hfAddressType"].EmptyIfNull();


        if (name == "" || phone == "" || zipcode == "" || addr1 == "" || addr2 == "") {
            result.success = false;
            result.message = "필수 정보를 입력해주세요.";
            JsonWriter.Write(result, context);
            return;
        }


        if(!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        UserInfo sess = new UserInfo();


        // 40세 이상이고 conId가 있는 회원(conId가 5, 6자리여야만 유효한 conId임)

        var commitInfo = new SponsorAction().GetCommitInfo();
        if(!commitInfo.success) {
            JsonWriter.Write(commitInfo, context);
        }

        if(sess.ConId.Length != 5 && sess.ConId.Length != 6) {
            result.success = false;
            result.message = "후원자만 신청 가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        if (sess.Birth.EmptyIfNull() != "" && sess.Birth.Length > 3) {
            var age = DateTime.Now.Year - int.Parse(sess.Birth.Substring(0, 4));
            if (age < 40) {
                result.success = false;
                result.message = "40세 이상만 신청가능합니다.";
                JsonWriter.Write(result, context);
                return;
            }
        } else {
            result.success = false;
            result.message = "40세 이상만 신청가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }


        // 최초 한번 컴파스에 주소를 업데이트 한다.
        if(sess.LocationType == "국내") {
            new SponsorAction().UpdateAddress(false, hfAddressType , sess.LocationType, "한국", zipcode , addr1, addr2);
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            // 중복 신청여부 확인
            var exist = www6.selectQ<subscription>("s_userid", sess.UserId);
            //if (dao.subscription.Any(p => p.s_userid == sess.UserId))
            if(exist.Any())
            {
                result.success = false;
                result.message = "이미 구독 중입니다.";
                JsonWriter.Write(result, context);
                return;
            }

            subscription subs = new subscription()
            {
                s_name = name,
                s_phone = phone,
                s_zipcode = zipcode,
                s_addr1 = addr1,
                s_addr2 = addr2,
                s_userid = sess.UserId,
                s_regdate = DateTime.Now
            };
            //dao.subscription.InsertOnSubmit(subs);
            www6.insert(subs);
            //dao.SubmitChanges();
        }


        JsonWriter.Write(result, context);


    }
}