﻿<%@ WebHandler Language="C#" Class="api_user_funding" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_user_funding : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "list") {
            this.GetList(context);
        } else if(t == "reply-list") {
            this.GetReplyList(context);
        } else if(t == "add-reply") {
            this.AddReply(context);
        } else if(t == "update-reply") {
            this.UpdateReply(context);
        } else if(t == "delete-reply") {
            this.DeleteReply(context);
        } else if(t == "notice-list") {
            this.GetNoticeList(context);
        } else if(t == "add-notice") {
            this.AddNotice(context);
        } else if(t == "update-notice") {
            this.UpdateNotice(context);
        } else if(t == "delete-notice") {
            this.DeleteNotice(context);
        } else if(t == "user-list") {
            this.GetUserList(context);
        } else if(t == "file-delete") {
            this.FileDelete(context);
        } else if(t == "hits") {
            this.Hits(context);

        }
    }

    void FileDelete( HttpContext context ) {

        //var file_path = context.Request["file_path"].EmptyIfNull().Replace(ConfigurationManager.AppSettings["domain_file"], "");
        var file_path = context.Request.Form["file_path"].EmptyIfNull();
        new UserFundingAction().FileDelete(file_path).Write(context);

    }

    // 참여자 목록 
    void GetUserList( HttpContext context ) {
        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var uf_id = Convert.ToInt32(context.Request["uf_id"].ValueIfNull("0"));

        JsonWriter.Write(new UserFundingAction().GetUser(page, rowsPerPage, uf_id), context);
    }

    // 업데이트 목록 
    void GetNoticeList( HttpContext context ) {
        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var uf_id = Convert.ToInt32(context.Request["uf_id"].ValueIfNull("0"));

        JsonWriter.Write(new UserFundingAction().GetNotice(page, rowsPerPage, uf_id), context);
    }


    // 업데이트소식 등록 
    void AddNotice( HttpContext context ) {

        JsonWriter result = new JsonWriter() { success = false };

        if(!UserInfo.IsLogin) {
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var uf_id = Convert.ToInt32(context.Request["uf_id"].ValueIfNull("-1"));
        var comment = context.Request["comment"].EmptyIfNull().EscapeSqlInjection();
        var file = context.Request["file"].EmptyIfNull();

        if(comment.Length < 1) {
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            //var item = dao.sp_tUserFunding_get_f(uf_id).FirstOrDefault();
            Object[] op1 = new Object[] { "uf_id" };
            Object[] op2 = new Object[] { uf_id };
            var item = www6.selectSP("sp_tUserFunding_get_f", op1, op2).DataTableToList<sp_tUserFunding_get_fResult>().FirstOrDefault();

            if (item == null)
            {
                result.message = "나눔펀딩 정보조회에 실패했습니다.(1)";
                JsonWriter.Write(result, context);
                return;
            }

            if (item.UserID != userInfo.UserId)
            {
                result.message = "나눔펀딩 정보조회에 실패했습니다.(2)";
                JsonWriter.Write(result, context);
                return;
            }

            var entity = new tUserFundingNotice()
            {
                UserID = userInfo.UserId,
                un_content = comment,
                un_regdate = DateTime.Now,
                un_image = file,
                un_uf_id = uf_id,
                un_deleted = false,
                un_a_id = -1
            };

            // 공지 카운트 증가
            //var uf_entity = dao.tUserFunding.First(p => p.uf_id == uf_id);
            var uf_entity = www6.selectQF<tUserFunding>("uf_id", uf_id);

            uf_entity.uf_cnt_notice = uf_entity.uf_cnt_notice + 1;

            //dao.tUserFundingNotice.InsertOnSubmit(entity);
            www6.insert(entity);
            //dao.SubmitChanges();
        }

        result.success = true;
        JsonWriter.Write(result, context);
    }


    // 업데이트 수정 
    void UpdateNotice( HttpContext context ) {

        JsonWriter result = new JsonWriter() { success = false };

        if(!UserInfo.IsLogin) {
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var idx = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var uf_id = Convert.ToInt32(context.Request["uf_id"].ValueIfNull("-1"));
        var comment = context.Request["comment"].EmptyIfNull().EscapeSqlInjection();
        var file = context.Request["file"].EmptyIfNull();

        if(comment.Length < 1) {
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            //var item = dao.sp_tUserFunding_get_f(uf_id).FirstOrDefault();
            Object[] op1 = new Object[] { "uf_id" };
            Object[] op2 = new Object[] { uf_id };
            var item = www6.selectSP("sp_tUserFunding_get_f", op1, op2).DataTableToList<sp_tUserFunding_get_fResult>().FirstOrDefault();

            if (item == null)
            {
                result.message = "나눔펀딩 정보조회에 실패했습니다.(1)";
                JsonWriter.Write(result, context);
                return;
            }

            if (item.UserID != userInfo.UserId)
            {
                result.message = "나눔펀딩 정보조회에 실패했습니다.(2)";
                JsonWriter.Write(result, context);
                return;
            }

            //var entity = dao.tUserFundingNotice.First(p => p.un_id == idx);
            var entity = www6.selectQF<tUserFundingNotice>("un_id", idx);
            entity.un_content = comment;
            entity.un_image = file;

            //dao.SubmitChanges();
            www6.update(entity);
        }

        result.success = true;
        JsonWriter.Write(result, context);
    }


    // 업데이트 삭제
    void DeleteNotice( HttpContext context ) {

        JsonWriter result = new JsonWriter() { success = false };

        if(!UserInfo.IsLogin) {
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var idx = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var uf_id = Convert.ToInt32(context.Request["uf_id"].ValueIfNull("-1"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            //var item = dao.sp_tUserFunding_get_f(uf_id).FirstOrDefault();
            Object[] op1 = new Object[] { "uf_id" };
            Object[] op2 = new Object[] { uf_id };
            var item = www6.selectSP("sp_tUserFunding_get_f", op1, op2).DataTableToList<sp_tUserFunding_get_fResult>().FirstOrDefault();

            if (item == null)
            {
                result.message = "나눔펀딩 정보조회에 실패했습니다.(1)";
                JsonWriter.Write(result, context);
                return;
            }

            if (item.UserID != userInfo.UserId)
            {
                result.message = "나눔펀딩 정보조회에 실패했습니다.(2)";
                JsonWriter.Write(result, context);
                return;
            }


            // 공지 카운트 감소
            //var uf_entity = dao.tUserFunding.First(p => p.uf_id == uf_id);
            var uf_entity = www6.selectQF<tUserFunding>("uf_id", uf_id);

            uf_entity.uf_cnt_notice = uf_entity.uf_cnt_notice + 1;
            www6.update(uf_entity);

            //var entity = dao.tUserFundingNotice.First(p => p.un_id == idx);
            var entity = www6.selectQF<tUserFundingNotice>("un_id", idx);
            //entity.un_deleted = true;
            //dao.SubmitChanges();
            www6.update(entity);
        }

        result.success = true;
        JsonWriter.Write(result, context);
    }





    // 댓글 등록 
    void AddReply( HttpContext context ) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if(!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var uf_id = Convert.ToInt32(context.Request["uf_id"].ValueIfNull("-1"));
        var content = context.Request["comment"].EmptyIfNull().EscapeSqlInjection();

        if(content.Length < 1) {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            var entity = new tUserFundingReply()
            {
                ur_uf_id = uf_id,
                UserID = userInfo.UserId,
                ur_content = content,
                ur_regdate = DateTime.Now,
                ur_deleted = false,
                ur_a_id = -1
            };
            // 댓글 카운트 증가
            //var uf_entity = dao.tUserFunding.First(p => p.uf_id == uf_id);
            var uf_entity = www6.selectQF<tUserFunding>("uf_id", uf_id);
            uf_entity.uf_cnt_reply = uf_entity.uf_cnt_reply + 1;
            www6.update(uf_entity);

            //dao.tUserFundingReply.InsertOnSubmit(entity);
            www6.insert(entity);
            //dao.SubmitChanges();



        }

        JsonWriter.Write(result, context);
    }

    // 댓글 수정
    void UpdateReply( HttpContext context ) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if(!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var idx = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var content = context.Request["content"].EmptyIfNull().EscapeSqlInjection();


        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            //var entity = dao.tUserFundingReply.First(p => p.ur_id == idx);
            var entity = www6.selectQF<tUserFundingReply>("ur_id", idx);
            var userID = entity.UserID;

            // 로그인한 유저가 맞는지 다시 확인 
            if (userInfo.UserId != userID)
            {
                result.success = false;
                result.message = "정상적인 접근이 아닙니다.";
                JsonWriter.Write(result, context);
                return;
            }

            // 수정 
            //entity.ur_content = content;
            www6.update(entity);
            //dao.SubmitChanges();
        }

        JsonWriter.Write(result, context);

    }

    // 댓글 삭제 
    void DeleteReply( HttpContext context )
    {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if(!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var idx = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var uf_id = Convert.ToInt32(context.Request["uf_id"].ValueIfNull("-1"));

        if(idx == -1) {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            //var entity = dao.tUserFundingReply.First(p => p.ur_id == idx);
            var entity = www6.selectQF<tUserFundingReply>("ur_id", idx);
            var userID = entity.UserID;

            // 로그인한 유저가 맞는지 다시 확인 
            if (userInfo.UserId != userID)
            {
                result.success = false;
                result.message = "정상적인 접근이 아닙니다.";
                JsonWriter.Write(result, context);
                return;
            }

            // 댓글 카운트 감소
            //var uf_entity = dao.tUserFunding.First(p => p.uf_id == uf_id);
            var uf_entity = www6.selectQF<tUserFunding>("uf_id", uf_id);

            uf_entity.uf_cnt_reply = uf_entity.uf_cnt_reply - 1;

            // 삭제
            entity.ur_deleted = true;
            //dao.SubmitChanges();
            www6.update(entity);
            www6.update(uf_entity);


        }

        result.message = "삭제 되었습니다.";

        JsonWriter.Write(result, context);
    }


    // 댓글 목록 
    void GetReplyList( HttpContext context )
    {
        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var uf_id = Convert.ToInt32(context.Request["uf_id"].ValueIfNull("0"));

        JsonWriter.Write(new UserFundingAction().GetReply(page, rowsPerPage, uf_id), context);
    }

    void GetList( HttpContext context ) {

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var type = context.Request["type"].EmptyIfNull();
        var sort = context.Request["sort"].EmptyIfNull();
        var keyword = context.Request["keyword"].EmptyIfNull();

        new UserFundingAction().GetList(page, rowsPerPage, type, sort, keyword).Write(context);

    }

    void Hits( HttpContext context )
    {
        var idx = Convert.ToInt32(context.Request["idx"].ValueIfNull("-1"));
        if(idx != -1)
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                //var entity = dao.tUserFunding.First(p => p.uf_id == idx);
                var entity = www6.selectQF<tUserFunding>("uf_id", idx);

                if (entity.uf_cnt_view == 0)
                {
                    entity.uf_cnt_view = 1;
                }
                else
                {
                    entity.uf_cnt_view++;
                }
                //dao.SubmitChanges();
                www6.update(entity);
            }
        }
    }

}