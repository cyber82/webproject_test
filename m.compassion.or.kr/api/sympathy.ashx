﻿<%@ WebHandler Language="C#" Class="api_sympathy" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_sympathy : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "list") {
            this.GetList(context);
        } else if(t == "apply") {
            this.ApplyBlueBook(context);
        } else if(t == "hits") {
            Hits(context);
        }
    }

    void GetList( HttpContext context )
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var boardId = context.Request["s_type"].EmptyIfNull();
        var k_type = context.Request["k_type"].ValueIfNull("both");
        var k_word = context.Request["k_word"].EmptyIfNull();
        var s_column = context.Request["s_column"].ValueIfNull("reg_date");
        var depth1 = context.Request["depth1"].EmptyIfNull();
        var depth2 = context.Request["depth2"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_sympathy_list_f(page, rowsPerPage, boardId, k_type, k_word, "", "", s_column, depth1, depth2).ToList();
            Object[] op1 = new Object[] {"page", "rowsPerPage", "board_id", "keyword_type", "keyword", "startdate", "enddate", "sort_column", "depth1", "depth2" };
            Object[] op2 = new Object[] { page, rowsPerPage, boardId, k_type, k_word, "", "", s_column, depth1, depth2 };
            var list = www6.selectSP("sp_sympathy_list_f", op1, op2).DataTableToList<sp_sympathy_list_fResult>();

            foreach (var item in list)
            {
                item.thumb = item.thumb.WithFileServerHost();
            }
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }


    void ApplyBlueBook( HttpContext context ) {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var name = context.Request["name"].EmptyIfNull();
        var phone = context.Request["phone"].EmptyIfNull();
        var zipcode = context.Request["zipcode"].EmptyIfNull();
        var addr1 = context.Request["addr1"].EmptyIfNull();
        var addr2 = context.Request["addr2"].EmptyIfNull();
        var addressType = context.Request["addressType"].EmptyIfNull();


        if(name == "" || phone == "" || zipcode == "" || addr1 == "" || addr2 == "" || addressType == "") {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        if(!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }


        UserInfo sess = new UserInfo();

        if(sess.ConId.Length != 6 && sess.ConId.Length != 5) {
            result.success = false;
            result.message = "후원자만 신청 가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var commitInfo = new SponsorAction().GetCommitInfo();
        if(!commitInfo.success) {
            JsonWriter.Write(commitInfo, context);
            return;
        }

        // 최초 한번 컴파스에 주소를 업데이트 한다.
        if(sess.LocationType == "국내") {
            new SponsorAction().UpdateAddress(false, addressType, sess.LocationType, "한국", zipcode, addr1, addr2);
        }


        // 신청내역 조회
        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<tBlueBook>("SponsorID", sess.ConId);
            //if (dao.tBlueBook.Any(p => p.SponsorID == sess.ConId))
            if(exist.Any())
            {
                result.success = false;
                result.message = "이미 신청하셨습니다.";
                JsonWriter.Write(result, context);
                return;
            }

            // 후원자 정보
            var comm_result = new SponsorAction().GetCommunications();
            if (!comm_result.success)
            {
                result.success = false;
                result.message = comm_result.message;
                JsonWriter.Write(result, context);
                return;
            }
            SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;


            // 주소 정보
            var addr_result = new SponsorAction().GetAddress();
            if (!addr_result.success)
            {
                result.success = false;
                result.message = addr_result.message;
                JsonWriter.Write(result, context);
                return;
            }
            SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;

            // UserClass확인
            if (sess.UserClass == null || sess.UserClass == "")
            {
                result.success = false;
                result.message = "회원님의 가입유형이 존재하지 않습니다.\n관리자에게 문의해주세요.";
                JsonWriter.Write(result, context);
                return;
            }

            var commitResult = commitInfo.data.ToJson().ToObject<SponsorAction.CommitResult>();
            var entity = new tBlueBook()
            {
                ConID = sess.ConId,
                SponsorID = sess.SponsorID,
                SponsorName = sess.UserName,
                Tel = comm_data.Mobile.TranslatePhoneNumber(),
                ZipCode = addr_data.Zipcode,
                Address1 = addr_data.Addr1,
                Address2 = addr_data.Addr2,
                CommitInfo = "CDSP " + commitResult.CDSP_count + "명, CIV, CSP 등 " + commitResult.CIVCSP_count.ToString() + "원",
                RegisterDate = DateTime.Now,
                RegisterID = sess.UserId,
                RegisterName = sess.UserName
            };

            //dao.tBlueBook.InsertOnSubmit(entity);
            www6.insert(entity);
            //dao.SubmitChanges();
            JsonWriter.Write(result, context);
        }
    }
    void Hits( HttpContext context )
    {
        var idx = Convert.ToInt32(context.Request["idx"].ValueIfNull("-1"));
        if(idx != -1)
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                //var entity = dao.ssb_article.First(p => p.idx == idx);
                var entity = www6.selectQF<ssb_article>("idx", idx);
                entity.view_count++;

                //dao.SubmitChanges();
                www6.update(entity);
            }
        }
    }
}