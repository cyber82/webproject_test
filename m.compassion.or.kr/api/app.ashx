﻿<%@ WebHandler Language="C#" Class="api_app" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_app : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public class app_userEx : app_user {

        public string download_link { get; set; }
        public string latest_version { get; set; }
        public string current_version { get; set; }

        public app_userEx(app_user u) {
            this.au_device = u.au_device;
            this.au_mainpage = u.au_mainpage;
            this.au_moddate = u.au_moddate;
            this.au_push = u.au_push;
            this.au_regdate = u.au_regdate;
        }
    }

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();
        if(t == "get-info") {
            this.GetInfo(context);
        }else if(t == "set-auto-login") {
            this.SetAutoLogin(context);
        }else if(t == "set-mainpage") {
            this.SetMainpage(context);
        }else if(t == "set-push") {
            this.SetPush(context);
        }
    }

    void SetPush(HttpContext context){

        var result = new JsonWriter(){
            success = false
        };


        bool yn = context.Request["yn"].ValueIfNull("Y") == "Y";


        if(!UserInfo.IsLogin) {
            result.message = "로그인이 필요합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        if(!AppSession.HasCookie(context)) {
            result.message = "앱에서만 접근가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        UserInfo sess = new UserInfo();
        var app = AppSession.GetCookie(context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.app_user.FirstOrDefault(p => p.UserID == sess.UserId);
            var entity = www6.selectQF<app_user>("UserID", sess.UserId);

            if (entity == null)
            {
                entity = new app_user()
                {
                    au_mainpage = "child",
                    au_moddate = DateTime.Now,
                    au_device = app.device,
                    au_push = true,
                    au_regdate = DateTime.Now,
                    UserID = sess.UserId
                };

                entity.au_push = yn;
                //dao.app_user.InsertOnSubmit(entity);
                www6.insert(entity);
            }
            else
            {
                entity.au_push = yn;
                www6.update(entity);

            }
            //dao.SubmitChanges();
        }
        result.success = true;
        JsonWriter.Write(result, context);

    }

    void GetInfo(HttpContext context){

        var result = new JsonWriter(){
            success = false
        };

        if(!UserInfo.IsLogin) {
            result.message = "로그인이 필요합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        if(!AppSession.HasCookie(context)) {
            result.message = "앱에서만 접근가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        UserInfo sess = new UserInfo();
        var app = AppSession.GetCookie(context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.app_user.FirstOrDefault(p => p.UserID == sess.UserId);
            var entity = www6.selectQF<app_user>("UserID", sess.UserId);

            if (entity == null)
            {
                entity = new app_user()
                {
                    au_mainpage = "child",
                    au_moddate = DateTime.Now,
                    au_device = app.device,
                    au_push = true,
                    au_regdate = DateTime.Now,
                    UserID = sess.UserId
                };

                //dao.app_user.InsertOnSubmit(entity);
                www6.insert(entity);
            }

            var data = new app_userEx(entity);

            //var v = dao.device_version.FirstOrDefault(p => p.dv_id == app.device);
            var v = www6.selectQF<device_version>("dv_id", app.device);
            if (v == null)
            {
                data.latest_version = "0";
            }
            else
            {
                data.latest_version = v.dv_max;
                data.download_link = v.dv_link;
            }
            data.current_version = app.version;

            result.data = data;

        }

        result.success = true;
        JsonWriter.Write(result, context);

    }

    void SetMainpage(HttpContext context){

        var result = new JsonWriter(){
            success = false
        };

        string val = context.Request["val"];
        if(string.IsNullOrEmpty(val)) {
            result.message = "값이 없습니다.";
            JsonWriter.Write(result, context);
            return;
        }

        if(!UserInfo.IsLogin) {
            result.message = "로그인이 필요합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        if(!AppSession.HasCookie(context)) {
            result.message = "앱에서만 접근가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        UserInfo sess = new UserInfo();
        var app = AppSession.GetCookie(context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.app_user.FirstOrDefault(p => p.UserID == sess.UserId);
            var entity = www6.selectQF<app_user>("UserID", sess.UserId);

            if (entity == null)
            {
                entity = new app_user()
                {
                    au_mainpage = val,
                    au_moddate = DateTime.Now,
                    au_device = app.device,
                    au_push = true,
                    au_regdate = DateTime.Now,
                    UserID = sess.UserId
                };

                //dao.app_user.InsertOnSubmit(entity);
                www6.insert(entity);
            }
            else
            {
                entity.au_mainpage = val;
                www6.update(entity);
            }
            //dao.SubmitChanges();
        }
        result.success = true;
        JsonWriter.Write(result, context);

    }

    void SetAutoLogin(HttpContext context){

        var result = new JsonWriter(){
            success = true
        };

        string yn = context.Request["yn"];
        FrontLoginSession.SetCookie(context, FrontLoginSession.GetCookie(context), yn == "Y");

        JsonWriter.Write(result, context);

    }



}