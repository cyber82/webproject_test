﻿<%@ WebHandler Language="C#" Class="api_userInfo" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_userInfo : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "get") {
            this.GetUserInfo(context);
        }
    }

    void GetUserInfo(HttpContext context) {


        JsonWriter result = new JsonWriter();
        result.success = true;


        UserInfo sess = new UserInfo();

        if(!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        result.data = sess.ToJson();

        JsonWriter.Write(result, context);


    }
}