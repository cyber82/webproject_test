﻿<%@ Page Language="C#" ResponseEncoding="UTF-8" %>
<%
    /* ============================================================================== */
    /* =   PAGE : 결제 요청 PAGE                                                    = */
    /* = -------------------------------------------------------------------------- = */
    /* =   이 페이지는 Payplus Plug-in을 통해서 결제자가 결제 요청을 하는 페이지    = */
    /* =   입니다. 아래의 ※ 필수, ※ 옵션 부분과 매뉴얼을 참조하셔서 연동을        = */
    /* =   진행하여 주시기 바랍니다.                                                = */
    /* = -------------------------------------------------------------------------- = */
    /* =   연동시 오류가 발생하는 경우 아래의 주소로 접속하셔서 확인하시기 바랍니다.= */
    /* =   접속 주소 : http://kcp.co.kr/technique.requestcode.do			        = */
    /* = -------------------------------------------------------------------------- = */
    /* =   Copyright (c)  2013   KCP Inc.   All Rights Reserverd.                   = */
    /* ============================================================================== */
%>

<%
    /* kcp와 통신후 kcp 서버에서 전송되는 결제 요청 정보 */
    String req_tx          = Request.Form[ "req_tx"         ]; // 요청 종류         
    String res_cd          = Request.Form[ "res_cd"         ]; // 응답 코드         
    String tran_cd         = Request.Form[ "tran_cd"        ]; // 트랜잭션 코드     
    String ordr_idxx       = Request.Form[ "ordr_idxx"      ]; // 쇼핑몰 주문번호   
    String buyr_name       = Request.Form[ "buyr_name"      ]; // 주문자명          
    String enc_info        = Request.Form[ "enc_info"       ]; // 암호화 정보       
    String enc_data        = Request.Form[ "enc_data"       ]; // 암호화 데이터     
    /* 기타 파라메터 추가 부분 - Start - */
    String param_opt_1    = Request.Form[ "param_opt_1"     ]; // 기타 파라메터 추가 부분
    String param_opt_2    = Request.Form[ "param_opt_2"     ]; // 기타 파라메터 추가 부분
    String param_opt_3    = Request.Form[ "param_opt_3"     ]; // 기타 파라메터 추가 부분
    /* 기타 파라메터 추가 부분 - End - */  

	String tablet_size     = "1.0"; // 화면 사이즈 고정
	String url             = Request.Url.ToString();
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<title>*** KCP [AX-HUB Version] ***</title>
<meta http-equiv="Content-Type" content="text/html;">
<meta http-equiv="Cache-Control" content="No-Cache">
<meta http-equiv="Pragma" content="No-Cache">

<meta name="viewport" content="width=device-width, user-scalable=<%=tablet_size%>, initial-scale=<%=tablet_size%>, maximum-scale=<%=tablet_size%>, minimum-scale=<%=tablet_size%>">



<!-- 거래등록 하는 kcp 서버와 통신을 위한 스크립트-->
<script type="text/javascript" src="approval_key.js"></script>

<script type="text/javascript">

    var controlCss = "css/style_mobile.css";
    var isMobile = {
        Android: function()
        {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function()
        {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function()
        {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function()
        {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function()
        {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function()
        {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    if( isMobile.any() )
    {
        document.getElementById("cssLink").setAttribute("href", controlCss);
    }
</script>
<script type="text/javascript">
    /* 주문번호 생성 예제 */
    function init_orderid()
    {
        var today = new Date();
        var year  = today.getFullYear();
        var month = today.getMonth() + 1;
        var date  = today.getDate();
        var time  = today.getTime();

        if (parseInt(month) < 10)
        {
            month = "0" + month;
        }

        if (parseInt(date) < 10)
        {
            date  = "0" + date;
        }

        var order_idxx = "TEST" + year + "" + month + "" + date + "" + time;

        document.order_info.ordr_idxx.value = order_idxx;
    }

    /* kcp web 결제창 호츨 (변경불가) */
    function call_pay_form() {
    	var v_frm = document.order_info;
    	document.getElementById("sample_wrap").style.display = "none";
    	document.getElementById("layer_all").style.display = "block";
    	v_frm.target = "frm_all";
    	// 인코딩 방식에 따른 변경 -- Start    
    	if (v_frm.encoding_trans == undefined) {
    		v_frm.action = PayUrl;
    	} else {
		
    		if (v_frm.encoding_trans.value == "UTF-8") {
    			v_frm.action = PayUrl.substring(0, PayUrl.lastIndexOf("/")) + "/jsp/encodingFilter/encodingFilter.jsp";
    			v_frm.PayUrl.value = PayUrl;
    			
    		} else {
    			v_frm.action = PayUrl;
    		}
    	}         // 인코딩 방식에 따른 변경 -- End  
    	if (v_frm.Ret_URL.value == "") {
    		/* Ret_URL값은 현 페이지의 URL 입니다. */
    		alert("연동시 Ret_URL을 반드시 설정하셔야 됩니다."); return false;
    	} else {
    		v_frm.submit();
    	}
    }


    /* kcp 통신을 통해 받은 암호화 정보 체크 후 결제 요청 (변경불가) */
    function chk_pay()
    {
        self.name = "tar_opener";
        var pay_form = document.pay_form;

        if (pay_form.res_cd.value == "3001" )
        {
            alert("사용자가 취소하였습니다.");
            pay_form.res_cd.value = "";
        }

        document.getElementById("sample_wrap").style.display = "block";
        document.getElementById("layer_all").style.display  = "none";

        if (pay_form.enc_info.value)
        {
            pay_form.submit();
        }
    }
</script>
</head>

<body onload="init_orderid();chk_pay();">
<div id="sample_wrap">
<form name="order_info" method="post">
	<input type="hidden" name="encoding_trans" value="UTF-8" /> 
	<input type="hidden" name="PayUrl" >

    <!-- 타이틀 -->
    <h1>[결제요청] <span>이 페이지는 결제를 요청하는 샘플(예시) 페이지입니다.</span></h1>

    <div class="sample">

    <!-- 상단 문구 -->
    <p>
        이 페이지는 결제를 요청하는 페이지입니다
    </p>

    <!-- 주문 정보 -->
    <h2>&sdot; 주문 정보</h2>
    <table class="tbl" cellpadding="0" cellspacing="0">
        <tr>
            <th>주문 번호</th>
            <td><input type="text" name="ordr_idxx" class="w200" value=""></td>
        </tr>
        <tr>
            <th>주문자명</th>
            <td><input type="text" name="buyr_name" class="w100" value="홍길동"></td>
        </tr>
        <tr>
            <th>그룹 아이디</th>
            <td><input type="text" name="kcp_group_id" class="w100" value="BA0011000348"></td> <!-- BA001 : BA0011000348 -->
        </tr>
        <tr>
            <th>결제 금액</th>
            <td><input type="text" name="good_mny" class="w100" value="1004"></td>
        </tr>
        <tr>
            <th>상품명</th>
            <td><input type="text" name="good_name" class="w100" value="핸드폰"></td>
        </tr>
    </table>

    <!-- 결제 요청/처음으로 이미지 -->
    <div class="footer">
        <b>※ PC에서 결제요청시 오류가 발생합니다. ※</b>
    </div>
    <div class="btnset" >
        <input name="" type="button" class="submit" value="결제요청" onclick="kcp_AJAX();">
        
    </div>
    </div>
    

    <!-- 공통정보 -->
    <input type="hidden" name="req_tx"          value="pay">                           <!-- 요청 구분 -->
    <input type="hidden" name="shop_name"       value="KCPTEST">                       <!-- 사이트 이름 --> 
    <input type="hidden" name="site_cd"         value="BA001">                         <!-- 사이트 코드 -->
    <input type="hidden" name="currency"        value="410"/>                          <!-- 통화 코드 -->
    <input type="hidden" name="eng_flag"        value="N"/>                            <!-- 한 / 영 -->
    <input type="hidden" name='kcp_cert_flag'   value="N"/>
    <input type="text" name="kcp_bath_info_view"     value="Y"> 상품명 노출파라미터 </br>

    <!-- 결제등록 키 -->
    <input type="hidden" name="approval_key"    id="approval">
    <!-- 인증시 필요한 파라미터(변경불가)-->
    <input type="hidden" name="escw_used"       value="N">
    <input type="hidden" name="pay_method"      value="AUTH">
    <input type="hidden" name="ActionResult"    value="batch">
    <!-- 리턴 URL (kcp와 통신후 결제를 요청할 수 있는 암호화 데이터를 전송 받을 가맹점의 주문페이지 URL) -->
    <input type="hidden" name="Ret_URL"         value="<%= url%>">
    <!-- 화면 크기조정 -->
    <input type="hidden" name="tablet_size"     value="<%= tablet_size%>">

    <!-- 추가 파라미터 ( 가맹점에서 별도의 값전달시 param_opt 를 사용하여 값 전달 ) -->
    <input type="hidden" name="param_opt_1"	   value="<%=param_opt_1%>">
    <input type="hidden" name="param_opt_2"	   value="<%=param_opt_2%>">
    <input type="hidden" name="param_opt_3"	   value="<%=param_opt_3%>">


</form>
</div>

<!-- 스마트폰에서 KCP 결제창을 레이어 형태로 구현-->
<div id="layer_all" style="position:absolute; left:0px; top:0px; width:100%;height:100%; z-index:1; display:none;">
    <table height="100%" width="100%" border="-" cellspacing="0" cellpadding="0" style="text-align:center">
        <tr height="100%" width="100%">
            <td>
                <iframe name="frm_all" frameborder="0" marginheight="0" marginwidth="0" border="0" width="100%" height="100%" scrolling="auto"></iframe>
            </td>
        </tr>
    </table>
</div>
<form name="pay_form" method="post" action="pp_cli_hub">
    <input type="hidden" name="req_tx"         value="<%=req_tx%>">               <!-- 요청 구분          -->
    <input type="hidden" name="res_cd"         value="<%=res_cd%>">               <!-- 결과 코드          -->
    <input type="hidden" name="tran_cd"        value="<%=tran_cd%>">              <!-- 트랜잭션 코드      -->
    <input type="hidden" name="ordr_idxx"      value="<%=ordr_idxx%>">            <!-- 주문번호           -->
    <input type="hidden" name="buyr_name"      value="<%=buyr_name%>">            <!-- 주문자명           -->
    <input type="hidden" name="enc_info"       value="<%=enc_info%>">
    <input type="hidden" name="enc_data"       value="<%=enc_data%>">

    <!-- 추가 파라미터 -->
    <input type="hidden" name="param_opt_1"	   value="<%=param_opt_1%>">
    <input type="hidden" name="param_opt_2"	   value="<%=param_opt_2%>">
    <input type="hidden" name="param_opt_3"	   value="<%=param_opt_3%>">
</form>
</body>
</html>
