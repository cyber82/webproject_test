<%@ Page Language="C#" AutoEventWireup="true" CodeFile="./pp_cli_hub.aspx.cs" Inherits="KCP.PP_CLI_COM.SRC.pp_cli_com" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
        <title>스마트폰 웹 결제창</title>
        <script type="text/javascript">
            function goResult()
            {
                document.pay_info.submit()
            }

            // 결제 중 새로고침 방지 샘플 스크립트 (중복결제 방지)
            function noRefresh()
            {
                /* CTRL + N키 막음. */
                if ((event.keyCode == 78) && (event.ctrlKey == true))
                {
                    event.keyCode = 0;
                    return false;
                }
                /* F5 번키 막음. */
                if(event.keyCode == 116)
                {
                    event.keyCode = 0;
                    return false;
                }
            }
            document.onkeydown = noRefresh ;
        </script>
    </head>

    <body onload="goResult()">
    <form name="pay_info" method="post" action="result">
        <input type="hidden" name="res_cd"          value="<%= res_cd           %>">    <!-- 결과 코드 -->
        <input type="hidden" name="res_msg"         value="<%= res_msg          %>">    <!-- 결과 메세지 -->
        <input type="hidden" name="ordr_idxx"       value="<%= ordr_idxx        %>">    <!-- 주문번호 -->
        <input type="hidden" name="good_mny"        value="<%= good_mny         %>">    <!-- 결제금액 -->
        <input type="hidden" name="good_name"       value="<%= good_name        %>">    <!-- 상품명 -->
        <input type="hidden" name="buyr_name"       value="<%= buyr_name        %>">    <!-- 주문자명 -->

        <!-- 신용카드 정보 -->
        <input type="hidden" name="card_cd"         value="<%= card_cd          %>">    <!-- 카드코드 -->
        <input type="hidden" name="card_name"       value="<%= card_name        %>">    <!-- 카드이름 -->
        <input type="hidden" name="batch_key"       value="<%= batch_key        %>">    <!-- 배치 인증키 -->

    </form>
    </body>
    </html>
