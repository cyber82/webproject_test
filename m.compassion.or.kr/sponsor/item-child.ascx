﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="item-child.ascx.cs" Inherits="sponsor_item_child" %>

 <!--상단 정보 스타일 1(1:1 어린이 양육)-->
         <div class="sponsordetail-child">
            <span class="frame"><span class="photo" style="background:url('<asp:Literal runat="server" ID="img" />') no-repeat"></span></span>    <!--어린이사진 inline style-->
            <em class="txt-type"><asp:Literal runat="server" ID="typeName" /></em>
            <p class="txt-name"><asp:Literal runat="server" ID="c_namekr" /></p>
             <p class="txt-info"><span class="color2">어린이ID</span> <asp:Literal runat="server" ID="c_childkey" /></p>
             <p class="txt-info"><span class="color2">국가</span> <asp:Literal runat="server" ID="c_country" /></p>
            <p class="txt-info"><span class="color2">생일</span> <asp:Literal runat="server" ID="c_birth" /> (<asp:Literal runat="server" ID="c_age" />세)</p>
            <p class="txt-info"><span class="color2">성별</span> <asp:Literal runat="server" ID="c_gender" /></p>
        </div>
        
        <table class="list-sponsordetail">
			<caption>후원유형,후원금액,매월 결제 금액</caption>
			<colgroup><col style="width:40%" /><col style="width:60%" /></colgroup>
			<tbody>
				<tr>
					<th>후원유형</th>
					<td><asp:Literal runat="server" ID="frequency"/>후원</td>
				</tr>
				<tr runat="server" id="pn_paymethod" visible="false">
					<th>납부방법</th>
					<td><asp:Literal runat="server" ID="paymethod"/></td>
				</tr>
				<tr>
					<th>후원금액</th>
					<td><asp:Literal runat="server" ID="amount"/></td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<th>총 후원 금액</th>
					<td><asp:Literal runat="server" ID="amount2"/></span><span class="won" runat="server" id="amount2_unit"> 원/월</span></td>
				</tr>
			</tfoot>
		</table>


