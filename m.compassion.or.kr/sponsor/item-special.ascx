﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="item-special.ascx.cs" Inherits="sponsor_item_special" %>


<div class="sponsordetail-child2" >
	<span class="photo" style="background:url('<asp:Literal runat="server" ID="img" />') no-repeat"></span>
	<p class="box-slogon">
		<em class="txt-type"><asp:Literal runat="server" ID="typeName" /></em><br />
		<strong><asp:Literal runat="server" ID="title" /></strong>
	</p>
</div>

 <table class="list-sponsordetail">
	<caption>후원유형,후원금액,매월 결제 금액</caption>
	<colgroup><col style="width:40%" /><col style="width:60%" /></colgroup>
	<tbody>
		<tr>
			<th>후원유형</th>
			<td><asp:Literal runat="server" ID="frequency"/>후원</td>
		</tr>
		<tr runat="server" id="pn_paymethod" visible="false">
			<th>납부방법</th>
			<td><asp:Literal runat="server" ID="paymethod"/></td>
		</tr>
		<tr>
			<th>후원금액</th>
			<td><asp:Literal runat="server" ID="amount"/></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th><asp:Literal runat="server" ID="amount2_title"/> 후원 금액</th>
			<td><asp:Literal runat="server" ID="amount2"/></span><span class="won" runat="server" id="amount2_unit"> 원/월</span></td>
		</tr>
	</tfoot>
</table>

