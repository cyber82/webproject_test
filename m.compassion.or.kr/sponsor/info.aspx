﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="info.aspx.cs" Inherits="sponsor_info" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/sponsor/item-child.ascx" TagPrefix="uc" TagName="child" %>
<%@ Register Src="/sponsor/item-user-funding.ascx" TagPrefix="uc" TagName="uf" %>
<%@ Register Src="/sponsor/item-special.ascx" TagPrefix="uc" TagName="sf" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/sponsor/info.js"></script>
	<script>
		$(function () {
			$("h2#title").text($("#hd_title").val());
		})
	</script>

    <script type="text/javascript"> 

  var _ntp = {}; 

  _ntp.host = (('https:' == document.location.protocol) ? 'https://' : 'http://') 

  _ntp.dID = 779; 

  document.write(unescape("%3Cscript src='" + _ntp.host + "nmt.nsmartad.com/content?cid=1' type='text/javascript'%3E%3C/script%3E")); 

</script> 

<script>

 callback = function(){}

</script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hd_title" value="" />
	<a runat="server" id="btnList"></a>
	
<div class="wrap-sectionsub">
    
    <div class="member-join sponsor-payment">
    	
		<uc:child runat="server" ID="view_child" Visible="false" />

		<uc:uf runat="server" ID="view_uf" Visible="false" />

		<uc:sf runat="server" ID="view_sf" Visible="false" />

        
        <div class="tab-column-3 mt20">
            <a class="selected btn_tab" id="btn_member" href="#">회원후원</a>

			<asp:PlaceHolder runat="server" ID="ph_frequency_temporary" Visible="false">
            <a href="#" class="btn_tab"  id="btn_nonmember">비회원후원</a>
            <a href="#" class="btn_tab" id="btn_noname">무기명후원</a>
			</asp:PlaceHolder>

			<asp:PlaceHolder runat="server" ID="ph_frequency_regular" Visible="false">
            <a href="<%:this.ViewState["joinUrl"].ToString() %>" onclick="_nto.callTrack('5578', callback());" >가입 후 바로 후원</a>
			</asp:PlaceHolder>
        </div>

		<!-- 안내 메시지 -->
		<p class="txt1">정기후원의 경우 회원가입 후 후원이 가능합니다.</p>

        <!---->
        <div class="sponsordetail-login sectionsub-margin1 pn_login pn_add_form" style="display:block">
        	로그인을 하시면 결제정보나 국세청 영수증 조회 등을 이용하실 수 있고, 후원에 관한 다양한 정보도 받아보실 수 있습니다.<br />지금 바로 간편가입 해 보세요.
            <fieldset>
            	<legend>로그인 입력</legend>
                <div class="row"><asp:TextBox TextMode="Email" id="user_id" runat="server" value="" placeholder="아이디 입력" style="width:100%" /></div>
                <div class="row"><asp:TextBox TextMode="password" id="user_pwd" runat="server" value="" placeholder="비밀번호 입력" style="width:100%" /></div>
            </fieldset>
            <div class="etc-link">
            	<a href="<%:this.ViewState["joinUrl"].ToString() %>" class="btn_join" onclick="_nto.callTrack('5578', callback());">회원가입</a><span class="bar">|</span>
                <a href="<%:this.ViewState["findIdUrl"].ToString() %>" class="btn_id">아이디찾기</a><span class="bar">|</span>
                <a href="<%:this.ViewState["findPwUrl"].ToString() %>" class="btn_pw">비밀번호찾기</a>
            </div>
            <div class="wrap-bt"><a class="bt-type11" style="width:100%" href="#" id="btn_login" onclick="_nto.callTrack('5579', callback());">로그인</a></div>
        </div>
        
		 <!--무기명 후원 선택 시  다른부분 모두 노출되지 않음-->
		<div class="pn_noname pn_add_form" style="display:none">
        <div class="msg-graybox mt10">
       		<strong>무기명 후원 시 후원자 정보가 없어<br />
                추후 <span class="color1">소득공제 및 후원관련 확인 및 증빙서류</span>를<br />
                받으실 수 없습니다.
			</strong>
        </div>
        
        <div class="wrap-bt"><a class="bt-type6" style="width:100%" href="/sponsor/pay/temporary/?noname=Y" >다음</a></div>
		</div>

         <!--비회원 후원 선택 시 영역 노출-->
        <div class="sponsordetail-login sectionsub-margin1 style-add mt30 pn_nonmember pn_add_form" style="display:none">
        	로그인을 하시면 결제정보나 국세청 영수증 조회 등을 이용하실 수 있고, 후원에 관한 다양한 정보도 받아보실 수 있습니다. 지금 바로 간편가입 해 보세요.

            <div class="wrap-bt"><a class="bt-type2 btn_join" style="width:100%" href="<%:this.ViewState["joinUrl"].ToString() %>">회원가입 후 후원</a></div>
            <div class="wrap-bt mt10"><a class="bt-type1" style="width:100%" href="/sponsor/pay/temporary/" >비회원으로 후원 계속</a></div>
        </div>

    </div>
    
<!--//-->
</div>

</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<form id="exfrm" method="post" action="<%:this.ViewState["LoginPage"].ToString() %>">
		<input type="hidden" id="userId" name="userId" />
		<input type="hidden" id="userPwd" name="userPwd" />
		<input type="hidden" id="r" name="r" value="<%:Request.UrlEx() %>"/>
        <%-- [이종진] 추가 --%>
        <input type="hidden" id="sessionId" name="sessionId" value="<%:this.ViewState["sessionId"] %>"/>
	</form>
</asp:Content>
