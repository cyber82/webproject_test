﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="sponsor_special_view" MasterPageFile="~/main.Master"  %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/sponsor/special/view.js?v=2.1"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="campaignId" value="" />
	<input type="hidden" runat="server" id="frequency" value="" />
	<input type="hidden" runat="server" id="amount" value="" />
	<input type="hidden" runat="server" id="data" value="" />
	<input type="hidden" runat="server" id="hd_percent" value="" />
	<input type="hidden" runat="server" id="hd_isClose" value="N" />
	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="sponsor-special">
    	
			<!-- 비주얼 -->
			<div class="wrap-visualtop sectionsub-margin1" style="background:url('<%:entity.sf_image_m.WithFileServerHost()%>') no-repeat">
        		<div class="dim"></div>    <!--dim-->
				<div class="contain" style="padding-top:50px">    <!--padding으로 컨텐츠 가운데에 맞게 조정-->
					<strong class="txt-slogon"><%:entity.CampaignName %></strong>
					<%:entity.sf_summary %>
					<div class="share-sns">
                		<a href="#" ng-click="sns.show($event)"><img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
					</div>
				</div>
			</div>
			<!--// 비주얼 -->
			
			<!-- 그래프영역 -->
			<div class="bg-gray sectionsub-margin1 direct-backing">
				<div class="div_graph" style="display:<%:percent < 0 ? "none" : "block"%>">
        			<p class="txt-percent"><em><%:percent %>%</em> <%:percent >= 100 ? "달성" : "달성중" %>  <span class="txt-dday"><asp:Literal runat="server" ID="days" /></span></p>
					<span class="wrap-graph">
						<span class="flag"	 style="left:<%:percent %>%"><%:percent %>%</span>   <!--하트좌표 left % 수치-->
						<span class="bg-graph">
							<span class="rate" style="width:<%:percent %>%"><%:percent %>%</span>      <!--노란그래프 width % 수치-->
						</span>
						<span class="txt-graph">
							<span class="txt1"><%:entity.sf_current_amount.ToString("N0") %>원</span>
							<span class="txt2"><%:entity.sf_goal_amount.ToString("N0") %>원</span>
						</span>
					</span>
				</div>
				<asp:PlaceHolder runat="server" ID="ph_amount">
					
					<div class="bg-gray sectionsub-margin1 direct-backing">
						<div class="wrap-tab5">
							<asp:PlaceHolder runat="server" ID="ph_sf_is_regular" Visible="false">
            				<a href="#"  id="sf_is_regular" class="frequency" data-id="정기" ng-click="frequencyClick('정기')" style="width:50%">정기후원</a>
							</asp:PlaceHolder>
							<asp:PlaceHolder runat="server" ID="ph_sf_is_temporary" Visible="false">
							<a href="#" id="sf_is_temporary" class="frequency" data-id="일시" ng-click="frequencyClick('일시')" style="width:50%">일시후원</a>
							</asp:PlaceHolder>
						</div>
						<div class="mt10"><input type="text"id="dsp_amount" value="20,000" class="input_amount number_only use_digit" maxlength="10" style="width:100%"  placeholder="금액을 입력해주세요."/></div>
						<div class="wrap-bt mt10"><a href="#" ng-click="goPay($event)" class="bt-type6" style="width:100%">바로 후원하기</a></div>
					</div>

				</asp:PlaceHolder>
					<!-- 완료되었을때 -->
					<div class="complete-comment" id="complete_comment" runat="server">모금이 완료되었습니다.</div>
					<!--// -->
			</div>
			<!--// 참여인원 -->
        
			<!-- 관리자등록 컨텐츠 -->
			<div class="editor-html">
				<asp:Literal runat="server" ID="content" />

			</div>

			<div ng-if="total > 0">
				<p class="txt-caption mt10">후원 결과 레포트</p>
				<ul class="list-report">
        			<li ng-repeat="item in list">
						<a ng-href="{{item.sr_file}}" target="_blank">{{item.sr_title}}<img src="/common/img/icon/pdf.png" class="pdf" alt="레포트 다운로드" /></a>
        			</li>
				</ul>
        
				<span class="more-loading" ng-click="showMore()" ng-show="total > list.length"><span>더보기</span></span>
			</div>
        	<!-- 관리자등록 컨텐츠 -->

		</div>
		
     <div data-id="footer_action"></div>
	<!--//-->
	</div>


</asp:Content>
