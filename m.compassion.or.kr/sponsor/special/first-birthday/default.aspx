﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="sponsor_special_first_birthday_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	

    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/sponsor/info.js"></script>
    <script type="text/javascript">
        function fnCheckLogin() {
            if ('<%=UserInfo.IsLogin%>'.toLowerCase() != 'true') {
                alert('로그인 후 신청이 가능합니다.');
            }
        }
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<style type="text/css">
    .box-bordergray .txt-title span {color:#2F2D53 ;}
    .box-bordergray .txt-note span {color:#005dab ;}
    </style>
	<div class="wrap-sectionsub">
	<!---->
    
		<div class="sponsor-special">
    	
			<div class="wrap-visualtop sectionsub-margin1" style="background:url('/common/img/page/sponsor/img-topvisual9.jpg') no-repeat">
        		<div class="dim"></div>    <!--dim-->
				<div class="contain" style="padding-top:35px">    <!--padding으로 컨텐츠 가운데에 맞게 조정-->
					<strong class="txt-slogon style-add">Happy 1<span style="vertical-align:super;font-size:11px;">st</span> Birthday<br />첫 생일을 맞아 시작하는 첫 나눔</strong>
					일 년간 건강하게 자라준 아기의 첫 생일을 축하합니다!<br />
					첫 생일 첫 나눔을 통해 더 많은 생명을 살리는 일에동참해주세요!
					<div class="share-sns">
                		<a href="#" ng-click="sns.show($event)"><img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
					</div>
				</div>
			</div>

			<p class="txt-sub color1 mt20">&lowast; 본 캠페인은 국내 회원만 신청 가능합니다. 외국에서 후원을 원하시는 분은 ‘1:1 문의’를 통해 문의해주세요.</p>
        
			<%-- <div class="box-bordergray mt20">
        		<p class="txt-title">컴패션 방문</p>
				<p class="txt-note">컴패션 사옥에 방문하셔서 첫 나눔을 시작해보세요. 컴패션이 마련한 작은 첫 생일상 앞에서 아이와 함께 사진을 찍으시면 아기 사진이 담긴 첫 나눔 기념 증서를 드립니다.</p>
				<a class="bt-type4" href="/sponsor/special/first-birthday/temporary/?g=V" runat="server" id="goPay1">신청하기</a>
			</div>
			<p class="txt-sub color1">&lowast; 방문은 매주 금요일 오후에 진행됩니다.</p>
        
			<div class="box-bordergray mt20">
        		<p class="txt-title">온라인 나눔</p>
				<p class="txt-note">온라인으로 손쉽게 첫 나눔을 시작하실 수 있습니다.  후원과 함께 아기 정보와 사진을 등록하시면 아기 사진이 담긴 첫 생일 첫 나눔 기념 증서를 드립니다.</p>
				<a class="bt-type4" href="/sponsor/special/first-birthday/temporary/?g=P" runat="server" id="goPay2">신청하기</a>
			</div>--%>

             <div class="box-bordergray mt20">
        		<p class="txt-title">첫 생일 첫 나눔</p>
				<p class="txt-note"><span>컴패션과 함께 첫 나눔을 시작해보세요. </span><br />사옥 방문이나 온라인을 통해 아기의 첫 생일을 기념하세요. <br />컴패션이 준비한 아기의 사진이 담긴 첫 나눔 기념증서를 드립니다.</p>
				<a class="bt-type4" href="/sponsor/special/first-birthday/temporary/?g=V" onclick="fnCheckLogin()">컴패션 방문</a>
                <a class="bt-type4" href="/sponsor/special/first-birthday/temporary/?g=P" onclick="fnCheckLogin()">온라인 나눔</a>
			</div>
			<p class="txt-sub color1">&lowast; 방문은 매주 금요일 오후에 진행됩니다.</p>

			<div class="box-bordergray mt20">
        		<p class="txt-title">첫 생일 첫 나눔 | <span>BLUEDOG</span></p>
				<p class="txt-note"><span>컴패션 첫 생일 첫 나눔, 블루독과 함께 해보세요. </span><br />컴패션과 블루독이 함께 준비한 특별한 선물과 기념증서를 드립니다. <br />자세한 안내는 페이지 하단에서 확인하세요.</p>
				<a class="bt-type4" href="/sponsor/special/first-birthday/temporary/?g=V&bluedog=Y" onclick="fnCheckLogin()">컴패션 방문</a>
                <a class="bt-type4" href="/sponsor/special/first-birthday/temporary/?g=P&bluedog=Y" onclick="fnCheckLogin()">온라인 나눔</a>
			</div>

			<div class="bg-gray sectionsub-margin1 mt30">
        		<p class="txt-caption">블루독에서 첫 생일 첫 나눔과 함께 합니다!</p>
				<p class="txt-sub2">컴패션 후원기업 블루독에서 아기를 위한 특별한 선물을 드립니다.</p>
				<div class="mt20"><img src="/common/img/page/sponsor/first_birth01_01.jpg" width="100%" alt="" /></div>
				<div class="mt20"><img src="/common/img/page/sponsor/first_birth01_02.jpg" width="100%" alt="" /></div>
				<ul class="list-bu mt20">
            		<li><span class="color2">기념증서</span> : 블루독과 함께하는 후원자님만을 위한 특별한 기념증서</li>
					<li><span class="color2">사진촬영</span> : 방문 시, 돌상 앞에서 사진 촬영</li>
					<li><span class="color2">후원금 사용처</span> : 후원금 사용처 소개서</li>
					<li><span class="color2">기부금 영수증</span> : 후원금 결제 시 로그인된 사용자 명의로 기부금 영수증 발급</li>
				</ul>
				<p class="txt-sub2 mt10">&lowast; 블루독에서 준비한 선물은 컴패션 ‘첫 생일 첫 나눔’ 일반선물과 중복되지 않습니다.</p>
			</div>

            <div class="bg-gray sectionsub-margin1 mt30">
        		<p class="txt-caption">컴패션에서 소중한 첫 나눔을 함께 하겠습니다!</p>
				<p class="txt-sub2">아기의 첫 나눔을 오랫동안 기억할 수 있는 선물을 드립니다.</p>
				<div class="mt20"><img src="/common/img/page/sponsor/first_birth02_01.jpg" width="100%" alt="" /></div>
				<div class="mt20"><img src="/common/img/page/sponsor/first_birth02_02_1.jpg" width="100%" alt="" /></div>
				<ul class="list-bu mt20">
            		<li><span class="color2">기념증서</span> : 아기의 사진이 담긴 기념 증서</li>
					<li><span class="color2">사진촬영</span> : 방문 시, 돌상 앞에서 사진 촬영</li>
					<li><span class="color2">후원금 사용처</span> : 후원금 사용처 소개서</li>
					<li><span class="color2">기부금 영수증</span> : 후원금 결제 시 로그인된 사용자 명의로 기부금 영수증 발급</li>
				</ul>
				<p class="txt-sub2 mt10">&lowast; 컴패션에서 준비한 선물은 ‘블루독과 함께하는 첫 생일 첫 나눔’ 선물과 중복되지 않습니다.</p>
			</div>

			<p class="txt-caption mt30">후원금은 이렇게 사용됩니다!</p>
			<p class="txt-sub2">후원금은 태국 ‘보 깨오 어린이센터’ 엄마와 아기들의 태아 • 영아생존 후원에 사용됩니다.</p>
			<div class="mt20"><img src="/common/img/page/sponsor/img29.jpg" width="100%" alt="" /></div>
        
			<div class="wrap-bt"><a href="http://www.srook.net/compassion/635974665239422185" target="_blank" class="bt-type6" style="width:100%">컴패션어린이센터 소개 보기</a></div>
        
		</div>
    
	<!--//-->
	</div>

</asp:Content>

