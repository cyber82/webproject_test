﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="temporary.aspx.cs" Inherits="sponsor_special_first_birthday_temporary" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>
<%--<%@ Register Src="/sponsor/pay/temporary/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/sponsor/pay/temporary/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/sponsor/pay/temporary/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>--%>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
	<script type="text/javascript" src="/sponsor/pay/temporary/default.js"></script>
	<script type="text/javascript" src="/sponsor/special/first-birthday/temporary.js?v=1"></script>
	
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>

	<script type="text/javascript">
	    $(function () {

	        if ($("#map").length > 0) {
	            appendGoogleMapApi();

	            $(".btn_loc").click(function () {
	                var num = $(this).index() + 1;

	                $(".btn_loc").removeClass("selected");
	                $(this).addClass("selected");

	                $(".location .loc1").hide();
	                $(".location .loc2").hide();
	                $(".location .loc" + num).show();

	                if (num == 1) {
	                    setPosition(37.537970, 127.005080);
	                } else {
	                    setPosition(35.116564, 129.040821);
	                }
	            })

	        }

	    });

	    function appendGoogleMapApi() {
	        if (typeof google === 'object' && typeof google.maps === 'object') {
	            initMap();
	        } else {
	            var script = document.createElement("script");
	            script.type = "text/javascript";
	            script.src = "https://maps.googleapis.com/maps/api/js?language=ko&region=kr&key=<%:this.ViewState["googleMapApiKey"].ToString() %>&callback=initMap";
				document.body.appendChild(script);
            }
        }

        function setPosition(lat, lng) {
            map.setCenter(new google.maps.LatLng(lat, lng));

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: "/common/img/icon/pin.png",
                map: map
            });

        }

        function initMap() {

            lat = 37.537970;
            lng = 127.005080;

            map = new google.maps.Map(document.getElementById('map'), {
                center: { lat: lat, lng: lng },
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: "/common/img/icon/pin.png",
                map: map
            });


        };


	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" runat="server" id="gubun" value="" />
    <input type="hidden" runat="server" id="bluedog" value="" />
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />
	<input type="hidden" runat="server" id="baby_gender" value="M" />
	<input type="hidden" runat="server" id="visit_region" value="S" />
	
	<input type="hidden" id="gender" runat="server"  />
	<input type="hidden" id="hd_auth_domain" runat="server"  />
	<input type="hidden" id="hd_age" runat="server"  />

	<input type="hidden" id="amount" runat="server"  />
	<input type="hidden" runat="server" id="jumin" value="" />		
	<input type="hidden" runat="server" id="ci" value="" />		<!-- 본인인증 CI -->
	<input type="hidden" runat="server" id="di" value="" />		<!-- 본인인증 DI -->
	<input type="hidden" runat="server" id="cert_gb" value="" />		<!-- 본인인증 수단 -->
	
	<input type="hidden" runat="server" id="user_class" value="20세이상" />
	<input type="hidden" runat="server" id="hdState" value="" />
	<input type="hidden" runat="server" id="hdLocation" value="" />
	<input type="hidden" runat="server" id="hdManageType" value="" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
	<input type="hidden" runat="server" id="hfAddressType" value="" />		<!-- 회원 기본주소와 동기화 하는 기능이 있는경우 hfAddressType 가 필요함 -->
	<input type="hidden" runat="server" id="hdCI" value="" />	<!-- 기존 등록된 본인인증 CI -->
	
	<div style="display:none">
	<input type="checkbox" runat="server" id="addr_overseas" />해외에 거주하고 있어요
	<input type="text" runat="server" id="user_name" />		<!-- 결제시 사용 -->
	</div>


	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!-- ng-app="cps" ng-cloak  ng-controller="defaultCtrl" -->
		<script type="text/javascript">
		    $(function () {
		        $(".tab_select button").click(function () {
		            $(this).parent().find("a").removeClass("selected");
		            $(this).addClass("selected");
		        });
		    })
		</script>
    
		<div class="member-join sponsor-special">
			
			<div class="wrap-visualtop sectionsub-margin1" style="background:url('/common/img/page/sponsor/img-topvisual10.jpg') no-repeat">
        		<div class="dim"></div>    <!--dim-->
				<div class="contain" style="padding-top:35px">    <!--padding으로 컨텐츠 가운데에 맞게 조정-->
					<span class="icon1"></span>
					<strong class="txt-slogon style-add">태국 ‘보 깨오 어린이센터’ <br />엄마와 아기 살리기</strong>
					태아·영아생존 일시후원<br />
					(<%=this.ViewState["bluedog"].ToString() == "Y" ? "30" : "10" %>만원 이상)
				</div>
			</div>
    	
			<p class="txt-title">우리아기 정보</p>
			<div class="tab-column-2 tab_select">
				<a id="btn_babyGender_M" class="btn_babyGender">아들</a>
				<a id="btn_babyGender_F" class="btn_babyGender">딸</a>
			</div>
        
			<fieldset class="frm-input mt10">
				<legend>우리아기 정보 입력</legend>
				<div class="row-table">
					<div class="col-td" style="width:100%">
						<span class="row"><asp:TextBox ID="txtBabyName" runat="server" MaxLength="25" type="text" value="" placeholder="이름" style="width:100%" /></span>
                    
						<span class="row">
							<span class="column" style="width:48%">
								<asp:DropDownList cssclass="custom_sel"  runat="server" ID="baby_birth_yyyy" Width="100%"></asp:DropDownList>
							</span>
							<span class="column" style="width:26%">
								<asp:DropDownList cssclass="custom_sel"  runat="server" ID="baby_birth_mm" Width="100%"></asp:DropDownList>
							</span>
							<span class="column" style="width:26%">
								<asp:DropDownList cssclass="custom_sel"  runat="server" ID="baby_birth_dd" Width="100%"></asp:DropDownList>
							</span>
						</span>
						
						<asp:PlaceHolder runat="server" ID="ph_v1">
						<span class="row pos-relative2 del">
							<input type="text" runat="server" id="lb_file_path" readonly="readonly"  placeholder="아기사진" style="width:100%" />
                    		<a class="bt-type8 pos-btn" style="width:70px" href="#" id="btn_file_path">파일선택</a>
							<a class="bt-type8 pos-btn-del" style="width:50px" href="#" id="btn_file_path_remove">삭제</a>
						</span>
						</asp:PlaceHolder>
					</div>
				</div>
			</fieldset>
        
			<asp:PlaceHolder runat="server" ID="ph_v2">
			<div class="box-gray">
        		<strong>사진 첨부 시 주의사항</strong><br />
				<ul class="list-bullet2 lineheight1">
            		<li>세로 사진을 올려주세요. 세로 사진이 기념증서에 알맞게 제작됩니다.</li>
					<li>400KB 이상의 사진이 예쁘게 제작됩니다.  핸드폰 사진은 다소 해상도, 색상이 떨어질 수 있습니다.</li>
					<li>첨부파일 용량을 2MB이하로 해주세요. 첨부파일은 이미지파일(jpg, jpeg, gif, png)만 가능합니다.</li>
				</ul>
			</div>
			</asp:PlaceHolder>

		
			<p class="txt-title">신청자 정보</p>
			<fieldset class="frm-input mt10">
				<legend>신청자 정보 입력</legend>
				<div class="row-table">
					<div class="col-td" style="width:100%">
						<span class="row"><asp:TextBox ID="txtSponsorName" runat="server" MaxLength="25" placeholder="이름"  style="width:100%" ></asp:TextBox></span>
						<span class="row"><asp:TextBox ID="txtBabyRelation" runat="server" MaxLength="25" placeholder="아기와의 관계"  style="width:100%" ></asp:TextBox></span>
						<span class="row"><asp:TextBox ID="txtPhone" runat="server" MaxLength="11" placeholder="휴대폰번호(-없이 입력)" style="width:100%" ></asp:TextBox></span>
						<span class="row"><asp:TextBox ID="txtEmail" runat="server" MaxLength="1000" placeholder="이메일" style="width:100%" ></asp:TextBox></span>
					</div>
				</div>
			</fieldset>
        
        
			<asp:PlaceHolder runat="server" ID="ph_gubun_V" Visible="false">
			<!--p class="txt-title">방문장소</p>
			<div class="tab-column-2 tab_select">
				<a class="btn_region" runat="server" id="btn_region_S" enableviewstate="true" onserverclick="btn_region_ServerClick">서울 본사</a>
				<a class="btn_region" runat="server" id="btn_region_B" enableviewstate="true" onserverclick="btn_region_ServerClick">부산 지사</a>
			</div-->


			<p class="txt-title">방문일자</p>
			<fieldset class="frm-input mt10">
				<legend>방문일자 정보 입력</legend>
				<div class="row-table">
					<div class="col-td" style="width:100%">
						<span class="row">
							<asp:DropDownList ID="ddlVisitSelect" cssclass="custom_sel" runat="server" Width="100%"></asp:DropDownList>
						</span>
					</div>
				</div>
			</fieldset>

			<!--div class="link-branchinfo">
        		<span class="btn_loc" >서울 본사 위치보기</span>
				<span class="btn_loc" >부산 지사 위치보기</span>
			</div-->

			<div class="view-branchinfo" style="margin-top:10px;">   <!--지도보기-->
        		
				<div class="map" id="map" style="height:197.5px;"></div>
				<p class="txt-red">주차장이 협소하니, 가급적 대중교통을 이용해 주시기 바랍니다.</p>
            
				<!-- 서울 -->
				<div class="loc1">
					<p class="txt-header">주소</p>
					서울시 용산구 한남대로 102-5 석전빌딩
					<div class="linebar"></div>
            
					<p class="txt-header">오시는 길</p>
					<p class="txt-header2">지하철</p>
					<ul class="list-bullet2">
            			<li>6호선 한강진역 2번 출구, 도보 10분~15분</li>
						<li>중앙선 한남역 1번 출구, 도보 10분~15분</li>
					</ul>
            
					<p class="txt-header2">버스</p>
					<ul class="list-bullet2" style="margin-bottom:0">
            			<li>한남동 '순천향대학병원' 정류장</li>
					</ul>
					<p class="txt-gray"><strong>[간선/지선]</strong> 110A,110B,140,142,144,400,402, 405,407,408,420,471,472,3011,6211,N13,N37</p>
					<p class="txt-gray"><strong>[직행/급행]</strong> 1005-1,1005-2,1150,1500,5000,5005,5007,5500,5500-1,5500-2,6030,7900,8100,8130,8150,8200,8800,9000,9001,9003,9007,9300,9401,9401B,9409</p>
				</div>
				
				<!-- 부산 -->
				<div class="loc2" style="display:none">
					<p class="txt-header">주소</p>
					부산시 동구 중앙대로 216 교원빌딩 17층
					<div class="linebar"></div>
            
					<p class="txt-header">오시는 길</p>
					<p class="txt-header2">지하철</p>
					<ul class="list-bullet2">
            			<li>1호선 부산역 10번 출구</li>
						<li>초량동 방향으로 도보 5분~10분</li>
					</ul>
            
					<p class="txt-header2">버스</p>
					<ul class="list-bullet2" style="margin-bottom:0">
            			<li>초량동 '부산역' 정류장</li>
					</ul>
					<p class="txt-gray"><strong>[일반]</strong> 2,13,17,26,27,39,40,41,43,48,61,67,81,82,85,87,88,101,103,139,167,190,508</p>
					<p class="txt-gray"><strong>[급행]</strong> 1000,1001,1003,1004</p>
				</div>

			</div>
			</asp:PlaceHolder>
			
			<!-- 배송지 입력 -->
			<asp:PlaceHolder runat="server" ID="ph_gubun_P" Visible="true">
			<p class="txt-title"><%=gubun.Value == "P" ? "배송지 정보" : "기념증서 배송지 정보" %></p>
			<fieldset class="frm-input">
				<legend>배송지 정보 입력</legend>
				<div class="row-table">
					<div class="col-td" style="width:100%">
						<span class="row pos-relative2">
							
							<input type="hidden" id="addr_domestic_zipcode" runat="server" />
							<input type="hidden" id="addr_domestic_addr1" runat="server" /> 
							<input type="hidden" id="addr_domestic_addr2" runat="server" />	

							<input type="text" id="post" readonly="readonly" placeholder="주소" style="width:100%" /><a class="bt-type8 pos-btn" style="width:70px" ng-click="findAddr($event)">주소찾기</a>

						</span>
						<!--<span class="row"><input type="text" id="addr_1" placeholder="상세주소" style="width:100%" readonly="readonly"/></span>-->
						
						<span id="addr_road" class="row fs13 mt15"></span>
						<span id="addr_jibun" class="row fs13"></span>

					</div>
				</div>
			</fieldset>


			</asp:PlaceHolder>
			<p class="txt-title">후원금액</p>

			<fieldset class="frm-input mt10">
				<legend>후원금액 정보 입력</legend>
				<div class="row-table">
					<div class="col-td" style="width:100%">
						<span class="row">
                    		<ul class="list-supportamount">
                                <% if (this.ViewState["bluedog"].ToString() == "Y") { %>
								<li><span class="box-amount pay_amount" data-amount="300000">300,000원</span></li>
								<li><span class="box-amount pay_amount" data-amount="400000">400,000원</span></li>
                                <% } else { %>
                                <li><span class="box-amount pay_amount" data-amount="100000">100,000원</span></li>
								<li><span class="box-amount pay_amount" data-amount="300000">300,000원</span></li>
                                <% } %>
								<li><span class="box-amount pay_amount" data-amount="500000">500,000원</span></li>
								<li><span class="box-amount pay_amount" data-amount="1000000">1,000,000원</span></li>
								<li><span class="box-amount pay_amount" data-amount="2000000">2,000,000원</span></li>
								<li><span class="box-amount pay_amount" data-amount="3650000">3,650,000원</span></li>
							</ul>
						</span>
						<span class="row"><input type="text" id="pay_amount_custom" class="pay_amount_custom number_only" maxlength="8" value="" placeholder="직접입력(<%=this.ViewState["bluedog"].ToString() == "Y" ? "30" : "10" %>만원이상 입력 가능)" style="width:100%" /></span>
						<p class="txt-error" id="pay_amount_custom_err" style="display:none;">금액입력은 <%=this.ViewState["bluedog"].ToString() == "Y" ? "30" : "10" %>만원 이상 입력해 주세요.</p>   <!--에러메시지 경우에만 출력-->
                    
						<span class="row">
                    		<span class="total-amount">총 결제금액<em><em id="txt_amount">0</em> 원</em></span>
						</span>
                    
						<asp:PlaceHolder runat="server" ID="ph_no_jumin" Visible="false">

						<span class="row">
                    		<span class="checkbox-ui">
								<input type="checkbox" class="css-checkbox" id="p_receipt_pub_ok" name="p_receipt_pub" runat="server"  checked >
								<label for="p_receipt_pub_ok" class="css-label">국세청 연말정산 영수증 신청</label>
							</span>
						</span>
						<span class="row pos-relative2"><input type="text" class="number_only" value="" data-id="jumin1" runat="server" id="jumin1" maxlength="13" placeholder="주민등록번호(-없이 입력)" style="width:100%" />
						<a class="bt-type9 pos-btn" style="width:70px" href="#" id="btn_name_check">실명인증</a></span>

						<p class="txt-result" id="msg_name_check" style="display:none"><span class="guide_comment1"></span></p>
						</asp:PlaceHolder>


						<!-- 모바일 디자인 추가 필요 (현재 웹버전 ) -->
						<asp:PlaceHolder runat="server" ID="ph_yes_jumin" Visible="false">
							<div class="receipt_infoBox">
								<p class="txt1">기존에 등록하신 <em class="fc_blue">국세청 연말정산 영수증 신청 정보</em>가 있습니다.</p>
								<p>기존 정보에 연말정산 정보가 추가로 등록됩니다.</p>
							</div>
						</asp:PlaceHolder>


					</div>
				</div>
			</fieldset>
        
       
			<asp:PlaceHolder runat="server" ID="ph_cert" Visible="false" >
			<p class="txt-title">본인인증</p>
			<p class="txt">온/오프라인 기존 후원정보 확인을 위해 본인 인증을 진행해 주세요.</p>
			<div class="self-account">
				<div class="link-account">
            		<a href="#" id="btn_cert_by_phone"><span>휴대폰 인증</span></a>
					<a href="#" id="btn_cert_by_ipin"><span>아이핀(i-PIN) 인증</span></a>
				</div>
				<div class="box-gray">
            		<ul class="list-bullet2">
                		<li>본인명의의 휴대폰으로만 본인인증이 가능합니다.<br />
						아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다. </li>
					</ul>
				</div>
			</div> 
			<p id="msg_cert" class="txt-result" style="display:none">본인 인증이 완료되었습니다.</p>
			</asp:PlaceHolder>
        
        
			<p class="txt-title">결제방법</p>
			 <ul class="list-paymethod">
         		<li><span class="radio-ui">
					<input type="radio" id="payment_method_card" runat="server" name="payment_method" class="css-radio" checked />
					<label for="payment_method_card" class="css-label">신용카드</label>
				</span></li>
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_cms" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_cms" class="css-label">실시간 계좌이체</label>
				</span></li>
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_kakao" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_kakao" class="css-label"><img src="/common/img/icon/kakaopay.png" alt="카카오 페이" class="txt-kakaopay" /></label>
				</span></li>
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_payco" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_payco" class="css-label"><img src="/common/img/icon/payco.png" alt="페이코" class="txt-payco" /></label>
				</span></li>
				 <!--
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_oversea" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_oversea" class="css-label">해외발급 카드</label>
				</span></li>
				 -->
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_phone" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_phone" class="css-label">휴대폰결제</label>
				</span></li>
			 </ul>

			<div class="box-gray">
				<ul class="list-bullet2">
					<li>후원금 결제 시 일부 카드사의 경우 결제 대행업체인 [NHN KCP 한국 사이버결제]로 승인 SMS가 전송됩니다.</li>
					<li>결제관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</li>
				</ul>
			</div>
        
			<div class="wrap-bt"><asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" CssClass="bt-type6" Width="100%">신청 및 결제</asp:LinkButton></div>
        
        
		</div>
    
	<!--//-->
	</div>



</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form" />
	<uc:payco_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="payco_form" />
	<uc:kakaopay_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kakaopay_form" />
</asp:Content>