﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_special_first_birthday_default : MobileFrontBasePage {
	
	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

        //if(!UserInfo.IsLogin) {
        //	goPay1.HRef = goPay2.HRef = "javascript:alert('로그인 후에 신청 가능합니다.');location.href='"+ base.GetLoginPage() + "'";
        //}

        /*         이벤트 정보 등록  -- 20170327 이정길       */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;
        //Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        //Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));

        //김태형 2017.06.01
        if (Request.Url.Host.IndexOf('.') != -1)
        {
            Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
            Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        }
        else
        {
            Response.Cookies["e_id"].Domain = "localhost";
            Response.Cookies["e_src"].Domain = "localhost";
        }

    }


    protected override void loadComplete( object sender, EventArgs e ) {

        // SNS
        //this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
        this.ViewState["meta_title"] = "[한국컴패션-첫 생일 첫 나눔]";

        // optional(설정 안할 경우 주석)
        //this.ViewState["meta_description"] = "";
        //this.ViewState["meta_keyword"] = "";
        // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,MobileFrontBasePage)
        this.ViewState["meta_image"] = ConfigurationManager.AppSettings["domain"] + "/common/img/page/sponsor/img-topvisual9.jpg";


    }




}