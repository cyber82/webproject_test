﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_special_default : MobileFrontBasePage {
	
	public override bool RequireSSL {
		get {
			return false;
		}
	}

    public WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

    protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		this.GetList();
	}

	void GetList()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tSpecialFunding_list_f("", "", "N", "", "").ToList();
            Object[] op1 = new Object[] { "AccountClassGroup", "AccountClass", "fixedYN", "ufYN", "pick" };
            Object[] op2 = new Object[] { "", "", "N", "", "" };
            var list = www6.selectSP("sp_tSpecialFunding_list_f", op1, op2).DataTableToList<sp_tSpecialFunding_list_fResult>();

            foreach (var entity in list)
            {
                entity.sf_image = entity.sf_image.WithFileServerHost();

                Object[] objParam = new object[] { "CampaignID" };
                Object[] objValue = new object[] { entity.CampaignID };
                Object[] objSql = new object[] { "sp_S_Campaign_PaymentTotal" };
                DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["TOTAL"] != DBNull.Value)
                    {
                        long total = Convert.ToInt32(ds.Tables[0].Rows[0]["TOTAL"]);
                        entity.sf_current_amount = total;
                    }
                    else
                    {
                        entity.sf_current_amount = 0;
                    }
                }
            }

            repeater.DataSource = list;
            repeater.DataBind();
        }

	}



	protected void repeater_ItemDataBound( object sender, RepeaterItemEventArgs e ) {

		//var heart = e.Item.FindControl("heart") as HtmlGenericControl;
		var per = e.Item.FindControl("per") as HtmlGenericControl;
		//var txt = e.Item.FindControl("txt") as HtmlGenericControl;
		var graph = e.Item.FindControl("graph") as HtmlGenericControl;
		//var layer_banner = e.Item.FindControl("layer_banner") as HtmlGenericControl;

		
		sp_tSpecialFunding_list_fResult entity = e.Item.DataItem as sp_tSpecialFunding_list_fResult;

		int percent = 0;
		if(entity.sf_goal_amount < 1) {
			percent = -1;
		} else {
			if(entity.sf_current_amount < 1)
				percent = 0;
			else
				percent = Convert.ToInt32(entity.sf_current_amount * 1.0 / entity.sf_goal_amount * 100);
			
			if(percent > 100)
				percent = 100;
		}

		//layer_banner.Style["top"] = percent < 0 ? "230px" : "";
		//heart.Style["left"] = percent.ToString() + "%";
		//txt.InnerHtml = string.Format("<em>{0}%</em> {1}" , percent.ToString() , percent < 100 ? "달성중" : "달성");
		per.Style["width"] = percent.ToString() + "%";
		graph.Style["display"] = percent < 0 ? "none" : "block";

	}
}