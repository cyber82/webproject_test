﻿
$(function () {

	$page.init();

});


var $page = {

	init: function () {

		$('input[type="radio"].frequency').click(function () {

			var self = $(this);
			var codeId = self.data("codeid");
			var amount = $(".amount[data-codeId='" + codeId + "']");

		//	amount.prop("readonly", self.val() == "정기");

			if (self.val() == "정기") {
				amount.val("20,000");
			} else {
				amount.val("20,000");
			}
		})

		$('a[data-action="pay"]').each(function () {

			var self = $(this);
			
			self.click(function () {

				var codeId = self.data("codeid");
				var campaignId = self.data("campaignid");

				var frequency = $(".frequency[data-codeId='" + codeId + "']:checked").val();
				var amount = $(".amount[data-codeId='" + codeId + "']").val().replace(/\,/g , "");

				if (amount < 10000) {
					alert("후원금은 1만원 이상 입력 가능합니다.");
					return false;
				}

				if (amount % 1000 > 0) {
					alert("죄송합니다.\n천원단위로 후원 가능합니다.");
					return false;
				}

				$.post("/sponsor/pay-gateway.ashx?t=go-special", { campaignId: campaignId, frequency: frequency, amount: amount }).success(function (r) {

					if (r.success) {
						location.href = r.data;
					} else {
						alert(r.message);
					}
				});


				return false;
			})
			
		})

		this.setMainVisual();


	},

	setMainVisual : function() {
		console.log("test")

		var swiper = new Swiper('.swiper-container', {
			pagination: '.swiper-container .page-count',
			paginationClickable: true,
			nextButton: '.swiper-container .page-next',
			prevButton: '.swiper-container .page-prev',
			paginationBulletRender: function (index, className) {
				return '<span class="' + className + '">' + (index + 1) + '</span>';
			},
			spaceBetween: 30
		});


	}

};

