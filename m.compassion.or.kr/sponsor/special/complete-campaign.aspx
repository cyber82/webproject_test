﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete-campaign.aspx.cs" Inherits="sponsor_special_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
    
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/sponsor/special/complete-campaign.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="sponsor-special">
    	
			<div class="wrap-tableW">
				<table class="tableW style-add">
					<caption>캠페인 제목,캠페인 기간</caption>
					<colgroup><col width="46%" /><col width="54%" /></colgroup>
					<thead>
						<tr>
							<th>캠페인 제목</th>
							<th>캠페인 기간</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="item in complete.list">
							<td class="align-left"><a href="#" ng-click="complete.goView($event,item)">{{item.campaignname}}</a></td>
							<td>{{item.startdate | date:'yyyy.MM.dd'}} ~ {{item.enddate | date:'yyyy.MM.dd'}}</td>
						</tr>
						
						<!-- 완료된 캠페인 없을 경우 -->
						<tr>
							<td colspan="2" class="no-result" ng-if="complete.total == 0">완료된 캠페인이 없습니다.</td>
						</tr>
						
					</tbody>
				</table>
			</div>
        
			<paging class="small" page="complete.params.page" page-size="complete.params.rowsPerPage" total="complete.total" show-prev-next="true" show-first-last="true" paging-action="complete.getList({page : page});"></paging>  

        
        
    
		</div>
    
	<!--//-->
	</div>


</asp:Content>
