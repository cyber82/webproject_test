﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="temporary.aspx.cs" Inherits="sponsor_special_wedding_temporary" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>
<%--<%@ Register Src="/sponsor/pay/temporary/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/sponsor/pay/temporary/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/sponsor/pay/temporary/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>--%>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
	<script type="text/javascript" src="/sponsor/pay/temporary/default.js"></script>
	<script type="text/javascript" src="/sponsor/special/wedding/temporary.js"></script>
	<style>
		.ui-datepicker select.ui-datepicker-year {width: 60%;}
	</style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />
	
	<input type="hidden" id="gender" runat="server"  />
	<input type="hidden" id="hd_auth_domain" runat="server"  />
	<input type="hidden" id="hd_age" runat="server"  />

	<input type="hidden" id="amount" runat="server"  />
	
	<input type="hidden" runat="server" id="jumin" value="" />		
	<input type="hidden" runat="server" id="ci" value="" />		<!-- 본인인증 CI -->
	<input type="hidden" runat="server" id="di" value="" />		<!-- 본인인증 DI -->
	<input type="hidden" runat="server" id="cert_gb" value="" />		<!-- 본인인증 수단 -->
	
	<input type="hidden" runat="server" id="user_class" value="20세이상" />
	<input type="hidden" runat="server" id="hdState" value="" />
	<input type="hidden" runat="server" id="hdLocation" value="" />
	<input type="hidden" runat="server" id="hdManageType" value="" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
	<input type="hidden" runat="server" id="hfAddressType" value="" />		<!-- 회원 기본주소와 동기화 하는 기능이 있는경우 hfAddressType 가 필요함 -->
	<input type="hidden" runat="server" id="hdCI" value="" />	<!-- 기존 등록된 본인인증 CI -->
	
	<div style="display:none">
	<input type="checkbox" runat="server" id="addr_overseas" />해외에 거주하고 있어요
	<input type="text" runat="server" id="user_name" />		<!-- 결제시 사용 -->
	</div>

	<div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
<!---->
    
		<div class="member-join sponsor-payment sponsor-special">
    	
			<div class="wrap-visualtop sectionsub-margin1" style="background:url('/common/img/page/sponsor/img-topvisual13.jpg') no-repeat">
        		<div class="dim"></div>    <!--dim-->
				<div class="contain" style="padding-top:35px">    <!--padding으로 컨텐츠 가운데에 맞게 조정-->
					<span class="icon1"></span>
					<strong class="txt-slogon style-add">아이티 ‘까르푸 센터’ 49 가정의<br />엄마와 아기 살리기</strong>
					태아·영아생존프로그램 일시후원<br />
					(10만 원 이상)
				</div>
			</div>

			<p class="txt-title mt20">신청자 정보</p>
			<fieldset class="frm-input mt10">
				<legend>신청자 정보 입력</legend>
				<div class="row-table">
					<div class="col-td" style="width:100%">
						<span class="row"><asp:TextBox ID="txtBrideName" runat="server" MaxLength="25" placeholder="신부 이름" style="width:100%"></asp:TextBox></span>
						<span class="row"><asp:TextBox ID="txtGroomName" runat="server" MaxLength="25" placeholder="신랑 이름" style="width:100%"></asp:TextBox></span>
						<span class="row">
							<input type="text" runat="server" id="txtWeddingDate" readonly="readonly" maxlength="10" placeholder="결혼기념일" style="width:100%;z-index: 900;position:relative" />
						</span>
						<span class="row">
							<asp:TextBox ID="txtPhone" runat="server" MaxLength="11" placeholder="휴대폰번호(-없이 입력)" CssClass="number_only" style="width:100%"></asp:TextBox>
						</span>
						<span class="row">
							<asp:TextBox ID="txtEmail" runat="server" MaxLength="100" placeholder="이메일" cssclass="input_type2 mb10" style="width:100%" ></asp:TextBox>
							</span>
                    
						<span class="row pos-relative2 del">
                    		<input type="text" runat="server" id="lb_file_path" class="input_type1 fl mr10" readonly="readonly" value="" placeholder="결혼사진" style="width:100%;"  />
							<a class="bt-type8 pos-btn" style="width:70px" href="#" id="btn_file_path">파일찾기</a>
							<a class="bt-type8 pos-btn-del" style="width:50px" href="#" id="btn_file_path_remove">삭제</a>
						</span>
					</div>
				</div>
			</fieldset>
        
			<div class="box-gray">
        		<strong>사진 첨부 시 주의사항</strong><br />
				<ul class="list-bullet2 lineheight1">
            		<li><span class="color3">기념증서 제작 및 발송은 약 2주정도 소요됩니다.</span></li>
					<li>세로 사진을 올려주세요. 세로 사진이 기념증서에 알맞게 제작됩니다.</li>
					<li>400KB 이상의 사진이 예쁘게 제작됩니다.  핸드폰 사진은 다소 해상도, 색상이 떨어질 수 있습니다.</li>
					<li>첨부파일 용량을 2MB이하로 해주세요. 첨부파일은 이미지파일(jpg, jpeg, gif, png)만 가능합니다.</li>
				</ul>
			</div>

		
			<input type="hidden" id="addr_domestic_zipcode" runat="server" />
			<input type="hidden" id="addr_domestic_addr1" runat="server" /> 
			<input type="hidden" id="addr_domestic_addr2" runat="server" />	
			<p class="txt-title">배송지 정보</p>
			<fieldset class="frm-input">
				<legend>배송지 정보 입력</legend>
				<div class="row-table">
					<div class="col-td" style="width:100%">
						<span class="row pos-relative2"><input type="text" value="" placeholder="주소" style="width:100%" id="post" readonly="readonly" /><a class="bt-type8 pos-btn" style="width:70px" href="#" ng-click="findAddr($event)">주소찾기</a></span>
						<!--<span class="row"><input type="text" value="" placeholder="상세주소" style="width:100%"  id="addr_1" readonly="readonly"/></span>-->
						
						<span id="addr_road" class="row fs13 mt15"></span>
						<span id="addr_jibun" class="row fs13"></span>

					</div>
				</div>
			</fieldset>

			<p class="txt-title">후원금액</p>
			<fieldset class="frm-input mt10">
				<legend>후원금액 정보 입력</legend>
				<div class="row-table">
					<div class="col-td" style="width:100%">
						<span class="row">
                    		<ul class="list-supportamount">
								<li><span class="box-amount btn_amount pay_amount" data-amount="100000">100,000원</span></li>
								<li><span class="box-amount btn_amount pay_amount" data-amount="300000">300,000원</span></li>
								<li><span class="box-amount btn_amount pay_amount" data-amount="500000">500,000원</span></li>
								<li><span class="box-amount btn_amount pay_amount" data-amount="1000000">1,000,000원</span></li>
								<li><span class="box-amount btn_amount pay_amount" data-amount="2000000">2,000,000원</span></li>
								<li><span class="box-amount btn_amount pay_amount" data-amount="3650000">3,650,000원</span></li>
							</ul>
						</span>
						<span class="row"><input type="text" value="" placeholder="직접입력(10만원이상 입력 가능)" style="width:100%" id="pay_amount_custom" class="pay_amount_custom number_only" maxlength="8"/></span>
						<p class="txt-error" id="pay_amount_custom_err" style="display:none;">금액입력은 10만원 이상 입력해 주세요.</p>   <!--에러메시지 경우에만 출력-->
                    
						<span class="row">
                    		<span class="total-amount">총 결제금액<em id="txt_amount">0원</em></span>
						</span>
                    
					</div>
				</div>
			</fieldset>
        
			
			<fieldset class="frm-input mt10">
				<legend>주민등록번호 입력</legend>
				<div class="row-table">
					<div class="col-td" style="width:100%">
						<span class="row" runat="server" ID="ph_notuser1" style="display:none"><input type="text" value="" runat="server" id="Text1" placeholder="이름" style="width:100%" /></span>
						<span class="row">
                    		<span class="checkbox-ui">
								<input type="checkbox" class="css-checkbox" id="p_receipt_pub_ok" runat="server" checked />
								<label for="p_receipt_pub_ok" class="css-label">국세청 연말정산 영수증 신청</label>
							</span>
						</span>
						<asp:PlaceHolder runat="server" ID="ph_no_jumin" Visible="false">
						<span class="row pos-relative2" id="func_name_check"><input type="text" class="number_only" value="" data-id="jumin1" runat="server" id="jumin1" maxlength="13" placeholder="주민등록번호" style="width:100%" /><a class="bt-type9 pos-btn" style="width:70px" href="#" id="btn_name_check">실명인증</a></span>
						</asp:PlaceHolder>
						<p class="txt-result" id="msg_name_check" style="display:none"><span class="guide_comment1"></span></p>
					</div>
				</div>
			</fieldset>
					

			<!--기존에 이미 실명인증했고, 영수증을 신청한 회원의 경우 국세청영수증 영역 대신 출력-->
			<asp:PlaceHolder runat="server" ID="ph_yes_jumin" Visible="false">
				<div class="msg-graybox mt10">
       				<strong>기존에 등록하신<br />
					<span class="color1">국세청 연말정산 영수증 신청 정보</span>가 있습니다.</strong><br />
					기존 정보에 연말정산 정보를 추가로 등록합니다. 
				</div>
			</asp:PlaceHolder>

			<asp:PlaceHolder runat="server" ID="ph_cert" Visible="false" >
				<p class="txt-title">본인인증</p>
				<p class="txt">온/오프라인 기존 후원정보 확인을 위해 본인 인증을 진행해 주세요.</p>
			
				<div class="self-account func_cert">
					<div class="link-account">
            			<a href="#" id="btn_cert_by_phone"><span>휴대폰 인증</span></a>
						<a href="#" id="btn_cert_by_ipin"><span>아이핀(i-PIN) 인증</span></a>
					</div>
					<div class="box-gray">
            			<ul class="list-bullet2">
                			<li>본인명의의 휴대폰으로만 본인인증이 가능합니다.<br />
							아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다. </li>
						</ul>
					</div>
				</div> 
				<p class="txt-result" id="msg_cert" style="display:none"><span class="guide_comment1">정보 확인이 완료되었습니다.</span></p>
			</asp:PlaceHolder>

        
			<p class="txt-title">결제방법</p>
			 <ul class="list-paymethod">
         		<li><span class="radio-ui">
					<input type="radio" id="payment_method_card"  runat="server" name="payment_method" class="css-radio" checked />
					<label for="payment_method_card" class="css-label">신용카드</label>
				</span></li>
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_cms" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_cms" class="css-label">실시간 계좌이체</label>
				</span></li>
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_kakao" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_kakao" class="css-label"><img src="/common/img/icon/kakaopay.png" alt="카카오 페이" class="txt-kakaopay" /></label>
				</span></li>
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_payco" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_payco" class="css-label"><img src="/common/img/icon/payco.png" alt="페이코" class="txt-payco" /></label>
				</span></li>
				 <!--
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_oversea" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_oversea" class="css-label">해외발급 카드</label>
				</span></li>
				 -->
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_phone" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_phone" class="css-label">휴대폰결제</label>
				</span></li>
			 </ul>

			<div class="box-gray">
				<ul class="list-bullet2">
					<li>후원금 결제 시 일부 카드사의 경우 결제 대행업체인 [NHN KCP 한국 사이버결제]로 승인 SMS가 전송됩니다.</li>
					<li>결제관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</li>
				</ul>
			</div>
        
			<div class="wrap-bt"><asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="bt-type6" style="width:100%" >신청 및 결제</asp:LinkButton></div>
			
		</div>
    
	<!--//-->
	</div>

</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">

	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form" />
	<uc:payco_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="payco_form" />
	<uc:kakaopay_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kakaopay_form" />
</asp:Content>