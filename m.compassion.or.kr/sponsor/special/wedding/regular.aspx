﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="regular.aspx.cs" Inherits="sponsor_special_wedding_regular" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_batch_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form_temporary" %>
<%--<%@ Register Src="/sponsor/pay/temporary/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form_temporary" %>--%>
<%--<%@ Register Src="/sponsor/pay/regular/kcp_batch_form.ascx" TagPrefix="uc" TagName="kcp_form" %>--%>
<%@ Register Src="/sponsor/pay/regular/cms_form.ascx" TagPrefix="uc" TagName="cms_form" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
	<script type="text/javascript" src="/sponsor/pay/regular/default.js"></script>
	<script type="text/javascript" src="/common/js/site/motive.js"></script>
	<script type="text/javascript" src="/sponsor/special/wedding/regular.js"></script>
	
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
		<script type="text/javascript">
		$(function () {

			$motive.init($("#motiveCode"), $("#motiveName"), $("#motive1"), $("#motive2"));

			$("#hd_title").val($("h2#title").text());

			var curPage = 1;
			$(".btn_next").click(function () {

				var page = parseInt($(this).data("page"));

				if (page == 2) {
					if (!$page.checkPage1()) return false;
				} else if (page == 3) {
					if (!$page.checkPage2()) return false;
				}

				curPage = page;

				$(".pn_step").hide();
				$(".pn_step.step" + curPage).show();
				if (curPage == 1) {
					$("h2#title").text($("#hd_title").val());
				} else if (curPage == 2) {
					$("h2#title").text("후원자 정보");
				} else if (curPage == 3) {
					$("h2#title").text("결제 정보");
				}

			});

		//	$(".pn_step.step1").hide();
		//	$(".pn_step.step2").show();
		//	$(".pn_step.step3").hide();

			$("h2#title").text($("#hd_title").val());
			if ($(".pn_step.step3").css("display") != "none") {
				$("h2#title").text("결제 정보");
				curPage = 3;
			}

			$("a.backgo").unbind("click");
			$("a.backgo").click(function () {

				if (curPage == 1) {
					location.href = "/sponsor/special/wedding/"
				} else {
					//	console.log($(".btn_next[data-page='" + (curPage) + "']"));
					$(".btn_next[data-page='" + (curPage - 1) + "']").trigger("click");
				}

				return false;
			});
		});

	</script>
	<style>
		.ui-datepicker select.ui-datepicker-year {width: 60%;}
	</style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" id="hd_title" runat="server" value="" />
	<a class="btn_next" data-page="1"></a>

	<input type="hidden" id="hd_amount" runat="server"  />
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />

	<input type="hidden" id="user_name" runat="server"  />
	<input type="hidden" id="gender" runat="server"  />
	<input type="hidden" id="hd_auth_domain" runat="server"  />
	<input type="hidden" id="hd_age" runat="server"  />

	<input type="hidden" runat="server" id="jumin" value="" />		
	<input type="hidden" runat="server" id="ci" value="" />		<!-- 본인인증 CI -->
	<input type="hidden" runat="server" id="di" value="" />		<!-- 본인인증 DI -->
	<input type="hidden" runat="server" id="cert_gb" value="" />		<!-- 본인인증 수단 -->
	
	<input type="hidden" runat="server" id="user_class" value="20세이상" />
	<input type="hidden" runat="server" id="parent_cert" value="" />
	<input type="hidden" runat="server" id="parent_name" value="" />
	<input type="hidden" runat="server" id="parent_juminId" value="" />
	<input type="hidden" runat="server" id="parent_mobile" value="" />
	<input type="hidden" runat="server" id="parent_email" value="" />

	<input type="hidden" runat="server" id="exist_account" value="" />
	<input type="hidden" runat="server" id="hdManageType" value="" />
	<input type="hidden" runat="server" id="hdState" value="" />
	<input type="hidden" runat="server" id="hfAddressType" value="" />
	<input type="hidden" runat="server" id="hdLocation" value="" />
	<input type="hidden" runat="server" id="hdOrganizationID" value="" />
	<input type="hidden" runat="server" id="hdSponsorId" value="" />
	<input type="hidden" runat="server" id="hdBirthDate" value="" />
	<input type="hidden" runat="server" id="hdChildMasterId" value="" />		<!-- 선택된 아동 아이디 -->
    <input type="hidden" runat="server" id="hdChildKey" value="" />		<!-- 선택된 아동 아이디 -->
    <input type="hidden" runat="server" id="hdChildGlobalId" value="" />		<!-- 선택된 아동 아이디 -->
	<input type="hidden" runat="server" id="zipcode" />
	<input type="hidden" runat="server" id="addr1" /> 
	<input type="hidden" runat="server" id="addr2" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
	<input type="hidden" runat="server" id="hdCI" value="" />	<!-- 기존 등록된 본인인증 CI -->
	<input type="hidden" runat="server" id="dmYN" value="Y" />
	<input type="hidden" runat="server" id="translationYN" value="N" />

	<asp:Panel runat="server" style="display:none">
		<input type="checkbox" runat="server" id="addr_overseas" />해외에 거주하고 있어요
		<asp:DropDownList runat="server" ID="ddlHouseCountry"></asp:DropDownList>
	</asp:Panel>

	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
	<!---->
    
		<div class="member-join sponsor-payment sponsor-special">
    		<div class="pn_step step1" runat="server" id="pn_step1">
				<div class="wrap-visualtop sectionsub-margin1" style="background:url('/common/img/page/sponsor/img-topvisual12.jpg') no-repeat">
        			<div class="dim"></div>    <!--dim-->
					<div class="contain" style="padding-top:40px">    <!--padding으로 컨텐츠 가운데에 맞게 조정-->
						<span class="icon2"></span>
						<strong class="txt-slogon style-add">결혼 기념일에 태어난 어린이<br />1:1 양육하기</strong>
						월 45,000원씩 정기후원
					</div>
				</div>

				<p class="txt-title mt20">신청자 정보</p>
				<fieldset class="frm-input mt10">
					<legend>신청자 정보 입력</legend>
					<div class="row-table">
						<div class="col-td" style="width:100%">
							<span class="row"><asp:TextBox ID="txtBrideName" runat="server" MaxLength="25" placeholder="신부이름" style="width:100%;"></asp:TextBox></span>
							<span class="row"><asp:TextBox ID="txtGroomName" runat="server" MaxLength="25" placeholder="신랑이름" style="width:100%;"></asp:TextBox></span>
                    
							<span class="row">
								<input type="text" runat="server" id="txtWeddingDate" readonly="readonly" maxlength="10" placeholder="결혼기념일" style="width:100%;z-index: 900;position:relative" />
								
							</span>
							<span class="row"><asp:TextBox ID="txtPhone" runat="server" MaxLength="11" placeholder="휴대폰번호(-없이 입력)" cssclass="number_only" style="width:100%"></asp:TextBox></span>
							<span class="row"><asp:TextBox ID="txtEmail" runat="server" MaxLength="1000" placeholder="이메일" style="width:100%;"></asp:TextBox></span>
							<span class="row pos-relative2 del">
								<input type="text" runat="server" id="lb_file_path" readonly="readonly" placeholder="결혼사진" style="width:100%"/>
								<a href="#" id="btn_file_path" class="bt-type8 pos-btn" style="width:70px">파일찾기</a>
								<a class="bt-type8 pos-btn-del" style="width:50px" href="#" id="btn_file_path_remove">삭제</a>
							</span>
						</div>
					</div>
				</fieldset>
        
				<div class="box-gray">
        			<strong>사진 첨부 시 주의사항</strong><br />
					<ul class="list-bullet2 lineheight1">
            			<li><span class="color3">기념증서 제작 및 발송은 약 2주정도 소요됩니다.</span></li>
						<li>세로 사진을 올려주세요. 세로 사진이 기념증서에 알맞게 제작됩니다.</li>
						<li>400KB 이상의 사진이 예쁘게 제작됩니다.  핸드폰 사진은 다소 해상도, 색상이 떨어질 수 있습니다.</li>
						<li>첨부파일 용량을 2MB이하로 해주세요. 첨부파일은 이미지파일(jpg, jpeg, gif, png)만 가능합니다.</li>
					</ul>
				</div>
  
				<p class="txt-title">배송지 정보</p>
				<fieldset class="frm-input">
					<legend>배송지 정보 입력</legend>
					<div class="row-table">
						<div class="col-td" style="width:100%">
							<span class="row pos-relative2">
							
								<input type="hidden" id="addr_domestic_zipcode" runat="server" />
								<input type="hidden" id="addr_domestic_addr1" runat="server" /> 
								<input type="hidden" id="addr_domestic_addr2" runat="server" />	

								<input type="text" id="addr_domestic_1" readonly="readonly" placeholder="주소" style="width:100%" />
								<a class="bt-type8 pos-btn" style="width:70px" ng-click="findAddr($event)">주소찾기</a>

							</span>
							<!--<span class="row"><input type="text" id="addr_1" placeholder="상세주소" style="width:100%" readonly="readonly"/></span>-->

							<span id="addr_road" class="row fs13 mt15"></span>
							<span id="addr_jibun" class="row fs13"></span>

						</div>
					</div>
				</fieldset>

				<div class="wrap-bt" id="pn_child" style="display:none">
					<a class="bt-type6" style="width:100%" href="#" ng-click="searchChild($event)">우리 결혼기념일에 태어난 어린이를 검색합니다.</a>
				</div>
				<!--어린이 검색 버튼.  터치 시 상단에 입력한 결혼기념일이 생일인 어린이중 대기일이 가장 긴 어린이를 선택하여 요약정보 하단에 출력 및 버튼 비노출-->

				<div class="married-searchchild" ng-if="child">
        			<p class="txt1">우리 결혼기념일에 태어난 어린이</p>
					<div class="sponsordetail-child">
						<span class="frame"><span class="photo" background-img="{{child.pic}}" style="background:no-repeat;"  data-default-image="/common/img/temp/temp13.jpg" ></span></span>    <!--어린이사진 inline style-->
						<em class="txt-type">1:1 어린이 양육</em>
						<p class="txt-name">{{child.namekr}}</p>
						<p class="txt-info"><span class="color2">국가 : </span> {{child.countryname}}</p>
						<p class="txt-info"><span class="color2">생일 : </span> {{child.birthdate | date:'yyyy.MM.dd'}} ({{child.age}}세)</p>
						<p class="txt-info"><span class="color2">성별 : </span> {{child.gender}}</p>
					</div>

					<div class="total-amount2">총 결제금액<em>45,000 원/월</em></div>
				</div>
				<!--//-->
			

				<div runat="server" ID="ph_cert_me">
				
					<!--국세청 영수증 신청 영역-->

					<asp:PlaceHolder runat="server" ID="ph_no_jumin" Visible="false">
					<fieldset class="frm-input mt10">
						<legend>주민등록번호 입력</legend>
						<div class="row-table">
							<div class="col-td" style="width:100%">
								<span class="row">
                    				<span class="checkbox-ui">
										<input type="checkbox" class="css-checkbox" id="p_receipt_pub_ok" runat="server" checked />
										<label for="p_receipt_pub_ok" class="css-label">국세청 연말정산 영수증 신청</label>
									</span>
								</span>
								<span class="row pos-relative2" id="func_name_check"><input type="text" class="number_only" value="" data-id="jumin1" runat="server" id="jumin1" maxlength="13" placeholder="주민등록번호" style="width:100%" /><a class="bt-type9 pos-btn" style="width:70px" href="#" id="btn_name_check">실명인증</a></span>
								<p class="txt-result" id="msg_name_check" style="display:none"><span class="guide_comment1"></span></p>
							</div>
						</div>
					</fieldset>
					</asp:PlaceHolder>

				
					<!--기존에 이미 실명인증했고, 영수증을 신청한 회원의 경우 국세청영수증 영역 대신 출력-->
					<asp:PlaceHolder runat="server" ID="ph_yes_jumin" Visible="false">
					<div class="msg-graybox mt10">
       					<strong>기존에 등록하신<br />
						<span class="color1">국세청 연말정산 영수증 신청 정보</span>가 있습니다.</strong><br />
						기존 정보에 연말정산 정보를 추가로 등록합니다. 
					</div>
					</asp:PlaceHolder>

					

					<!--본인인증 영역.(최초 1회만) 실명인증 시 비노출-->
					<asp:PlaceHolder runat="server" ID="ph_cert" Visible="false" >
					<p class="txt-title mt20">본인인증</p>
					<p class="txt1 align-left mt20" style="padding:0">온/오프라인 기존 후원정보 확인을 위해 본인 인증을 진행해 주세요.</p>
					<div class="self-account func_cert">
						<div class="link-account">
            				<a href="#" id="btn_cert_by_phone"><span>휴대폰 인증</span></a>
							<a href="#" id="btn_cert_by_ipin"><span>아이핀(i-PIN) 인증</span></a>
						</div>
						<div class="box-gray">
            				<ul class="list-bullet2">
                				<li>본인명의의 휴대폰으로만 본인인증이 가능합니다.<br />
								아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다. </li>
							</ul>
						</div>
					</div> 
					<p class="txt-result" id="msg_cert" style="display:none"><span class="guide_comment1">정보 확인이 완료되었습니다.</span></p>
					</asp:PlaceHolder>

					<!--만 19세 미만의 경우 보호자 동의영역 추가 출력-->
					<asp:PlaceHolder runat="server" ID="ph_parent_cert" Visible="false" >
					<p class="txt-title">보호자동의</p>
					<p class="txt1 align-left" style="padding:0">만 14세 미만은 법률에 의거하여 보호자(법적대리인)의 동의가 필요합니다.</p>
					<div class="self-account hide_parent_cert">
						<div class="link-account">
            				<a href="#" id="btn_parent_cert_by_phone"><span>휴대폰 인증</span></a>
							<a href="#" id="btn_parent_cert_by_ipin"><span>아이핀(i-PIN) 인증</span></a>
						</div>
					</div>
			
					<p class="txt-result" id="msg_parent_cert" style="display:none"><span class="guide_comment1">보호자 동의 인증이 완료되었습니다.</span></p>
					</asp:PlaceHolder>

				
				</div>

				<div class="wrap-bt"><a class="bt-type6 btn_next" data-page="2" style="width:100%" href="#"  >다음</a></div>

			</div>
		
			<!-- page 2 후원자정보 -->
			<div class="pn_step step2" style="display:none" runat="server" id="pn_step2">

				<p class="txt-title mt20">영문이름</p>
				<fieldset class="frm-input mt10">
					<legend>영문이름정보 입력</legend>
					<div class="row-table">
						<div class="col-td" style="width:100%">
							<span class="row"><input type="text" value="" placeholder="영문성" style="width:49%" class="fl"  maxlength="20" runat="server" id="last_name" /><input type="text" value="" placeholder="영문이름" style="width:49%" class="fr"  maxlength="20" runat="server"  id="first_name"/></span>
						</div>
					</div>
				</fieldset>
        
				<p class="txt-title">종교</p>
				<fieldset class="frm-input">
					<legend>종교 정보 입력</legend>
					<div class="row-table">
						<div class="col-td" style="width:100%">
							<!--기독교 선택시 교회명 영역 추가 로 나타남-->
							<span class="row"><asp:DropDownList style="width:100%" runat="server" id="religion">
								<asp:ListItem Text="기독교" Value="기독교"></asp:ListItem>
								<asp:ListItem Text="천주교" Value="천주교"></asp:ListItem>
								<asp:ListItem Text="불교" Value="불교"></asp:ListItem>
								<asp:ListItem Text="없음" Value="무교"></asp:ListItem>
							</asp:DropDownList></span>
							<span class="row pos-relative4" id="pn_church" runat="server"><a class="bt-type8 pos-btn" style="width:70px" href="#"  ng-click="findOrganization($event)">교회찾기</a><input type="text" value="" placeholder="교회명 직접입력" style="width:100%"  runat="server" id="church_name" maxlength="30"  /></span>
						</div>
					</div>
				</fieldset>
       
				
				<div class="hide_overseas">
				<p class="txt-title">후원정보 수신</p>
				<ul class="list-bullet2">
					<li>입력하신 주소로 후원과 관련한 우편물을 수신하시겠습니까?</li>
					<li>단, 후원과 관련하여 후원자님께 중요하게 안내되어야 하는 우편물은<br />수신 여부와 상관없이 발송됩니다.</li>
				</ul>
				<div class="tab-column-2 mt15">
					<a  href="#" class="dmYN" data-value="Y">예</a>
					<a href="#" class="dmYN" data-value="N">아니오</a>
				</div>
				</div>
        
				<p class="txt-title">어린이 편지 번역 여부</p>
				<div class="tab-column-2">
					<a href="#" class="translationYN" data-value="N" >영문</a>
					<a href="#" class="translationYN" data-value="Y" >한글</a>
				</div>
				 <ul class="list-bullet2 mt20">
        			<li>영문으로 선택하시면 어린이편지를 더 빨리 받으실 수 있습니다.</li>
					<li>어린이에게 편지를 보내실 때는 위 선택과 관계없이 한글 또는 영문으로 작성하실 수 있습니다.</li>
				</ul>
        
				<p class="txt-title">후원계기</p>
				<input type="hidden" runat="server" id="motiveCode" />
				<input type="hidden" runat="server" id="motiveName" />
				<fieldset class="frm-input">
					<legend>후원계기 정보 입력</legend>
					<div class="row-table">
						<div class="col-td" style="width:100%">
							<span class="row"><select style="width:100%" id="motive1" ></select></span>
							<span class="row"><select style="width:100%" id="motive2" >
								<option value="">선택하세요</option>
							</select></span>

							<span class="row" id="pn_motive2_etc" style="display:none"><input type="text" style="width:100%" id="motive2_etc" placeholder="기타 후원계기를 입력해주세요" maxlength="30" ></input></span>
						</div>
					</div>
				</fieldset>
        
            
				<div class="wrap-bt"><a class="bt-type6 btn_next" data-page="3" style="width:100%" href="#">다음</a></div>


			</div>
        
			<!-- page 3 결제정보 -->
			<div class="pn_step step3" style="display:none" runat="server" id="pn_step3">

				<table class="list-sponsordetail">
					<caption>후원유형,후원금액,매월 결제 금액</caption>
					<colgroup><col style="width:40%" /><col style="width:60%" /></colgroup>
					<tbody>
						<tr>
							<th>후원유형</th>
							<td>정기후원</td>
						</tr>
						<tr>
							<th>후원금액</th>
							<td>45,000 원</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th>매월 결제 금액</th>
							<td>45,000 원/월</td>
						</tr>
					</tfoot>
				</table>

				<!--기존 결제정보 결제 안내 메시지    기존에 정기결제로 등록한 결제정보가 있는 경우 결제 항목(신용카드 자동이체, CMS 자동이체) 메시지 출력-->
				<asp:PlaceHolder runat="server" ID="ph_exist_pay" Visible="false">
				<p class="txt-title mt20">결제방법</p>
				<div class="msg2-graybox sectionsub-margin1">
        			<p class="txt-msg"><asp:Literal ID="lb_exist_pay_msg1" runat="server"/></p>
					<strong><asp:Literal ID="lb_exist_pay_msg2" runat="server" Text = ""/></strong>
					
					<asp:PlaceHolder runat="server" ID="ph_exist_pay2" Visible="false">
					<asp:Literal runat="server" ID="lt_exist_pay2">아래 등록 버튼을 누르시면 첫 후원금은 즉시 결제되며,<br />
					다음달부터 기존 결제일에 선택하신 후원 금액정보가<br />
					추가되어 결제됩니다.</asp:Literal>								
						<input type="hidden" id="confirmMsg" value="기존 결제정보에 추가로 결제금액을 등록하시겠습니까?" />
					</asp:PlaceHolder>
				</div>
				</asp:PlaceHolder>

				<asp:PlaceHolder runat="server" ID="ph_new_pay" >
				<p class="txt-title mt20">결제방법  <a class="txt-link" href="/customer/faq/?k_word=후원금" target="_blank">결제방법보기</a></p>
				<fieldset class="frm-input">
					<legend>결제방법 정보 입력</legend>
					<div class="row-table">
						<div class="col-td" style="width:100%">
							<span class="row">
								<asp:PlaceHolder runat="server" ID="ph_payment_method_cms">
									<!--
								<span class="radio-ui" style="font-size:13px;margin-bottom:10px">
									<input type="radio" id="payment_method_cms" runat="server" name="payment_method" class="css-radio payment_method" data-type="cms" />
									<label for="payment_method_cms" class="css-label">CMS 자동이체</label>
								</span>&nbsp;&nbsp;
										-->
								</asp:PlaceHolder>
								<asp:PlaceHolder runat="server" ID="ph_payment_method_card">
								<span class="radio-ui" style="font-size:13px;margin-bottom:10px">
									<input type="radio" id="payment_method_card" runat="server" name="payment_method" class="css-radio payment_method" data-type="card" />
									<label for="payment_method_card" class="css-label">신용카드 자동이체</label>
								</span>&nbsp;&nbsp;
								</asp:PlaceHolder>
								<asp:PlaceHolder runat="server" ID="ph_payment_method_oversea">
									<!--
								<span class="radio-ui" style="font-size:13px;margin-bottom:10px">
									<input type="radio" id="payment_method_oversea" runat="server" name="payment_method" class="css-radio payment_method" data-type="oversea_card" />
									<label for="payment_method_oversea" class="css-label">해외발급 카드 즉시결제</label>
								</span>
									-->
								</asp:PlaceHolder>

								<asp:PlaceHolder runat="server" ID="ph_hidden_payment_method" Visible="false">
									<input type="radio" id="payment_method_giro" runat="server" />
									<input type="radio" id="payment_method_virtualaccount" runat="server" />
								</asp:PlaceHolder>

							</span>
						</div>
					</div>
				</fieldset>
				<ul class="list-bullet2 mt10">
        			<li>해외에서 발급하신 카드를 이용하시는 후원자님께서는 즉시결제를 선택해주세요.</li>
				</ul>
				</asp:PlaceHolder>

				<div class="payinfo" data-type="card" style="display:none">
				<p class="txt-title">카드주</p>
				<div class="tab-column-2">
					<a class="selected btn_card_owner" href="#">본인</a>
					<a class="btn_card_owner" href="#">타인</a>
				</div>

				<!--신용카드결제시 선택 시-->
				<p class="txt-title">결제일</p>
				<fieldset class="frm-input">
					<legend>결제일 정보 입력</legend>
					<div class="row-table">
						<div class="col-td" style="width:100%">
							<span class="row">
								<span class="radio-ui" style="font-size:13px">
									<input type="radio" id="paymentDay5" value="05" name="paymentDay" runat="server" class="css-radio paymentDay" checked />
									<label for="paymentDay5" class="css-label">매월 5일</label>
								</span>&nbsp;&nbsp;
								<span class="radio-ui" style="font-size:13px">
									<input type="radio" id="paymentDay15" value="15" name="paymentDay" runat="server" class="css-radio paymentDay" />
									<label for="paymentDay15" class="css-label">매월 15일</label>
								</span>&nbsp;&nbsp;
								<span class="radio-ui" style="font-size:13px">
									<input type="radio" id="paymentDay25" value="25" name="paymentDay" runat="server" class="css-radio paymentDay" />
									<label for="paymentDay25" class="css-label">매월 25일</label>
								</span>
							</span>
						</div>
					</div>
				</fieldset>
				<ul class="list-bullet2 mt20">
        			<li class="color1">첫 후원금은 해당 카드에서 바로 결제 후 다음달부터 선택하신 이체일에 자동 결제됩니다.</li>
					<li>
						재결제일 안내<br />
						자동이체일이 5일, 15일인 경우 : 매월 20일, 27일<br />
						자동이체일이 25일인 경우 : 매월 27일 
					</li>
				</ul>

				<div class="payinfo" data-type="cms" style="display:none">
					<!--cms 자동이체 선택 시-->
					<input type="radio" id="cms_owner_type1" value="본인" name="cms_owner_type" runat="server" checked="true" style="display:none"/>
					<input type="radio" id="cms_owner_type2" value="타인" name="cms_owner_type" runat="server" style="display:none" />

					<p class="txt-title">예금주와의 관계</p>
					<div class="tab-column-2">
						<a class="selected btn_cms_owner" href="#" data-value="본인">본인</a>
						<a href="#" class="btn_cms_owner" data-value="타인">타인</a>
					</div>

					<input type="radio" id="cms_account_type1" name="cms_account_type" data-value="개인계좌" runat="server" class="cms_account_type" checked  style="display:none"/>
					<input type="radio" id="cms_account_type2" name="cms_account_type" data-value="기업계좌" runat="server" class="cms_account_type" style="display:none"/>
					<p class="txt-title">계좌정보</p>
					<div class="tab-column-2">
						<a class="selected btn_cms_account_type" href="#" data-value="개인계좌">개인계좌</a>
						<a href="#" class="btn_cms_account_type" data-value="기업계좌">기업계좌</a>
					</div>
        
					<fieldset class="frm-input mt10">
						<legend>계좌정보 입력</legend>
						<div class="row-table">
							<div class="col-td" style="width:100%">
								<span class="row"><input type="text" value="" placeholder="예금주명" style="width:100%"  id="cms_owner" runat="server"  maxlength="10" /></span>
								<span class="row cms_account_type1"><input type="text" value="" placeholder="예금주 생년월일(6자리)" style="width:100%" class="number_only" id="cms_birth" runat="server" maxlength="6" /></span>
								<span class="row cms_account_type2"><input type="text" value="" placeholder="사업자번호(-없이 입력)" style="width:100%" class="number_only" id="cms_soc" runat="server" maxlength="10"/></span>
								<span class="row">
									<input type="hidden" runat="server" id="hd_cms_bank" value="" />
									<input type="hidden" runat="server" id="hd_cms_bank_name" value="" />
                    				<ul class="list-bankinfo">
                        				<li><span class="radio-ui">
											<input type="radio" id="bank_1" data-code="004" name="cms_bank" class="cms_bank css-radio" checked  />
											<label for="bank_1" class="css-label">국민은행</label>
										</span></li>
										<li><span class="radio-ui">
											<input type="radio" id="bank_2" data-code="020" name="cms_bank" class="cms_bank css-radio" />
											<label for="bank_2" class="css-label">우리은행</label>
										</span></li>
										<li><span class="radio-ui">
											<input type="radio" id="bank_3" data-code="012" name="cms_bank" class="cms_bank css-radio" />
											<label for="bank_3" class="css-label">농협회원조합</label>
										</span></li>
										<li><span class="radio-ui">
											<input type="radio" id="bank_4" data-code="011" name="cms_bank" class="cms_bank css-radio" />
											<label for="bank_4" class="css-label">농협중앙회</label>
										</span></li>
										<li><span class="radio-ui">
											<input type="radio" id="bank_5" data-code="088" name="cms_bank" class="cms_bank css-radio" />
											<label for="bank_5" class="css-label">신한은행</label>
										</span></li>
										<li><span class="radio-ui">
											<input type="radio" id="bank_6" data-code="081" name="cms_bank" class="cms_bank css-radio" />
											<label for="bank_6" class="css-label">KEB하나은행</label>
										</span></li>
										<li><span class="radio-ui">
											<input type="radio" id="bank_7" data-code="003" name="cms_bank" class="cms_bank css-radio" />
											<label for="bank_7" class="css-label">기업은행</label>
										</span></li>
									</ul>
								</span>
								<span class="row"><asp:DropDownList runat="server" ID="cms_bank_etc" style="width:100%" >
													<asp:ListItem Text="그 외" Value=""></asp:ListItem>
												</asp:DropDownList>
								</span>
                    
								<input type="hidden" runat="server" id="is_checked_account" value="N" />
								<span class="row pos-relative2"><input type="text" value="" placeholder="계좌번호(-없이 입력)" style="width:100%" class="number_only" maxlength="15" id="cms_account" runat="server"  />
									<a class="bt-type8 pos-btn" style="width:70px" href="#" id="btn_cms_check_account">계좌확인</a>
								</span>
							</div>
						</div>
					</fieldset>
					<ul class="list-bullet2 mt20">
        				<li>기업은행 평생계좌는 반드시 모계좌로 등록해주세요.</li>
						<li>농협계좌로 자동이체를 신청하시는 경우 통장을 잘 확인하여 은행명을 지정해 주세요.<br />
							&nbsp;-농협대표,농협중앙회,농협은행 : 농협은행<br />
							&nbsp;-단위농협,농협회원조합 : 농협회원조합
						</li>
					</ul>

					<p class="txt-title">이체일</p>
					<fieldset class="frm-input">
						<legend>이체일 정보 입력</legend>
						<div class="row-table">
							<div class="col-td" style="width:100%">
								<span class="row">
									<span class="radio-ui" style="font-size:13px">
										<input type="radio" id="cmsday_5" value="05" name="cmsday" runat="server" class="css-radio" checked />
										<label for="cmsday_5" class="css-label">매월 5일</label>
									</span>&nbsp;&nbsp;
									<span class="radio-ui" style="font-size:13px">
										<input type="radio" id="cmsday_15" value="05" name="cmsday" runat="server" class="css-radio" />
										<label for="cmsday_15" class="css-label">매월 5일</label>
									</span>&nbsp;&nbsp;
									<span class="radio-ui" style="font-size:13px">
										<input type="radio" id="cmsday_25" value="05" name="cmsday" runat="server" class="css-radio" />
										<label for="cmsday_25" class="css-label">매월 25일</label>
									</span>
								</span>
							</div>
						</div>
					</fieldset>
					<ul class="list-bullet2 mt20">
        				<li class="color1">첫 후원금은 해당 카드에서 바로 결제 후 다음달부터 선택하신 이체일에 자동 결제됩니다.</li>
						<li>
							재출금일 안내<br />
							자동이체일이 5일, 15일인 경우 : 매월 20일, 27일<br />
							자동이체일이 25일인 경우 : 매월 27일
						</li>
					</ul>
        
        
					<p class="txt-title">신청동의</p>
					<div class="cms-agree">
        				<span class="checkbox-ui">
							<input type="checkbox" class="css-checkbox"  id="agree_cms">
							<label for="agree_cms" class="css-label">한국컴패션에 CMS 자동이체를 이용하여 후원금을 납부하는 것에  동의합니다.</label>
						</span>
						<span class="more-loading" ng-click="showCMSAgree($event)"><span>전문 보기</span></span>
						
					</div>
					<!--//cms 자동이체 선택 시-->
				</div>

				<div class="payinfo" data-type="oversea_card" style="display:none">
					<!--해외발급카드 결제 선택 시-->
					<p class="txt-title">결제금액</p>
					<div class="foreign-payment">
        				<input type="text" value="12" runat="server" id="oversea_pay_month"  placeholder="개월 수 입력" class="number_only"  maxlength="3" style="width:40px" /> &nbsp;개월
						<em id="oversea_pay_total">0</em>
					</div>
					<div class="box-gray mt20" class="payinfo" data-type="oversea_card" style="display:none">
						<strong><span class="color1">결제 전 꼭 확인해주세요!</span></strong><br />
						<ul class="list-bullet2 mt10">
							<li>결제관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</li>
							<li>추가 금액은 마이컴패션>후원관리에서 결제하실 수 있습니다.</li>
						</ul>
					</div>
					<!--//해외발급카드 결제 선택 시-->
				</div>
        
				<!--신용카드,cms 안내문구-->
				<div class="box-gray mt20" class="payinfo" data-type="card" style="display:none">
        			<strong><span class="color1">결제 전 꼭 확인해주세요!</span></strong><br />
					<ul class="list-bullet2 mt10">
						<li>후원금 결제 시 일부 카드사의 경우 결제 대행업체인 [NHN KCP 한국 사이버결제]로 승인 SMS가 전송됩니다.</li>
						<li>결제관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</li>
					</ul>
				</div>

				<div class="box-gray mt20" class="payinfo" data-type="cms" style="display:none">
        			<strong><span class="color1">결제 전 꼭 확인해주세요!</span></strong><br />
					<ul class="list-bullet2 mt10">
						<li>후원신청 후 해당 은행에서 자동이체 등록승인 문자가 중복 안내될 수 있습니다.</li>
						<li>한국컴패션은 금융결제원과 효성FMS 시스템을 사용하고 있습니다.</li>
						<li>결제관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</li>
					</ul>
				</div>
				<!--//신용카드,cms 안내문구-->

			</div>
        
			<div class="wrap-bt"><asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="bt-type6" style="width:100%" >결제</asp:LinkButton></div>

        
		</div>
    
	<!--//-->
	</div>


</asp:Content>


<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form" />
	<uc:kcp_form_temporary runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form_temporary" />	
	<uc:cms_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="cms_form" />
</asp:Content>