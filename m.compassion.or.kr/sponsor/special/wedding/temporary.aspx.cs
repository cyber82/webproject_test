﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_special_wedding_temporary : SponsorPayBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	public override bool RequireLogin {
		get {
			return true;
		}
	}


	protected override void OnBeforePostBack() {
        /*         이벤트 정보 등록  -- 20170327 이정길       */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;
        //Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        //Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));

        //김태형 2017.06.01
        if (Request.Url.Host.IndexOf('.') != -1)
        {
            Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
            Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        }
        else
        {
            Response.Cookies["e_id"].Domain = "localhost";
            Response.Cookies["e_src"].Domain = "localhost";
        }


        if (AppSession.HasCookie(this.Context) && Request["app_ex"].EmptyIfNull() == "") {
			var app = AppSession.GetCookie(this.Context);
			if (app.device == "iphone") {
				Response.Redirect(Request.RawUrl + "?app_ex=1");
			}
		}

		upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_cpswedding);
		// 첫생일 첫나눔 정보

		var payInfo = new PayItemSession.Entity() { type = PayItemSession.Entity.enumType.CSP_WEDDING, campaignId = "20150128114815082", frequency = "일시", group = "CSP", codeId = "CSPF", codeName = "CSP 결혼첫나눔", amount = 0 };
		PayItemSession.SetCookie(this.Context, payInfo);

		// 결제정보를 디비에 저장하고 이후 결제프로세스는 디비의 값을 참조한다.
		var data = new PayItemSession.Store().Create(payInfo);
		if(data == null) {
			base.AlertWithJavascript("후원정보 임시저장에 실패했습니다.", "goBack()");
			return;
		}
		this.ViewState["payItem"] = data.ToJson();


		//this.ViewState["payInfo"] = payInfo.ToJson();

		UserInfo sess = new UserInfo();
		
		base.OnBeforePostBackTemporary(payInfo);
		
		// 국외주소 회원은 신청제한
		if(hdLocation.Value != "국내") {
			base.AlertWithJavascript("국외주소지 회원은 해당 서비스를 이용하실 수 없습니다." , "goBack()");
			return;
		}

		if(sess.GenderCode == "C") {
			base.AlertWithJavascript("첫 생일 첫 나눔과 결혼 첫 나눔 후원은 개인회원이신 경우 신청하실 수 있습니다.", "goBack()");
			return;
		}

		if(sess.GenderCode == "M") {
			txtGroomName.Text = sess.UserName;
		} else if(sess.GenderCode == "F") {
			txtBrideName.Text = sess.UserName;
		}

		var addr_result = new SponsorAction().GetAddress();
		if(!addr_result.success) {
			base.AlertWithJavascript(addr_result.message, "goBack()");
			return;
		}
		
		SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;
		addr_domestic_zipcode.Value = addr_data.Zipcode;
		addr_domestic_addr1.Value = addr_data.Addr1;
		addr_domestic_addr2.Value = addr_data.Addr2;
		hfAddressType.Value = addr_data.AddressType;
		dspAddrDoro.Value = addr_data.DspAddrDoro;
		dspAddrJibun.Value = addr_data.DspAddrJibun;

		var comm_result = new SponsorAction().GetCommunications();
		if(!comm_result.success) {
			base.AlertWithJavascript(comm_result.message, "goBack()");
			return;
		}

		SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
		txtEmail.Text = comm_data.Email;
		txtPhone.Text = comm_data.Mobile;
		

	}
	

	// 결제
	protected void btn_submit_Click( object sender, EventArgs e ) {

		kakaopay_form.Hide();
		kcp_form.Hide();
		payco_form.Hide();

		if(base.IsRefresh) {
			return;
		}

		if(!base.UpdateTemporaryUserInfo()) {
			return;
		}

		var sess = new UserInfo();
		var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

		var action = new CommitmentAction();
		
		// 최초 등록하는 주소인경우 기본정보(주소) 업데이트
		new SponsorAction().UpdateAddress(false, hfAddressType.Value, sess.LocationType, "한국", addr_domestic_zipcode.Value, addr_domestic_addr1.Value, addr_domestic_addr2.Value);

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var result = dao.INS_tCspWedding("일시", sess.SponsorID, user_name.Value, "", Convert.ToDecimal(amount.Value), txtGroomName.Text, txtBrideName.Text, txtWeddingDate.Value,
            //    txtEmail.Text, txtPhone.Text, file_path.Value, addr_domestic_zipcode.Value, addr_domestic_addr1.Value, addr_domestic_addr2.Value,
            //    'N').First();

            Object[] op1 = new Object[] { "Gubun", "SponsorID", "SponsorName", "childMasterID", "PayAmount", "GroomName", "BrideName", "WeddingDate", "Email", "Tel", "WeddingImage", "Zip", "Address1", "Address2", "PaymentYN" };
            Object[] op2 = new Object[] { "일시", sess.SponsorID, user_name.Value, "", Convert.ToDecimal(amount.Value), txtGroomName.Text, txtBrideName.Text, txtWeddingDate.Value, txtEmail.Text, txtPhone.Text, file_path.Value, addr_domestic_zipcode.Value, addr_domestic_addr1.Value, addr_domestic_addr2.Value, 'N' };
            var result = www6.selectSP("INS_tCspWedding", op1, op2).DataTableToList<INS_tCspWeddingResult>().First();

            if (result.Result == 'N')
            {
                base.AlertWithJavascript(result.Result_String);
                return;
            }

            payInfo.relation_key = result.idx.Value;
            payInfo.amount = Convert.ToInt32(amount.Value);
            this.ViewState["good_mny"] = payInfo.amount.ToString();
            if (payment_method_card.Checked)
            {
                payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card;
            }
            else if (payment_method_cms.Checked)
            {
                payInfo.payMethod = PayItemSession.Entity.enumPayMethod.cms;
            }
            else if (payment_method_oversea.Checked)
            {
                payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card_oversea;
            }
            else if (payment_method_phone.Checked)
            {
                payInfo.payMethod = PayItemSession.Entity.enumPayMethod.phone;
            }
            else if (payment_method_payco.Checked)
            {
                payInfo.payMethod = PayItemSession.Entity.enumPayMethod.payco;
            }
            else if (payment_method_kakao.Checked)
            {
                payInfo.payMethod = PayItemSession.Entity.enumPayMethod.kakao;
            }


            base.SetTemporaryPaymentInfo();

            var orderId = PayItemSession.Store.GetNewOrderID();
            payInfo.extra = new Dictionary<string, object>() {
                {"jumin" , ViewState["jumin"].ToString() } ,
                {"ci" , ViewState["ci"].ToString() },
                {"di" , ViewState["di"].ToString() },
                {"motiveCode" , "" },
                {"motiveName" , "" },
                {"overseas_card" , ViewState["overseas_card"].ToString() },

                //[jun.heo] 2017-12 : 결제 모듈 통합
                {"successUrl" , "/pay/complete_wedding/" + orderId } ,
                {"failUrl" , "/pay/fail_sponsor/?r=" + HttpUtility.UrlEncode("/sponsor/special/wedding/temporary/") }
    //            {"successUrl" , "/sponsor/special/wedding/complete/" + orderId } ,
				//{"failUrl" , "/sponsor/pay/fail/?r=" + HttpUtility.UrlEncode("/sponsor/special/wedding/temporary/") }
			}.ToJson().Encrypt();

            payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo, orderId);
            //payItem.data = payInfo.ToJson();
            this.ViewState["payItem"] = payItem.ToJson();

        }

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "Temporary";  // 결제 페이지 종류 


        if (payment_method_payco.Checked) {
			payco_form.Show(this.ViewState);
		} else if(payment_method_kakao.Checked) {
			kakaopay_form.Show(this.ViewState);
		} else {
			kcp_form.Show(this.ViewState);
		}

	}
}