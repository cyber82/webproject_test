﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_special_wedding_regular : SponsorPayBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
        /*         이벤트 정보 등록  -- 20170327 이정길       */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;
        //Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        //Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));

        //김태형 2017.06.01
        if (Request.Url.Host.IndexOf('.') != -1)
        {
            Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
            Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        }
        else
        {
            Response.Cookies["e_id"].Domain = "localhost";
            Response.Cookies["e_src"].Domain = "localhost";
        }


        if (AppSession.HasCookie(this.Context) && Request["app_ex"].EmptyIfNull() == "") {
			var app = AppSession.GetCookie(this.Context);
			if (app.device == "iphone") {
				Response.Redirect(Request.RawUrl + "?app_ex=1");
			}
		}

		var payInfo = new PayItemSession.Entity() { type = PayItemSession.Entity.enumType.CSP_WEDDING, campaignId = "20150128114815082", frequency = "정기", group = "CDSP", codeId = "DS", codeName = "CSP 결혼첫나눔", amount = 45000 };
		PayItemSession.SetCookie(this.Context, payInfo);

		hd_amount.Value = payInfo.amount.ToString();
		// 결제정보를 디비에 저장하고 이후 결제프로세스는 디비의 값을 참조한다.
		var data = new PayItemSession.Store().Create(payInfo);
		if(data == null) {
			base.AlertWithJavascript("후원정보 임시저장에 실패했습니다.", "goBack()");
			return;
		}
		this.ViewState["payItem"] = data.ToJson();

		upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_cpswedding);
		// 첫생일 첫나눔 정보

		UserInfo sess = new UserInfo();
		
		base.OnBeforePostBackRegular(payInfo);

		// 국외주소 회원은 신청제한
		if(hdLocation.Value != "국내") {
			base.AlertWithJavascript("국외주소지 회원은 해당 서비스를 이용하실 수 없습니다.", "goBack()");
			return;
		}

		if(sess.GenderCode == "C") {
			base.AlertWithJavascript("첫 생일 첫 나눔과 결혼 첫 나눔 후원은 개인회원이신 경우 신청하실 수 있습니다.", "goBack()");
			return;
		}

		if(string.IsNullOrEmpty(sess.Birth)) {
			base.AlertWithJavascript("나이를 확인할 수 없습니다.", "goBack()");
			return;
		}

		if(Convert.ToDateTime(sess.Birth).GetAge() < 18) {
			base.AlertWithJavascript("결혼 첫 나눔 신청은 만18세 성인 이상의 국내후원자만 가능합니다.", "goBack()");
			return;
		}

		if(sess.GenderCode == "M") {
			txtGroomName.Text = sess.UserName;
		} else if(sess.GenderCode == "F") {
			txtBrideName.Text = sess.UserName;
		}

		var comm_result = new SponsorAction().GetCommunications();
		if(!comm_result.success) {
			base.AlertWithJavascript(comm_result.message, "goBack()");
			return;
		}

		
		SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
		txtEmail.Text = comm_data.Email;
		txtPhone.Text = comm_data.Mobile;

		cms_owner.Value = user_name.Value;
		if(!string.IsNullOrEmpty(hdBirthDate.Value))
			cms_birth.Value = hdBirthDate.Value.Substring(2);
	}


	// 결제
	protected void btn_submit_Click( object sender, EventArgs e ) {
		
		if(base.IsRefresh) {
			return;
		}

		if(!base.UpdateRegularUserInfo())
			return;

		pn_step1.Style["display"] = pn_step2.Style["display"] = "none";
		pn_step3.Style["display"] = "block";

		if(string.IsNullOrEmpty(hdChildMasterId.Value)) {
			base.AlertWithJavascript("후원아동을 검색해주세요");
			return;
		}

		var actionResult = this.RegistWedding();
		if(!actionResult.success) {
			return;
		}
		
		var payItem = (pay_item_session)actionResult.data;
		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();
		
		if(payment_method_card.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card;
		} else if(payment_method_cms.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.cms;
		} else if(payment_method_oversea.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card_oversea;
		} else if(payment_method_giro.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.jiro;
		} else if(payment_method_virtualaccount.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.virtualAccount;
		}

		JsonWriter result = this.DoPayCDSP(payItem);
	
		if(result.success) {

			if(payment_method_card.Checked) {

			} else if(payment_method_oversea.Checked) {

				this.ViewState["campaignId"] = "";
				this.ViewState["jumin"] = "";
				this.ViewState["ci"] = "";
				this.ViewState["di"] = "";
				this.ViewState["good_mny"] = payInfo.amount.ToString();
				this.ViewState["pay_method"] = "100000000000";
				this.ViewState["overseas_card"] = "Y";
				this.ViewState["used_card_YN"] = "Y";
				this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_oversea"];
				this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_web_card_Foreign"];

			} else if(payment_method_cms.Checked) {

				actionResult = new PaymentAction().GetBankAccount();
				if(!actionResult.success) {
					base.AlertWithJavascript(actionResult.message);
				} else {

					this.ViewState["relation"] = cms_owner_type1.Checked ? cms_owner_type1.Value : cms_owner_type2.Value;
					this.ViewState["cms_owner"] = cms_owner.Value;
					this.ViewState["cms_account"] = cms_account.Value;
					this.ViewState["birth"] = cms_account_type1.Checked ? cms_birth.Value : cms_soc.Value;
					this.ViewState["bank_name"] = hd_cms_bank_name.Value;
					this.ViewState["bank_code"] = hd_cms_bank.Value;
					this.ViewState["cmsday"] = cmsday_5.Checked ? cmsday_5.Value : (cmsday_15.Checked ? cmsday_15.Value : cmsday_25.Value);

					this.ViewState["src"] = string.Format("예금주와의 관계 : {0} /\n예금주 : {1} /\n예금주 생년월일 : {2} /\n은행명 : {3} /\n"
							   + "은행번호 : {4} /\n계좌번호 : {5} /\n이체일 : {6} /\n"
							   , this.ViewState["relation"]
							   , this.ViewState["cms_owner"]
							   , this.ViewState["birth"]
							   , this.ViewState["bank_name"]
							   , this.ViewState["bank_code"]
							   , this.ViewState["cms_account"]
							   , this.ViewState["cmsday"]
							   );

				}
			}


			var orderId = PayItemSession.Store.GetNewOrderID();
			payInfo.extra = new Dictionary<string, object>() {
				{"jumin" , ViewState["jumin"].ToString() } ,
				{"ci" , ViewState["ci"].ToString() },
				{"di" , ViewState["di"].ToString() },
				{"motiveCode" , ViewState["motiveCode"].ToString() },
				{"motiveName" , ViewState["motiveName"].ToString() },
				{"overseas_card" , ViewState["overseas_card"].ToString() },

                //[jun.heo] 2017-12 : 결제 모듈 통합
				{"successUrl" , "/pay/complete_wedding/" + orderId } ,
				{"failUrl" , "/pay/fail_sponsor/?r=" + HttpUtility.UrlEncode("/sponsor/special/wedding/regular/") }
                //{"successUrl" , "/sponsor/special/wedding/complete/" + orderId } ,
                //{"failUrl" , "/sponsor/pay/fail/?r=" + HttpUtility.UrlEncode("/sponsor/special/wedding/regular/") }
            }.ToJson().Encrypt();

			payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo, orderId);
			//payItem.data = payInfo.ToJson();
			this.ViewState["payItem"] = payItem.ToJson();

            //[jun.heo] 2017-12 : 결제 모듈 통합
            this.ViewState["payPageType"] = "Regular";  // 결제 페이지 종류 

            kcp_form.Visible = cms_form.Visible = kcp_form_temporary.Visible = false;

			if(payment_method_card.Checked) {
				kcp_form.Visible = true;
				kcp_form.Show(this.ViewState);
			} else if(payment_method_oversea.Checked) {
				kcp_form_temporary.Visible = true;
				kcp_form_temporary.Show(this.ViewState);
			} else if(payment_method_cms.Checked) {
				cms_form.Visible = true;
				cms_form.Show(this.ViewState);

			}

		}


	}

	JsonWriter RegistWedding()
    {
		JsonWriter result = new JsonWriter() { success = false };
		UserInfo sess = new UserInfo();

		var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<tCspWedding>("SponsorID", sess.SponsorID, "PaymentYN", "N", "CancelYN", "N");
            //if (dao.tCspWedding.Any(p => p.SponsorID == sess.SponsorID && p.PaymentYN == 'Y' && p.CancelYN == 'N'))
            if(exist.Any())
            {
                base.AlertWithJavascript(string.Format("이미 {0}에 후원중 이십니다.", payInfo.codeName));
                return result;
            }

            //var actionResult = dao.INS_tCspWedding("정기", sess.SponsorID, user_name.Value, hdChildMasterId.Value, Convert.ToDecimal(payInfo.amount), txtGroomName.Text, txtBrideName.Text, txtWeddingDate.Value,
            //    txtEmail.Text, txtPhone.Text, file_path.Value, zipcode.Value, addr1.Value, addr2.Value,
            //    'N').First();
            Object[] op1 = new Object[] { "Gubun", "SponsorID", "SponsorName", "childMasterID", "PayAmount", "GroomName", "BrideName", "WeddingDate", "Email", "Tel", "WeddingImage", "Zip", "Address1", "Address2", "PaymentYN" };
            Object[] op2 = new Object[] { "정기", sess.SponsorID, user_name.Value, hdChildMasterId.Value, Convert.ToDecimal(payInfo.amount), txtGroomName.Text, txtBrideName.Text, txtWeddingDate.Value, txtEmail.Text, txtPhone.Text, file_path.Value, zipcode.Value, addr1.Value, addr2.Value, 'N' };
            var actionResult = www6.selectSP("INS_tCspWedding", op1, op2).DataTableToList<INS_tCspWeddingResult>().First();

            if (actionResult.Result == 'N')
            {
                base.AlertWithJavascript(actionResult.Result_String);
                return result;
            }


            payInfo.relation_key = actionResult.idx.Value;
            payInfo.childMasterId = hdChildMasterId.Value;
            //[이종진]추가
            payInfo.childKey = hdChildKey.Value;
            payInfo.childGlobalId = hdChildGlobalId.Value;

            if (payment_method_oversea.Checked)
            {
                payInfo.amount = Convert.ToInt32(oversea_pay_month.Value) * Convert.ToInt32(hd_amount.Value);
            }
            else
            {
                payInfo.amount = Convert.ToInt32(hd_amount.Value);
            }
            this.ViewState["good_mny"] = payInfo.amount.ToString();

            payInfo.userId = sess.UserId;
            payInfo.sponsorName = sess.UserName;
            payInfo.sponsorId = sess.SponsorID;

            /*
			var data = new PayItemSession.Store().Create(payInfo);
			if(data == null) {
				base.AlertWithJavascript("후원정보 임시저장에 실패했습니다.", "goBack()");
				return result;
			}
			this.ViewState["payItem"] = data.ToJson();
			
			result.data = data;
			*/

            payItem.data = payInfo.ToJson();
            result.data = payItem;
        }

		result.success = true;
		return result;
	}

	JsonWriter DoPayCDSP(pay_item_session payItem ) {
		var sess = new UserInfo();
		var action = new CommitmentAction();
		JsonWriter result = new JsonWriter();

		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

		/*
		이미 자동결제 정보가 있는 경우 추가 승인번호를 요청하지 않는다.
		*/

		if(exist_account.Value == "Y") {
			
			result = action.DoCDSP(hdChildMasterId.Value, payInfo.childKey, payInfo.campaignId , payInfo.codeName , motiveCode.Value, motiveName.Value);
			
			if(!result.success) {
				base.AlertWithJavascript(result.message);
				return result;
			}

			var commitmentId = result.data.ToString();

            //this.ViewState["returnUrl"] = "/sponsor/special/wedding/complete/" + payItem.orderId;
            this.ViewState["returnUrl"] = "/pay/complete_wedding/" + payItem.orderId;
            if (this.Pay(payItem, result.data.ToString())) {
				Response.Redirect(this.ViewState["returnUrl"].ToString());
			} else {

				//action.DeleteCommitment(commitmentId);
				result.success = false;
				result.message = "결제실패";
			//	base.AlertWithJavascript(result.message);
				return result;
			}

			

		} else {
			
			action.ValidateCDSP(hdChildMasterId.Value, payInfo.campaignId, payInfo.codeName, sess.SponsorID, ref result);
			
			if(!result.success) {
				base.AlertWithJavascript(result.message);
				return result;
			}
			
		}

		return result;
	}

	// 정기후원 즉시결제
	bool Pay( pay_item_session payItem, string commitmentId ) {

		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

		// 신용카드, CMS 인 경우 즉시결제 처리
		var actionResult = new BatchPay().Pay(commitmentId, payItem.orderId, payInfo.amount, payInfo.group);
		if(actionResult.success)
			return true;
		else {
			base.AlertWithJavascript(
				string.Format(@"정기 후원 신청이 완료되었으나,{0}첫 후원금 결제가 정상적으로 처리되지 못했습니다. 1~3일 내에 등록하신 연락처로 전화 드려 후원금 납부를 도와 드리도록 하겠습니다.",
				string.IsNullOrEmpty(actionResult.message) ? "" : string.Format("{0}의 사유로 ", actionResult.message)),
				string.Format("location.href='{0}'", this.ViewState["returnUrl"].ToString()));
			//base.AlertWithJavascript(actionResult.message);
			return false;
		}

	}

}