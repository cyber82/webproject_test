﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="sponsor_special_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
    
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/sponsor/special/default.js"></script>
	<script src="/common/js/util/swiper.min.js"></script>
	<link href="/common/css/swiper.css" rel="stylesheet" />
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<div class="wrap-sectionsub">
	<!---->
    
		<div class="sponsor-special">
    	
			<div class="wrap-slider sectionsub-margin1 swiper-container">
				<div class="swiper-wrapper" id="layer_banner" runat="server">
					
					 <asp:Repeater runat="server" ID="repeater" OnItemDataBound="repeater_ItemDataBound">
                        <ItemTemplate>
							<div class="swiper-slide bg-img" style="background:url('<%# Eval("sf_image_m").ToString().WithFileServerHost()%>') no-repeat center;">
            					<a href="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>">

									<div class="dim"></div>

            						<div style="position:absolute;left:0;top:0;z-index:201;width:100%;padding:58px 60px 0 60px">    <!--jquery 방법에 따라 inline  임시css  추후 변경바람니다-->
                						<p class="txt1"><%#Eval("campaignName") %></p>
										<p class="txt2" <%# Convert.ToInt32(Eval("sf_goal_amount")) == 0 ? "style=display:none;" : "style=display:block;" %>>
                                            <%#Eval("sf_current_amount" , "{0:N0}") %>원 후원중</p>
                                        <p class="txt2" <%# Convert.ToInt32(Eval("sf_goal_amount")) == 0 ? "style=display:block;" : "style=display:none;" %>>
                                            <a class="bt-type1" style="width:90px" href="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>">더보기</a></p>
										<span class="wrap-graph" id="graph" runat="server" >
											<span class="bg-graph">
                        						<span id="per" runat="server" class="rate" ></span>      <!--노란그래프 width % 수치-->
											</span>
										</span>
									</div>
								</a>
							</div>
                        </ItemTemplate>
                    </asp:Repeater>

				</div>
            
				
				<div class="page-prev">이전보기</div>
				<div class="page-next">다음보기</div>
				<div class="page-count">
				</div>
			</div>
        
			<div class="txt-commitview sectionsub-margin1"><a href="complete-campaign/">완료된 캠페인 보기</a></div>
        
			<p class="txt-caption mt30">양육에 가치를 더하는 또 다른 방법
        		<em></em>
			</p>
			<p class="txt-sub">긴급 수술, 재해 복구, 학대 예방, 직업교육 등 어린이들 앞에 놓여진 많은  장애물들을 제거하여 지속적이고 효과적인 양육을 받을 수 있도록 돕습니다.</p>
        
			<ul class="list-critical-needs">
        		<li><a href="/sponsor/special/fixed/CSPF">태아·영아 생존</a><span class="arrow"></span></li>
				<li><a href="/sponsor/special/fixed/RF">재난 구호</a><span class="arrow"></span></li>
				<li><a href="/sponsor/special/fixed/EDU">교육 지원</a><span class="arrow"></span></li>
				<li><a href="/sponsor/special/fixed/MDA">의료 지원</a><span class="arrow"></span></li>
				<li><a href="/sponsor/special/fixed/OVC">취약 어린이 지원</a><span class="arrow"></span></li>
				<li><a href="/sponsor/special/fixed/AIDS">에이즈 예방 및 퇴치</a><span class="arrow"></span></li>
			</ul>
        
			<div class="bg-gray sectionsub-margin1">
        		<p class="txt-caption">기념일엔 더 특별한 후원을</p>
				<p class="txt-sub2">내 아이의 첫 생일, 결혼기념일 등 특별한 날을 오래 기억할 수 있도록 여러분이 받은 사랑과 축복을 어린이들에게 나눠주세요.</p>
            
        		<ul class="list-banner">
            		<li><a href="/sponsor/special/first-birthday/"><img src="/common/img/page/sponsor/img-banner.jpg" alt="Happy 1.st Birthday 첫 생일 첫 나눔" /></a></li>
					<li><a href="/sponsor/special/wedding/"><img src="/common/img/page/sponsor/img-banner2.jpg" alt="나눔으로 시작하는 동행, 결혼 첫 나눔" /></a></li>
					<li><a href="/sponsor/special/fixed/legacy"><img src="/common/img/page/sponsor/img-banner3.jpg" alt="후원자님의 삶과 사랑을 전하는 믿음의 유산" /></a></li>
				</ul>
			</div>
        
			<p class="txt-caption mt30">컴패션북한사역</p>
			<p class="txt-sub2">북한을 변화시킬 희망의 씨앗이 될 북한어린이들에게 전인적인 양육을 제공하기 위한 준비사역에 재정적으로 함께해주세요.</p>
        
			<ul class="list-banner">
				<li><a href="/sponsor/special/fixed/NK"><img src="/common/img/page/sponsor/img-banner4.jpg" alt="북한어린이를 향한 변화의 시작 컴패션북한사역" /></a></li>
			</ul>
        

		</div>
    
	<!--//-->
	</div>

	



</asp:Content>
