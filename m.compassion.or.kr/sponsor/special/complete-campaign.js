﻿
$(function () {

	$page.init();

});


var $page = {

	init: function () {

	}
};


(function () {
	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, paramService) {

		// 어린이선물
		$scope.complete = {

			instance: null,
			list: null,
			total : -1 , 
			params : {
				page : 1 , 
				rowsPerPage : 5
			},

			init: function () {
				
				$scope.complete.item = $.extend({}, $scope.child);
			
				$scope.complete.getList();

			},

			goView : function($event , item){
				$event.preventDefault();
				location.href = "/sponsor/special/view/" + item.campaignid;
			} , 

			getList : function (params) {
				$scope.complete.params = $.extend($scope.complete.params, params);
				console.log($scope.complete.params)
				$http.get("/api/special-funding.ashx?t=complete-list", { params: $scope.complete.params }).success(function (r) {
					console.log(r);
					if (r.success) {

						var list = r.data;
						$.each(list, function () {
							this.startdate = new Date(this.startdate);
							this.enddate = new Date(this.enddate);
						});

						$scope.complete.list = r.data;
						$scope.complete.total = r.data.length > 0 ? r.data[0].total : 0;
						
					} else {
						alert(r.message);
					}

				});
		
			}

		}



		$scope.complete.init();

	});

})();
