﻿


(function () {

    var app = angular.module("cps.page", []);
    app.controller("defaultCtrl", function ($scope, $http, popup, footerSponsorAction) {

        $(".use_digit").currencyFormat();

        if ($(".frequency").length > 0) {
            $($(".frequency")[1]).addClass("selected", true);
        }

        $(".frequency").click(function () {
            $(".frequency").removeClass("selected");
            $(this).addClass("selected");
            return false;
        })

        if (!$("#sf_is_regular").length > 0 || !$("#sf_is_temporary").length > 0) {
            $("#sf_is_regular").hide();
            $("#sf_is_temporary").hide();
        }

        $scope.total = -1;
        $scope.list = [];
        $scope.requesting = false;

        $scope.params = {
            page: 1,
            rowsPerPage: 3,
            campaignId: $("#campaignId").val()
        };

        $scope.getList = function (params) {

            $scope.params = $.extend($scope.params, params);

            $http.get("/api/special-funding.ashx?t=report", { params: $scope.params }).success(function (r) {
                console.log(r);

                if (r.success) {

                    var list = r.data;

                    if (r.data.length > 0) {
                        $scope.total = r.data[0].total;
                        $scope.list = $.merge($scope.list, list);
                        console.log($scope.list, $scope.total);

                    } else {
                        $scope.total = 0;
                    }

                } else {
                    alert(r.message);
                }
            });
        };

        $scope.frequencyClick = function (f) {
            $("#frequency").val(f);
        }

        $scope.showMore = function () {
            $scope.getList({ page: $scope.params.page + 1 });
        };

        $scope.item = $.parseJSON($("#data").val());
        console.log($scope.item);

        if ($scope.item.EndDate == null || new Date() - new Date($scope.item.EndDate)) {
            $scope.getList();
        }


        // 결제 
        $scope.goPay = function ($event) {
            $event.preventDefault();

            //var frequency = $(".frequency.selected").data("id");
            var frequency = $("#frequency").val();
            var amount = $("#dsp_amount").val().replace(/\,/g, "");

            if (amount < 1000) {
                alert("후원금은 천원 이상 입력 가능합니다.");
                return;
            }

            if (amount % 1000 > 0) {
                alert("죄송합니다.\n천원단위로 후원 가능합니다.");
                return;
            }

            goPay(frequency, amount);
        };

        var goPay = function (frequency, amount) {
            console.log("goPay", frequency, amount);

            if (frequency == null) {
                alert("후원유형을 선택해주세요.");
                return;
            }

            var params = {
                campaignId: $("#campaignId").val(),
                frequency: frequency,
                amount: amount
            }
            $.post("/sponsor/pay-gateway.ashx?t=go-special", params).success(function (r) {

                if (r.success) {
                    location.href = r.data;

                } else {
                    alert(r.message);
                }
            });


        };

        if ($("#hd_isClose").val() == "N") {
            var frequency = "";
            frequency += ($("#sf_is_regular").length > 0) ? "Y" : "N";
            frequency += ($("#sf_is_temporary").length > 0) ? "Y" : "N";
            footerSponsorAction.init($scope, frequency, true, function (frequency, amount) {

                goPay(frequency, amount);

            });
        }

    });





})();