﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RF.aspx.cs" Inherits="sponsor_special_fixed_rf" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/sponsor/special/fixed/fixed.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
    <script src="/common/js/util/swiper.min.js"></script>
    <link href="/common/css/swiper.css" rel="stylesheet" />
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="campaignId" />
	<input type="hidden" runat="server" id="frequency" />
	<input type="hidden" runat="server" id="amount" />

	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="sponsor-special">
    	
			<div class="wrap-visualtop sectionsub-margin1" style="background:url('/common/img/page/sponsor/img-topvisual3.jpg') no-repeat">
        		<div class="dim"></div>    <!--dim-->
				<div class="contain" style="padding-top:60px">    <!--padding으로 컨텐츠 가운데에 맞게 조정-->
					<strong class="txt-slogon">재난으로 고통 받는 어린이 지원</strong>
					재난 구호 Disaster Relief
					<div class="share-sns">
                		<a href="#" ng-click="sns.show($event)"><img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
					</div>
				</div>
			</div>
			
			<!-- 후원영역 -->
			<div class="bg-gray sectionsub-margin1 direct-backing">
				<div class="wrap-tab5">
					<asp:PlaceHolder runat="server" ID="ph_sf_is_regular" Visible="false">
            		<a href="#"  id="sf_is_regular" class="frequency" data-id="정기" style="width:50%">정기후원</a>
					</asp:PlaceHolder>
					<asp:PlaceHolder runat="server" ID="ph_sf_is_temporary" Visible="false">
					<a href="#" id="sf_is_temporary" class="frequency" data-id="일시" style="width:50%">일시후원</a>
					</asp:PlaceHolder>
				</div>
				<div class="mt10"><input type="text" id="dsp_amount" value="20,000" class="input_amount number_only use_digit" maxlength="10" style="width:100%"  placeholder="금액을 입력해주세요."/></div>
				<div class="wrap-bt mt10"><a  ng-click="goPay($event)" class="bt-type6" style="width:100%">바로 후원하기</a></div>
			</div>

			<!--// 후원영역 -->
            <style>
                .sponsor-special .wrap-slider .page-count {
                    display: block;
                    position: absolute;
                    bottom: 5px;
                    z-index: 202;
                    text-align: center;
                    width: 100%;
                }
                .sponsor-special .wrap-slider .page-count>span {
                    display: inline-block;
                    width: 8px;
                    height: 8px;
                    background-color: #999999;
                    font-size: 0;
                    line-height: 0;
                    text-indent: -9999px;
                    margin: 0 1px;
                    border-radius: 4px;
                    -webkit-border-radius: 4px;
                    -moz-border-radius: 4px;
                }
                .sponsor-special .wrap-slider .page-count>.swiper-pagination-bullet-active {
                    width: 25px;
                    height: 8px;
                    background-color: #666666;
                }
                .sponsor-special .wrap-slider .page-prev, .sponsor-special .wrap-slider .page-next { display:none; }


                /* !important is needed sometimes */
                .scrollbaron::-webkit-scrollbar {
                width: 5px !important;
                }

                /* Track */
                .scrollbaron::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3) !important;
                -webkit-border-radius: 10px !important;
                border-radius: 10px !important;
                }

                /* Handle */
                .scrollbaron::-webkit-scrollbar-thumb {
                -webkit-border-radius: 10px !important;
                border-radius: 10px !important;
                background: #aaaaaa !important; 
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5) !important; 

                }
                .scrollbaron::-webkit-scrollbar-thumb:window-inactive {
                background: #aaaaaa !important; 
                }
            </style>
            <p class="txt-caption" style="margin-top:30px; margin-bottom:20px;">재난&#149;재해소식</p>	
            <div class="wrap-slider sectionsub-margin1 swiper-container" style="border:1px solid #666666; border-width:1px 0;">
				<div class="swiper-wrapper" id="layer_banner" runat="server">
					 <asp:Repeater runat="server" ID="repeater">
                        <ItemTemplate>
							<div class="item-field swiper-slide">
                                <div class="txt" style="text-align: center;">
                                    <p class="textcrop-1row tit" style="width:95%; margin:5px auto; font-size:15px !important; font-weight:bold;"><%#Eval("subject") %></p>
                                    <p class="textcrop-1row con scrollbaron" style="width:95%; margin:0 auto; overflow-y:auto; max-height:75px; margin-bottom:20px; "><%# ((string)Eval("contentMobile")).Replace("\n", "<br/>") %></p>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                     <script>
                        $(document).ready(function () {
                            var swiper2 = new Swiper('.swiper-container', {
                                pagination: '.swiper-container .page-count',
                                paginationClickable: true,
                                //nextButton: '.swiper-container .page-next',
                                //prevButton: '.swiper-container .page-prev',
                                nextButton: null,
                                prevButton: null,
                                paginationBulletRender: function (index, className) {
                                    return '<span class="' + className + '">' + (index + 1) + '</span>';
                                },
                                spaceBetween: 30
                            });


                            //$('.resetscroll').each(function () {
                            //    var cardID = $(this).attr('id');
                            //    var a = new iScroll(cardID, {
                            //        hScroll: false, hideScrollbar: false
                            //    });
                                
                            //    a.destroy();
                            //});
                            //var iscroll = new iScroll("layer_banner", {
                            //    hScroll: false, hideScrollbar: false
                            //});
                        });
                    </script>
				</div>
				<div class="page-prev">이전보기</div>
				<div class="page-next">다음보기</div>
				<div class="page-count">
				</div>
			</div>


			<div class="editor-html" style="margin-top:10px;">
                <%=sf_content_m %>
			</div>
        
			<!-- 다운로드 -->
			<div ng-if="report.total > 0">
				<p class="txt-caption mt10">양육을 돕는 뉴스레터</p>
				<ul class="list-report">
        			<li ng-repeat="item in report.list">
						<a ng-href="{{item.sr_file}}" target="_blank">{{item.sr_title}} <img src="/common/img/icon/new.png" class="new" alt="" ng-show="item.is_new"/><img src="/common/img/icon/pdf.png" class="pdf" alt="레포트 다운로드" /></a>

        			</li>
				</ul>
        
				<span class="more-loading" ng-click="report.showMore($event)" ng-show="report.total > report.list.length"><span>더보기</span></span>
			</div>
        
			<!--// 다운로드 -->
		</div>
    
		<div data-id="footer_action"></div>
	<!--//-->
	</div>


</asp:Content>
