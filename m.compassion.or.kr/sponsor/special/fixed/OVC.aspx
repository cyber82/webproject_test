﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OVC.aspx.cs" Inherits="sponsor_special_fixed_ovc" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/sponsor/special/fixed/fixed.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="campaignId" />
	<input type="hidden" runat="server" id="frequency" />
	<input type="hidden" runat="server" id="amount" />
	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="sponsor-special">
    	
			<div class="wrap-visualtop sectionsub-margin1" style="background:url('/common/img/page/sponsor/img-topvisual6.jpg') no-repeat">
        		<div class="dim"></div>    <!--dim-->
				<div class="contain" style="padding-top:60px">    <!--padding으로 컨텐츠 가운데에 맞게 조정-->
					<strong class="txt-slogon">‘위험’과의 불안한 동거</strong>
					취약 어린이 지원 Highly Vulnerable Children
					<div class="share-sns">
                		<a href="#" ng-click="sns.show($event)"><img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
					</div>
				</div>
			</div>

			<div class="bg-gray sectionsub-margin1 direct-backing">
				<div class="wrap-tab5">
            		<asp:PlaceHolder runat="server" ID="ph_sf_is_regular" Visible="false">
            		<a href="#"  id="sf_is_regular" class="frequency" data-id="정기" style="width:50%">정기후원</a>
					</asp:PlaceHolder>
					<asp:PlaceHolder runat="server" ID="ph_sf_is_temporary" Visible="false">
					<a href="#" id="sf_is_temporary" class="frequency" data-id="일시" style="width:50%">일시후원</a>
					</asp:PlaceHolder>
				</div>
				<div class="mt10"><input type="text" id="dsp_amount" value="20,000" class="input_amount number_only use_digit" maxlength="10" style="width:100%"  placeholder="금액을 입력해주세요."/></div>
				<div class="wrap-bt mt10"><a  ng-click="goPay($event)" class="bt-type6" style="width:100%">바로 후원하기</a></div>
			</div>
        
			<div class="editor-html">
				<%=sf_content_m %>
			</div>
        
			<!-- 다운로드 -->
			<div  ng-if="report.total > 0">
				<p class="txt-caption mt10">생명을 살리는 뉴스레터</p>
				<ul class="list-report">
        			<li ng-repeat="item in report.list">
						<a ng-href="{{item.sr_file}}" target="_blank">{{item.sr_title}} <img src="/common/img/icon/new.png" class="new" alt="" ng-show="item.is_new"/><img src="/common/img/icon/pdf.png" class="pdf" alt="레포트 다운로드" /></a>
        			</li>
				</ul>
        
				<span class="more-loading" ng-click="report.showMore($event)" ng-show="report.total > report.list.length"><span>더보기</span></span>
			</div>
			<!--// 다운로드 -->
		</div>
    <div data-id="footer_action"></div>
	<!--//-->
	</div>



</asp:Content>
