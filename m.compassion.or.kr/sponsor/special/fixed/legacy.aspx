﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="legacy.aspx.cs" Inherits="sponsor_special_fixed_legacy" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/sponsor/special/fixed/fixed.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="campaignId" />
	<input type="hidden" runat="server" id="frequency" />
	<input type="hidden" runat="server" id="amount" />

	<div class="wrap-sectionsub">
	<!---->
    
		<div class="member-join sponsor-special">
    	
			<div class="wrap-visualtop sectionsub-margin1" style="background:url('/common/img/page/sponsor/img-topvisual14.jpg') no-repeat">
        		<div class="dim"></div>    <!--dim-->
				<div class="contain" style="padding-top:30px">    <!--padding으로 컨텐츠 가운데에 맞게 조정-->
					<strong class="txt-slogon">믿음의 유산 사랑이 남습니다.</strong>
					믿음으로 하루하루를 성실하게 보낸 후원자님의 삶은<br />
					전해지고 이어져야 할 소중한 자산입니다.<br />
					유산기부를 통해 전해질 믿음, 소망, 사랑은 어린이들이 꿈을<br />
					향해 일어설 수 있는 희망의 이유가 될 것입니다.

					<div class="share-sns">
                		<a href="#" ng-click="sns.show($event)"><img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
					</div>
				</div>
			</div>

			<p class="txt-caption mt20">신청방법</p>
			<p class="txt-sub2">'믿음의 유산’ 후원 상담을 원하시면 '문의하기' 클릭 후 성함과 연락처를 남겨주세요. 담당자 확인 후 연락 드리겠습니다. </p>
        
			<p class="txt-emp color1 mt20">유언을 통한 기부</p>
			<p class="txt-sub">가장 일반적인 형태의 유산 기부로, 민법에서 정한 유언 공증의 방식에 따라  유산을 기부하실 수 있습니다. 후원자님이 남기신 유언에 따라 추후 유산이 집행되어, 지정하신 용처에 맞게 후원금이 사용됩니다.</p>
			<ul class="list-bullet2 lineheight1">
        		<li>유산기부 상담 &gt; 법률, 세무 검토 &gt; 유언공증 및 서약서 작성 &gt; 사유언 집행 &gt; 후원금 처리</li>
			</ul>
        
			<p class="txt-emp color1 mt20">상속 유산기부</p>
			<p class="txt-sub">부모님, 가족으로부터 상속받은 유산을 컴패션 유산기부 프로그램 ‘믿음의 유산’에 기부하실 수 있습니다.</p>
			<ul class="list-bullet2 lineheight1">
        		<li>유산기부 상담 &gt; 후원금 용처 선택 &gt; 후원금 처리 &gt; 후원 결과 보고서</li>
			</ul>
        
			<p class="txt-emp color1 mt20">그 외 유산기부</p>
			<p class="txt-sub">뜻 깊은 나눔을 실천하고 나눔의 열매를 경험하고자 할 경우 유산기부 프로그램 ‘믿음의 유산’을 통해 미리 기부를 준비하실 수 있습니다.</p>
			<ul class="list-bullet2 lineheight1">
        		<li class="nobullet">※ 법무법인 광장의 사회공헌위원회에서 유산기부에 관련된 법률 자문을 제공합니다.</li>
			</ul>

			<div class="bg-gray sectionsub-margin1 mt30">
        		<p class="txt-caption">후원금은 이렇게 사용됩니다!</p>
				<p class="txt-sub2">상담을 통해 기부금이 가장 소중하게 쓰여질 사용처를 지정하실 수 있습니다.</p>
				<div class="mt20"><img src="/common/img/page/sponsor/img32.jpg" width="100%" alt="" /></div>
				<p class="txt-emp mt20">미결연 어린이 후원금 지원</p>
				<p class="txt-sub2">컴패션어린이센터에는 아직 후원자들을 만나지 못한 어린이들이 있습니다. 결연을 기다리고 있는 어린이들을 도와주세요.</p>
				<p class="txt-emp mt10">어린이센터 설립 후원</p>
				<p class="txt-sub2">컴패션어린이센터를 통해 어린이들은 전인적인 양육을 받습니다. 더 많은 어린이들이 혜택을 받을 수 있도록 신규 어린이센터 설립을 지원해주세요.</p>
				<p class="txt-emp mt10">양육보완 프로그램 후원</p>
				<p class="txt-sub2">의료지원, 재난구호, 환경 개선 등의 꼭 필요한 부분을 지원해주세요.</p>
				<p class="txt-emp mt10">태아•영아생존프로그램 후원</p>
				<p class="txt-sub2">태아•영아 사망률이 높은 지역에 거주하는 아기와 엄마를 살리고, 건강한 삶을 영위할 수 있도록 도와주세요.</p>
			</div>

			<div class="mt20"><img src="/common/img/page/sponsor/img33.jpg" width="100%" alt="" /></div>
        
			<div class="wrap-bt"><a href="https://app.smartsheet.com/b/form?EQBCT=f55cd9f7170f4e029c07099fbb2c401b" target="_blank" class="bt-type6" style="width:100%">문의하기</a></div>
        
		</div>
    
	<!--//-->
	</div>



</asp:Content>
