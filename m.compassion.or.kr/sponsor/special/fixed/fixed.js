﻿
(function () {

	var app = angular.module("cps.page", []);
	app.controller("defaultCtrl", function ($scope, $http, popup, footerSponsorAction) {

		$(".use_digit").currencyFormat();

		if ($(".frequency").length > 0) {
		    $($(".frequency")[0]).addClass("selected", true);
		}



		$(".frequency").click(function () {
			$(".frequency").removeClass("selected");
			$(this).addClass("selected");
			return false;
		})

		// 뉴스레터
		$scope.report = {
			total: -1,
			list: [],
			params: {
				page: 1,
				rowsPerPage: 3,
				campaignId: $("#campaignId").val()
			},

			getList: function (params, reload) {
				$scope.report.params = $.extend($scope.report.params, params);

				$http.get("/api/special-funding.ashx?t=report", { params: $scope.report.params }).success(function (r) {
					if (r.success) {
						console.log(r.data);
						var list = r.data;

						$.each(list, function () {
							this.sr_regdate = new Date(this.sr_regdate);
							var elapsed = (new Date() - this.sr_regdate) / (1000 * 60 * 60 * 24);		// days
							this.is_new = elapsed < 20;

						});

						// 더보기인경우 merge
						if (reload) {
							$scope.report.list = list;
						} else {
							$scope.report.list = $.merge($scope.report.list, list);
						}
						$scope.report.total = list.length > 0 ? list[0].total : 0;


					} else {
						alert(r.message);
					}
				});

			},

			showMore: function ($event) {
				$event.preventDefault();

				$scope.report.params.page++;
				$scope.report.getList();
			}

		};

		$scope.report.getList();
	

		$scope.showSns = function ($event) {
			console.log("showSns");
			sns.show($event)
		}

		// 결제 
		$scope.goPay = function ($event) {
			$event.preventDefault();
			
			var frequency = $(".frequency.selected").data("id");
			var amount = $("#dsp_amount").val().replace(/\,/g, "");

			if (amount < 1000) {
				alert("후원금은 천원 이상 입력 가능합니다.");
				return;
			}

			if (amount % 1000 > 0) {
				alert("죄송합니다.\n천원단위로 후원 가능합니다.");
				return;
			}

			goPay(frequency, amount);
		};

		var goPay = function (frequency, amount) {
			console.log("goPay", frequency, amount);

			var params = {
				campaignId: $("#campaignId").val(),
				frequency: frequency , 
				amount: amount
			}
			$.post("/sponsor/pay-gateway.ashx?t=go-special", params).success(function (r) {

				if (r.success) {
					location.href = r.data;
				} else {
					alert(r.message);
				}
			});


		};

		var frequency = "";
		frequency += ($("#sf_is_regular").length > 0) ? "Y" : "N";
		frequency += ($("#sf_is_temporary").length > 0) ? "Y" : "N";
		footerSponsorAction.init($scope, frequency , true , function (frequency , amount) {
			
			goPay(frequency, amount);

		});

		
	});





})();
