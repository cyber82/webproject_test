﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NK.aspx.cs" Inherits="sponsor_special_fixed_nk" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/sponsor/special/fixed/fixed.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="campaignId" />
	<input type="hidden" runat="server" id="frequency" />
	<input type="hidden" runat="server" id="amount" />
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="sponsor-special">
    	
			<div class="wrap-visualtop sectionsub-margin1" style="background:url('/common/img/page/sponsor/img-topvisual8.jpg') no-repeat">
				<div class="contain" style="padding-top:150px">    <!--padding으로 컨텐츠 가운데에 맞게 조정-->
					<div class="share-sns">
                		<a href="#" ng-click="sns.show($event)"><img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
					</div>
				</div>
			</div>

			<div class="bg-gray sectionsub-margin1 direct-backing">
				<div class="wrap-tab5">
            		<asp:PlaceHolder runat="server" ID="ph_sf_is_regular" Visible="false">
            		<a href="#"  id="sf_is_regular" class="frequency" data-id="정기" style="width:50%">정기후원</a>
					</asp:PlaceHolder>
					<asp:PlaceHolder runat="server" ID="ph_sf_is_temporary" Visible="false">
					<a href="#" id="sf_is_temporary" class="frequency" data-id="일시" style="width:50%">일시후원</a>
					</asp:PlaceHolder>
				</div>
				<div class="mt10"><input type="text"id="dsp_amount" value="20,000" class="input_amount number_only use_digit" maxlength="10" style="width:100%"  placeholder="금액을 입력해주세요."/></div>
				<div class="wrap-bt mt10"><a href="#" ng-click="goPay($event)" class="bt-type6" style="width:100%">바로 후원하기</a></div>
			</div>
        
			<div class="editor-html">
				<%=sf_content_m %>
			</div>
        

			<div class="hope-wrapping sectionsub-margin1">
        		<p class="txt-caption3">모금된 후원금은 컴패션북한사역<br>준비영역에 사용됩니다.</p>
				<ul class="list-hope">
					<li><span class="box"><span class="imgtxt"><img src="/common/img/page/sponsor/txt-hope1.gif" alt="" width="25" height="30"></span>
                		전인적 어린이양육<br>프로그램
						<em>(Holistic Child<br>Development)</em>
					</span></li>
					<li><span class="box"><span class="imgtxt"><img src="/common/img/page/sponsor/txt-hope2.gif" alt="" width="29" height="32"></span>
                		어린이센터 수립전략 및<br>운영 시스템
						<em>(Operation System)</em>
					</span></li>
					<li><span class="box"><span class="imgtxt"><img src="/common/img/page/sponsor/txt-hope3.gif" alt="" width="23" height="30"></span>
                		샬롬기도운동
						<em>(Prayer<br>Movement)</em>
					</span></li>
					<li><span class="box"><span class="imgtxt"><img src="/common/img/page/sponsor/txt-hope4.gif" alt="" width="20" height="30"></span>
                		비전캐스팅 및 사역훈련
						<em>(Equipping Church<br>Partners)</em>
					</span></li>
				</ul>
			</div>


			<div class="wrap-bt"><a class="bt-type6" style="width:100%" href="#">컴패션북한사역 자세히 보기</a></div>
        
			 <div class="box-tel">
        		<p class="txt-title">후원문의</p>
				<a class="tel" href="tel:02-740-1000"><strong>전화</strong> 02)740-1000</a>
				<a class="email" href="mailto:nkhope@compassion.or.kr"><strong>이메일</strong> nkhope@compassion.or.kr</a>
			</div>

        

		</div>
    <div data-id="footer_action"></div>
	<!--//-->
	</div>



</asp:Content>
