﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="sponsor_user_funding_update" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript" src="/sponsor/user-funding/update.js"></script>
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>


</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" id="hd_image_domain" runat="server" />
    <input type="hidden" id="hd_upload_root" runat="server" />
    <input type="hidden" id="hd_uf_image" runat="server" value="" />
    <input type="hidden" id="hd_uf_content_image" runat="server" value="" />
    <input type="hidden" id="hd_uf_movie" runat="server" value="" />

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <!---->

        <div class="member-join sponsor-funding">

            <p class="txt-title mt0">나눔펀딩 이미지</p>
            <div class="reg-fundimg" style="background: no-repeat center top;" id="btn_uf_image">
                <input type="file" accept="image/*" class="hidden_btn_uf_image" style="width:280px;height:150px;top:0px;left:0px;position:absolute;opacity:0.0;background:red;z-index:10">
                <a class="img_guide">
                    <strong>대표 이미지를 등록해 주세요.</strong>
                    최대 1MB 첨부가능 / JPEG,JPG,PNG,GIF<br />
                    1280*960이상 / 4:3비율권장
                </a>
				
                <!-- 이미지 등록 시, 아래 <button>태그 노출 (디폴트는 display:none;) -->
                <button class="fundimg-del" id="btn_uf_image_del" style="z-index: 3147483583"><span>삭제</span></button>
            </div>


            <p class="txt-stitle mt30">추가 영상(선택사항)<a class="add-file" ng-hide="form.movie.list.length > 2" ng-click="form.movie.control($event , -1)"><img src="/common/img/btn/fileplus.png" alt="추가" /></a></p>
            <fieldset class="frm-input">
                <legend>유투브 경로입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row" ng-repeat="item in form.movie.list">


                            <span class="row pos-filebtn">
                                <input type="text" id="youtube_url_{{$index}}" name="uf_movie" value="{{item.val}}" placeholder="" style="width: 100%" />
                                <a class="minus-file" ng-hide="$index == 0 && form.movie.list.length > 2" class="regist_img" ng-click="form.movie.control($event , $index)">
                                    <img ng-src="/common/img/btn/fileminus.png" ng-show="$index > 0" alt="삭제" />

                                </a></span>
                        </span>


                    </div>
                </div>
            </fieldset>

            <div class="box-gray mt20">
                <strong style="padding-bottom: 5px"><span class="color1">유투브 영상 업로드 안내</span></strong>
                <ul class="list-nobullet">
                    <li>공유하실 유투브 영상에서 공유하기 버튼을 클릭하신 뒤 https://youtu.be/compassion과 같은 주소를 복사하여 붙여 넣어주세요.</li>
                </ul>
            </div>

            <p class="txt-title">나눔펀딩 주요 정보</p>
            <fieldset class="frm-input">
                <legend>나눔펀딩 주요 정보 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row pos-byte1">

                            <input type="text" id="uf_title" runat="server" maxlength="20" placeholder="" style="width: 100%" /><span class="txt-byte"><em><span id="uf_title_count">0</span></em>/20</span></span>
                        <span class="row pos-byte2">
                            <textarea style="width: 100%; height: 60px" id="uf_summary" runat="server" maxlength="40" placeholder=""></textarea><span class="txt-byte"><em><span id="uf_summary_count">0</span></em>/40</span></span>
                    </div>
                </div>
            </fieldset>

            <p class="txt-title">나눔펀딩 상세 스토리</p>
            <fieldset class="frm-input">
                <legend>나눔펀딩 상세 스토리 정보 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <div class="frm-row box mt0">
                            <textarea style="width: 100%; height: 200px" id="uf_content" runat="server" placeholder=""></textarea>

                            <p class="txt-byte"><em><span id="uf_content_count"></span></em>/2000자</p>
                        </div>
                    </div>
                </div>
            </fieldset>

            <p class="txt-stitle mt20">
                본문 이미지 (선택사항)   <a class="add-file" ng-hide="form.image.list.length > 4" ng-click="form.image.control($event , -1)">
                    <img ng-src="/common/img/btn/fileplus.png" alt="등록" />
                </a>
            </p>
            <!-- -->
            <div class="reg-fundimg uf_content_image" ng-repeat="item in form.image.list" style="background: url('{{item.url}}') no-repeat center top; margin-bottom: 5px" data-url="{{item.val}}" id="uf_content_image_{{$index}}">
                <input type="file" accept="image/*" class="hidden_uf_content_image_{{$index}}" style="width:280px;height:150px;top:0px;left:0px;position:absolute;opacity:0.0;background:red;z-index:10">
                <a class="minus-file" ng-hide="$index == 0" style="z-index: 6000000000" ng-click="form.image.control($event , $index)">
                    <img ng-src="/common/img/btn/fileminus.png" alt="삭제" />
                </a>
                <a class="img_guide">
					<strong>이미지를 등록해 주세요.</strong>
					<p class="bu">최대 1MB 첨부가능 / JPEG,JPG,PNG,GIF</p>
                </a>

				<!-- 첫번째 본문이미지만 아래 <button>태그 노출 (디폴트는 display:none;) -->
				<button class="fundconimg-del"><span>삭제</span></button>
            </div>



            <div class="wrap-bt">
                <a class="bt-type7 fl" style="width: 49%;z-index: 3147483583;" id="btn_cancel" runat="server">취소</a>
                <asp:LinkButton CssClass="bt-type6 fr" Style="width: 49%;z-index: 3147483583;" runat="server" ID="btn_submit" OnClick="btn_submit_Click">수정</asp:LinkButton>
            </div>


        </div>

        <!--//-->
    </div>


</asp:Content>
