﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="guide.aspx.cs" Inherits="sponsor_user_funding_guide" %>

<div style="background: transparent;" class="fn_pop_container" id="childModalview">

    <div class="wrap-layerpop">
        <div class="header2">
            <p class="txt-title">나눔펀딩 개설안내</p>
        </div>
        <div class="contents">

            <div class="sponsor-funding">
                <p class="txt-caption">따뜻한 나눔 펀딩,<br />
                    당신의 이야기를 들려주세요.</p>
                <div class="top-slogon">
                    <p class="txt">어떤 펀딩을 구성할 수 있나요?</p>
                    에콰도르 지진 긴급 구호, 기근에 시달리는 에티오피아를 위한 식량 지원 사업 등 나눔펀딩을 제작하는 시점에서 가장 모금이 시급한 기부처를 안내해드립니다.
                </div>

                <p class="txt-caption3 color1 mt20" style="padding-bottom: 10px">어떤 나눔 스토리를 써야 하나요?</p>
                누구나 나눔 펀딩의 개설자가 될 수 있습니다.<br />
                많은 사람들이 공감할 수 있는 구체적인 이야기를 들려주세요. 작성이 막막하다면, 아래 내용을 참고해서 구성해보세요.
                <div class="linebar" style="margin: 20px 0"></div>

                <dl class="txt-define ic-define1">
                    <dt>자기소개</dt>
                    <dd>당신은 누구신가요?<br />
                        사람들이 신뢰를 갖고 함께할 수 있도록 간단하게 ‘자신’을 소개 해주세요.
                    </dd>
                </dl>
                <dl class="txt-define ic-define2 mt30">
                    <dt>관심 주제</dt>
                    <dd>컴패션 홈페이지를 둘러보며, 관심있는 주제를 찾아보세요. 주제별 기부처의 모금 목표액을 고려하여, 적절한 금액의 목표금액을 설정하는 것도 중요합니다.
                    </dd>
                </dl>
                <dl class="txt-define ic-define3 mt30">
                    <dt>기념일 모금</dt>
                    <dd>생일, 승진, 졸업 등 오래 기억하고 싶은 날이 있을 때, ‘나눔펀딩’을 통해 기념일을 더 특별하게 기억해보세요.</dd>
                </dl>
                <dl class="txt-define ic-define4 mt30">
                    <dt>리워드 제공</dt>
                    <dd>모금에 성공할 경우, 참여한 사람들이 기념할 수 있는 리워드를 제공해보세요. 더 즐겁고 활발한 모금이 이루어질 수 있습니다.<br />
                        예) 후원 성공 시, 캘리그라피 엽서 제공</dd>
                </dl>
                <dl class="txt-define ic-define5 mt30">
                    <dt>소식 업데이트</dt>
                    <dd>기부자들과 소통해보세요!<br />
                        모금함 개설 후, ‘업데이트 소식’ 메뉴를 통해 모금과 관련된 소식들을 전달할 수 있습니다.</dd>
                </dl>

                <div class="wrap-bt"><a class="bt-type6" style="width: 100%" href="#" ng-click="modal.close($event)">새로운 나눔펀딩 만들기</a></div>


                <div>
                </div>
                <div class="close2"><span class="ic-close" ng-click="modal.close($event)">레이어 닫기</span></div>
            </div>

        </div>
        <!--//레이어 안내 팝업-->




    </div>
</div>
