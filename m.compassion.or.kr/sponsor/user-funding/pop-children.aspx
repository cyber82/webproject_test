﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pop-children.aspx.cs" Inherits="sponsor_user_funding_pop_children"  %>

<div style="background:#fff;width:800px;min-height:600px">
	
	<h3>어린이선택</h3>

	<div style="overflow:auto;height:450px">
	 <ul>
		<li ng-repeat="item in modal.pop_children" ng-click="modal.select(item)">
			<img ng-src="{{item.imageurl}}" style="max-width:100px"/>
			{{item.preferredname}} / {{item.age}} / {{item.country}} / {{item.birthdate | date:'yyyy.MM.dd'}} / {{item.gender}} / {{item.waitingsincedate}} / {{item.remainamount}}
		</li>
	</ul>
	</div>
	
	<div ng-show="modal.item">
	남은 예상금액 : {{modal.item.remainamount | number:N0}}
		<a ng-click="modal.confirm()">선택</a>
	</div>

</div>