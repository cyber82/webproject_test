﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
using Microsoft.AspNet.FriendlyUrls;


public partial class sponsor_user_funding_view : MobileFrontBasePage {

	public int percent;
	public int ori_percent;
	public sp_tUserFunding_get_fResult entity;
	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		
		var requests = Request.GetFriendlyUrlSegments();
		if(requests.Count < 1) {
			Response.Redirect("/", true);
		}

		uf_id.Value = requests[0];
		
	}

	protected override void loadComplete( object sender, EventArgs e )
    {	
		upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_user_funding, uf_id.Value);
		this.ViewState["c_img"] = "";

        using (FrontDataContext dao = new FrontDataContext())
        {
            //entity = dao.sp_tUserFunding_get_f(Convert.ToInt32(uf_id.Value)).FirstOrDefault();
            Object[] op1 = new Object[] { "uf_id" };
            Object[] op2 = new Object[] { Convert.ToInt32(uf_id.Value) };
            entity = www6.selectSP("sp_tUserFunding_get_f", op1, op2).DataTableToList<sp_tUserFunding_get_fResult>().FirstOrDefault();

            if (entity == null)
            {
                entity = new sp_tUserFunding_get_fResult();

                entity.UserPic = "";
                entity.uf_id = 0;
                entity.uf_title = "";
                entity.uf_summary = "";
                entity.uf_current_amount = 0;
                entity.uf_goal_amount = 0;
                base.AlertWithJavascript("나눔펀딩 정보가 없습니다.", "goBack()");
                return;
            }

            campaignId.Value = entity.CampaignID.EmptyIfNull();

            // SNS
            this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
            this.ViewState["meta_title"] = string.Format("[한국컴패션-나눔펀딩]-{0}", entity.uf_title);
            // optional(설정 안할 경우 주석)
            //this.ViewState["meta_description"] = entity.uf_summary;
            this.ViewState["meta_keyword"] = "나눔펀딩," + entity.SponsorName;
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,MobileFrontBasePage)
            this.ViewState["meta_image"] = entity.uf_image.WithFileServerHost();

            creator.Text = entity.SponsorName;
            this.ViewState["creator_image"] = entity.UserPic.WithFileServerHost();
            this.ViewState["entity"] = entity.ToJson();

            uf_title.Text = entity.uf_title;
            uf_summary.Text = entity.uf_summary;
            uf_content.Text = entity.uf_content.ToHtml();
            //uf_cnt_user.Text = entity.uf_cnt_user.ToString("N0");
            hd_is_end.Value = (entity.uf_date_end < DateTime.Now).ToString();

            if (!string.IsNullOrEmpty(entity.uf_movie) && entity.uf_movie.IndexOf("https:") != -1)
            {
                ph_movie.Visible = true;
                repeater_movie.DataSource = entity.uf_movie.Split('|').ToList();
                repeater_movie.DataBind();
            }

            if (!string.IsNullOrEmpty(entity.uf_content_image))
            {
                ph_content_image.Visible = true;
                if (entity.uf_content_image != null)
                {
                    repeater_content_image.DataSource = entity.uf_content_image.Split('|').ToList();
                }
                repeater_content_image.DataBind();
            }

            // 업데이트소식 쓰기가능 여부
            if (UserInfo.IsLogin)
            {
                //ph_add_update.Visible = 
                ph_notice_add.Visible = ph_go_update.Visible = entity.UserID == new UserInfo().UserId;
            }


            /*
			campaignID.Text = entity.CampaignID;
			CampaignName.Text = entity.CampaignName;
			sf_image.Attributes["src"] = entity.sf_image;
			sf_summary.Text = entity.sf_summary;
			sf_date.Text = string.Format( "{0:yyyy년 MM월 dd일}", entity.StartDate ) + " ~ " + string.Format( "{0:yyyy년 MM월 dd일}", entity.EndDate );
			sf_amount.Text = string.Format( "{0:N0}", entity.sf_current_amount ) + "원 / " + string.Format( "{0:N0}", entity.sf_goal_amount ) + "원";
			*/

            // 어린이 펀딩
            if (entity.uf_type == "child")
            {
                var actionResult = new ChildAction().GetChild(entity.ChildMasterID);
                if (actionResult.success)
                {
                    ph_child.Visible = true;
                    var child = (ChildAction.ChildItem)actionResult.data;
                    this.ViewState["c_img"] = child.Pic;
                    c_name.Text = child.Name;
                    c_age.Text = child.Age.ToString();
                    c_country.Text = child.CountryName;
                    c_birth.Text = child.BirthDate.ToString("yyyy년 MM월 dd일");
                    //Response.Write( child.ToJson() );
                }

                var caseStudynew = new ChildAction().GetCaseStudy(entity.ChildMasterID, entity.ChildKey);
                if (caseStudynew.success)
                {
                    var data = (ChildAction.ChildCaseStudy)caseStudynew.data;
                    c_hobby.Text = data.hobby;
                }

            }
            else
            {
                ph_campaign.Visible = ph_campaign_content.Visible = true;
                sf_content_m.Text = entity.sf_content_m.WithFileServerHost();
            }

            // days


            if (entity.uf_current_amount < 1)
                percent = 0;
            else
                percent = Convert.ToInt32(entity.uf_current_amount * 1.0 / entity.uf_goal_amount * 100);


            ori_percent = percent;

            if (percent > 100)
            {
                percent = 100;
            }

            hd_percent.Value = percent.ToString();


            if (entity.uf_date_end.Subtract(DateTime.Now).TotalDays > 0)
            {
                days.Text = string.Format("<span class='d_day'>D-{0}</span>", entity.uf_date_end.Subtract(DateTime.Now).TotalDays.ToString("N0"));
            }
            else
            {
                days.Text = string.Format("<span class='d_day'>{0}</span>", "모금종료");
            }

            /*
			if (percent == 100) {
				days.Text = string.Format( "<span class='d_day'>{0}</span>", "모금종료" );
			}
			*/



        }
	}
	

}