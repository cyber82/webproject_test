﻿ $(function () {

	$page.init();

});

var $page = {

	init: function () {
	
		$("#reply_comment").textCount($("#reply_comment_count"), { limit: 300 });
		if ($("#notice_comment").length > 0) $("#notice_comment").textCount($("#notice_comment_count"), { limit: 300 });


		if ($("#btn_file_path").length > 0) {
			var uploader = attachUploader("btn_file_path");
			uploader._settings.data.fileDir = $("#upload_root").val();
			uploader._settings.data.fileType = "file";
			uploader._settings.data.limit = 5120;
			setTimeout(function () {
			    $("input[name=userfile]").css("display", "block");
			}, 300);
		}


		if ($("#hd_is_end").val() == "True") {
			$(".btn_sponsor").hide();
			$("div[data-id=footer_action]").hide();
			return false;
		}
	}

}

var goBack = function () {
	if (location.pathname.indexOf("/sponsor/user-funding/view") > -1)
		location.href = "/";
	else
		history.back();
}

var app = angular.module('cps.page', []);
app.controller("defaultCtrl", function ($scope, $http, $filter, popup, footerSponsorAction, paramService) {

	$scope.tab = paramService.getParameter("tab");
	if (!$scope.tab) $scope.tab = "1";


	$scope.changeTab = function ($event, tab) {
		$event.preventDefault();
		$scope.tab = tab;

		scrollTo($("#tab"), 30);

	}

	// 댓글
	$scope.reply = {
		total : -1 , 
		list : [] , 
		params : {
			page : 1 , 
			rowsPerPage : 4 , 
			uf_id: $("#uf_id").val()
		},
		comment : "" ,
	
		getList : function (params , reload) {
			$scope.reply.params = $.extend($scope.reply.params, params);

			$http.get("/api/user-funding.ashx?t=reply-list", { params: $scope.reply.params }).success(function (r) {
				//console.log(r);
				if (r.success) {
					var list = r.data;

					$.each(list, function () {
						//this.ur_regdate = moment(this.ur_regdate).fromNow();
						this.ur_regdate = new Date(this.ur_regdate);
					});

					// 더보기인경우 merge
					if (reload) {
						$scope.reply.list = list;
					} else {
						$scope.reply.list = $.merge($scope.reply.list, list);
					}
					$scope.reply.total = list.length > 0 ? list[0].total : 0;


					// 아이폰 footer_action에 가려 댓글 수정버튼 안눌림
					setTimeout(function () {
						$("textarea").bind("click focus blur", function (event) {
							//alert(event.type);
							if (event.type == "click" || event.type == "focus") {
								setTimeout(function () {
									$(".footer_action").css("pointer-events", "none");
								}, 300);
							} else if (event.type == "blur") {
								setTimeout(function () {
									$(".footer_action").css("pointer-events", "auto");
								}, 300);
							}
						});
					}, 500)

				} else {
					alert(r.message);
				}
			});

		} , 
		
		add: function ($event) {

			$event.preventDefault();
			if (!common.checkLogin()) {
				return;
			}

			if ($scope.reply.comment.length < 10) {
				alert("10자 이상 입력해야 등록가능합니다.")
				return;
			}
			
			$http.post("/api/user-funding.ashx", { t: 'add-reply', uf_id: $("#uf_id").val(), comment: $scope.reply.comment }).success(function (result) {
				if (result.success) {
					//console.log(result);
					$scope.reply.comment = "";
					$("#reply_comment_count").text("0");
					$scope.reply.getList({ page: 1 }, true);


					// 아이폰 footer_action에 가려 댓글 수정버튼 안눌림
					setTimeout(function () {
						$("textarea").bind("click focus blur", function (event) {
							//alert(event.type);
							if (event.type == "click" || event.type == "focus") {
								setTimeout(function () {
									$(".footer_action").css("pointer-events", "none");
								}, 300);
							} else if (event.type == "blur") {
								setTimeout(function () {
									$(".footer_action").css("pointer-events", "auto");
								}, 300);
							}
						});
					}, 500)


				} else {
					alert(result.message);
				}
			})
			
		},

		showMore: function ($event) {
			$event.preventDefault();

			$scope.reply.params.page++;
			$scope.reply.getList();
		},

		toggleUpdate: function (idx, $event) {

			target = $(".reply_modify_row[data-idx='" + idx + "']");

			if (target.css("display") == "none") {
				$(".reply_modify_row").slideUp(300);

				if (target.find(".reply_modify").length > 0) target.find(".reply_modify").textCount(target.find(".count"), { limit: 300 });

				target.slideDown(300);
			} else {
				target.slideUp(300);
			}

			$event.preventDefault();
		},

		update: function (idx, $event) {
			if (common.checkLogin()) {

				var content = $("#re_modify_" + idx ).val();


				if (content.length < 10) {
					alert("10자 이상 입력해야 등록가능합니다.")
					return false;
				}
				
				$http.post("/api/user-funding.ashx", { t: 'update-reply', id: idx, content: content }).success(function (result) {
					console.log(result);
					if (result.success) {
						$scope.reply.getList({} , true);
						target.slideDown(300);

					} else {
						alert(result.message);
					}
				})
			}

			$event.preventDefault();
		},

		delete: function (idx, $event) {
			console.log()
			if (common.checkLogin()) {
				if (confirm("삭제하시겠습니까?")) {
					$http.post("/api/user-funding.ashx", { t: 'delete-reply', id: idx, uf_id: $("#uf_id").val() }).success(function (result) {
						if (result.success) {
							alert("삭제 되었습니다.");
							$scope.reply.getList({}, true);
						} else {
							alert(result.message);
						}
					})
				}
			}

			$event.preventDefault();
		}
	};

	$scope.reply.getList();

	// 업데이트 소식
	$scope.notice = {
		total: -1,
		list: [],
		params: {
			page: 1,
			rowsPerPage: 2,
			uf_id: $("#uf_id").val()
		},
		comment: "",
		isNew: false, // 신규글이 있는지 여부
		//hasNew : false ,	// 신규글이 있는지 여부


		getList: function (params, reload) {
			$scope.notice.params = $.extend($scope.notice.params, params);

			$http.get("/api/user-funding.ashx?t=notice-list", { params: $scope.notice.params }).success(function (r) {
				if (r.success) {
					console.log(r.data);
					var list = r.data;

					$.each(list, function () {
						//this.un_regdate = moment(this.un_regdate).fromNow();
						this.un_regdate = new Date(this.un_regdate);
						var split = this.un_image.split("/")
						this.or_image = split[split.length - 1]

						//$scope.notice.setUploadEvent(this.un_id);
					});

					// 더보기인경우 merge
					if (reload) {
						$scope.notice.list = list;
					} else {
						$scope.notice.list = $.merge($scope.notice.list, list);
					}

					$scope.notice.total = list.length > 0 ? list[0].total : 0;
					
					// new 여부 확인
					if ($scope.notice.list[0]) {
						var week_ago = new Date();
						week_ago.setDate(week_ago.getDate() - 7);
						$scope.notice.isNew = $scope.notice.list[0].un_regdate > week_ago;
					}
					/*
					if ($scope.notice.params.page == 1 && $scope.notice.list.length > 0) {
						var elapsed = (new Date() - $scope.notice.list[0].un_regdate) / (1000 * 60 * 60 * 24);		// days
						if (elapsed < 14) {
							$scope.notice.hasNew = true;
						}
					}
					*/

				} else {
					alert(r.message);
				}
			});

		},

		add: function ($event) {

			$event.preventDefault();
			if (!common.checkLogin()) {
				return;
			}

			
			if ($scope.notice.comment.length < 10) {
				alert("10자 이상 입력해야 등록가능합니다.")
				return;
			}
		

			var file = "";
			if ($("#file_path").val() != "") {
				file = $("#upload_root").val() + $("#file_path").val();
			}

			$http.post("/api/user-funding.ashx", { t: 'add-notice', uf_id: $("#uf_id").val(), comment: $scope.notice.comment, file: file }).success(function (result) {
				if (result.success) {
					console.log(result);
					$scope.notice.comment = "";
					$("#notice_comment_count").text(0)
					$("#lb_file_path").val("");
					$("#file_path").val("");
					$scope.notice.getList({ page: 1 }, true);
				} else {
					alert(result.message);
				}
			})

		},

		showMore: function ($event) {
			$event.preventDefault();

			$scope.notice.params.page++;
			$scope.notice.getList();
		},
		
		toggleUpdate: function (idx, $event) {

			$scope.notice.setUploadEvent(idx);
			target = $(".notice_modify_row[data-idx='" + idx + "']");

			if (target.css("display") == "none") {
				$(".notice_modify_row").hide(300);
				$("#file_path").val("");

				if (target.find(".no_modify_comment").length > 0) target.find(".no_modify_comment").textCount(target.find(".count"), { limit: 300 });

				target.slideDown(300);
			} else {
				target.slideUp(300);
			}

			$event.preventDefault();
		},

		update: function (idx, $event) {

			$event.preventDefault();
			if (!common.checkLogin()) {
				return;
			}

			var content = $(".notice_modify_row[data-idx='" + idx + "'] textarea")
			var fileName = $(".notice_modify_row[data-idx='" + idx + "'] input[type='text']");

			if (content < 10) {
				alert("10자 이상 입력해야 등록가능합니다.")
				return;
			}


			var file = "";

			if (fileName.val() != "") {
				file = $("#upload_root").val() + fileName.val();
			}

			console.log(fileName);
			if ($("#file_path").val() != "") {
				file = $("#upload_root").val() + $("#file_path").val();
			}

			$http.post("/api/user-funding.ashx", { t: 'update-notice', uf_id: $("#uf_id").val(), "id": idx, comment: content.val(), file: file }).success(function (result) {
				if (result.success) {
					console.log(result);
					content = "";
					fileName.val("");
					$("#file_path").val("");
					$scope.notice.getList({ page: 1 }, true);
				} else {
					alert(result.message);
				}
			})
			

		},

		delete: function (idx, $event) {
			if (common.checkLogin()) {
				if (confirm("삭제하시겠습니까?")) {
					
					$http.post("/api/user-funding.ashx", { t: 'delete-notice', uf_id: $("#uf_id").val(), id: idx }).success(function (result) {
						if (result.success) {
							alert("삭제 되었습니다.");
							$scope.notice.getList({}, true);
						} else {
							alert(result.message);
						}
					})
					
				}
			}

			$event.preventDefault();
		},

		setUploadEvent: function (idx) {
			if ($("#btn_modify_file_path_" + idx).length > 0) {
				setTimeout(function () {
					var uploader = attachUploader("btn_modify_file_path_" + idx, idx);
					uploader._settings.data.fileDir = $("#upload_root").val();
					uploader._settings.data.fileType = "image";
					uploader._settings.data.limit = 5120;
					setTimeout(function () {
					    $("input[name=userfile]").css("display", "block");
					}, 300);

				}, 600);

				// 삭제버튼
				delete_target = $("#btn_delete_file_" + idx);
				if (delete_target.length > 0) {
					delete_target.unbind("click");
					delete_target.click(function () {
						$("#file_path").val("");
						$(".notice_modify_row[data-idx='" + idx + "'] input[type='text']").val("");
						return false;
					})

				}

			}
		}
	};

	$scope.notice.getList();

	// 참여자
	$scope.user = {
		total: -1,
		list: [],
		params: {
			page: 1,
			rowsPerPage: 4,
			uf_id: $("#uf_id").val()
		},
		comment: "",

		getList: function (params, reload) {
			$scope.user.params = $.extend($scope.user.params, params);

			$http.get("/api/user-funding.ashx?t=user-list", { params: $scope.user.params }).success(function (r) {
				if (r.success) {
					var list = r.data;

					$.each(list, function () {
						//this.un_regdate = moment(this.un_regdate).fromNow();
						this.uu_regdate = new Date(this.uu_regdate);
					});

					// 더보기인경우 merge
					if (reload) {
						$scope.user.list = list;
					} else {
						$scope.user.list = $.merge($scope.user.list, list);
					}
					$scope.user.total = list.length > 0 ? list[0].total : 0;


				} else {
					alert(r.message);
				}
			});

		},

		showMore: function ($event) {
			$event.preventDefault();

			$scope.user.params.page++;
			$scope.user.getList();
		}

	};

	$scope.user.getList();

	// 양육을 돕는 뉴스레터
	$scope.report = {
		total: -1,
		list: [],
		params: {
			page: 1,
			rowsPerPage: 3,
			campaignId: $("#campaignId").val()
		},
		
		getList: function (params, reload) {
			$scope.report.params = $.extend($scope.report.params, params);

			$http.get("/api/special-funding.ashx?t=report", { params: $scope.report.params }).success(function (r) {
				if (r.success) {
					//console.log(r.data);
					var list = r.data;

					$.each(list, function () {
						this.sr_regdate = new Date(this.sr_regdate);
						var elapsed = (new Date() - this.sr_regdate) / (1000 * 60 * 60 * 24);		// days
						this.is_new = elapsed < 14;

					});

					// 더보기인경우 merge
					if (reload) {
						$scope.report.list = list;
					} else {
						$scope.report.list = $.merge($scope.report.list, list);
					}
					$scope.report.total = list.length > 0 ? list[0].total : 0;


				} else {
					alert(r.message);
				}
			});

		},

		showMore: function ($event) {
			$event.preventDefault();

			$scope.report.params.page++;
			$scope.report.getList();
		}

	};

	$scope.report.getList();

	// 결제 
	$scope.goPay = function ($event) {
		$event.preventDefault();

		/*
		if (!$scope.checkEnd()) {
			return false;
		}
		*/

		var frequency = "";
		var amount = $("#dsp_amount").val().replace(/\,/g, "");

		if (amount < 1000) {
			alert("후원금은 천원 이상 입력 가능합니다.");
			return;
		}

		if (amount % 1000 > 0) {
			alert("죄송합니다.\n천원단위로 후원 가능합니다.");
			return;
		}

		goPay(frequency, amount);
	};


	$scope.checkEnd = function () {
		console.log($("#hd_percent").val());
		if ($("#hd_percent").val() >= 100) {
			alert("이미 달성된 나눔펀딩입니다.");
			return false;
		}
		return true;
	}



	var goPay = function (frequency, amount) {
	
		var params = {
			uf_id: $("#uf_id").val(),
			amount: amount
		}
		$.post("/sponsor/pay-gateway.ashx?t=go-user-funding", params).success(function (r) {

			if (r.success) {
				location.href = r.data;
			} else {
				alert(r.message);
			}
		});


	};

	var frequency = "NN"; // 정기,일시 선택숨김
	footerSponsorAction.init($scope, frequency, true, function (frequency, amount) {
		goPay(frequency, amount);
	});

});

var attachUploader = function (button , idx) {
	return new AjaxUpload(button, {
		action: '/common/handler/upload',
		responseType: 'json',
		onChange: function () {
		},
		onSubmit: function (file, ext) {
			this.disable();
		},
		onComplete: function (file, response) {

			this.enable();

			if (response.success) {

				$("#file_path").val(response.name.replace(/^.*[\\\/]/, ''));

				if (button == "btn_file_path") {
					$("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
				} else {
					$(".notice_modify_row[data-idx='"+idx+"'] input[type='text']").val(response.name.replace(/^.*[\\\/]/, ''));
				}

			} else
				alert(response.msg);
		}
	});
}