﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class sponsor_user_funding_update : MobileFrontBasePage {

	public override bool RequireLogin {
		get {
			return true;
		}
	}


	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();
		
		var requests = Request.GetFriendlyUrlSegments();
		if(requests.Count < 1) {
			Response.Redirect("/", true);
		}
		
		btn_cancel.HRef = (Request.UrlReferrer == null) ? "/sponsor/user-funding/" : Request.UrlReferrer.AbsoluteUri;

		UserInfo sess = new UserInfo();
		hd_image_domain.Value = ConfigurationManager.AppSettings["domain_file"];
		hd_upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_user_funding);
		var uf_id = Convert.ToInt32(requests[0]);

		this.PrimaryKey = uf_id.ToString();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.tUserFunding.FirstOrDefault(p => p.uf_id == uf_id && p.UserID == sess.UserId && p.uf_display == true && p.uf_date_end > DateTime.Now);
            var entity = www6.selectQF2<tUserFunding>("uf_id = ", uf_id, "UserID = ", sess.UserId, "uf_display = ", 1, "uf_date_end > ", DateTime.Now);
            if (entity == null)
            {
                base.AlertWithJavascript("수정가능한 나눔펀딩 정보가 없습니다.");
            }            

            
            hd_uf_image.Value = entity.uf_image;
            hd_uf_content_image.Value = entity.uf_content_image;
            hd_uf_movie.Value = entity.uf_movie;
            uf_title.Value = entity.uf_title;
            uf_summary.Value = entity.uf_summary;
            uf_content.Value = entity.uf_content;
        }

	}
	
	// 결제
	protected void btn_submit_Click( object sender, EventArgs e ) {
		
		if(base.IsRefresh) {
			return;
		}

		if(!this.UpdateUserFunding()) {
			return;
		}

	}

	bool UpdateUserFunding()
    {
		
		#region 폼값 체크
		

		if(string.IsNullOrEmpty(hd_uf_image.Value)) {
			base.AlertWithJavascript("대표이미지를 선택해 주세요");
			return false;
		}

		if(string.IsNullOrEmpty(uf_title.Value)) {
			base.AlertWithJavascript("제목을 입력해 주세요");
			return false;
		}

		if(string.IsNullOrEmpty(uf_summary.Value)) {
			base.AlertWithJavascript("한줄설명을 입력해 주세요");
			return false;
		}

		if(string.IsNullOrEmpty(uf_content.Value)) {
			base.AlertWithJavascript("상세스토리를 입력해 주세요");
			return false;
		}

		#endregion
		
		UserInfo sess = new UserInfo();
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.tUserFunding.First(p => p.uf_id == Convert.ToInt32(this.PrimaryKey));
            var entity = www6.selectQF<tUserFunding>("uf_id", Convert.ToInt32(this.PrimaryKey));

            entity.uf_content = uf_content.Value;
            entity.uf_image = hd_uf_image.Value;
            entity.uf_moddate = DateTime.Now;
            entity.uf_movie = hd_uf_movie.Value;
            entity.uf_content_image = hd_uf_content_image.Value;
            entity.uf_summary = uf_summary.Value;
            entity.uf_title = uf_title.Value;

            //dao.SubmitChanges();
            www6.update(entity);

            base.AlertWithJavascript("수정되었습니다.", "location.href='/sponsor/user-funding/view/" + this.PrimaryKey + "'");
        }
		
		return true;
	}
	

}