﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete.aspx.cs" Inherits="sponsor_user_funding_complete" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	<div class="wrap-sectionsub">
<!---->
     
		<div class="sponsor-funding">
    	
			<div class="msg-applycommit">
        		<strong class="txt-msg3">나눔펀딩 등록이<br />완료되었습니다.</strong>
				개설하신 나눔펀딩에서 첫 나눔 후원을 진행해보세요.
				<p class="txt-msg2">등록하신 나눔펀딩은 <a class="color1" href="/my/user-funding/create">마이컴패션&gt; 나눔펀딩관리</a>에서<br />
					새로운 업데이트 소식을 관리하거나<br />
					후원현황을 확인하실 수 있습니다.
				</p>
			</div>
        
			<div class="wrap-bt">
        		<a class="bt-type7 fl" style="width:49%" href="/my/user-funding/create">내 나눔펀딩 보기</a>
				<a class="bt-type6 fr" style="width:49%" href="/sponsor/user-funding/view/<%:uf_id %>">첫 후원하기</a>
			</div>
        
        
		</div>
    
	<!--//-->
	</div>


</asp:Content>
