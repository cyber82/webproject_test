﻿$(function () {
	$motive.init($("#motiveCode"), $("#motiveName"), $("#motive1"), $("#motive2"));

	$("#next_page").click(function () {

		$("a.backgo").unbind("click").attr("href", "");

		$("a.backgo").click(function () {

			location.href = "#";
			$("#sponsor_info").hide();
			$("#sponsor_funding").show();

			return false;
		});

		$("h2#title").text("개설자 정보");

		if ($("#hd_uf_type").val() == "child") {

			if (!validateForm([
				{ id: "#hd_childMasterId", msg: "어린이를 선택해주세요" }
			])) {
				return false;
			}

		} else {
			if (!validateForm([
				{ id: "#CampaignID", msg: "후원금 사용처를 선택해 주세요" }
			])) {
				return false;
			}
		}

		$(".minamount").hide();
		if (isNaN($("#uf_goal_amount").val()) || parseInt($("#uf_goal_amount").val()) < 1) {
			$(".minamount").show();
			$("#uf_goal_amount").focus();
			return false;
		}

		if (!validateForm([
				{ id: "#uf_date_start", msg: "시작일을 선택해 주세요" },
				{ id: "#uf_date_end", msg: "종료일을 선택해 주세요" },
				{ id: "#uf_goal_amount", msg: "목표금액을 입력해 주세요" },
				{ id: "#hd_uf_image", msg: "대표이미지를 선택해 주세요" },
				{ id: "#uf_title", msg: "제목을 입력해 주세요" },
				{ id: "#uf_summary", msg: "한줄설명을 입력해 주세요" },
				{ id: "#uf_content", msg: "상세스토리를 입력해 주세요" }
		])) {
			return false;
		}

		if ($("#uf_title").val().length < 4) { alert("제목을 4자 이상 입력해 주세요"); return false; }
		if ($("#uf_summary").val().length < 4) { alert("한줄설명을 4자 이상 입력해 주세요"); return false; }
		if ($("#uf_content").val().length < 10) { alert("상세스토리를 10자 이상 입력해 주세요"); return false; }

		location.href = "#";
		$("#sponsor_info").show();
		$("#sponsor_funding").hide();


	});

	$("#prev_page").click(function () {
		location.href = "#";
		$("#sponsor_info").hide();
		$("#sponsor_funding").show();

	});

	$("#btn_uf_image_del").click(function () {
		$("#hd_uf_image").val("");
		$("#btn_uf_image").css({ "background-image": "url('')" });
		$("#btn_uf_image").find(".img_guid2").css({ opacity: 100 });
		$("#btn_uf_image_del").hide();
	});

	$("#uf_content_image_del").click(function () {
		$("#hd_uf_content_image").val("");
		$(".uf_content_image").css({ "background-image": "url('')" });
		$(".uf_content_image").find(".img_guide").css({ opacity: 100 });
		$("#uf_content_image_del").hide();
	});


	$("#image_UserPic_del").click(function () {
		$("#hd_user_pic").val("");
		$("#btn_UserPic").css({ "background-image": "url('')" });
		$("#btn_UserPic").find(".img_guide3").css({ opacity: 100 });
		$("#image_UserPic_del").hide();
	});
});


var $extPage = {

	timer: null,
	cert_target: "",		// me , parent
	checked_account_val: "",
	is_checked_account: false,
	selected_campaign: null,

	init: function () {

		$page.init();

		$extPage.setFundingEvent();

		// 가입하기 버튼
		$("#btn_submit").unbind("click");
		$("#btn_submit").click(function () {

			return $extPage.onSubmit();

		});



		$("#addr_domestic_1").val($("#addr_domestic_zipcode").val());

		// 컴파스의 데이타를 불러오는경우 
		if ($("#dspAddrDoro").val() != "") {
			$("#addr_road").text("[도로명주소] (" + $("#addr_domestic_zipcode").val() + ") " + $("#dspAddrDoro").val());
			if ($("#dspAddrJibun").val() != "") {
				$("#addr_jibun").text("[지번] (" + $("#addr_domestic_zipcode").val() + ") " + $("#dspAddrJibun").val());
			}

		} else if ($("#addr_domestic_addr1").val() != "") {

			if ($("#addr_domestic_addr1").val() != "") {

				addr_array = $("#addr_domestic_addr1").val().split("//");
				if (addr_array[0] != "") {
					$("#addr_road").text("[도로명주소] " + addr_array[0] + " " + $("#addr_domestic_addr2").val());
				}
				if (addr_array[1]) {
					$("#addr_jibun").text("[지번주소] " + addr_array[1] + " " + $("#addr_domestic_addr2").val());
				}
			}
		}







	},

	setDateGapComent: function () {

		start = new Date($("#uf_date_start").val());
		end = new Date($("#uf_date_end").val())

		gap = ((end - start) / 86400000);

		target = $("#date_comment");
		if (gap < 30) {
			target.show();
		} else if (gap > 90) {
			target.show();
		} else {
			target.hide();
		}

	},

	setFundingEvent: function () {

		$("#uf_title").textCount($("#uf_title_count"), { limit: 20 });
		$("#uf_summary").textCount($("#uf_summary_count"), { limit: 40 });
		$("#uf_content").textCount($("#uf_content_count"), { limit: 2000 });

		$("#uf_title_count").text($("#uf_title").val().length);
		$("#uf_summary_count").text($("#uf_summary").val().length);
		$("#uf_content_count").text($("#uf_content").val().length);


		setDatePicker($(".date"));
		$('.date').datepicker("option", "minDate", new Date());

		$("#uf_date_start").dateValidate({
			end: "#uf_date_end",
			onSelect: function () {

			}
		});

		$extPage.campaigns = $.parseJSON($("#hd_campaigns").val());
		$("#CampaignID").change(function () {
			var id = $(this).val();

			if (id == "") {

				$extPage.selected_campaign = null;

				//	$("#campaign_msg").html("");
				$("#goal_amount_msg").html("");
				$("#total_amount_msg").html("");
			} else {

				var entity = $.grep($extPage.campaigns, function (r) {
					return r.CampaignID == id;
				})[0];

				$extPage.selected_campaign = entity;

				//	var startdate = entity.StartDate.substr(0, 10).replace(/-/g, ".")
				//	var enddate = entity.EndDate.substr(0, 10).replace(/-/g, ".")

				//$("#campaign_msg").html(startdate + " ~ " + enddate);
				$("#goal_amount_msg").html((parseInt($("#uf_goal_amount").val()) / 10000).format());
				if ($("#uf_goal_amount").val() == "")
					$("#goal_amount_msg").html("0");
				$("#total_amount_msg").html((parseInt(entity.sf_goal_amount / 10000)).format());
			}
		})

		// 목표금액
		$("#uf_goal_amount").focusout(function () {
			if ($extPage.selected_campaign == null) {
				alert("먼저 후원금 사용처를 선택해 주세요");
				$("#uf_goal_amount").val("");
				$("#CampaignID").focus();
			}
		}).keyup(function () {
			var val = parseInt($(this).val());
			if (val > parseInt($extPage.selected_campaign.sf_goal_amount / 10000)) {
				alert("목표금액은 캠페인 목표금액보다 클 수 없습니다.");
				$("#goal_amount_msg").html("0");
				$(this).val("");
				return;
			}
			$("#goal_amount_msg").html($("#uf_goal_amount").val().format());

		})

		var onCloseStartDate = function (selectedDate) {
			var d = new Date(selectedDate.replace(/-/g, '/'));
			d.setMonth(d.getMonth() + 3);
			d.setDate(d.getDate() - 1);
			$("#uf_date_end").datepicker("option", "maxDate", d);

		};


		$(".date_range").click(function () {
			var month = $(this).data("month");
			var startObj = $("#uf_date_start");
			var endObj = $("#uf_date_end");

			startObj.val($.datepicker.formatDate('yy-mm-dd', new Date()));

			var d = new Date();
			d.setMonth(d.getMonth() + month);
			d.setDate(d.getDate() - 1);
			endObj.val($.datepicker.formatDate('yy-mm-dd', d));

			onCloseStartDate($('#uf_date_start').val());

		})

		if ($("#uf_date_start").val() == "") {
			$($(".date_range")[0]).trigger("click");
		}

		setDatePicker($(".date"));
		$('.date').datepicker("option", "minDate", new Date());
		$('#uf_date_start').datepicker("option", "onClose", onCloseStartDate);

		onCloseStartDate($('#uf_date_start').val());


		$("#uf_date_start").dateValidate({
			end: "#uf_date_end",
			onSelect: function () {
				$extPage.setDateGapComent();
			}
		});



		{

		    var entity = $.parseJSON(cookie.get("cps.app"));

		    if (entity == null) {

		        setTimeout(function () {
		            var uploader = $extPage.attachUploader("btn_uf_image");
		            uploader._settings.data.fileDir = $("#hd_upload_root").val();
		            uploader._settings.data.fileType = "image";
		            uploader._settings.data.limit = 1024;

		            $extPage.updateContentImageData();

		        }, 300);
		    }
		    else {
		        // 나눔펀딩 대표 이미지
		        $extPage.setImageUploader($(".btn_uf_image"), $("#hd_upload_root").val(), "hd_uf_image", 1048576, function (r) {
		            var img = r.data;
		            $("#hd_uf_image").val(img);
		            $("#btn_uf_image").find(".img_guide").hide();
		            $("#btn_uf_image").find(".img_guid2").css({ opacity: 0 });
		            $("#btn_uf_image").css({ "background-image": "url('" + $("#hd_image_domain").val() + $("#hd_uf_image").val() + "')" });
		            if ($("#hd_uf_image").val() != "") {
		                $("#btn_uf_image_del").show();
		            }
		            setTimeout(function () {
		                $("input[name=userfile]").css("display", "block");
		            }, 300);
		        });
		    }


			/*
            var uploader = $extPage.attachUploader("btn_uf_image");
            uploader._settings.data.fileDir = $("#hd_upload_root").val();
            uploader._settings.data.fileType = "image";
            uploader._settings.data.limit = 1024;
            setTimeout(function () {
                $("input[name=userfile]").css("display", "block");
            }, 300);
			*/

		}

		{
		    if (entity == null) {

		        setTimeout(function () {
		            var uploader = $extPage.attachUploader("btn_UserPic");
		            uploader._settings.data.fileDir = $("#hd_userpic_upload_root").val();
		            uploader._settings.data.fileType = "image";
		            uploader._settings.data.limit = 1024;

		            $extPage.updateContentImageData();

		        }, 300);
		    }
		    else {
		        $extPage.setImageUploader($(".hidden_btn_UserPic"), $("#hd_userpic_upload_root").val(), 'hd_user_pic', 1048576, function (r) {
		            var img = r.data;
		            $("#hd_user_pic").val(img);
		            $("#image_UserPic").val(img.replace(/^.*[\\\/]/, ''));

		            $("#btn_UserPic").css({ "background-image": "url('" + $("#hd_image_domain").val() + $("#hd_user_pic").val() + "')" });
		            $("#btn_UserPic").find(".img_guide3").css({ opacity: 0 });
		            $("#image_UserPic_del").show();

		            setTimeout(function () {
		                $("input[name=userfile]").css("display", "block");
		            }, 300);
		        });
		    }

			/*
            var uploader = $extPage.attachUploader("btn_UserPic");
            uploader._settings.data.fileDir = $("#hd_userpic_upload_root").val();
            uploader._settings.data.fileType = "image";
            uploader._settings.data.limit = 1024;
            setTimeout(function () {
                $("input[name=userfile]").css("display", "block");
            }, 300);
			*/

		}

		if ($("#hd_uf_image").val() != "") {
			$("#btn_uf_image").css({ "background-image": "url('" + $("#hd_image_domain").val() + $("#hd_uf_image").val() + "')" });
			//$("#image_uf_image").attr("src", $("#hd_image_domain").val() + $("#hd_uf_image").val());
			//$("#image_uf_image").show();
		}

		if ($("#hd_user_pic").val() != "") {
			$("#image_UserPic").css({ "background-image": "url('" + $("#hd_image_domain").val() + $("#hd_user_pic").val() + "')" });
			$("#btn_uf_image").find("#btn_uf_image_del").show();

		}

	},


	setImageUploader: function (hidden_photo_btn, path, id, limit, cb) {
	    var entity = $.parseJSON(cookie.get("cps.app"));
	    if (entity != null && entity != undefined) {
	        var appDevice = entity.device;
	        var version = $extPage.getAndroidVersion();
	        hidden_photo_btn.click(function () {
	            if (appDevice == "android" && parseFloat(version) == 4.4) {
	                JSInterface.takePhoto("takePictureField", id, path);
	                return false;
	            }
	        });
	    }

		hidden_photo_btn.change(function () {
			if (this.files && this.files[0]) {

				if (this.files[0].size > limit) {
					alert(bytesToSize(limit) + " 이하의 사진만 올릴 수 있습니다.")
					return false;
				}
				var FR = new FileReader();
				FR.onload = function (e) {
					hybrid_api.submitUserImage(path, e.target.result, function (r) {
						if (cb) cb(r);
					});
				};
				FR.readAsDataURL(this.files[0]);
			}
		});
	},

	getAndroidVersion: function (ua) {
		ua = (ua || navigator.userAgent).toLowerCase();
		var match = ua.match(/android\s([0-9\.]*)/);
		return match ? match[1] : false;
	},

	bytesToSize: function (bytes) {
		var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
		if (bytes == 0) return 'n/a';
		var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		if (i == 0) return bytes + ' ' + sizes[i];
		return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
	},

	attachUploader: function (button) {
		return new AjaxUpload(button, {
			action: '/common/handler/upload',
			responseType: 'json',
			onChange: function () {
			},
			onSubmit: function (file, ext) {
				this.disable();
			},
			onComplete: function (file, response) {

				this.enable();

				if (response.success) {
					if (button == "btn_UserPic") {
						$("#hd_user_pic").val(response.name);
						$("#image_UserPic").val(response.name.replace(/^.*[\\\/]/, ''));

						$("#btn_UserPic").css({ "background-image": "url('" + $("#hd_image_domain").val() + $("#hd_user_pic").val() + "')" });
						$("#" + button).find(".img_guide3").css({ opacity: 0 });
						$("#image_UserPic_del").show();

					} else if (button == "btn_uf_image") {
						$("#hd_uf_image").val(response.name);
						$("#btn_uf_image").find(".img_guide").hide();
						$("#" + button).find(".img_guid2").css({ opacity: 0 });
						$("#btn_uf_image").css({ "background-image": "url('" + $("#hd_image_domain").val() + $("#hd_uf_image").val() + "')" });
						if ($("#hd_uf_image").val() != "") {
							$("#btn_uf_image_del").show();
						}

					} else if (button.indexOf("uf_content_image") > -1) {
						$("#" + button).attr("data-url", response.name);
						$("#" + button).find(".img_guide").css({ opacity: 0 });
						$("#btn_uf_image").find("#btn_uf_image_del").show();
						$("#" + button).css({ "background-image": "url('" + $("#hd_image_domain").val() + response.name + "')" });
						$("#uf_content_image_del").show();
						$extPage.updateContentImageData();
					}
				} else
					alert(response.msg);
			}
		});
	},

	updateContentImageData: function () {

		var data = "";
		$.each($(".uf_content_image"), function () {
			var url = $(this).attr("data-url");
			if (url != "") {
				data += "|" + url;
				//	console.log("url>" + url);
			}

		});
		if (data.length > 0) {
			data = data.substring(1);
		}

		$("#hd_uf_content_image").val(data);
		//console.log($(".uf_content_image").length, data);

	},

	updateMovieData: function () {

		var data = "";
		$.each($("input[name=uf_movie]"), function () {
			var val = $(this).val();
			if (val != "") {
				data += "|" + val;
				//	console.log("url>" + url);
			}

		});
		if (data.length > 0) {
			data = data.substring(1);
		}
		$("#hd_uf_movie").val(data);

	},

	// 확인
	onSubmit: function () {

		if ($("#hd_uf_type").val() == "child") {

			if (!validateForm([
				{ id: "#hd_childMasterId", msg: "어린이를 선택해주세요" }
			])) {
				return false;
			}

		} else {
			if (!validateForm([
				{ id: "#CampaignID", msg: "후원금 사용처를 선택해 주세요" }
			])) {
				return false;
			}
		}

		if (!validateForm([
				{ id: "#uf_date_start", msg: "시작일을 선택해 주세요" },
				{ id: "#uf_date_end", msg: "종료일을 선택해 주세요" },
				{ id: "#uf_goal_amount", msg: "목표금액을 입력해 주세요" },
				{ id: "#hd_uf_image", msg: "대표이미지를 선택해 주세요" },
				{ id: "#uf_title", msg: "제목을 입력해 주세요" },
				{ id: "#uf_summary", msg: "한줄설명을 입력해 주세요" },
				{ id: "#uf_content", msg: "상세스토리를 입력해 주세요" },
				{ id: "#hd_user_pic", msg: "개설자의 사진을 선택해주세요" }
		])) {
			return false;
		}

		if (!$page.onSubmit()) {
			return false;
		}

		if (!$("#agree").prop("checked")) {
			alert("나눔펀딩 개설 안내 동의가 필요합니다.");
			return false;
		}


		if ($("#hd_user_pic").val() == "") {
			alert("개설자 사진을 등록해 주세요");
			return false;
		}

		$extPage.updateMovieData();

		$extPage.updateContentImageData();

		loading.show("나눔펀딩 생성중입니다. 서버 상태에 따라 1~3분정도 소요됩니다.");

		return true;
	}


};

var goBack = function () {
	if (location.pathname.indexOf("/sponsor/user/create/") > -1)
		location.href = "/";
	else
		history.back();
};

// 본인인증 결과 응답
// result = Y or N , birth = yyyyMMdd
// sex = M or F
// method = ipin or phone
var cert_setCertResult = function (method, result, ci, di, name, birth, gender, phone) {

	console.log(method, result, ci, di, name, birth, gender, phone);

	if (result != "Y") {
		alert("본인인증에 실패했습니다. 다시 시도해 주세요.");
		return;
	}

    if ($page.cert_target == "me") {

        if ($("#hd_user_name").val() != name) {
            alert("로그인정보와 본인인증정보가 일치하지 않습니다.");
            return;
        }

        checkCI(method, ci, di, gender);
    }
    else {
        $("#msg_parent_cert").show();
        var target = $("#msg_parent_cert")
        var msgObj = $("#msg_parent_cert").find("span");
        target.removeClass("txt-error").addClass("txt-result");

        $(".hide_parent_cert").hide();
        $("#parent_cert").val((method == "ipin" ? "보호자 아이핀 인증" : "보호자 휴대폰 인증"));
        $("#parent_name").val(name);
        $("#parent_juminId").val(birth.substring(2, 7) + "0000000");
        if (phone) $("#parent_mobile").val(phone);

        msgObj.addClass("guide_comment1");
        msgObj.html("보호자 동의 인증이 완료되었습니다.");

    }


};

// 실명인증 결과 응답
var cert_setNameCheckResult = function (result, ci, di, name, jumin, msg) {
	console.log(result, ci, di, name, jumin, msg);
	if (result != "Y") {
		alert(msg);
		return;
	}

	$("#msg_receipt_pub").html("실명 인증이 완료되었습니다.");

	var gender = jumin.substr(6, 1);
	if (gender == 1 || gender == 3 || gender == 5) {
		gender = "M";
	} else {
		gender = "F";
	}
	checkCI("namecheck", ci, di, gender, jumin);
};

var checkCI = function (method, ci, di, gender, jumin) {

	$.post("/api/sponsor.ashx", { t: "sync", ci: ci, di: di, gender: gender, jumin: jumin }, function (r) {

		if (r.success) {

			if (r.action == "login") {
				alert("본인인증된 계정이 이미 존재합니다. \n해당 계정으로 로그인하셔서 진행해 해주십시요. (아이디 : " + r.data.user_id + ")");
				// 로그인창오픈

			} else if (r.action == "reload") {
				// 동기화 완료
				$("#hd_gender").val(gender);
				$("#hd_cert_gb").val(method);
				$("#hd_ci").val(ci);
				$("#hd_di").val(di);
				$("#msg_cert").html("본인 인증이 완료되었습니다.");
				$("#func_cert").hide();
				if (jumin) {
					$("#hd_jumin").val(jumin);
					$("#func_name_check").hide();
				}
			}
		} else {
			alert(r.message);
		}

	});

	return false;

};

(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup, $address) {

		$scope.uf_type = $("#hd_uf_type").val();
		$scope.child = null;

		$scope.children = {

			list: null,
			item: null,
			total: -1,

			params: {
				page: 1,
				rowsPerPage: 5
			},

			getList: function (params) {

				$scope.children.params = $.extend($scope.children.params, params);
				console.log($scope.children.params);
			    //[이종진]로딩추가
				loading.hide();
				$http.get("/api/tcpt.ashx?t=user-funding", { params: $scope.children.params }).success(function (r) {
					console.log(r);
					if (r.success) {

						var list = r.data;

						$.each(list, function () {
							this.birthdate = new Date(this.birthdate);

						});

						$scope.children.list = list;
						if (r.data.length > 0) {
							$scope.children.total = r.data[0].total;
						}

						// 어린이가 로딩되고 나서 이벤트 적용
						// 로딩되기전에 이벤트가적용되면 사진 업로드 창이 뜨는경우가 발생
						if (!params) {
							$extPage.init();
						}

					} else {
						alert(r.message);
					}
				    //[이종진]로딩추가
					loading.hide();
				});
			    

			},

			select: function ($event, item) {
			    //	$event.preventDefault();

			    loading.show();
			    //[이종진] 선택 시, Global Pool에서 Beneficuary Kit을 조회
			    //존재하지 않다면, 선택불가
			    if (item == null) {
			        alert("선택 어린이가 잘못되었습니다");
			        return;
			    }

			    $http.get("/api/tcpt.ashx?t=user-funding-select", { params: item }).success(function (r) {
			        console.log(r);
			        //기존방법조회 시 선택 한 정보를 그대로 넣음(필요한 정보가 모두 있기 때문)
			        if (r.data == "db") {
			            $scope.children.item = item;
			        }
			            //신규방법조회 성공
			        else if (r.success) {
			            //성공 시
			            $scope.children.item = r.data;
			        }
			        else {
			            //실패
			            if (confirm("해당 어린이는 현재 후원할 수 없습니다.\r\n목록을 새로 조회 하시겠습니까?\r\n(예 : 어린이목록 재조회, 아니오 : 어린이목록 유지)")) {
			                $scope.page = $scope.page + 1;
			                $scope.children.getList();
			            }
			        }
			        loading.hide();
			    });

			    //$scope.children.item = item;
			    //console.log(item);
			    //	$event.preventDefault();
			},

			cancel: function ($event) {
				$event.preventDefault();
				$scope.children.item = null;
				$scope.child = null;
				$("input[name=child]").prop("checked", false);
			},

			confirm: function ($event) {
				$event.preventDefault();
				$("#hd_childMasterId").val($scope.children.item.childmasterid);
				$("#hd_childName").val($scope.children.item.name);
				$("#hd_childKey").val($scope.children.item.childkey);
				$("#uf_goal_amount").val($scope.children.item.remainamount);
				//alert($scope.children.item.remainamount)
				$scope.child = $.extend({}, $scope.children.item);

				setTimeout(function () {
					if ($("#selected_child").length > 0)
						scrollTo($("#selected_child"), 30);
				}, 300);


			}

		};

		$scope.changeType = function ($event, arg) {
			$event.preventDefault();
			$scope.children.item = null;
			$("#uf_goal_amount").val("");
			$scope.uf_type = arg;
			$("#hd_uf_type").val(arg);
		}

		$scope.children.getList();

		// 어린이 상세보기 팝업

		$scope.showChildPop = function ($event, item) {
			loading.show();
			if ($event) $event.preventDefault();
			popup.init($scope, "/common/child/pop/" + item.countrycode + "/" + item.childmasterid + "/" + item.childkey + "?fn=hide", function (modal) {
				modal.show();

				initChildPop($http, $scope, modal, item);

			}, { top: 0, iscroll: true, removeWhenClose: true });
		}


		// form
		$scope.form = {

			init: function () {
				$scope.form.movie.init();
				$scope.form.image.init();
			},

			movie: {
				list: [],

				init: function () {

					var val = $("#hd_uf_movie").val();
					if (val == "") {
						$scope.form.movie.list.push({ index: $scope.form.movie.list.length, val: "" });
					} else {
						for (i = 0 ; i < val.split('|').length ; i++) {
							var m = val.split('|')[i];
							if (m == "") continue;

							$scope.form.movie.list.push({ index: $scope.form.movie.list.length, val: m });
						}
					}

				},

				control: function ($event, $index) {
					console.log($index);
					if ($index > -1) {	// 삭제
						console.log($scope.form.movie.list);
						$scope.form.movie.list.splice($index, 1);
					} else {

						$scope.form.movie.list.push({ index: $scope.form.movie.list.length, val: "" });
					}
				}
			},

			image: {
				list: [],

				init: function () {

					var val = $("#hd_uf_content_image").val();
					if (val == "") {
						var index = $scope.form.image.list.length;
						$scope.form.image.list.push({ index: index, val: "" });
						$scope.form.image.setUploadEvent(index);

					} else {
						for (i = 0 ; i < val.split('|').length ; i++) {
							var m = val.split('|')[i];
							if (m == "") continue;

							var index = $scope.form.image.list.length;
							$scope.form.image.list.push({ index: index, val: m });
							$scope.form.image.setUploadEvent(index);
						}
					}

				},

				control: function ($event, $index) {
					$event.preventDefault();
					//console.log($index);
					if ($index > -1) {	// 삭제
						//console.log($scope.form.image.list);
						$scope.form.image.list.splice($index, 1);
					} else {

						var index = $scope.form.image.list.length;
						$scope.form.image.list.push({ index: index, val: "" });
						$scope.form.image.setUploadEvent(index);
					}

				},

				setUploadEvent: function (index) {


				    var entity = $.parseJSON(cookie.get("cps.app"));
                    
				    if (entity == null) {

				        setTimeout(function () {
				            var uploader = $extPage.attachUploader("uf_content_image_" + index);
				            uploader._settings.data.fileDir = $("#hd_upload_root").val();
				            uploader._settings.data.fileType = "image";
				            uploader._settings.data.limit = 1024;

				            $extPage.updateContentImageData();

				        }, 300);
				    }
				    else {

				        setTimeout(function () {
				            $extPage.setImageUploader($(".hidden_uf_content_image_" + index), $("#hd_upload_root").val(), 'uf_content_image_' + index, 1048576, function (r) {
				                var img = r.data;
				                $("input[name=userfile]").css("display", "block");
				                $("#uf_content_image_" + index).attr("data-url", img);
				                $("#uf_content_image_" + index).find(".img_guide").css({ opacity: 0 });
				                $("#btn_uf_image").find("#btn_uf_image_del").show();
				                $("#uf_content_image_" + index).css({ "background-image": "url('" + $("#hd_image_domain").val() + img + "')" });
				                $("#uf_content_image_del").show();
				                $extPage.updateContentImageData();
				            });
				        }, 300);
				    }

					///*
                    //setTimeout(function () {
                    //    var uploader = $extPage.attachUploader("uf_content_image_" + index);
                    //    uploader._settings.data.fileDir = $("#hd_upload_root").val();
                    //    uploader._settings.data.fileType = "image";
                    //    uploader._settings.data.limit = 1024;
                    //    setTimeout(function () {
                    //        $("input[name=userfile]").css("display", "block");
                    //    }, 300);


                    //    $extPage.updateContentImageData();

                    //}, 300);
					//*/
				}
			}

		};

		$scope.form.init();

		//교회찾기
		$scope.findOrganization = function ($event) {

			$event.preventDefault();

			url = "/common/popup/organization";
			popup.init($scope, url, function (modal) {
				initOrganization($scope, $http, modal, function (id, name) {		// callback
					$("#hdOrganizationID").val(id);
					$("#church_name").val(name);
				});

			}, { top: 0, iscroll: true, removeWhenClose: true });

		}


		// 주소찾기
		// addressApiKey 제거
		$scope.findAddr = function ($event) {
			$event.preventDefault();

			$scope.scrollTop = $(document).scrollTop();
			$(".wrap-sectionsub").hide();

			popup.init($scope, "/common/popup/address", function (modal) {
				initAddress($scope, $address, modal, function (zipcode, addr1, addr2, jibun) {		// callback


					$("#addr_domestic_zipcode").val(zipcode);
					$("#addr_domestic_addr1").val(addr1 + "//" + jibun);
					$("#addr_domestic_addr2").val(addr2);

					// 화면에 표시
					$("#addr_domestic_1").val(zipcode);
					//$("#addr_domestic_2").val(addr2);

					$("#addr_road").text("[도로명주소] " + addr1 + " " + addr2);
					$("#addr_jibun").text("[지번주소] " + jibun + " " + addr2);

				}, function () {
					$(".wrap-sectionsub").show();
					window.scroll(0, $scope.scrollTop)
				});

			}, { top: 0, iscroll: true, removeWhenClose: true });

		}

		$scope.cmsAgreeInstance = null;
		$scope.showCMSAgree = function ($event) {
			$event.preventDefault();

			popup.init($scope, "/sponsor/pay/regular/pop-cms-agree.html", function (modal) {

				$scope.cmsAgreeInstance = modal;
				modal.show();

			}, { removeWhenClose: true });

		}

		$scope.modal = {

			instance: null,
			show: function ($event) {
				$event.preventDefault();

				popup.init($scope, "/sponsor/user-funding/guide", function (modal) {
					$scope.modal.instance = modal;

					modal.show();

				}, { top: 0, iscroll: true, removeWhenClose: true });


			},



			close: function ($event) {
				$event.preventDefault();
				if (!$scope.modal.instance)
					return;
				$scope.modal.instance.hide();

			},
		}
	});

})();

function showLoading() {
    //loading.show("나눔펀딩 처리 중입니다. 서버 상태에 따라 1~3분 정도 소요됩니다. 잠시만 기다려주세요.");
}