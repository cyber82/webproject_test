﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="create.aspx.cs" Inherits="sponsor_user_funding_create" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>

    <script type="text/javascript" src="/sponsor/pay/regular/default.js"></script>
    <script type="text/javascript" src="/sponsor/user-funding/create.js?v=1.3"></script>

    <script type="text/javascript" src="/common/js/site/motive.js"></script>

    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
	<style>
		.ui-datepicker select.ui-datepicker-year {width: 60%;}
	</style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">


    <input type="hidden" id="hd_upload_root" runat="server" />
    <input type="hidden" id="hd_userpic_upload_root" runat="server" />
    <input type="hidden" id="hd_image_domain" runat="server" />
    <input type="hidden" id="hd_campaigns" runat="server" value="[]" />
    <input type="hidden" id="hd_uf_image" runat="server" value="" />
    <input type="hidden" id="hd_user_pic" runat="server" value="" />
    <input type="hidden" id="hd_childMasterId" runat="server" value="" />
    <input type="hidden" id="hd_childName" runat="server" value="" />
    <input type="hidden" id="hd_childKey" runat="server" value="" />
    <input type="hidden" id="hd_uf_type" runat="server" value="child" />

    <input type="hidden" id="hd_uf_movie" runat="server" value="" />
    <input type="hidden" id="hd_uf_content_image" runat="server" value="" />

    <input type="hidden" id="user_name" runat="server" />
    <input type="hidden" id="gender" runat="server" />
    <input type="hidden" id="hd_auth_domain" runat="server" />
    <input type="hidden" id="hd_age" runat="server" />
    <input type="hidden" runat="server" id="jumin" value="" />
    <input type="hidden" runat="server" id="ci" value="" />
    <!-- 본인인증 CI -->
    <input type="hidden" runat="server" id="di" value="" />
    <!-- 본인인증 DI -->
    <input type="hidden" runat="server" id="cert_gb" value="" />
    <!-- 본인인증 수단 -->

    <input type="hidden" runat="server" id="user_class" value="20세이상" />
    <input type="hidden" runat="server" id="parent_cert" value="" />
    <input type="hidden" runat="server" id="parent_name" value="" />
    <input type="hidden" runat="server" id="parent_juminId" value="" />
    <input type="hidden" runat="server" id="parent_mobile" value="" />
    <input type="hidden" runat="server" id="parent_email" value="" />

    <input type="hidden" runat="server" id="exist_account" value="" />
    <input type="hidden" runat="server" id="hdManageType" value="" />
    <input type="hidden" runat="server" id="hdState" value="" />
    <input type="hidden" runat="server" id="hfAddressType" value="" />
    <input type="hidden" runat="server" id="hdLocation" value="" />
    <input type="hidden" runat="server" id="hdOrganizationID" value="" />
    <input type="hidden" runat="server" id="hdSponsorId" value="" />
    <input type="hidden" runat="server" id="hdBirthDate" value="" />
    <input type="hidden" runat="server" id="zipcode" />
    <input type="hidden" runat="server" id="addr1" />
    <input type="hidden" runat="server" id="addr2" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
    <input type="hidden" runat="server" id="hdCI" value="" />
    <!-- 기존 등록된 본인인증 CI -->
    <input type="hidden" runat="server" id="dmYN" value="Y" />
    <input type="hidden" runat="server" id="translationYN" value="N" />

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <div class="member-join sponsor-funding" id="sponsor_funding">

            <!--메뉴2개  toggle (흑백되는 부분에 "grayscale"  css추가)-->

            <div class="bg-gray2 sectionsub-margin1">
                <p class="txt-caption">어떤 <span class="color1">나눔펀딩</span>을 만들고 싶으세요?</p>
                    
                <a class="funding-child1" ng-class="{'grayscale':uf_type != 'child'}" ng-click="changeType($event,'child')"><span class="txt">1:1어린이양육을 위한<br />
                    나눔펀딩을 만들고 싶어요</span><span class="dim-fundchild"></span></a>
                    
                <a class="funding-child2 mt10 grayscale" ng-class="{'grayscale':uf_type != 'normal'}" ng-click="changeType($event,'normal')"><span class="txt">금액을 목표로 양육을 돕는<br />
                    나눔펀딩을 만들고 싶어요</span><span class="dim-fundchild"></span></a>
                <div class="wrap-bt mt20"><a class="bt-type6" style="width: 100%" href="#" ng-click="modal.show($event);">나눔펀딩 더 자세히 알기</a></div>
            </div>
            <!-- 어린이 나눔펀딩 선택시-->
            <p class="txt-title" ng-show="uf_type == 'child' && children.list">어린이 선택</p>

            <ul class="list-nobullet" ng-show="uf_type == 'child' && (children.list == null || children.list.length == 0)">
                    <li>현재 나눔펀딩으로 개설 할 수 있는 어린이가 없습니다.</li>
                </ul>
            <ul class="list-childselect" ng-show="uf_type == 'child' && children.list">

                <li ng-show="child == null" ng-class="{'padding1' : $index == 0}" ng-repeat="item in children.list">
                    <span class="wrap-caption">
                        <span class="txt-clock">{{item.waitingdays}}일</span>
                        <span class="radio2-ui">
                            <input type="radio" id="sel_child_{{$index}}" name="child" class="css-radio2" ng-click="children.select($event , item)" />
                            <label for="sel_child_{{$index}}" class="css-label2">선택</label>
                        </span>
                    </span>
                    <span class="pos-photo" ng-click="showChildPop($event , item)"><span class="photo" style="background: url('{{item.pic}}') no-repeat"></span></span>
                    <span class="txt-name" ng-click="showChildPop($event , item)">{{item.namekr}}</span>
                    <span class="txt-info" ng-click="showChildPop($event , item)">국가 : <span class="color1">{{item.countryname}}</span><br />
                        생일 : <span class="color1">{{item.birthdate | date:'yyyy.MM.dd'}} ({{item.age}}세)</span><br />
                        성별 : <span class="color1">{{item.gender}}</span>
                    </span>
                </li>


                <!--선택된 어린이 -->
                <li id="selected_child" ng-show="child != null">
                    <span class="wrap-caption">
                        <span class="txt-clock">{{child.waitingdays}}일</span>
                        <span class="delete" ng-click="children.cancel($event);">
                            <img src="/common/img/btn/delete.png" alt="삭제" /></span>
                    </span>
                    <span class="pos-photo"><span class="photo" style="background: url('{{child.pic}}') no-repeat"></span></span>
                    <span class="txt-name">{{child.namekr}}</span>
                    <span class="txt-info">국가 : <span class="color1">{{child.countryname}}</span><br />
                        생일 : <span class="color1">{{child.birthdate | date:'yyyy.MM.dd'}} ({{child.age}}세)</span><br />
                        성별 : <span class="color1">{{child.gender}}</span>
                    </span>
                </li>
            </ul>

            <!--리스트 없을시 안나옴  -->
            <paging class="small" ng-show="uf_type == 'child' && child == null" page="children.params.page" page-size="children.params.rowsPerPage" total="children.total" show-prev-next="true" show-first-last="true" paging-action="children.getList({page : page})"></paging>

            <p class="txt-childselect" ng-show="children.item">
                선택하신 어린이가 성인이 될 때까지 필요한<br />
                예상 금액은 <em>{{children.item.remainamount | number:N0}}원</em> 입니다.<br />
                이 금액으로 나눔펀딩을 진행합니다. 
            </p>


            <!--리스트 있을 경우 나옴-->
            <div class="wrap-bt mt20" ng-show="children.item && child == null"><a class="bt-type6" style="width: 100%" ng-click="children.confirm($event)">이 어린이의 결연 펀딩을 만들래요</a></div>


            <!-- 어린이 나눔펀딩 선택시-->

            <!-- 금액목표 나눔펀딩 선택시 -->
            <p class="txt-title" ng-show="uf_type == 'normal'">후원금 사용처</p>
            <fieldset class="frm-input" ng-show="uf_type == 'normal'">
                <legend>후원금 사용처 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row">
                            <asp:DropDownList runat="server" ID="CampaignID" Style="width: 100%"></asp:DropDownList>
                        </span>
                    </div>
                </div>
            </fieldset>

            <p class="txt-title">나눔펀딩 모금기간</p>
            <fieldset class="frm-input">
                <legend>나눔펀딩 모금기간 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row">
                            <span class="radio-ui">
                                <input type="radio" id="period1" name="chk_radio" class="css-radio date_range" data-day="0" data-month="1" checked="checked" />
                                <label for="period1" class="css-label">1개월</label>
                            </span>&nbsp;&nbsp;
                        <span class="radio-ui">
                            <input type="radio" id="period2" name="chk_radio" class="css-radio date_range" data-day="0" data-month="2" />
                            <label for="period2" class="css-label">2개월</label>
                        </span>&nbsp;&nbsp;
                        <span class="radio-ui">
                            <input type="radio" id="period3" name="chk_radio" class="css-radio date_range" data-day="0" data-month="3" />
                            <label for="period3" class="css-label">3개월</label>
                        </span>


                        </span>
                        <span class="row">

                            <span class="box-date" style="width: 47%">
                                <input type="text" placeholder="시작일" style="width: 100%" id="uf_date_start" runat="server" class="date" readonly />
                                <span class="cal" onclick="$('#uf_date_start').trigger('focus');return false;">달력보기</span>
                            </span>

                            <span class="box-gap" style="width: 6%">&sim;</span>

                            <span class="box-date" style="width: 47%">
                                <input type="text" placeholder="종료일" style="width: 100%" id="uf_date_end" runat="server" class="date" readonly />
                                <span class="cal" onclick="$('#uf_date_end').trigger('focus');return false;">달력보기</span>
                            </span>

                        </span>
                        <span class="txt-error" id="date_comment" style="display: none">모금 기간은 최소 1개월 이상 최대 3개월 까지만 설정 가능합니다.</span>
                        <!--에러메시지 경우에만 출력-->

                    </div>
                </div>
            </fieldset>


            <p class="txt-title" ng-show="uf_type == 'normal'">목표금액</p>
            <fieldset class="frm-input" ng-show="uf_type == 'normal'">
                <legend>목표금액 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row pos-unit">
                            <input type="text" value="" placeholder="목표금액 " style="width: 100%" runat="server" id="uf_goal_amount" maxlength="4" class="number_only" />
                            <span class="txt">만원</span>
                        </span>
                        <span class="txt-error minamount" style="display: none">모금 금액은 1만원 이상 입력 가능합니다.</span>
                        <!--에러메시지 경우에만 출력-->
                    </div>
                </div>
            </fieldset>
            <div class="total-targetamount mt20" ng-show="uf_type == 'normal'">
                선택된 캠페인의 모금액
            <span class="txt-amount"><em><span id="goal_amount_msg">0</span></em> / <span id="total_amount_msg">0</span>만원</span>
            </div>
            <!-- 금액목표 나눔펀딩 선택시 -->


            <p class="txt-title">나눔펀딩 대표 이미지</p>
            <div class="reg-fundimg" id="btn_uf_image" style="background-repeat: no-repeat; background-position: center top;">
                <input type="file" accept="image/*" class="hidden_btn_uf_image" style="width:280px;height:150px;top:0px;left:0px;position:absolute;opacity:0.0;background:red;z-index:10">
                <a class="img_guid2">
                    <strong>대표 이미지를 등록해 주세요.</strong>
                    최대 1MB 첨부가능 / JPEG,JPG,PNG,GIF<br />
                    1280*960이상 / 4:3비율권장
                </a>

                <!-- 이미지 등록 시, 아래 <button>태그 노출 (디폴트는 display:none;) -->
                <button class="fundimg-del" id="btn_uf_image_del" style="z-index: 3147483467"><span>삭제</span></button>
            </div>


            <p class="txt-stitle mt30">추가 영상(선택사항)<a class="add-file" ng-hide="form.movie.list.length > 2" ng-click="form.movie.control($event , -1)"><img src="/common/img/btn/fileplus.png" alt="추가" /></a></p>
            <fieldset class="frm-input">
                <legend>유투브 경로입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row" ng-repeat="item in form.movie.list">


                            <span class="row pos-filebtn">
                                <input type="text" id="youtube_url_{{$index}}" name="uf_movie" value="{{item.val}}" placeholder="유투브 등에 올라가있는 대표 영상 URL을 올려주세요." style="width: 100%" />
                                <a class="minus-file" ng-hide="$index == 0 && form.movie.list.length > 2" class="regist_img" ng-click="form.movie.control($event , $index)">
                                    <img ng-src="/common/img/btn/fileminus.png" ng-show="$index > 0" alt="삭제" />

                                </a></span>
                        </span>


                    </div>
                </div>
            </fieldset>



            <div class="box-gray mt20">
                <strong style="padding-bottom: 5px"><span class="color1">유투브 영상 업로드 안내</span></strong>
                <ul class="list-nobullet">
                    <li>공유하실 유투브 영상에서 공유하기 버튼을 클릭하신 뒤 https://youtu.be/compassion과 같은 주소를 복사하여 붙여 넣어주세요.</li>
                </ul>
            </div>

            <p class="txt-title">나눔펀딩 주요 정보</p>
            <fieldset class="frm-input">
                <legend>나눔펀딩 주요 정보 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row pos-byte1">
                            <input type="text" id="uf_title" runat="server" maxlength="20" placeholder="나눔펀딩 제목 20자" style="width: 100%" />
                            <span class="txt-byte"><em><span id="uf_title_count">0</span></em>/20</span></span>
                        <span class="row pos-byte2">
                            <textarea id="uf_summary" runat="server" maxlength="40" style="width: 100%; height: 60px" placeholder="나눔펀딩의 목적, 다루고자하는 내용 한줄설명 40자"></textarea><span class="txt-byte"><em><span id="uf_summary_count">0</span></em>/40</span></span>
                    </div>
                </div>
            </fieldset>

            <p class="txt-title">나눔펀딩 상세 스토리</p>
            <fieldset class="frm-input">
                <legend>나눔펀딩 상세 스토리 정보 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <div class="frm-row box mt0">
                            <textarea id="uf_content" runat="server" style="width: 100%; height: 200px" placeholder="프로젝트 내용 2000자"></textarea>
                            <p class="txt-byte"><em><span id="uf_content_count">0</span></em>/2000자</p>
                        </div>
                    </div>
                </div>
            </fieldset>

            <p class="txt-stitle mt20">
                본문 이미지 (선택사항)   <a class="add-file" ng-hide="form.image.list.length > 4" ng-click="form.image.control($event , -1)">

                    <img ng-src="/common/img/btn/fileplus.png" alt="등록" />
                </a>
            </p>
            <!-- -->
            <div class="reg-fundimg uf_content_image" ng-repeat="item in form.image.list" style="background: no-repeat center top; margin-bottom: 5px" data-url="{{item.val}}" id="uf_content_image_{{$index}}">
                <input type="file" accept="image/*" class="hidden_uf_content_image_{{$index}}" style="width:280px;height:150px;top:0px;left:0px;position:absolute;opacity:0.0;background:red;z-index:10">
                <a class="minus-file" ng-hide="$index == 0" style="z-index: 6000000000" ng-click="form.image.control($event , $index)">
                    <img ng-src="/common/img/btn/fileminus.png" alt="삭제" />
                </a>
                <a class="img_guide">
                    <strong>이미지를 등록해 주세요.</strong>
                    <p class="bu">최대 1MB 첨부가능 / JPEG,JPG,PNG,GIF</p>
                </a>

				<!-- 첫번째 본문이미지만 아래 <button>태그 노출 (디폴트는 display:none;) -->
				<button class="fundconimg-del" ng-if="$first" id="uf_content_image_del" style="z-index: 3147483583"><span>삭제</span></button>
            </div>

            <div class="wrap-bt"><a class="bt-type6" style="width: 100%" id="next_page">다음</a></div>

        </div>

        <!--//-->

        <!-- 후원자 정보 -->

        <div class="member-join sponsor-funding" style="display: none" id="sponsor_info" style="background: no-repeat center;">
            <p class="txt-title">개설자 이미지</p>
            <input type="hidden" id="image_UserPic" runat="server" />
            <div class="reg-fundimg" id="btn_UserPic" style="background: no-repeat center top;">
                <input type="file" accept="image/*" class="hidden_btn_UserPic" style="width:280px;height:150px;top:0px;left:0px;position:absolute;opacity:0.0;background:red;z-index:10">
                <a class="img_guide3">
                    <strong>개설자 사진 등록해 주세요.</strong>
                    최대 1MB 첨부가능 / JPEG,JPG,PNG,GIF<br />
                    정비율권장
                </a>
                <button class="fundimg-del" id="image_UserPic_del" style="z-index: 3147483467;"><span>삭제</span></button>
            </div>
            <!-- 사진입력 -->




            <p class="txt-title">본인인증</p>
            <!--해외거주여부-->
            <div class="txt-inhabit">
                <span class="checkbox-ui">
                    <input type="checkbox" class="css-checkbox" id="addr_overseas" runat="server" />
                    <label for="addr_overseas" class="css-label">해외에 거주하고 있어요 </label>
                </span>
            </div>

            <div runat="server" id="ph_cert_me">
                <div >
                    <!--국세청 영수증 신청 영역-->

                    <asp:PlaceHolder runat="server" ID="ph_no_jumin" Visible="false">
                        <fieldset class="frm-input mt10">
                            <legend>주민등록번호 입력</legend>
                            <div class="row-table">
                                <div class="col-td" style="width: 100%">
                                    <span class="row">
                                        <span class="checkbox-ui">
                                            <input type="checkbox" class="css-checkbox" id="p_receipt_pub_ok" runat="server" checked />
                                            <label for="p_receipt_pub_ok" class="css-label">국세청 연말정산 영수증 신청</label>
                                        </span>
                                    </span>
                                    <span class="row pos-relative2" id="func_name_check">
                                        <input type="text" class="number_only" value="" data-id="jumin1" runat="server" id="jumin1" maxlength="13" placeholder="주민등록번호" style="width: 100%" /><a class="bt-type9 pos-btn" style="width: 70px" href="#" id="btn_name_check">실명인증</a></span>
                                    <p class="txt-result" id="msg_name_check" style="display: none"><span class="guide_comment1"></span></p>
                                </div>
                            </div>
                        </fieldset>
                    </asp:PlaceHolder>

                    <!--기존에 이미 실명인증했고, 영수증을 신청한 회원의 경우 국세청영수증 영역 대신 출력-->
                    <asp:PlaceHolder runat="server" ID="ph_yes_jumin" Visible="false">
                        <div class="msg-graybox mt10">
                            <strong>기존에 등록하신<br />
                                <span class="color1">국세청 연말정산 영수증 신청 정보</span>가 있습니다.</strong><br />
                            기존 정보에 연말정산 정보를 추가로 등록합니다. 
                        </div>
                    </asp:PlaceHolder>

					<div class="hide_overseas">
                    <!--본인인증 영역.(최초 1회만) 실명인증 시 비노출-->
                    <asp:PlaceHolder runat="server" ID="ph_cert" Visible="false">
                        <p class="txt-sub mt20">온/오프라인 기존 후원정보 확인을 위해 본인 인증을 진행해 주세요.</p>
                        <div class="self-account func_cert">
                            <div class="link-account">
                                <a href="#" id="btn_cert_by_phone"><span>휴대폰 인증</span></a>
                                <a href="#" id="btn_cert_by_ipin"><span>아이핀(i-PIN) 인증</span></a>
                            </div>
                            <div class="box-gray">
                                <ul class="list-bullet2">
                                    <li>본인명의의 휴대폰으로만 본인인증이 가능합니다.<br />
                                        아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다. </li>
                                </ul>
                            </div>
                        </div>
                        <p class="txt-result" id="msg_cert" style="display: none">본인 인증이 완료되었습니다.</p>
                    </asp:PlaceHolder>
					</div>
                </div>
            </div>

            <!--만 19세 미만의 경우 보호자 동의영역 추가 출력-->
			<asp:PlaceHolder runat="server" ID="ph_parent_cert" Visible="false" >
			<p class="txt-title">보호자동의</p>
			<p class="txt1 align-left" style="padding:0">만 14세 미만은 법률에 의거하여 보호자(법적대리인)의 동의가 필요합니다.</p>
			<div class="self-account hide_parent_cert">
				<div class="link-account">
            		<a href="#" id="btn_parent_cert_by_phone"><span>휴대폰 인증</span></a>
					<a href="#" id="btn_parent_cert_by_ipin"><span>아이핀(i-PIN) 인증</span></a>
				</div>
			</div>
			
			<p class="txt-result" id="msg_parent_cert" style="display:none"><span class="guide_comment1">보호자 동의 인증이 완료되었습니다.</span></p>
			</asp:PlaceHolder>

            <p class="txt-title mt20">영문이름</p>
            <fieldset class="frm-input mt10">
                <legend>영문이름정보 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row">
                            <input type="text" maxlength="20" runat="server" id="last_name" placeholder="영문성" style="width: 49%" class="fl" />
                            <input type="text" maxlength="20" runat="server" id="first_name" placeholder="영문이름" style="width: 49%" class="fr" /></span>
                    </div>
                </div>
            </fieldset>

            <p class="txt-title">종교</p>
            <fieldset class="frm-input">
                <legend>종교 정보 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <!--기독교 선택시 교회명 영역 추가 로 나타남-->
                        <span class="row">
                            <asp:DropDownList Style="width: 100%" runat="server" ID="religion">
                                <asp:ListItem Text="기독교" Value="기독교"></asp:ListItem>
                                <asp:ListItem Text="천주교" Value="천주교"></asp:ListItem>
                                <asp:ListItem Text="불교" Value="불교"></asp:ListItem>
                                <asp:ListItem Text="없음" Value="무교"></asp:ListItem>
                            </asp:DropDownList>

                        </span>
                        <span class="row pos-relative4" id="pn_church" runat="server"><a class="bt-type8 pos-btn" style="width: 70px" ng-click="findOrganization($event)">교회찾기</a>
                            <input type="text" value="" placeholder="교회명 직접입력" style="width: 100%" runat="server" id="church_name" maxlength="30" /></span>
                    </div>
                </div>
            </fieldset>

            <p class="txt-title" style="display: none;" id="title_addr">주소</p>
            <input type="hidden" id="addr_domestic_zipcode" />
            <input type="hidden" id="addr_domestic_addr1" />
            <input type="hidden" id="addr_domestic_addr2" />

            <div id="pn_addr_domestic" runat="server" style="display: none; width: 100%">
                <fieldset class="frm-input">
                    <legend>주소 정보 입력</legend>
                    <div class="row-table">
                        <div class="col-td" style="width: 100%">
                            <span class="row pos-relative2">
                                <input type="text" value="" placeholder="주소" style="width: 100%" id="addr_domestic_1" runat="server" readonly="readonly" />
                                <a class="bt-type8 pos-btn" style="width: 70px" ng-click="findAddr($event)">주소찾기</a>
                            </span>
                            <!-- 
							<span class="row">
								<input type="text" value="" placeholder="상세주소" style="width: 100%" id="addr_domestic_2" runat="server" readonly="readonly" />
                            </span>
							-->
                            <span id="addr_road" class="row fs13 mt15"></span>
                            <span id="addr_jibun" class="row fs13"></span>

                        </div>
                    </div>
                </fieldset>
            </div>
            <!--해외 선택시 국내 주소 입력란 대신 해외주소 입력 영역 노출 -->
            <div id="pn_addr_overseas" runat="server" style="display: none">
                <p class="txt-title">주소(해외)</p>
                <fieldset class="frm-input">
                    <legend>주소(해외) 정보 입력</legend>
                    <div class="row-table">
                        <div class="col-td" style="width: 100%">
                            <span class="row">
                                <asp:DropDownList runat="server" ID="ddlHouseCountry" Style="width: 49%" class="fl"></asp:DropDownList>

                                <input type="text" value="" placeholder="우편번호" style="width: 49%" class="fr" id="addr_overseas_zipcode" maxlength="10" />
                            </span>
                            <span class="row">
                                <input type="text" value="" placeholder="주소" style="width: 100%" id="addr_overseas_addr1" maxlength="100" /></span>
                            <span class="row">
                                <input type="text" value="" placeholder="주소" style="width: 100%" id="addr_overseas_addr2" maxlength="100" /></span>
                        </div>
                    </div>
                </fieldset>
            </div>


            <div class="hide_overseas">
                <p class="txt-title">후원정보 수신</p>
                <ul class="list-bullet2 mt20">
                    <li>입력하신 주소로 후원과 관련한 우편물을 수신하시겠습니까?</li>
                    <li>단, 후원과 관련하여 후원자님께 중요하게 안내되어야 하는 우편물은 수신 여부와 상관없이 발송됩니다.</li>
                </ul>
                <div class="tab-column-2 mt15">
                    <a href="#" class="dmYN" data-value="Y">예</a>
                    <a href="#" class="dmYN" data-value="N">아니오</a>
                </div>
            </div>

            <div ng-show="uf_type == 'child'">
                <p class="txt-title">어린이 편지 번역 여부</p>
                <div class="tab-column-2">
                    <a href="#" class="translationYN" data-value="N">영문</a>
                    <a href="#" class="translationYN" data-value="Y">한글</a>
                </div>
                <ul class="list-bullet2 mt20">
                    <li>영문으로 선택하시면 어린이편지를 더 빨리 받으실 수 있습니다.</li>
                    <li>어린이에게 편지를 보내실 때는 위 선택과 관계없이 한글 또는 영문으로 작성하실 수 있습니다.</li>
                </ul>
            </div>

            <p class="txt-title">후원계기</p>
            <input type="hidden" runat="server" id="motiveCode" />
            <input type="hidden" runat="server" id="motiveName" />
            <fieldset class="frm-input">
                <legend>후원계기 정보 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row">
                            <select style="width: 100%" id="motive1"></select></span>
                        <span class="row">
                            <select style="width: 100%" id="motive2">
                                <option value="">선택하세요</option>
                            </select></span>
						<span class="row" id="pn_motive2_etc" style="display:none"><input type="text" style="width:100%" id="motive2_etc" placeholder="기타 후원계기를 입력해주세요" maxlength="30" ></input></span>
                    </div>
                </div>
            </fieldset>

            <p class="txt-title"><span class="color1">나눔펀딩 개설 안내 동의</span></p>
            <div class="agree-nanumfund">
                <ul>
                    <li>등록된 나눔펀딩은 수정/삭제가 되지 않습니다. 등록 버튼을 누르기 전에 꼭 확인해주세요.</li>
                    <li>나눔펀딩으로 모금된 후원금의 집행은 한국컴패션에서 진행됩니다.</li>
                    <li>목표금액을 달성하지 못한 모금액과 목표금액을 초과한 모금액 모두 현재 모금중인 캠페인에 기부됩니다. (1:1어린이 양육을 위한 모금일 경우 해당 부분은 후원자님을 만나지 못한 어린이를 위한 후원금으로 사용됩니다.)</li>
                    <li>1:1어린이양육을 위한 목표금액이 달성되면 일주일 이내에 개설자가 어린이의 후원자로 등록되어 편지쓰기, 선물하기등이 가능하며 후원 취소는 하실 수 없습니다.</li>
                    <li>나눔펀딩으로 1:1 후원하게 된 후원어린이가 어린이의 사정으로 컴패션을 떠나게 되는 경우, 잔여 모금액은 후원자님을 만나지 못한 어린이를 위한 후원금으로 사용됩니다.</li>
                    <li>모금중이신 나눔펀딩이 있으실 경우 추가 혹은 중복으로 개설하실 수 없습니다.</li>
                </ul>
                <div class="txt-agree">
                    <span class="checkbox-ui">
                        <input type="checkbox" id="agree" class="css-checkbox" />
                        <label for="agree" class="css-label">위 내용에 동의합니다.</label>
                    </span>
                </div>
            </div>


            <div class="wrap-bt">
                <a class="bt-type7 fl" style="width: 49%" id="prev_page">이전</a>
                <asp:LinkButton runat="server" ID="btn_submit" OnClientClick="showLoading();" OnClick="btn_submit_Click" class="bt-type6 fr" Style="width: 49%">나눔펀딩 등록</asp:LinkButton>
            </div>



        </div>

    </div>




</asp:Content>
