﻿$(function () {

	$page.init();

});

var $page = {

	init: function () {
		this.setMainVisual();
	},

	setMainVisual: function () {
		console.log("test")

		var swiper = new Swiper('.swiper-container', {
			pagination: '.swiper-container .page-count',
			paginationClickable: true,
			nextButton: '.swiper-container .page-next',
			prevButton: '.swiper-container .page-prev',
			paginationBulletRender: function (index, className) {
				return '<span class="' + className + '">' + (index + 1) + '</span>';
			},
			spaceBetween: 30
		});


	}

};

(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

		$scope.total = -1;
		$scope.list = [];
		$scope.today = new Date();
		$scope.nodata = false;
		var rowsPerPage = 4;

		var type = paramService.getParameter("type");
		if (!type) type = "";

		$scope.params = {
			type: type,
			sort: "new",
			keyword: "",
			page: 1,
			rowsPerPage: 4
		}

		$scope.params = $.extend($scope.params, paramService.getParameterValues());
		if ($scope.params.page > 1) {
			$scope.params.rowsPerPage = $scope.params.page * rowsPerPage;
			$scope.params.page = 1;
		}

		$scope.getList = function (params , reload) {

			$scope.params = $.extend($scope.params, params);
			console.log($scope.params);

			$http.get("/api/user-funding.ashx?t=list", { params: $scope.params }).success(function (r) {

				// 2페이지 이상에서 상세로 갔다가 돌아온경우 요청된 페이지만큼  데이타를 가져오고 page , rowsPerPage 를 원래대로 돌려놓는다.
				// 페이지 중복 호출로 인해 주석처리 
				//$scope.params.rowsPerPage = rowsPerPage;
				//$scope.params.page = $scope.params.rowsPerPage / rowsPerPage;

				if (r.success) {
					console.log(r.data);
					var list = r.data;
					
					$.each(list, function () {
						this.uf_date_end = new Date(this.uf_date_end);
						this.uf_date_start = new Date(this.uf_date_start);
						this.uf_regdate = new Date(this.uf_regdate);
						this.uf_type_name = this.uf_type == "normal" ? "양육을 돕는 펀딩" : "어린이 결연 펀딩";
						this.uf_image = location.origin.replace("//m." , "//www.")  + this.uf_image;
						var oneDay = 24 * 60 * 60 * 1000;
					});

					// 더보기인경우 merge

					if (reload) {
						$scope.list = list;
						if (list.length < 1)
							$scope.nodata = true;
					} else {
						$scope.list = $.merge($scope.list, list);
					}

				    //모금중인 모금함이 우선으로 보이도록 Start 
					var nowDate = new Date();
					var nowY = String(nowDate.getFullYear());
					var nowM = ((nowDate.getMonth() + 1) < 10 ? '0' : '') + String((nowDate.getMonth() + 1));
					var nowD = (nowDate.getDate() < 10 ? '0' : '') + String(nowDate.getDate());
					var nowDateString = nowY + nowM + nowD;

					var lst = angular.copy($scope.list);
					lst.sort(function (a, b) {
					    var endDate = new Date(a.uf_date_end);
					    var endY = String(endDate.getFullYear());
					    var endM = ((endDate.getMonth() + 1) < 10 ? '0' : '') + String((endDate.getMonth() + 1));
					    var endD = (endDate.getDate() < 10 ? '0' : '') + String(endDate.getDate());
					    var endDateString = endY + endM + endD;
					    return endDateString > nowDateString ? -1 : 1;
					});
					$scope.list = lst;
				    //모금중인 모금함이 우선으로 보이도록 End
					
					$scope.total = list.length > 0 ? list[0].total : 0;
					
					setTimeout(function () {
						common.bindSNSAction();
					}, 500);


				} else {
					alert(r.message);
				}
			});

		}

		$scope.showMore = function ($event) {
			$event.preventDefault();
			$scope.getList({ page: $scope.params.page + 1 });
		}

		$scope.goDetail = function ($event, item) {

			var uf_id = "";
			if ($.type(item) == "number") {
				uf_id = item;
			} else {
				uf_id = item.uf_id;
			}
			
			$event.preventDefault();
			location.href = "/sponsor/user-funding/view/" + uf_id + "?" + $.param($scope.params);
		}

		$scope.changeType = function ($event, type) {
			$event.preventDefault();

			$(".teb_menu").removeClass("selected");
			angular.element($event.currentTarget).addClass("selected");

			$scope.params.type = type;
			$scope.getList({page : 1} , true);
		}

		$scope.sort = function ($event,arg) {
			$event.preventDefault();
			$scope.params.sort = arg;
			$scope.getList({page : 1}, true);
		}

		$scope.search = function ($event) {
			if ($event)
				$event.preventDefault();
			$scope.getList({ page: 1 }, true);
		}

		$scope.getList();

	});

	app.directive('ufItem', function ($http) {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {

				attrs.$observe('ufItem', function (obj) {

					if (!obj) return;
					obj = $.parseJSON(obj);
					var url = "http://" + location.host + "/sponsor/user-funding/view/" + obj.uf_id;

					element.attr("data-url", url);
					element.attr("data-title", "나눔펀딩-" + obj.uf_title);
				});
			}
		};
	});

})();