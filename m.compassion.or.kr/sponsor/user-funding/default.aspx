﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="sponsor_user_funding_default" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/sponsor/user-funding/default.js?v=1"></script>
	<script src="/common/js/util/swiper.min.js"></script>
	<link href="/common/css/swiper.css" rel="stylesheet" />

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="sponsor-funding">
    	
			<div class="wrap-slider sectionsub-margin1 swiper-container">
				<div class="swiper-wrapper">
					
					 <asp:Repeater runat="server" ID="repeater" >
                        <ItemTemplate>

							<div class="swiper-slide" style="position:relative">
            					<a ng-click="goDetail($event,<%# Eval("uf_id").ToString()%>)" style="display:block;">
									<div class="dim"></div>
            						<div class="uf_view_visual" style="background:url('<%# Eval("uf_image").ToString()%>') no-repeat center top;">
										<div style="position:absolute;left:0;top:28%;z-index:201;width:100%;padding:0 40px">    <!--jquery 방법에 따라 inline  임시css  추후 변경바람니다-->
                							<p class="txt-slogon"><strong><%# Eval("uf_title").ToString()%></strong>
                    							<%# Eval("uf_summary").ToString()%>
											</p>
										</div>
									</div>
            					</a>
							</div>

						</ItemTemplate>
                    </asp:Repeater>
				</div>
				<div class="page-prev">이전보기</div>
				<div class="page-next">다음보기</div>
				<div class="page-count">
					<span class="recent">1페이지</span>
					<span>2페이지</span>
					<span>3페이지</span>
				</div>
			</div>

			<p class="txt-caption mt30">나눔펀딩</p>
			<p class="txt-sub">후원이 끊긴 어린이, 어린이센터 건축, 깨끗한 식수 지원, 직업교육, 의료지원, 엄마와 아기 지원 등 다양한 곳에 후원해보세요</p>
        
			<div class="align-list align-center mt30">
        		<a href="#" ng-class="{'selected':params.sort == 'new'}" ng-click="sort($event,'new')">최신순</a>
				<span class="bar">|</span>
				<a href="#" ng-class="{'selected':params.sort == 'interest'}" ng-click="sort($event,'interest')">인기순</a>
				<span class="bar">|</span>
				<a href="#" ng-class="{'selected':params.sort == 'deadline'}" ng-click="sort($event,'deadline')">종료임박</a>
			</div>
        
			<div class="wrap-tab6">
				<a class="teb_menu selected" href="#" ng-class="{'on':params.type == ''}" ng-click="changeType($event,'')" style="width:33%" >전체나눔펀딩</a>
				<a class="teb_menu" href="#" ng-class="{'on':params.type == 'child'}" ng-click="changeType($event,'child')" style="width:34%">어린이 결연 펀딩</a>
				<a class="teb_menu" href="#" ng-class="{'on':params.type == 'normal'}" ng-click="changeType($event,'normal')" style="width:33%">양육을 돕는 펀딩</a>
			</div>
		
			<p class="txt-searchcount">현재까지 <em>“ {{total}} ”</em>개의 나눔펀딩이 개설되었습니다.</p>

			<div class="wrap-thumbil">
            
				<ul class="list-thumbil width100">
					<li ng-repeat="item in list" class="{{((item.uf_date_end | amDifference : today : 'days') <= 0) ? 'ending' : '' }}">
							<span class="box-block">
								<!-- 모금 중 -->
								<div ng-if="((item.uf_date_end | amDifference : today : 'days') > 0)">
									<span class="photo" style="background:url('{{item.uf_image}}') center;">
                    					<p class="txt-dday">D-{{ item.uf_date_end | amDifference : today : 'days' }}</p>
										<a class="share-sns" ng-click="sns.show($event , {url : '/sponsor/user-funding/view/' + item.uf_id , title : '[한국컴패션-나눔펀딩]' + item.uf_title , desc : '' , picture : item.uf_image})">
											<img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
										<a ng-click="goDetail($event,item)"><span class="dim"></span></a>
										<span class="wrap-graph">
											<span class="txt-flag" ng-style="{left:item.uf_current_amount / item.uf_goal_amount * 100 > 100 ? 100 + '%' : item.uf_current_amount / item.uf_goal_amount * 100 + '%'}"><em>{{item.uf_current_amount / item.uf_goal_amount|percentage:0}}</em><span ng-if="item.uf_current_amount < item.uf_goal_amount">달성중</span><span ng-if="item.uf_current_amount >= item.uf_goal_amount">달성</span></span>   <!--텍스트좌표 left % 수치-->
											<span class="flag"	 ng-style="{left:item.uf_current_amount / item.uf_goal_amount * 100 > 100 ? 100 + '%' : item.uf_current_amount / item.uf_goal_amount * 100 + '%'}">{{item.uf_current_amount / item.uf_goal_amount|percentage:0}}</span>   <!--하트좌표 left % 수치-->
											<span class="bg-graph">
												<span class="rate" ng-style="{width:item.uf_current_amount / item.uf_goal_amount * 100 > 100 ? 100 + '%' : item.uf_current_amount / item.uf_goal_amount * 100 + '%'}"></span>      <!--노란그래프 width % 수치-->
											</span>
											<span class="txt-graph">
												<span class="txt1">{{item.uf_current_amount| number:N0}}원</span>
												<span class="txt2">{{item.uf_goal_amount | number:N0}}원</span>
											</span>
										</span>
									</span>
								</div>
								<!-- 모금 완료 -->
								<div ng-if="((item.uf_date_end | amDifference : today : 'days') <= 0)" ng-click="goDetail($event,item)">
									<span class="photo" style="background:url('{{item.uf_image}}') center;">
                    					<span class="dim">모금이 완료되었습니다</span>
									</span>
								</div>
								
                    			<!-- 하단 공통  -->
								<a ng-click="goDetail($event,item)">
									<span class="sponsor-fundinginfo">
										<span class="txt-category">{{item.uf_type_name}}</span>
										<span class="txt-title"><span class="textcrop-1row">{{item.uf_title}}</span></span>
										<span class="txt-note">{{item.uf_summary}}</span>
										<span class="txt-date">
                        					By {{item.sponsorname}}<span class="bar"></span>댓글 <span class="color1">{{item.uf_cnt_reply}}</span>
										</span>
									</span>
								</a>
							</span>
					</li>
					
					<!--게시물없음-->
					<li class="no-result" ng-if="nodata">
                		<strong>앗, 기존 나눔펀딩이 모두 종료되어<br />진행중인 나눔펀딩이 없어요.</strong>
						새로운 나눔펀딩을 개설해 보시겠어요? <br />단계에 따라 손쉽게 생성하실 수있습니다.
					</li>
				</ul>
			</div>
	
			<div class="wrap-bt" ng-show="list.length < total" ><a class="bt-type6" style="width:100%" href="#" ng-click="showMore($event)">더 많은 나눔펀딩 보기<span class="bt-bu2"></span></a></div>
		
			<div class="bottom-fundmake sectionsub-margin1">
        		새로운 나눔펀딩을 개설해 보시겠어요?<br />
				단계에 따라 손쉽게 생성하실 수있습니다.
				
				<asp:LinkButton runat="server" ID="btn_create" OnClick="btn_create_Click" class="bt-type1" style="width:100%">새로운 나눔펀딩 만들기</asp:LinkButton>
			</div>

		</div>
    
	<!--//-->
	</div>


</asp:Content>
