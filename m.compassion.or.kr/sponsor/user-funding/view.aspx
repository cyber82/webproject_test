﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="sponsor_user_funding_view" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
	
	<script type="text/javascript" src="/sponsor/user-funding/view.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" id="uf_id" runat="server" />
	<input type="hidden" id="campaignId" runat="server" />
	<input type="hidden" id="amount" runat="server" />
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />
	<input type="hidden" runat="server" id="hd_percent" value="" />
	<input type="hidden" runat="server" id="hd_is_end" value="" />
	
	
	<div class="wrap-sectionsub"  ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="sponsor-funding">
    	
			<div class="wrap-visualtop sectionsub-margin1" style="background:url('<%:entity.uf_image.WithFileServerHost()%>') no-repeat">
				<div class="dim"></div>    <!--dim-->
				<div class="contain" style="padding-top:60px">    <!--padding으로 컨텐츠 가운데에 맞게 조정-->
					<strong class="txt-slogon"><asp:Literal runat="server" ID="uf_title" /></strong>
					<asp:Literal runat="server" ID="uf_summary" />
				</div>
			</div>

			<div class="box-achieve sectionsub-margin1">
        		<p class="txt-percent"><em><%:ori_percent %>%</em> <%:percent >= 100 ? "달성" : "달성중" %>  <span class="txt-dday"><asp:Literal runat="server" ID="days" /></span></p>


				<span class="wrap-graph" style="display:<%:percent < 0 ? "none" : "block"%>">
					<span class="flag"	 style="left:<%:percent %>%"><%:percent %>%</span>   <!--하트좌표 left % 수치-->
					<span class="bg-graph">
						<span class="rate" style="width:<%:percent %>%"><%:ori_percent %>%</span>      <!--노란그래프 width % 수치-->
					</span>
					<span class="txt-graph">
						<span class="txt1"><%:entity.uf_current_amount.ToString("N0") %>원</span>
						<span class="txt2"><%:entity.uf_goal_amount.ToString("N0") %>원</span>
					</span>
				</span>
            
				<!--하단 스크롤 고정-->
				<div class="bottomscroll-fund"> 
					<!--후원하기-->
					<div class="row-support btn_sponsor">
                		
						<div class="mt10 mb10"><input type="text"id="dsp_amount" value="20,000" class="input_amount number_only use_digit" maxlength="10" style="width:100%"  placeholder="금액을 입력해주세요."/></div>
					</div>
					<!--//후원하기-->
                
					<!--//공유하기-->
					<div class="row btn_sponsor">
						<a class="sns-share" ng-click="sns.show($event)"><img src="/common/img/icon/share2.png" alt="SNS공유" /></a>
						<a href="#" ng-click="goPay($event)" class="bt-type6" style="width:100%">바로 후원하기</a>

					</div>
				</div>
				<!--//하단 스크롤 고정-->
            
			</div>
        
        
			<div>

				<!--펀딩 개설자 정보-->
				<p class="txt-caption mt30">나눔 크리에이터</p>
				<p class="txt-caption2"><asp:Literal runat="server" ID="creator" /></p>
				<div class="photo-fundingcreater" style="background:url('<%:entity.UserPic.WithFileServerHost()%>') no-repeat"></div>
			
				<asp:PlaceHolder runat="server" ID="ph_go_update" Visible="false" >
				<div class="wrap-bt"><a class="bt-type4" style="width:100%" href="/sponsor/user-funding/update/<%:entity.uf_id%>">내용 수정하기</a></div>
				</asp:PlaceHolder>
				<!--// -->

				<!-- 이야기펀딩일 경우 -->
				<asp:PlaceHolder runat="server" ID="ph_campaign" Visible="false" >
				<div class="funding-program mt30">
        			<p class="txt-program1"><em>이야기 펀딩</em>제가 함께 하고 싶은 후원은요,</p>
					<p class="txt-program2"><%:entity.uf_title%></p>
					<p class="bottom-program"><%:entity.uf_summary%></p>
				</div>
				</asp:PlaceHolder>
				<!--// -->

			
				<!-- 어린이후원일 경우 -->
				<asp:PlaceHolder runat="server" ID="ph_child" Visible="false" >

				<!-- 어린이 결연 펀딩일 경우 -->
				<div class="funding-program mt30">
					<p class="txt-program1 tal"><em>어린이 결연 펀딩</em>제가 함께 하고 싶은 어린이는요,</p>
					<div class="child-intro">
						<div class="photo-border"><span class="photo-child" style="background:url('<%:this.ViewState["c_img"].ToString()%>') no-repeat center top"></span></div>
						<p class="greeting">안녕하세요, 제 이름은 <span><asp:Literal runat="server" ID="c_name" /></span>예요.</p>
					</div>
					<p class="story">
						나이는 <em><asp:Literal runat="server" ID="c_age" /></em>살 이고요, <em><asp:Literal runat="server" ID="c_country" /></em>에 살고 있어요.<br />
						제 생일은 <em><asp:Literal runat="server" ID="c_birth" /></em> 이고요, 제가 좋아하는 것은
						<em><asp:Literal runat="server" ID="c_hobby" /></em>이예요. 제게도 후원자님이 생겨서
						컴패션 친구들과 함께 배우고 싶어요.
					</p>
				</div>
				<!--//  -->

				</asp:PlaceHolder>
				<!--// -->

				
			</div>
			
			<!-- 탭메뉴 -->
			<div class="wrap-tab6 mt30" id="tab">
				<a href="#" ng-class="{'selected' : tab == '1'}" ng-click="changeTab($event,'1')" style="width:33%">나눔 스토리</a>
				<a href="#" ng-class="{'selected' : tab == '2'}" ng-click="changeTab($event,'2')" style="width:35%">업데이트 소식
					<img src="/common/img/icon/new.png" ng-show="notice.isNew" alt="new" width="18" style="vertical-align:top;margin:-1px 0 0 4px" /></a>
				<a href="#" ng-class="{'selected' : tab == '3'}" ng-click="changeTab($event,'3')" style="width:32%">참여자</a>
			</div>
			<!--// 탭메뉴 -->
			
			<!------- 나눔스토리 -------->
			<div class="sf_view_tab story" ng-show="tab == '1'">
				
		
				<div class="editor-html" style="margin-bottom:15px;">
        	
					<!--개설 시 등록한 펀딩 영상.(선택사항) 등록한 경우만 노출 , url 임시경로 추후 수정요망-->
					
					<!-- 영상 -->
					<asp:PlaceHolder runat="server" ID="ph_movie" Visible="false">
					<div class="youtube">
						<asp:Repeater runat="server" ID="repeater_movie">
							<ItemTemplate>
								<iframe src="<%#Container.DataItem.ToString().Replace("https://youtu.be/", "https://www.youtube.com/embed/") + "?rel=0&amp;showinfo=0;wmode=transparent" %>" frameborder="0" allowfullscreen></iframe>
							</ItemTemplate>
						</asp:Repeater>
					</div>
					</asp:PlaceHolder>
					<!--// -->

					<!-- 본문내용 -->
					<div>
						<asp:Literal runat="server" ID="uf_content" />
					</div>
					<!--// -->

					<!-- 본문이미지 -->
					<asp:PlaceHolder runat="server" ID="ph_content_image" Visible="false">
					<div class="mt20">
						<asp:Repeater runat="server" ID="repeater_content_image">
							<ItemTemplate>
								<img src="<%#Container.DataItem.ToString().WithFileServerHost() %>"  alt="" /><br />
							</ItemTemplate>
						</asp:Repeater>
					</div>
					</asp:PlaceHolder>
					<!--// -->

				</div> 
            
				
				<!-- 캠페인 컨텐츠 인클루드영역 -->
				<div>
					
					<asp:PlaceHolder runat="server" ID="ph_campaign_content" Visible="false">
                        <div class="sponsor-special">
                            <asp:Literal runat="server" ID="sf_content_m" />
                        </div>
					</asp:PlaceHolder>
					


					<!-- 다운로드 -->
					<div class="report" ng-if="report.total > 0">
						<p class="txt-caption mt30">양육을 돕는 뉴스레터</p>
						<ul class="list-report">
							<li ng-repeat="item in report.list">
								<a ng-href="{{item.sr_file}}" target="_blank">{{item.sr_title}}<img src="/common/img/icon/pdf.png" class="pdf" alt="레포트 다운로드" /></a>

							</li>
						</ul>
						<span class="more-loading" ng-click="report.showMore($event)" ng-show="report.total > report.list.length"><span>더보기</span></span>
					</div>
					<!--// 다운로드 -->
        
				</div>
				<!--// 캠페인 컨텐츠 인클루드영역 -->

				<div class="box-gray mt30">
					<strong>나눔펀딩 안내</strong>
					<ol class="list-nobullet">
						<li>1. 나눔펀딩으로 모금된 후원금의 집행은 한국컴패션에서 진행됩니다.</li>
						<li>2. 목표금액을 달성하지 못한 모금액과 목표금액을 초과한 모금액 모두 현재 모금중인 캠페인에 기부됩니다. (1:1어린이양육을 위한 모금일 경우 해당 부분은 후원자님을 만나지 못한 어린이를 위한 후원금으로 사용됩니다.)</li>
						<li>3. 1:1어린이양육을 위한 목표금액이 달성되면 일주일 이내에 개설자가 어린이의 후원자로 등록되어 편지쓰기, 선물하기 등이 가능하며 후원 취소는 하실 수 없습니다.</li>
						<li>4. 나눔펀딩으로 1:1 후원하게 된 후원어린이가 어린이의 사정으로 컴패션을 떠나게 되는 경우, 잔여 모금액은 후원자님을 만나지 못한 어린이를 위한 후원금으로 사용됩니다.</li>
					</ol>
				</div>

		
				
				<!-- 응원댓글 -->
				<div class="wrap-reply mt30"  ng-show="reply.total > -1">

					<div class="box-gray">
						<p class="txt-count">응원댓글 <em>{{reply.total}}</em></p>
						<span class="row">
							<input type="text" id="reply_comment" maxlength="300" ng-model="reply.comment" placeholder="댓글을 입력해주세요." />
							<a class="bt-reg" href="#" ng-click="reply.add($event)">등록</a>
						</span>
						<p class="txt-byte"><span id="reply_comment_count">0</span>/300자</p>
					</div>

					<ul class="list-reply">
						<li ng-repeat="item in reply.list" data-idx="{{item.ur_id}}">
							<p class="txt-id">{{item.userid}}</p>
							<p class="txt-reply">{{item.ur_content}}</p>
							<time class="txt-time">{{item.ur_regdate | date:'yyyy.MM.dd HH:mm:ss' }}</time>
                    
							<span class="action" ng-if="item.is_owner">
								<span class="modify" ng-click="reply.toggleUpdate(item.ur_id , $event)">수정</span>
								<span class="delete" ng-click="reply.delete(item.ur_id , $event)">삭제</span>
							</span>

							<!--수정 버튼 클릭시-->
							<div class="reply_modify_row modifyArea"  data-idx="{{item.ur_id}}" ng-if="item.is_owner" style="display:none;">
								<textarea id="re_modify_{{item.ur_id}}" class="reply_modify" style="width:100%;height:70px">{{item.ur_content}}</textarea>
								<p class="txt-byte"><em class="count">0</em>/300자</p>
								<span class="wrap-bt">
									<a class="bt-type8" onclick="$('.modifyArea').slideUp(300);">수정취소</a>
									<a class="bt-type6" ng-click="reply.update(item.ur_id , $event)">수정하기</a>
								</span>
							</div>
							<!--//수정 버튼 클릭시-->
						</li>

						<!----- 댓글 없을때 ----->
						<li class="no-result" ng-if="reply.total == 0">등록된 댓글이 없습니다.<br />첫 댓글로 나눔펀딩을 응원해보세요.</li>
						<!-----// ----->
					</ul>
            
					<span class="more-loading" ng-show="reply.total > reply.list.length" ng-click="reply.showMore($event)"><span>더보기</span></span>
				</div>
				<!-- 응원댓글 -->
			</div>		
			<!-------// 나눔스토리 -------->
			

			<!------- 업데이트소식 -------->
			<div class="sf_view_tab update" ng-show="tab == '2'">
				<div ng-show="notice.total > -1">
					
					<!--개설자가 로그인한 경우 나타나는 소식 업데이트 영역-->
					
					<asp:PlaceHolder runat="server" id="ph_notice_add" Visible="false" >
					<fieldset class="top-fundcreater">
        				<legend>업데이트소식 등록</legend>

						<div class="frm-row box">
            				<textarea id="notice_comment" ng-model="notice.comment" style="width:100%;height:95px" placeholder="최근 나눔펀딩의 진행소식을 전해주세요."></textarea>
							<p class="txt-byte"><em id="notice_comment_count">0</em>/1000자</p>
						</div>

						<div class="frm-row file">
							<input type="text" id="lb_file_path" value="" placeholder="이미지 파일을 선택해주세요." style="width:100%" readonly="readonly" />
							<a id="btn_file_path" class="bt-type8 pos-btn" style="width:70px" href="#">이미지 찾기</a>
						</div>

					</fieldset>
					<div class="wrap-bt mt20"><a class="bt-type6" style="width:100%"  ng-click="notice.add($event)">등록</a></div>
					</asp:PlaceHolder>
        


					<ul class="list-fundupdate mt30">
        				<li  data-idx="{{item.un_id}}" ng-repeat="item in notice.list">
							<span class="pos-photo"><span class="photo" background-img="{{item.userpic}}" data-default-image="/common/img/page/my/no_pic.png" style="background:no-repeat"></span></span>
            				<time class="txt-time">{{item.un_regdate | date:'yyyy.MM.dd HH:mm:ss'}}</time>
							<span class="txt-contents">
								<div ng-bind-html="item.un_content"></div>
								<div class="update_img" ng-show="item.un_image"><img ng-src="{{item.un_image}}" alt="업데이트 이미지"/></div>
							</span>
							<span class="action" ng-if="item.is_owner">
								<span class="modify" ng-click="notice.toggleUpdate(item.un_id , $event)">수정</span>
								<span class="delete" ng-click="notice.delete(item.un_id , $event)">삭제</span>
							</span>

							<!--수정시 -->
							<div class="notice_modify_row modify-layer" ng-if="item.is_owner" data-idx="{{item.un_id}}" style="display:none;">
                				<span class="frm-row box">
									<textarea id="no_modify_{{item.un_id}}" class="no_modify_comment" style="width:100%;height:78px">{{item.un_content}}</textarea>
									<p class="txt-byte"><em class="count">0</em>/1000자</p>
								</span>
								<span class="frm-row file del">
									<input type="text" value="{{item.or_image}}" placeholder="이미지 파일을 선택해주세요." style="width:100%" />
									<a class="bt-type8 pos-btn" style="width:70px"  id="btn_modify_file_path_{{item.un_id}}">이미지 찾기</a>
									<a class="bt-type8 pos-btn2" style="width:50px" id="btn_delete_file_{{item.un_id}}">삭제</a>
								</span>
								<span class="wrap-bt">
									<a class="bt-type8" onclick="$('.notice_modify_row').slideUp(300);">수정취소</a>
									<a class="bt-type6" ng-click="notice.update(item.un_id , $event)">수정하기</a>
								</span>

							</div>
							<!--//수정시 -->
						</li>

						<!----- 업데이트 소식 없을때 ----->
						<li class="no-result" ng-if="notice.total == 0">등록된 업데이트 소식이 없습니다.</li>
						<!-----// ----->
					</ul>
        
					 <span class="more-loading" ng-show="notice.total > notice.list.length" ng-click="notice.showMore($event)"><span>더보기</span></span>
					
				</div>
			</div>
			<!-------// 업데이트소식 -------->


			
			<!------- 참여자 -------->
			<div class="sf_view_tab participant" ng-show="tab == '3'">
				<div ng-show="user.total > -1">
					
        
					<ul class="list-fundparticipant mt30">
        				<li ng-repeat="item in user.list">
							<span class="txt1"><em>{{item.uu_amount | number:0}}</em>원이 후원되었어요.</span>
            				<span class="txt2">{{item.userid}}</span>
							<span class="txt3">{{item.uu_regdate | date:'yyyy.MM.dd HH:mm:ss' }}</span>
						</li>

						<!----- 참여자 없을때 ----->
						<li class="no-result" ng-if="user.total == 0">아직 이 나눔펀딩에 참여자가 없습니다.<br />참여를 통해 어린이들의 꿈을 응원해주세요.</li>
						<!-----// ----->
					</ul>
        
					 <span class="more-loading" ng-show="user.total > user.list.length" ng-click="user.showMore($event)"><span>더보기</span></span>

				</div>
			</div>
			<!-------// 참여자 -------->



			<div class="bottom-fundmake sectionsub-margin1">
        		새로운 나눔펀딩을 개설해 보시겠어요?<br />
				단계에 따라 손쉽게 생성하실 수있습니다.
				<a class="bt-type1" style="width:100%" href="/sponsor/user-funding/create">새로운 나눔펀딩 만들기</a>
			</div>

		</div>
    
		 <div data-id="footer_action"></div>
	<!--//-->
	</div>



</asp:Content>
