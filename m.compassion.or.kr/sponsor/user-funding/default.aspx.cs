﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class sponsor_user_funding_default : MobileFrontBasePage {


	protected override void OnBeforePostBack() {

        /*         이벤트 정보 등록  -- 20170327 이정길       */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;
        Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));


        base.OnBeforePostBack();

		this.GetList();

	}

	void GetList()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tUserFunding_pick_list_f().ToList();
            Object[] op1 = new Object[] {  };
            Object[] op2 = new Object[] {  };
            var list = www6.selectSP("sp_tUserFunding_pick_list_f", op1, op2).DataTableToList<sp_tUserFunding_pick_list_fResult>();

            foreach (var entity in list)
            {
                entity.uf_image = entity.uf_image.WithFileServerHost();
            }

            repeater.DataSource = list;
            repeater.DataBind();
        }

	}


    protected void btn_create_Click( object sender, EventArgs e )
    {
        UserInfo sess = new UserInfo();
        using (FrontDataContext dao = new FrontDataContext())
        {
            // 펀딩 개설 가능한지 여부 
            //var result = dao.sp_tUserFunding_can_create_f(sess.UserId).First();
            Object[] op1 = new Object[] { "UserId" };
            Object[] op2 = new Object[] { sess.UserId };
            var result = www6.selectSP("sp_tUserFunding_can_create_f", op1, op2).DataTableToList<sp_tUserFunding_can_create_fResult>().First();

            if (result.result == "N")
            {
                base.AlertWithJavascript(result.msg, "goBack()");
                return;
            }
            else
            {

                Response.Redirect("/sponsor/user-funding/create");
            }

        }


    }
}