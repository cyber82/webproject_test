﻿
var $page = {

	timer: null,
	cert_target: "",		// me , parent
	checked_account_val: "",
	

	init: function () {

		$page.setPaymentEvent();

		// 국세청 영수증 체크시만 
		if ($("#p_receipt_pub_ok").length > 0) {
			$("#p_receipt_pub_ok").change(function () {
				if ($(this).prop("checked")) {
					$("#func_name_check").show();
				} else {
					$("#func_name_check").hide();
				}
			});

			$("#p_receipt_pub_ok").trigger("change");
		}
		


		//실명인증등
		$("#btn_name_check").click(function () {
			cert_nameCheck($("#user_name"), $("[data-id=jumin1]"), null, $("#hd_auth_domain").val());

			return false;
		})

		// 보호자 본인 인증
		// 휴대폰 인증
		$("#btn_parent_cert_by_phone").click(function () {
			$page.cert_target = "parent";
			cert_openPopup("phone", $("#hd_auth_domain").val());

			return false;
		})

		// 아이핀 인증
		$("#btn_parent_cert_by_ipin").click(function () {
			$page.cert_target = "parent";
			cert_openPopup("ipin", $("#hd_auth_domain").val());
			

			return false;
		})

		// 본인인증
		// 휴대폰 인증
		$("#btn_cert_by_phone").click(function () {
			$page.cert_target = "me";
			cert_openPopup("phone", $("#hd_auth_domain").val());

			return false;
		})

		// 아이핀 인증
		$("#btn_cert_by_ipin").click(function () {
			$page.cert_target = "me";
			cert_openPopup("ipin", $("#hd_auth_domain").val());
				
			return false;
		})

		// 종교
		$("#religion").change(function () {
			if ($(this).val() == "기독교") {
				$("#pn_church").show();
			} else {
				$("#pn_church").hide();
			}
		})

		$("#ddlHouseCountry").change(function () {
			if ($(this).val() == "한국") {
				$("#hdLocation").val("국내");
			} else {
				$("#hdLocation").val("국외");
			}
		})

		// 가입하기 버튼
		$("#btn_submit").click(function () {
			return $page.onSubmit();
		});

		// 주소선택 국내/국외
		$("#addr_overseas").change(function () {
			if ($(this).prop("checked")) {

				$(".dmYN").removeClass("selected");
				$(".dmYN[data-value='N']").addClass("selected");
				$(".hide_overseas").hide();
				$("#pn_addr_domestic").hide();
				$("#title_addr").hide();
				$("#pn_addr_overseas").show();

			} else {
				$(".hide_overseas").show();
				$("#pn_addr_domestic").show();
				$("#title_addr").show();
				$("#pn_addr_overseas").hide();
			}
		})

		
		$("#church_name").focus(function () {
			$("#church_name").val("");
			$("#hdOrganizationID").val("");
		})

		$("#cms_owner , #cms_birth , input[name=cms_bank] , #cms_bank_etc").change(function () {
			$("#is_checked_account").val("N");
		})

		// 기본값세팅
		var ssn = $("#jumin").val();
		if (ssn.length == 13) {
			$("#jumin1").val(ssn);
		}

		if ($("#religion").val() == "기독교") {
			$("#pn_church").show();
		} else {
			$("#pn_church").hide();
		}

		if ($("#addr_overseas").prop("checked")) {
		    $("#pn_addr_domestic").hide();
		    $("#title_addr").hide();
			$("#pn_addr_overseas").show();

			$("#addr_overseas_zipcode").val($("#zipcode").val());
			$("#addr_overseas_addr1").val($("#addr1").val());
			$("#addr_overseas_addr2").val($("#addr2").val());

			
		} else {

		    $("#pn_addr_domestic").show();
		    $("#title_addr").show();
			$("#pn_addr_overseas").hide();

			$("#addr_domestic_zipcode").val($("#zipcode").val());
			$("#addr_domestic_addr1").val($("#addr1").val());
			$("#addr_domestic_addr2").val($("#addr2").val());


			$("#addr_domestic_1").val($("#zipcode").val());

			// 컴파스의 데이타를 불러오는경우 
			if ($("#dspAddrDoro").val() != "") {
				$("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
				if ($("#dspAddrJibun").val() != "") {
					$("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
				}

			} else if ($("#addr1").val() != "") {
				addr_array = $("#addr1").val().split("//");
				$("#addr_road").text("[도로명주소] " + addr_array[0] + " " + $("#addr2").val());
				if (addr_array[1]) {
					$("#addr_jibun").text("[지번주소] " + addr_array[1] + " " + $("#addr2").val());
				}
			}

		}

		$("#addr_overseas").trigger("change");

		$(".dmYN[data-value='" + $("#dmYN").val() + "']").addClass("selected");
		$(".translationYN[data-value='" + $("#translationYN").val() + "']").addClass("selected");

		$(".btn_cms_owner").click(function () {

			var val = $(this).data("value");
			$("input[name=cms_owner_type][value='" + val + "']").prop("checked", true);
			$(".btn_cms_owner").removeClass("selected");
			$(this).addClass("selected");
			return false;
		})

		$(".btn_cms_account_type").click(function () {

			var val = $(this).data("value");
			
			$(".cms_account_type[data-value='" + val + "']").trigger("click");
			$(".btn_cms_account_type").removeClass("selected");
			$(this).addClass("selected");
			return false;
		})

		$(".btn_card_owner").click(function () {

			var val = $(this).data("value");
			//$("input[name=cms_account_type][value='" + val + "']").prop("checked", true);
			$(".btn_card_owner").removeClass("selected");
			$(this).addClass("selected");
			return false;
		})

		$(".dmYN").click(function () {
			$(".dmYN").removeClass("selected");
			$(this).addClass("selected");
			return false;
		});
		$(".translationYN").click(function () {
			$(".translationYN").removeClass("selected");
			$(this).addClass("selected");
			return false;
		});

		if ($("#cms_account_type2").length > 0 && $("#cms_account_type2").prop("checked")) {
			$(".btn_cms_account_type").removeClass("selected");
			$($(".btn_cms_account_type")[1]).addClass("selected");
		}
	}, 

	setPaymentEvent: function () {

		// 결제
		$("input.payment_method").click(function () {
			var id = $(this).attr("id");

			var type = $(this).data("type");
			
			$(".payinfo").hide();
			$(".payinfo[data-type='" + type + "']").show();
		});

		var checkedPayType = $("input.payment_method:checked");
		if (checkedPayType.length) {
			$("input.payment_method:checked").trigger("click");
		}

		$("#oversea_pay_month").keyup(function () {
			var val = parseInt($("#oversea_pay_month").val());
			if (isNaN(val))
				$("#oversea_pay_total").html("0<span>&nbsp;&nbsp;원</span>");
			else
				$("#oversea_pay_total").html((val * parseInt($("#hd_amount").val())).format() + "<span>&nbsp;&nbsp;원</span>");
		})

		$("#oversea_pay_month").trigger("keyup");
		// CMS
		// 계좌정보
		$("input.cms_account_type").click(function () {
			var id = $(this).attr("id");

			$(".cms_account_type1").hide();
			$(".cms_account_type2").hide();

			if (id == "cms_account_type1") {		// 개인
				$(".cms_account_type1").show();
				$("#cms_soc").val("");
			} else {
				$(".cms_account_type2").show();
				$("#cms_birth").val("");
			}
		});

		var checkedCmsAccountType = $("input.cms_account_type:checked");
		if (checkedCmsAccountType.length) {
			$("input.cms_account_type:checked").trigger("click");
		}

		// 예금주와의 관계
		$("input.cms_owner_type").click(function () {
			var id = $(this).attr("id");

			if (id == "cms_owner_type1") {		// 개인
				$("#cms_owner").val($("#user_name").val());
				if ($("#hdBirthDate").val() != "")
					$("#cms_birth").val($("#hdBirthDate").val().substring(2));
			} else {
				$("#cms_owner").val("");
				$("#cms_birth").val("");
			}
		});

		// 계좌확인
		$("#btn_cms_check_account").click(function () {

			if ($page.checkCMS()) {

				$.post("/api/payment.ashx", {
					t: "check-cms-owner",
					bankCode: $("#hd_cms_bank").val(),
					bankAccount: $("#cms_account").val(),
					birth: ($("#cms_birth").val() == "" ? $("#cms_soc").val() : $("#cms_birth").val()),
					owner: $("#cms_owner").val()
				}, function (r) {

					console.log(r);

					if (r.success) {
						var result = r.data;
						if (result.success) {
							alert("계좌가 확인되었습니다.")
							$("#is_checked_account").val("Y");
							
						} else {
							alert(result.msg);
						}

					} else {
						alert(r.message)
					}
				});

			}
			return false;
		})

		$(".cms_bank").click(function () {
			$("#cms_bank_etc").val("");
		})

		$("#cms_bank_etc").change(function () {
			if ($("#cms_bank_etc").val() != "") {
				$(".cms_bank").prop("checked", false);
			}
		});

		var cmsBank = $("#hd_cms_bank").val();
		if (cmsBank != "") {

			var checkedCms = false;
			$.each($(".cms_bank"), function () {
				if ($(this).data("code") == cmsBank) {
					$(this).prop("checked", true);
					checkedCms = true;
				}
			})

			if (!checkedCms) {
				$("#cms_bank_etc").val(cmsBank);
			}
		}

		$("#cms_account").keyup(function () {

			//			if ($("#cms_account").val() != $page.checked_account_val) {
			$("#is_checked_account").val("N");
			//}

		})

	},

	checkCMS: function () {

		if ($("#cms_owner").val() == "") {
			alert("이름을 입력해 주세요");
			$("#cms_owner").focus();
			return false;
		}
		if ($("#cms_account_type1").prop("checked")) {
			if ($("#cms_birth").val() == "") {
				alert("생년월일을 입력해 주세요");
				$("#cms_birth").focus();
				return false;
			}

			if ($("#cms_birth").val().length != 6) {
				alert("생년월일을 정확히 입력해 주세요");
				$("#cms_birth").focus();
				return false;
			}
		} else {
			if ($("#cms_soc").val() == "") {
				alert("사업자번호를 입력해 주세요");
				$("#cms_soc").focus();
				return false;
			}

			if ($("#cms_soc").val().length != 10) {
				alert("사업자번호를 정확히 입력해 주세요");
				$("#cms_soc").focus();
				return false;
			}
		}

		if ($(".cms_bank:checked").length < 1 && $("#cms_bank_etc").val() == "") {
			alert("은행을 선택해주세요");
			return false;
		}

		if ($(".cms_bank:checked").length > 0) {
			$("#hd_cms_bank").val($($(".cms_bank:checked")[0]).data("code"));
			$("#hd_cms_bank_name").val($("label[for='" + $($(".cms_bank:checked")[0]).attr("id") + "']").text());
		} else {
			$("#hd_cms_bank").val($("#cms_bank_etc").val());
			$("#hd_cms_bank_name").val($("#cms_bank_etc option:selected").text());
		}


		if ($("#cms_account").val() == "") {
			alert("계좌번호를 입력해 주세요");
			$("#cms_account").focus();
			return false;
		}

		if ($("#cms_account").val().length < 9) {
			alert("계좌번호를 정확히 입력해 주세요");
			$("#cms_account").focus();
			return false;
		}

		return true;
	},

	// 확인
	onSubmit: function () {

		if ($("#confirmMsg").length > 0) {
			if (!confirm($("#confirmMsg").val())) {
				return false;
			}
		}

		if (!$page.checkPage1()) return false;

		if (!$page.checkPage2()) return false;

		if (!$page.checkPage3()) return false;

		if ($("#payment_method_cms").length > 0 && $("#payment_method_cms").prop("checked") && !$("#agree_cms").prop("checked")) {
			alert("후원금 납부에 동의해 주세요");
			$("#agree_cms").focus();
			return false;
		}

		if ($("#payment_method_oversea").length > 0 && $("#payment_method_oversea").prop("checked")) {

			var val = $("#oversea_pay_month").val() ;
			if (val == "" || val == "0" || isNaN(val)) {
				alert("개월수를 입력해 주세요");
				$("#oversea_pay_month").focus();
				return false;
			}
			
		}

		//if ($("#payment_method_cms").length > 0 && $("#payment_method_cms").prop("checked")) {
		    loading.show();
		//}
        
		return true;
	},

	checkPage1: function () {
		// 본인인증
		if (!$("#addr_overseas").prop("checked")) {

			// 본인인증을 하지 않는 경우 (기업,미주등)
			if ($("#ph_cert_me").length > 0 && $("#ph_cert_me").css("display") == "none") {

			} else {

				if ($("#cert_gb").val() == "" && $("#user_class").val() != "기업") {
					alert("본인인증을 해주세요.");
					return false;
				}

				// 19세미만 보호자동의
				if ($("#hdState").val() != "기업" && $("#hdState").val() != "기타" && ($("#user_class").val() == "14세미만" || $("#user_class").val() == "19세미만")) {
					if ($("#parent_cert").val() == "") {
						alert("결제를 위해서 보호자 동의가 필요합니다.");
						return false;
					}

				}
			}

			if ($("#hdState").val() != "기업" && $("#hdState").val() != "기타") {
				// 연말정산 영수증 신청의 경우
				if ($("#p_receipt_pub_ok").prop("checked") && $("#jumin").val() == "") {
					alert("연말정산 영수증 신청을 위해 주민등록번호 확인을 해주세요.");
					return false;
				}
			}
		}

		return true;
	},

	checkPage2: function () {
		
		$("#dmYN").val($(".dmYN[data-value='Y']").hasClass("selected") ? "Y" : "N");
		$("#translationYN").val($(".translationYN[data-value='Y']").hasClass("selected") ? "Y" : "N");

		if ($("#addr_overseas").prop("checked")) {

			$("#zipcode").val($("#addr_overseas_zipcode").val());
			$("#addr1").val($("#addr_overseas_addr1").val());
			$("#addr2").val($("#addr_overseas_addr2").val());

		} else {

			$("#zipcode").val($("#addr_domestic_zipcode").val());
			$("#addr1").val($("#addr_domestic_addr1").val());
			$("#addr2").val($("#addr_domestic_addr2").val());

		}

		if ($("#last_name").val() == "") {
			alert('영문이름(성)을 입력해주세요.');
			$("#last_name").focus();
			return false;
		}

		if (!/^[a-zA-Z0-9\s]+$/.test($("#last_name").val())) {
			alert('영문이름(성)은 영문만 입력 가능합니다.');
			$("#last_name").focus();
			return false;
		}

		if ($("#first_name").val() == "") {
			alert('영문이름(이름)을 입력해주세요.');
			$("#first_name").focus();
			return false;
		}

		if (!/^[a-zA-Z0-9\s]+$/.test($("#first_name").val())) {
			alert('영문이름(이름)은 영문만 입력 가능합니다.');
			$("#first_name").focus();
			return false;
		}

		if ($("#religion1").prop("checked") && $("#church_name").val() == "") {
			// 기독교 & 교회명 은 체크안함.
		}

		// 국내주소
		if ($("#addr_overseas").prop("checked")) {
			if ($("#zipcode").val() == "") {
				alert('우편물 수령지 우편번호를 검색해주세요.');
				return false;
			}

			if ($("#addr2").val() == "") {
				alert('우편물 수령지 주소를 입력해주세요.');
				return false;
			}
			// 해외주소
		} else {

			if ($("#zipcode").val() == "") {
				alert('우편번호를 입력해주세요');
				return false;
			}

			if ($("#addr1").val() == "" || $("#addr2").val() == "") {
				alert('우편물 수령지 주소를 입력해주세요.');
				return false;
			}

		}

		// 후원계기
		if ($("#motive1").val() == "" || $("#motive2").val() == "") {
			alert("후원계기를 선택해 주세요");
			return false;
		}
		
		if ($("#motive2_etc").val() == "" && $("#motive1").val() == "기타" && $("#motive2").val() == "M0055") {
		    alert("기타 후원 계기를 입력해 주세요.");
		    return false;
		}
		return true;
	},

	checkPage3: function () {
		if ($(".payment_method").length > 0 && $(".payment_method:checked").length < 1) {
			alert("결제방법을 선택해주세요");
			return false;
		}

		var pay_type = $(".payment_method:checked").data("type");
		if (pay_type == "cms") {

			if (!$page.checkCMS()) {
				return false;
			}

			/*20170317 효성 FMS  작업으로 인해 주석처리 3월20일 월요일 에 주석 삭제
			if ($("#is_checked_account").val() != "Y") {
				alert("계좌 확인을 해주세요");
				return false;
			}
            */

		}

		return true;
	},


};


var goBack = function () {
	if (location.pathname.indexOf("/sponsor/pay/regular/") > -1)
		location.href = "/";
	else
		history.back();
};

var goLogin = function () {
	location.href = $("#hd_auth_domain").val() + "/login?r=/sponsor/pay/regular/";
};

// 본인인증 결과 응답
// result = Y or N , birth = yyyyMMdd
// sex = M or F
// method = ipin or phone
var cert_setCertResult = function (method, result, ci, di, name, birth, gender, phone) {

	console.log(method, result, ci, di, name, birth, gender, phone);

	if ($page.cert_target == "me") {

		$("#msg_cert").show();
		var target = $("#msg_cert")
		var msgObj = $("#msg_cert").find("span");
		target.removeClass("txt-error").addClass("txt-result");

		if (result != "Y") {
			target.removeClass("txt-result").addClass("txt-error");
			msgObj.html("본인인증에 실패했습니다. 다시 시도해 주세요.");
			return;
		}

		if ($("#user_name").val() != name) {
			target.removeClass("txt-result").addClass("txt-error");
			msgObj.html("이름과 본인인증정보가 일치하지 않습니다.");
			return;
		}

		checkCI(method, ci, di, gender);

	} else {

		$("#msg_parent_cert").show();
		var target = $("#msg_parent_cert")
		var msgObj = $("#msg_parent_cert").find("span");
		target.removeClass("txt-error").addClass("txt-result");

		if (result != "Y") {
			target.removeClass("txt-result").addClass("txt-error");
			msgObj.html("보호자 본인인증에 실패했습니다. 다시 시도해 주세요.");
			return;
		}

		$(".hide_parent_cert").hide();
		$("#parent_cert").val((method == "ipin" ? "보호자 아이핀 인증" : "보호자 휴대폰 인증"));
		$("#parent_name").val(name);
		$("#parent_juminId").val(birth.substring(2, 7) + "0000000");
		if (phone) $("#parent_mobile").val(phone);

		msgObj.addClass("guide_comment1");
		msgObj.html("보호자 동의 인증이 완료되었습니다.");

	}

};

// 실명인증 결과 응답
var cert_setNameCheckResult = function (result, ci, di, name, jumin, msg) {
	console.log(result, ci, di, name, jumin, msg);
	$("#msg_name_check").show();
	var target = $("#msg_name_check")
	var msgObj = $("#msg_name_check").find("span");
	target.removeClass("txt-error").addClass("txt-result");

	if (result != "Y") {
		target.removeClass("txt-result").addClass("txt-error");
		msgObj.html(msg);
		return;
	}

	msgObj.addClass("guide_comment1");
	msgObj.html("정보 확인이 완료되었습니다.");

	var gender = jumin.substr(6, 1);
	if (gender == 1 || gender == 3 || gender == 5) {
		gender = "M";
	} else {
		gender = "F";
	}
	checkCI("namecheck", ci, di, gender, jumin);
};

var checkCI = function (method, ci, di, gender, jumin) {

	$.post("/api/sponsor.ashx", { t: "sync", ci: ci, di: di, gender: gender, jumin: jumin }, function (r) {

		if (r.success) {

			if (r.action == "login") {
			    alert("본인인증된 계정이 이미 존재합니다. \n해당 계정으로 로그인하셔서 후원내역을 확인해주세요 \n(아이디 : " + r.data.user_id + ")");
			    // 로그인창오픈

			    location.href = "/login";

			} else if (r.action == "reload") {
				// 동기화 완료
				$("#ci").val(ci);
				$("#di").val(di);
				$("#gender").val(gender);
				$("#cert_gb").val(method);

				var msgObj = $("#msg_cert").find("span");
				$("#msg_cert").show();
				msgObj.removeClass("guide_comment1").removeClass("guide_comment2");
				msgObj.addClass("guide_comment1")
				msgObj.html("본인 인증이 완료되었습니다.");
				$(".func_cert").hide();
				if (jumin) {
					$("#jumin").val(jumin);
					$("#func_name_check").hide();
				}
			}
		} else {
			alert(r.message);
		}

	});

	return false;

};

// 기 승인된 회원이 있는경우 로그인변경 유도
var changeLogin = function () {
	alert("changeLogin");
};

(function () {
	
	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup, $address) {

		//교회찾기
	    $scope.findOrganization = function ($event) {
            
			$event.preventDefault();

			url = "/common/popup/organization";
			popup.init($scope, url, function (modal) {
				initOrganization($scope, $http, modal, function (id, name) {		// callback
					$("#hdOrganizationID").val(id);
					$("#church_name").val(name);
				});

			}, { top: 0, iscroll: true, removeWhenClose: true });

		}


		// 주소찾기
		// addressApiKey 제거
		$scope.findAddr = function ($event) {
			$event.preventDefault();

			$scope.scrollTop = $(document).scrollTop();
			$(".wrap-sectionsub").hide();

			popup.init($scope, "/common/popup/address", function (modal) {
				initAddress($scope, $address, modal, function (zipcode, addr1, addr2, jibun) {		// callback


					$("#addr_domestic_zipcode").val(zipcode);
					$("#addr_domestic_addr1").val(addr1 + "//" + jibun);
					$("#addr_domestic_addr2").val(addr2);

					// 화면에 표시
					$("#addr_domestic_1").val(zipcode);
					//$("#addr_domestic_2").val(addr2);

					$("#addr_road").text("[도로명주소] " + addr1 + " " + addr2);
					$("#addr_jibun").text("[지번주소] " + jibun + " " + addr2);

				}, function () {
					$(".wrap-sectionsub").show();
					window.scroll(0, $scope.scrollTop)
				});

			}, { top: 0, iscroll: true, removeWhenClose: true });

		}

		$scope.cmsAgreeInstance = null;
		$scope.showCMSAgree = function ($event) {
			$event.preventDefault();

			popup.init($scope, "/sponsor/pay/regular/pop-cms-agree.html", function (modal) {
				
				$scope.cmsAgreeInstance = modal;
				modal.show();

			}, { removeWhenClose: true });

		}

        //팝업
		$scope.selectNation = $("#ddlHouseCountry").val();

		$scope.changeValue = function () {
		    //console.log($scope.selectNation);
		    if ($scope.selectNation == "미국") {
		       // $scope.modal.show();
		    }
		}
        /*
		$scope.modal = {
		    instance: null,
		    show: function () {

		        if (!cookie.get("sponsor_usa_pop")) {
		            $scope.sponsor = $("#user_name").val()
		            popup.init($scope, "/sponsor/pay/regular/american-member", function (modal) {
		                $scope.modal.instance = modal;

		                modal.show();

		            }, {removeWhenClose: true, backgroundClick: true });
		        }

		    },

		    close: function ($event) {
		        $event.preventDefault();
		        if (!$scope.modal.instance)
		            return;

		        cookie.set("sponsor_usa_pop", "Y", 365 * 100);
		        $scope.modal.instance.hide();

		    }
		}
        */
	});
	
})();
