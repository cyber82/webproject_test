﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="cms_form.ascx.cs" Inherits="sponsor_pay_regular_cms_form" %>
<asp:PlaceHolder runat="server" ID="ph_content" Visible="false">

	<script type="text/javascript">

		$(function () {

			var status = $("#cms_status").val();
			if (status == "ready") {

				loading.show("결제중...");
				setTimeout(function () {
					checkStatus();
				}, 3000);

			} else {
				if (status == "mobile_empty") {
					alert("CMS 동의 전화 녹취를 위해서 휴대폰 번호가 필요합니다. 먼저 마이페이지에서 휴대폰번호를 등록해주세요");
				}
			}
			
			var checkStatusInterval = null;
			var checkStatus = function () {

				checkStatusInterval = setInterval(function () {

					$.get("/sponsor/pay/regular/cms.ashx?t=verify&agreementAgentId=" + $("#agreementAgentId").val(), function (r) {

						console.log(r);
						if (r.success) {
							clearInterval(checkStatusInterval);
							$("#signed_data").val(r.data);
							document.ex_frm.target = "hd_ex_frm";
							document.ex_frm.submit();
						}

					})

				}, 1500);

			};

		})

	</script>
		<div style="display:none">
	<iframe id="hd_ex_frm" name="hd_ex_frm" style="width:1000px;height:500px;border:1px solid red"></iframe>
	<form id="ex_frm" name="ex_frm" method="post" action="/sponsor/pay/regular/cms">

		<input type="hidden" runat="server" id="cms_status" />
		<input type="hidden" runat="server" id="agreementAgentId" />
		<input type="hidden" id="signed_data" name="signed_data" />
		<input type="hidden" id="relation" name="relation" value="<%:this.ViewState["relation"].ToString() %>"/>
        <input type="hidden" id="cms_owner" name="cms_owner" value="<%:this.ViewState["cms_owner"].ToString() %>"/>
		<input type="hidden" id="cms_account" name="cms_account" value="<%:this.ViewState["cms_account"].ToString() %>"/>
		<input type="hidden" id="birth" name="birth" value="<%:this.ViewState["birth"].ToString() %>"/>
		<input type="hidden" id="bank_name" name="bank_name" value="<%:this.ViewState["bank_name"].ToString() %>"/>
		<input type="hidden" id="bank_code" name="bank_code" value="<%:this.ViewState["bank_code"].ToString() %>"/>
		<input type="hidden" id="cmsday" name="cmsday" value="<%:this.ViewState["cmsday"].ToString() %>"/>

		<input type='hidden' name='returnUrl'      value='<%:this.ViewState["returnUrl"].ToString() %>'>
		<input type='hidden' id="motiveCode" name='motiveCode'      value='<%:this.ViewState["motiveCode"].ToString() %>'>
		<input type='hidden' id="motiveName" name='motiveName'      value='<%:this.ViewState["motiveName"].ToString() %>'>
		<input type='hidden' id="payInfo" name='payInfo'      value='<%:this.ViewState["payInfo"].ToString() %>'>
		<input type="hidden" id="ordr_idxx" name="ordr_idxx" value="<%:this.ViewState["orderId"].ToString() %>" />

		<!--// KCP -->
	</form>
			</div>
</asp:PlaceHolder>