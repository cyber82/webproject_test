﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using CommonLib;

public partial class sponsor_pay_regular_cms_form : System.Web.UI.UserControl {
	
	public void Show( StateBag state) {

		this.ViewState["src"] = state["src"];		// 서명할 내용
		this.ViewState["relation"] = state["relation"];
		this.ViewState["cms_owner"] = state["cms_owner"];
		this.ViewState["cms_account"] = state["cms_account"];
		this.ViewState["birth"] = state["birth"];	// 사업자번호로도 사용
		this.ViewState["bank_name"] = state["bank_name"];
		this.ViewState["bank_code"] = state["bank_code"];
		this.ViewState["cmsday"] = state["cmsday"];

		//this.ViewState["returnUrl"] = state["returnUrl"];
		
		this.ViewState["payItem"] = state["payItem"];
		var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();
		this.ViewState["payInfo"] = payItem.data;
		this.ViewState["orderId"] = payItem.orderId;

		var extra = payInfo.extra.Decrypt().ToObject<Dictionary<string, object>>();
		this.ViewState["motiveCode"] = extra["motiveCode"].ToString();
		this.ViewState["motiveName"] = extra["motiveName"].ToString();
		this.ViewState["returnUrl"] = extra["successUrl"].ToString();
		this.ViewState["good_mny"] = state["good_mny"];
		var sess = new UserInfo();

		cms_status.Value = "ready";

		if(string.IsNullOrEmpty(sess.Mobile)) {
			cms_status.Value = "mobile_empty";
		} else {
			var res = new CMSARS().Request(sess.UserId, sess.Mobile, this.ViewState["bank_code"].ToString().Substring(1) , this.ViewState["bank_name"].ToString() ,
				this.ViewState["cms_account"].ToString() , this.ViewState["cms_owner"].ToString(), this.ViewState["birth"].ToString());
			agreementAgentId.Value = res.agreementAgentId;


			// sponsorID가 없는 상태에서 해외회원으로 체크하고 결제하는 경우 sponsorID로 ensure 되지 않기때문에,
			// 다시한번 ensure 한다.
			if(payInfo.type == PayItemSession.Entity.enumType.CDSP) {
				var childAction = new ChildAction();
				childAction.Ensure(payInfo.childMasterId);
			}

		}


		ph_content.Visible = true;
	}

	protected override void OnLoad(EventArgs e) {
		
		base.OnLoad(e);
		
	}


}
