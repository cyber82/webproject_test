﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cms.aspx.cs" Inherits="sponsor_pay_regular_cms" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title></title>
	<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript">

		// 결제 중 새로고침 방지 샘플 스크립트
		function noRefresh() {
			/* CTRL + N키 막음. */
			if ((event.keyCode == 78) && (event.ctrlKey == true)) {
				event.keyCode = 0;
				return false;
			}
			/* F5 번키 막음. */
			if (event.keyCode == 116) {
				event.keyCode = 0;
				return false;
			}
		}
		document.onkeydown = noRefresh;

		$(function () {

			if ($("#result").val() == "Y") {
			//	alert("자동이체 신청이 완료되었습니다\nCMS 계좌 자동이체가 승인되기 전까지는 지로로 납부방법이 \n설정되오니 착오 없으시기 바랍니다.\nCMS 계좌 자동이체가 승인완료 되는 데에는 최대 일주일이 \n소요됩니다.");
				var url = $("#returnUrl").val();
				parent.location.href = url;
			} else {
				parent.location.href = "/sponsor/pay/fail?t=regular";
			}

		})
	</script>
</head>
<body>
	<form runat="server" id="frm">
		
		<input type="hidden" id="result" runat="server" value="" />
        <input type="hidden" id="msg" runat="server" value="" />
		<input type="hidden" id="returnUrl" runat="server" value="" />
        <input type="hidden" id="failUrl" runat="server" value="" />
	</form>
</body>
</html>

