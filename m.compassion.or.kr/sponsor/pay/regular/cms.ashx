﻿<%@ WebHandler Language="C#" Class="sponsor_pay_regular_cms_ashx" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class sponsor_pay_regular_cms_ashx : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest(HttpContext context) {

		var t = context.Request["t"].EmptyIfNull();
		if(t == "verify") {
			this.Verify(context);
		}
	}

	void Verify(HttpContext context){

		var result = new JsonWriter(){
			success = false
		};

		var agreementAgentId = context.Request["agreementAgentId"];

		var res = new CMSARS().Verify(agreementAgentId);

		switch(res.result.flag) {
			case "Y":
				result.success = true;
				result.data = res.ToJson();
				break;
			default:    // null (대기)
						// 일정시간 후 다시 요청 , Y or N이 나올때까지 요청(timeout 필요)
				break;
		}

		result.Write(context);

	}



}