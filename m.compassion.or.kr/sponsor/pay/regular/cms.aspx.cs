﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

using CommonLib;
using System.Data.Linq;
using System.Linq;

public partial class sponsor_pay_regular_cms : MobileFrontBasePage {

	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}

	string signed_data = "";
	string src = "";
	string relation = "";
	string cms_owner = "";
	string cms_account = "";
	string birth = "";
	string bank_name = "";
	string bank_code = "";
	string cmsday = "";
	string ordr_idxx = "";
	string motiveCode;          // 동기코드
	string motiveName;          // 동기명
	string pi;
	
	WWWService.Service _wwwService = new WWWService.Service();
	
	protected override void OnBeforePostBack() {

		returnUrl.Value = Request["returnUrl"];
		signed_data = Request.Form["signed_data"];
		src = Request["src"];
		relation = Request["relation"];
		cms_owner = Request["cms_owner"];
		cms_account = Request["cms_account"];
		birth = Request["birth"];
		bank_name = Request["bank_name"];
		bank_code = Request["bank_code"];
		cmsday = Request["cmsday"];
		ordr_idxx = Request["ordr_idxx"];
		motiveCode = Request["motiveCode"];
		motiveName = Request["motiveName"];
		pi = Request["payInfo"];
		
		this.DoSave();
	

	}
	

	bool DoSave() {
		
		if(!UserInfo.IsLogin) {
			base.AlertWithJavascript("로그인 사용자만 결제 가능합니다.");
			result.Value = "N";
			return false;
		}

		UserInfo sess = new UserInfo();

		if(string.IsNullOrEmpty(sess.SponsorID)) {
			base.AlertWithJavascript("후원회원만 등록가능합니다.");
			result.Value = "N";
			return false;
		}

		var action = new CommitmentAction();
		JsonWriter actionResult = new JsonWriter();
		var payInfo = pi.ToObject<PayItemSession.Entity>();
		
		
		if(payInfo.group == "CDSP") {
			actionResult = action.DoCDSP(payInfo.childMasterId, payInfo.childKey, payInfo.campaignId, payInfo.codeName, motiveCode, motiveName);
		} else {
			actionResult = action.DoCIV(sess.SponsorID, payInfo.amount, 1, "", payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.childMasterId, payInfo.campaignId, motiveCode, motiveName);
		}

		//Response.Write(result.ToJson());

		if(!actionResult.success) {

			base.AlertWithJavascript(actionResult.message);
			return false;
		}

		var commitmentId = actionResult.data.ToString();

		if(!this.CheckRegisteredAccount(sess.SponsorID, bank_code, cms_account.Replace("-", ""))) {

			return false;
		}

		if(existAccount) {

			// 즉시결제
			return this.DirectPay(commitmentId, action, payInfo);

		} else {
			bool isCmsComplete = RegistCMS();

			if(isCmsComplete) {
				//기존결제방법 가져오기 
				DataSet dsPaymentType = new DataSet();
				if(!GetCheckPaymentAccountData(sess.SponsorID, ref dsPaymentType)) {
					return false;
				}

				//추가 지로초기추가 (2012-02-15)
				if(dsPaymentType.Tables[0].Rows.Count == 0) {

					string sTimeStamp = _wwwService.GetTimeStamp(); //타임스탬프
					sTimeStamp = sTimeStamp.Substring(0, sTimeStamp.Length - 1) + "0";

					//지로 저장하는 웹서비스
					_wwwService.CMSInitRegisterWeb(sess.SponsorID, "", "", sTimeStamp, sess.UserName, "", sess.Jumin
						, CodeAction.ChannelType, DateTime.Now.ToString("yyyy-MM-dd"), sess.UserId, sess.UserName);
				}

                // [이종진] [질문] 기존 납부방법이 없으면 결제를 하지 않는가?
                // www는 DirectPay()를 타도록 되어있음.
				//	this.DirectPay(commitmentId, action, payInfo);
				result.Value = "Y";
				return true;

			}


			return isCmsComplete;
		}

	}

	bool DirectPay( string commitmentId, CommitmentAction action, PayItemSession.Entity payInfo ) {
        // 즉시결제
        #region old
        //old
        //var actionResult = new BatchPay().Pay(commitmentId, ordr_idxx, payInfo.amount);

        //if (actionResult.success)
        //{
        //    result.Value = "Y";
        //    new ChildAction().Release(payInfo.childMasterId);
        //    return true;
        //}
        //else
        //{
        //    action.DeleteCommitment(commitmentId);
        //    base.AlertWithJavascript(actionResult.message);
        //    return false;
        //}
        #endregion

        //new 2017.06.13 김태형 : www 참조로 수정
        #region new
        var actionResult = new BatchPay().Pay("", commitmentId, ordr_idxx, "한국컴패션후원금", payInfo.amount, "CMS", cms_account.Replace("-", ""), "", bank_code, birth, cms_owner, payInfo.group);

        result.Value = "Y";
        //[이종진]2017-12-20 : Global Pool에서 홀딩푸는 로직은 수행하지 않음
        new ChildAction().Release(payInfo.childMasterId, false);

        bool success = true;

        if (actionResult.message != null)
        {
            success = false;
            msg.Value =
            //string.Format(@"정기 후원 신청이 완료되었으나,{0}첫 후원금 결제가 정상적으로 처리되지 않았습니다. 첫 후원금을 다시 결제하시겠습니까?.",
            //string.IsNullOrEmpty(actionResult.message) ? "" : string.Format("[{0}]의 사유로 ", actionResult.message));
            string.Format(@"정기 후원 신청이 완료되었으나, 첫 후원금 결제가 정상적으로 처리되지 않았습니다. 첫 후원금을 다시 결제하시겠습니까?");

            failUrl.Value = "/sponsor/pay/regular/pay_again?motiveCode=" + motiveCode + "&motiveName=a";// + motiveName;
        }

        if (actionResult.success && success)
        {
            return true;
        }
        else
        {
            //action.DeleteCommitment(commitmentId);
            return false;
            /*첫 결재 실패 로직 변경 2017-01-13 이석호 과장 요청*/
            //1.메시지 변경
            //2. 메시지 표시 후 
        }


        // 즉시결제 실패해도 결연 성공으로 처리 
        // 2016-10-24 이석호 과장 요청
        #endregion
    }

    bool existAccount = false;
	bool CheckRegisteredAccount( string sSponsorID, string sBankCode, string sBankAccount ) {
		UserInfo sess = new UserInfo();

		try {
			var dsPaymentType = _wwwService.selectUsingPaymentAccount(sSponsorID, "0004");
			if(dsPaymentType.Tables[0].Rows.Count > 0) {
				var account = dsPaymentType.Tables[0].Rows[0]["Account"].ToString().Trim();
				var bankCode = dsPaymentType.Tables[0].Rows[0]["bankCode"].ToString().Trim();

				if(sBankCode == bankCode && sBankAccount == account) {
					existAccount = true;
				}
			}

			return true;
		} catch {
			base.AlertWithJavascript("기존 납부방법을 가져오는 중 오류가 발생했습니다.");
			result.Value = "N";
			return false;
		}
	}

	bool GetCheckPaymentAccountData( string sSponsorID, ref DataSet dsPaymentType ) {
		UserInfo sess = new UserInfo();

		try {
			dsPaymentType = _wwwService.selectUsingPaymentAccount(sSponsorID, "");
			return true;
		} catch {
			base.AlertWithJavascript("납부방법을 가져오는 중 오류가 발생했습니다.");
			result.Value = "N";
			return false;
		}

	}

	bool RegistCMS() {

		UserInfo sess = new UserInfo();

		//----- Defien, set Data --------------------------------------------------------------
		DataSet dsPaymentAccount = new DataSet();

		//추가 2013-01-28
		DataSet dsPayWay = new DataSet();

		string sResCD = string.Empty;
		string sSign = string.Empty;
		string sCount = string.Empty;
		string sConID = string.Empty;

		//추가 2013-01-28
		string sJiroNo = string.Empty;
		string sRefuseNewID = DateTime.Now.ToString("yyyyMMddHHmmssff2");
		string payway = string.Empty;
		string paycode = string.Empty;

		//- 시스템코드 View
		DataView dvCode = new DataView(); //View생성

		WWWService.Service _wwwService = new WWWService.Service();

		//추가 2013-01-28
		//----- 전체에서 결제방법을 가져온다 ---------------------------------------------------
		try {
			dsPayWay = _wwwService.Payway(sess.SponsorID.ToString().Trim());
			DataSet data = _wwwService.MypageApplyChildInfo(sess.SponsorID.Trim(), "", "", 0, 9999, "");
			DataSet dsList = _wwwService.ComplementaryApplySearch(sess.SponsorID.Trim());
			int dataCnt = data.Tables[0].Rows.Count;
			int dsListCnt = dsList.Tables[0].Rows.Count;

			if(dsPayWay != null) {
				if(dsPayWay.Tables["BankT"].Rows.Count > 0 && (dataCnt > 0 || dsListCnt > 0)) {
					payway = dsPayWay.Tables["BankT"].Rows[0]["PaymentName"].ToString().Trim();

					switch(dsPayWay.Tables["BankT"].Rows[0]["PaymentName"].ToString().Trim()) {
						case "신용카드자동결제":
							paycode = "0003";
							break;

						case "지로":
							paycode = "0005";
							break;

						case "CMS":
							paycode = "0004";
							break;

						case "가상계좌":
							paycode = "0001";
							break;

						case "미주":
							paycode = "0008";
							break;

						case "휴대폰자동결제":
							paycode = "0020";
							break;

						default:
							paycode = "9999";
							break;
					}
				} else {
					paycode = "9999";
				}
			} else {
				paycode = "9999";
			}
		} catch(Exception ex) {
			//Exception Error Insert
			_wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);
			result.Value = "N";
			base.AlertWithJavascript("납부방법을 가져오는중 오류입니다.");

			return false;
		}


		//----- 결제방법을 가져온다 -----------------------------------------------------------
		if(sess.SponsorID != "" && sess.SponsorID != null) {
			try {
				dsPaymentAccount = _wwwService.selectUsingPaymentAccount(sess.SponsorID, "");

				if(dsPaymentAccount.Tables.Count <= 0) {
					result.Value = "N";
					base.AlertWithJavascript("결제방법 가져오기에 실패했습니다.");
					return false;
				}
			} catch(Exception ex) {
				result.Value = "N";
				base.AlertWithJavascript("결제방법 가져오기에 실패했습니다.");
				return false;
			}


			DateTime dtDate2 = _wwwService.GetDate();
			//---- 데이터가 없으면 CMS신청만 한다. ------------------------------------------------
			if(dsPaymentAccount.Tables[0].Rows.Count <= 0) {


				try {
					//- 카운트 가져오기
					sCount = _wwwService.countTableTerm(dtDate2);

					//- 7자리 맞추기
					sCount = Convert.ToString(Convert.ToInt32(sCount) + 1).PadLeft(7, '0');

					//- CMS신청시 수정
					sResCD = _wwwService.insertCMSApplication(dtDate2
															, sCount
															, sess.SponsorID
															, cms_owner
															, bank_code
															, bank_name
															, cms_account.Replace("-", "")
															, cmsday
															, birth
															, sess.SponsorID
															, sess.UserName);

					//- CMS 자동이체 신청시 전자서명값 등록
					try {
				//		sSign = _wwwService.updateCmsSignData(sess.SponsorID, signed_data);

						//추가 2013-01-28  결제방법이 지로일때 지로 수신거부 설정 (결제방법이 없을때에도 포함)
						if((paycode == "0004") || (paycode == "0005") || (paycode == "9999")) {
							try {
								sJiroNo = _wwwService.CMSJiroNoSet(sess.SponsorID, sRefuseNewID, sess.UserName);
								return true;
							} catch {
								base.AlertWithJavascript("지로 수신거부 등록이 실패했습니다.");
								result.Value = "N";
								return false;
							}
						} else {
							return true;
						}
					} catch(Exception ex) {
						base.AlertWithJavascript("전자서명값 등록이 실패했습니다.");
						result.Value = "N";
						return false;
					}
				} catch(Exception ex) {
					base.AlertWithJavascript("CMS계좌 등록이 실패했습니다.");
					result.Value = "N";

					return false;
				}
			} else {
				//- CMS사용중이면 CMStoCMS를 한다.
				try {

					sResCD = _wwwService.insertCMStoCMSApplication(sess.SponsorID
																, cms_owner
																, bank_code
																, bank_name
																, cms_account
																, birth
																, cmsday
																, dtDate2
																, dsPaymentAccount.Tables[0].Rows[0]["PaymentType"].ToString()
																, dsPaymentAccount.Tables[0].Rows[0]["BankCode"].ToString()
																, dsPaymentAccount.Tables[0].Rows[0]["Account"].ToString()
																, dsPaymentAccount.Tables[0].Rows[0]["PaymentAccountID"].ToString()
																, sess.SponsorID
																, sess.UserName
																);

					//- CMS 자동이체 신청시 전자서명값 등록
					try {
				//		sSign = _wwwService.updateCmsSignData(sess.SponsorID, signed_data);

						try {
							sJiroNo = _wwwService.CMSJiroNoSet(sess.SponsorID, sRefuseNewID, sess.UserName);
							return true;
						} catch {
							base.AlertWithJavascript("지로 수신거부 등록이 실패했습니다.");
							result.Value = "N";
							return false;
						}

					} catch(Exception ex) {
						base.AlertWithJavascript("전자서명값 등록이 실패했습니다.");
						result.Value = "N";
						return false;
					}
				} catch(Exception ex) {
					base.AlertWithJavascript("CMS계좌 등록이 실패했습니다.");
					result.Value = "N";
					return false;
				}

			}

			//----- 결과 출력 ---------------------------------------------------------------------
			if(sResCD == "10") {
				return true;
			} else {
				base.AlertWithJavascript("CMS계좌 등록이 실패했습니다.");
				result.Value = "N";
				return false;
			}

			if(sSign == "10") {
				return true;
			} else {
				base.AlertWithJavascript("전자서명값 등록이 실패했습니다.");
				result.Value = "N";
				return false;
			}

			if(sJiroNo == "10") {
				return true;
			} else {
				base.AlertWithJavascript("지로 수신거부 등록이 실패했습니다.");
				result.Value = "N";
				return false;
			}

		} else {

			base.AlertWithJavascript("연결이 끊어졌습니다. 다시 로그인하여 주시기 바랍니다.");
			result.Value = "N";

			return false;
		}


		return true;
	}


}
