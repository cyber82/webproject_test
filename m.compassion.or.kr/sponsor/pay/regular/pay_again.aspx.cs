﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_pay_regular_payagain : SponsorPayBasePage
{
    public override bool RequireSSL
    {
        get
        {
            return true;
        }
    }

    public override bool NoCache
    {
        get
        {
            return true;
        }
    }

    protected override void OnBeforePostBack()
    {
        
            //base.OnBeforePostBackTemporary();
            base.OnBeforePostBackPayAgain();

        //btnList.HRef = "/sponsor/";

        //// 결제실패에서 넘어온경우 후원 메인으로 보낸다.
        //if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.IndexOf("/sponsor/pay/fail/") < 0)
        //    btnList.HRef = Request.UrlReferrer.AbsoluteUri;

        var payInfo = PayItemSession.GetCookie(this.Context);
        this.ViewState["overseas_card"] = "N";
        this.ViewState["good_mny"] = payInfo.amount.ToString();

        //base.AlertWithJavascript(payInfo.ToJson());

        
        motiveCode.Value = Request.QueryString["motiveCode"].EmptyIfNull();
        motiveName.Value = Request.QueryString["motiveName"].EmptyIfNull();

        hd_amount.Value = payInfo.amount.ToString();

        //if (!UserInfo.IsLogin)
        //{

        //    if (Request["noname"] == "Y")
        //    {
        //        noname.Checked = true;
        //        p_receipt_pub_ok.Checked = false;
        //    }
        //    else
        //    {
        //        ph_notuser1.Style["display"] = "block";
        //    }
        //}

        // 후원내역
        var childAction = new ChildAction();

        //	Response.Write(payInfo.ToJson());

        if (payInfo.type == PayItemSession.Entity.enumType.CDSP)
        {

            view_child.Visible = true;

            if (string.IsNullOrEmpty(payInfo.childMasterId))
            {
                Response.Redirect("/sponsor/children/");
                return;

            }

            //var actionResult = childAction.Ensure();
            //if (!actionResult.success)
            //{
            //    base.AlertWithJavascript(actionResult.message, "goBack()");
            //    return;
            //}

        }
        else if (payInfo.type == PayItemSession.Entity.enumType.SPECIAL_FUNDING)
        {
            view_sf.Visible = true;
        }
        else if (payInfo.type == PayItemSession.Entity.enumType.USER_FUNDING)
        {

            view_uf.Visible = true;
            /*
			if(payInfo.IsEmptyChildMasterId) {
				view_uf.Visible = true;
			} else {
				view_child.Visible = true;
			}
			*/
        }


        // 결제정보를 디비에 저장하고 이후 결제프로세스는 디비의 값을 참조한다.
        var data = new PayItemSession.Store().Create();
        if (data == null)
        {
            base.AlertWithJavascript("후원정보 임시저장에 실패했습니다.", "goBack()");
            return;
        }

        this.ViewState["payItem"] = data.ToJson();

        //
        if (this.ViewState["kakaopayAgain"] != null) {
            base.AlertWithJavascript(this.ViewState["kakaopayAgain"].ToString());
        }

        //payInfo.extra.isKakao == true


        //var payItemTemp = new PayItemSession.Store().Get(ordr_idxx);
        //var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

        //var payItemTemp = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
        //var payInfoTemp = payItem.data.ToObject<PayItemSession.Entity>();


        if (payInfo.extra != null) {
            if (payInfo.extra.Decrypt().ToObject<Dictionary<string, object>>().ContainsKey("isKakao"))
            {
                string a = payInfo.extra.Decrypt().ToObject<Dictionary<string, object>>()["isKakao"].ToString();
                if (a == "true")
                {
                    kakaopay_form.Show(this.ViewState);
                }
            }
        }
        
        
    }

    // 결제
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        //kakaopay_form.Hide();
        //kcp_form.Hide();
        //payco_form.Hide();

        if (base.IsRefresh)
        {
            return;
        }

        if (!base.UpdateAgainUserInfo())
        {
            return;
        }

        JsonWriter result = new JsonWriter();
        var sess = new UserInfo();
        var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
        var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

        var action = new CommitmentAction();

        //action.ValidateCIV(sess.SponsorID, payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.campaignId, ref result);

        //if (!result.success)
        //{
        //    base.AlertWithJavascript(result.message);
        //    return;
        //}

        //// 경고메세지를 띄우는 경우
        //if (result.action != null && result.action == "confirm")
        //{
        //    base.ConfirmWithJavascript(result.message, "goBack()");
        //}

        // 결제정보를 세팅
        //base.SetTemporaryPaymentInfo();
        this.ViewState["jumin"] = jumin.Value;
        this.ViewState["ci"] = ci.Value;
        this.ViewState["di"] = di.Value;

        this.ViewState["used_card_YN"] = "Y";
        this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_domestic"];

        if (payment_method_card.Checked)
        {
            this.ViewState["pay_method"] = "100000000000";
            this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_web_card"];

        }
        else if (payment_method_cms.Checked)
        {
            this.ViewState["pay_method"] = "010000000000";
            this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_account"];

        }
        else if (payment_method_phone != null && payment_method_phone.Checked)
        {
            this.ViewState["pay_method"] = "000010000000";
            this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_phone"];

        }
        else if (payment_method_oversea.Checked)
        {
            this.ViewState["pay_method"] = "100000000000";
            this.ViewState["overseas_card"] = "Y";
            this.ViewState["used_card_YN"] = "Y";
            this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_oversea"];
            this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_web_card_Foreign"];
        }

        if (payment_method_card.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card;
        }
        else if (payment_method_cms.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.cms;
        }
        else if (payment_method_oversea.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card_oversea;
        }
        else if (payment_method_phone.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.phone;
        }
        else if (payment_method_payco.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.payco;
        }
        else if (payment_method_kakao.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.kakao;
        }

        var orderId = PayItemSession.Store.GetNewOrderID();


        this.ViewState["buyr_name"] = user_name.Value;



        payInfo.extra = new Dictionary<string, object>() {
            {"jumin" , ViewState["jumin"].ToString() } ,
            {"ci" , ViewState["ci"].ToString() },
            {"di" , ViewState["di"].ToString() },
            {"motiveCode" , "" },
            {"motiveName" , "" },
            {"overseas_card" , ViewState["overseas_card"].ToString() },

            //[jun.heo] 2017-12 : 결제 모듈 통합
            {"successUrl" , "/pay/complete_sponsor/" + orderId } ,
            {"failUrl" , string.Format("/pay/fail_sponsor/?r={0}" , HttpUtility.UrlEncode("/sponsor/pay/temporary/?noname=" + Request["noname"])) },
            //{"successUrl" , "/sponsor/pay/complete/" + orderId } ,
            //{"failUrl" , string.Format("/sponsor/pay/fail/?r={0}" , HttpUtility.UrlEncode("/sponsor/pay/temporary/?noname=" + Request["noname"])) },
            {"isKakao", "true"},
            {"buyr_name", user_name.Value}
        }.ToJson().Encrypt();

        //PayItemSession.SetCookie(this.Context, new PayItemSession.Entity() { extra = payInfo.extra });

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "Temporary";  // 결제 페이지 종류 

        payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo, orderId);
        //payItem.data = payInfo.ToJson();
        this.ViewState["payItem"] = payItem.ToJson();

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["returnUrl"] = "/pay/complete_sponsor/" + payItem.orderId;
        //this.ViewState["returnUrl"] = "/sponsor/pay/complete/" + payItem.orderId;
        this.ViewState["firstPay"] = "1";
        this.ViewState["payAgain"] = "1";

        kakaopay_form.Hide();
        kcp_form.Hide();
        payco_form.Hide();

        if (payment_method_payco.Checked)
        {
            payco_form.Show(this.ViewState);
        }
        else if (payment_method_kakao.Checked)
        {
            this.ViewState["kakaopayAgain"] = true;
            kakaopay_form.Show(this.ViewState);

            
        }
        else
        {
            kcp_form.Show(this.ViewState);
        }

    }
}