﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
using TCPTModel.Request.Supporter;
using Newtonsoft.Json;
using TCPTModel;
using TCPTModel.Response.Supporter;
using TCPTModel.Request.Hold.Beneficiary;

public partial class sponsor_regular_default : SponsorPayBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}
    private delegate void DoStuff(); //delegate for the action
    public string SponsorID;
    public string ChildMasterID;
    public string CommitmentID;

	protected override void OnBeforePostBack() {
		base.OnBeforePostBackRegular();


		btnList.HRef = "/sponsor/";
		if(Request.UrlReferrer != null)
			btnList.HRef = Request.UrlReferrer.AbsoluteUri;

		var payInfo = PayItemSession.GetCookie(this.Context);
		hd_amount.Value = payInfo.amount.ToString();

		// 후원내역
		var childAction = new ChildAction();

		hd_title.Value = payInfo.frequency + "후원";
		
		if(payInfo.type == PayItemSession.Entity.enumType.CDSP) {

			view_child.Visible = true;

			var actionResult = childAction.Ensure();
			if(!actionResult.success) {
				base.AlertWithJavascript(actionResult.message, "goBack()");
				return;
			}

		} else if(payInfo.type == PayItemSession.Entity.enumType.SPECIAL_FUNDING) {
			pn_letter.Style["display"] = "none";

			view_sf.Visible = true;
		} else if(payInfo.type == PayItemSession.Entity.enumType.CSP_WEDDING) {

			if(payInfo.frequency == "정기") {
				view_child.Visible = true;
				
			} else {
				pn_letter.Style["display"] = "none";
			}
		}

		// 결제정보 페이지 데이타
		p3_frequency.Text = payInfo.frequency;
		if(payInfo.frequency == "정기") {

			p3_amount.Text = string.Format("{0} 원" , payInfo.amount.ToString("N0"));
			p3_tot_amount.Text = string.Format("{0} 원/월", payInfo.amount.ToString("N0"));
			p3_tot_amount_title.Text = "매월 결제 금액";
		} else {
			p3_amount.Text = string.Format("{0} 원", payInfo.amount.ToString("N0"));
			p3_tot_amount.Text = string.Format("{0} 원", payInfo.amount.ToString("N0"));
			p3_tot_amount_title.Text = "총 결제 금액";
		}


		// 결제정보를 디비에 저장하고 이후 결제프로세스는 디비의 값을 참조한다.
		var data = new PayItemSession.Store().Create();
		if(data == null) {
			base.AlertWithJavascript("후원정보 임시저장에 실패했습니다.", "goBack()");
			return;
		}
		 
		this.ViewState["payItem"] = data.ToJson();
		

		cms_owner.Value = user_name.Value;
		if (!string.IsNullOrEmpty(hdBirthDate.Value))
			cms_birth.Value = hdBirthDate.Value.Substring(2);
	}
	
	// 결제
	protected void btn_submit_Click( object sender, EventArgs e ) {
		
		if(base.IsRefresh) {
			return;
		}
		
		if(!base.UpdateRegularUserInfo())
			return;

		pn_step1.Style["display"] = pn_step2.Style["display"] = "none";
		pn_step3.Style["display"] = "block";

		UserInfo sess = new UserInfo();
		var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

		var sponsorId = sess.SponsorID;
        
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();

        // 첫결연이고 본인인증을 하지 않는 경우(기업,국외) sponsorId 예외처리
        if (string.IsNullOrEmpty(sess.SponsorID)) {
			if(this.ViewState["sponsorId"] == null) {
				base.AlertWithJavascript("후원회원 정보가 없습니다.");
				return;
			} else {
				sponsorId = this.ViewState["sponsorId"].ToString();
			}
		}

		var result = new JsonWriter();
	
		if(payment_method_card.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card;
		} else if(payment_method_cms.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.cms;
		} else if(payment_method_oversea.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card_oversea;

			// 국외회원 첫 결연시 sponsorID를 업데이트 시 얻어 오기때문에 다시 Keep 한다.
			// card , cms 는 form 에서 ensure 처리
			if (payInfo.type == PayItemSession.Entity.enumType.CDSP) {
				var result2 = new ChildAction().Keep(sponsorId, payInfo.childMasterId);
				if (!result2.success) {
					base.AlertWithJavascript(result2.message, "goBack()");
					return;
				}
			}

		} else if(payment_method_giro.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.jiro;
		} else if(payment_method_virtualaccount.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.virtualAccount;
		}

		var orderId = PayItemSession.Store.GetNewOrderID();
		payInfo.extra = new Dictionary<string, object>() {
			{"motiveCode" , ViewState["motiveCode"].ToString() },
			{"motiveName" , ViewState["motiveName"].ToString() },
			{"overseas_card" , payment_method_oversea.Checked ? "Y" : "N" },

            //[jun.heo] 2017-12 : 결제 모듈 통합
			{"successUrl" , "/pay/complete_sponsor/" + orderId } ,
			{"failUrl" , "/pay/fail_sponsor/?r=" + HttpUtility.UrlEncode("/sponsor/pay/regular/") }
            //{"successUrl" , "/sponsor/pay/complete/" + orderId } ,
            //{"failUrl" , "/sponsor/pay/fail/?r=" + HttpUtility.UrlEncode("/sponsor/pay/regular/") }
        }.ToJson().Encrypt();

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["returnUrl"] = "/pay/complete_sponsor/" + orderId;
        //this.ViewState["returnUrl"] = "/sponsor/pay/complete/" + orderId;

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "Regular";  // 결제 페이지 종류 

        payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo , orderId);
		//payItem.data = payInfo.ToJson();
		this.ViewState["payItem"] = payItem.ToJson();
        //이종진 2018-03-12 - 결제실패 시, pay-again 페이지로 가기위한 url을 들고다님
        this.ViewState["againUrl"] = "/sponsor/pay/regular/pay_again?motiveCode=" + motiveCode.Value + "&motiveName=a";

        //[이종진]신규 추가 - 모바일에서는 기존결제방법이 신용카드결제가 아니라면(cms, 해외카드결제가 모바일에선 현재 안됨),
        //                   결연정보등록까지하고 pay-again으로 보낸다 (결연처리 후, pay-again에서 다시 결제) 
        //                   pay-again에서 결제하지 않을 경우, compass4에서 현업이 처리(전화통화, 문자메시지 등)
        if (payInfo.type == PayItemSession.Entity.enumType.CDSP //1:1후원
            && exist_account.Value == "Y"   //기존결재 방법존재
            && !payment_method_card.Checked)    //신용카드 결제가 아님
        {
            //어린이정보 insert, nomoneyHold, 결연정보등록
            result = new CommitmentAction().DoCDSP(payInfo.childMasterId, payInfo.childKey, payInfo.campaignId, payInfo.codeName, motiveCode.Value, motiveName.Value);
            if(result.success)
            {
                new ChildAction().Release(payInfo.childMasterId, false);
                base.ConfirmWithJavascript(
                            "후원 신청이 완료되었습니다.\\n\\r첫 후원금 결제를 바로 진행하시겠습니까?",
                            "location.href= '" + this.ViewState["returnUrl"].ToString() + "'",
                            "location.href='" + this.ViewState["againUrl"].ToString() + "'"
                            );
            }
            else
            {
                //Response.Redirect(this.ViewState["returnUrl"].ToString());
                base.AlertWithJavascript("결연에 실패하였습니다");
            }
            return;
        }

		if(payInfo.type == PayItemSession.Entity.enumType.CDSP) {
			result = this.DoPayCDSP(sponsorId);

            // TCPT 관련 SponsorGlobalID 생성 및 어린이 Hold 상태 변경 및 Kit 발송 테이블에 Insert
            // [이종진] 신규방법 strdbgp_kind == "2" 이면, pay()에서 CommitmentAction.ProcSuccessPay() 에서 모두 처리하므로
            // 기존방법일 경우만, 해당 로직 타도록 함
            if (result.success && result.data != null && strdbgp_kind == "1")
            {
                SponsorID = sponsorId;
                ChildMasterID = payInfo.childMasterId;
                CommitmentID = result.data.ToString();
                //DoStuff myAction = new DoStuff(DoTCPT(result.data.ToString(), sponsorId, payInfo.childMasterId));
                DoStuff myAction = new DoStuff(DoTCPT);
                //invoke it asynchrnously, control passes to next statement
                myAction.BeginInvoke(null, null);
            }

		} else if(payInfo.type == PayItemSession.Entity.enumType.SPECIAL_FUNDING) {
			result = this.DoPayCIV(sponsorId);
		} else {
			Response.Redirect("/sponsor/children/");
		}

		cms_form.Visible = false;
		kcp_form.Visible = false;
		kcp_form_temporary.Visible = false;

		this.ViewState["good_mny"] = hd_amount.Value;
		this.ViewState["payItem"] = payItem.ToJson();

		if(result.success) {
			if(payment_method_card.Checked) {
				kcp_form.Visible = true;
				kcp_form.Show(this.ViewState);

			} else if(payment_method_oversea.Checked) {

				payInfo.month = Convert.ToInt32(oversea_pay_month.Value);
				payItem.data = payInfo.ToJson();
				payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo , null);
				//payItem.data = payInfo.ToJson();
				this.ViewState["payItem"] = payItem.ToJson();


				this.ViewState["campaignId"] = "";
				this.ViewState["jumin"] = "";
				this.ViewState["ci"] = "";
				this.ViewState["di"] = "";
				this.ViewState["good_mny"] = (payInfo.month * Convert.ToInt32(hd_amount.Value)).ToString();
				this.ViewState["pay_method"] = "100000000000";
				this.ViewState["overseas_card"] = "Y";
				this.ViewState["used_card_YN"] = "Y";
				this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_oversea"];
				this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_web_card_Foreign"];
				kcp_form_temporary.Visible = true;
				kcp_form_temporary.Show(this.ViewState);
			} else if(payment_method_cms.Checked) {

				var actionResult = new PaymentAction().GetBankAccount();
				if(!actionResult.success) {
					base.AlertWithJavascript(actionResult.message);
				} else {
					
					this.ViewState["relation"] = cms_owner_type1.Checked ? cms_owner_type1.Value : cms_owner_type2.Value;
					this.ViewState["cms_owner"] = cms_owner.Value;
					this.ViewState["cms_account"] = cms_account.Value;
					this.ViewState["birth"] = cms_account_type1.Checked ? cms_birth.Value : cms_soc.Value;
					this.ViewState["bank_name"] = hd_cms_bank_name.Value;
					this.ViewState["bank_code"] = hd_cms_bank.Value;
					this.ViewState["cmsday"] = cmsday_5.Checked ? cmsday_5.Value : (cmsday_15.Checked ? cmsday_15.Value : cmsday_25.Value);
					
					this.ViewState["src"] = string.Format("예금주와의 관계 : {0} /\n예금주 : {1} /\n예금주 생년월일 : {2} /\n은행명 : {3} /\n"
							   + "은행번호 : {4} /\n계좌번호 : {5} /\n이체일 : {6} /\n"
							   , this.ViewState["relation"]
							   , this.ViewState["cms_owner"]
							   , this.ViewState["birth"]
							   , this.ViewState["bank_name"]
							   , this.ViewState["bank_code"]
							   , this.ViewState["cms_account"]
							   , this.ViewState["cmsday"]
							   );
					
					cms_form.Visible = true;
					cms_form.Show(this.ViewState);


				}
			}
		}
	}

	// 정기후원 즉시결제
	bool Pay( pay_item_session payItem, string commitmentId ) {

        #region old
        //old
        //var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

        //// 신용카드, CMS 인 경우 즉시결제 처리
        //var actionResult = new BatchPay().Pay(commitmentId, payItem.orderId, payInfo.amount);
        //if (actionResult.success)
        //    return true;
        //else
        //{
        //    base.AlertWithJavascript(
        //        string.Format(@"정기 후원 신청이 완료되었으나, 첫 후원금 결제가 정상적으로 처리되지 못했습니다. 1~3일 내에 등록하신 연락처로 전화 드려 후원금 납부를 도와 드리도록 하겠습니다.",
        //        string.IsNullOrEmpty(actionResult.message) ? "" : string.Format("{0}의 사유로 ", actionResult.message)),
        //        string.Format("location.href='{0}'", this.ViewState["returnUrl"].ToString()));
        //    //base.AlertWithJavascript(actionResult.message);
        //    return false;
        //}
        #endregion

        //new 2017.06.13 김태형 : www 참조로 수정
        #region new
        var payInfo = payItem.data.ToObject<PayItemSession.Entity>();
        
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();

        // 신용카드, CMS 인 경우 즉시결제 처리
        UserInfo sess = new UserInfo();
        //for (int i = 0; i < 50; i++)
        //{
        var actionResult = new BatchPay().Pay(commitmentId, payItem.orderId, payInfo.amount, payInfo.group);

        //[이종진] 2017-12-20 : 주석처리 문희원
        // -> 이종진 다시 주석 품. 일단 기존처리 방식이면 실행하도록 해놓음
        //신규 GlobalPool 조회방법인지 체크하여 GlobalSponsorID생성 및 NomoneyHold (기존소스 유지한것임)
        //신규방법은 Pay() 에서 GlobalSponsorID생성 및 Compass4에있는 NomoneyHold및 임시결연정보 삭제하고, GlobalCommitment생성
        if (this.ViewState["childmasterid"] != null && strdbgp_kind == "1")
            RegistCommitmentTemp(sess.SponsorID, sess.UserName, this.ViewState["childmasterid"].ToString());

        if (actionResult.success)
        {
            if (actionResult.message != null && actionResult.message.Contains("즉시결제 대상은행이 아님"))
            {
                string msg = "정기 후원 신청이 완료되었으나, 첫 후원금 결제가 정상적으로 처리되지 않았습니다. 첫 후원금을 다시 결제하시겠습니까?";
                base.ConfirmWithJavascript(
                    msg,
                    "location.href= '" + this.ViewState["returnUrl"].ToString() + "'",
                        "location.href='" + this.ViewState["againUrl"].ToString() + "'");// + motiveName.Value + "'");
                return false;
            }
            //else if (actionResult.message != null && actionResult.message.Contains("어린이를 다시 확인해주세요"))
            //{
            //    string msg = actionResult.message;
            //    base.AlertWithJavascript(msg);
            //    return false;
            //}
            else
            {
                return true;
            }
        }
        else
        {
            //i++;
            //continue;

            /*
                base.AlertWithJavascript(
                    string.Format(@"정기 후원 신청이 완료되었으나,{0}첫 후원금 결제가 정상적으로 처리되지 못했습니다. 1~3일 내에 등록하신 연락처로 전화 드려 후원금 납부를 도와 드리도록 하겠습니다.",
                    string.IsNullOrEmpty(actionResult.message) ? "" : string.Format("{0}의 사유로 ", actionResult.message)), 
                    string.Format("location.href='{0}'" , this.ViewState["returnUrl"].ToString()));
            */

            PayItemSession.SetCookie(this.Context, new PayItemSession.Entity() { type = payInfo.type, frequency = payInfo.frequency, group = payInfo.type.ToString(), codeId = payInfo.codeId, amount = payInfo.amount, childMasterId = payInfo.childMasterId, campaignId = payInfo.campaignId });

            if (payment_method_giro.Checked || payment_method_virtualaccount.Checked)
            {

                base.ConfirmWithJavascript(
                        "후원 신청이 완료되었습니다.\\n\\r첫 후원금 결제를 바로 진행하시겠습니까?",
                        "location.href= '" + this.ViewState["returnUrl"].ToString() + "'",
                        "location.href='" + this.ViewState["againUrl"].ToString() + "'"//" + motiveName.Value + "'"
                        );
                return false;
            }
            else
            {
                base.ConfirmWithJavascript(
                        //"정기 후원 신청이 완료되었으나, [" + actionResult.message + "]의 사유로 첫 후원금 결제가 정상적으로 처리되지 못했습니다. 첫 후원금을 다시 결제하시겠습니까?",
                        "정기 후원 신청이 완료되었으나, 첫 후원금 결제가 정상적으로 처리되지 않았습니다. 첫 후원금을 다시 결제하시겠습니까?",
                        "location.href= '" + this.ViewState["returnUrl"].ToString() + "'",
                        "location.href='" + this.ViewState["againUrl"].ToString() + "'"//" + motiveName.Value + "'"
                        );

                return false;
            }

        }
        //}

        //return true;
        #endregion
    }

    JsonWriter DoPayCDSP(string sponsorId) {
        #region old
        //old
        //var sess = new UserInfo();
        //var action = new CommitmentAction();
        //JsonWriter result = new JsonWriter();
        //var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
        //var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

        ///*
        //이미 자동결제 정보가 있는 경우 추가 승인번호를 요청하지 않는다.
        //*/
        //if (exist_account.Value == "Y")
        //{

        //    result = action.DoCDSP(payInfo.childMasterId, payInfo.campaignId, payInfo.codeName, motiveCode.Value, motiveName.Value);

        //    if (!result.success)
        //    {
        //        base.AlertWithJavascript(result.message);
        //        return result;
        //    }

        //    var commitmentId = result.data.ToString();
        //    payInfo.commitmentId = commitmentId;
        //    new PayItemSession.Store().Update(payItem.orderId, payInfo, payItem.orderId);


        //    if (this.Pay(payItem, result.data.ToString()))
        //    {
        //        Response.Redirect("/sponsor/pay/complete/" + payItem.orderId);
        //    }
        //    else
        //    {

        //        //action.DeleteCommitment(commitmentId);
        //        result.success = false;
        //        result.message = "결제실패";
        //        //	base.AlertWithJavascript(result.message);
        //        return result;
        //    }


        //}
        //else
        //{

        //    action.ValidateCDSP(payInfo.childMasterId, payInfo.campaignId, payInfo.codeName, sponsorId, ref result);

        //    if (!result.success)
        //    {
        //        base.AlertWithJavascript(result.message);
        //        return result;
        //    }

        //}

        //return result;
        #endregion

        //new 2017.06.13 김태형 : www 참조로 수정
        #region new
        var sess = new UserInfo();
        var action = new CommitmentAction();
        JsonWriter result = new JsonWriter();
        var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
        var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

        /*
  이미 자동결제 정보가 있는 경우 추가 승인번호를 요청하지 않는다.
  */
        if (exist_account.Value == "Y")
        {

            result = action.DoCDSP(payInfo.childMasterId, payInfo.childKey, payInfo.campaignId, payInfo.codeName, motiveCode.Value, motiveName.Value);

            ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 DoPayCDSP result=========" + result.message);

            if (!result.success)
            {
                base.AlertWithJavascript(result.message);
                return result;
            }

            var commitmentId = result.data.ToString();

            ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 PAY commitmentId=========" + commitmentId);
            ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 PAY result.data.ToString()=========" + result.data.ToString());

            //ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 PAY RETURN URL=========" + this.Pay(payItem, result.data.ToString()));

            if (this.Pay(payItem, result.data.ToString()))
            {
                if (payment_method_giro.Checked || payment_method_virtualaccount.Checked)
                {
                    PayItemSession.SetCookie(this.Context, new PayItemSession.Entity() { type = payInfo.type, frequency = payInfo.frequency, group = payInfo.type.ToString(), codeId = payInfo.codeId, amount = payInfo.amount, childMasterId = payInfo.childMasterId, campaignId = payInfo.campaignId });

                    base.ConfirmWithJavascript(
                            "후원 신청이 완료되었습니다.\\n\\r첫 후원금 결제를 바로 진행하시겠습니까?",
                            "location.href= '" + this.ViewState["returnUrl"].ToString() + "'",
                            "location.href='" + this.ViewState["againUrl"].ToString() + "'"//" + motiveName.Value + "'"
                            );
                }
                else
                {
                    Response.Redirect(this.ViewState["returnUrl"].ToString());
                }
            }
            else
            {
                ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 PAY RETURN URL=========" + this.ViewState["returnUrl"].ToString());

                // 즉시결제 실패해도 결연 성공으로 처리 
                // 2016-10-24 이석호 과장 요청
                /* 테스트를 위해 잠시 삭제 주석 제거..20170313

                action.DeleteCommitment(commitmentId);
                */
                result.success = false;
                result.message = "결제실패";
                return result;
            }

        }
        else
        {

            action.ValidateCDSP(payInfo.childMasterId, payInfo.campaignId, payInfo.codeName, sponsorId, ref result);

            if (!result.success)
            {
                base.AlertWithJavascript(result.message);
                return result;
            }

        }

        return result;
        #endregion
    }

    JsonWriter DoPayCIV(string sponsorId) {
        #region old
        //old
        //      var sess = new UserInfo();
        //var action = new CommitmentAction();
        //JsonWriter result = new JsonWriter();
        //var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
        //var payInfo = payItem.data.ToObject<PayItemSession.Entity>();


        ///*
        //이미 자동결제 정보가 있는 경우 추가 승인번호를 요청하지 않는다.
        //*/
        //if(exist_account.Value == "Y") {

        //	result = action.DoCIV(sponsorId, payInfo.amount , 1 , "", payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.childMasterId , payInfo.campaignId, motiveCode.Value, motiveName.Value);

        //	if(!result.success) {
        //		base.AlertWithJavascript(result.message);
        //		return result;
        //	}

        //	var commitmentId = result.data.ToString();
        //          payInfo.commitmentId = commitmentId;
        //          new PayItemSession.Store().Update(payItem.orderId, payInfo, payItem.orderId);

        //          // 경고메세지를 띄우는 경우
        //          if(result.action != null && result.action == "confirm") {
        //		base.ConfirmWithJavascript(result.message, "goBack()" , "location.href='"+ this.ViewState["returnUrl"].ToString() + "'");
        //		result.success = false;
        //		return result;
        //	}

        //	if(this.Pay(payItem, result.data.ToString())) {
        //		Response.Redirect("/sponsor/pay/complete/" + payItem.orderId);
        //	} else {

        //		//action.DeleteCommitment(commitmentId);
        //		result.success = false;
        //		result.message = "결제실패";
        //		base.AlertWithJavascript(result.message);
        //		return result;
        //	}

        //} else {

        //	// CIV 의 경우
        //	// action = confirm 인경우 javascript confirm 후 false 면 history.back
        //	action.ValidateCIV(sponsorId, payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.campaignId, ref result);

        //	if(!result.success) {
        //		base.AlertWithJavascript(result.message);
        //		return result;
        //	}

        //	// 경고메세지를 띄우는 경우
        //	if(result.action != null && result.action == "confirm") {
        //		base.ConfirmWithJavascript(result.message, "goBack()");
        //	}

        //}

        //return result;
        #endregion

        //new 2017.06.13 김태형 : www 참조로 수정
        #region new
        var sess = new UserInfo();
        var action = new CommitmentAction();
        JsonWriter result = new JsonWriter();
        var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
        var payInfo = payItem.data.ToObject<PayItemSession.Entity>();


        /*
		이미 자동결제 정보가 있는 경우 추가 승인번호를 요청하지 않는다.
		*/
        if (exist_account.Value == "Y")
        {

            result = action.DoCIV(sponsorId, payInfo.amount, 1, "", payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.childMasterId, payInfo.campaignId, motiveCode.Value, motiveName.Value);

            if (!result.success)
            {
                base.AlertWithJavascript(result.message);
                return result;
            }

            var commitmentId = result.data.ToString();

            // 경고메세지를 띄우는 경우
            if (result.action != null && result.action == "confirm")
            {
                base.ConfirmWithJavascript(result.message, "goBack()", "location.href='" + this.ViewState["returnUrl"].ToString() + "'");
                result.success = false;
                return result;
            }

            if (this.Pay(payItem, result.data.ToString()))
            {
                if (payment_method_giro.Checked || payment_method_virtualaccount.Checked)
                {
                    base.ConfirmWithJavascript(
                            "정기후원 신청이 완료되었습니다.\\n\\r첫 후원금 결제를 바로 진행하시겠습니까?",
                            "location.href= '" + this.ViewState["returnUrl"].ToString() + "'",
                            "location.href='" + this.ViewState["againUrl"].ToString() + "'"//" + motiveName.Value + "'"
                            );
                }
                else
                {
                    Response.Redirect(this.ViewState["returnUrl"].ToString());
                }
            }
            else
            {
                /* 테스트를 위해 잠시 삭제 주석 제거..20170313
                action.DeleteCommitment(commitmentId);
                */
                result.success = false;
                result.message = "결제실패";
                return result;
            }

        }
        else
        {

            // CIV 의 경우
            // action = confirm 인경우 javascript confirm 후 false 면 history.back
            action.ValidateCIV(sponsorId, payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.campaignId, ref result);

            if (!result.success)
            {
                base.AlertWithJavascript(result.message);
                return result;
            }

            // 경고메세지를 띄우는 경우
            if (result.action != null && result.action == "confirm")
            {
                base.ConfirmWithJavascript(result.message, "goBack()");
            }

        }

        return result;
        #endregion
    }

    private void DoTCPT()
    {
        bool regGlobalID = false;
        bool updateHold = false;
        //string sponsorid = context.Request["sponsorid"].EmptyIfNull().EscapeSqlInjection();
        //string childMasterId = context.Request["childmasterid"].EmptyIfNull().EscapeSqlInjection();
        // 문희원 테스트용 추가
        string test = "";
        try
        {

            //test += sponsorid + " " + childMasterId + " ";
            TCPTService.Service svc = new TCPTService.Service();
            svc.Timeout = 180000;

            test += "결연정보 조회 / ";
            //결연정보 조회
            DataSet ds = svc.GetCommitmentInfoForTCPT(CommitmentID, CommitmentID);

            if (ds == null || ds.Tables.Count == 0)
            {
                test += "결연정보 조회 실패 / ";
            }
            else
            {
                //결연정보 조회되지 않음
                if (ds.Tables[0].Rows.Count == 0)
                {
                    test += "결연정보 조회 되지 않음 / ";
                }
                else
                {
                    test += "결연정보 조회됨 / ";
                    // GlobalSponsorID  생성
                    DataRow dr = ds.Tables[0].Rows[0];

                    bool regSuccess = false;
                    string result = string.Empty;
                    string sponsorID = SponsorID;
                    string childMasterId = dr["ChildMasterID"].ToString();
                    string userid = "";
                    string userName = "";
                    string GlobalSponsorID = string.Empty;
                    string ChildGlobalID = dr["ChildGlobalID"].ToString();
                    //string CommitmentID = dr["CommitmentID"].ToString();
                    string holdID = dr["HoldID"].ToString();
                    string holdUID = dr["HoldUID"].ToString();
                    string holdType = dr["HoldType"].ToString();
                    string conid = "54-" + dr["ConID"].ToString();
                    string firstName = dr["FirstName"].ToString();
                    string lastName = dr["LastName"].ToString();
                    string genderCode = dr["GenderCode"].ToString();

                    if (dr["SponsorGlobalID"].ToString() != "")
                    {
                        GlobalSponsorID = dr["SponsorGlobalID"].ToString();
                        regSuccess = true;
                    }

                    if (genderCode == "M")
                        genderCode = "Male";
                    else if (genderCode == "F")
                        genderCode = "Female";

                    if (string.IsNullOrEmpty(GlobalSponsorID))
                    {
                        test += "Sponsor GlobalID  생성 시작 / ";

                        try
                        {
                            SupporterCreateInSFCI_POST kit = new SupporterCreateInSFCI_POST();
                            SupporterProfile profile = new SupporterProfile();

                            profile.GlobalPartner = "Compassion Korea";
                            profile.CorrespondenceDeliveryPreference = "Digital";
                            profile.FirstName = firstName;
                            profile.LastName = lastName;
                            if (!string.IsNullOrEmpty(genderCode))
                                profile.Gender = genderCode;
                            profile.GPID = "54-" + conid;
                            profile.PreferredName = "";
                            profile.Status = "Active";
                            profile.StatusReason = "New";
                            profile.MandatoryReview = false;

                            kit.SupporterProfile.Add(profile);

                            string json = JsonConvert.SerializeObject(kit);
                            result = svc.SupporterCreateInSFCI_POST(json);
                            TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(result);
                            if (msg.IsSuccessStatusCode)
                            {
                                test += "Sponsor GlobalID  생성 성공 / ";
                                SupporterProfileResponse_Kit response = JsonConvert.DeserializeObject<SupporterProfileResponse_Kit>(msg.RequestMessage.ToString());
                                GlobalSponsorID = response.SupporterProfileResponse[0].GlobalID;

                                test += "Sponsor GlobalID  생성 성공 " + GlobalSponsorID + " / ";
                                test += "Sponsor GlobalID  업데이트 시작 / ";
                                bool b = svc.UpdateSponsorGlobalID(sponsorID, GlobalSponsorID);

                                if (b)
                                {
                                    regGlobalID = true;
                                    test += "Sponsor GlobalID  업데이트 성공 / ";
                                }
                                else
                                {
                                    test += "Sponsor GlobalID  업데이트 실패 / ";
                                }
                            }
                            else
                            {
                                test += "Sponsor GlobalID  생성 실패 " + msg.RequestMessage.ToString() + " / ";
                                //오류
                            }
                        }
                        catch (Exception ex)
                        {
                            test += "Sponsor GlobalID  생성 오류 " + ex.Message + "/ ";
                        }
                    }


                    test += "no money hold 로 업데이트 시작 / ";
                    //  no money hold 로 업데이트
                    string endDT = DateTime.Now.AddMonths(3).ToString("yyyy-MM-dd");
                    try
                    {
                        BeneficiaryHoldRequestList hKit = new BeneficiaryHoldRequestList();
                        hKit.Beneficiary_GlobalID = ChildGlobalID;
                        hKit.BeneficiaryState = "No Money Hold";
                        hKit.HoldEndDate = endDT + "T23:59:59Z";
                        hKit.IsSpecialHandling = false;
                        hKit.PrimaryHoldOwner = conid;
                        hKit.GlobalPartner_ID = "KR";
                        hKit.HoldID = holdID;

                        string hJson = JsonConvert.SerializeObject(hKit);

                        result = svc.BeneficiaryHoldSingle_PUT(ChildGlobalID, hKit.HoldID, hJson);
                        TCPTResponseMessage updateResult = JsonConvert.DeserializeObject<TCPTResponseMessage>(result);
                        if (updateResult.IsSuccessStatusCode)
                        {
                            test += "no money hold 로 업데이트 성공 / ";
                            test += "기존 hold  expired  처리 시작 / ";
                            // 기존 hold  expired  처리
                            bool b = svc.UpdateHoldStatus(new Guid(holdUID), holdID, hKit.BeneficiaryState, endDT, "Expired", "2000", "", sponsorID, sponsorID);

                            if (b)
                            {
                                test += "기존 hold  expired  처리 성공 / ";
                            }
                            else
                            {
                                test += "기존 hold  expired  처리 실패 / ";
                            }
                            // 신규  hold insert
                            Guid newHoldUID = Guid.NewGuid();
                            test += "신규 hold insert / ";
                            b = svc.InsertHoldHistory(newHoldUID, holdID, ChildGlobalID, hKit.BeneficiaryState, endDT, sponsorID, "", "2000", "web 어린이 결연");
                            if (b)
                            {
                                test += "신규 hold insert 성공 / ";
                            }
                            else
                            {
                                if (b)
                                {
                                    test += "신규 hold insert 성공 / ";
                                    updateHold = true;
                                }
                                else
                                {
                                    test += "신규 hold insert 실패 / ";
                                }
                            }

                            // TCPT_CommitmentTemp 테이블에 insert
                            test += "TCPT_CommitmentTemp 테이블에 insert / ";
                            string s = svc.InsertTCPT_CommitmentTemp(CommitmentID, sponsorID, ChildMasterID, newHoldUID.ToString(), holdID, sponsorID, "");
                            test += "TCPT_CommitmentTemp 테이블에 insert 결과 : " + s + " / ";

                        }
                        else
                        {
                            // hold update error
                            test += "no money hold 로 업데이트 실패 " + updateResult.RequestMessage.ToString() + " / ";
                        }
                    }
                    catch (Exception ex)
                    {
                        test += "no money hold 로 업데이트 오류 " + ex.Message + " / ";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            test += "오류 오류 " + ex.Message + " / ";
        }
        ErrorLog.Write(HttpContext.Current, 0, test);
    }
    

}