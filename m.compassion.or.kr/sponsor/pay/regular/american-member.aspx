﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="american-member.aspx.cs" Inherits="sponsor_pay_regular_american_member" %>
<div style="background: transparent;width:100%;" class="fn_pop_container" id="popup">
<div class="wrap-layerpop" style="width:80%;left:10%">
        <div class="header">
        	<p class="txt-title"><img src="/common/img/common/logo1.png" alt="" width="112" height="42"  /></p>
        </div>
        <div class="contents">
        	
            <strong class="txt-f20"><span class="color1">{{sponsor}}</span> 후원자님</strong><br /><br />
            
            <p class="color2">
                안녕하세요, 한국컴패션입니다.<br /><br />
                국제컴패션 서비스가 변경되어 앞으로는<br />
                미국컴패션의 한글 번역 기능을 통해<br />
                후원 관리 및 어린이와의 편지 서비스를<br />
                이용하실 수 있습니다.<br /><br />
                아래의 버튼을 눌러 미국컴패션으로<br />
                이동해 주세요.<br /><br />
                언제나 후원자님의 귀한 섬김에 감사드립니다.
            </p>
            <div class="wrap-bt"><a href="http://www.compassion.com/" style="width:180px" class="bt-type6">미국컴패션 바로 가기</a></div>
            
            
        </div>
        <div class="close"><span class="ic-close" ng-click="modal.close($event)">레이어 닫기</span></div>
    </div>
</div>

