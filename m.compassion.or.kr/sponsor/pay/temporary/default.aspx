﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="sponsor_temporary_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<%@ Register Src="/sponsor/item-child.ascx" TagPrefix="uc" TagName="child" %>
<%@ Register Src="/sponsor/item-user-funding.ascx" TagPrefix="uc" TagName="uf" %>
<%@ Register Src="/sponsor/item-special.ascx" TagPrefix="uc" TagName="sf" %>

<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>
<%--<%@ Register Src="/sponsor/pay/temporary/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/sponsor/pay/temporary/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/sponsor/pay/temporary/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>--%>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
	<script type="text/javascript" src="/sponsor/pay/temporary/default.js"></script>
	
</asp:Content>


<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<a runat="server" id="btnList"></a>

	<input type="hidden" id="gender" runat="server"  />
	<input type="hidden" id="hd_auth_domain" runat="server"  />
	<input type="hidden" id="hd_age" runat="server"  />

	<input type="hidden" id="amount" runat="server"  />
	<input type="hidden" runat="server" id="jumin" value="" />		
	<input type="hidden" runat="server" id="ci" value="" />		<!-- 본인인증 CI -->
	<input type="hidden" runat="server" id="di" value="" />		<!-- 본인인증 DI -->
	<input type="hidden" runat="server" id="cert_gb" value="" />		<!-- 본인인증 수단 -->
	
	<input type="hidden" runat="server" id="user_class" value="20세이상" />
	<input type="hidden" runat="server" id="hdState" value="" />
	<input type="hidden" runat="server" id="hdLocation" value="" />
	<input type="hidden" runat="server" id="hdManageType" value="" />
	<input type="hidden" runat="server" id="hdBirthDate" value="" />
	<input type="hidden" runat="server" id="hdCI" value="" />	<!-- 기존 등록된 본인인증 CI -->
	
	<input type="checkbox" runat="server" id="noname" style="display:none" /> <!--무기명-->
	

	<div class="wrap-sectionsub">
<!---->
    
		<div class="member-join sponsor-payment">
    	
			<uc:child runat="server" ID="view_child" Visible="false" />

			<uc:uf runat="server" ID="view_uf" Visible="false" />

			<uc:sf runat="server" ID="view_sf" Visible="false" />
      

			<!---->
			
        
			<div class="show_noname" style="display:none">
				<!--무기명 회원인경우 보여줄 메세지.-->
			</div>

			<div class="hide_noname">
				<!--국세청 영수증 신청 영역-->
				<div runat="server" ID="ph_cert_me">
				<!--거주구분 선택-->
				<div class="txt-inhabit">
					<span class="checkbox-ui">
						<input type="checkbox" id="addr_overseas" runat="server" class="css-checkbox" />
						<label for="addr_overseas" class="css-label">해외에 거주하고 있어요 </label>
					</span>
				</div>

				<fieldset class="frm-input mt10">
					<legend>주민등록번호 입력</legend>
					<div class="row-table">
						<div class="col-td" style="width:100%">
							<span class="row" runat="server" ID="ph_notuser1" style="display:none"><input type="text" value="" runat="server" id="user_name" placeholder="이름" style="width:100%" /></span>
							<span class="row">
                    			<span class="checkbox-ui">
									<input type="checkbox" class="css-checkbox" id="p_receipt_pub_ok" runat="server" checked />
									<label for="p_receipt_pub_ok" class="css-label">국세청 연말정산 영수증 신청</label>
								</span>
							</span>
							<asp:PlaceHolder runat="server" ID="ph_no_jumin" Visible="false">
							<span class="row pos-relative2" id="func_name_check"><input type="text" class="number_only" value="" data-id="jumin1" runat="server" id="jumin1" maxlength="13" placeholder="주민등록번호" style="width:100%" /><a class="bt-type9 pos-btn" style="width:70px" href="#" id="btn_name_check">실명인증</a></span>
							</asp:PlaceHolder>
							<p class="txt-result" id="msg_name_check" style="display:none"><span class="guide_comment1"></span></p>
						</div>
					</div>
				</fieldset>
					

				<!--기존에 이미 실명인증했고, 영수증을 신청한 회원의 경우 국세청영수증 영역 대신 출력-->
				<asp:PlaceHolder runat="server" ID="ph_yes_jumin" Visible="false">
					<div class="msg-graybox mt10">
       					<strong>기존에 등록하신<br />
						<span class="color1">국세청 연말정산 영수증 신청 정보</span>가 있습니다.</strong><br />
						기존 정보에 연말정산 정보를 추가로 등록합니다. 
					</div>
				</asp:PlaceHolder>

				<div class="hide_overseas">
				
						<asp:PlaceHolder runat="server" ID="ph_cert" Visible="false">
							<p class="txt-title mt20">본인인증</p>
							<!--본인인증 영역.(최초 1회만) 실명인증 시 비노출-->
							<p class="txt1 align-left mt20" style="padding: 0">온/오프라인 기존 후원정보 확인을 위해 본인 인증을 진행해 주세요.</p>
							<div class="self-account func_cert">
								<div class="link-account">
									<a href="#" id="btn_cert_by_phone"><span>휴대폰 인증</span></a>
									<a href="#" id="btn_cert_by_ipin"><span>아이핀(i-PIN) 인증</span></a>
								</div>
								<div class="box-gray">
									<ul class="list-bullet2">
										<li>본인명의의 휴대폰으로만 본인인증이 가능합니다.<br />
											아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다. </li>
									</ul>
								</div>
							</div>
							<p class="txt-result" id="msg_cert" style="display: none"><span class="guide_comment1">정보 확인이 완료되었습니다.</span></p>
						</asp:PlaceHolder>
					
				</div>
				</div>

			</div>
		
		
        
			<p class="txt-title">결제방법</p>
			 <ul class="list-paymethod">
         		<li><span class="radio-ui">
					<input type="radio" id="payment_method_card"  runat="server" name="payment_method" class="css-radio" checked />
					<label for="payment_method_card" class="css-label">신용카드</label>
				</span></li>
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_cms" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_cms" class="css-label">실시간 계좌이체</label>
				</span></li>
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_kakao" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_kakao" class="css-label"><img src="/common/img/icon/kakaopay.png" alt="카카오 페이" class="txt-kakaopay" /></label>
				</span></li>
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_payco" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_payco" class="css-label"><img src="/common/img/icon/payco.png" alt="페이코" class="txt-payco" /></label>
				</span></li>
				 <!--
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_oversea" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_oversea" class="css-label">해외발급 카드</label>
				</span></li>
				 -->
				<li><span class="radio-ui">
					<input type="radio" id="payment_method_phone" runat="server" name="payment_method" class="css-radio" />
					<label for="payment_method_phone" class="css-label">휴대폰결제</label>
				</span></li>
			 </ul>

			<div class="box-gray">
				<ul class="list-bullet2">
					<li>후원금 결제 시 일부 카드사의 경우 결제 대행업체인 [NHN KCP 한국 사이버결제]로 승인 SMS가 전송됩니다.</li>
					<li>결제관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</li>
				</ul>
			</div>
        
			<div class="wrap-bt"><asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="bt-type6" style="width:100%" >결제</asp:LinkButton></div>

			 <p class="txt-bottom">회원가입이 어려우시거나 본인확인/인증이 안되시는 경우 한국컴패션으로 연락주시기 바랍니다.<br />
				후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr로 문의해주세요.
			</p>


        
		</div>
    
	<!--//-->
	</div>


</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form" />
	<uc:payco_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="payco_form" />
	<uc:kakaopay_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kakaopay_form" />
</asp:Content>