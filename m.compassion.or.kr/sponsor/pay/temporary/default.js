﻿$(function () {

	$page.init();

});


var $page = {

	timer: null,
	cert_target : "" ,		// me , parent

	init: function () {

		$("#btn_name_check").click(function () {
			cert_nameCheck($("#user_name"), $("[data-id=jumin1]"), null , $("#hd_auth_domain").val());
			return false;
		})

		// 본인인증
		// 휴대폰 인증
		$("#btn_cert_by_phone").click(function () {

			if ($("#user_name").val() == "") {
				alert("이름을 입력해주세요");
				$("#user_name").focus();
				return false;
			}

			$page.cert_target = "me";
			cert_openPopup("phone", $("#hd_auth_domain").val());

			return false;
		})

		// 아이핀 인증
		$("#btn_cert_by_ipin").click(function () {

			if ($("#user_name").val() == "") {
				alert("이름을 입력해주세요");
				$("#user_name").focus();
				return false;
			}
			
			$page.cert_target = "me";
			cert_openPopup("ipin", $("#hd_auth_domain").val());
			
			return false;
		})

		$("#btn_find_addr").unbind("click");
		$("#btn_find_addr").click(function () {
			cert_setDomain();
			var pop = window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=601,height=675, scrollbars=no, resizable=no");

			return false;
		})

		// 버튼
		$("#btn_submit").click(function () {

			return $page.onSubmit();

		});
		
		$("#addr_overseas").click(function () {
			if ($("#addr_overseas").prop("checked")) {
				$(".hide_overseas").hide();
			} else {
				$(".hide_overseas").show();
			}
		})

		if ($("#addr_overseas").prop("checked")) {
			$(".hide_overseas").hide();
		} else {
			$(".hide_overseas").show();
		}

		if ($("#noname").length > 0) {
			$("#noname").change(function () {

				if ($(this).prop("checked")) {
					$("#p_receipt_pub_deny").prop("checked", true);
					$(".hide_noname").hide();
					$(".show_noname").show();
				} else {
					$(".hide_noname").show();
					$(".show_noname").hide();
				}
			})
		}

		if ($("#jumin").length > 0 && $("#jumin1").length > 0 ) {
			var ssn = $("#jumin").val();
			if (ssn.length == 13) {
				$("#jumin1").val(ssn);
				
			}
		}

		if ($("#noname").length > 0 && $("#noname").prop("checked")) {
			$(".hide_noname").hide();
		}

		if ($("#cert_gb").val() != "") {
			
			updateCertUI($("#cert_gb").val() , $("#jumin").val());
			

		}

		if ($("#p_receipt_pub_ok").length > 0) {
			$("#p_receipt_pub_ok").change(function () {
				var checked = $(this).prop("checked");
				if (checked) {
					$("#func_name_check").show();
				} else {
					$("#jumin1").val("");
					$("#func_name_check").hide();
				}

			});
		}
	},

	// 확인
	onSubmit: function () {

		var is_noname = $("#noname").length > 0 && $("#noname").prop("checked");

		if ($("#addr_overseas").prop("checked")) {
			
			if (!is_noname) {
				if ($("#user_name").val() == "") {
					alert("이름을 입력해 주세요.");
					$("#user_name").focus();
					return false;
				}
			}

		} else {

			// 본인인증을 하지 않는 경우 (기업,미주등)
			if ($("#ph_cert_me").length > 0 && $("#ph_cert_me").css("display") == "none")
				return true;

			// 연말정산 영수증 신청의 경우
			if ($("#p_receipt_pub_ok").length > 0 && $("#p_receipt_pub_ok").prop("checked") && $("#jumin").val() == "") {
				alert("연말정산 영수증 신청을 위해 주민등록번호 확인을 해주세요.");
				return false;
			}

			// 본인인증
			if (!is_noname && $("#msg_cert").length > 0) {

				if ($("#user_name").length > 0 && $("#cert_gb").val() == "") {
					alert("본인인증을 해주세요.");
					return false;
				}
			}

		}

		return true;
	}


}


var goBack = function () {
	if (location.pathname.indexOf("/sponsor/CSP/first-birthday/") > -1)
		location.href = "/";
	else
		history.back();
}

var updateCertUI = function (method , jumin) {
	{
		var msgObj = $("#msg_cert").find("span");
		$("#msg_cert").show();
		msgObj.removeClass("guide_comment1").removeClass("guide_comment2");
		msgObj.addClass("guide_comment1")
		msgObj.html("본인 인증이 완료되었습니다.");
	}

	if (method == "namecheck") {
		var msgObj = $("#msg_name_check").find("span");
		$("#msg_name_check").show();
		msgObj.removeClass("guide_comment1").removeClass("guide_comment2");
		msgObj.addClass("guide_comment1");
		msgObj.html("정보 확인이 완료되었습니다.");


	}
	$(".func_cert").hide();

	if (jumin) {
		$("#jumin").val(jumin);
		$("#func_name_check").hide();
	}

}

// 본인인증 결과 응답
// result = Y or N , birth = yyyyMMdd
// sex = M or F
// method = ipin or phone
var cert_setCertResult = function (method, result, ci, di, name, birth, gender, phone) {
	
	console.log(method, result, ci, di, name, birth, gender, phone);
	$("#msg_cert").show();
	var target = $("#msg_cert")
	var msgObj = $("#msg_cert").find("span");
	target.removeClass("txt-error").addClass("txt-result");

	if (result != "Y") {
		target.removeClass("txt-result").addClass("txt-error");
		msgObj.html("본인인증에 실패했습니다. 다시 시도해 주세요.");
		return;
	}
	
	if ($("#user_name").val() != name) {
		target.removeClass("txt-result").addClass("txt-error");
		msgObj.html("이름과 본인인증정보가 일치하지 않습니다.");
		return;
	}

	checkCI(method, ci, di, gender);


}

// 실명인증 결과 응답
var cert_setNameCheckResult = function (result, ci, di, name, jumin , msg) {
	console.log(result, ci, di, name, jumin, msg);
	$("#msg_name_check").show();
	var target = $("#msg_name_check")
	var msgObj = $("#msg_name_check").find("span");
	target.removeClass("txt-error").addClass("txt-result");

	if (result != "Y") {
		target.removeClass("txt-result").addClass("txt-error");
		msgObj.html(msg);
		return;
	}

	
	var gender = jumin.substr(6, 1);
	if (gender == 1 || gender == 3 || gender == 5) {
		gender = "M";
	} else {
		gender = "F";
	}
	checkCI("namecheck", ci, di, gender , jumin);
}

var checkCI = function (method, ci, di, gender, jumin) {

	if (common.isLogin()) {

		$.post("/api/sponsor.ashx", { t: "sync", ci: ci, di: di, gender: gender, jumin: jumin }, function (r) {

			if (r.success) {

				if (r.action == "login") {
				    alert("본인인증된 계정이 이미 존재합니다. \n해당 계정으로 로그인하셔서 후원내역을 확인해주십시요. \n(아이디 : " + r.data.user_id + ")");
					// 로그인창오픈
				    location.href = "/login";

				} else if (r.action == "reload") {
					// 동기화 완료
					$("#ci").val(ci);
					$("#di").val(di);
					$("#gender").val(gender);
					$("#cert_gb").val(method);

					updateCertUI(method , jumin);
				}
			} else {
				alert(r.message);
			}

		});

	} else {
		// 로그인 되지 않은 상태면 CI 중복여부만 확인
		$.post("/api/sponsor.ashx", { t: "check", ci: ci }, function (r) {

			if (r.success) {

				// 동기화 완료
				$("#ci").val(ci);
				$("#di").val(di);
				$("#gender").val(gender);
				$("#cert_gb").val(method);

				updateCertUI(method , jumin);

			} else {
				if (r.action == "login") {
					if (confirm("후원자님은 이미 회원으로 가입되어 있습니다.\n로그인 후 진행 하시겠어요?")) {
						location.href = "/login?r=" + location.href;
					}
					/*
					$("#ci").val(ci);
					$("#di").val(di);
					$("#gender").val(gender);
					$("#cert_gb").val(method);

					updateCertUI(method, jumin);
					*/

				}
			}
		});
	}

	return false;

}


// 기 승인된 회원이 있는경우 로그인변경 유도
var changeLogin = function () {
	alert("changeLogin");
}