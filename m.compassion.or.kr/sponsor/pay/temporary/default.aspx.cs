﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_temporary_default : SponsorPayBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBackTemporary();

        try
        {

            btnList.HRef = "/sponsor/";

            // 결제실패에서 넘어온경우 후원 메인으로 보낸다.
            if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.IndexOf("/pay/fail_sponsor/") < 0)
                btnList.HRef = Request.UrlReferrer.AbsoluteUri;

            var payInfo = PayItemSession.GetCookie(this.Context);
            this.ViewState["good_mny"] = payInfo.amount.ToString();

            if (!UserInfo.IsLogin)
            {

                if (Request["noname"] == "Y")
                {
                    noname.Checked = true;
                    p_receipt_pub_ok.Checked = false;
                }
                else
                {
                    ph_notuser1.Style["display"] = "block";
                }
            }

            // 후원내역
            var childAction = new ChildAction();

            //	Response.Write(payInfo.ToJson());

            if (payInfo.type == PayItemSession.Entity.enumType.CDSP)
            {

                view_child.Visible = true;

                var actionResult = childAction.Ensure();
                if (!actionResult.success)
                {
                    base.AlertWithJavascript(actionResult.message, "goBack()");
                    return;
                }

            }
            else if (payInfo.type == PayItemSession.Entity.enumType.SPECIAL_FUNDING)
            {
                view_sf.Visible = true;
            }
            else if (payInfo.type == PayItemSession.Entity.enumType.USER_FUNDING)
            {

                view_uf.Visible = true;
                /*
                if(payInfo.IsEmptyChildMasterId) {
                    view_uf.Visible = true;
                } else {
                    view_child.Visible = true;
                }
                */
            }


            // 결제정보를 디비에 저장하고 이후 결제프로세스는 디비의 값을 참조한다.
            var data = new PayItemSession.Store().Create();
            if (data == null)
            {
                base.AlertWithJavascript("후원정보 임시저장에 실패했습니다.", "goBack()");
                return;
            }

            this.ViewState["payItem"] = data.ToJson();
        }
        catch (Exception ex)
        {
            ErrorLog.Write(this.Context, 1010, ex.Message);
        }
	}
	
	// 결제
	protected void btn_submit_Click( object sender, EventArgs e ) {
		
		kakaopay_form.Hide();
		kcp_form.Hide();
		payco_form.Hide();

		if(base.IsRefresh) {
			return;
		}

		if(!base.UpdateTemporaryUserInfo()) {
			return;
		}
		
		JsonWriter result = new JsonWriter();
		var sess = new UserInfo();
		var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();
        
		var action = new CommitmentAction();
		
		action.ValidateCIV(sess.SponsorID, payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.campaignId, ref result);

		if(!result.success) {
			base.AlertWithJavascript(result.message);
			return;
		}

		// 경고메세지를 띄우는 경우
		if(result.action != null && result.action == "confirm") {
			base.ConfirmWithJavascript(result.message, "goBack()");
		}

		// 결제정보를 세팅
		base.SetTemporaryPaymentInfo();

		if(payment_method_card.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card;
		} else if(payment_method_cms.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.cms;
		} else if(payment_method_oversea.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card_oversea;
		} else if(payment_method_phone.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.phone;
		} else if(payment_method_payco.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.payco;
		} else if(payment_method_kakao.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.kakao;
		}

		var orderId = PayItemSession.Store.GetNewOrderID();
		payInfo.extra = new Dictionary<string, object>() {
			{"jumin" , ViewState["jumin"].ToString() } ,
			{"ci" , ViewState["ci"].ToString() },
			{"di" , ViewState["di"].ToString() },
			{"motiveCode" , "" },
			{"motiveName" , "" },
			{"overseas_card" , ViewState["overseas_card"].ToString() },

            //[jun.heo] 2017-12 : 결제 모듈 통합
            {"successUrl" , "/pay/complete_sponsor/" + orderId } ,
            {"failUrl" , string.Format("/pay/fail_sponsor/?r={0}" , HttpUtility.UrlEncode("/sponsor/pay/temporary/?noname=" + Request["noname"])) }
   //         {"successUrl" , "/sponsor/pay/complete/" + orderId } ,
			//{"failUrl" , string.Format("/sponsor/pay/fail/?r={0}" , HttpUtility.UrlEncode("/sponsor/pay/temporary/?noname=" + Request["noname"])) }

		}.ToJson().Encrypt();

		payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo , orderId);
		//payItem.data = payInfo.ToJson();
		this.ViewState["payItem"] = payItem.ToJson();

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "Temporary";  // 결제 페이지 종류 

        if (payment_method_payco.Checked) {
			payco_form.Show(this.ViewState);
		}else if(payment_method_kakao.Checked) {
			kakaopay_form.Show(this.ViewState);
		} else {
			kcp_form.Show(this.ViewState);
		}
		
	}
}