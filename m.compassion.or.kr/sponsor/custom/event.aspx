﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="event.aspx.cs" Inherits="sponsor_custom_event" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
    $(function () {
        $("#btn_goCDSP").click(function () {
            loading.show();
            $.get("/sponsor/pay-gateway.ashx?t=go-cdsp", {
                    childMasterId: $("#childmasterid").val()
                    , childKey: $("#childKey").val()
                    , Pic: $("#Pic").val()
                    , IsOrphan: $("#IsOrphan").val()
                    , IsHandicapped: $("#IsHandicapped").val()
                    , WaitingDays: $("#WaitingDays").val()
                    , Age: $("#hdnAge").val()
                    , BirthDate: $("#BirthDate").val()
                    , CountryCode: $("#CountryCode").val()
                    , Gender: $("#Gender").val()
                    , HangulName: $("#HangulName").val()
                    , HangulPreferredName: $("#HangulPreferredName").val()
                    , FullName: $("#FullName").val()
                    , PreferredName: $("#PreferredName").val()
            }).success(function (r) {
                if (r.success) {
                    location.href = r.data;
                } else {
                    alert(r.message);
                    loading.hide();
                }
            });
        })
    });

    (function () {
        var app = angular.module('cps.page', []);
        app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {
            $scope.snsShow = function ($event) {
                var url = '/sponsor/children/?c=' + '<%:this.ViewState["childmasterid"].ToString()%>';
                var title = '[한국컴패션-1:1어린이양육]' + '<%:this.ViewState["childName"].ToString()%>';
                var picture = '<%:this.ViewState["childPic"].ToString()%>';
                $scope.sns.show($event, { url: url, title: title, desc: '', picture: picture });
            }
        });
    })();
    </script>
</asp:Content>
<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    
    <input type="hidden" runat="server" id="childmasterid" />
    <%-- [이종진] 히든값 추가, GP조회방법 일시 필요한 파라미터 --%>
    <input type="hidden" runat="server" id="childKey" />
    <input type="hidden" runat="server" id="Pic" />
    <input type="hidden" runat="server" id="IsOrphan" />
    <input type="hidden" runat="server" id="IsHandicapped" />
    <input type="hidden" runat="server" id="WaitingDays" />
    <input type="hidden" runat="server" id="hdnAge" />
    <input type="hidden" runat="server" id="BirthDate" />
    <input type="hidden" runat="server" id="CountryCode" />
    <input type="hidden" runat="server" id="Gender" />
    <input type="hidden" runat="server" id="HangulName" />
    <input type="hidden" runat="server" id="HangulPreferredName" />
    <input type="hidden" runat="server" id="FullName" />
    <input type="hidden" runat="server" id="PreferredName" />

    <div class="wrap-sectionsub">
    <div class="sponsor-match">
        <p class="txt-result1">두근두근! 후원자님을 기다리는 어린이의 마음!</p>
        <div class="photo-result" style="background:url('<%:this.ViewState["childPic"].ToString()%>') no-repeat"></div>
        <p class="txt-result2"><strong><asp:Literal runat="server" ID="country_name"/></strong>에 사는 <strong><asp:Literal runat="server" ID="age"/>살 <asp:Literal runat="server" ID="name"/></strong>가 후원자님을 기다립니다.</p>
        <p class="txt-result3">포기하지 않은 사랑으로 어린이와 함께해주세요.</p>
        <div class="wrap-bt">
        	<a class="bt-type6" style="width:100%" id="btn_goCDSP">후원하러 가기</a>
            <div ng-app="cps" ng-cloak ng-controller="defaultCtrl">
                <%--<a class="bt-type6" style="width:100%; margin-top:5px;" ng-click="sns.show($event , {url : '/sponsor/custom/event/?min_age=0&max_age=24', title : '[한국컴패션-1:1어린이양육]' + item.name , desc : '' , picture : item.pic})">친구에게 소개하기</a>--%>
                <a class="bt-type6" style="width:100%; margin-top:5px;" ng-click="snsShow($event)">친구에게 소개하기</a>
            </div>
        </div>
    </div>
</div>





</asp:Content>
