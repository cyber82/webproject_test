﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="sponsor_custom_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/sponsor/custom/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="min_age" />
    <input type="hidden" runat="server" id="max_age" />
    <input type="hidden" runat="server" id="gender" />
    <input type="hidden" runat="server" id="country" />


    <div class="wrap-sectionsub" id="l">
        <!---->

        <div class="sponsor-match">
            <p class="txt1">
                간단한 질문을 통해,<br />
                후원자님의 소중한 가족이 될 어린이를 소개해드립니다.
            </p>

            <div class="flow-step sectionsub-margin1">
                <span class="selected step_circle1">STEP 01</span>
                <span class="step_circle2">STEP 02</span>
                <span class="step_circle3">STEP 03</span>
                <span class="step_circle4">STEP 04</span>
            </div>
            <!-- step01 -->
            <p class="txt-question step01">
                타임머신을 발견한 당신,<br />
                어린 시절로 돌아갈 수 있다면<br />
                <strong>어느 때로 가고 싶으신가요?</strong>
            </p>
            <ul class="list-stepanswer step01">
                <li data-min_age="3" data-max_age="5" class="btn_step01"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step1-1.jpg" alt="" /><span class="pos-txt">호기심이 많은<br />
                        유치원생</span></span></li>
                <li class="btn_step01" data-min_age="6" data-max_age="12"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step1-2.jpg" alt="" /><span class="pos-txt">활발한 매력이<br />
                        넘치는 초등학생</span></span></li>
                <li class="btn_step01" data-min_age="13"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step1-3.jpg" alt="" /><span class="pos-txt">감수성이<br />
                        풍부한 중·고등학생</span></span></li>
            </ul>
            <!-- step01 -->
            <!-- step02 -->
            <p class="txt-question step02" style="display: none">
                타임머신에서 내린 당신은<br />
                두근거리는 마음으로 교실에 들어갔습니다.<br />
                <strong>내 짝꿍은 누구일까요?</strong>
            </p>
            <ul class="list-stepanswer step02" style="display: none">
                <li class="btn_step02" data-gender="F"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step2-1.jpg" alt="" /><span class="pos-txt">눈웃음이<br />
                        매력적인 여학생</span></span></li>
                <li class="btn_step02" data-gender="M"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step2-2.jpg" alt="" /><span class="pos-txt">장난기<br />
                        가득한 남학생</span></span></li>
            </ul>
            <!-- step02 -->
            <!-- step03 -->
            <p class="txt-question step03" style="display: none">
                당신은 학생의 역할을 멋지게 해내고 방학을<br />
                맞이해 가족여행을 가게 됐습니다.<br />
                <strong>방학을 어느 곳에서 보내실 건가요?</strong>
            </p>
            <ul class="list-stepanswer step03" style="display: none">
                <li class="btn_step03" data-index="1"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step3-1.jpg" alt="" /><span class="pos-txt">사방이 탁 트인<br />
                        높은 산으로 올라갈래요</span></span></li>
                <li class="btn_step03" data-index="2"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step3-2.jpg" alt="" /><span class="pos-txt">야생동물로 가득한<br />
                        국립공원에 놀러갈래요</span></span></li>
                <li class="btn_step03" data-index="3"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step3-3.jpg" alt="" /><span class="pos-txt">금강산도 식후경!<br />
                        맛집 투어를 시작할래요</span></span></li>
                <li class="btn_step03" data-index="4"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step3-4.jpg" alt="" /><span class="pos-txt">에메랄드빛<br />
                        바닷가를 걸어볼래요</span></span></li>
            </ul>
            <!-- step03 -->
            <!-- step04-1 -->
            <p class="txt-question step04" style="display: none">
                즐거웠던 방학을 마무리하며 사진을 정리하던 중,<br />
                왠지 마음에 끌리는 사진을 발견했습니다.<br />
                <strong>그 사진은 무엇인가요?</strong>
            </p>
            <ul class="list-stepanswer step04_1" style="display: none">
                <li class="btn_step04" data-nation="BO"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step4-1.jpg" alt="볼리비아 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="BR"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step4-2.jpg" alt="브라질 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="EC"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step4-3.jpg" alt="에콰도르 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="PE"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step4-4.jpg" alt="페루 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="CO"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step4-5.jpg" alt="콜롬비아 어린이 선택" /></span></li>
            </ul>
            <!-- step04-1 -->
            <!-- step04-2 -->
            <ul class="list-stepanswer step04_2" style="display: none">
                <li class="btn_step04" data-nation="TG"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step5-1.jpg" alt="토고 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="TZ"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step5-2.jpg" alt="탄자니아 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="GH"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step5-3.jpg" alt="가나 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="BF"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step5-4.jpg" alt="부르키나파소 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="RW"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step5-5.jpg" alt="르완다 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="KE"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step5-6.jpg" alt="케냐 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="UG"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step5-7.jpg" alt="우간다 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="ET"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step5-8.jpg" alt="에디오피아 어린이 선택" /></span></li>
            </ul>
            <!-- step04-2 -->
            <!-- step04-3 -->
            <ul class="list-stepanswer step04_3" style="display: none">
                <li class="btn_step04" data-nation="BD"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step6-1.jpg" alt="방글라데시아 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="PH"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step6-2.jpg" alt="필리핀 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="TH"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step6-3.jpg" alt="태국 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="IO"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step6-4.jpg" alt="인도네시아 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="LK"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step6-5.jpg" alt="스리랑카 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="IN"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step6-6.jpg" alt="인도 어린이 선택" /></span></li>
            </ul>
            <!-- step04-3 -->
            <!-- step04-4 -->
            <ul class="list-stepanswer step04_4" style="display: none">
                <li class="btn_step04" data-nation="HO"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step7-1.jpg" alt="온두라스 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="ES"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step7-2.jpg" alt="엘살바도르 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="DR"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step7-3.jpg" alt="도미니카공화국 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="GU"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step7-4.jpg" alt="콰테말라 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="HA"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step7-5.jpg" alt="아이티 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="ME"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step7-6.jpg" alt="멕시코 어린이 선택" /></span></li>
                <li class="btn_step04" data-nation="NI"><span class="img-block" onclick="">
                    <img src="/common/img/page/sponsor/img-step7-7.jpg" alt="니카라과 어린이 선택" /></span></li>
            </ul>
            <!-- step04-4 -->

        </div>
    </div>
    <!--//-->
</asp:Content>


