﻿$(function () {
    $(".appPageNav").find(".back").attr("href", "javascript:history.go(-1);");
    $(".btn_step01").click(function () {

        setTimeout(function () {

            $(".step01").hide();
            $(".step02").show();

            $(".step_circle1").removeClass("selected");
            $(".step_circle2").addClass("selected");

            scrollTo($("#l"));
        }, 500);

        $("#min_age").val($(this).data("min_age"))
        $("#max_age").val($(this).data("max_age"))
        //console.log("minage", $("#min_age").val())
        //console.log("maxage", $("#max_age").val())
    })

    $(".btn_step02").click(function () {
        setTimeout(function () {
            $(".step02").hide();
            $(".step03").show();

            $(".step_circle2").removeClass("selected");
            $(".step_circle3").addClass("selected");

            scrollTo($("#l"));
        }, 500);
        $("#gender").val($(this).data("gender"));
        //console.log($("#gender").val())
    })

    $(".btn_step03").click(function () {
        var selectStep = ".step04_" + $(this).data("index");
        //console.log($(this).data("index"));
        setTimeout(function () {
            $(".step03").hide();
            $(".step04").show();

            $(".step_circle3").removeClass("selected");
            $(".step_circle4").addClass("selected");

            $(selectStep).show();

            scrollTo($("#l"));
        }, 500);
    })

    $(".btn_step04").click(function () {
        $("#country").val($(this).data("nation"));
        setTimeout(function () {
            location.href = "/sponsor/custom/result/?min_age=" + $("#min_age").val() + "&max_age=" + $("#max_age").val() + "&gender=" + $("#gender").val() + "&country=" + $("#country").val();
        }, 500);
    })
    


});

