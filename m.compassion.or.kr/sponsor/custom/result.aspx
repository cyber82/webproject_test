﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="result.aspx.cs" Inherits="sponsor_custom_result" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        $(function () {
        	$("#btn_goCDSP").click(function () {
                $.get("/sponsor/pay-gateway.ashx?t=go-cdsp", { childMasterId: $("#childmasterid").val() }).success(function (r) {
                    if (r.success) {
                        location.href = r.data;
                    } else {
                        alert(r.message);
                    }
                });
            })
        });
    </script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="childmasterid" />

    <div class="wrap-sectionsub">
<!---->
    
    <div class="sponsor-match">
    	
        <p class="txt-result1">후원자님의 소중한 가족이 될<br />어린이를 찾았습니다!</p>
        <div class="photo-result" style="background:url('<%:this.ViewState["childPic"].ToString()%>') no-repeat"></div>
        <p class="txt-result2"><strong><asp:Literal runat="server" ID="country_name"/></strong>에 사는 <strong><asp:Literal runat="server" ID="age"/>살 <asp:Literal runat="server" ID="name"/></strong>에게 말해주세요.
			앞으로의 중요한 순간들을 함께하겠다고.</p>
        <p class="txt-result3">후원자님의 사랑으로 이 어린이의 삶이 변화됩니다.</p>

        <div class="wrap-bt">
        	<a class="bt-type6" style="width:100%" id="btn_goCDSP">후원하러 가기</a>
        	<a class="bt-type8 mt10" style="width:100%" href="/sponsor/custom/">다시하기</a>
        </div>
        
        
    </div>
    
<!--//-->
</div>





</asp:Content>
