﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_custom_event : MobileFrontBasePage
{
    public override bool RequireSSL
    {
        get
        {
            return false;
        }
    }

    protected override void OnBeforePostBack()
    {
        //Dim MySiteMapProvider As SqlSiteMapProvider = DirectCast(SiteMap.Provider, SqlSiteMapProvider)
        //MySiteMapProvider.Refresh()

        //SiteMapProvider sqlSiteMapProvider = SiteMap.Providers["mobileweb"];
        //sqlSiteMapProvider.
        //SiteMap.Providers["mobileweb"]

        //SiteMapNode currentNode = SiteMap.Providers["mobileweb"].CurrentNode;
        //currentNode.

        var minAge = Convert.ToInt32(Request.QueryString["min_age"].ValueIfNull("-1"));
        var maxAge = Convert.ToInt32(Request.QueryString["max_age"].ValueIfNull("-1"));

        var gender = Request["gender"];
        var country = Request["country"];
        this.ViewState["childPic"] = "";
        this.ViewState["meta_description"] = "한국컴패션 │ 꿈을 잃은 어린이들에게 그리스도의 사랑을";
        resultShow(minAge, maxAge, country, gender);

        /* 이벤트 정보 등록 -- 20170327 이정길 */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;

        //김태형 2017.06.01 
        if (Request.Url.Host.IndexOf('.') != -1)
        {
            Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
            Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        }
        else
        {
            Response.Cookies["e_id"].Domain = "localhost";
            Response.Cookies["e_src"].Domain = "localhost";
        }
    }

    protected override void loadComplete(object sender, EventArgs e)
    {
        this.ViewState["meta_description"] = "한국컴패션 │ 꿈을 잃은 어린이들에게 그리스도의 사랑을";
    }

    protected void resultShow(int minAge, int maxAge, string country, string gender)
    {
        //[이종진] 2018-01-22 기존방법(DB조회 : 1), 신규방법(GlobalPool조회 : 2) 방식을 나눔
        string dbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"];
        var actionResult = new JsonWriter();
        if (dbgp_kind == "1")
        {
            actionResult = new ChildAction().GetChildren(country, gender, null, null, minAge, maxAge, null, null, 1, 10, null, -1, -1);
        }
        else
        {
            actionResult = new ChildAction().GetChildrenGp(country, gender, null, null, minAge, maxAge, null, null, 1, 10, null, -1, -1);
        }
        Random random = new Random();
        if (actionResult.success)
        {
            var items = (List<ChildAction.ChildItem>)actionResult.data;

            //Response.Write(items.Count);
            if (items.Count > 0)
            {
                var i = random.Next(0, items.Count);
                //Response.Write(items[0].ToJson());
                //name.Text = items[i].Name;
                name.Text = items[i].NameKr;    //[이종진] 애칭으로 보이도록함
                age.Text = items[i].Age.ToString();
                country_name.Text = items[i].CountryName;
                this.ViewState["childPic"] = items[i].Pic;
                this.ViewState["childName"] = items[i].Name;
                this.ViewState["childmasterid"] = items[i].ChildMasterId;
                childmasterid.Value = items[i].ChildMasterId;
                //신규방법일 시, Keep할 경우 GlobalPool에 E-Commerce Hold 할때, 필요한 정보들
                childKey.Value = items[i].ChildKey;
                Pic.Value = items[i].Pic;
                IsOrphan.Value = items[i].IsOrphan == true ? "Y" : "N";
                IsHandicapped.Value = items[i].IsHandicapped == true ? "Y" : "N";
                WaitingDays.Value = items[i].WaitingDays.ToString();
                hdnAge.Value = items[i].Age.ToString();
                BirthDate.Value = items[i].BirthDate.ToString("yyyy-MM-dd");
                CountryCode.Value = items[i].CountryCode;
                Gender.Value = items[i].Gender == "남자" ? "M" : "F";
                HangulName.Value = items[i].HangulName;
                HangulPreferredName.Value = items[i].HangulPreferredName;
                FullName.Value = items[i].FullName;
                PreferredName.Value = items[i].PreferredName;
            }
            else
            {
                randomSearch();
                base.AlertWithJavascript("죄송합니다! 선택하신 정보에 맞는 어린이가 없어 후원자님께 맞는 다른 어린이를 추천드립니다.");
            }

        }
        else
        {
            base.AlertWithJavascript(actionResult.message);
        }
    }

    protected void randomSearch()
    {
        //[이종진] 2018-01-22 기존방법(DB조회 : 1), 신규방법(GlobalPool조회 : 2) 방식을 나눔
        string dbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"];
        var actionResult = new JsonWriter();
        if (dbgp_kind == "1")
        {
            actionResult = new ChildAction().GetChildren(null, null, null, null, -1, -1, null, null, 1, 10, "waiting", -1, -1);
        }
        else
        {
            actionResult = new ChildAction().GetChildrenGp(null, null, null, null, -1, -1, null, null, 1, 10, "waiting", -1, -1);
        }

        Random random = new Random();

        if (actionResult.success)
        {
            var items = (List<ChildAction.ChildItem>)actionResult.data;

            if (items.Count > 0)
            {
                var i = random.Next(0, items.Count);

                //name.Text = items[i].Name;
                name.Text = items[i].NameKr;    //[이종진] 애칭으로 보이도록함
                age.Text = items[i].Age.ToString();
                country_name.Text = items[i].CountryName;
                this.ViewState["childPic"] = items[i].Pic;
                this.ViewState["childName"] = items[i].Name;
                this.ViewState["childmasterid"] = items[i].ChildMasterId;
                childmasterid.Value = items[i].ChildMasterId;
                //신규방법일 시, Keep할 경우 GlobalPool에 E-Commerce Hold 할때, 필요한 정보들
                childKey.Value = items[i].ChildKey;
                Pic.Value = items[i].Pic;
                IsOrphan.Value = items[i].IsOrphan == true ? "Y" : "N";
                IsHandicapped.Value = items[i].IsHandicapped == true ? "Y" : "N";
                WaitingDays.Value = items[i].WaitingDays.ToString();
                hdnAge.Value = items[i].Age.ToString();
                BirthDate.Value = items[i].BirthDate.ToString("yyyy-MM-dd");
                CountryCode.Value = items[i].CountryCode;
                Gender.Value = items[i].Gender == "남자" ? "M" : "F";
                HangulName.Value = items[i].HangulName;
                HangulPreferredName.Value = items[i].HangulPreferredName;
                FullName.Value = items[i].FullName;
                PreferredName.Value = items[i].PreferredName;
            }

        }
        else
        {
            base.AlertWithJavascript(actionResult.message);
        }
    }
}