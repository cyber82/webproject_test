﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_custom_result : MobileFrontBasePage {

    public override bool RequireSSL
    {
        get
        {
            return false;
        }
    }

    protected override void OnBeforePostBack() {
        var minAge = Convert.ToInt32(Request.QueryString["min_age"].ValueIfNull("-1"));
        var maxAge = Convert.ToInt32(Request.QueryString["max_age"].ValueIfNull("-1"));

        var gender = Request["gender"];
        var country = Request["country"];
        this.ViewState["childPic"] = "";

        resultShow(minAge, maxAge, country, gender);

    }

    protected override void loadComplete( object sender, EventArgs e ) {

    }

    protected void resultShow( int minAge, int maxAge, string country, string gender ) {
        var actionResult = new ChildAction().GetChildren(country, gender, null, null, minAge, maxAge, null, null, 1, 10, null, -1, -1);
        Random random = new Random();
        if(actionResult.success) {
            var items = (List<ChildAction.ChildItem>)actionResult.data;

            //Response.Write(items.Count);
            if(items.Count > 0) {
                var i = random.Next(0, items.Count);
                //Response.Write(items[0].ToJson());
                name.Text = items[i].Name;
                age.Text = items[i].Age.ToString();
                country_name.Text = items[i].CountryName;
                this.ViewState["childPic"] = items[i].Pic;
                childmasterid.Value = items[i].ChildMasterId;

            } else {
                randomSearch();
                base.AlertWithJavascript("죄송합니다! 선택하신 정보에 맞는 어린이가 없어 후원자님께 맞는 다른 어린이를 추천드립니다.");
            }

        } else {
            base.AlertWithJavascript(actionResult.message);
        }
    }

    protected void randomSearch() {
        var actionResult = new ChildAction().GetChildren(null, null, null, null, -1, -1, null, null, 1, 10, "waiting", -1, -1);

        Random random = new Random();

        if(actionResult.success) {
            var items = (List<ChildAction.ChildItem>)actionResult.data;

            if(items.Count > 0) {
                var i = random.Next(0, items.Count);

                name.Text = items[i].Name;
                age.Text = items[i].Age.ToString();
                country_name.Text = items[i].CountryName;
                this.ViewState["childPic"] = items[i].Pic;
                childmasterid.Value = items[i].ChildMasterId;

            }

        } else {
            base.AlertWithJavascript(actionResult.message);
        }
    }

}
