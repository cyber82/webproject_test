﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using System.IO;
using CommonLib;

public partial class sponsor_pop_pay : MobileFrontBasePage
{

    public override bool IgnoreProtocol
    {
        get
        {
            return true;
        }
    }


    protected override void OnBeforePostBack()
    {


        var requests = Request.GetFriendlyUrlSegments();
        if (requests.Count < 1 || requests[0].Length != 2)
        {
            Response.ClearContent();
            return;
        }

        var is_regular = requests[0].Substring(0, 1) == "Y";
        var is_temporary = requests[0].Substring(1, 1) == "Y";

        ViewState["isRegularSelected"] = is_regular && !is_temporary ? "selected" : "";
        ViewState["isTemporarySeleted"] = !is_regular && is_temporary ? "selected" : "";

        ph_regular.Visible = is_regular;
        ph_temporary.Visible = is_temporary;
    }

}

