﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ios-bridge.aspx.cs" Inherits="sponsor_ios_bridge" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/sponsor/item-child.ascx" TagPrefix="uc" TagName="child" %>
<%@ Register Src="/sponsor/item-user-funding.ascx" TagPrefix="uc" TagName="uf" %>
<%@ Register Src="/sponsor/item-special.ascx" TagPrefix="uc" TagName="sf" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript">
		$(function () {

			var s = location.search;
			
			$.get("/sponsor/pay-gateway.ashx" + location.search + "&bypass=1").success(function (r) {

				if (r.success) {
					
					location.href = r.data;
				} else {
					alert(r.message);
				}
			});
		})
	</script>
</asp:Content>
