﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;

public partial class sponsor_item_child : System.Web.UI.UserControl {

	protected override void OnLoad(EventArgs e) {
		
		base.OnLoad(e);
		
		if(!PayItemSession.HasCookie(this.Context)) {
			return;
		}
		
	}

	public string orderId {
		get {
			if(this.ViewState["orderId"] == null)
				return null;
			return this.ViewState["orderId"].ToString();
		}
		set {
			this.ViewState["orderId"] = value;
		}
	}

	public override bool Visible {
		get {
			return base.Visible;
		}
		set {
			if(value) {
				PayItemSession.Entity payInfo;
				if(this.orderId == null)
					payInfo = PayItemSession.GetCookie(this.Context);
				else
					payInfo = new PayItemSession.Store().Get(this.orderId).data.ToObject<PayItemSession.Entity>();
				this.GetData(payInfo);
			}
			base.Visible = value;
		}
	}

	void GetData( PayItemSession.Entity payInfo) {
		
		string ChildMasterID = payInfo.childMasterId;

		if(string.IsNullOrEmpty(ChildMasterID))
			return;

        var actionResult = new JsonWriter();
        if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
        {
            actionResult = new ChildAction().GetChild(ChildMasterID);
        }
        else if (ConfigurationManager.AppSettings["dbgp_kind"] == "2")
        {
            actionResult = new ChildAction().GetChildGp(ChildMasterID);
        }

        if (!actionResult.success) {
			throw new Exception(actionResult.message);
		}

		var entity = (ChildAction.ChildItem)actionResult.data;

        c_childkey.Text = entity.ChildKey;
		c_namekr.Text = entity.NameKr;
		c_birth.Text = entity.BirthDate.ToString("yyyy.MM.dd");
		c_gender.Text = entity.Gender;
		c_country.Text = entity.CountryName;
		c_age.Text = entity.Age.ToString();
		img.Text = entity.Pic;

		typeName.Text = payInfo.TypeName;
		frequency.Text = payInfo.frequency;
		amount2.Text = amount.Text = payInfo.amount.ToString("N0");

		if(!string.IsNullOrEmpty(payInfo.commitmentId)) {
			pn_paymethod.Visible = true;
			paymethod.Text = payInfo.PayMethodName;
		}
		if(payInfo.frequency == "정기") {
            //amount2_unit.InnerHtml = "원/월";
            amount2_unit.InnerHtml = "원";
            amount.Text += " 원";
		} else {
			amount2_unit.InnerHtml = "원";
			amount.Text += " 원 ";
		}

	}
}
