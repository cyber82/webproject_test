﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pop-pay.aspx.cs" Inherits="sponsor_pop_pay" %>

<script type="text/javascript">


    var initPopPay = function ($scope, sender, use_option) {

        common.bindSNSAction();

        var wh = $(window).height();
        $(window).scroll(function () {
            if ($(this).scrollTop() > wh) {
                $(".footer_action").fadeIn();
                $("#floating-btn").css({ bottom: "100px" })
            } else {
                $(".footer_action").fadeOut();
                $("#floating-btn").css({ bottom: "80px" })
            }
        });

        var frequency = null;

        $(".pay_amount").change(function () {
            if ($(this).val() == "") {
                return;
            }
            var amount = $(this).val();
            sender.setPay(frequency, amount);
            return false;

        });

        $(".pop_frequency").click(function () {
            var val = $(this).data("value");
            //$("#frequency").val(val);
            frequency = val;

            $(".pop_frequency").removeClass("selected");
            $(this).addClass("selected");
            return false;
        })

        // 바로 후원하기
        $(".footer_action .fn_pay").click(function () {

            if (!use_option) {
                sender.setPay(null, null);
                return false;
            }
            $(".footer_action .row-sns").hide();
            $(".footer_action .row-support").toggle();

            if ($(".frequency.selected").length > 0) {
                frequency = $(".frequency.selected").data("id");
                $(".pop_frequency").removeClass("selected");
                $($(".pop_frequency[data-value='" + frequency + "']")).addClass("selected");

            }

            return false;
        })

        /*
		$(".footer_action .fn_sns").click(function () {
			$(".footer_action .row-sns").toggle();
			$(".footer_action .row-support").hide();
			return false;
		})
        */


        if ('<%=ViewState["isRegularSelected"].ToString() %>' == 'selected') {
            frequency = '정기';
        }
        else if ('<%=ViewState["isTemporarySeleted"].ToString() %>' == 'selected') {
            frequency = '일시';
        }
    }

</script>

<div class="bottomscroll-fund bottomscroll-fund-fixed footer_action" style="display: none; z-index: 100000;">
    <!--후원하기-->
    <input type="radio" id="type_1" value="정기" name="pop_frequency" style="display: none" />
    <input type="radio" id="type_2" value="일시" name="pop_frequency" style="display: none" />

    <div class="row-support" style="display: none">
        <div class="tab-column-2">
            <asp:placeholder runat="server" id="ph_regular" visible="false">
			<a class="pop_frequency <%=ViewState["isRegularSelected"].ToString() %>" href="#" data-value="정기">정기후원</a>
			</asp:placeholder>
            <asp:placeholder runat="server" id="ph_temporary" visible="false">
			<a class="pop_frequency <%=ViewState["isTemporarySeleted"].ToString() %>" data-value="일시" href="#">일시후원</a>
			</asp:placeholder>

        </div>
        <div class="mt10 mb10">
            <select style="width: 100%" class="pay_amount">
                <option value="">선택하세요</option>
                <option value="10000">10,000원</option>
                <option value="20000">20,000원</option>
                <option value="30000">30,000원</option>
                <option value="50000">50,000원</option>
                <option value="100000">100,000원</option>
            </select>
        </div>
    </div>
    <!--//후원하기-->
    <!--공유하기-->
    <!--
	<div class="row-sns" style="display:none">
		<a href="#" data-role="sns" data-provider="fb"><img src="/common/img/icon/facebook.png" alt="페이스북" /></a>
		<a href="#" data-role="sns" data-provider="tw"><img src="/common/img/icon/twitter.png" alt="트위터" /></a>
		<a href="#" data-role="sns" data-provider="ks"><img src="/common/img/icon/kakaostory.png" alt="카카오스토리" /></a>
		<a href="#" data-role="sns" data-provider="kt"><img src="/common/img/icon/kakao2.png" alt="카카오톡" /></a>
		<a href="#" data-role="sns" data-provider="line"><img src="/common/img/icon/line2.png" alt="라인" /></a>
	</div>
    -->
    <!--//공유하기-->
    <div class="row">
        <a class="fn_sns sns-share" >
            <img src="/common/img/icon/share2.png" alt="SNS공유" /></a>
        <a href="#" class="fn_pay bt-type6" style="width: 100%">바로 후원하기</a>

    </div>
</div>


<!--// popup -->
