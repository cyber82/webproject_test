﻿$(function () {

	$("#btn_goCDSP").click(function () {

		var childmasterid = $(this).data("childmasterid");

		$.get("/sponsor/pay-gateway.ashx?t=go-cdsp", { childMasterId: childmasterid }).success(function (r) {

			if (r.success) {
				location.href = r.data;
			} else {
				alert(r.message);
			}
		});

	});

	$("#btn_login").click(function () {

		var userId = $("#user_id").val();
		var userPwd = $("#user_pwd").val();

		if (userId == "") {
			alert("아이디를 입력해주세요");
			$("#user_id").focus();
			return false;
		}

		if (userPwd == "") {
			alert("비밀번호를 입력해주세요");
			$("#user_pwd").focus();
			return false;
		}

		$("#userId").val(userId);
		$("#userPwd").val(userPwd);

		$("#exfrm").attr("action", $("#exfrm").attr("action") + "?action=REFER");
		$("#exfrm").submit();

		return false;

	});

	$("#btn_member").click(function () {
		$(".btn_tab").removeClass("selected");
		$(this).addClass("selected");
		$(".pn_add_form").hide();
		$(".pn_login").show();
		$("#user_id").focus();
		return false;
	})

	$("#btn_nonmember").click(function () {
		$(".btn_tab").removeClass("selected");
		$(this).addClass("selected");
		$(".pn_add_form").hide();
		$(".pn_nonmember").show();
		return false;
	})

	$("#btn_noname").click(function () {
		$(".btn_tab").removeClass("selected");
		$(this).addClass("selected");
		$(".pn_add_form").hide();
		$(".pn_noname").show();
		return false;
	})
});

