﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_info : MobileFrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}
	
	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		this.ViewState["LoginPage"] = ConfigurationManager.AppSettings["domain_auth"] + "/m/login/";
		this.ViewState["pic"] = "";
		this.ViewState["childmasterid"] = "";

        //[이종진] 20180202 - 결연 시, 생성되는 sessionId를 파라미터로 넘김
        //비로그인 결연 시, 결연임시테이블에 userid, sponsorid가 없으므로 login페이지에 파라미터로 넘겨서 sessionid로 체크하여 userid,sponsorid를 update
        this.ViewState["sessionId"] = "";
        if (HttpContext.Current.Request.Cookies["sessionId"] != null)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["sessionId"];
            this.ViewState["sessionId"] = cookie.Value;
        }

        btnList.HRef = "/sponsor/";
		if(Request.UrlReferrer != null)
			btnList.HRef = Request.UrlReferrer.AbsoluteUri;

		if(!PayItemSession.HasCookie(this.Context)) {
			Response.Redirect("/");
			return;
		}

		var payInfo = PayItemSession.GetCookie(this.Context);
		
		if(UserInfo.IsLogin) {
			if (payInfo.frequency == "정기") {
				Response.Redirect("/sponsor/pay/regular/");
			} else {
				Response.Redirect("/sponsor/pay/temporary/");
			}
			return;
		}

		hd_title.Value = payInfo.frequency + "후원";

		this.ViewState["joinUrl"] = ConfigurationManager.AppSettings["domain_auth"] + "/m/join/?action=SPONSOR&app_ex=1";
		this.ViewState["findIdUrl"] = ConfigurationManager.AppSettings["domain_auth"] + "/m/find/id/?action=SPONSOR";
		this.ViewState["findPwUrl"] = ConfigurationManager.AppSettings["domain_auth"] + "/m/find/pwd/?action=SPONSOR";


		if(payInfo.frequency == "정기") {
			ph_frequency_regular.Visible = true;
		} else {
			ph_frequency_temporary.Visible = true;
		//	detail_view.Attributes["class"] = "payment temporary";
		}

		var childAction = new ChildAction();
		

		//Response.Write(payInfo.ToJson());
		
		if(payInfo.type == PayItemSession.Entity.enumType.CDSP) {

			view_child.Visible = true;

			var actionResult = childAction.Ensure();
			if(!actionResult.success) {
				base.ConfirmWithJavascript(actionResult.message, "goBack()", "location.href='/sponsor/children/'");
				return;
			}
			
		} else if (payInfo.type == PayItemSession.Entity.enumType.SPECIAL_FUNDING ){
			view_sf.Visible = true;
		} else if(payInfo.type == PayItemSession.Entity.enumType.USER_FUNDING) {

			view_uf.Visible = true;
			/*
			if(string.IsNullOrEmpty(payInfo.childMasterId)) {
				view_uf.Visible = true;
			} else {
				view_child.Visible = true;
			}
			*/
		}

	}

	


}