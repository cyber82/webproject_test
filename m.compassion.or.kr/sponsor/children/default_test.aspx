﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default_test.aspx.cs" Inherits="sponsor_children_default_test" MasterPageFile="~/main_without_header.Master"%>
<%@ MasterType virtualpath="~/main_without_header.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/sponsor/children/default.js"></script>
	<script type="text/javascript" src="/sponsor/children/default_test.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '267588443715394'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=267588443715394&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<!--NSmart Track Tag Script-->
<script type='text/javascript'>
callbackFn = function() {};
var _nsmart = _nsmart || [];
_nsmart.host = _nsmart.host || (('https:' == document.location.protocol) ? 'https://' : 'http://');
_nsmart.push([12490, 31583]); // 캠페인 번호와 페이지 번호를 배열 객체로 전달
document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
</script>
<!--NSmart Track Tag Script End..-->

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="s_country_list"/>
    
	<a id="btnList" href="/"></a>
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="sponsor-nurture">
			<!-- 어린이리스트 -->
			<ul class="list-childselect mt20" id="l">
				<li ng-repeat="item in list" >
            		<span class="wrap-caption" >
						<span class="txt-clock">{{item.waitingdays}}일</span>
						<span class="share" ng-click="sns.show($event , {url : '/sponsor/children/?c=' +  item.childmasterid, title : '[한국컴패션-1:1어린이양육]' + item.name , desc : '' , picture : item.pic})">
                            <img src="/common/img/icon/share2.png" alt="sns공유" /></span>
					</span>
					<a href="#" ng-click="showChild($event , item)" onclick="javascript:NTrackObj.callTrackTag('31585', callbackFn, 12490);">					
						<span class="pos-photo" ><span class="photo" style="background:url('{{item.pic}}') no-repeat"></span></span>
						<span class="txt-name">{{item.name}}</span>
						<span class="txt-info">
							국가 : <span class="color1">{{item.countryname}}</span><br />
							생일 : <span class="color1">{{item.birthdate | date:'yyyy.MM.dd'}} ({{item.age}}세)</span><br />
							성별 : <span class="color1">{{item.gender}}</span>
						</span>
					</a>
					
				</li>
				
				<!-- 검색결과 없을때 -->
				<li class="no-result" ng-if="nodata">선택하신 조건에 맞는 어린이가 없습니다.</li>
				<!--// -->

			</ul>

			<!--<div class="wrap-bt" ng-show="!nomore"><a class="bt-type6" style="width:100%" ng-click="showMore($event)">더 많은 어린이들 보기<span class="bt-bu2"></span></a></div>-->
			<!--// 어린이리스트 -->

		</div>
    
	<!--//-->
	</div>







</asp:Content>
