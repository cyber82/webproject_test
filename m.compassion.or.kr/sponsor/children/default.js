﻿$(function () {

    $("#btn_goCDSP").click(function () {

        var childmasterid = $(this).data("childmasterid");

        loading.show();

        $.get("/sponsor/pay-gateway.ashx?t=go-cdsp", { childMasterId: childmasterid }).success(function (r) {
            if (r.success) {
                location.href = r.data;
            } else {
                alert(r.message);
            }
        });

    });

    $(".list-faq li").click(function () {
        //console.log($(this).css("display"));
        if ($(this).hasClass("on")) {
            $(this).removeClass("on");
        } else {
            $(".list-faq li").removeClass("on");
            $(this).addClass("on");
        }

    });
    


});


(function () {
    var app = angular.module('cps.page', []);

    var first = true;

    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {


        $scope.list = [];
        //$scope.detail = [];
        $scope.nomore = false;
        $scope.nodata = false;
        //[이종진] 조회방법
        $scope.dbgp_kind = $("#dbgp_kind").val();
        $scope.list30 = []; //GP조회의 경우, 30개를 미리 넣어둠

        $scope.ageRange = [];
        for (var i = 1; i < 21; i++) {
            $scope.ageRange.push(i.toString());
        };

        $scope.dateRange = [];
        for (var i = 1; i < 13; i++) {
            $scope.dateRange.push(i.toString())
        };
        $scope.dayRange = [];
        for (var i = 1; i < 32; i++) {
            $scope.dayRange.push(i.toString());
        };


        $scope.params = {
            //orderby: paramService.getParameter("orderby") ? paramService.getParameter("orderby") : "new",
            orderby: paramService.getParameter("orderby") ? paramService.getParameter("orderby") : "waiting",
            page: 1,
            rowsPerPage: 10,
            country: paramService.getParameter("country") ? paramService.getParameter("country") : "",
            //minAge: "3",
            minAge: paramService.getParameter("minAge") ? paramService.getParameter("minAge") : "1",
            //maxAge: "20",
            maxAge: paramService.getParameter("maxAge") ? paramService.getParameter("maxAge") : "20",
            //gender: "",		// 성별 Y,N
            gender: paramService.getParameter("gender") ? paramService.getParameter("gender") : "",
            //today: "",		// 오늘이 생일인경우 Y,N
            today: paramService.getParameter("today") ? paramService.getParameter("today") : "",
            //mm: "",			// 특별한 기념일 월 mm
            mm: paramService.getParameter("mm") ? paramService.getParameter("mm") : "",
            //dd: "",			// 특별한 기념일 일 dd
            dd: paramService.getParameter("dd") ? paramService.getParameter("dd") : "",
            orphan: paramService.getParameter("orphan") ? paramService.getParameter("orphan") : "",		// 고아 Y,N
            specialNeed: paramService.getParameter("specialNeed") ? paramService.getParameter("specialNeed") : ""	// 장애 Y,N
        };

        if (paramService.getParameter("specialNeed") == "Y")
            $("#s_specialNeed").prop("checked", true);

        if (paramService.getParameter("orphan") == "Y")
            $("#s_orphan").prop("checked", true);

        if (paramService.getParameter("orderby") == "waiting")
            $("#s_waiting").prop("checked", true);

        if (paramService.getParameter("today") == "Y")
            $("#s_today").prop("checked", true);

        // list
        $scope.getList = function (params) {
           
            switch ($scope.dbgp_kind) {
                case "1":
                    $scope.getListDB();
                    break;
                case "2":
                    $scope.getListGP();
                    break;
                default:
                    $scope.getListDB();
                    break;
            }
        }

        $scope.getListDB = function (params) {

        	$scope.params = $.extend($scope.params, params);
        	console.log($scope.params);
            //console.log($scope.params);
            if ($scope.params.maxAge == null) {
                $scope.params.maxAge = "16";
            }
            $http.get("/api/tcpt.ashx?t=list", { params: $scope.params }).success(function (r) {
                //console.log(r);
                if (r.success) {

                    var data = r.data;

                    $scope.nomore = data.length < 1

                    $.each(data, function (i) {
                        this.birthdate = new Date(this.birthdate);
                        this.row = $scope.list.length;

                    });
                    $scope.list = $.merge($scope.list, data);
                    //console.log($scope.list);


                    // SNS 애니메이션
                    setTimeout(function () {
                        common.bindSNSAction();
                    }, 500);


                    if ($scope.params.page == 1) {
                        $scope.nodata = r.data.length < 1;
                    }

                    if (params)
                        scrollTo($("#l"), 30);
                } else {

                    //alert(r.message);
                }
            });


        };

        //[이종진] 2018-01-31 GlobalPool 조회의 경우, 조회속도가 최소 3초이상이므로 한번에 무조건 30개를 가져옴. 
        // '더보기'(아래화살표. showmore) 클릭 시, 6개씩 3번을 더 뿌려주고(모바일은10개씩3번), 더보기 추가 클릭 시, 더 조회되지는 않음. (고객(장성권과장)과 협의한 사항임)
        $scope.getListGP = function (params) {

            $scope.params = $.extend($scope.params, params);
            console.log($scope.params);
            //console.log($scope.params);
            if ($scope.params.maxAge == null) {
                $scope.params.maxAge = "16";
            }
            //파라미터중, 페이지관련 재설정.
            $scope.params.rowsPerPage = 30; //한번에 30개를 가져옴.
            $http.get("/api/tcpt.ashx?t=list", { params: $scope.params }).success(function (r) {
                //console.log(r);
                if (r.success) {

                    $scope.list30 = r.data; //30개를 미리 넣어둠
                    var data = r.data;

                    //$scope.nomore = data.length < 1
                    $scope.nomore = data.length / 10 <= 1; //10개보다 같거나 적으면, 더 보여줄 데이터가 없으므로, showmore 버튼 숨김

                    var row = [];
                    $.each(data, function (i) {
                        if (i == 10) { //10개만 바인딩함
                            return false; //each 문은 return false 가 break 역할을 함
                        }
                        this.birthdate = new Date(this.birthdate);
                        this.row = $scope.list.length;
                        //row.push(this);
                        $scope.list.push(this);
                    });
                    //$scope.list.push($.extend([], row));
                    //$scope.list = $.merge($scope.list, data);
                    //console.log($scope.list);


                    // SNS 애니메이션
                    setTimeout(function () {
                        common.bindSNSAction();
                    }, 500);


                    if ($scope.params.page == 1) {
                        $scope.nodata = r.data.length < 1;
                    }

                    if (params)
                        scrollTo($("#l"), 30);
                } else {

                    //alert(r.message);
                }
            });


        };

        $scope.search = function ($event) {
            $event.preventDefault();
            console.log($scope.params.minAge)
            console.log($scope.params.maxAge)
            console.log(!$scope.params.minAge)
            console.log(!$scope.params.maxAge)
            
                if (parseInt($scope.params.minAge) > parseInt($scope.params.maxAge)) {
                    alert("나이 설정을 다시 해주세요.");
                    return false;
                }
            

            $scope.params.page = 1;
            $scope.list = [];
            $scope.getList();
            $scope.showSearch.instance.hide();
        };

        $scope.showMore = function ($event) {
            //DB조회
            if ($scope.dbgp_kind == "1") {
                $event.preventDefault();
                $scope.params.page++;
                $scope.getList();
            }
            else {
                $event.preventDefault();
                $scope.params.page = $scope.params.page + 1;
                //$scope.getList();
                //페이지카운트만큼의 데이터를 data에 넣어준다.
                var start = ($scope.params.page - 1) * 10;
                var end = ($scope.params.page * 10) - 1;
                var data = $scope.list30;

                $.each(data, function (i) {
                    if (i < start) { //start보다 적으면 continue해서 다음 실행. * each문은 return true가 continue
                        return true;
                    }
                    if (i > end) {
                        return false; //end보다 크면 break. * each 문은 return false 가 break
                    }
                    this.birthdate = new Date(this.birthdate);
                    $scope.list.push(this);
                });
                //개수에 따라서, showmore 숨김. end는 9, 19, 29, ..이기 때문에 , +1해줌
                $scope.nomore = data.length <= end + 1;

            }
        };

        $scope.checkOption = function ($event, id) {

            $scope.params.page = 1;

            var checked = $("#" + id).prop("checked");

            if (id == "s_orphan") {
                $scope.params.orphan = checked ? "Y" : "";
            } else if (id == "s_specialNeed") {
                $scope.params.specialNeed = checked ? "Y" : "";
            } else if (id == "s_waiting") {
                $scope.params.orderby = checked ? "waiting" : "new";
            } else if (id == "s_today") {
                $scope.params.today = checked ? "Y" : "";

                if (checked) {

                    $scope.params.mm = "";
                    $scope.params.dd = "";

                    var mm_sb = $("#s_mm").attr("sb");
                    $("#sbSelector_" + mm_sb).text($($("#s_mm option")[0]).text());

                    var dd_sb = $("#s_dd").attr("sb");
                    $("#sbSelector_" + dd_sb).text($($("#s_dd option")[0]).text());

                }

            }


        };



        $scope.showChildPop = function ($event, item) {
            loading.show();

            if ($event) $event.preventDefault();
            popup.init($scope, "/common/child/pop/" + item.countrycode + "/" + item.childmasterid + "/" + item.childkey, function (modal) {

                modal.show();
                //setTimeout(function () {
                    initChildPop($http, $scope, modal, item);
                //}, 2000);
                


                if (!first) {
                    var center = map.getCenter();
                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center);
                }
                first = false;

            }, { top: 0, iscroll: true, removeWhenClose: true });
        };


        $scope.showSearch = {
            instance: null,
            show: function ($event) {

                if ($event) $event.preventDefault();
                popup.init($scope, "/sponsor/children/search/", function (modalSearch) {
                    $scope.showSearch.instance = modalSearch;
                    modalSearch.show();

                    //$("#s_waiting").prop('checked', $scope.params.orderby == "waiting" ? true : false);
                    $("#s_waiting").prop('checked', true);
                    $("#s_today").prop('checked', $scope.params.today == "Y" ? true : false);
                    $("#s_specialNeed").prop('checked', $scope.params.specialNeed == "Y" ? true : false);
                    $("#s_orphan").prop('checked', $scope.params.orphan == "Y" ? true : false);

                }, { top: 0, iscroll: true, removeWhenClose: true, backgroundClick: true });


            },

            close: function ($event) {
                $event.preventDefault();
                $scope.showSearch.instance.hide();
            }
        };
        $scope.getList();

        var childMasterId = paramService.getParameter("c");
        if (childMasterId) {

            $http.get("/api/tcpt.ashx?t=get", { params: { childMasterId: childMasterId } }).success(function (r) {

                if (r.success) {

                    var data = r.data;
                    data.birthdate = new Date(data.birthdate);

                    $scope.showChildPop(null, data);


                } else {

                    alert(r.message);
                }
            });

        };

        

        $scope.changeDate = function () {
            $scope.params.today = "";
            $("#s_today").prop("checked", false);

            if ($scope.params.mm == "2") {
                $scope.dayRange = [];
                for (var i = 1; i < 30; i++) {
                    $scope.dayRange.push(i);
                };
                
            } else {
                $scope.dayRange = [];
                for (var i = 1; i < 32; i++) {
                    $scope.dayRange.push(i);
                };
            }
        };

        $scope.s_country_list = $.parseJSON($("#s_country_list").val());

        $scope.reset = function ($event) {
            $event.preventDefault();
            $("#s_country").val("");
            $("#s_minAge").val("");
            $("#s_maxAge").val("");
            $("#s_mm").val("");
            $("#s_dds").val("");
            $(".check_value")
            $("#sex_all").prop('checked', true);
            $("#s_waiting, #s_today, #s_specialNeed, #s_orphan").prop('checked', false);

            $scope.params = {
                orderby: "new",
                country: "",
                minAge: "1",
                maxAge: "20",
                gender: "",
                today: "",
                mm: "",
                dd: "",
                orphan: "",
                specialNeed: ""
            };


        };

    });

})();


