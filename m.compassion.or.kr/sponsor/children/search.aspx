﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="search.aspx.cs" Inherits="sponsor_children_search" %>

<div style="background: #fff;" class="fn_pop_container" id="organizationModal">

    <div class="wrap-layerpop fn_pop_content" style="width: 100%; left: 0;">
        <div class="header2">
            <p class="txt-title">어린이 검색</p>
        </div>
        <div class="contents">

            <div class="sponsor-nurture">

                <fieldset class="frm-input">
                    <legend>어린이검색 정보 입력</legend>
                    <div class="row-table">
                        <div class="col-th" style="width: 30%">성별</div>
                        <div class="col-td" style="width: 70%">
                            <ul class="tab-column-3">

                                <li style="width: 33%">
                                    <input type="radio" id="sex_all" class="check_value" name="s_gender" ng-model="params.gender" value="" style="left: 0; position: absolute; width: 100px; height: 50px" checked />
                                    <label for="sex_all">전체</label></li>
                                <li style="width: 34%">
                                    <input type="radio" id="sex_1" class="check_value" name="s_gender" value="M" ng-model="params.gender" /><label for="sex_1">남자</label></li>
                                <li style="width: 33%">
                                    <input type="radio" id="sex_2" class="check_value" name="s_gender" value="F" ng-model="params.gender" /><label for="sex_2">여자</label></li>
                            </ul>

                        </div>
                    </div>
                    <div class="row-table">
                        <div class="col-th" style="width: 30%">국가</div>
                        <div class="col-td" style="width: 70%">

                            <select style="width: 100%" ng-model="params.country" id="s_country" ng-options="option.c_id as option.c_name for option in s_country_list">
                                <option value="">전체</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-table">
                        <div class="col-th" style="width: 30%">나이</div>
                        <div class="col-td" style="width: 70%">
                            <select style="width: 39%" id="s_minAge" ng-model="params.minAge" ng-options="option for option in ageRange">
                                <option value="">전체</option>
                            </select>&nbsp;세 ~&nbsp;
                        <select style="width: 39%" id="s_maxAge" ng-model="params.maxAge" ng-options="option for option in ageRange">
                            <option value="">전체</option>
                        </select>&nbsp;세
                        </div>
                    </div>
                    <div class="row-table2">

                        <div class="col-th" style="width: 30%">특별한 기념일</div>
                        <div class="col-td" style="width: 70%">
                            <select style="width: 40%" id="s_mm" ng-change="changeDate()" ng-model="params.mm" ng-options="option for option in dateRange">
                                <option value="">전체</option>
                            </select>&nbsp;월&nbsp;&nbsp;&nbsp;
                        <select style="width: 40%" id="s_dd" ng-change="changeDate()" ng-model="params.dd" ng-options="option for option in dayRange">
                            <option value="">전체</option>
                        </select>&nbsp;일
                        </div>
                    </div>
                    <div class="row-table2">
                        <div class="col-td" style="width: 100%">
                            <span class="checkbox-ui mt10">
                                <input type="checkbox" class="css-checkbox" id="s_waiting" ng-click="checkOption($event,'s_waiting')" name="view-child" />
                                <label for="s_waiting" class="css-label" style="font-size: 13px">오래 기다린 어린이부터 보기</label>
                            </span>
                            <br />
                            <span class="checkbox-ui mt10 mb10">
                                <input type="checkbox" class="css-checkbox" id="s_today" ng-click="checkOption($event,'s_today')" name="view-child" />
                                <label for="s_today" class="css-label" style="font-size: 13px">오늘 생일인 어린이 보기</label>
                            </span>
                        </div>
                    </div>
                    <div class="row-table2">
                        <div class="col-td" style="width: 100%">
                            <p style="font-family: 'noto_r'; font-size: 13px; color: #333">관심과 보호가 필요해요</p>
                            <span class="checkbox-ui mt10">
                                <input type="checkbox" class="css-checkbox" id="s_specialNeed" ng-click="checkOption($event,'s_specialNeed')" name="type-child" />
                                <label for="s_specialNeed" class="css-label" style="font-size: 13px">몸이 불편한 어린이</label>
                            </span>
                            <br />
                            <span class="checkbox-ui mt10 mb10">
                                <input type="checkbox" class="css-checkbox" id="s_orphan" ng-click="checkOption($event,'s_orphan')" name="type-child" />
                                <label for="s_orphan" class="css-label" style="font-size: 13px">부모님이 안 계신 어린이</label>
                            </span>
                        </div>
                    </div>
                </fieldset>

                <div class="wrap-bt">
                    <a class="bt-type8 fl" style="width: 49%" ng-click="reset($event)">검색 초기화</a>
                    <a class="bt-type6 fr" style="width: 49%" href="#" ng-click="search($event)">검색하기</a>
                </div>

            </div>

        </div>
        <div class="close2"><span class="ic-close" ng-click="showSearch.close($event)">레이어 닫기</span></div>
        <div style="height: 50px"></div>
    </div>
</div>

