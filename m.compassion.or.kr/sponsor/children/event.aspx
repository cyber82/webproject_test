﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="event.aspx.cs" Inherits="sponsor_children_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/sponsor/children/event.js?v=1215"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="s_country_list"/>
    
	<a id="btnList" href="/"></a>
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="sponsor-nurture">
    	
			<div class="wrap-visualtop sectionsub-margin1" style="background:url('/common/img/page/sponsor/img-topvisual17.jpg') no-repeat">
				<div class="dim"></div>    <!--dim-->
				<div class="contain" style="padding-top:35px">    <!--padding으로 컨텐츠 가운데에 맞게 조정-->
					<strong class="txt-slogon">한 어린이의 삶이 바뀝니다.</strong>
					한 어린이가 꿈을 찾는 양육비는 매월 4만 5천원입니다.<br />
					후원금과 기도, 사랑의 편지로<br />
					어린이의 삶에 기적을 선물해주세요.
					<div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/custom/">어떤 어린이를 후원해야 할지 모르겠어요</a></div>
				</div>
			</div>
			<span class="detail-search sectionsub-margin1" ng-click="showSearch.show($event)">상세검색 열기<span class="bu"></span></span>
        

			<!-- 어린이리스트 -->
			<ul class="list-childselect mt20" id="l">
				<li ng-repeat="item in list" >
            		<span class="wrap-caption" >
						<span class="txt-clock">{{item.waitingdays}}일</span>
						<span class="share" ng-click="sns.show($event , {url : '/sponsor/children/?c=' +  item.childmasterid, title : '[한국컴패션-1:1어린이양육]' + item.name , desc : '' , picture : item.pic})">
                            <img src="/common/img/icon/share2.png" alt="sns공유" /></span>
					</span>
					<a href="#" ng-click="showChildPop($event , item)">
						<span class="pos-photo" ><span class="photo" style="background:url('{{item.pic}}') no-repeat"></span></span>

						<span class="txt-name">{{item.name}}</span>
						<span class="txt-info">
							국가 : <span class="color1">{{item.countryname}}</span><br />
							생일 : <span class="color1">{{item.birthdate | date:'yyyy.MM.dd'}} ({{item.age}}세)</span><br />
							성별 : <span class="color1">{{item.gender}}</span>
						</span>
					</a>
					
				</li>
				
				<!-- 검색결과 없을때 -->
				<li class="no-result" ng-if="nodata">선택하신 조건에 맞는 어린이가 없습니다.</li>
				<!--// -->

			</ul>

			<div class="wrap-bt" ng-show="!nomore"><a class="bt-type6" style="width:100%" ng-click="showMore($event)">더 많은 어린이들 보기<span class="bt-bu2"></span></a></div>
			<!--// 어린이리스트 -->

			
			<!-- 함께하는 양육 -->
			<div class="together sectionsub-margin1">
				<p class="txt-caption">한 어린이와 끝까지 함께하는 양육</p>
				<span class="bar"></span>
				<p class="con">엄마 뱃속에서부터 스스로 가난에서 벗어날 수 있을 때까지 각 단계에 맞는 커리큘럼으로 어린이들을 양육합니다.</p>
				<div class="desc-wrap desc1 sectionsub-margin1">
					<div class="desc1">
						<p class="tit">태아&middot;영아생존프로그램 <span class="period">(0-3세)</span></p>
						태아·영아 사망률이 높은 지역의 산모와 0~3세의<br />
						어린이가 건강하게 생존할 수 있도록 합니다.
					</div>
					<div class="desc2">
						<p class="tit">1:1어린이양육프로그램</p>
						<span class="period">(3세-자립 가능한 성인)</span>
						컴패션 사역의 핵심 프로그램으로서,<br />
						전인적인 양육을 통해 모든 어린이들에게<br />
						‘가난에서 벗어날 수 있는 힘’을 제공합니다.
					</div>
				</div>
				<div class="desc-wrap desc2 sectionsub-margin1">
					<div class="desc3">
						<p class="tit">양육보완프로그램</p>
						1:1어린이양육을 기초로, 어린이들에게 발생하는<br />
						재난 구호, 의료 활동, 교육 지원 등 양육의 빠진 부분들을<br />
						보완하고 온전한 양육 환경을 갖출 수 있도록 돕습니다.
					</div>
				</div>
			</div>
			<!--//함께하는 양육 -->
        

			<!-- 삶의변화 -->
			<div class="life-change sectionsub-margin1">
        		<p class="txt-caption">어린이들의 삶은 어떻게 변화될까요?</p>
				컴패션 졸업생 삼손이 1:1어린이양육프로그램에 대해 설명합니다. 가난 속에서 태어난 어린이들이 어떤 양육을 받으며 변화되는지 들여다볼까요?
				<div class="box mt30">
            		<strong class="ic1">건강하게 자랐어요(신체적 영역)</strong>
					“어린이센터에서 정기적인 건강검진과 균형잡힌 식사, 위생교육을 받으며 자랐어요. 그들은 제 건강과 관련된 모든 것들을 가르쳐주고 지원해줬어요.”
				</div>
				<div class="box mt20">
            		<strong class="ic2">충분한 교육을 받았어요(지적 영역)</strong>
					“어린이센터에 등록됐다는 소식을 듣고, 저도 교복을 입고 학교에 갈 수 있다는 생각에 정말 기뻤어요. 체험 활동과 방과후학습 등의 다양한 교육들이 저의 삶을 바꿔놨어요.”
				</div>
				<div class="box mt20">
            		<strong class="ic3">내 편이 생겼어요(사회·정서적 영역)</strong>
					“컴패션을 통해 제가 사랑 받고 있다는 사실을 알았어요. 저를 위해 항상 기도하고 격려해주시던 후원자님의 편지는 아직도 간직하고 있는 보물이랍니다.”
				</div>
				<div class="box mt20">
            		<strong class="ic4">예수님의 사랑을 배웠어요(영적 영역)</strong>
					“어린이센터에서 처음 예수님에 대해서 배웠던 시간은 절대 잊을 수 없는 사건이었어요. 선생님들로부터 성경이야기를 듣고 함께 찬양하던 시간은 정말 행복했던 경험이었어요.”
				</div>
			</div>
			<!--// 삶의변화 -->

			<!-- 자주묻는질문 -->

			<div class="faq sectionsub-margin1">
        		<p class="txt-caption">자주 묻는 질문</p>
				<ul class="list-faq">
            		<li><span class="field-question" onClick="">후원금은 어떻게 쓰이나요?<span class="bu"></span></span>   <!--선택 됐을 때 "on"  만 li에 주면 됩니다-->
                		<!---->	
						<span class="field-answer">
							<span class="txt-color1">컴패션은 후원금의 80% 이상을 반드시 어린이 양육을 위해 사용합니다. 모든 양육은 현지 인력과 시설을 중심으로 진행되며, 이는 비용 절감 및 현지 경제에도 큰 도움을 줍니다.</span><br /><br />
							<strong class="txt-color2">1. 어린이 양육비, 눈으로 확인할 수 있어요</strong>
							7,000여 개 어린이센터에서는 매달 사용된 후원금을 정기적으로 보고하며, 모든 세금계산서와 영수증을 정리하여 언제든 열람할 수 있게 합니다.<br /><br />
                        
							<strong class="txt-color2">2. 어린이 양육에 직접 쓰여집니다</strong>
							학교 교육, 균형 잡힌 식사, 건강관리 및 치료,  교재 및 필수품 구입, 1:1 상담 등 어린이에게 직접 사용됩니다.<br />
							※ 어린이 양육 환경 개선에 사용되는 비용은 별도 기금으로 운영됩니다.<br /><br />
                        
							<strong class="txt-color2">3. 매달보고, 매년 감사, 투명하게 운영돼요</strong>
							매달 정기 보고 시, 미흡한 점이 발견되면 불시에 감사를 받고 3일~7일 사이에 모든 보고를 점검 받습니다. 각 수혜국의 어린이센터는 B등급 이상의 평가를 받아야만 지속적인 운영이 가능합니다. <br /><br />
                        
							<strong class="txt-color2">4. 통일된 시스템으로 운영돼요</strong>
							12개 후원국과 26개 수혜국이 국제컴패션 사무국을 통해 동일한 비용절감 효과와 높은 효율성을 갖습니다.<br /><br />
                        
							<strong class="txt-color2">5. 어린이 센터를 관리하는 양육사업 지원 담당자인 PF(Program Facilitator)가 있어요</strong>
							전인적인 어린이 양육과 협업에 대한  현지 전문가인 직원 (PF)이 어린이센터를 정기적으로 직접 방문해 어린이들이 제대로 양육 받고 있는지 관리하고 감독합니다.
						</span>
						<!--//-->	
					</li>
					<li><span class="field-question" onClick="">비기독교인 어린이도 후원을 받을 수 있나요?<span class="bu"></span></span>
                		<!---->	
						<span class="field-answer">
							<div class="mb20"><img src="/common/img/page/sponsor/img53.jpg" alt="" width="100%" /></div>
							<strong class="txt-color2">네, 그렇습니다.</strong>
							컴패션은 어린이와 가족들의 종교와 관계없이 어린이가 가진 존귀함을 최고의 가치로 여기며, 예수님의 사랑을 바탕으로 어린이들을 돕습니다. 따라서 가장 연약하고 도움이 필요한 어린이들을 돕습니다.<br /><br />
							만성 질병이나 영양실조에 걸린 어린이들이  어린이센터에 최우선으로 등록되고, 그 다음은 저소득 가정의 어린이들이 등록됩니다.<br /><br />
							그 외에도 어린이센터가 있는 지역에 오래 남아있을 수 있는지, 어린이들이 가까이 사는지 등을 고려하여 등록시키게 됩니다.
						</span>
						<!--//-->
					</li>    	
					<li><span class="field-question" onClick="">컴패션 1:1 양육의 효과는 어떤가요?<span class="bu"></span></span>
                		<!---->	
						<span class="field-answer">
							<span class="txt-color1">네, 그렇습니다.</span>
							미국 경제학자인 브루스 위딕 교수가 세계 최초로 1:1어린이결연의 경제학적 효과를 조사했습니다. 
							경제학적 분석으로 진행된 이 연구는 <strong>컴패션의 1:1어린이 양육프로그램이 어린이의 삶에 큰 변화를 가져왔음</strong>을 보여주고 있습니다.
							<span class="diagram1 mt30">
                        		<span class="txt-slogon">교육<em>더 커진 미래</em></span>
								<span class="pos-txt1">중등교육 졸업 비율<br />27 ~ 40 %</span>
								<span class="pos-txt2">대학교육을 마친 비율<br />50 ~ 80 %</span>
							</span>
							<span class="diagram2 mt30">
                        		<span class="txt-slogon">직업<em>스스로의 가능성을<br />찾아 나가는 힘</em></span>
								<span class="pos-txt1">직장인이 되는 비율<br />14 ~ 18 %</span>
								<span class="pos-txt2">선생님이 되는 비율<br />65 %</span>
							</span>
							<span class="diagram3 mt30">
                        		<span class="txt-slogon">리더십<em>베푸는 사람으로의 성장</em></span>
								<span class="pos-txt1">지역사회 지도자가 될 가능성<br />30 ~ 75 %</span>
								<span class="pos-txt2">교회 지도자가 될 가능성<br />40 ~ 70 %</span>
							</span>
						</span>
						<!--//-->
					</li>
				</ul>
			</div>
			<!--// 자주묻는질문 -->

			<!-- 양육을 하시면? -->
			<div class="down-guide mt30">
        		<p class="txt-caption">1:1어린이양육을 하시면?</p>
				결연하신 어린이의 사진이 담긴 소개서와 가이드파일을 받으실 수 있습니다.
				<ul class="list-guide">
            		<li><span class="wrapping1"><img src="/common/img/page/sponsor/img49.jpg" alt="" /></span>
						<span class="wrapping2"><span class="txt">첫 결연 후원자 가이드</span></span>
					</li>
					<li><span class="wrapping1"><img src="/common/img/page/sponsor/img50.jpg" alt="" /></span>
						<span class="wrapping2"><span class="txt">어린이의 편지<br />(온라인)</span></span>
					</li>
					<li><span class="wrapping1"><img src="/common/img/page/sponsor/img51.jpg" alt="" /></span>
						<span class="wrapping2"><span class="txt">어린이 성장 보고서</span></span>
					</li>
					<li><span class="wrapping1"><img src="/common/img/page/sponsor/img52.jpg" alt="" /></span>
						<span class="wrapping2"><span class="txt">E-mail뉴스레터<br />(신청자 대상)</span></span>
					</li>
				</ul>
			</div>
			<!--// 양육을 하시면? -->

		</div>
    
	<!--//-->
	</div>







</asp:Content>
