﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_children_default : MobileFrontBasePage {

    public override bool RequireSSL
    {
        get
        {
            return false;
        }
    }

    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();

        this.ViewState["pic"] = "";
        this.ViewState["childmasterid"] = "";

        

        // 국가정보
        var actionResult = new CountryAction().GetList();
        if(!actionResult.success) {
            base.AlertWithJavascript(actionResult.message, "goBack()");
            return;
        }

        var countries = (List<sp_country_list_fResult>)actionResult.data;

        s_country_list.Value = countries.ToJson();

    }

    protected override void loadComplete( object sender, EventArgs e ) {

        var childMasterId = Request["c"].EmptyIfNull();
        if(string.IsNullOrEmpty(childMasterId)) {

        } else {
            var actionResult = new ChildAction().GetChild(childMasterId);
            if(actionResult.success) {
                var item = (ChildAction.ChildItem)actionResult.data;
                // SNS
                this.ViewState["meta_url"] = Request.UrlEx();
                this.ViewState["meta_title"] = "[한국컴패션-1:1어린이양육]-" + item.NameKr;

                // optional(설정 안할 경우 주석)
                //this.ViewState["meta_description"] = "";
                this.ViewState["meta_keyword"] = this.ViewState["meta_keyword"].ToString() + ",1:1어린이양육," + item.NameKr + "," + item.NameEn;
                // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,MobileFrontBasePage)
                this.ViewState["meta_image"] = item.Pic.WithFileServerHost();

            }


        }


    }


}