﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CommonLib;
using Newtonsoft.Json.Linq;
using System.Configuration;

public partial class _default2 : MobileFrontBasePage {

	protected override void OnBeforePostBack() {

        /*         이벤트 정보 등록  -- 20170327 이정길       */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;
        
        //김태형 2017.06.01
        if (Request.Url.Host.IndexOf('.') != -1)
        {
            Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
            Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        }
        else
        {
            Response.Cookies["e_id"].Domain = "localhost";
            Response.Cookies["e_src"].Domain = "localhost";
        }

        base.OnBeforePostBack();

		if (AppSession.HasCookie(this.Context)) {
			Response.Redirect("/app/main");
		}

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.content.First(p => p.c_id == "main");
            var entity = www6.selectQF<content>("c_id", "main");

            var data = JObject.Parse(entity.c_text.ToString());

            children.Value = data["child"].ToString();
            //main_content.Text = data["content"].ToString();

        }
		this.GetMainVisualChild();

		this.GetEvent();

		this.GetSympathy();


	}




	// 비로그인 나오는 어린이 추천 3종 , 일별 캐싱
	void GetMainVisualChild() {
		
		var today = DateTime.Now.ToString("yyyyMMdd");
		var action = new ChildAction();
		
		#region 생일이 어제인 어린이
		{

			var yesterday = DateTime.Now.AddDays(-1);
			var month = Convert.ToInt32(yesterday.ToString("MM"));
			var day = Convert.ToInt32(yesterday.ToString("dd"));
            /*
			var exist = false;
			if (Application["main_visual_child_type1"] != null) {
				var data = (KeyValuePair<string, object>)Application["main_visual_child_type1"];
				if (data.Key == today) {
					exist = true;
				} else {
					Application["main_visual_child_type1"] = null;
				}
			}

			if(!exist) {
				var actionResult = action.GetChildren(null, null, month, day, null, 20, null, null, 1, 3, "new", -1, -1);
				if(actionResult.success) {
					Random rnd = new Random();
					var items = (List<ChildAction.ChildItem>)actionResult.data;
					var index = rnd.Next(items.Count);

					var item = items[index];
					var data = new KeyValuePair<string, object>(today, item);
					Application["main_visual_child_type1"] = data;
				}
			}
			
			if(Application["main_visual_child_type1"] != null) {
				var data = (KeyValuePair<string, object>)Application["main_visual_child_type1"];
				var item = (ChildAction.ChildItem)data.Value;

				//main_recmd1_pic.Style["background"] = "url('"+ item.Pic +"') no-repeat";

				main_recmd1_country.Text = main_recmd1_country2.Text = item.CountryName;
				main_recmd1_age.Text = main_recmd1_age2.Text = item.Age.ToString();
				main_recmd1_gender.Text = item.Gender;
				main_recmd1_waitingdays.Text = item.WaitingDays.ToString();
				main_recmd1_name.Text = main_recmd1_name2.Text = item.Name;
				main_recmd1_link.HRef = main_recmd1_link.HRef.Replace("{childMasterId}", item.ChildMasterId);

			}
			*/

            var actionResult = action.GetChildren(null, null, month, day, null, 20, null, null, 1, 3, "new", -1, -1);
			if(actionResult.success) {
				Random rnd = new Random();
				var items = (List<ChildAction.ChildItem>)actionResult.data;

                if(items.Count < 1) {
                    actionResult = action.GetChildren(null, null, null, null, null, 20, null, null, 1, 10, "waiting", -1, -1);
                    if(actionResult.success) {
                        items = (List<ChildAction.ChildItem>)actionResult.data;
                        ph_recmd2.Visible = true;
                    }
                } else {
                    ph_recmd1.Visible = true;
                }

                if(items.Count > 0) {
					var index = rnd.Next(items.Count);

					var item = items[index];

					main_recmd1_pic.Style["background"] = "url('" + item.Pic + "') no-repeat";
					main_recmd1_country.Text = main_recmd1_country2.Text = item.CountryName;
					main_recmd1_age.Text = main_recmd1_age2.Text = item.Age.ToString();
					main_recmd1_gender.Text = item.Gender;
					main_recmd1_waitingdays.Text = item.WaitingDays.ToString();
					main_recmd1_name.Text = main_recmd1_name2.Text = item.Name;
					main_recmd1_link.HRef = main_recmd1_link.HRef.Replace("{childMasterId}", item.ChildMasterId);
				} else {
					ph_child_birthday.Visible = false;
				}
			} else {
				ph_child_birthday.Visible = false;
			}

		}
		#endregion

	}

	//캠페인 
	void GetEvent()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_mainpage_list_f(10, "main_event_mobile");
            Object[] op1 = new Object[] { "count", "position" };
            Object[] op2 = new Object[] { 10, "main_event_mobile" };
            var list = www6.selectSP("sp_mainpage_list_f", op1, op2).DataTableToList<sp_mainpage_list_fResult>();

            repeater_event.DataSource = list;
            repeater_event.DataBind();
        }
	}

	void GetSympathy()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_sympathy_list_latest_f();
            Object[] op1 = new Object[] { };
            Object[] op2 = new Object[] { };
            var list = www6.selectSP("sp_sympathy_list_latest_f", op1, op2).DataTableToList<sp_sympathy_list_latest_fResult>();

            repeater_sympathy.DataSource = list;
            repeater_sympathy.DataBind();
        }
	}
}