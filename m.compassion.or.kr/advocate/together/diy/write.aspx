﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="write.aspx.cs" Inherits="advocate_together_diy_write" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/advocate/together/diy/write.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body" >
	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="advocate-diy">
    	
			<div class="bg-gray sectionsub-margin1">후원자님만의 방법으로<br />
				사랑을 나눌 계획을 알려 주세요. 
			</div>
        
			<fieldset class="frm-input">
				<legend>배송정보 입력</legend>
				<div class="row-table2">
					<div class="col-th" style="width:25%">이름</div>
					<div class="col-td" style="width:75%"><input type="text" id="ds_name" runat="server" style="width:100%" readonly="readonly"/></div>
				</div>
				<div class="row-table2">
					<div class="col-th" style="width:25%">휴대폰</div>
					<div class="col-td" style="width:75%"><input type="text" id="ds_phone" runat="server" style="width:100%" /></div>
				</div>
				<div class="row-table2" style="padding-bottom:20px">
					<div class="row">
                		<p class="txt-field">사연 및 실천 방안 소개</p>
						<textarea id="ds_content" maxlength="1000"  style="width:100%;height:180px"></textarea>
					</div>
				</div>
			</fieldset>
        
			<p class="txt-sub align-center mt20">후원자님께서 작성해 주신 내용을 바탕으로 담당자의
        		검토가 이루어질 예정이며, 프로그램 진행에 문제가 없을 경우
				유선으로 자세한 후원 진행 방법을 안내해 드리겠습니다.
			</p>
        
			<div class="wrap-bt"><a class="bt-type6" style="width:100%" ng-click="request($event)">신청하기</a></div>
        
			<div class="align-center">
        		<p class="txt-error" style="display:none;">필수 정보를 입력해 주세요.</p>
			</div>
        
		</div>
    
	<!--//-->
	</div>

</asp:Content>
