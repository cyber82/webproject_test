﻿(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, popup, paramService) {

        $scope.requesting = false;
        $scope.total = -1;
        $scope.isSearch = false;
        $scope.rowsPerPage = 9;

        $scope.list = [];
        $scope.nationList = [];
        $scope.params = {
            page: 1,
            rowsPerPage: $scope.rowsPerPage,
            k_word: ""
        };

        // 파라미터 초기화
        $scope.params = $.extend($scope.params, paramService.getParameterValues());
        if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);

        // 검색
        $scope.search = function () {
            $scope.params.page = 1;
            $scope.params.k_word = $("#k_word").val();

            $scope.getList();
        }

        // list
        $scope.getList = function (params) {

            $http.get("/api/advocate.ashx?t=diy_list", { params: $scope.params }).success(function (result) {
                $scope.list = result.data;
                $scope.total = result.data.length > 0 ? result.data[0].total : 0;

                $scope.isSearch = false;
                if ($scope.params.k_word != "") {
                	$scope.isSearch = true;
                }

            });

            if (params)
                scrollTo($("#l"), 10);
        }

    	// 상세페이지
        $scope.goView = function (id, $event) {
        	$http.post("/api/advocate.ashx?t=hits&type=diy&id=" + id).then().finally(function () {
        		location.href = "/advocate/together/diy/view/" + id + "?" + $.param($scope.params);
        	});
        	$event.preventDefault();
        }

        $scope.goWrite = function ($event) {
        	if (common.checkLogin()) {
        		location.href = "/advocate/together/diy/write/";
        	}

        	$event.preventDefault();
        }

        $scope.getList();


        $scope.parseDate = function (datetime) {
        	return new Date(datetime);
        }


    });

})();