﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_together_diy_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/advocate/together/diy/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
<!---->
    
    <div class="advocate-diy">
    	
        <div class="top-intro sectionsub-margin1">
        	<strong>Donate It Your way! </strong>
            <p>DIY 캠페인은 후원자님께서 직접 후원 프로그램을 만들고,<br />
                함께 참여할 분들을 모집하는 활동입니다.<br />
                후원자님만 할 수 있는 특별한 나눔을 실천해 보세요!
            </p>
            <a class="bt-type1" ng-click="goWrite($event)">프로그램 사연 신청</a>
        </div>
        
        <fieldset class="frm-search">
            <legend>상품검색입력</legend>
            <input type="text" id="k_word" ng-enter="search()" name="k_word"  placeholder="검색어를 입력해 주세요" />
            <span class="search" ng-click="search()">검색</span>
        </fieldset>
        
        <!--썸네일 리스트-->
        <div class="wrap-thumbil">
            
            <!--결과없음-->
            <div class="no-result" ng-if="total == 0 && isSearch">
            	<strong class="txt-title">검색 결과가 없습니다.</strong>
                <p class="txt-msg">다른 검색어를 선택해주세요.</p>
            </div>
            <!--//결과없음-->
            
			<!-- 게시글 없을때 -->
			<div class="no-content" ng-if="total == 0 && !isSearch">
				<strong class="txt-title">등록된 글이 없습니다.</strong>
			</div>
			<!--//  -->
            
            <ul class="list-thumbil width100">
                <li ng-repeat="item in list">
					<a href="#" ng-click="goView(item.db_id , $event)" class="box-block">
						<span class="photo"><img ng-src="{{item.db_thumb}}" alt="" /></span>
						<span class="advocate-diyinfo">
							<span class="txt-title">{{item.db_title}}</span>
							<span class="txt-supporter">{{item.db_name}} 후원자님</span>
							<span class="txt-date">
                        		{{parseDate(item.db_regdate) | date : 'yyyy.MM.dd' }}<span class="bar"></span><span class="txt-viewcnt">{{item.db_hits}}</span>
							</span>
						</span>
					</a>
                </li>
              
            </ul>
        </div>
        
        <paging ng-if="total != 0" class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>  

        
        <div class="box-tel">
        	<p class="txt-title">문의</p>
            <a class="tel" href="tel:02-3668-3450"><strong>전화</strong> 02)3668-3450</a>
            <a class="email" href="mailto:ebaek@compassion.or.kr"><strong>이메일</strong> ebaek@compassion.or.kr</a>
        </div>
        
        
    </div>
    
<!--//-->
</div>
</asp:Content>
