﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="advocate_together_diy_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    
    <script type="text/javascript" src="/advocate/together/diy/view.js"></script>
    <script type="text/javascript" >
        $(function () {
        	$("#reply").keyup(function () { $("#count").text($(this).val().length); })


        });

       
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
        <input type="hidden" id="id" runat="server" /> 

		<div class="advocate-diy">
    	
			<div class="wrap-board">

        		<div class="view-board">
            		<div class="txt-title sectionsub-margin1">
						<asp:Literal runat="server" ID="db_title" />
					</div>
					<div class="sympathy-info">
						<p class="txt2"><asp:Literal runat="server" ID="db_name" /> 후원자님</p>
					</div>
                
					<div class="txt-date">
                		<asp:Literal runat="server" ID="db_regdate" /><span class="bar">|</span><span class="viewcount">조회수</span><asp:Literal runat="server" ID="db_hits" />
					</div>
					<div class="editor-html" style="color:#767676">
                		저는 이번에 이러이러한 모금을 통해 루하마와 비슷한 아이들을 돕고자 합니다.<br />
						후원자님들의 힘을 모아 주세요. 
					</div>

                    <div class="share-sns">
                        <a href="#" ng-click="sns.show($event)">
                            <img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
                    </div>
                
					<div class="wrap-reply">
                		<div class="box-gray">
                    		<p class="txt-count">댓글 <em>{{total}}</em></p>
							<span class="row">
                        		<input type="text"  id="reply" name="content" ng-model="content" ng-bind="content" placeholder="댓글을 입력해주세요." /><a class="bt-reg" ng-click="add()">등록</a>
							</span>
							<p class="txt-byte"><span id="count">0</span>/300자</p>
						</div>
						<ul class="list-reply">
							<li ng-repeat="item in list">
                    			<div>
									<p class="txt-id">{{item.c_user_id}}</p>
									<p class="txt-reply">{{item.c_content}}</p>
									<time class="txt-time">{{parseDate(item.c_regdate) | date : 'yyyy-MM-dd HH:mm:ss' }}</time>
                    
									<span class="action" ng-show="item.is_owner">
										<span class="modify" ng-click="toggleEvent(item.c_id)">수정</span>
										<span class="delete" ng-click="remove(item.c_id)">삭제</span>
									</span>
								</div>

								<div class="modifyArea" data-idx="{{item.c_id}}" ng-if="item.is_owner" style="display:none;" >
									<textarea class="textarea_type1" style="width:100%;height:70px">{{item.c_content}}</textarea>
									<span class="wrap-bt">
                						<a class="bt-type8"  ng-click="update(item.c_id)">수정</a>
									</span>
								</div>
							</li>
						</ul>
                    
						<span class="more-loading"  ng-show="total > list.length" ng-click="showMore($event)"><span>더보기</span></span>
					</div>
                
				</div>
            
			</div>
        
			<div class="wrap-bt"><a style="width:100%" class="bt-type5" runat="server" id="btnList">목록</a></div>
        
		</div>
    
	<!--//-->
	</div>

    
</asp:Content>