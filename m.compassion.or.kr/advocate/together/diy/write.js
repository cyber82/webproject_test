﻿(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, popup, paramService) {


    	$scope.request = function ($event) {

    		if ($("#ds_name").val() == "" || $("#ds_phone").val() == "" || $("#ds_content").val() == "") {
            	$(".txt-error").show();
                return;
            } else {
            	$(".txt-error").hide();
            }

            if ($scope.requesting) return;
            $scope.requesting = true;

            var param = {
                ds_name: $("#ds_name").val(),
                ds_phone: $("#ds_phone").val(),
                ds_content: $("#ds_content").val()
            }

            $http.post("/api/advocate.ashx?t=add_story", param).success(function (r) {
                $scope.requesting = false;
                if (r.success) {
                	alert("신청이 완료되었습니다.\n후원자님의 귀한 섬김에 언제나 감사드립니다.");
					location.href="/advocate/together/diy/"
                } else {
                    alert(r.message);
                }

            });
        }



    });

})();