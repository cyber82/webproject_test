﻿function jusoCallback(zipNo, addr1, addr2) {
	$(".zipcode").val(zipNo);
	$(".addr1").val(addr1);
	$(".addr2").val(addr2);
}

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		// 상세페이지
		$scope.goWrite = function ($event) {
			$event.preventDefault();
		
			if (common.checkLogin()) {
				if ($("#locationType").val() != "국내") {
					alert("국내거주 회원만 신청가능합니다.");
					return;
				}
				location.href = "/advocate/together/blueboard/write/";
			}
		}

	});

})();