﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="write.aspx.cs" Inherits="advocate_together_diy_write" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false"  %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
    <script type="text/javascript" src="/advocate/together/blueboard/write.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    
    <input type="hidden" runat="server" id="locationType" />
	<input type="hidden" id="addr1" runat="server" />
	<input type="hidden" id="addr2" runat="server" />
	<input type="hidden" runat="server" id="hfAddressType" value="" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
    
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="advocate-blueboard">
    	
			<fieldset class="frm-input">
				<legend>배송정보 입력</legend>
				<div class="row-table2">
					<div class="col-th" style="width:25%">이름</div>
					<div class="col-td" style="width:75%"><input type="text" id="name" runat="server" maxlength="10" style="width:100%" /></div>
				</div>
				<div class="row-table2">
					<div class="col-th" style="width:25%">휴대폰</div>
					<div class="col-td" style="width:75%"><input type="text" id="phone" runat="server" maxlength="13" style="width:100%" /></div>
				</div>
				<div class="row-table2">
					<div class="col-th" style="width:25%">주소</div>
					<div class="col-td" style="width:75%">

                		<span class="row pos-relative2"><input type="text" runat="server" name="zipcode" id="zipcode" class="zipcode" placeholder="주소"  style="width:100%" readonly="readonly" />
						<a class="bt-type8 pos-btn" style="width:70px" runat="server" id="popup" ng-click="findAddr($event)">주소찾기</a></span>
						
						<span id="addr_road" class="row fs13 mt15"></span>
						<span id="addr_jibun" class="row fs13"></span>
						
					</div>
				</div>
				<div class="row-table2" style="padding-bottom:20px">
					<div class="row">
                		<p class="txt-field">활동계획</p>
						<textarea id="plan" maxlength="1000" style="width:100%;height:180px"></textarea>
					</div>
				</div>
			</fieldset>
        
			<p class="txt-sub align-center mt20">블루보드는 현장에서 바로 결연이 이루어질 수 있도록
				온라인에 공개되지 않은 어린이 3명의 정보가 함께 발송됩니다. 
				소중한 어린이의 정보가 담기는 자료인 만큼, 보다 신중하게
				신청해 주세요.
			</p>
        
			<div class="wrap-bt"><a class="bt-type6" style="width:100%" ng-click="request()">신청하기</a></div>
        
			<div class="align-center">
        		<p class="txt-error" style="display:none;">필수 정보를 입력해 주세요.</p>
			</div>
        
		</div>
    
	<!--//-->
	</div>

    
</asp:Content>