﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_together_blueboard_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/advocate/together/blueboard/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    

    <input type="hidden" id="id" runat="server"/>
    <input type="hidden" runat="server" id="locationType" />
	<input type="hidden" runat="server" id="hfAddressType" value="" />
	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="advocate-blueboard">
    	
			<div class="top-intro sectionsub-margin1">
        		<strong>소중한 사람들에게 컴패션을 알립니다</strong>
				<p>컴패션블루보드(Blue Board)란<br />
					컴패션 어린이 정보가 담긴 블루보드를 활용하여<br />
					후원자님이 속한 곳(회사, 사업장, 교회 등)에서 <br />
					소중한 사람들에게 자연스럽게 컴패션을 알리고 <br />
					소개하는 활동입니다
				</p>
			</div>
        
			<p class="txt-caption">활동 가이드</p>
			<div class="youtube">      
        		<iframe src="https://www.youtube.com/embed/RnLcOyj99vk?rel=0&amp;showinfo=0wmode=transparent" frameborder="0" allowfullscreen></iframe>
			</div>
        
			<p class="txt-caption2">컴패션블루보드 구성</p>
			<div class="box-gray">
        		<ul class="list-bu">
            		<li>결연 안내 보드</li>
					<li>컴패션 브로슈어</li>
					<li>결연서/반송봉투</li>
				</ul>
			</div>
        
			<p class="bu">현장에서 바로 결연이 이루어질 수 있도록, 온라인에 공개되지 않은 어린이 3명의 정보가 함께 발송됩니다.</p>
			<p class="bu">소중한 어린이의 정보가 담기는 자료인 만큼, 기도하는 마음으로 보다 신중하게 신청해 주세요.</p>
        
			<p class="txt-caption2">신청 및 발송</p>
			<p class="txt-sub">아래  [패킷 신청] 버튼을 통해 패킷 활용 계획을 알려 주시면, 담당자의 승인 절차를 거쳐 2일 내 발송됩니다.</p>
        
			<p class="txt-caption2">신청 조건</p>
			<p class="txt-sub">블루보드 패킷은 하루 최대 1회, 컴패션 애드보킷만 신청 가능합니다. 더 많은 어린이 정보가 필요하시거나 결연 행사를 계획하고 계신 후원자님은 DIY신청이나 담당자에게 직접 연락 바랍니다. </p>
        
			<div class="wrap-bt"><a class="bt-type6" style="width:100%" ng-click="goWrite($event)">패킷 신청</a></div>
        
			<div class="box-tel">
        		<p class="txt-title">문의</p>
				<a class="tel" href="tel:02-3668-3436"><strong>전화</strong> 02)3668-3436</a>
				<a class="fax" href="tel:02-3668-3501"><strong>팩스</strong> 02)3668-3501</a>
				<a class="email" href="mailto:sham@compassion.or.kr"><strong>이메일</strong> sham@compassion.or.kr</a>
			</div>
        
        
		</div>
    
	<!--//-->
	</div>
    
</asp:Content>