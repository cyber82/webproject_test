﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="finish.aspx.cs" Inherits="advocate_intro_finish" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/advocate/together/compassion-a-day/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	
<div class="wrap-sectionsub">
<!---->
    
    <div class="advocate-compassion">
    	
        <div class="msg-sendok sectionsub-margin1">
        	<p>메시지 발송 완료</p>
        </div>
        
        <p class="txt-caption3 align-left">CAD 이후</p>
        <ul class="list-bu">
        	<li>수신인은 메시지를 받은 후 일주일 동안 해당 메시지를 통해  결연 신청이 가능합니다.</li>
            <li>신중한 후원 추천을 위해 CAD 메시지의 유효 기간  (전송 후 일주일간) 내에는 메시지 추가 발송이 불가능합니다.</li>
            <li>보다 많은 어린이가 후원자님을 만나 사랑으로 양육될 수 있도록 기도로 함께해 주세요!</li>
        </ul>
        
       <div class="linebar" ></div>
       
        <div class="align-center">
            <p class="txt-sub">추천 후에는 내가 추천한 어린이의 결연 현황을<br />
                마이컴패션에서 확인하실 수 있습니다.
            </p>
            <a class="bt-type8" href="/my/activity/cad/">CAD 추천 현황 보기</a>
        </div>
        
    </div>
    
<!--//-->
</div>

	
	    
</asp:Content>