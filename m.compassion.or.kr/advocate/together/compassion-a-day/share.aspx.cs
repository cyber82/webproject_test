﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class advocate_together_compasion_a_day_share : MobileFrontBasePage {
	
	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var c_id = Request["c"].EmptyIfNull();
		childMasterId.Value = Request["child"].EmptyIfNull();

       

		if(string.IsNullOrEmpty(c_id) || string.IsNullOrEmpty(childMasterId.Value)) {
			Response.Redirect("/");
		}


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.tCAD.FirstOrDefault(p => p.c_id == c_id);
            var entity = www6.selectQF<tCAD>("c_id", c_id);

            if (entity == null)
            {
                Response.Redirect("/");
            }

            using (AuthDataContext data = new AuthDataContext())
            {
                var senderName = data.tSponsorMaster.First(p => p.SponsorID == entity.sponsorId);

                sender_name.Text = senderName.SponsorName;
            }

            // 메세지 발송 후 CAD 완료를 하지 않으면 디비에 데이타가 없다. 
            if (entity != null)
            {
                if (childMasterId.Value != entity.globalId)
                {
                    Response.Redirect("/");
                }

                ph_private1.Visible = ph_private2.Visible = true;
                msg.Text = entity.msg.ToHtml();

                var remainDate = Convert.ToInt32(entity.regdate.AddDays(7).Subtract(Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"))).TotalDays);
                if (remainDate < 0)
                    remain_day.Text = "기간종료";
                else
                    remain_day.Text = string.Format("D-{0}", remainDate);


            }



        }

	}
}