﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="share.aspx.cs" Inherits="advocate_together_compasion_a_day_share" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/advocate/together/compassion-a-day/share.js"></script>
    <script type="text/javascript">
        function receiveLetter(x) {
            $('#animationSandbox').addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                $(this).removeClass(x + ' animated');
            });
        };

        $(document).ready(function () {
            setInterval(function () {
                receiveLetter("ani_bounce");
            }, 2000)

        });
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
<input type="hidden" runat="server" id="childMasterId" />

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

        <div class="advocate-compassion" id="receive_letter">

            <p class="msg-cadlanding">
                <strong><span><asp:Literal runat="server" ID="sender_name"/></span>님으로부터<br />
                    편지가 도착했습니다~!</strong>
                이 봉투 안에는 오직 한 사람에게만<br />
                전달되는 소중한 편지가 담겨 있습니다.
            </p>

            <div id="animationSandbox" class="msg-cadlanding-letter"><a ng-click="getLetter()">
                <img src="/common/img/page/advocate/card.png" alt="" width="328" /></a></div>
            <button class="triggerAnimation">click</button>
        </div>
    


    
    <div class="advocate-compassion" id="send_letter" style="display:none">
        <asp:PlaceHolder runat="server" ID="ph_private1" Visible="false">
            
        <!-- 편지지 -->
        <div class="preview-cad sectionsub-margin1" style="margin-top:-20px !important">
        <asp:Literal runat="server" ID="msg"/>
            <!-- 고정 영역 -->
            <br /><br />
            <div class="line1"></div>
            <div class="line2"></div>
            <!--// 고정 영역 -->
        </div>
        <!--// 편지지 -->
	</asp:PlaceHolder>

        <div class="box-gray">
            <p class="txt-title">후원자님을 기다리고 있는 컴패션어린이입니다</p>
            <div class="thumbil-child">
                <div class="pos-photo"><span class="photo" style="background:url('{{item.pic}}') no-repeat"ㄴ></span><em class="txt-day">{{item.waitingdays}}일</em></div>
                <div class="info">
                    <p class="txt-name">{{item.namekr}}</p>
                    <p><span class="color1">국가 : </span>{{item.countryname}}</p>
                    <p><span class="color1">생일 : </span>{{item.birthdate | date:'yyyy-MM-dd'}} ({{item.age}}세)</p>
                    <p><span class="color1">성별 : </span>{{item.gender}}</p>
                </div>
            </div>

            
            <div class="introduce-child">
                <p class="txt1">안녕하세요? <br />
                제 이름은 <strong>{{item.namekr}}</strong>예요.</p>
                
                제 나이는 <strong>{{item.age}}살</strong>이고요, 생일은 <strong>{{item.birthdate | date:'yyyy년 MM월 dd일'}}</strong>이에요.
                저는 <strong>{{item.countryname}}</strong>에 살고 있어요. <br />
                가족은 <strong>{{item.family}}</strong>이고요, 제가 좋아하는 것은 <strong>{{item.hobby}}</strong>예요.<br /><br />
                
                후원자님과 함께 컴패션에서 꿈을 키우고 싶어요.
                저와 함께해 주시겠어요?
            </div>
        </div>
        
        <div class="wrap-bt"><a class="bt-type6" style="width:100%" ng-click="goPay($event);">결연하기</a></div>
        
        <div class="linebar"></div>
        <asp:PlaceHolder runat="server" ID="ph_private2" Visible="false">
            <div class="guide-reginfo">
                <p class="txt-title">후원 신청 안내<em><asp:Literal runat="server" ID="remain_day" /></em></p>
                이 어린이는 일주일간 후원자님을 통해서만 후원 신청이 
            가능합니다. 보다 많은 어린이가 사랑으로 양육될 수 있도록 
            어린이 정보 공개 기간에 제한을 두고 있는 점 양해 
            부탁드립니다.
            </div>
        </asp:PlaceHolder>
        <p class="txt-caption3 align-left">한국컴패션이 더 궁금하신 분들은 아래 링크를 통해 더 많은 이야기를 만나 보세요</p>
        
        <div class="wrap-bt mt0 mb20">
        	<a class="bt-type7 fl" style="width:49%" href="<%: ConfigurationManager.AppSettings["domain_file"] %>/10million/shoeshine_01.html">후원자 스토리 보기</a>
            <a class="bt-type6 fr" style="width:49%" href="<%: ConfigurationManager.AppSettings["domain_file"] %>/10million/miriam.html">어린이 스토리 보기</a>
        </div>
    
    </div>
    
    
<!--//-->
</div>	
	    
</asp:Content>