﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="share.aspx.cs" Inherits="popup_share"  %>

    
<div style="background:transparent; width:100%;" >
    <script type="text/javascript" src="/common/js/site/common.js"></script>
	 <script>
	 	$(function () {
	 		common.bindSNSAction();
	 	})
	 </script>
	<!--레이어 안내 팝업-->
	<div class="wrap-layerpop" style="width:80%;left:10%;">
		<div class="header">
        	<p class="txt-title">공유하실 채널을 선택해 주세요</p>
		</div>
		<div class="contents">
			<div class="advocate-compassion">
            	<div class="share-channel">
					
                	<a data-role="sns" data-provider="kt" data-url="" data-title="Compassion A Day-{{item.namekr}}" data-picture="{{item.pic}}" data-desc="" class="pop_share"><img src="/common/img/icon/kakao.png" alt="카카오톡" /></a>
					<a data-role="sns" data-provider="line" data-url="" data-title="Compassion A Day-{{item.namekr}}" data-picture="{{item.pic}}" data-desc="" class="pop_share"><img src="/common/img/icon/line.png" alt="라인" /></a>
					<a data-role="sns" data-provider="sms" data-url="" data-title="Compassion A Day-{{item.namekr}}" data-picture="{{item.pic}}" data-desc="" class="pop_share"><img src="/common/img/icon/sms.png" alt="문자메시지" /></a>
					<div class="linebar"></div>
				</div>
                	
				메시지 발송 후,<br />
				아래 <span class="color2">[메시지 발송 완료]</span> 버튼을 눌러 주세요.
                
				<div class="wrap-bt"><a href="#" ng-click="pop.complete($event)" class="bt-type6">메시지 발송 완료</a></div>
                
				<div class="box2-gray">
                	메시지 발송 완료 여부를 체크해 주셔야  CAD 서비스가 완료됩니다. <br /><br />
					후원자님만의 메시지에 소중한 어린이의 정보가 담길 수 있도록 꼭! 완료 버튼을  눌러주세요.  
				</div>
                
			</div>
		</div>
		<div class="close"><span class="ic-close" ng-click="pop.hide($event)">레이어 닫기</span></div>
	</div>
	<!--//레이어 안내 팝업-->
</div>