﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="preview.aspx.cs" Inherits="popup_preview"  %>

<div style="background:transparent;" class="fn_pop_container" id="childModalview">

	<div class="wrap-layerpop fn_pop_content" style="width:100%;left:0;">
		<div class="header2" >
        	<p class="txt-title">CAD 미리보기</p>
		</div>
		<div class="contents" >
			<div class="advocate-compassion">
            
				<div class="preview-cad sectionsub-margin1" style="margin-top:-40px !important;">
					<div style="white-space: pre-wrap;" ng-bind="txt_msg">
					민주에게<br /><br />
    
					사실 나도 몇 년 전부터 한국컴패션과 함께 가난 가운데 있는 어린이들을 후원하고 있어. 후원자로서 컴패션을 알아가면서 이 일은 꼭 해야 한다는 생각이 들었어. 한국도 60년 전 누군가의 
					도움의 손길이 없었다면 지금처럼 도움을 주는 나라가 되는 기적같은 일이 있었을까 생각해 보곤해..<br /><br />
                    
					내가 전달하는 이 봉투에는 후원자를 기다리는 한 어린이가 들어 있어. 내가 기도하면서 네가 꼭 손을 잡아 주었으면 해서 선택한 어린이야.<br /><br />
                    
					너도 기도하는 마음으로 함께해 주었으면 좋겠다.<br />
					오늘 하루도 파이팅!
					</div>
					<div class="line1"></div>
                    <div class="line2"></div>
				</div>
                
				<div class="box-gray mt20">
        			<p class="txt-title">후원자님을 기다리고 있는 컴패션어린이입니다</p>
					<div class="thumbil-child">
            			<div class="pos-photo"><span class="photo" style="background: url('{{item.pic}}') no-repeat"></span><em class="txt-day">{{item.waitingdays}}일</em></div>
						<div class="info">
                			<p class="txt-name">{{item.namekr}}</p>
							<p><span class="color1">국가 : </span>{{item.countryname}}</p>
							<p><span class="color1">생일 : </span> {{item.birthdate | date:'yyyy-MM-dd'}} ({{item.age}}세)</p>
							<p><span class="color1">성별 : </span>{{item.gender}}</p>
						</div>
					</div>
            
					<div class="introduce-child">
						<p class="txt1">안녕하세요? <br />
						제 이름은 <strong>{{item.namekr}}</strong>예요.</p>
                
						제 나이는 <strong>{{item.age}}살</strong>이고요, 생일은 <strong>{{item.birthdate | date:'yyyy년 MM월 dd일'}}</strong>이에요.
						저는 <strong>{{item.countryname}}</strong>에 살고 있어요. <br />
						<span ng-if="item.family">가족은 <strong>{{item.family}}</strong>이고요,</span> <span ng-if="item.hobby">제가 좋아하는 것은 <strong>{{item.hobby}}</strong>예요.</span><br />

					</div>
				</div>
                
				<p class="txt-sub align-center">후원자님과 함께 컴패션에서 꿈을 키우고 싶어요.<br />
					저와 함께해 주시겠어요?
				</p>
				<div class="wrap-bt"><a class="bt-type4" style="width:100%" >결연하기</a></div>
                
				<div class="linebar"></div>
                
				<div class="guide-reginfo">
					<p class="txt-title">후원 신청 안내<em>D-7</em></p>
					이 어린이는 일주일간 후원자님을 통해서만 후원 신청이 
					가능합니다. 보다 많은 어린이가 사랑으로 양육될 수 있도록 
					어린이 정보 공개 기간에 제한을 두고 있는 점 양해 
					부탁드립니다.
				</div>
				<div class="wrap-bt"><a class="bt-type6" style="width:100%" ng-click="pop.hide($event)">확인</a></div>
            
			</div>
            
		</div>
		<div class="close2" ng-click="pop.hide($event)"><span class="ic-close">레이어 닫기</span></div>
	</div>
</div>