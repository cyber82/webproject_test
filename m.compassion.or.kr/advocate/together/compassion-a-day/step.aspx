﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="step.aspx.cs" Inherits="advocate_day_step" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/advocate/together/compassion-a-day/step.js?v=1.2"></script>
    <script type="text/javascript">
        $(function () {

        });
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <div class="wrap-tab3 sectionsub-margin1">
            <a style="width: 50%" href="/advocate/together/compassion-a-day/">소개</a>
            <a style="width: 50%" class="selected">참여</a>
        </div>

        <div class="advocate-compassion" ng-show="cur_step == 1">

            <div class="flow-step sectionsub-margin1">
                <span class="selected">STEP 01</span>
                <span>STEP 02</span>
                <span>STEP 03</span>
            </div>

            <div class="step-select ic-bg1">
                <p class="txt-quiz">
                    추천하고자 하는<br />
                    <strong>어린이의 국가를 선택</strong>해 주세요
                </p>

                <ul class="list-select">
                    <li><span class="bt-type7 " ng-click="selectRegion($event , 'asia')">아시아</span></li>
                    <li><span class="bt-type7 " ng-click="selectRegion($event , 'america')">중남미</span></li>
                    <li><span class="bt-type7 " ng-click="selectRegion($event , 'africa')">아프리카</span></li>
                    <li><span class="bt-type7 " ng-click="selectRegion($event , '')">상관없음</span></li>
                </ul>
            </div>

        </div>

        <div class="advocate-compassion" ng-show="cur_step == 2">

            <div class="flow-step sectionsub-margin1">
                <span>STEP 01</span>
                <span class="selected">STEP 02</span>
                <span>STEP 03</span>
            </div>

            <div class="step-select ic-bg2">
                <p class="txt-quiz">
                    <strong>어린이의 성별</strong>을<br />
                    선택해 주세요
                </p>

                <ul class="list-select">
                    <li><span class="bt-type7" ng-click="selectGender($event , 'M')">남자아이</span></li>
                    <li><span class="bt-type7 " ng-click="selectGender($event , 'F')">여자아이</span></li>
                    <li><span class="bt-type7 " ng-click="selectGender($event , '')">상관없음</span></li>
                </ul>
            </div>

            <div class="box-gray">
                <p class="txt-title">안내</p>
                만약 후원자님이 선택하신 값에 매칭되는 어린이가 없을 경우, 오래 기다린 어린이가 자동 추천됩니다.
            </div>


        </div>

        <div class="advocate-compassion" ng-show="cur_step == 3">

            <div class="flow-step sectionsub-margin1">
                <span>STEP 01</span>
                <span>STEP 02</span>
                <span class="selected">STEP 03</span>
            </div>

            <div class="box-gray">
                <p class="txt-title">후원자님을 기다리고 있는 컴패션어린이입니다</p>
                <div class="thumbil-child">
                    <div class="pos-photo"><span class="photo" style="background: url('{{item.pic}}') no-repeat"></span><em class="txt-day">{{item.waitingdays}}일</em></div>
                    <div class="info">
                        <p class="txt-name">{{item.namekr}}</p>
                        <p><span class="color1">국가 : </span>{{item.countryname}}</p>
                        <p><span class="color1">생일 : </span>{{item.birthdate | date:'yyyy-MM-dd'}} ({{item.age}}세)</p>
                        <p><span class="color1">성별 : </span>{{item.gender}}</p>
                    </div>
                </div>

                <div class="introduce-child">
                    <p class="txt1">안녕하세요?<br />
                        제 이름은 <strong>{{item.namekr}}</strong>예요.</p>
                        제 나이는 <strong>{{item.age}}살</strong>이고요, 생일은 <strong>{{item.birthdate | date:'yyyy년 MM월 dd일'}}</strong>이에요.
	    				저는 <strong>{{item.countryname}}</strong>에 살고 있어요.<br />
                        <span ng-if="item.family">가족은 <strong>{{item.family}}</strong>이고요,</span> <span ng-if="item.hobby">제가 좋아하는 것은 <strong>{{item.hobby}}</strong>예요.</span><br />

                    후원자님과 함께 컴패션에서 꿈을 키우고 싶어요.
					저와 함께해 주시겠어요?
                </div>
            </div>

            <fieldset class="message-child">
                <legend>메시지 입력</legend>
                <p>소중한 어린이와 함께<br />
                    <strong>전달하고 싶은 메시지</strong>를 적어 주세요</p>
                <textarea id="txt_msg" ng-model="txt_msg" placeholder="메시지를 입력해 주세요" style="width: 100%; height: 220px"></textarea>
            </fieldset>
            <div class="wrap-bt">
                <a class="bt-type7 fl" style="width: 49%" ng-click="pop.show('preview', $event)">미리보기</a>
                <a class="bt-type6 fr" style="width: 49%" ng-click="pop.show('share', $event)">보내기</a>
            </div>


        </div>

        <!--//-->
    </div>

</asp:Content>
