﻿function jusoCallback(zipNo, addr1, addr2) {
	$(".zipcode").val(zipNo);
	$(".addr1").val(addr1);
	$(".addr2").val(addr2);
}

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		// 상세페이지
		$scope.goPage = function ($event) {
			$event.preventDefault();
		
			if (common.checkLogin()) {
				
				$http.get("/api/cad.ashx?t=can-request-check", { params: {}}).success(function (r) {
					if (r.success) {

						location.href = "/advocate/together/compassion-a-day/step/";

						//$("a.pop_share_hidden[data-provider='" + provider + "']").trigger("click");

						modal.show();

					} else {
						if (r.action == "mypage") {
							if (confirm(r.message)) {
								location.href = "/my/activity/cad/";
							}

						} else {
							alert(r.message);
						}
						
					}
				});

				
			}
		}

		

	});

})();