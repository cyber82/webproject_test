﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_intro_together" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/advocate/together/compassion-a-day/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="wrap-tab3 sectionsub-margin1">
			<a style="width:50%" class="selected">소개</a>
			<a style="width:50%" href="/advocate/together/compassion-a-day/step/">참여</a>
		</div>
		<div class="advocate-compassion">
    	
			<div class="top-intro sectionsub-margin1">
        		<strong>하루에 한 번, 어린이를 향한 사랑고백 </strong>
				<p>Compassion A Day란<br />
					하루에 한 번, 친구나 지인들에게 컴패션을 소개하고<br />
					도움이 필요한 어린이를 알리는 캠페인입니다.<br /><br />
					하루에 한 번 컴패션을 전해 주시면<br />
					한 어린이를 향한 놀라운 기적이 시작됩니다.
				</p>
			</div>
			<div class="youtube">   <!--추후 영상 제작 예정(url 샘플값 변동요망)-->
        		<iframe src="https://www.youtube.com/embed/fQjCrHEYV2E?rel=0&amp;showinfo=0wmode=transparent" frameborder="0" allowfullscreen></iframe>
			</div>
        
			<p class="txt-caption">어린이 추천 과정</p>
			<div class="box-gray">
        		<p class="txt-title"><strong>STEP 1</strong> 어린이 유형 선택</p>
				추천하고 싶은 어린이 유형을 선택하세요. 컴패션에서는 유형에 맞는 한 명의 어린이를 후원자님만의 편지에 담아 드립니다.
			</div>
			<div class="box-gray">
        		<p class="txt-title"><strong>STEP 2</strong> 추천 메시지 쓰기</p>
				후원자님의 진심 어린 메시지는 한 어린이가 밝은 내일을 맞이할 수 있는 힘이 됩니다. 신중한 마음으로 함께해주세요.
			</div>
			<div class="box-gray">
        		<p class="txt-title"><strong>STEP 3</strong> 어린이 패킷 발송 완료</p>
				수신인은 후원자님의 메시지와 함께 모바일에서 확인 가능한 어린이 정보를 받게 됩니다. 어린이의 양육 신청이 가능한 7일 동안 후원자님도 기도로 함께해주세요! 
			</div>
			<div class="bg-gray sectionsub-margin1">
        		<p class="txt-caption2">하루 한 명, 소중한 사람들에게<br />
					<strong>컴패션 어린이를 추천</strong>해 보세요
				</p>
				<p class="txt-sub">CAD 서비스는 애드보킷이나 어린이를 양육 중이신<br />
	        		후원자만 참여 가능합니다.
				</p>
				<a class="bt-type6" ng-click="goPage($event)">CAD 함께하기</a>
			</div>
        
			<div class="align-center">
				<p class="txt-sub">추천 후에는 내가 추천한 어린이의 결연 현황을<br />
					마이컴패션에서 확인하실 수 있습니다.
				</p>
				<a class="bt-type8" href="/my/activity/cad/">CAD 추천 현황 보기</a>
			</div>
        
		</div>
    
	<!--//-->
	</div>
	
	    
</asp:Content>