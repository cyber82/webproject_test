﻿
(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		$scope.item = "";
		$scope.cur_step = 1;
		$scope.region = "";
		$scope.txt_msg = "";

		$scope.params = {
			orderby: "new",
			page: 1,
			rowsPerPage: 10,
			country: "",
			minAge: "4",
			maxAge: "16",
			gender: "",		// 성별 Y,N
			today: "",		// 오늘이 생일인경우 Y,N
			mm: "",			// 특별한 기념일 월 mm
			dd: "",			// 특별한 기념일 일 dd
			orphan: "",
			specialNeed: ""
		},

		// 대륙선택
		$scope.selectRegion = function ($event, arg) {
			$scope.region = arg;
			$scope.cur_step = 2;
		};

		// 성별선택
		$scope.selectGender = function ($event, arg) {
			$scope.params.gender = arg;
			$scope.search();
			
		};

		$scope.search = function () {

			// 대륙으로 
		    $http.get("/api/country.ashx?t=list", { params: { region: $scope.region } }).success(function (r) {
			    if (r.success) {
                    console.log("나라")
					$scope.params.country = "";

					$.each(r.data, function (i) {
						$scope.params.country += this.c_id;
						if (r.data.length - 1 != i) {
							$scope.params.country += ",";
						}
					})

					getChild();

				} else {
					alert(r.message);
				}

			});

			// 어린이 기본정보
			var getChild = function () {

				console.info("params", $scope.params);
				$http.get("/api/tcpt.ashx?t=get-random-child", { params: $scope.params }).success(function (r) {
					console.log(r);
					if (r.success) {
						$scope.item = $.extend({} , r.data);
						$scope.item.birthdate = new Date($scope.item.birthdate);

						getCaseStudy(r.data.childmasterid , r.data.childkey);

						$scope.cur_step = 3;

					} else {
						alert(r.message);
					}
				});

			};

			// 어린이 부가정보
			var getCaseStudy = function (childmasterid , childkey) {

				$http.get("/api/tcpt.ashx?t=get-casestudy", { params: { childKey: childkey, childmasterid: childmasterid } }).success(function (r) {

					console.log(r);
					if (r.success) {

						if (r.data.family) {
							$scope.item.family = r.data.family;
						}
						if (r.data.hobby) {
							$scope.item.hobby = r.data.hobby;
						}
						if (r.data.health) {
							$scope.item.health = r.data.health;
						} else {
							$scope.item.health = "없음";
						}

					}

				})

			};

		};

		$scope.pop = {

			instance: null,
			show: function (target, $event) {
			    if ($event) $event.preventDefault();
			    if ($scope.txt_msg == "") {
			        alert("메세지를 입력해주세요");
			        $("#txt_msg").focus();
			        return false;
			    }
				if (target == "preview") {
					popup.init($scope, "/advocate/together/compassion-a-day/popup/" + target + "?v=3", function (modal) {
						$scope.pop.instance = modal;

						modal.show();

					}, { top: 0, iscroll: true, removeWhenClose : true});
				} else {

					if ($scope.txt_msg == "") {
						alert("메세지를 입력해주세요");
						$("#txt_msg").focus();
						return false;
					}

					popup.init($scope, "/advocate/together/compassion-a-day/popup/" + target + "?v=1", function (modal) {
						$scope.pop.instance = modal;

						var params = { globalId: $scope.item.childmasterid, childKey: $scope.item.childkey };
					
						$http.get("/api/cad.ashx?t=can-request", { params: params }).success(function (r) {
						
							if (r.success) {

								$scope.CampaignID = r.data;

								$(".pop_share").attr("data-desc", $scope.txt_msg.replace(/,/g, " "));
								$(".pop_share").attr("data-url", "http://" + location.host + "/advocate/together/compassion-a-day/share?c=" + $scope.CampaignID + "&child=" + $scope.item.childmasterid);

								//$("a.pop_share_hidden[data-provider='" + provider + "']").trigger("click");

								modal.show();

							} else {
								if (r.action == "mypage") {
									if (confirm(r.message)) {
										location.href = "/my/activity/cad/";
									}

								} else {
									alert(r.message);
								}
							}
						});

						

					});
				}

			},

			hide: function ($event) {
				$event.preventDefault();
				if (!$scope.pop.instance)
					return;
				$scope.pop.instance.hide();

			},

			// 공유완료
			complete: function ($event) {
				$event.preventDefault();

				loading.show("CAD 생성 중입니다. 서버 상태에 따라 1~3분 정도 소요됩니다. 잠시만 기다려주세요.");
				var params = { CampaignID: $scope.CampaignID, globalId: $scope.item.childmasterid, childKey: $scope.item.childkey, childName: $scope.item.name, msg: $scope.txt_msg };
				console.log(params);
				$http.post("/api/cad.ashx?t=request", params).success(function (r) {
				    loading.hide();
					console.log(r);
					if (r.success) {

						location.href = "/advocate/together/compassion-a-day/finish";

					} else {
						alert(r.message);
					}
				});

			}

		}

	
	});

})();