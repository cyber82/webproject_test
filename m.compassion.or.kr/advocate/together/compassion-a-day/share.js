﻿
(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		$scope.item = "";


		$scope.getChild = function () {

			$http.get("/api/tcpt.ashx?t=get", { params: { "childMasterId": $("#childMasterId").val() } }).success(function (r) {
				console.log(r);
				if (r.success) {
					$scope.item = $.extend({}, r.data);
					$scope.item.birthdate = new Date($scope.item.birthdate);

					getCaseStudy(r.data.childmasterid , r.data.childkey);

				} else {
					alert(r.message);
				}
			});

			// 어린이 부가정보
			var getCaseStudy = function (childmasterid , childkey) {

				$http.get("/api/tcpt.ashx?t=get-casestudy", { params: { childKey: childkey, childmasterid: childmasterid } }).success(function (r) {

					console.log(r);
					if (r.success) {

						if (r.data.family) {
							$scope.item.family = r.data.family;
						}
						if (r.data.hobby) {
							$scope.item.hobby = r.data.hobby;
						}
						if (r.data.health) {
							$scope.item.health = r.data.health;
						} else {
							$scope.item.health = "없음";
						}

					}

				})

			};

		};

		$scope.goPay = function ($event) {
			$event.preventDefault();

			$http.get("/sponsor/pay-gateway.ashx?t=go-cdsp", { params: { childMasterId: $("#childMasterId").val() } }).success(function (r) {

				if (r.success) {
					location.href = r.data;
				} else {
					alert(r.message);
				}
			});
		}

		$scope.getChild();

		$scope.getLetter = function () {
		    $("#receive_letter").hide();
		    $("#send_letter").show();
		}

	});

})();