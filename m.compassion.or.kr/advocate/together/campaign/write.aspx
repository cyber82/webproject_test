﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="write.aspx.cs" Inherits="advocate_together_diy_write"  culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
    <script type="text/javascript" src="/advocate/together/campaign/write.js"></script>
	<script>
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<div class="wrap-sectionsub"  ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
 
		<input type="hidden" id="id" runat="server"/>
		<input type="hidden" runat="server" id="locationType" />
		<input type="hidden" runat="server" id="hfAddressType" value="" />
		<input type="hidden" id="addr1" runat="server" />
		<input type="hidden" id="addr2" runat="server" />
		<input type="hidden" runat="server" id="dspAddrJibun" value="" />
		<input type="hidden" runat="server" id="dspAddrDoro" value="" />
    
		<div class="advocate-campaign">
    	
			<fieldset class="frm-input">
				<legend>배송정보 입력</legend>
				<div class="row-table2">
					<div class="col-th" style="width:25%">이름</div>
					<div class="col-td" style="width:75%"><input type="text" id="name" runat="server" maxlength="10" style="width:100%" /></div>
				</div>
				<div class="row-table2">
					<div class="col-th" style="width:25%">휴대폰</div>
					<div class="col-td" style="width:75%"><input type="text" id="phone" class="number_only" runat="server" maxlength="13" style="width:100%" /></div>
				</div>
				<div class="row-table2">
					<div class="col-th" style="width:25%">주소</div>
					<div class="col-td" style="width:75%">

                		<span class="row pos-relative2"><input type="text" runat="server" name="zipcode" id="zipcode" placeholder="주소" class="zipcode"  style="width:100%" readonly="readonly"/>
							<a class="bt-type8 pos-btn" style="width:70px" ng-click="findAddr($event)">주소찾기</a></span>
					
						
						<span id="addr_road" class="row fs13 mt15"></span>
						<span id="addr_jibun" class="row fs13"></span>

					</div>
				</div>
				<div class="row-table2" style="padding-bottom:20px">
					<div class="row">
                		<p class="txt-field">활동계획</p>
						<textarea id="plan" maxlength="1000" style="width:100%;height:180px"></textarea>
					</div>
				</div>
			</fieldset>
        
			<p class="txt-sub align-center mt20">해당 패킷은 캠페인당 1회만 신청 가능하며,<br />
				승인될 경우 입력하신 주소로 배송될 예정입니다.
			</p>
        
			<div class="wrap-bt"><a class="bt-type6" style="width:100%"  ng-click="request($event)" >신청하기</a></div>
        
			<div class="align-center">
        		<p class="txt-error" style="display:none;">필수 정보를 입력해 주세요.</p>
			</div>
        
		</div>
    
	<!--//-->
	</div>



</asp:Content>