﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="advocate_together_campaign_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/advocate/together/campaign/view.js"></script>
	<script>
		$(function () {
		
		})
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
 
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
		 <input type="hidden" id="id" runat="server"/>
		 <input type="hidden" id="is_end" runat="server"/>
    
		<div class="advocate-campaign">
    	
			<div class="wrap-board">

        		<div class="view-board">
            		<div class="txt-title sectionsub-margin1">
						<asp:Literal runat="server" ID="sc_title" />
					</div>
					<div class="sympathy-info">
						<p class="txt3">
                    		참여 기간 : <asp:Literal runat="server" ID="sc_begin" /> ~ <asp:Literal runat="server" ID="sc_end" /><br />
                            <asp:PlaceHolder runat="server" ID="lb_sc_announce" Visible="false">
							    당첨자 발표 : <asp:Literal runat="server" ID="sc_announce" /><br />
                            </asp:PlaceHolder>
							제공 아이템 : <asp:Literal runat="server" ID="sc_offer" />
						</p>
					</div>
                
					<div class="editor-html">
                		<asp:Literal runat="server" ID="sc_content" />
					</div>
                

                
					<div class="share-sns">
                        <a href="#" ng-click="sns.show($event)"><img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
                    </div>
                
				</div>
            
			</div>
        
			<div class="linebar"></div>
			<div class="wrap-bt">
				<a style="width:100%" class="bt-type6" ng-click="goWrite()" data-id="">신청</a>
				<a style="width:100%" class="bt-type5 mt10" runat="server" id="btnList">목록</a>
			</div>
        
		</div>
    
	<!--//-->
	</div>

</asp:Content>