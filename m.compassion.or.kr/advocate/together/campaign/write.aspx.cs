﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
using Microsoft.AspNet.FriendlyUrls;

public partial class advocate_together_diy_write : MobileFrontBasePage {

	const string listPath = "/advocate/together/campaign";

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];

		if (!UserInfo.IsLogin) {
			Response.ClearContent();
			return;
		}

		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect( listPath, true );
		}
		var isValid = new RequestValidator()
			.Add( "p", RequestValidator.Type.Numeric )
			.Validate( this.Context, listPath );

		if (!requests[0].CheckNumeric()) {
			Response.Redirect( listPath, true );
		}
		base.PrimaryKey = requests[0];
		id.Value = PrimaryKey.ToString();



        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<seasonal_campaign>("sc_id", Convert.ToInt32(PrimaryKey), "sc_display", 1);
            //if (!dao.seasonal_campaign.Any(p => p.sc_id == Convert.ToInt32(PrimaryKey) && p.sc_display))
            if (!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            //var entity = dao.seasonal_campaign.First(p => p.sc_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<seasonal_campaign>("sc_id", Convert.ToInt32(PrimaryKey));

            var is_end = DateTime.Compare(entity.sc_end, DateTime.Now).ToString();

            if (is_end != "1")
            {
                Response.Redirect(listPath, true);
            }

        }




		UserInfo sess = new UserInfo();
		// 이름
		name.Value = sess.UserName;
		

		locationType.Value = sess.LocationType;
		if (sess.LocationType == "국내") {

			var addr_result = new SponsorAction().GetAddress();
			if (!addr_result.success) {
				base.AlertWithJavascript(addr_result.message, "goBack()");
				return;
			}

			SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;
			hfAddressType.Value = addr_data.AddressType;
			zipcode.Value = addr_data.Zipcode;
			addr1.Value = addr_data.Addr1;
			addr2.Value = addr_data.Addr2;
			dspAddrDoro.Value = addr_data.DspAddrDoro;
			dspAddrJibun.Value = addr_data.DspAddrJibun;

			var comm_result = new SponsorAction().GetCommunications();
			if (!comm_result.success) {
				base.AlertWithJavascript(comm_result.message, "goBack()");
				return;
			}

			SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
			phone.Value = comm_data.Mobile.TranslatePhoneNumber();
		}
	}

}