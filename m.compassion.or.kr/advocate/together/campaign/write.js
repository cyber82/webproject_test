﻿
(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService, $address) {


		$scope.init = function () {
			$("#phone").mobileFormat();
		}

		$scope.request = function ($event) {
			
			if ($("#name").val() == "" || $("#phone").val() == "" || $("#zipcode").val() == "" || $("#plan").val() == "" ) {
				$(".txt-error").show();
				return;
			} else {
				$(".txt-error").hide();
			}
			if ($scope.requesting) return;
			$scope.requesting = true;


			var name = $("#name").val();
			var phone = $("#phone").val();
			var zipcode = $("#zipcode").val();
			var addr1 = $("#addr1").val();
			var addr2 = $("#addr2").val();
			var plan = $("#plan").val();
			var hfAddressType = $("#hfAddressType").val()

			var param = {
				id: $("#id").val(),
				name: name,
				phone: phone,
				zipcode: zipcode,
				addr1: addr1,
				addr2: addr2,
				plan: plan,
				hfAddressType: hfAddressType
			};

			$http.post("/api/advocate.ashx?t=campaign_apply", param).success(function (r) {
				$scope.requesting = false;
				if (r.success) {
					alert("신청이 완료되었습니다.\n후원자님의 귀한 섬김에 언제나 감사드립니다.");
				} else {
					alert(r.message);
				}
				
				location.href = "/advocate/together/campaign/"
			});
		}

		// 주소찾기
		// addressApiKey 제거
		$scope.findAddr = function ($event) {
			$event.preventDefault();

			$scope.scrollTop = $(document).scrollTop();
			$(".wrap-sectionsub").hide();

			popup.init($scope, "/common/popup/address", function (modal) {
				initAddress($scope, $address, modal, function (zipcode, addr1, addr2, jibun) {	// callback

					$("#addr1").val(addr1 + "//" + jibun);
					$("#addr2").val(addr2);

					$("#zipcode").val(zipcode);

					$("#addr_road").text("[도로명주소] " + addr1 + " " + addr2);
					$("#addr_jibun").text("[지번주소] " + jibun + " " + addr2);
				}, function () {
					$(".wrap-sectionsub").show();
					window.scroll(0, $scope.scrollTop)
				});

			}, { top: 0, iscroll: true, removeWhenClose: true });

		}

		$scope.init();
	});

})();


$(function () {

	$("#addr_domestic_1").val($("#zipcode").val());
	
	// 컴파스의 데이타를 불러오는경우 
	if ($("#dspAddrDoro").val() != "") {
		$("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
		if ($("#dspAddrJibun").val() != "") {
			$("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
		}

	} else if ($("#addr1").val() != "") {
		addr_array = $("#addr1").val().split("//");
		$("#addr_road").text("[도로명주소] " + addr_array[0] + " " + $("#addr2").val());
		if (addr_array[1]) {
			$("#addr_jibun").text("[지번주소] " + addr_array[1] + " " + $("#addr2").val());
		}
	}

})