﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_together_campaign_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/advocate/together/campaign/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div class="advocate-campaign">
    	
			<div class="top-intro sectionsub-margin1">
        		<strong>다양한 캠페인을 만나보세요</strong>
				<p>컴패션 애드보킷 캠페인이란,<br />
					시즌별 컨셉에 맞는 홍보 활동 및 결연 행사를 진행하는<br />
					캠페인입니다. 캠페인별로 제공되는 다양한 프로그램과<br />
					패킷을 활용하여 나눔의 기쁨을 느껴 보세요!
				</p>
			</div>
        
			<p class="txt-caption">진행 중인 캠페인</p>
        
			<!--썸네일 리스트-->
			<div class="wrap-thumbil">
				<ul class="list-thumbil width100">
					<li ng-repeat="item in list">
						<a ng-click="goView(item.sc_id)" class="box-block">
							<span class="photo"><img ng-src="{{item.sc_thumb}}" alt="" />
								<span class="period1" ng-if="!item.is_end">진행중</span>
								<span class="period2" ng-if="item.is_end">마감</span></span>
							<span class="advocate-campaigninfo">
								<span class="txt-title">{{item.sc_title}}</span>
								<span class="txt-etc">참여 기간 : {{parseDate(item.sc_begin) | date:'yyyy년 MM월 dd일'}} ~ {{parseDate(item.sc_end) | date:'yyyy년 MM월 dd일'}}<br />
									당첨자 발표 : {{parseDate(item.sc_announce) | date:'yyyy년 MM월 dd일'}}<br />
									제공 아이템 : {{item.sc_offer}}
								</span>
							</span>
						</a>
					</li>
					
					<!-- 게시글 없을때 -->
					<li class="no-content" ng-if="total == 0">등록된 글이 없습니다.</li>
					<!--//  -->

				</ul>
			</div>
        
			<paging ng-if="total != 0" class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>  

        
		</div>
    
	<!--//-->
	</div>

</asp:Content>
