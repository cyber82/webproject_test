﻿(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		$scope.total = -1;
		$scope.rowsPerPage = 3;

		$scope.list = [];
		$scope.params = {
			page: 1,
			rowsPerPage: $scope.rowsPerPage
		};


		// list
		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);
			$http.get("/api/advocate.ashx?t=campaign_list", { params: $scope.params }).success(function (result) {
				console.log(result.data);

				$.each(result.data, function () {
					this.is_end = new Date(this.sc_end) < new Date();
				})

				$scope.list = result.data;
				$scope.total = result.data.length > 0 ? result.data[0].total : 0;

			});
		}

		// 상세페이지
		$scope.goView = function (id) {
			$http.post("/api/advocate.ashx?t=hits&type=campaign&id=" + id).then().finally(function () {
				location.href = "/advocate/together/campaign/view/" + id + "?" + $.param($scope.params);
			});
		}

		$scope.parseDate = function (datetime) {
			return new Date(datetime);
		}


		$scope.getList();




	});

})();