﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class advocate_together_campaign_view : MobileFrontBasePage {

	const string listPath = "/advocate/together/campaign";


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();

		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect(listPath, true);
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}
		base.PrimaryKey = requests[0];
		id.Value = PrimaryKey.ToString();

		

	}


	protected override void loadComplete(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<seasonal_campaign>("sc_id", Convert.ToInt32(PrimaryKey), "sc_display", 1);
            //if (!dao.seasonal_campaign.Any(p => p.sc_id == Convert.ToInt32(PrimaryKey) && p.sc_display))
            if (!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            //var entity = dao.seasonal_campaign.First(p => p.sc_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<seasonal_campaign>("sc_id", Convert.ToInt32(PrimaryKey));

            sc_title.Text = entity.sc_title;
            sc_content.Text = entity.sc_content.WithFileServerHost();
            sc_begin.Text = entity.sc_begin.ToString("yyyy년 MM월 dd일");
            sc_end.Text = entity.sc_end.ToString("yyyy년 MM월 dd일");

            if (entity.sc_announce != null)
            {
                sc_announce.Text = ((DateTime)entity.sc_announce).ToString("yyyy년 MM월 dd일");
                lb_sc_announce.Visible = true;
            }

            sc_offer.Text = entity.sc_offer;

            is_end.Value = DateTime.Compare(entity.sc_end, DateTime.Now).ToString();

            // SNS
            this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
            this.ViewState["meta_title"] = string.Format("[한국컴패션-컴패션 애드보킷 캠패인] {0}", entity.sc_title);

            // optional(설정 안할 경우 주석)
            //this.ViewState["meta_description"] = "";
            //this.ViewState["meta_keyword"] = entity.sc_meta_keyword.EmptyIfNull();
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,MobileFrontBasePage)
            this.ViewState["meta_image"] = entity.sc_thumb.WithFileServerHost();

        }
    } 


}