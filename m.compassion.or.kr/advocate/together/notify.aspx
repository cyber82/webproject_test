﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="notify.aspx.cs" Inherits="advocate_intro_notify" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/advocate/together/notify.js"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />
	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
		<div class="wrap-tab3 sectionsub-margin1">
			<a style="width:50%" class="teb_menu selected" data-id="introduction" >소개</a>
			<a style="width:50%" class="teb_menu" data-id="participation" >참여</a>
		</div>
		<div class="advocate-million"  data-id="introduction">
    	
			<div class="top-intro sectionsub-margin1">
				<div class="div-table">
            		<div class="div-cell">
                		천만 명에게 컴패션 알리기,<br />
						함께하는 방법을 알려드립니다
					</div>
				</div>
			</div>
        
			<p class="txt-caption">컴패션 천만 명에게 알리기란? </p>
			<p class="txt-sub">
        		문자 메시지 및 모바일 메신저(카카오톡, 라인)를 이용하여 지인에게 컴패션의 스토리를 전달하는 프로젝트입니다. 이미지와 짧은 영상으로 구성된 컴패션의 감동스러운 스토리들을 소중한 사람들과 함께 나눠 보세요.
			</p>
			<div class="box-gray">
        		<p class="bu">천만 명 알리기 프로젝트는 1인이 세 번에 걸쳐 지인에게 메시지를 통해 컴패션을 알리는 방식으로 진행됩니다.</p>
			</div>
        
			<p class="txt-caption">참여과정</p>
			<div class="box2-gray">
        		<p class="txt-title"><strong>STEP 1</strong> 후원자 스토리 알리기</p>
				<p class="txt-note">친구에게 메시지와 함께 ‘컴패션 후원자 스토리’ 링크를 보내 주세요</p>
				첫 번째 스토리는 총 4개로 구성되어 있습니다. 스토리 내용을 확인하시고, <span class="color1">지인의 상황에 맞는 스토리를 한 가지만 선택</span>하여 메시지와 함께 지인에게 공유해 주세요.
			</div>
			<div class="box2-gray">
        		<p class="txt-title"><strong>STEP 2</strong> 컴패션 스토리 알리기</p>
				<p class="txt-note">두 번째 메시지와 함께 ‘컴패션 스토리’ 영상 링크를 보내주세요</p>
				<ul class="list-bu">
            		<li>첫 번째 스토리 공유 후 2-3일 후에 지인에게 다시 한 번 스토리를 공유해 주세요. </li>
					<li>두 번째 스토리는 컴패션에 대해 더 깊게 알아볼 수 있는 이야기들로 구성되어 있습니다.</li>
				</ul>
			</div>
			<div class="box2-gray">
        		<p class="txt-title"><strong>STEP 3</strong> 후원 추천 카드 보내기</p>
				<p class="txt-note">‘내 안에 컴패션’의 마음을 담아 후원 추천 카드 보내기</p>
				셋째 날은 온라인 애드보킷 분들이 가지고 계신 컴패션의 마음을 전달하는 날입니다. 직접 마음을 담아 메시지를 작성해 주시고, 어린이의 손을 잡아 줄 수 있는 링크를 함께 공유해 주세요~ 
			</div>
        
			<p class="txt-sub2">
        		지금, 컴패션 천만 명에게 알리기 프로젝트에<br />
				함께해 주세요!
			</p>
			<div class="wrap-bt"><a class="bt-type6 btn_participation" style="width:100%">참여하기</a></div>
        
			<div class="txt-slogon">
        		<strong>당신의 작은 행동이 가난 가운데 있는 <br />
					어린이들에게 큰 힘이 될 것입니다
				</strong>
				Releasing children from poverty in Jesus’ name 
			</div>
        
		</div>

		        <div class="advocate-million" data-id="participation" style="display: none;">

            <div class="ic-caption">
                <span>소중한 분들께 전하고 싶은<br />
                    스토리를 선택해 주세요</span>
            </div>

            <div class="step-support">
                <p class="circle-step">STEP1</p>
                <p class="txt-caption"><span class="color1">후원자</span> 스토리 알리기</p>
                <p class="txt-sub">스토리를 선택하시면 공유 채널 선택 페이지가 나타납니다.</p>
                <ul class="list-story">
                    <li><a ng-click="getPopup.show('http://www.compassion.or.kr/10million/shoeshine_01.html','꿈을 잃은 어린이들에게 그리스도의 사랑을 ㅣ 한국컴패션', '김청하 구두닦이 목사님',$event);">
                        <img src="/common/img/page/advocate/img-million.jpg" alt="" />
                        <span class="textrow2">김정하<br />
                            구두닦이 목사님</span></a></li>
                    <li><a ng-click="getPopup.show('http://www.compassion.or.kr/10million/cfc.html', '꿈을 잃은 어린이들에게 그리스도의 사랑을 ㅣ 한국컴패션', 'Cycling For Compassion', $event);">
                        <img src="/common/img/page/advocate/img-million2.jpg" alt="" />
                        <span class="textrow2">Cycling For<br />
                            Compassion</span></a></li>
                    <li><a ng-click="getPopup.show('http://www.compassion.or.kr/10million/neldi.html', '꿈을 잃은 어린이들에게 그리스도의 사랑을 ㅣ 한국컴패션', '세진이와 넬디 이야기', $event);">
                        <img src="/common/img/page/advocate/img-million3.jpg" alt="" /><span class="textrow2">세진이와 넬디<br />
                            이야기</span></a></li>
                    <li><a ng-click="getPopup.show('http://www.compassion.or.kr/10million/shoeshine_02.html', '꿈을 잃은 어린이들에게 그리스도의 사랑을 ㅣ 한국컴패션', '전용출 후원자 이야기', $event);">
                        <img src="/common/img/page/advocate/img-million4.jpg" alt="" /><span class="textrow2">전용출 후원자<br />
                            이야기</span></a></li>
                </ul>
            </div>

            <div class="step-support">
                <p class="circle-step">STEP2</p>
                <p class="txt-caption"><span class="color1">컴패션</span> 스토리 알리기</p>
                <p class="txt-sub">스토리를 선택하시면 공유 채널 선택 페이지가 나타납니다.</p>
                <ul class="list-story">
                    <li><a ng-click="getPopup.show('http://www.compassion.or.kr/10million/history.html','꿈을 잃은 어린이들에게 그리스도의 사랑을 ㅣ 한국컴패션', '컴패션 히스토리',$event);">
                        <img src="/common/img/page/advocate/img-million5.jpg" alt="" /><span class="textrow1">컴패션 히스토리</span></a></li>
                    <li><a ng-click="getPopup.show('http://www.compassion.or.kr/10million/finance.html','꿈을 잃은 어린이들에게 그리스도의 사랑을 ㅣ 한국컴패션', '양육의 투명성',$event);">
                        <img src="/common/img/page/advocate/img-million6.jpg" alt="" /><span class="textrow1">양육의 투명성</span></a></li>
                    <li><a ng-click="getPopup.show('http://www.compassion.or.kr/10million/miriam.html', '꿈을 잃은 어린이들에게 그리스도의 사랑을 ㅣ 한국컴패션', '마리암의 노래',$event);">
                        <img src="/common/img/page/advocate/img-million7.jpg" alt="" /><span class="textrow1">마리암의 노래</span></a></li>
                    <li><a ng-click="getPopup.show('http://www.compassion.or.kr/10million/mine.html', '꿈을 잃은 어린이들에게 그리스도의 사랑을 ㅣ 한국컴패션', '금 캐는 아이들',$event);">
                        <img src="/common/img/page/advocate/img-million8.jpg" alt="" /><span class="textrow1">금 캐는 아이들</span></a></li>
                </ul>
            </div>

            <div class="step-support">
                <p class="circle-step">STEP3</p>
                <p class="txt-caption">후원 추천 카드 보내기</p>
                <p class="txt-sub">원하는 카드를 선택해 주세요.</p>
                <ul class="list-story">
                    <li><a ng-click="getPopup.show('http://www.compassion.or.kr/10million/letter_03.html','꿈을 잃은 어린이들에게 그리스도의 사랑을 ㅣ 한국컴패션', '사랑이 반드시 이깁니다', $event);">
                        <img src="/common/img/page/advocate/img-million9.jpg" alt="1번 결연 카드 이미지" /></a></li>
                    <li><a ng-click="getPopup.show('http://www.compassion.or.kr/10million/letter_04.html','꿈을 잃은 어린이들에게 그리스도의 사랑을 ㅣ 한국컴패션', '사랑이 반드시 이깁니다', $event);">
                        <img src="/common/img/page/advocate/img-million10.jpg" alt="2번 결연 카드 이미지" /></a></li>
                </ul>
            </div>

        </div>

        <!--//-->
    </div>





</asp:Content>