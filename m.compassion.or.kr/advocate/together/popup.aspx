﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="popup.aspx.cs" Inherits="popup_cart"  %>

    
<div style="background:transparent; width:100%;" class="fn_pop_container">

    <script>
        $(function () {
            common.bindSNSAction();
        })
    </script>
	<!--레이어 안내 팝업(장바구니)-->

	<div class="wrap-layerpop" style="width:80%;left:10%;">
        <div class="header">
        	<p class="txt-title">공유하실 채널을 선택해 주세요</p>
        </div>
        <div class="contents">
            <div class="advocate-compassion">
                <div class="share-channel">
                	<a data-role="sns" data-provider="kt" data-url="{{url}}" data-title="{{title}}" data-picture="" data-desc="{{desc}}" class="pop_share"><img src="/common/img/icon/kakao.png" alt="카카오톡" /></a>
					<a data-role="sns" data-provider="line" data-url="{{url}}" data-title="{{title}}" data-picture="" data-desc="{{desc}}" class="pop_share"><img src="/common/img/icon/line.png" alt="라인" /></a>
					<a data-role="sns" data-provider="sms" data-url="{{url}}" data-title="{{title}}" data-picture="" data-desc="{{desc}}" class="pop_share"><img src="/common/img/icon/sms.png" alt="문자메시지" /></a>
					<div class="linebar"></div>
				</div>
                	
                <div class="wrap-bt"><a ng-href="{{url}}" class="bt-type4">스토리 미리보기</a></div>
                
            </div>
        </div>
        <div class="close"><span class="ic-close" ng-click="getPopup.hide($event)">레이어 닫기</span></div>
    </div>
     
	<!--//레이어 안내 팝업(장바구니)-->
</div>