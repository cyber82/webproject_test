﻿$(function () {

	$page.init();
});

var $page = {

	init: function () {

	},


};

var app = angular.module('cps.page', []);
app.controller("defaultCtrl", function ($scope, $http, popup, $filter, paramService) {

	$scope.total = -1;
	$scope.rowsPerPage = 4;
	$scope.list = null;


	$scope.init = function () {

		$(".teb_menu").click(function () {
			$(".teb_menu").removeClass("selected")
			$(this).addClass("selected")

			participation = $(".advocate-million[data-id='participation']");
			introduction = $(".advocate-million[data-id='introduction']");


			if ($(this).data("id") == "introduction") {
				participation.hide();
				introduction.show();
			} else {
				introduction.hide();
				participation.show();
			}


		})
		$(".btn_participation").click(function () {
			$(".teb_menu[data-id='participation']").trigger("click");

			scrollTo($(".wrap-sectionsub"), 30);
		})

	}
	$scope.init();



	$scope.getPopup = {

	    instance: null,
	    item: null,

	    host: function () {
	        var host = '';
	        var loc = String(location.href);
	        if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
	        else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
	        else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
	        return host;
	    },

	    show: function (url, title, desc, $event) {
	        
	        popup.init($scope, "/advocate/together/popup.aspx", function (modal) {
	            $scope.getPopup.instance = modal;

	            modal.show();

	        });

	        var host = $scope.getPopup.host();
	        url = url.replace('www.compassion.or.kr', host);
	        $scope.url = url;
	        $scope.title = title;
	        $scope.desc = desc;

	    },

	    hide: function ($event) {
	        $event.preventDefault();
	        if (!$scope.getPopup.instance)
	            return;
	        $scope.getPopup.instance.hide();

	    },


	}


    

});
