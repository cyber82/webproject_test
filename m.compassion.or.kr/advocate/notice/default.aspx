﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_notice_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="default.js"></script>
  <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

 
<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
<!---->
    
	<div id="header_menu" class="menu-sympathy sectionsub-margin1" style="position: relative; overflow: hidden;">
    	
        <ul class="notice_type" style="position:absolute;left:0px;top:0;width:900px">  <!--임시 inline style(수정가능)-->
			<asp:Repeater runat="server" ID="repeater">
				<ItemTemplate>
        			<li style="display:inline-block" ng-click="sorting('<%#Eval("cd_key") %>')" data-type="<%#Eval("cd_key") %>"><a><%#Eval("cd_value") %></a></li>
				</ItemTemplate>
			</asp:Repeater>
			<li style="display:inline-block"><a href="#"></a></li>
        </ul>
        
    </div>
    <div class="advocate-notice">
    	
        <fieldset class="frm-search">
            <legend>검색입력</legend>
            <input type="text" name="k_word" id="k_word" ng-enter="search()" value="" placeholder="검색어를 입력해 주세요" />
            <span class="search" ng-click="search()">검색</span>
        </fieldset>

        <div class="wrap-board">

        	<!--결과없음-->
            <div class="no-result" ng-if="total == 0 && isSearch">
            	<strong class="txt-title">검색 결과가 없습니다.</strong>
            </div>
            <!--//결과없음-->
            
			<!-- 게시글 없을때 -->
			<div class="no-content"  ng-if="total == 0 && !isSearch">
				<strong class="txt-title">등록된 글이 없습니다.</strong>
			</div>
			<!--//  -->
            
            <ul class="list-board" ng-if="total != 0">
            	<li ng-repeat="item in list" class="{{item.li_class}}">
					<a class="box-block" ng-click="goView(item.b_id)">
						<strong class="txt-title">
                    		<span class="textcrop-1row" style="display:block;width:95%;"><span ng-if="item.b_sub_type_name == '전체'">[전체공지]</span> {{item.b_title}}</span>
							<!--
							<a href="#" ng-click="sns.show($event , {url : '/advocate/notice/view/' + item.b_id , title : '[한국컴패션-활동공지]' + item.b_title , desc : '' , picture : ''})">공유테스트</a>
							-->
						</strong>
						<span class="txt-date">
                    		{{parseDate(item.b_regdate) | date:'yyyy.MM.dd'}}<span class="bar">|</span><span class="txt-viewcount">{{item.b_hits}}</span>
						</span>
					</a>
            	</li>
            </ul>
        </div>
		<paging ng-if="total != 0" class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>  
       

    </div>
    
<!--//-->
</div>
    
</asp:Content>