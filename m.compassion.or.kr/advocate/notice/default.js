﻿$(function () {


	if (!$(".notice_type li a").hasClass("selected")) {
		$(".notice_type li[data-type=online] a").addClass("selected");
	}

	var sly = new Sly($("#header_menu"), {    // Call Sly on frame

		horizontal: 1,
		itemNav: 'centered',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 3,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1

	});

	sly.init();

	$.each($("#header_menu a"), function (i) {
		if ($(this).hasClass("selected")) {
			sly.activate(i);
		}
	})

});


(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		$scope.total = -1;
		$scope.page = 1;
		$scope.isSearch = false;
		$scope.rowsPerPage = 10;

		$scope.list = [];
		$scope.params = {
			page: $scope.page,
			rowsPerPage: $scope.rowsPerPage,
			b_type: 'advocate',
			b_sub_type: 'all,online',
            k_word : ""
		};

		// 파라미터 초기화
		$scope.params = $.extend($scope.params, paramService.getParameterValues());
		if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);


		if ($scope.params.b_sub_type != '') {
			type = $scope.params.b_sub_type.split(",");
			
			$(".notice_type li a").removeClass("selected")
			$(".notice_type li[data-type='" + type[1] + "'] a").addClass("selected");
		}


		// 검색
		$scope.search = function (params) {
			$scope.params = $.extend($scope.params, params);
			$scope.params.k_word = $("#k_word").val();
			$scope.getList();
		}


		$scope.sorting = function (type) {
			$(".notice_type li a").removeClass("selected");
			$(".notice_type li[data-type=" + type + "] a").addClass("selected");

			$scope.params.page = 1;
			$scope.params.b_sub_type = "all,"+type;
			$("#k_word").val("");

			$scope.search();
		}


		// list
		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);

			$http.get("/api/board.ashx?t=file_list", { params: $scope.params }).success(function (result) {
				$scope.list = result.data;
				$scope.total = result.data.length > 0 ? result.data[0].total : 0;

				$scope.isSearch = false;
				if ($scope.params.k_word != "") {
					$scope.isSearch = true;
				}

				console.log($scope.list);
				$.each($scope.list, function () {
					this.li_class = this.b_file ? "thumb" : "";
					this.title_class = this.b_sub_type_name == '전체' ? "lst_tit" : "lst_tit2"
				})

				//if (params)
			    //scrollTo($("#l"), 10);
				console.log($scope.isSearch);
				console.log($scope.params.k_word);
			});

		}


		// 상세페이지
		$scope.goView = function (id) {
			$http.post("/api/board.ashx?t=hits&id=" + id).then().finally(function () {
				location.href = "/advocate/notice/view/" + id + "?" + $.param($scope.params);
			});
		}

		$scope.parseDate = function (datetime) {
			return new Date(datetime);
		}


		$scope.getList();



	});

})();

