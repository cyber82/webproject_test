﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class advocate_share_view : MobileFrontBasePage {

	const string listPath = "/advocate/share/";


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();

		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect(listPath, true);
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}
		base.PrimaryKey = requests[0];
		id.Value = PrimaryKey.ToString();
	}


	protected override void loadComplete(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<user_board>("b_id", Convert.ToInt32(PrimaryKey), "ub_display", 1, "ub_deleted", 0);
            //if (!dao.user_board.Any(p => p.ub_id == Convert.ToInt32(PrimaryKey) && p.ub_display && !p.ub_deleted))
            if (!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            //var entity = dao.user_board.First(p => p.ub_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<user_board>("ub_id", Convert.ToInt32(PrimaryKey));

            var type = "";
            if (entity.ub_category == "online")
            {
                type = "온라인 애드보킷";
            }
            else if (entity.ub_category == "share")
            {
                type = "나눔별";
            }
            else if (entity.ub_category == "shop")
            {
                type = "프렌즈샵";
            }
            else
            {
                type = entity.ub_category.ToUpper();
            }

            ub_type.Text = type;
            ub_title.Text = entity.ub_title;
            user_id.Text = entity.ub_user_id.Mask("*", 3);
            ub_regdate.Text = entity.ub_regdate.ToString("yyyy.MM.dd");
            ub_hits.Text = entity.ub_hits.ToString("N0");
            ub_content.Text = entity.ub_content.WithFileServerHost();

            // SNS
            this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
            this.ViewState["meta_title"] = string.Format("[한국컴패션-활동공유] {0}", entity.ub_title);

            // optional(설정 안할 경우 주석)
            //this.ViewState["meta_description"] = "";
            //this.ViewState["meta_keyword"] = entity.b_meta_keyword.EmptyIfNull();
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,MobileFrontBasePage)
            this.ViewState["meta_image"] = entity.ub_thumb.WithFileServerHost();

        }
    }



	protected void btn_remove_Click(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            // 수정 삭제버튼
            var user = new UserInfo();
            //var entity = dao.user_board.First(p => p.ub_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<user_board>("ub_id", Convert.ToInt32(PrimaryKey));

            if (user.UserId == entity.ub_user_id)
            {
                entity.ub_deleted = true;
                //dao.SubmitChanges();
                www6.update(entity);

                base.AlertWithJavascript("삭제되었습니다.", "goList()");
            }
        }

		
	}
}