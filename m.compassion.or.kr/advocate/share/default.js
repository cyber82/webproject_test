﻿$(function () {


	if (!$(".share_type li a").hasClass("selected")) {
		$(".share_type li[data-type=all] a").addClass("selected");
	}

	$(".share_type li[data-type=all] a").text("전체");
	
	
	var sly = new Sly($("#header_menu"), {    // Call Sly on frame

		horizontal: 1,
		itemNav: 'centered',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 3,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1

	});

	sly.init();

	$.each($("#header_menu a"), function (i) {
		if ($(this).hasClass("selected")) {
			sly.activate(i);
		}
	})
	
});


(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		$scope.total = -1;
		$scope.page = 1;
		$scope.isSearch = false;
		$scope.rowsPerPage = 3;

		$scope.top = [];
		$scope.list = [];
		$scope.params = {
			page: $scope.page,
			rowsPerPage: $scope.rowsPerPage,
			type: 'advocate',
			category: paramService.getParameter("sorting") == "" ? 'all' : paramService.getParameter("sorting"),
		};

		// 파라미터 초기화
		$scope.params = $.extend($scope.params, paramService.getParameterValues());
		if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);


		if ($scope.params.category != '') {
			$(".share_type li a").removeClass("selected")
			$(".share_type li[data-type='" + $scope.params.category + "'] a").addClass("selected");
		}


		// 검색
		$scope.search = function (params) {
			$scope.params = $.extend($scope.params, params);
			$scope.params.k_word = $("#k_word").val();
			$scope.getList();
		}


		$scope.sorting = function (type) {

			$(".share_type li a").removeClass("selected");
			$(".share_type li[data-type=" + type + "] a").addClass("selected");

			$scope.params.page = 1;
			$scope.params.category = type == "all" ? "" : type;
			$("#k_word").val("");

			$scope.search();
		}


		// list
		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);

			

			$http.get("/api/advocate.ashx?t=share_list", { params: $scope.params }).success(function (result) {
				$scope.list = result.data;
				$scope.total = result.data.length > 0 ? result.data[0].total : 0;
                console.log($scope.list)
				$scope.isSearch = false;
				if ($scope.params.k_word != "") {
					$scope.isSearch = true;
				}

				$.each($scope.list, function () {
					if (this.ub_category == "online") {
						this.type = "온라인 애드보킷"
					} else if (this.ub_category == "share") {
						this.type = "나눔별"
					} else if (this.ub_category == "shop") {
						this.type = "프렌즈샵"
					} else {
						this.type = this.ub_category.toUpperCase();
					}
				})
				//if (params)
					//scrollTo($("#l"), 10);

			});

		}


		// 상세페이지
		$scope.goView = function (id) {
			$http.post("/api/advocate.ashx?t=hits&type=share&id=" + id).then().finally(function () {
				location.href = "/advocate/share/view/" + id + "?" + $.param($scope.params);
			});
		}

		$scope.parseDate = function (datetime) {
			return new Date(datetime);
		}

		$scope.getList();



	});

})();

