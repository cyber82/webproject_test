﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_share_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    <script type="text/javascript" src="default.js"></script>
    <script>
    </script>
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	<!---->
    
		<div id="header_menu" class="menu-sympathy sectionsub-margin1" style="position: relative; overflow: hidden;">
    	
			<ul class="share_type" style="position:absolute;left:0px;top:0;">  <!--임시 inline style(수정가능)-->
				<asp:Repeater runat="server" ID="repeater">
					<ItemTemplate>
        				<li style="display:inline-block" ng-click="sorting('<%#Eval("cd_key") %>')" data-type="<%#Eval("cd_key") %>"><a class="" href="#"><%#Eval("cd_value") %></a></li>
					</ItemTemplate>
                    
				</asp:Repeater>
				<li style="display:inline-block"><a href="#"></a></li>
			</ul>
        
		</div>
		<div class="advocate-share">
    	
			<fieldset class="frm-search">
				<legend>검색입력</legend>
				<input type="text" name="k_word" id="k_word" ng-enter="search()" placeholder="검색어를 입력해 주세요" />
				<span class="search" ng-click="search()">검색</span>
			</fieldset>

			<div class="box-gray">
        		<p class="bu">활동공유 글 작성 및 수정/삭제는 PC버전에서 가능합니다.</p>
			</div>
		
			<!--썸네일 리스트-->
			<div class="wrap-thumbil">
            
				<!--결과없음-->
				<div class="no-result"  ng-if="total == 0 && isSearch">
            		<strong class="txt-title">검색 결과가 없습니다.</strong>
				</div>
				<!--//결과없음-->
				
				<!-- 게시글 없을때 -->
				<div class="no-content" ng-if="total == 0 && !isSearch">
					<strong class="txt-title">등록된 글이 없습니다.</strong>
				</div>
				<!--//  -->

				<ul class="list-thumbil width100" ng-if="total != 0">
					<li ng-repeat="item in list">
						<a class="box-block" ng-click="goView(item.ub_id)">
							<span class="photo"><img ng-src="{{item.ub_thumb}}" alt="" /></span>
							<span class="advocate-diyinfo">
								<span class="txt-category">{{item.type}}</span>
								<span class="txt-title">{{item.ub_title}}</span>
								<span class="txt-date">
                        			{{item.ub_user_id}}<span class="bar"></span>{{parseDate(item.ub_regdate) | date:'yyyy.MM.dd'}}<span class="bar"></span><span class="txt-viewcnt">{{item.ub_hits}}</span>
								</span>
							</span>
						</a>
					</li>
				</ul>
			</div>
        
			<paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>  


		</div>
    
	<!--//-->
	</div>
    
</asp:Content>