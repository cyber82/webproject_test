﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class advocate_intro_apply_step2 : MobileFrontBasePage {
	
	
	public override bool RequireLogin{
		get{
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		if (base.IsRefresh) {
			return;
		}


		if (Request.UrlReferrer == null || Request.UrlReferrer.ToString().IndexOf("/advocate/about/apply/step1") < 0) {
			Response.Redirect("/advocate/about/apply/step1");
		}
	}
}