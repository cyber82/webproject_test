﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_intro_apply_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />
	
<div class="wrap-sectionsub">
<!---->
    <div class="wrap-tab3 sectionsub-margin1">
        <a style="width:50%" class="selected">소개</a>
        <a style="width:50%" href="/advocate/about/apply/step1">지원</a>
    </div>
    <div class="advocate-online">
    	
        <p class="txt-caption"><strong>온라인 애드보킷</strong>은<br />
			연약한 어린이들의 목소리가 되어 
        </p>
        <p class="txt-sub">온라인 상에서 캠페인 참여, 컴패션 활동 공유, SNS 홍보 등을 통하여 적극적으로 컴패션 사역을 알리는 후원자를 말합니다.  
        </p>
        <div style="margin:20px 0"><img src="/common/img/page/advocate/img-online.jpg" alt="" width="100%" /></div>
        
        <div class="register sectionsub-margin1">
        	<p>온라인 애드보킷 활동 자격</p>
            <p>어린이의 목소리가 되고자 하는 뜨거운 마음이 있어야 합니다.</p>
            <p>후원자님의 이름으로 컴패션에 후원한 이력이 있는 분이어야 합니다. (온라인 가입 시 확인 가능합니다.) </p>
            <p>온라인으로 진행되는 동영상 교육 과정을 이수해야 합니다.<br />
			이 과정은 필수입니다.</p>
            <ul>
            	<li>컴패션을  배울 수 있는 3개의 동영상 시청으로 진행되며, 20~25분 정도 소요됩니다.</li>
                <li>각 단계별 영상시청 후 간단한 퀴즈를 통해 컴패션 이해 정도를 확인합니다.</li>
                <li>모든 교육이 끝나면 바로 온라인 애드보킷으로 활동(온라인 CAD, 활동 패킷 신청 등)하실 수 있습니다.</li>
            </ul>
            <a class="bt-type1" href="/advocate/about/apply/step1">온라인 애드보킷 지원하기</a>
        </div>
        
        <p class="txt-caption2">온라인 애드보킷의 자세한 활동 소식이 궁금하신가요?</p>
        <div class="box-gray">
        	<p class="txt-title">공지 게시판</p>
            <p class="txt-note">온라인 애드보킷 활동 공지 게시판 입니다.</p>
            <a class="bt-type4" href="/advocate/notice/">바로 가기</a>
        </div>
        <div class="box-gray">
        	<p class="txt-title">공유 게시판</p>
            <p class="txt-note">온라인 애드보킷 활동 공유 게시판 입니다.</p>
            <a class="bt-type4" href="/advocate/share/?sorting=online">바로 가기</a>
        </div>
        
    </div>
<!--//-->
</div>

    
</asp:Content>