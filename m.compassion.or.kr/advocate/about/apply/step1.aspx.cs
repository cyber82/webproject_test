﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class advocate_intro_apply_step1 : MobileFrontBasePage {
	
	
	public override bool RequireLogin{
		get{
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);
	}

	protected void btnAnswer_Click(object sender, EventArgs e) {
		Response.Redirect("/advocate/about/apply/step2");
	}
}