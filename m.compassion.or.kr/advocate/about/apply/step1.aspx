﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="step1.aspx.cs" Inherits="advocate_intro_apply_step1" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        $(function () {
        });

        var onSubmit = function () {
            if ($("#answer").val() == "") {
                alert("답을 입력해주세요.");
                $("#answer").focus();
                return false;
            }

            if ($("#answer").val() != "한국") {
                $("#answer_message").show();
                return false;
            }

            return true;
        }
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />

	
	<div class="wrap-sectionsub">
	<!---->
    
		<div class="wrap-tab3 sectionsub-margin1">
			<a style="width:50%" href="/advocate/about/apply/">소개</a>
			<a style="width:50%" class="selected">지원</a>
		</div>
		<div class="advocate-online">
    	
			<p class="txt-caption"><strong>컴패션의 가치</strong> 를 전하는 온라인 애드보킷이</p>
			<p class="txt-sub">꼭 갖춰야 할 배경 지식을 학습하는 과정입니다. 간단한 퀴즈를 통해 온라인 애드보킷으로서의 걸음을 시작해 보아요!</p>
        
			<div class="flow-step sectionsub-margin1">
        		<span class="selected">STEP 01</span>
				<span>STEP 02</span>
				<span>STEP 03</span>
			</div>
        
			<p class="ic-title1"><span>컴패션의 시작</span></p>
			<div class="youtube">
        		<iframe src="https://www.youtube.com/embed/cNX_DzClfZ4?rel=0&amp;showinfo=0wmode=transparent" frameborder="0" allowfullscreen></iframe>
			</div>
        
			<p class="txt-quiz">컴패션이 시작된 나라는 어디일까요?</p>
			<div class="box-quiz">
        		컴패션은 1952년, <asp:TextBox type="text" id="answer" runat="server" maxlength="2" style="width:80px"/>  의 전쟁고아를
				본 에버렛 스완슨 목사의 결단으로 시작되었다.
			</div>
        
			<div class="wrap-bt"><asp:LinkButton id="btnAnswer" runat="server" OnClick="btnAnswer_Click" OnClientClick="return onSubmit()" class="bt-type6" Width="80px">확인</asp:LinkButton></div>
			<p class="txt-quizresult comment" id="answer_message" style="display:none;"><span>앗, 정답이 아닙니다. 다른 답을 입력해 보세요~</span></p>
        
        
		</div>
    
	<!--//-->
	</div>


</asp:Content>