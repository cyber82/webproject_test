﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="finish.aspx.cs" Inherits="advocate_intro_apply_finish" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />

	<div class="wrap-sectionsub">
	<!---->
    
		<div class="advocate-online">
    	
			<div class="result-join">
				<p class="txt-caption">온라인 애드보킷 <strong>지원 완료</strong></p>
				<p class="txt-sub">온라인 애드보킷이 되신 걸 축하드립니다!<br />
					다양한 활동으로 컴패션의 양육가치를 널리<br />
					알려 주세요~
				</p>
			</div>
			<div class="result-hotlink sectionsub-margin1">
        		<p class="txt1">온라인 애드보킷은…</p>
				<div class="box-white">
            		<strong>Compassion A Day</strong>
					<p>소중한 컴패션어린이를 친구에게 추천할 수 있어요. </p>
					<a class="bt-type8" href="/advocate/together/compassion-a-day/">바로가기</a>
				</div>
				<div class="box-white">
            		<strong>컴패션블루보드</strong>
					<p>회사, 사업장, 교회에 컴패션을 소개할 수 있는 자료가 담긴 컴패션블루보드를 제공받을 수 있어요. </p>
					<a class="bt-type8" href="/advocate/together/blueboard/">바로가기</a>
				</div>
				<div class="box-white">
            		<strong>컴패션 DIY</strong>
					<p>직접 후원 프로그램을 기획하고 진행할 수 있어요.</p>
					<a class="bt-type8" href="/advocate/together/diy/">바로가기</a>
				</div>
				<div class="box-white">
            		<strong>컴패션 애드보킷 캠페인</strong>
					<p>캠페인별로 제공되는 다양한 프로그램과 패킷을 활용하여 나눔의 기쁨을 느껴 보세요.</p>
					<a class="bt-type8" href="/advocate/together/campaign/">바로가기</a>
				</div>
				<div class="box-white">
            		<strong>컴패션 천만 명에게 알리기</strong>
					<p>많은 사람들에게 컴패션의 가치를 공유할 수 있어요.</p>
					<a class="bt-type8" href="/advocate/together/notify/">바로가기</a>
				</div>
			</div>
        
			<p class="bottom-slogon">우리는 모두 <strong>컴패션</strong>입니다</p>

		</div>
    
	<!--//-->
	</div>


</asp:Content>