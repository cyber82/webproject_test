﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="step2.aspx.cs" Inherits="advocate_intro_apply_step2" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        $(function () {

            $("#btnAnswer").click(function () {
                if ($("input[name=answer]:checked").length < 1) {
                    alert("답을 선택해 주세요.");
                    return false;
                }

                if ($("input[name=answer]:checked").val() != "answer3") {
                    $("#answer_message").show();
                    return false;
                }

                location.href = "/advocate/about/apply/step3";
            });
            
        })

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />

	
	<div class="wrap-sectionsub">
	<!---->
    
		<div class="wrap-tab3 sectionsub-margin1">
			<a style="width:50%" href="/advocate/about/apply/">소개</a>
			<a style="width:50%" class="selected">지원</a>
		</div>
		<div class="advocate-online">
    	
			<p class="txt-caption"><strong>컴패션의 가치</strong> 를 전하는 온라인 애드보킷이</p>
			<p class="txt-sub">꼭 갖춰야 할 배경 지식을 학습하는 과정입니다. 간단한 퀴즈를 통해 온라인 애드보킷으로서의 걸음을 시작해 보아요!</p>
        
			<div class="flow-step sectionsub-margin1">
        		<span>STEP 01</span>
				<span class="selected">STEP 02</span>
				<span>STEP 03</span>
			</div>
        
			<p class="ic-title2"><span>컴패션의 투명성</span></p>
			<div class="youtube">
        		<iframe src="https://www.youtube.com/embed/1rEqPV-H7ms?rel=0&amp;showinfo=0wmode=transparent" frameborder="0" allowfullscreen></iframe>
			</div>
        
			<p class="txt-quiz">컴패션어린이 센터에서는 한 명의 어린이에 대한 정보를 어느 정도의 수준으로 관리하나요?</p>
			<div class="box-quiz">
        		<ul class="list-quiz">
            		<li><span class="radio-ui">
						<input type="radio" value="answer1" id="answer1" name="answer" class="css-radio" checked />
						<label for="answer1" class="css-label">양육관리, 회계관리(그룹별)</label>
					</span></li>
					<li><span class="radio-ui">
						<input type="radio" value="answer2" id="answer2" name="answer" class="css-radio" />
						<label for="answer2" class="css-label">양육관리(그룹별), 회계관리(개인)</label>
					</span></li>
					<li><span class="radio-ui">
						<input type="radio" value="answer3" id="answer3" name="answer" class="css-radio" />
						<label for="answer3" class="css-label">양육관리, 회계관리 (개인)</label>
					</span></li>
				</ul>
            
			</div>
        
			<div class="wrap-bt"><a id="btnAnswer" class="bt-type6" style="width:80px">확인</a></div>
			<p class="txt-quizresult comment" id="answer_message" style="display:none;"><span>앗, 정답이 아닙니다. 다른 답을 입력해 보세요~</span></p>
        
        
		</div>
    
	<!--//-->
	</div>

    
</asp:Content>