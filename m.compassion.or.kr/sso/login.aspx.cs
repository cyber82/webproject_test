﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using CommonLib;

public partial class sso_login : System.Web.UI.Page {

	string auth_domain = ConfigurationManager.AppSettings["domain_auth"];
	protected void Page_Load( object sender, EventArgs e ) {

		return;
		//http://m.compassionko.org/sso/login/?token=HZxuFW%2bwcdZIOiinCSD8NkGexx3ZDlAT%2f0NgCmFUr1szaAAJeMZYhRLbe6ZtYV%2bjo2Ip9rgWucRCZePYTqg1piFeqFBShj4SpAM2LtxMvcw%3d

		// SSO 사이트에서 온 요청이 아니면 무시
		/*
		if(Request.UrlReferrer == null ||
			!Request.UrlReferrer.AbsoluteUri.StartsWith(auth_domain)) {
			return;
		}
		*/
		var token = Request.QueryString["token"];
		
		
		/*
		var values = token.Decrypt().Split('|');
		var uuid = values[0];
		var ip = values[1];
		var date = DateTime.Parse(values[2]);

		Response.Write(token.Decrypt() + "<br>");
		Response.Write(string.Format("uuid={0},ip={1},date={2}" , uuid , ip , date) + "<br>");
		return;
		*/

		Authentication.Result result = authenticate(token);

		if(result.success) {

			//Response.Write(result.message + " : " + result.success.ToString() + " : " + result.errCode.ToString() + " : ");
			if(result.data != null) {
				Response.Write(result.data.ToString());

				/*
				{
					sponsorid: "",
					conid: "",
					userid: "0237love",
					username: "박은주",
					groupno: null,
					jumin: "",
					gendercode: "",
					voc: "",
					mate: "",
					userclass: "14세이상",
					translationflag: "",
					locationtype: "국내",
					certifydate: null,
					commitcount: 0,
					applycount: 0,
					birth: "1981-12-16",
					managetype: "",
					zipcode: "02710",
					addr1: "서울 성북구 정릉로21가길 20 (정릉동, 에이스빌)(서울 성북구 정릉동 389-3)",
					addr2: "203호",
					phone: "",
					mobile: "01074740237",
					email: "ejpark@compassion.or.kr",
					nkid: "0237love"
				}
				*/

			}

			/*
			HttpCookie cookie = new HttpCookie("token");
			cookie.Value = token;
			cookie.Path = "/";
			//cookie.Domain = ConfigurationManager.AppSettings["cookie_domain"];
			Response.Cookies.Set(cookie);
			*/
		}

	}

	Authentication.Result authenticate( string token) {

		using(WebClient wc = new WebClient()) {
			wc.Encoding = System.Text.Encoding.UTF8;
			wc.Headers.Add("cps-token", token);
			wc.Headers.Add("cps-host", Request.Url.Host);
			wc.Headers.Add("Accept", "application/json");

			var url = string.Format("{0}/authenticate/", auth_domain);
			
			string data = wc.DownloadString(url);
			
			return data.ToObject<Authentication.Result>();
		}

	}
}