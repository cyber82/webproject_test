﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete_store.aspx.cs" Inherits="pay_complete_store" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/pay/complete_store.js?v=1.0"></script>
</asp:Content>

<asp:Content ID="head_script_GA" runat="server" ContentPlaceHolderID="head_script_GA">
    <script>
    //GA Product Start
    var orderDetailList = JSON.parse('<%=ViewState["orderDetailList"].ToString()%>');
    console.log('orderDetailList', orderDetailList);
    var productList = [];
    $.each(orderDetailList, function () {
        productList.push({
            'id': this.product_idx,  // 상품 코드           
            'name': this.productTitle, // 상품 이름           
            'brand': '한국컴패션', // 브랜드           
            'category': this.product_gift_flag ? '어린이선물' : '일반상품', // 상품 카테고리           
            'price': this.price, // 가격(\)           
            'quantity': this.qty, // 제품 수량           
            'variant': this.optionDetail // 상품 옵션 
        });
    });

    dataLayer.push({
        'ecommerce': {
            'currencyCode': 'KRW',
            'purchase': {
                'actionField': {
                    'id': '<%=this.ViewState["hdn_orderNo"].ToString()%>', // 주문번호         
                    'revenue': '<%=this.ViewState["hdn_revenue"].ToString()%>',   // 총 결제금액(\)         
                    //'tax': '<%=this.ViewState["hdn_discount"].ToString()%>',  // 할인금액(\)         
                    'shipping': '<%=this.ViewState["hdn_deliveryFee"].ToString()%>'  // 배송비(\)       
                },
                'products': productList
            }
        }
    });
    console.log('dataLayer', dataLayer);
    //GA Product End
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

<div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
<!---->
    
    <input type="hidden" id="hdn_orderNo" runat="server" />
    <input type="hidden" id="hdn_revenue" runat="server" />
    <input type="hidden" id="hdn_discount" runat="server" />
    <input type="hidden" id="hdn_deliveryFee" runat="server" />

    <div class="store-basket">
    	
        <div class="wrap-msg sucess-order sectionsub-margin1">
            <strong>회원님의 주문이 정상적으로 완료 되었습니다.</strong>
            한국컴패션 스토어를 이용해주셔서 감사합니다.
            <div class="wrap-bt"><a class="bt-type3" href="/my/store/order/">주문내역확인</a></div>            
        </div>
        
        <ul class="list-basket">
			<asp:Repeater ID="repeater" runat="server">
                <ItemTemplate>
					 <li>
						<span class="item">
							<img src='<%# Eval("name_img").ToString().WithFileServerHost()%>' alt="" class="photo" />
							<p class="txt-prdname"><%# Eval("productTitle")%></p>
							<p class="txt-option">(옵션)  <%# Eval("optionDetail")%></p>
							<p class="txt-price"><span class="txt-count">수량 <em><%# Eval("qty")%></em></span></p>
						</span>
					</li>
                   
                </ItemTemplate>
            </asp:Repeater>
		</ul>
        
        <ul class="list-totalpay">
            <li>주문금액 합계<span class="txt-price"><em><asp:Literal runat="server" ID="subtotal" /></em>원</span></li>
            <li>배송비<span class="txt-price"><em><asp:Literal runat="server" ID="delivery_fee" /></em>원</span></li>
            <li class="total">총 주문 합계<span class="txt-price"><em><asp:Literal runat="server" ID="total" /></em>원</span></li>
        </ul>
        
    </div>
    
<!--//-->
</div>



	<asp:PlaceHolder runat="server" ID="ph_hidden" Visible="false" >
	<h4>배송정보</h4>
	<div>주문번호 <asp:Literal runat="server" ID="orderNo"/></div>
	<div>받는사람 <asp:Literal runat="server" ID="receiver"/></div>
	<div>주문일 <asp:Literal runat="server" ID="order_date"/></div>
	<div>휴대전화 <asp:Literal runat="server" ID="mobile"/></div>
	<div>배송지주소 <asp:Literal runat="server" ID="addr"/></div>
	
	<h4>주문정보</h4>

	<ul>

		<li>
			<a href="<%# Context.Href("/store/item/" , Eval("product_idx")) %>">
			<img style="max-width:50px" src=<%# Eval("name_img").ToString().WithFileServerHost()%> />
			<%# Eval("productTitle")%> / <%# Eval("optionDetail")%>	/ <%# Eval("childName")%>
			판매가  :<%# (Convert.ToInt32(Eval("totalPrice")) / Convert.ToInt32(Eval("qty"))).ToString("N0") %>
			수량 : <%# Eval("qty")%>
				상품구분 : <%# Eval("childID").ToString().Trim() == "" ? "일반" : "어린이에게<br />보내는 선물"%>
				구매가격 : <%# Eval("totalPrice" , "{0:N0}")%>
			</a>
		</li>	

	</ul>

	subtotal : 
	delivery : 
	total : 

	<h4>결제내역</h4>
	결제금액 : <asp:Literal runat="server" ID="total2" />
	결제수단 : <asp:Literal runat="server" ID="pay_method" />
	</asp:PlaceHolder>
</asp:Content>
