﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete_wedding.aspx.cs" Inherits="pay_complete_wedding" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/ZeroClipboard/ZeroClipboard.js" ></script>
	<script type="text/javascript">

		$(function () {


			var msg = "이 부부는 예식 비용의 일부를 한국컴패션에 기부하였습니다.\n나눔으로 시작한 두 사람의 동행을 진심으로 축복합니다.";

			$("#btn_copy").click(function () {
				window.prompt("아래의 메세지를 선택하여 복사해 주세요", msg);
				return false;

			});

		});

		(function () {
			var app = angular.module('cps.page', []);
			app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

			});

		})();

	</script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
	<!---->
    
		<div class="sponsor-special">
    	
			<div class="msg-applycommit2 sectionsub-margin1">
        		<strong class="txt1">결혼 첫 나눔 신청이<br />완료되었습니다.</strong>
				<div class="box1-married"></div>
				<div class="box2-married">
            		<p class="txt2">&lowast; 샘플이미지 입니다.</p>
					하나님의 자녀 홍길동, 신인선이 하나님 안에서<br />
					하나가 되어 온전히 사랑하겠습니다.<br />
					바쁘시더라도 귀한 걸음 하시어 축복해 주시기 바랍니다.<br /><br />
					<strong>2016년 11월 6일 일요일 오후 1시 약수교회</strong><br /><br />
					<span class="color2">홍상직 · 옥영향</span>의 큰 아들 <span class="color2">길동</span><br />
					<span class="color2">신명화 · 이사온</span>의 막내 딸 <span class="color2">인선</span>
                
					<div class="info-string">
                		<strong>청첩장에 ‘결혼 첫 나눔’ 안내문구를<br />넣어보세요!</strong>
						“이 부부는 예식 비용의 일부를<br />
						한국컴패션에 기부하였습니다.<br />
						나눔으로 시작한 두 사람의 동행을<br />
						진심으로 축복합니다. ”
					</div>
					<div class="wrap-bt"><a class="bt-type8" style="width:140px" id="btn_copy" href="#">안내문구 복사하기</a></div>
					<p class="comment">
						컴패션 로고가 필요한 경우,<br />
						‘마이컴패션>1:1문의’를 통해 문의해주세요.
					</p>
				</div>
				<div class="box3-married"></div>
			</div>
			<p class="align-center" style="font-size:13px;color:#767676;padding-top:20px">두 사람이 하나되는 출발점에서<br />컴패션 어린이의 손을 잡아주셔서 감사합니다.</p>
			<div class="wrap-bt"><a class="bt-type6" style="width:100%" href="/">메인</a></div>
        
		</div>
    
	<!--//-->
	</div>


</asp:Content>
