﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="kcp_no_form.ascx.cs" Inherits="pay_kcp_no_form" %>
<asp:PlaceHolder runat="server" ID="ph_content" Visible="false">
	<div style="display:none">
            <form id="ex_frm" name="ex_frm" method="post" action="/pay/kcp/kcp_no">
	            <input type="hidden" name="buyr_mail" class="w200" value="" maxlength="30" />
	            <input type="hidden" name="buyr_tel1" class="w100" value=""/>
	            <input type="hidden" name="buyr_tel2" class="w100" value=""/>
		
	            <input type="hidden" id="order_item" name="order_item" value="<%:this.ViewState["order_item"].ToString() %>" />
            </form>

            <script type="text/javascript">
                $(function () {
                    $("#ex_frm").submit();
                })
            </script>
    </div>
</asp:PlaceHolder>