﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="kcp.aspx.cs" Inherits="pay_kcp_batch_kcp" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>*** KCP Online Payment System [ASP.NET Version] ***</title>
	<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript">

		// 결제 중 새로고침 방지 샘플 스크립트
		function noRefresh() {
			/* CTRL + N키 막음. */
			if ((event.keyCode == 78) && (event.ctrlKey == true)) {
				event.keyCode = 0;
				return false;
			}
			/* F5 번키 막음. */
			if (event.keyCode == 116) {
				event.keyCode = 0;
				return false;
			}
		}
		document.onkeydown = noRefresh;

		$(function () {

			if ($("#result").val() == "Y") {
				if ($("#msg").val() != "") {
					alert($("#msg").val());
				}
				location.href = $("#successUrl").val();
            }
            //2018-03-13 이종진 - 결제코드에러 시(한도초과 등), pay_again 페이지로 이동
            else if ($("#result").val() == "F") {
                if (confirm($("#msg").val())) {
                    location.href = $("#againUrl").val();
                }
                else {
                    location.href = $("#failUrl").val();
                }
            }
            else {
                //[이종진]2018-03-19 페이지 제자리에 머물러 있도록 하기.
                //kcp_batch 폼이 닫히지 않으므로, 새로고침 시킨다. (데이터는 유지됨)
                location.reload();

				//if ($("#failUrl").val() == "")
				//	return;
				//
				//location.href = $("#failUrl").val();
			}

		})
	</script>
</head>
<body>
	<form runat="server" id="frm">
	<input type="hidden" id="result" runat="server" value="" />
	<input type="hidden" id="msg" runat="server" value="" />
	<input type="hidden" id="successUrl" runat="server" value="" />
	<input type="hidden" id="failUrl" runat="server" value="" />
    <%--이종진 2018-03-12 - 결제실패 시, pay-again 페이지로 가기위한 url을 들고다님--%>
    <input type="hidden" id="againUrl" runat="server" value="" />
	
        <!-- Y , N -->
	<input type="hidden" name="res_cd" value="<%= res_cd    %>" />
	<!-- 결과코드 -->
	<input type="hidden" name="res_msg" value="<%= res_msg   %>" />
	<!-- 결과메시지 -->
	<input type="hidden" name="ordr_idxx" value="<%= ordr_idxx %>" />
	<!-- 주문번호 -->
	<input type="hidden" name="buyr_name" value="<%= buyr_name %>" />
	<!-- 주문자명 -->
	<input type="hidden" name="card_cd" value="<%= card_cd   %>" />
	<!-- 카드코드  -->
	<input type="hidden" name="batch_key" value="<%= batch_key %>" />
	<!-- 배치 인증키-->
	</form>
</body>
</html>

