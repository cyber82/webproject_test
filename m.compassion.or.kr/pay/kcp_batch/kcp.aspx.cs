﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

using KCP.PP_CLI_COM.LIB;
using CommonLib;
using System.Data.Linq;
using System.Linq;
using System.Collections.Generic;

public partial class pay_kcp_batch_kcp : MobileFrontBasePage {

	#region KCP
	/* ==================================================================================== */
	/* +    변수                                                                          + */
	/* - -------------------------------------------------------------------------------- - */
	private string m_strCFG_paygw_url;  // 결제 GW URL
	private string m_strCFG_paygw_port; // 결제 GW PORT
	private string m_strCFG_key_path;   // KCP 모듈 KEY 경로
	private string m_strCFG_log_path;   // KCP 모듈 LOG 경로 
	private string m_strCFG_site_cd;    // 상점 코드
	private string m_strCFG_site_key;   // 상점 키
										/* - -------------------------------------------------------------------------------- - */
	private string m_strCustIP;         // 요청 IP
	private string m_strTxCD;           // 처리 종류
	private string m_strSet_user_type;  // PG1,NEWPG 구분
										/* - -------------------------------------------------------------------------------- - */
	protected string req_tx;            // 요청 종류
	protected string ordr_idxx;         // 주문 번호
	protected string good_name;         // 상품명
	protected string good_mny;          // 결제 총금액
	protected string buyr_name;         // 주문자 명
										/* - -------------------------------------------------------------------------------- - */
	protected string batch_key;         // 배치 인증키
	protected string card_cd;           // 신용카드코드
	protected string card_name;         // 신용카드 명
	protected string res_cd;            // 결과코드
	protected string res_msg;           // 결과메시지
										/* - -------------------------------------------------------------------------------- - */
	private string trad_numb;           // 암호화 정보
	private string enct_info;           // 암호화 정보
	private string enct_data;           // 암호화 정보
										/* - -------------------------------------------------------------------------------- - */

	private string param_opt_1;           // 기타 파라메터 추가 정보
	private string param_opt_2;           // 기타 파라메터 추가 정보
	private string param_opt_3;           // 기타 파라메터 추가 정보  

	#endregion

	WWWService.Service _wwwService = new WWWService.Service();
	WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();
	
	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}

	/* - -------------------------------------------------------------------------------- - */
	protected override void OnBeforePostBack() {
		m_f__get_param();
		
		/*
		m_f__load_env();    // 가맹점 고유 환경설정
		
		m_f__do_tx();

		result.Value = "N";

		if(res_cd == "0000") {
			_wwwService = new WWWService.Service();
			_www6Service = new WWW6Service.SoaHelper();

			if(DoSave()) {
				result.Value = "Y";
			}

		} else {
			base.AlertWithJavascript(res_msg);
			return;
		}
		*/

		
		result.Value = "N";
		
		if(DoSave()) {
			result.Value = "Y";
		}
		

	}

	bool DoSave() {
		
		string tran_cd = m_f__get_post_data("tran_cd");

		var action = new CommitmentAction();
		JsonWriter result = new JsonWriter();
		
		var payInfo = m_f__get_post_data("param_opt_1").ToObject<PayItemSession.Entity>();
		var payday = m_f__get_post_data("param_opt_2");
		var kcpgroup_id = ConfigurationManager.AppSettings["kcpgroup_id"];
		
		var extra = payInfo.extra.Decrypt().ToObject<Dictionary<string, object>>();
		var motiveCode = extra["motiveCode"].ToString();
		var motiveName = extra["motiveName"].ToString();
		successUrl.Value = extra["successUrl"].ToString();
		failUrl.Value = extra["failUrl"].ToString();
		
		if(!UserInfo.IsLogin) {
			base.AlertWithJavascript("로그인 사용자만 결제 가능합니다.");
			return false;
		}

		var sess = new UserInfo();

		if(payInfo.group == "CDSP") {
            //result = action.DoCDSP(payInfo.childMasterId, payInfo.campaignId, payInfo.codeName, motiveCode, motiveName);
            result = action.DoCDSP(payInfo.childMasterId, payInfo.childKey, payInfo.campaignId, payInfo.codeName, motiveCode, motiveName);
        } else {
			result = action.DoCIV(sess.SponsorID, payInfo.amount, 1, "", payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.childMasterId, payInfo.campaignId, motiveCode, motiveName);
		}

		//Response.Write(result.ToJson());

		if(!result.success) {

			base.AlertWithJavascript(result.message);
			return false;
		}

        
        var commitmentId = result.data.ToString();
        payInfo.commitmentId = commitmentId;
        new PayItemSession.Store().Update(ordr_idxx, payInfo, null);
        
        DataSet dsResult;
		string site_cd = ConfigurationManager.AppSettings["g_conf_batch_site_cd"];
		string site_key = ConfigurationManager.AppSettings["g_conf_batch_site_key"];

		string sPaymentType = m_f__get_post_data("pay_method");

		_wwwService = new WWWService.Service();
		dsResult = _wwwService.IssueCardCMS(req_tx, sPaymentType, ordr_idxx, buyr_name, kcpgroup_id
													, site_cd, site_key, trad_numb, tran_cd, enct_info
													, enct_data, m_strCustIP, payday, "", "0003"
													, sess.SponsorID, "", sess.UserId, sess.UserName, "Web");

		string[] sResult = new string[3];

		//getResult
		sResult[0] = dsResult.Tables[0].Rows[0]["ResCd"].ToString();
		sResult[1] = dsResult.Tables[0].Rows[0]["ResMsg"].ToString();

		//===== 결제성공시
		if(sResult[0] == "0000") {

            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성
            string strLog = string.Empty;
            strLog += "SponsorID:" + payInfo.sponsorId + ", ";
            strLog += "OrderID:" + dsResult.Tables[0].Rows[0]["OrderIdxx"].ToString() + ", ";
            strLog += "UserID:" + payInfo.userId + ", ";
            strLog += "CampaignID:" + payInfo.campaignId + ", ";
            strLog += "Group:" + payInfo.group + ", ";
            strLog += "ResultCode:" + dsResult.Tables[0].Rows[0]["ResCd"].ToString() + ", ";
            strLog += "mid:" + m_strCFG_site_cd + ", ";   // 가맹점 ID
            strLog += "tid:" + "" + ", ";   // 거래 ID
            strLog += "moid:" + dsResult.Tables[0].Rows[0]["OrderIdxx"].ToString() + ", ";  // 주문번호
            strLog += "amt:" + payInfo.amount.ToString() + ", ";
            strLog += "cardcode:" + dsResult.Tables[0].Rows[0]["CardCd"].ToString() + ", ";  // 카드사코드
            strLog += "cardname:" + dsResult.Tables[0].Rows[0]["CardName"].ToString() + ", ";  // 결제카드사명
            strLog += "cardbin:" + "" + ", ";   // 카드 BIN 번호
            strLog += "cardpoint:" + "" + ", "; // 카드 포인트 사용여부(0-미사용, 1-포인트사용, 2-세이브포인트사용)
            strLog += "codeID:" + payInfo.codeId + ", ";
            strLog += "buyr_name:" + dsResult.Tables[0].Rows[0]["BuyerName"].ToString() + ", ";
            strLog += "UserName:" + payInfo.sponsorName + ", ";
            strLog += "Mobile:" + sess.Mobile + ", ";
            strLog += "IsUser:" + dsResult.Tables[0].Rows[0]["BuyerName"].ToString() + ", ";    // 회원(회원명), 비회원, 무기명
            strLog += "codename:" + payInfo.codeName + ", ";
            strLog += "Childkey:" + payInfo.childKey + ", ";
            strLog += "ChildMasterID:" + payInfo.childMasterId + ", ";
            strLog += "month:" + payInfo.month + ", ";
            strLog += "frequency:" + payInfo.frequency + ", ";
            strLog += "TypeName:" + payInfo.TypeName + ", ";

            //-----------------------------------------------------------------------------
            // USER-AGENT 구분
            //-----------------------------------------------------------------------------
            string WebMode = Request.UserAgent.ToLower();
            if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
            {
                WebMode = "MOBILE";
            }
            else
            {
                WebMode = "PC";
            }
            strLog += "WebMode:" + WebMode + " ";

            ErrorLog.Write(HttpContext.Current, 605, strLog);
            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성

            // 즉시결제
            var actionResult = new BatchPay().Pay(commitmentId, ordr_idxx, payInfo.amount, payInfo.group);

            //[이종진] GlobalPool의 Hold는 풀지않음
			//new ChildAction().Release(payInfo.childMasterId, false);

			if(actionResult.success) {

			} else {
				//	action.DeleteCommitment(commitmentId);
				msg.Value =
			string.Format(@"정기 후원 신청이 완료되었으나,{0}첫 후원금 결제가 정상적으로 처리되지 못했습니다. 1~3일 내에 등록하신 연락처로 전화 드려 후원금 납부를 도와 드리도록 하겠습니다.",
				string.IsNullOrEmpty(actionResult.message) ? "" : string.Format("{0}의 사유로 ", actionResult.message));

			}

			return true;

		} else {
            //[이종진] 결연삭제 부분은 제거. -> 다시 되돌림.
            //추가로 TCPT용 삭제 작업이 필요함.
            //->nomoneyHold를 ecommerceHold로 변경, compass4 DB에 nomoneyHold 및 임시결연데이터 삭제
            action.DeleteCommitment(commitmentId);
            base.AlertWithJavascript("에러가 발생했습니다. " + sResult[1]);

            //이종진 2018-03-12 - 결제실패 시, pay-again 페이지로 가도록 수정
            //this.result.Value = "F";
            //msg.Value = "정기 후원 신청이 완료되었으나, 첫 후원금 결제가 정상적으로 처리되지 않았습니다. 첫 후원금을 다시 결제하시겠습니까?";

            return false;
		}

		return true;

	}



	#region KCP 
	/* ==================================================================================== */
	/* +    METHOD : GET POST DATA                                                        + */
	/* - -------------------------------------------------------------------------------- - */
	private void m_f__get_param() {
		req_tx = m_f__get_post_data("req_tx");
		m_strCustIP = Request.ServerVariables.Get("REMOTE_ADDR");
		ordr_idxx = m_f__get_post_data("ordr_idxx");
		good_name = m_f__get_post_data("good_name");
		good_mny = m_f__get_post_data("good_mny");
		buyr_name = m_f__get_post_data("buyr_name");
		param_opt_1 = m_f__get_post_data("param_opt_1");
		param_opt_2 = m_f__get_post_data("param_opt_2");
		param_opt_3 = m_f__get_post_data("param_opt_3");
		/* - ---------------------------------------------------------------------------- - */
		trad_numb = m_f__get_post_data("trace_no");
		enct_info = m_f__get_post_data("enc_info");
		enct_data = m_f__get_post_data("enc_data");
        /* - ---------------------------------------------------------------------------- - */

        //이종진 2018-03-12 - 결제실패 시, pay-again 페이지로 가기위한 url을 들고다님
        againUrl.Value = m_f__get_post_data("againUrl");
    }
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : GET POST DATA                                                        + */
	/* - -------------------------------------------------------------------------------- - */
	private string m_f__get_post_data( string parm_strName ) {
		string strRT;

		strRT = Request.Form[parm_strName];

		if(strRT == null)
			strRT = "";

		return strRT;
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 환경 설정                                                            + */
	/* - -------------------------------------------------------------------------------- - */
	private void m_f__load_env() {
		/* -------------------------------------------------------------------------------- */
		/* +    환경설정 DATA 설정                                                        + */
		/* - ---------------------------------------------------------------------------- - */
		/* GET WEB.CONFIG DATA */
		m_strCFG_paygw_url = ConfigurationManager.AppSettings["g_conf_gw_url"];
		m_strCFG_paygw_port = ConfigurationManager.AppSettings["g_conf_gw_port"];
		m_strCFG_key_path = ConfigurationManager.AppSettings["g_kcp_key_path"];
		m_strCFG_log_path = ConfigurationManager.AppSettings["g_kcp_log_path"];
		m_strCFG_site_cd = ConfigurationManager.AppSettings["g_conf_site_cd"];
		m_strCFG_site_key = ConfigurationManager.AppSettings["g_conf_site_key"];
		m_strSet_user_type = ConfigurationManager.AppSettings["g_conf_user_type"];
		/* - ---------------------------------------------------------------------------- - */
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 요청 거래 처리                                                       + */
	/* - -------------------------------------------------------------------------------- - */
	private void m_f__do_tx() {
		C_PP_CLI_COM c_PP_CLI = new C_PP_CLI_COM();
		int nDataSetInx_req;
		bool bRT = false;
		bool bNetCan = false;

		/* -------------------------------------------------------------------------------- */
		/* +    초기화                                                                    + */
		/* - ---------------------------------------------------------------------------- - */
		m_strTxCD = "";
		nDataSetInx_req = 0;
		/* - ---------------------------------------------------------------------------- - */
		c_PP_CLI.m_f__set_env(m_strCFG_paygw_url, m_strCFG_paygw_port,
							   m_strCFG_log_path, m_strCFG_key_path);

		m_strCustIP = Request.ServerVariables.Get("REMOTE_ADDR");
		/* - ---------------------------------------------------------------------------- - */
		c_PP_CLI.m_f__init();
		/* -------------------------------------------------------------------------------- */

		/* -------------------------------------------------------------------------------- */
		/* +    요청 처리                                                                 + */
		/* - ---------------------------------------------------------------------------- - */
		if(req_tx.Equals("pay")) {
			nDataSetInx_req = m_f__set_dataset_pay(ref c_PP_CLI);
		}
		/* - ---------------------------------------------------------------------------- - */
		if(!m_strTxCD.Equals("")) {
			c_PP_CLI.m_f__do_tx(req_tx, m_strTxCD, nDataSetInx_req, m_strCustIP,
								 m_strCFG_site_cd, m_strCFG_site_key, ordr_idxx);

			res_cd = c_PP_CLI.m_strResCD;
			res_msg = c_PP_CLI.m_strResMsg;
		} else {
			res_cd = "9562";
			res_msg = "지불모듈 연동 오류 (TX_CD) 가 정의되지 않았습니다.";
		}
		/* -------------------------------------------------------------------------------- */

		/* -------------------------------------------------------------------------------- */
		/* +    결과 처리                                                                 + */
		/* - ---------------------------------------------------------------------------- - */
		if(res_cd.Equals("0000")) {
			/* - ------------------------------------------------------------------------ - */
			if(req_tx.Equals("pay")) {
				/* - -------------------------------------------------------------------- - */
				bRT = m_f__to_do_shop_pay();                         /* 정상 거래 결과 처리 */
																	 /* - -------------------------------------------------------------------- - */
				m_f__disp_rt_pay_succ(ref c_PP_CLI);
				/* - -------------------------------------------------------------------- - */
			}
			/* - ------------------------------------------------------------------------ - */
		} else {
			/* - ------------------------------------------------------------------------ - */
			m_f__to_do_shop_fail();
			/* - ------------------------------------------------------------------------ - */
			m_f__disp_rt_fail();
		}
		/* -------------------------------------------------------------------------------- */
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 요청 DATA 생성                                                       + */
	/* - -------------------------------------------------------------------------------- - */
	private int m_f__set_dataset_pay( ref C_PP_CLI_COM parm_c_PP_CLI ) {
		/* -------------------------------------------------------------------------------- */
		/* +    지불 요청 DATA 구성                                                       + */
		/* - ---------------------------------------------------------------------------- - */
		m_strTxCD = m_f__get_post_data("tran_cd");

		return parm_c_PP_CLI.m_f__set_axdataset(trad_numb, enct_info, enct_data);
		;
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 정상 결제 결과 처리                                                  + */
	/* - -------------------------------------------------------------------------------- - */
	private bool m_f__to_do_shop_pay() {
		bool bRT = true;

		/* -------------------------------------------------------------------------------- */
		/* +    결과 처리                                                                 + */
		/* - ---------------------------------------------------------------------------- - */

		/* -------------------------------------------------------------------------------- */
		/* +    TODO (필수) : 처리 결과가 정상인 경우에는 반드시 bRT 값을 true로,         + */
		/* +                  오류가 발생한 경우에는 false 로 설정하여 주시기 바랍니다.   + */
		/* - ---------------------------------------------------------------------------- - */
		bRT = true;                  /* 정상 처리인 경우 : true, 오류가 발생한 경우 : false */
									 /* -------------------------------------------------------------------------------- */

		return bRT;
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 오류 결과 처리                                                       + */
	/* - -------------------------------------------------------------------------------- - */
	private bool m_f__to_do_shop_fail() {
		bool bRT = true;

		/* -------------------------------------------------------------------------------- */
		/* +    거래 종류 별 오류 결과 처리                                               + */
		/* - ---------------------------------------------------------------------------- - */
		if(req_tx.Equals("pay")) {
			/* - ------------------------------------------------------------------------ - */
			/* +    TODO : 승인 거래 오류 시 처리                                         + */
			/* - ------------------------------------------------------------------------ - */
		}

		return bRT;
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 결과 출력 (적립/조회/사용 정상)                                      + */
	/* - -------------------------------------------------------------------------------- - */
	private void m_f__disp_rt_pay_succ( ref C_PP_CLI_COM parm_c_PP_CLI ) {
		/* -------------------------------------------------------------------------------- */
		/* +    정상 결과 출력                                                            + */
		/* - ---------------------------------------------------------------------------- - */
		res_cd = res_cd;
		res_msg = res_msg;
		/* - ---------------------------------------------------------------------------- - */
		ordr_idxx = ordr_idxx;
		good_name = good_name;
		good_mny = good_mny;
		buyr_name = buyr_name;
		card_cd = parm_c_PP_CLI.m_f__get_res("card_cd");
		card_name = parm_c_PP_CLI.m_f__get_res("card_name");
		batch_key = parm_c_PP_CLI.m_f__get_res("batch_key");
		/* -------------------------------------------------------------------------------- */
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 결과 출력 (오류)                                                     + */
	/* - -------------------------------------------------------------------------------- - */
	private void m_f__disp_rt_fail() {
		/* -------------------------------------------------------------------------------- */
		/* +    오류 결과 출력                                                            + */
		/* - ---------------------------------------------------------------------------- - */
		res_cd = res_cd;
		res_msg = res_msg;
		/* -------------------------------------------------------------------------------- */
	}
	/* ==================================================================================== */
	#endregion
}
