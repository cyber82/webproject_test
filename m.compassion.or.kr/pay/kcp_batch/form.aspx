﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="form.aspx.cs" Inherits="pay_kcp_batch_form" %>
<%
    /* ============================================================================== */
    /* =   PAGE : 결제 요청 PAGE                                                    = */
    /* = -------------------------------------------------------------------------- = */
    /* =   이 페이지는 Payplus Plug-in을 통해서 결제자가 결제 요청을 하는 페이지    = */
    /* =   입니다. 아래의 ※ 필수, ※ 옵션 부분과 매뉴얼을 참조하셔서 연동을        = */
    /* =   진행하여 주시기 바랍니다.                                                = */
    /* = -------------------------------------------------------------------------- = */
    /* =   연동시 오류가 발생하는 경우 아래의 주소로 접속하셔서 확인하시기 바랍니다.= */
    /* =   접속 주소 : http://kcp.co.kr/technique.requestcode.do			        = */
    /* = -------------------------------------------------------------------------- = */
    /* =   Copyright (c)  2013   KCP Inc.   All Rights Reserverd.                   = */
    /* ============================================================================== */
%>

<%
	/* kcp와 통신후 kcp 서버에서 전송되는 결제 요청 정보 */
	String req_tx          = Request.Form[ "req_tx"         ]; // 요청 종류         
	String res_cd          = Request.Form[ "res_cd"         ]; // 응답 코드         
	String tran_cd         = Request.Form[ "tran_cd"        ]; // 트랜잭션 코드     
	String ordr_idxx       = Request.Form[ "ordr_idxx"      ]; // 쇼핑몰 주문번호   
	String good_name       = Request.Form[ "good_name"      ]; // 상품명            
	String good_mny        = Request.Form[ "good_mny"       ]; // 결제 총금액       
	String buyr_name       = Request.Form[ "buyr_name"      ]; // 주문자명          
	String enc_info        = Request.Form[ "enc_info"       ]; // 암호화 정보       
	String enc_data        = Request.Form[ "enc_data"       ]; // 암호화 데이터     
	String kcp_group_id        = Request.Form[ "kcp_group_id"       ]; // 암호화 데이터     
	String site_cd     = Request.Form[ "site_cd"    ];
	String app_url = AppSession.HasCookie(this.Context) ? "cpsApp://" : "";

	/* 기타 파라메터 추가 부분 - Start - */
	String param_opt_1    = Request.Form[ "param_opt_1"     ]; // payInfo
	String param_opt_2    = Request.Form[ "param_opt_2"     ]; // 
	String param_opt_3    = Request.Form[ "param_opt_3"     ]; // payday
	

	String tablet_size     = "1.0"; // 화면 사이즈 고정
	String url             = Request.Url.ToString();

		if(Request["action"] == "move") {

		ordr_idxx       = Request[ "ordr_idxx"      ]; // 쇼핑몰 주문번호   
		good_name       = Request[ "good_name"      ]; // 상품명            
		good_mny        = Request[ "good_mny"       ]; // 결제 총금액       
		buyr_name       = Request[ "buyr_name"      ]; // 주문자명          
		site_cd     = Request[ "site_cd"    ];
		param_opt_2     = Request[ "param_opt_2"    ];
		
		var payItem = new PayItemSession.Store().Get(ordr_idxx);
		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();
		param_opt_1 = payItem.data;
		param_opt_3 = payInfo.extra.Decrypt().ToObject<Dictionary<string, object>>()["failUrl"].ToString();
		%>
        
        <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
		<form id="ex_frm" name="ex_frm" method="post" action="/pay/kcp_batch/form">
        <%--<form id="ex_frm" name="ex_frm" method="post" action="/sponsor/pay/regular/kcp/form">--%>

			<!-- KCP -->
			<input type="hidden" name="ordr_idxx" value="<%=ordr_idxx%>" />
			<input type="hidden" name="good_name" value="<%=good_name%>"/>
			<input type="hidden" name="good_mny" value="<%=good_mny%>" />
			<input type="hidden" name="buyr_name" value="<%=buyr_name %>"/>
			<input type="hidden" name="site_cd" value="<%=site_cd%>" />
			<input type='hidden' name='param_opt_1' value='<%=param_opt_1%>'>
			<input type='hidden' name='param_opt_2' value='<%=param_opt_2%>'>
			<input type='hidden' name='param_opt_3' value='<%=param_opt_3%>'>
		
			<!--// KCP -->
		</form>
		<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
		<script type="text/javascript">

			$(function () {
				$("#ex_frm").submit();
			})
		</script>
		<%
	return;

	}

%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
	<title>*** KCP [AX-HUB Version] ***</title>
	<meta http-equiv="Content-Type" content="text/html;">
	<meta http-equiv="Cache-Control" content="No-Cache">
	<meta http-equiv="Pragma" content="No-Cache">

	<meta name="viewport" content="width=device-width, user-scalable=<%=tablet_size%>, initial-scale=<%=tablet_size%>, maximum-scale=<%=tablet_size%>, minimum-scale=<%=tablet_size%>">

	<!-- 거래등록 하는 kcp 서버와 통신을 위한 스크립트-->
	<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>

	<script type="text/javascript" src="/pay/kcp_batch/approval_key.js"></script>
	<script type="text/javascript" src="/pay/kcp_batch/form.js"></script>
<%--	<script type="text/javascript" src="/sponsor/pay/regular/kcp/approval_key.js"></script>
	<script type="text/javascript" src="/sponsor/pay/regular/kcp/form.js"></script>--%>
	<script>
		$(function () {
		//	alert($("#param_opt_1").val())
			//alert($("#kcp_group_id").val())
		})
	</script>
</head>

<body>
<div id="sample_wrap">
<form name="order_info" method="post">

	<input type="hidden" name="encoding_trans" value="UTF-8" /> 
	<input type="hidden" name="PayUrl" >
	<input type="hidden" name="ordr_idxx" value="<%=ordr_idxx %>" />
	<input type="hidden" name="good_name" value="<%=good_name %>" />
	<input type="hidden" name="good_mny" value="<%=good_mny %>" maxlength="9" />
	<input type="hidden"  name="buyr_name" value="<%=buyr_name %>" />
	<input type="hidden" id="kcp_group_id" name="kcp_group_id" value="<%=ConfigurationManager.AppSettings["kcpgroup_id"].ToString() %>" />

	 <!-- 공통정보 -->
    <input type="hidden" name="req_tx"          value="pay">                           <!-- 요청 구분 -->
    <input type="hidden" name="shop_name"       value="한국컴패션">                       <!-- 사이트 이름 --> 
    <input type="hidden" name="site_cd" value="<%=site_cd %>">
    <input type="hidden" name="currency"        value="410"/>                          <!-- 통화 코드 -->
    <input type="hidden" name="eng_flag"        value="N"/>                            <!-- 한 / 영 -->
    <input type="hidden" name='kcp_cert_flag'   value="N"/>
    <input type="hidden" name="kcp_bath_info_view"     value="N">

    <!-- 결제등록 키 -->
    <input type="hidden" name="approval_key"    id="approval">
    <!-- 인증시 필요한 파라미터(변경불가)-->
    <input type="hidden" name="escw_used"       value="N">
    <input type="hidden" name="pay_method"      value="AUTH">
    <input type="hidden" name="ActionResult"    value="batch">
    <!-- 리턴 URL (kcp와 통신후 결제를 요청할 수 있는 암호화 데이터를 전송 받을 가맹점의 주문페이지 URL) -->
    <input type="hidden" name="Ret_URL"         value="<%= url%>">
    <!-- 화면 크기조정 -->
    <input type="hidden" name="tablet_size"     value="<%= tablet_size%>">

    <!-- 추가 파라미터 ( 가맹점에서 별도의 값전달시 param_opt 를 사용하여 값 전달 ) -->
    <input type="hidden" id="param_opt_1" name="param_opt_1"	   value='<%:param_opt_1%>'>
    <input type="hidden" name="param_opt_2"	   value="<%=param_opt_2%>">
    <input type="hidden" name="param_opt_3"	   value="<%=param_opt_3%>">
	<input type="hidden" name='AppUrl'		 value="<%=app_url%>">
<%
    /* ============================================================================== */
    /* =   옵션 정보                                                                = */
    /* = -------------------------------------------------------------------------- = */
    /* =   ※ 옵션 - 결제에 필요한 추가 옵션 정보를 입력 및 설정합니다.             = */
    /* = -------------------------------------------------------------------------- = */
	/* 카드사 리스트 설정
	예) 비씨카드와 신한카드 사용 설정시

    /*  무이자 옵션
            ※ 설정할부    (가맹점 관리자 페이지에 설정 된 무이자 설정을 따른다)                             - "" 로 설정
            ※ 일반할부    (KCP 이벤트 이외에 설정 된 모든 무이자 설정을 무시한다)                           - "N" 로 설정
            ※ 무이자 할부 (가맹점 관리자 페이지에 설정 된 무이자 이벤트 중 원하는 무이자 설정을 세팅한다)   - "Y" 로 설정
    <input type="hidden" name="kcp_noint"       value=""/> */

    /*  무이자 설정
            ※ 주의 1 : 할부는 결제금액이 50,000 원 이상일 경우에만 가능
            ※ 주의 2 : 무이자 설정값은 무이자 옵션이 Y일 경우에만 결제 창에 적용
            예) 전 카드 2,3,6개월 무이자(국민,비씨,엘지,삼성,신한,현대,롯데,외환) : ALL-02:03:04
            BC 2,3,6개월, 국민 3,6개월, 삼성 6,9개월 무이자 : CCBC-02:03:06,CCKM-03:06,CCSS-03:06:04
    <input type="hidden" name="kcp_noint_quota" value="CCBC-02:03:06,CCKM-03:06,CCSS-03:06:09"/> */

	/* KCP는 과세상품과 비과세상품을 동시에 판매하는 업체들의 결제관리에 대한 편의성을 제공해드리고자, 
	   복합과세 전용 사이트코드를 지원해 드리며 총 금액에 대해 복합과세 처리가 가능하도록 제공하고 있습니다
	   복합과세 전용 사이트 코드로 계약하신 가맹점에만 해당이 됩니다
       상품별이 아니라 금액으로 구분하여 요청하셔야 합니다
	   총결제 금액은 과세금액 + 부과세 + 비과세금액의 합과 같아야 합니다. 
	   (good_mny = comm_tax_mny + comm_vat_mny + comm_free_mny)
	
	    <input type="hidden" name="tax_flag"       value="TG03">  <!-- 변경불가	   -->
	    <input type="hidden" name="comm_tax_mny"   value=""    >  <!-- 과세금액	   --> 
        <input type="hidden" name="comm_vat_mny"   value=""    >  <!-- 부가세	   -->
	    <input type="hidden" name="comm_free_mny"  value=""    >  <!-- 비과세 금액 -->  */
    /* = -------------------------------------------------------------------------- = */
    /* =   옵션 정보 END                                                            = */
    /* ============================================================================== */
%>


</form>
</div>

<!-- 스마트폰에서 KCP 결제창을 레이어 형태로 구현-->
<div id="layer_all" style="position:absolute; left:0px; top:0px; width:100%;height:100%; z-index:1; display:none;">
	<iframe name="frm_all" frameborder="0" marginheight="0" marginwidth="0" border="0" width="100%" height="100%" scrolling="auto"></iframe>
</div>

<form name="pay_form" method="post" action="kcp">
    <input type="hidden" name="req_tx"         value="<%=req_tx%>">               <!-- 요청 구분          -->
    <input type="hidden" name="res_cd"         value="<%=res_cd%>">               <!-- 결과 코드          -->
    <input type="hidden" name="tran_cd"        value="<%=tran_cd%>">              <!-- 트랜잭션 코드      -->
    <input type="hidden" name="ordr_idxx"      value="<%=ordr_idxx%>">            <!-- 주문번호           -->
    <input type="hidden" name="buyr_name"      value="<%=buyr_name%>">            <!-- 주문자명           -->
    <input type="hidden" name="enc_info"       value="<%=enc_info%>">
    <input type="hidden" name="enc_data"       value="<%=enc_data%>">

    <!-- 추가 파라미터 -->
	<input type="hidden" name="param_opt_1"	   value='<%:param_opt_1%>'>

    <input type="hidden" name="param_opt_2"	   value="<%=param_opt_2%>">
    <input type="hidden" name="param_opt_3"	   value="<%=param_opt_3%>">
	
</form>
</body>
</html>

