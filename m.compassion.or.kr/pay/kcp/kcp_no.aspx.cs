﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

using KCP.PP_CLI_COM.LIB;
using CommonLib;
using System.Data.Linq;
using System.Linq;
using System.Text;

public partial class pay_kcp_no : FrontBasePage {
	string order_item;
	
	public override bool RequireSSL {
		get {
			return true;
		}
	}

	/* - -------------------------------------------------------------------------------- - */
	protected override void OnBeforePostBack() {
		result.Value = "N";

        order_item = m_f__get_post_data("order_item");

        if (DoSave())
        {
            result.Value = "Y";
        }

        else
        {
            return;
        }
	}

	bool DoSave() {
        JsonWriter result = new JsonWriter();

        StoreAction.orderItem oi = order_item.ToObject<StoreAction.orderItem>();
        orderNo.Value = oi.orderNo;

        if (UserInfo.IsLogin)
        {
            var sponsorId = "";
            var sUserID = "";
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
            sUserID = sess.UserId;

            //Pay Log 생성 (결제모듈 응답)
            StringBuilder sb = new StringBuilder();
            sb.Append("sponsorId:" + sponsorId + ", ");
            sb.Append("sUserID:" + sUserID + ", ");
            sb.Append("payInfo.campaignId:" + "" + ", ");
            sb.Append("payInfo.group:" + "store" + ", ");
            sb.Append("param_01:" + m_f__get_post_data("param_opt_1"));
            ErrorLog.Write(HttpContext.Current, 605, sb.ToString());

            oi.authDate = "";
            oi.authNumber = "";
            oi.bank = "";
            oi.bankCode = "";
            oi.cardName = "";
            oi.cardPeriod = "";
            oi.cardType = "";
            oi.settlePrice = Convert.ToInt32("0");
            oi.site_code = "";
            oi.payKey = "";
            oi.payMethod = "";
            oi.req_tx = "pay";
            oi.resCD = "";
            oi.resMsg = "";

            result = new StoreAction().Order(oi);

            if (result.success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }

        return true;
	}

    private string m_f__get_post_data(string parm_strName)
    {
        string strRT;

        strRT = Request.Form[parm_strName];

        if (strRT == null)
            strRT = "";

        return strRT;
    }
}