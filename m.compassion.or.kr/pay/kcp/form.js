﻿/* kcp web 결제창 호츨 (변경불가) */

function call_pay_form() {
	
	var v_frm = document.order_info;
	document.getElementById("sample_wrap").style.display = "none";
	document.getElementById("layer_all").style.display = "block";
	v_frm.target = "frm_all";
	// 인코딩 방식에 따른 변경 -- Start    
	if (v_frm.encoding_trans == undefined) {
		v_frm.action = PayUrl;
	} else {
		if (v_frm.encoding_trans.value == "UTF-8") {
			v_frm.action = PayUrl.substring(0, PayUrl.lastIndexOf("/")) + "/jsp/encodingFilter/encodingFilter.jsp";
			v_frm.PayUrl.value = PayUrl;
		} else {
			v_frm.action = PayUrl;
		}
	}         // 인코딩 방식에 따른 변경 -- End  
	if (v_frm.Ret_URL.value == "") {
		/* Ret_URL값은 현 페이지의 URL 입니다. */
		alert("연동시 Ret_URL을 반드시 설정하셔야 됩니다."); return false;
	} else {
		v_frm.submit();
	}
}

/* kcp 통신을 통해 받은 암호화 정보 체크 후 결제 요청 (변경불가) */
function chk_pay() {
    
	self.name = "tar_opener";
    var pay_form = document.pay_form;

    // [jun.heo] 2017-12 : 결제 모듈 통합 - param_opt_2 에 overseas_card 와 payPageType 을 묶어서(구분자 | ) 사용함
    var payPageType = pay_form.param_opt_2.value.split('|')[1];
	
    if (pay_form.res_cd.value == "3001") {
	 //   alert("사용자가 취소하였습니다.");
		pay_form.res_cd.value = "";
		//alert(pay_form.failUrl.value)
	    //location.href = pay_form.param_opt_2.value;
        
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
        switch (payPageType) {
            case "Store":    // 스토어
                location.href = "/store/order";
                break;
            case "PayDelay":    // 지연된 후원금
                alert("사용자가 취소하였습니다.");
                location.href = "/my/sponsor/pay-delay/";
                break;
            case "Temporary":   // 일시후원 및 나머지
            default:
                location.href = pay_form.param_opt_3.value;
                break;
        }
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
        
		return;
	}
	else if (pay_form.res_cd.value == "3000") {
		alert("30만원 이상 결제를 할 수 없습니다.");
		pay_form.res_cd.value = "";
	    //location.href = pay_form.param_opt_2.value;

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
        switch (payPageType) {
            case "Store":    // 스토어
                location.href = "/pay/fail_store";
                break;
            case "PayDelay":    // 지연된 후원금
                location.href = "/my/sponsor/pay-delay/";
                break;
            case "Temporary":   // 일시후원 및 나머지
            default:
                location.href = pay_form.param_opt_3.value;
                break;
        }
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분

		return false;
	}

	document.getElementById("sample_wrap").style.display = "block";
	document.getElementById("layer_all").style.display = "none";

    if (pay_form.enc_info.value) {
		pay_form.submit();
	} else {
		kcp_AJAX();
		
	}


}

$(function () {
	
	chk_pay();
})