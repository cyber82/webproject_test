﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fail_sponsor.aspx.cs" Inherits="pay_fail_sponsor" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/sponsor/item-child.ascx" TagPrefix="uc" TagName="child" %>
<%@ Register Src="/sponsor/item-special.ascx" TagPrefix="uc" TagName="sf" %>
<%@ Register Src="/sponsor/item-user-funding.ascx" TagPrefix="uc" TagName="uf" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
</script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
<div class="wrap-sectionsub">
<!---->
	<a href="#" id="btnList" runat="server"></a>
    
    <div class="sponsor-payment">
    	
        <strong class="txt-payfail">결제가<br />실패하였습니다.</strong>
        
        <div class="wrap-bt"><a class="bt-type6" style="width:100%" href="#" runat="server" id="btnPay">다시 결제하기</a></div>
        
        
    </div>
    
<!--//-->
</div>

</asp:Content>
