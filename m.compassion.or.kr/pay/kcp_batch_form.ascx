﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="kcp_batch_form.ascx.cs" Inherits="pay_kcp_batch_form" %>
<asp:PlaceHolder runat="server" ID="ph_content" Visible="false">

	<script type="text/javascript">

		$(function () {

			// 결제 중 새로고침 방지 샘플 스크립트 (중복결제 방지)
			function noRefresh() {
				/* CTRL + N키 막음. */
				if ((event.keyCode == 78) && (event.ctrlKey == true)) {
					event.keyCode = 0;
					return false;
				}
				/* F5 번키 막음. */
				if (event.keyCode == 116) {
					event.keyCode = 0;
					return false;
				}
			}
			document.onkeydown = noRefresh;

			$("#payday").val($(".paymentDay:checked").val());

			$("#ex_frm").submit();

		})
	</script>
		<div style="display:none">

    <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
    <form id="ex_frm" name="ex_frm" method="get" action="/pay/kcp_batch/form">
	<%--<form id="ex_frm" name="ex_frm" method="get" action="/sponsor/pay/regular/kcp/form">--%>

		<!-- KCP -->
	
		<input type="hidden" name="ordr_idxx" value="<%:this.ViewState["orderId"].ToString() %>" />
		<input type="hidden" name="good_name" value="<%:this.ViewState["good_name"].ToString() %>"/>
		<input type="hidden" name="good_mny" value="<%:this.ViewState["good_mny"].ToString() %>" />
		<input type="hidden" name="buyr_name" value="<%:this.ViewState["user_name"].ToString() %>"/>
		
		<input type="hidden" name="site_cd" value="<%:this.ViewState["site_cd"].ToString() %>" />
		<!--
		<input type='hidden' name='param_opt_1' value='<%:this.ViewState["payInfo"].ToString() %>'>
			-->

		<input type='hidden' name='param_opt_2' id="payday" value=''>
		
		<input type='hidden' name='action' value='move'>

        <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
        <input type="hidden" name="payPageType" value="<%:this.ViewState["payPageType"].ToString() %>" />
		
        <%--이종진 2018-03-12 - 결제실패 시, pay-again 페이지로 가기위한 url을 들고다님--%>
        <input type='hidden' id="againUrl" name='againUrl'      value='<%:this.ViewState["againUrl"].ToString() %>'>

		<!--// KCP -->

	</form>
			</div>
</asp:PlaceHolder>