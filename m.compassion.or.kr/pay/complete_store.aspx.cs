﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class pay_complete_store : MobileFrontBasePage
{

    public override bool RequireSSL
    {
        get
        {
            return true;
        }
    }

    public override bool NoCache
    {
        get
        {
            return true;
        }
    }

    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    const string listPath = "/store/";

    protected override void OnBeforePostBack()
    {

        var requests = Request.GetFriendlyUrlSegments();
        if (requests.Count < 1)
        {
            Response.Redirect(listPath, true);
        }

        UserInfo sess = new UserInfo();
        base.PrimaryKey = requests[0];

        try
        {
            using (StoreDataContext dao = new StoreDataContext())
            {

                //var om = dao.orderMaster.First(p => p.orderNo == this.PrimaryKey.ToString() && sess.UserId == p.UserID && p.State == 2);      // state == 2 결제완료
                var om = www6.selectQFStore<orderMaster>("orderNo", this.PrimaryKey.ToString(), "UserID", sess.UserId, "State", 2);
                //var od = dao.orderDelivery.First(p => p.orderNo == this.PrimaryKey.ToString());
                var od = www6.selectQFStore<orderDelivery>("orderNo", this.PrimaryKey.ToString());

                // 선물편지의 경우 결제처리 flag update
                //var orderDetail = dao.orderDetail.Where(p => p.orderNo == this.PrimaryKey.ToString() && p.repCorrespondenceId != null);
                var orderDetail = www6.selectQ2Store<orderDetail>("orderNo = ", this.PrimaryKey.ToString(), "repCorrespondenceId ", "IS NOT NULL");

                if (orderDetail != null)
                {
                    if (orderDetail.Count() > 0)
                    {
                        foreach (var detail in orderDetail)
                        {
                            CommonLib.WWW6Service.SoaHelperSoap _www6Service;
                            _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
                            String sqlText = " update  tCorrespondenceWeb set isOrder = 1 where CorrespondenceWebID = '" + detail.CorrespondenceWebID + "' ";
                            Object[] objSql = new object[1] { sqlText };
                            var iResult = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
                        }
                    }
                }

                hdn_orderNo.Value = om.orderNo;
                ViewState["hdn_orderNo"] = om.orderNo;

                hdn_revenue.Value = om.settlePrice.ToString();
                ViewState["hdn_revenue"] = om.settlePrice.ToString();

                hdn_discount.Value = om.discountPrice.ToString();
                ViewState["hdn_discount"] = om.discountPrice.ToString();
                
                hdn_deliveryFee.Value = om.deliveryFee.ToString();
                ViewState["hdn_deliveryFee"] = om.deliveryFee.ToString();

                orderNo.Text = om.orderNo;
                receiver.Text = od.rcvName;
                order_date.Text = om.dtReg.Value.ToString("yyyy-MM-dd");
                mobile.Text = od.rcvPhone;
                addr.Text = string.Format("[{0}] {1} {2}", od.rcvZipCode, od.rcvAddr, od.rcvAddrDetail);

                subtotal.Text = om.orderPrice.ToString("N0");
                delivery_fee.Text = om.deliveryFee.ToString("N0");
                total2.Text = total.Text = om.settlePrice.ToString("N0");

                if (om.payMethod == "100000000000")
                {
                    pay_method.Text = "신용카드";
                }
                else if (om.payMethod == "010000000000")
                {
                    pay_method.Text = "실시간 계좌이체";
                }
                else if (om.payMethod == "000010000000")
                {
                    pay_method.Text = "휴대폰";
                }
                else if (om.payMethod == "payco")
                {
                    pay_method.Text = "페이코";
                }
                else if (om.payMethod == "kakaopay")
                {
                    pay_method.Text = "카카오페이";
                }
                var imagePath = Uploader.GetRoot(Uploader.FileGroup.image_shop);
                repeater.DataSource = new StoreAction().GetOrderDetailList(this.PrimaryKey.ToString(), imagePath).data;

                ViewState["orderDetailList"] = Newtonsoft.Json.JsonConvert.SerializeObject(new StoreAction().GetOrderDetailList(this.PrimaryKey.ToString(), imagePath).data);

                repeater.DataBind();

            }
        }
        catch (Exception ex)
        {
            Response.Write(ex);
            //		Response.Redirect(listPath , true);
        }

    }

}