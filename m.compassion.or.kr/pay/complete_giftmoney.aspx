﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete_giftmoney.aspx.cs" Inherits="pay_complete_giftmoney" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	<asp:PlaceHolder runat="server" ID="ph_complete" Visible="false">
		<script type="text/javascript">
			$(function () {
				alert("결제가 완료되었습니다.");
				location.href = "/my/sponsor/gift-money/";
			});

		</script>
	</asp:PlaceHolder>
</asp:Content>
