﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="payco_form.ascx.cs" Inherits="pay_payco_form" %>
<asp:PlaceHolder runat="server" ID="ph_content" Visible="false">
	<script type="text/javascript" src="https://static-bill.nhnent.com/payco/checkout/js/payco.js" charset="UTF-8"></script>

	<script type="text/javascript">
		
    function order() {
    	
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리
        var params;
        switch ($("#payPageType").val()) {
            case "PayDelay":    // 지연된 후원금인 경우
                params = {
                    customerOrderNumber: $("#ordr_idxx").val(),
                    //[jun.heo] 2017-12 : 결제 모듈 통합
                    path: "/pay/payco",
                    //path: "/sponsor/pay/temporary/payco",
                    productName: $("#good_name").val(),
                    amount: $("#good_mny").val(),
                    completePage: $("#returnUrl").val(),
                    cancelPage: "/my/sponsor/pay-delay/",
                    jumin: $("#jumin").val(),
                    ci: $("#ci").val(),
                    di: $("#di").val(),
                    month: $("#month").val(),
                    buyr_name: $("#buyr_name").val(),
                    payPageType: $("#payPageType").val()    //[jun.heo] 2017-12 : 결제 모듈 통합
                };
                break;
            case "Temporary":    // 특별한 모금 및 나머지
            default:    // 나머지 경우 : 일시후원 등
                params = {
                    customerOrderNumber: $("#ordr_idxx").val(),
                    //[jun.heo] 2017-12 : 결제 모듈 통합
                    path: "/pay/payco",
                    //path: "/sponsor/pay/temporary/payco",
                    productName: $("#good_name").val(),
                    amount: $("#good_mny").val(),
                    completePage: $("#returnUrl").val(),
                    cancelPage: $("#cancelUrl").val(),
                    failPage: $("#failUrl").val(),
                    month: $("#month").val(),
                    buyr_name: $("#buyr_name").val(),
                    payPageType: $("#payPageType").val()    //[jun.heo] 2017-12 : 결제 모듈 통합
                };
                break;
        }
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리

	
		// localhost 로 테스트 시 크로스 도메인 문제로 발생하는 오류
		$.support.cors = true;

		$.ajax({
            type: "POST",
            //[jun.heo] 2017-12 : 결제 모듈 통합
            url: "/pay/payco/payco_reserve",
            //url: "/sponsor/pay/temporary/payco/payco_reserve",
			data: params,		// JSON 으로 보낼때는 JSON.stringify(customerOrderNumber)
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			dataType: "json",
			success: function (data) {
			
				if (data.code == '0') {
					console.log(data.result.reserveOrderNo);
					$('#order_num').val(data.result.reserveOrderNo);
					$('#order_url').val(data.result.orderSheetUrl);

					payco_open();

				} else {
					alert("code:" + data.code + "\n" + "message:" + data.message);
				}
			},
			error: function (request, status, error) {
				//alert("에러가 발생했습니다.");
				//에러코드
			//	alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
			//	if (request.status > 0)
			//		alert("code:" + request.status + "\n" + "message:" + request.responseText );
				/*
				document.getElementById("status").innerHTML = request.status + "<br />";
				document.getElementById("responseText").innerHTML = request.responseText + "<br />";
				document.getElementById("error_desc").innerHTML = error + "<br />";
				*/
				return false;
			}
		});
	}

	function payco_open() {
    
		var order_url = $('#order_url').val();
		document.location.href = order_url;
    
	}

	$(function () {
		
		if ($("#noname").length > 0 && $("#noname").prop("checked")) {
			$("#user_name").val("무기명");
		}

		$("#buyr_name").val($("#user_name").val());

		setTimeout("order();", 300);

	})
	</script>
	<div style="display:none">
	<input type="hidden" id="ordr_idxx" name="ordr_idxx" value="<%:this.ViewState["orderId"].ToString() %>" />
	<input type='hidden' id="returnUrl" name='returnUrl'      value='<%:this.ViewState["returnUrl"].ToString() %>'>
	<input type='hidden' id="failUrl" name='failUrl'      value='<%:this.ViewState["failUrl"].ToString() %>'>
	<input type='hidden' id="cancelUrl" name='cancelUrl'      value='<%:this.ViewState["cancelUrl"].ToString() %>'>
	
	<input type='hidden' id="buyr_name" name='buyr_name'      value='<%:this.ViewState["user_name"].ToString() %>'>
	<input type='hidden' id="jumin" name='jumin'      value='<%:this.ViewState["jumin"].ToString() %>'>
	<input type='hidden' id="ci" name='ci'      value='<%:this.ViewState["ci"].ToString() %>'>
	<input type='hidden' id="di" name='di'      value='<%:this.ViewState["di"].ToString() %>'>
	<input type='hidden' id="month" name='month'      value='<%:this.ViewState["month"].ToString() %>'>
	<input type="hidden" id="good_name" name="good_name" value="<%:this.ViewState["good_name"].ToString() %>"/>
	<input type="hidden" id="good_mny" name="good_mny" value="<%:this.ViewState["good_mny"].ToString() %>" maxlength="9"/>
    <input type="hidden" id="firstPay" name="firstPay" value="<%: this.ViewState["firstPay"] == null ? "" : this.ViewState["firstPay"].ToString() %>" maxlength="9"/>

	<input type="hidden" name="order_num" id="order_num" value=""  >
	<input type="hidden" name="order_url" id="order_url" value=""  >

    <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
    <input type="hidden" id="payPageType" name="payPageType" value="<%:this.ViewState["payPageType"].ToString() %>" />

	</div>
</asp:PlaceHolder>