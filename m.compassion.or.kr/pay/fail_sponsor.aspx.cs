﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class pay_fail_sponsor : MobileFrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		
		var r = Request["r"].ValueIfNull("/");
		btnPay.HRef = btnList .HRef = r;


	}

}