﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;


public partial class pay_complete_paydelay : MobileFrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {

		var requests = Request.GetFriendlyUrlSegments();
		var orderId = "";
		if(requests.Count > 0) {
			orderId = requests[0];
		}
		
		if(!string.IsNullOrEmpty(orderId)) {

			ph_complete.Visible = true;

			var payItem = new PayItemSession.Store().Get(orderId);
			if(payItem == null || payItem.is_completed)
				return;
			
			new PayItemSession.Store().Complete(orderId);
			
		} else {
		}

	
	}

}