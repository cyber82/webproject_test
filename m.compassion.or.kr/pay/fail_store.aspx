﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fail_store.aspx.cs" Inherits="fail_store" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	<div class="wrap-sectionsub">
	<!---->
    
		<div class="store-basket">
    	
			<div class="wrap-msg fail-order sectionsub-margin1">
        		<strong>주문이 실패하였습니다.</strong>
			</div>
			<div class="wrap-bt">
        		<a class="bt-type6 fl" style="width:48.5%" href="/store/order/">다시 결제하기</a>
				<a class="bt-type7 fr" style="width:48.5%" href="/store/cart/">장바구니 가기</a>
			</div>            
        
		</div>
	</div>
</asp:Content>
