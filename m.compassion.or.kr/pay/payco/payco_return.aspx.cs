﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Newtonsoft.Json.Linq;
using payco_easypay_pay1_csharp;

//-----------------------------------------------------------------------------
// PAYCO 주문 완료시 호출되는 RETURN 페이지 샘플 ( ASP.NET )
// payco_return.asp
// 2015-03-25	PAYCO기술지원 <dl_payco_ts@nhnent.com>
//-----------------------------------------------------------------------------

//---------------------------------------------------------------------------------
// Payco 결제가 완료되었습니다.
// 이 페이지는 PAYCO 결제창에서 "주문이 완료되었습니다." 라는 페이지에서 버튼을 눌러야 호출이 되는 페이지 입니다.
// 고객이 결제창을 x 를 눌러 닫을 수 있기 때문에 주문 목록 페이지로 이동 할 수 없을 수 있습니다.
// DB 작업이나 기타 작업을 이곳에 하시면 안됩니다.
//---------------------------------------------------------------------------------

public partial class pay_payco_return : System.Web.UI.Page {
	protected string AppWebPath;
	protected string WebMode;                     //USER-AGENT를 분석하여 호출 모드를 설정

	protected payco_util pu = new payco_util();

	protected void Page_Load( object sender, EventArgs e ) {
		Response.ContentType = "text/html";

		string receive_str;
		receive_str = "payco_return.aspx is Called - ";

		foreach(string key in Request.Form.Keys) {
			receive_str += key + " : " + Request[key] + ", ";
		}
		pu.Write_Log(receive_str);
	}

	protected void EXEC_API() {
		//-----------------------------------------------------------------------------
		// 오류가 발생했는지 기억할 변수와 결과를 담을 변수를 선언합니다.
		//-----------------------------------------------------------------------------
		string resultValue;
		string Read_code, Read_message;
		string returnUrlParam1, returnUrlParam2, returnUrlParam3;
		//  string AppWebPath = ConfigurationManager.AppSettings["AppWebPath"].ToString();
		//string AppWebPath = Request.Domain() + "/_test/payco";

		// 호출 성공여부 읽기
		Read_code = Request["code"];

		//주문예약시 전달한 returnUrlParam 의 처리(이 파라메터는 취소시에도 받을 수 있으므로 결제 확인 여부 이전에 먼저 나와야 합니다.)
		//---------------------------------------------------------------------------------
		string AppWebPath = Request["AppWebPath"];
		string customerOrderNumber = Request["customerOrderNumber"];
		string completePage = Request["completePage"];
		string cancelPage = Request["cancelPage"];
        string failPage = Request["failPage"];

        //---------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------
        // Read_code 값이 0 또는 2222 값 중 하나가 옵니다.
        // 0 - 결제 인증 성공
        // 2222 - 사용자에 의한 결제 취소
        // 내역을 표시하고 창을 닫습니다.
        //-----------------------------------------------------------------------------
        if(Read_code != "0") {
            var nextPage = (Read_code.ToString().Equals("2222")) ? cancelPage : failPage;
            Read_message = Request["message"];
			if(Read_code.ToString().Equals("2222")) { Read_message = "사용자에 의해 취소되었음."; }
			resultValue = "Code : " + Read_code.ToString() + ", message : " + Read_message.ToString();
			pu.Write_Log("payco_return.aspx is canceled : " + resultValue);

			Response.Write("<html>");
			Response.Write("    <head>");
			Response.Write("        <title>주문 취소</title>");
			Response.Write("        <script type=\"text/javascript\">");
			Response.Write("	        alert(\"" + Read_message + "\");");
			if(WebMode.Equals("MOBILE")) {
				Response.Write("	       location.href = \"" + nextPage + "\";");
			} else {
				Response.Write("	        opener.location.href = \"" + nextPage + "\";");
				Response.Write("	        window.close();");
			}
			Response.Write("        </script>");
			Response.Write("    </head>");
			Response.Write("</html>");
			return;                                       // 페이지 종료
		}

		Response.Write("<html>");
		Response.Write("    <head>");
		Response.Write("        <title>주문 완료</title>");
		Response.Write("        <script type=\"text/javascript\">");
		if(WebMode.Equals("MOBILE")) {
			Response.Write("	       location.href = \"" + completePage + "\";");
		} else {
            //[jun.heo] 2017-12 : 결제 모듈 통합
            Response.Write("	        location.href = \"" + completePage + "\";");
            //Response.Write("	        opener.location.href = \"" + completePage + "\";");
            //Response.Write("	        window.close();");
		}
		Response.Write("        </script>");
		Response.Write("    </head>");
		Response.Write("</html>");
	}
}
