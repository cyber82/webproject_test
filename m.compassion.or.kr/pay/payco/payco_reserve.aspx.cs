﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Newtonsoft.Json.Linq;
using CommonLib;

//-----------------------------------------------------------------------------
// PAYCO 주문 예약 페이지 샘플 ( ASP.NET )
// payco_reserve.aspx
// 2015-04-20	PAYCO기술지원 <dl_payco_ts@nhnent.com>
//-----------------------------------------------------------------------------

public partial class pay_payco_reserve : System.Web.UI.Page {
	protected string Result;
	protected string WebMode;                    //USER-AGENT를 분석하여 호출 모드를 설정
	protected string customerOrderNumber;
	public string AppWebPath , productName , completePage , cancelPage , failPage;
	public uint amount;
	public string payInfo , jumin, ci, di, month, buyr_name;
    public string payPageType;  //[jun.heo] 2017-12 : 결제 모듈 통합
    public string userId, sponsorId, userName;
	private payco_util pu = new payco_util();
	
	protected void Page_Load( object sender, EventArgs e ) {
		
		Response.Charset = "UTF-8";

		//-----------------------------------------------------------------------------
		// USER-AGENT 구분
		//-----------------------------------------------------------------------------
		WebMode = Request.UserAgent.ToLower();
		if(!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0)) {
			WebMode = "MOBILE";
		} else {
			WebMode = "PC";
		}
		
		//AppWebPath = Request.Domain() + "/_test/payco";
		AppWebPath = Request.Domain() + Request["path"];
		productName = Request["productName"];
		completePage = Request["completePage"];
		cancelPage = Request["cancelPage"];
        failPage = Request["failPage"];
        //payInfo = Request["payInfo"];
        jumin = Request["jumin"];
        ci = Request["ci"];
        di = Request["di"];
        month = Request["month"];
		buyr_name = Request["buyr_name"];
		amount = Convert.ToUInt32(Request["amount"]);
        payPageType = Request["payPageType"];  //[jun.heo] 2017-12 : 결제 모듈 통합

        if (UserInfo.IsLogin) {
			var sess = new UserInfo();
			userId = sess.UserId;
			sponsorId = sess.SponsorID;
            //buyr_name = sess.UserName;
            userName = sess.UserName;

			//ErrorLog.Write(this.Context , 0 , "sponsorID=" + sponsorId);
		}
		
		//-----------------------------------------------------------------------------
		// 이 문서는 json 형태의 데이터를 반환합니다.
		//-----------------------------------------------------------------------------
		Response.ContentType = "application/json";

		//-----------------------------------------------------------------------------
		// (로그) 호출 시점과 호출값을 파일에 기록합니다.
		//-----------------------------------------------------------------------------
		string receive_str = "payco_reserve.aspx is Called - ";

		foreach(string key in Request.Form.Keys) {
			receive_str += key + " : " + Request.Form[key] + ", ";
		}

		pu.Write_Log(receive_str);
		//---------------------------------------------------------------------------------
		// 이전 페이지에서 전달받은 고객 주문번호 설정
		//---------------------------------------------------------------------------------

		customerOrderNumber = Request.Form["customerOrderNumber"];

		EXEC_API();

        //Pay Log 생성 (결제모듈 요청)
        string strLog = string.Empty;
        strLog += "sponsorId:" + sponsorId + ", ";
        strLog += "orderId:" + customerOrderNumber + ", ";
        strLog += "good_mny:" + amount + " ";
        ErrorLog.Write(HttpContext.Current, 602, strLog);
    }

	protected void EXEC_API() {
		//pu.Write_Log("2");
		//---------------------------------------------------------------------------------
		// 상품정보 변수 선언 및 초기화
		//---------------------------------------------------------------------------------
		int OrderNumber;
		uint orderQuantity, productUnitPrice, productUnitPaymentPrice, productAmt, productPaymentAmt, TotalProductPaymentAmt;
		string iOption;
		int sortOrdering;
		string orderConfirmUrl, orderConfirmMobileUrl, productImageUrl, sellerOrderProductReferenceKey;
		JObject jsonOrder, jsonOrderProducts;
		JArray jsonOrderProductsArry;
		string taxationType;
		uint unitTaxfreeAmt, unitTaxableAmt, unitVatAmt;
		uint totalTaxfreeAmt, totalTaxableAmt, totalVatAmt;

		//---------------------------------------------------------------------------------
		// 변수 초기화
		//---------------------------------------------------------------------------------
		TotalProductPaymentAmt = 0;     // 주문 상품이 여러개일 경우 상품들의 총 금액을 저장할 변수
		OrderNumber = 0;                // 주문 상품이 여러개일 경우 순번을 매길 변수
		totalTaxfreeAmt = 0;            // 총 면세 금액
		totalTaxableAmt = 0;            // 총 과세 공급가액
		totalVatAmt = 0;                // 총 과세 부가세액

		unitTaxfreeAmt = 0;
		unitTaxableAmt = 0;
		unitVatAmt = 0;

		//---------------------------------------------------------------------------------
		// 상품정보 값 입력(여러개의 상품일 경우 묶어서 한개의 정보로 포함해서 보내주시면 됩니다. 배송비 포함)
		//---------------------------------------------------------------------------------
		OrderNumber = OrderNumber + 1;                                          // 상품에 순번을 정하기 위해 값을 증가합니다.

		orderQuantity = 1;                                                      // (필수) 주문수량 (배송비 상품인 경우 1로 세팅)
																				//상품단가(productAmt)는 원 상품단가이고 상품결제단가(productPaymentAmt)는 상품단가에서 할인등을 받은 금액입니다. 실제 결제에는 상품결제단가가 사용됩니다.
		productUnitPrice = amount;                                               // (필수) 상품 단가 ( 테스트용으로써 70,000원으로 설정. )
		productUnitPaymentPrice = amount;                                        // (필수) 상품 결제 단가 ( 테스트용으로써 69,000원으로 설정. )

		productAmt = productUnitPrice * orderQuantity;                          // (필수) 상품 결제금액(상품단가 * 수량)
		productPaymentAmt = productUnitPaymentPrice * orderQuantity;            // (필수) 상품 결제금액(상품결제단가 * 수량)
		TotalProductPaymentAmt = TotalProductPaymentAmt + productPaymentAmt;    // 주문정보를 구성하기 위한 상품들 누적 결제 금액(상품 결제 금액), 면세금액,과세금액,부가세의 합

		iOption = "";                                                        // 옵션 ( 최대 100 자리 )
		sortOrdering = OrderNumber;                                             // (필수) 상품노출순서, 10자 이내
		//productName = productName;  // (필수) 상품명, 4000자 이내
		orderConfirmUrl = "";                                                   // 주문완료 후 주문상품을 확인할 수 있는 url, 4000자 이내
		orderConfirmMobileUrl = "";                                             // 주문완료 후 주문상품을 확인할 수 있는 모바일 url, 1000자 이내
		productImageUrl = "";                                                   // 이미지URL (배송비 상품이 아닌 경우는 필수), 4000자 이내, productImageUrl에 적힌 이미지를 썸네일해서 PAYCO 주문창에 보여줍니다.
		sellerOrderProductReferenceKey = "item";                                   // 외부가맹점에서 관리하는 주문상품 연동 키(sellerOrderProductReferenceKey)는 상품 별로 고유한 key이어야 합니다.)

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
        if (payPageType == "Store")
        {
            taxationType = "TAXATION";                                              // 과세타입(기본값 : 과세). DUTYFREE :면세, COMBINE : 결합상품, TAXATION : 과세
        }
        else
        {
            taxationType = "DUTYFREE";
        }

		switch(taxationType) {
			case "TAXATION":
				unitTaxfreeAmt = 0;                                             // 개별상품단위 면세 금액은 0 원으로 설정
				unitTaxableAmt = (uint)(productUnitPaymentPrice / 1.1);         // 개별상품단위 상품금액으로 공급가액 계산
				unitVatAmt = (uint)((productUnitPaymentPrice / 1.1) * 0.1);     // 개별상품단위 상품금액으로 부가세액 계산
				if(unitTaxableAmt + unitVatAmt != productUnitPaymentPrice)  // 개별상품단위 공급가액과 부가세액을 더했을 경우 상품금액과 일치하지 않으면 
				{
					unitTaxableAmt = unitTaxableAmt + 1;                        // 공급가액에 1원을 더해 맞춤.
				}
				break;
			case "DUTYFREE":
				unitTaxfreeAmt = productUnitPaymentPrice;                   // 개별상품단위 면세금액에 상품금액 전체를 넣음.
				unitTaxableAmt = 0;                                         // 개별상품단위 공급가액은 0 원
				unitVatAmt = 0;                                             // 개별상품단위 부가세액은 0 원
				break;
			case "COMBINE":
				unitTaxfreeAmt = 36000;                                     // 개별상품단위 상품금액에서 면세 금액 부분을 입력. ( 샘플에서는 36,000으로 설정 )
				unitTaxableAmt = 30000;                                     // 개별상품단위 공급가액을 입력.( 샘플에서는 30,000으로 설정 )
				unitVatAmt = 3000;                                          // 개별상품단위 부가세액을 입력.( 샘플에서는  3,000으로 설정 )
				break;
		}

		totalTaxfreeAmt = totalTaxfreeAmt + unitTaxfreeAmt;                 // 총 면세 금액 추가
		totalTaxableAmt = totalTaxableAmt + unitTaxableAmt;                 // 총 과세 공급가액 추가
		totalVatAmt = totalVatAmt + unitVatAmt;                             // 총 과세 부가세액 추가

		//---------------------------------------------------------------------------------
		// 구매 상품을 변수에 셋팅 ( JSON 문자열을 생성 )
		//---------------------------------------------------------------------------------

		jsonOrder = new JObject();
		jsonOrderProducts = new JObject();
		jsonOrderProductsArry = new JArray();

		jsonOrderProducts.Add("cpId", JToken.FromObject(ConfigurationManager.AppSettings["pc_cpId"]));
		jsonOrderProducts.Add("productId", JToken.FromObject(ConfigurationManager.AppSettings["pc_productId"]));
		jsonOrderProducts.Add("productAmt", JToken.FromObject(productAmt));
		jsonOrderProducts.Add("productPaymentAmt", JToken.FromObject(productPaymentAmt));
		jsonOrderProducts.Add("orderQuantity", JToken.FromObject(orderQuantity));
		jsonOrderProducts.Add("option", JToken.FromObject(iOption));
		jsonOrderProducts.Add("sortOrdering", JToken.FromObject(sortOrdering));
		jsonOrderProducts.Add("productName", JToken.FromObject(productName));
		jsonOrderProducts.Add("orderConfirmUrl", JToken.FromObject(orderConfirmUrl));
		jsonOrderProducts.Add("orderConfirmMobileUrl", JToken.FromObject(orderConfirmMobileUrl));
		jsonOrderProducts.Add("productImageUrl", JToken.FromObject(productImageUrl));
		jsonOrderProducts.Add("sellerOrderProductReferenceKey", JToken.FromObject(sellerOrderProductReferenceKey));
		jsonOrderProducts.Add("taxationType", JToken.FromObject(taxationType));

		jsonOrderProductsArry.Add(jsonOrderProducts);
		jsonOrder.Add("orderProducts", JToken.FromObject(jsonOrderProductsArry));

		//---------------------------------------------------------------------------------
		// 주문정보 변수 선언
		//---------------------------------------------------------------------------------
		string sellerOrderReferenceKey, sellerOrderReferenceKeyType;
		string iCurrency;
		uint totalOrderAmt, totalDeliveryFeeAmt, totalPaymentAmt;
		string orderTitle, serviceUrl, returnUrl, nonBankbookDepositInformUrl;
		JObject returnUrlParam, serviceUrlParam;
		string orderChannel, inAppYn;
		string individualCustomNoInputYn;
		JObject extraData;
		string payExpiryYmdt, virtualAccountExpiryYmd, appUrl, cancelMobileUrl;
		string showMobileTopGnbYn, iframeYn;

		//---------------------------------------------------------------------------------
		// 주문정보 값 입력 ( 가맹점 수정 가능 부분 )
		//---------------------------------------------------------------------------------
		sellerOrderReferenceKey = customerOrderNumber;                                  // (필수) 외부가맹점의 주문번호
		sellerOrderReferenceKeyType = "UNIQUE_KEY";                                     // 외부가맹점의 주문번호 타입(String 50) UNIQUE_KEY 유니크 키 - 기본값, DUPLICATE_KEY 중복 가능한 키( 외부가맹점의 주문번호가 중복 가능한 경우 사용)
		iCurrency = "KRW";                                                              // 통화(default=KRW)
		totalOrderAmt = TotalProductPaymentAmt;                                         // (필수) 총 주문금액. 지수형태를 피하고 문자열로 보낼려면 formatnumber(TotalProductPaymentAmt,0,,,0)
		totalDeliveryFeeAmt = 0;                                                        // 총 배송비(상품가격에 포함).
		totalPaymentAmt = totalOrderAmt + totalDeliveryFeeAmt;                          // (필수) 총 결재 할 금액.
		orderTitle = productName;                                    // 주문 타이틀
		serviceUrl = AppWebPath + "/payco_callback";   // 주문완료 시 PAYCO에서 호출할 가맹점의 Service API의 URL
		serviceUrl = AppWebPath.Replace("10.77.185.145", "210.206.104.163").Replace("10.0.9.163", "210.206.104.163") + "/payco_callback";    // 배포시 삭제

        if (Request.Domain().IndexOf("compassionko.org") > -1)
        {
            //[jun.heo] 2017-12 : 결제 모듈 통합
            serviceUrl = "http://121.138.118.134/pay/payco/payco_callback2";
            //serviceUrl = "http://121.138.118.134/sponsor/pay/temporary/payco/payco_callback2";
        }
        else
        {
            //[jun.heo] 2017-12 : 결제 모듈 통합
            serviceUrl = Request.Domain() + "/pay/payco/payco_callback";
            //serviceUrl = Request.Domain() + "/sponsor/pay/temporary/payco/payco_callback";
        }
		if(ConfigurationManager.AppSettings["stage"] == "dev")
			serviceUrl = serviceUrl.Replace("https://", "http://");

        //Page.ClientScript.RegisterStartupScript(this.GetType(), "", string.Format("alert('{0}');{1};", serviceUrl, ""), true);

        serviceUrlParam = new JObject();                                                    // 주문완료 시 PAYCO에서 가맹점의 Service API 호출할때 같이 전달할 파라미터
		
		serviceUrlParam.Add("userId", userId);
		serviceUrlParam.Add("sponsorId", sponsorId);
        serviceUrlParam.Add("month", month);
        //serviceUrlParam.Add("payInfo", payInfo);
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
        if (payPageType == "PayDelay")
        {
            serviceUrlParam.Add("jumin", jumin);
            serviceUrlParam.Add("ci", ci);
            serviceUrlParam.Add("di", di);
            serviceUrlParam.Add("buyr_name", buyr_name);
            serviceUrlParam.Add("userName", userName);
        }
        serviceUrlParam.Add("payPageType", payPageType);    //[jun.heo] 2017-12 : 결제 모듈 통합

        returnUrl = AppWebPath + "/payco_return";      // 주문완료 후 Redirect 되는 Url
		returnUrlParam = new JObject();                                                          // 주문완료 후 Redirect 되는 Url 에 함께 전달되어야 하는 파라미터
																								 //--------------------------------------------------------------------------------
																								 //여러개의 값을 보낼 경우  (payco_reserve.aspx에서 payco_return.aspx로 전달할 값을 JSON 형태의 문자열로 전달)
		returnUrlParam.Add("AppWebPath", AppWebPath);                                    // SAMPLE_RETURN_PARAM1 라는 필드에 002001 값을 보냄.
		returnUrlParam.Add("customerOrderNumber", customerOrderNumber);                                    // SAMPLE_RETURN_PARAM2 라는 필드에 002002 값을 보냄.
	//	returnUrlParam.Add("payInfo", payInfo);                                    // SAMPLE_RETURN_PARAM3 라는 필드에 002003 값을 보냄.
		returnUrlParam.Add("completePage", completePage);
		returnUrlParam.Add("cancelPage", cancelPage);
        returnUrlParam.Add("failPage", failPage);


        //--------------------------------------------------------------------------------
        nonBankbookDepositInformUrl = AppWebPath + "/payco_without_bankbook";          //무통장입금완료통보 URL
		orderChannel = WebMode;                                                         // 주문채널 ( default : PC / MOBILE )

        if (payPageType == "PayDelay") inAppYn = "Y";                                                                  // 인앱결제 여부( Y/N ) ( default = N )
        else inAppYn = "N";                                                                  // 인앱결제 여부( Y/N ) ( default = N )

        individualCustomNoInputYn = "N";                                                // 개인통관고유번호 입력 여부 ( Y/N ) ( default = N )
																						//orderSheetUiType = "GRAY";												    // 주문서 UI 타입 선택 ( 선택 가능값 : RED / GRAY ) - 사용안함

		//---------------------------------------------------------------------------------
		// 주문서에 담길 부가 정보( extraData ) 를 JSON 으로 작성
		//---------------------------------------------------------------------------------
		// payExpiryYmdt = "20161210180000";										// 해당 주문예약건 만료 처리 일시 (해당일시 이후에는 결제 불가) Ex. "20150210180000"
		payExpiryYmdt = DateTime.Now.AddMinutes(10).ToString("yyyyMMddHHmmss");
		virtualAccountExpiryYmd = "20161201";                                   // 가상계좌만료일시. ( YYYYMMDD or YYYYMMDDHHmmss, Default 3일, YYYYMMDD 일 경우 그날 24시 만료. )
		appUrl = "";
        if (AppSession.HasCookie(this.Context))
            appUrl = "cpsApp://open";
        if (WebMode == "MOBILE") {
			cancelMobileUrl = Request.Domain() + cancelPage;
			// (모바일이면 필수) 모바일 결제페이지에서 취소 버튼 클릭시 이동할 URL ( 결제창 이전 URL등 ).
			// 1순위 : (앱결제인 경우) 주문예약 > "nebilres://orderCancel" 으로 이동
			// 2순위 : 주문예약 > extraData > cancelMobileUrl 값이 있을시 => cancelMobileUrl 이동
			// 3순위 : 주문예약시 전달받은 returnUrl 이동 + 실패코드(오류코드:2222)
			// 4순위 : 가맹점 URL로 이동(가맹점등록시 받은 사이트URL)
			// 5순위 : 이전 페이지로 이동 => history.Back();
		} else {
			cancelMobileUrl = "";
		}
		showMobileTopGnbYn = "N";                                               // 모바일 상단 GNB 노출여부
		iframeYn = "N";                                                         // 모바일에서 Iframe 으로 호출 여부

		JObject viewOptions;

		viewOptions = new JObject();
		viewOptions.Add("showMobileTopGnbYn", JToken.FromObject(showMobileTopGnbYn));                   // 모바일 상단 GNB 노출여부
		viewOptions.Add("iframeYn", JToken.FromObject(iframeYn));                                       // Iframe 으로 호출 여부

		extraData = new JObject();
        if (payPageType == "PayDelay") extraData.Add("payExpiryYmdt", JToken.FromObject(payExpiryYmdt));
        if (payPageType == "PayDelay") extraData.Add("virtualAccountExpiryYmd", JToken.FromObject(virtualAccountExpiryYmd));
        if (payPageType == "PayDelay") extraData.Add("appUrl", JToken.FromObject(appUrl));
		extraData.Add("cancelMobileUrl", JToken.FromObject(cancelMobileUrl));
		extraData.Add("viewOptions", JToken.FromObject(viewOptions.ToString()));

		//---------------------------------------------------------------------------------
		// 설정한 주문정보들을 Json String 을 작성합니다.
		//---------------------------------------------------------------------------------
		jsonOrder.Add("sellerKey", JToken.FromObject(ConfigurationManager.AppSettings["pc_sellerKey"]));
		jsonOrder.Add("sellerOrderReferenceKey", JToken.FromObject(sellerOrderReferenceKey));
		if(!sellerOrderReferenceKeyType.Equals("")) { jsonOrder.Add("sellerOrderReferenceKeyType", JToken.FromObject(sellerOrderReferenceKeyType)); }
		if(!iCurrency.Equals("")) { jsonOrder.Add("currency", JToken.FromObject(iCurrency)); }
		//jsonOrder.Add("totalDeliveryFeeAmt", JToken.FromObject(totalDeliveryFeeAmt));
		jsonOrder.Add("totalPaymentAmt", JToken.FromObject(totalPaymentAmt));
		jsonOrder.Add("totalTaxfreeAmt", JToken.FromObject(totalTaxfreeAmt));
		jsonOrder.Add("totalTaxableAmt", JToken.FromObject(totalTaxableAmt));
		jsonOrder.Add("totalVatAmt", JToken.FromObject(totalVatAmt));
		if(!orderTitle.Equals("")) { jsonOrder.Add("orderTitle", JToken.FromObject(orderTitle)); }
		if(!serviceUrl.Equals("")) { jsonOrder.Add("serviceUrl", JToken.FromObject(serviceUrl)); }
		jsonOrder.Add("serviceUrlParam", JToken.FromObject(serviceUrlParam.ToString()));
		if(!returnUrl.Equals("")) { jsonOrder.Add("returnUrl", JToken.FromObject(returnUrl)); }
		jsonOrder.Add("returnUrlParam", JToken.FromObject(returnUrlParam.ToString()));
        if (payPageType == "PayDelay")
        {
            if (!nonBankbookDepositInformUrl.Equals("")) { jsonOrder.Add("nonBankbookDepositInformUrl", JToken.FromObject(nonBankbookDepositInformUrl)); }
        }
		jsonOrder.Add("orderMethod", JToken.FromObject(ConfigurationManager.AppSettings["pc_orderMethod"]));
		if(!orderChannel.Equals("")) { jsonOrder.Add("orderChannel", JToken.FromObject(orderChannel)); }

		if(!inAppYn.Equals("")) { jsonOrder.Add("inAppYn", JToken.FromObject(inAppYn)); }
		if(!individualCustomNoInputYn.Equals("")) { jsonOrder.Add("individualCustomNoInputYn", JToken.FromObject(individualCustomNoInputYn)); }
		if(!ConfigurationManager.AppSettings["pc_payMode"].Equals("")) { jsonOrder.Add("payMode", JToken.FromObject(ConfigurationManager.AppSettings["pc_payMode"])); }
		jsonOrder.Add("extraData", JToken.FromObject(extraData.ToString()));

        pu.Write_Log(jsonOrder.ToString());
        Result = pu.payco_reserve(jsonOrder.ToString());

        

    }
}
