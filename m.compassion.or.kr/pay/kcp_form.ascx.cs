﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using CommonLib;


public partial class pay_kcp_form : System.Web.UI.UserControl
{

    public void Show(StateBag state)
    {

        string ActionResult = "";
        string pay_method = "";
        switch (state["pay_method"].ToString())
        {
            default:
                ActionResult = "card";
                pay_method = "CARD";
                break;
            case "010000000000":
                ActionResult = "acnt";
                pay_method = "BANK";
                break;
            case "000010000000":
                ActionResult = "mobx";
                pay_method = "MOBX";
                break;

        }

        this.ViewState["ActionResult"] = ActionResult;
        this.ViewState["pay_method"] = pay_method;

        this.ViewState["site_cd"] = state["site_cd"];
        this.ViewState["used_card_YN"] = state["used_card_YN"];
        this.ViewState["used_card"] = state["used_card"];
        this.ViewState["good_name"] = state["good_name"];
        this.ViewState["good_mny"] = state["good_mny"];
        this.ViewState["order_item"] = state["order_item"] == null ? ""  : state["order_item"];

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 페이지별 처리
        this.ViewState["payPageType"] = state["payPageType"];
        this.ViewState["quotaopt"] = "0";
        this.ViewState["payInfo"] = "";
        // Option 을 3개 밖에 사용할 수 없어서 param_opt_2 에 overseas_card 와 payPageType 을 묶어서(구분자 | ) 사용함.
        this.ViewState["overseas_card"] = state["overseas_card"] == null ? "|" + state["payPageType"] : state["overseas_card"] + "|" + state["payPageType"];
        //this.ViewState["overseas_card"] = state["overseas_card"] == null ? "" : state["overseas_card"];
        this.ViewState["returnUrl"] = state["returnUrl"] == null ? "" : state["returnUrl"];
        this.ViewState["buyr_name"] = state["buyr_name"] == null ? "" : state["buyr_name"];

        string codeId = "", group = ""; 
        string payGroup = "";
        switch (this.ViewState["payPageType"].ToString())
        {
            case "Store":   // 스토어
                this.ViewState["orderId"] = state["ordr_idxx"];
                break;
            case "PayDelay":    // 지연된 후원금
                this.ViewState["buyr_name"] = state["user_name"];

                this.ViewState["payItem"] = state["payItem"];
                var payItem1 = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
                var payInfo1 = payItem1.data;
                this.ViewState["payInfo"] = payInfo1; // payInfo 의 구조는 후원결제에서 사용하는 포맷과 다르다. dataset
                this.ViewState["orderId"] = payItem1.orderId;

                this.ViewState["order_item"] = payInfo1;    // [jun.heo] 2017-12 : 결제 모듈 통합 - 지연된 후원금인 경우 order_item 변경
                this.ViewState["user_name"] = state["user_name"];   
                break;
            case "Temporary":   // 일시후원 : 특별한 모금 및 나머지
            default:
                var payItem = state["payItem"].ToString().ToObject<pay_item_session>();
                var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

                this.ViewState["failUrl"] = payInfo.extra.Decrypt().ToObject<Dictionary<string, object>>()["failUrl"];

                this.ViewState["payInfo"] = payItem.data;
                this.ViewState["orderId"] = payItem.orderId;

                var payInfo2 = payItem.data.ToObject<PayItemSession.Entity>();
                if (payInfo2 != null)
                {
                    codeId = payInfo2.codeId.EmptyIfNull();
                    group = payInfo2.group.EmptyIfNull();
                }

                payGroup = payInfo.group;
                break;
        }
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 페이지별 처리

        ph_content.Visible = true;

        //Pay Log 생성 (결제모듈 요청)
        string sponsorId = "", userName = "", mobile = "", sponsorUserId = "";
        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
            userName = sess.UserName;
            mobile = sess.Mobile;
            sponsorUserId = sess.UserId;
        }

        //string strLog = string.Empty;
        //strLog += "sponsorId:" + sponsorId + ", ";
        //strLog += "orderId:" + this.ViewState["orderId"].ToString() + ", ";
        //strLog += "good_mny:" + state["good_mny"] + ", ";
        //strLog += "codeId:" + codeId + " ";
        //strLog += "buyr_name:" + state["buyr_name"] + ", ";
        //strLog += "userName:" + userName + ", ";
        //strLog += "mobile:" + mobile + ", ";
        //ErrorLog.Write(HttpContext.Current, 604, strLog);

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제전 로그 생성
        string strLog = string.Empty;
        strLog += "SponsorID:" + sponsorId + ", ";
        strLog += "OrderID:" + this.ViewState["ordr_idxx"] + ", ";
        strLog += "UserID:" + sponsorUserId + ", ";
        strLog += "CampaignID:" + state["campaignId"] + ", ";
        strLog += "Group:" + group + ", ";
        strLog += "ResultCode:" + "" + ", ";
        strLog += "mid:" + "" + ", ";
        strLog += "tid:" + "" + ", ";
        strLog += "moid:" + "" + ", ";
        strLog += "amt:" + state["good_mny"] + ", ";
        strLog += "cardcode:" + "" + ", ";
        strLog += "cardname:" + "" + ", ";
        strLog += "cardbin:" + "" + ", ";
        strLog += "cardpoint:" + "" + ", ";
        strLog += "codeID:" + codeId + ", ";
        strLog += "buyr_name:" + state["buyr_name"] + ", ";
        strLog += "UserName:" + userName + ", ";
        strLog += "Mobile:" + mobile + ", ";
        strLog += "IsUser:" + state["buyr_name"] + ", ";    // 회원(회원명), 비회원, 무기명
        strLog += "codename:" + "" + ", ";
        strLog += "Childkey:" + "" + ", ";
        strLog += "ChildMasterID:" + "" + ", ";
        strLog += "month:" + "" + ", ";
        strLog += "frequency:" + "" + ", ";
        strLog += "TypeName:" + state["codeName"] + ", ";

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 비전트립인 경우 로그 추가
        if (state["payPageType"].ToString() == "VisionTrip")
        {
            strLog += "plandetail_id:" + this.ViewState["plandetail_id"] + ", ";
        }

        //-----------------------------------------------------------------------------
        // USER-AGENT 구분
        //-----------------------------------------------------------------------------
        string WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
        {
            WebMode = "MOBILE";
        }
        else
        {
            WebMode = "PC";
        }
        strLog += "WebMode:" + WebMode + ", ";

        strLog += "payPageType:" + state["payPageType"] + " ";  //[jun.heo] 2017-12 : 결제 모듈 통합 - Log 에 결제 종류 추가
        ErrorLog.Write(HttpContext.Current, 604, strLog);
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제전 로그 생성

        UserInfo user = new UserInfo();
        ErrorLog.Write(this.Context, 0, "선물금카드결제(kcp_form.ascx.cs) - SponsorID:" + user.SponsorID + ", Group:" + payGroup);
    }

    public void Hide()
    {

        ph_content.Visible = false;
    }

    protected override void OnLoad(EventArgs e)
    {

        base.OnLoad(e);

    }


}
