﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete_sponsor.aspx.cs" Inherits="pay_complete_sponsor" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/sponsor/item-child.ascx" TagPrefix="uc" TagName="child" %>
<%@ Register Src="/sponsor/item-special.ascx" TagPrefix="uc" TagName="sf" %>
<%@ Register Src="/sponsor/item-user-funding.ascx" TagPrefix="uc" TagName="uf" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script>

    <!-- 문희원 추가 -->
    $(document).ready(function () {
	        
	        var childmasterid = $("#<%= hfChildMasterID.ClientID%>").val();
	        var sponsorid = $("#<%= hfSponsorID.ClientID%>").val();
            // Global CommitmentID 생성 / No Money Hold 처리
	        if (childmasterid != '' && childmasterid != null && sponsorid != '' && sponsorid != null) {
	            $.get("/sponsor/tcpt.ashx?t=regCommit", { childMasterId: childmasterid, sponsorid: sponsorid }).success(function (r) {

                    // 후원자 경고 없이 진행
	                if (r.success) {
	                    //alert(r.message);
	                } else {
	                        //alert('1');
	                }
	            });
	        }

	});

	(function () {
		var app = angular.module('cps.page', []);
		app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		});

	})();

</script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<div class="wrap-sectionsub">
<!---->
    
    <div class="sponsor-payment">
    	
        <asp:PlaceHolder runat="server" ID="ph_regular_title" Visible="false">
            <strong class="txt-paycommit1"><asp:Literal runat="server" ID="typeName2_1" /> 신청이<br />정상적으로 완료되었습니다.</strong>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="ph_temporary_title" Visible="false">
            <strong class="txt-paycommit1"><asp:Literal runat="server" ID="typeName2" /> 신청이<br />정상적으로 완료되었습니다.</strong>
        </asp:PlaceHolder>

        <p class="txt-paycommit2">후원하신 정보는 <a class="color1" href="/my/sponsor/commitment/">마이컴패션&gt; 후원관리</a> 메뉴에서<br />
			확인하실 수 있습니다.</p>
        

		<uc:child runat="server" ID="view_child" Visible="false" />

		<uc:sf runat="server" ID="view_sf" Visible="false" />

		<uc:uf runat="server" ID="view_uf" Visible="false" />

        
		<asp:PlaceHolder runat="server" ID="ph_cdsp_child" Visible="false">
        <!--1:1 어린이 양육을 결제한 경우 나타나는 영역-->
        <div class="sponsorchild-commit sectionsub-margin1">
        	<strong class="txt-slogon">지금 바로 아이에게<br />첫 편지를 쓰러 가보시겠어요?</strong>
            1:1어린이양육을 시작하신 것을 축하드립니다.<br />아이에게 쓰는 편지는 아이가 정서적으로 사랑을 느끼는데 중요한 역할을 합니다.
            <div class="box">
            	<p class="txt-hello"><strong><asp:Literal runat="server" ID="userName"/> 후원자</strong>님 안녕하세요?</p>
                <span class="photo" style="background:url('<%:this.ViewState["pic"].ToString() %>') no-repeat"></span>
                <p class="txt-name">제 이름은<br /><asp:Literal runat="server" ID="childName" />(이)예요.</p>
                후원자님과 함께하게 되어서 너무 기뻐요. <br />
				저와 같이 편지를 해보시지 않으실래요?
            </div>
            <div class="wrap-bt">
            	<a class="bt-type7 fl" style="width:49%" href="/my/sponsor/gift-money/pay/?t=temporary&childMasterId=<%:this.ViewState["childmasterid"].ToString() %>">선물금 보내기</a>
                <a class="bt-type6 fr" style="width:49%" href="/my/letter/write/?c=<%:this.ViewState["childmasterid"].ToString() %>|<%:this.ViewState["childkey"].ToString() %>|<%:this.ViewState["name"].ToString() %>">첫 편지 쓰러 가기</a>
            </div>
        </div>
        </asp:PlaceHolder>

        <!--스탬프 발급 영역-->
        <div class="sponsorchild-stamp">
			<asp:PlaceHolder runat="server" ID="ph_first_sponsor" Visible="false">
        	<strong class="txt-slogon">첫 후원을 축하드립니다.</strong>
			</asp:PlaceHolder>

			<asp:PlaceHolder runat="server" ID="ph_stamp1" Visible="false">
            <p class="con"><%:this.ViewState["stampTitle"] .ToString()%> 스탬프가 발급되었습니다.<br />
            달성된 스탬프는 <a class="color1" href="/my/activity/stamp/">마이컴패션&gt;나의 참여/활동&gt;스탬프 투어</a> 페이지에서 확인하실 수 있습니다.</p>
            <div class="stamp <%:this.ViewState["stampId"] .ToString()%> on"></div>
			</asp:PlaceHolder>
			<asp:PlaceHolder runat="server" ID="ph_stamp2" Visible="false">
			<div class="tour">
            	다른 스탬프는 어떤 스탬프가 있는지 확인해보세요. 
                <a class="bt-type4" style="width:90px" href="/my/activity/stamp/">스탬프 투어</a>
            </div>
			</asp:PlaceHolder>
        </div>
        
        
        <div class="wrap-bt"><a class="bt-type6" style="width:100%" href="/">메인</a></div>
        
        
    </div>

    <!-- 문희원 추가 -->
    <asp:HiddenField runat="server" ID="hfSponsorID" />
    <asp:HiddenField runat="server" ID="hfChildMasterID" />
    
<!--//-->
</div>
</asp:Content>
