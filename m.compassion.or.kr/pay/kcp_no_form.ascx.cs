﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;


public partial class pay_kcp_no_form : System.Web.UI.UserControl {
	
	public void Show(StateBag state) {	
		this.ViewState["order_item"] = state["order_item"];

		ph_content.Visible = true;

        //Pay Log 생성 (결제모듈 요청)
        var sponsorId = "";
        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
        }
        string strLog = string.Empty;
        strLog += "sponsorId:" + sponsorId + ", ";
        strLog += "orderId:" + state["ordr_idxx"] + ", ";
        strLog += "good_mny:" + state["good_mny"] + " ";
        ErrorLog.Write(HttpContext.Current, 604, strLog);
    }

	public void Hide( ) {
		ph_content.Visible = false;
	}

	protected override void OnLoad(EventArgs e) {	
		base.OnLoad(e);	
	}
}
