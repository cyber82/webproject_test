﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="form.aspx.cs" Inherits="pay_kakaopay_form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="utf-8" lang="utf-8">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title></title>
    <style>
        .loading_box {width:80%;height:120px;padding:75px 15px 0 15px;font-size:13px;color:#929292;text-align:center;letter-spacing:-0.5px;border:1px solid #ededed;background:#fff url('/common/img/common/loading.gif') no-repeat center 25px;background-size:30px 30px;}
    </style>
    <script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
    
    <!-- OpenSource Library -->
    <script src="<%=RequestDealPaymentUrl %>/dlp/scripts/lib/easyXDM.min.js" type="text/javascript"></script>
    <script src="<%=RequestDealPaymentUrl %>/dlp/scripts/lib/json3.min.js" type="text/javascript"></script>

    <!-- DLP창에 대한 KaKaoPay Library -->
    <script type="text/javascript" src="<%=RequestDealApproveUrl %>/js/dlp/client/kakaopayDlpConf.js" charset="utf-8"></script>
    <script type="text/javascript" src="<%=RequestDealApproveUrl %>/js/dlp/client/kakaopayDlp.min.js" charset="utf-8"></script>

    <link href="https://pg.cnspay.co.kr:443/dlp/css/kakaopayDlp.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

    /**
    cnspay	를 통해 결제를 시작합니다.
    */
    function getKakaoLayerInfo() {
        //alert($('#kakaopay_layer').css('display'));

	//$('#kakaopay_layer').css('display','block');//.css('height','50%');

	//alert($('#easyXDM_dlp_provider').contents().html());
	//alert(1);
	//alert($('#kakaopay_layer').css('display'));
	//alert($('#ph_content').css('display'));
	//$('#ph_content').css('display','block');
    }

    function cnspay() {
        
	//setTimeout("getKakaoLayerInfo();", 5000);
        // TO-DO : 가맹점에서 해줘야할 부분(TXN_ID)과 KaKaoPay DLP 호출 API
        // 결과코드가 00(정상처리되었습니다.)
        if (document.payForm.resultCode.value == '00') {
            // TO-DO : 가맹점에서 해줘야할 부분(TXN_ID)과 KaKaoPay DLP 호출 API
            kakaopayDlp.setTxnId(document.payForm.txnId.value);
            
            //kakaopayDlp.setChannelType('WPM', 'TMS'); // PC결제
            kakaopayDlp.setChannelType('MPM', 'WEB'); // 모바일 웹(브라우저)결제
            //kakaopayDlp.addRequestParams({ MOBILE_NUM : '010-1234-5678'}); // 초기값 세팅
//setTimeout(function() {
//alert($("#easyXDM_dlp_provider").contents().find("#callAppBtn").length);
//$('#kakaopay_layer').css('display','block');
//$("#easyXDM_dlp_provider").contents().find("#callAppBtn").click();
//kakaopayDlp.callDlp('kakaopay_layer', document.payForm, submitFunc);
//}, 2000);
            //alert(1);
            //alert(321);
kakaopayDlp.callDlp('kakaopay_layer', document.payForm, submitFunc);
//alert(321);
            

		
        } else {
            alert('[RESULT_CODE] : ' + document.payForm.resultCode.value + '\n[RESULT_MSG] : ' + document.payForm.resultMsg.value);
        }

    }

    function getTxnId() {
	
        // form에 iframe 주소 세팅
        document.payForm.target = "txnIdGetterFrame";
        //[jun.heo] 2017-12 : 결제 모듈 통합
        document.payForm.action = "/pay/kakaopay/GetTxnId";
        //document.payForm.action = "/sponsor/pay/temporary/kakaopay/GetTxnId";
        document.payForm.acceptCharset = "utf-8";
        if (payForm.canHaveHTML) { // detect IE
            document.charset = payForm.acceptCharset;
        }

        // post로 iframe 페이지 호출
        document.payForm.submit();



        // payForm의 타겟, action을 수정한다
        document.payForm.target = "hd_ex_frm";
        //[jun.heo] 2017-12 : 결제 모듈 통합
        document.payForm.action = "/pay/kakaopay/KakaopayLiteResult";
        //document.payForm.action = "/sponsor/pay/temporary/kakaopay/KakaopayLiteResult";
        document.payForm.acceptCharset = "utf-8";
        if (payForm.canHaveHTML) { // detect IE
            document.charset = payForm.acceptCharset;
        }
        // getTxnId.jsp의 onload 이벤트를 통해 cnspay() 호출
    }

    var submitFunc = function cnspaySubmit(data) {
	//alert('/'+String(data.RESULT_CODE)+'/');
        if (data.RESULT_CODE === '00') {
            loading.show();
        	alert("결제가 진행 중입니다. 정상적으로 후원신청이 완료될 수 있도록 잠시만 기다려 주세요");
            // 매뉴얼 참조하여 부인방지코드값 관리

            document.payForm.submit();
            
        } else if (data.RESULT_CODE === 'KKP_SER_002') {
            // X버튼을 눌렀을 때의 이벤트 처리 코드 등록
            //alert('[RESULT_CODE] : ' + data.RESULT_CODE + '\n[RESULT_MSG] : ' + data.RESULT_MSG);
            window.history.go(-2);

        } else {
            //alert('[RESULT_CODE] : ' + data.RESULT_CODE + '\n[RESULT_MSG] : ' + data.RESULT_MSG);
        }
    };

    var loading = {
        show: function (msg) {

            $("#loading_bg , #loading_container").bind("touchmove", function (e) {
                e.preventDefault();
                return false;
            })

            /*
            $(window).resize(function () {
                loading._resize();
            });
            */


            $("#loading_bg").css({ opacity: 0, display: "block" });
            $("#loading_bg").animate({
                opacity: 0.6
            }, 'fast', function () {
                $("body").css({ overflow: "hidden" })
                var container = $("#loading_container");

                if (msg)
                    $("#loading_container .msg").html(msg);
                else {
                    $("#loading_container .msg").html("로딩 중입니다.<br/>잠시만 기다려주세요.");	// 임시 , 이미지로 처리예정
                }
                container.show();

            })


        }
    };

	$(function () {
	    
		if ($("#noname").length > 0 && $("#noname").prop("checked")) {
			$("#user_name").val("무기명");
		}
		
		$("#buyr_name").val($("#user_name").val());

	    setTimeout("getTxnId();", 300);
	})
	</script>
</head>
<body>
    <!-- 로딩 -->
	<div style="width:100%;height:100%;position:fixed;left:0;top:0;z-index:100990;opacity:0.0;background:#000;display:none" id="loading_bg"></div>
	<div class="loading_box" style="position:fixed;left:10%;top:40%;z-index:100991;display:none; width:72%; height:70px;" id="loading_container">
		<span class="msg">잠시만 기다려주세요.</span>
	</div>
	<!--//-->
    <div id="kakaopay_layer"  style="display: none"></div>
    
    <div style="display:none">
    <iframe id="hd_ex_frm" name="hd_ex_frm"></iframe>
    <form name="payForm" id="payForm" action="KakaopayLiteResult" accept-charset="utf-8" method="post">
    






	<input type="hidden" id="ordr_idxx" name="ordr_idxx" value="<%:this.ViewState["orderId"].ToString() %>" />
	
	<input type='hidden' id="payInfo" name='payInfo'      value='<%:this.ViewState["payInfo"].ToString() %>'>
	<input type='hidden' id="buyr_name" name='buyr_name'      value=''>
	<input type='hidden' id="month" name='month'      value='<%:this.ViewState["month"].ToString() %>'>
	<input type="hidden" id="good_name" name="good_name" value="<%:this.ViewState["good_name"].ToString() %>"/>
	<input type="hidden" id="good_mny" name="good_mny" value="<%:this.ViewState["good_mny"].ToString() %>" maxlength="9"/>
    <input type="hidden" id="payAgain" name="payAgain" value="<%:this.ViewState["payAgain"].ToString() %>"/>

<input type='hidden' id="returnUrl" name='returnUrl' value='<%:this.ViewState["returnUrl"].ToString() %>'>
	<input type='hidden' id="jumin" name='jumin' value=''>
	<input type='hidden' id="ci" name='ci' value=''>
	<input type='hidden' id="di" name='di' value=''>
	<input type="hidden" id="firstPay" name="firstPay" value="" maxlength="9" />

    <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
    <input type="hidden" id="payPageType" name="payPageType" value="<%:this.ViewState["payPageType"].ToString() %>" />

        <!-- 결제 파라미터 목록 -->
        <b>결제 변수 목록(매뉴얼 참조)</b><br  />
        (*) 필수
        <ul>
            <li>(*)결제수단 : <input type="checkbox" name="PayMethod" value="KAKAOPAY" checked="checked"/>KAKAOPAY 고정</li>
            <li>(*)상품명 : <input name="GoodsName" type="text" value="<%:this.ViewState["good_name"].ToString() %>"/></li>
            <li>(*)상품가격 : <input name="Amt" type="text" value="<%=Amt%>"/></li>
            <li>공급가액 : <input name="SupplyAmt" type="text" value="<%=Amt%>"/></li>
            <li>부가세 : <input name="GoodsVat" type="text" value="0"/></li>
            <li>봉사료 : <input name="ServiceAmt" type="text" value="0"/></li>
            <li>(*)상품갯수 : <input name="GoodsCnt" type="text" value="1" readonly="readonly" style="background-color: #e2e2e2;" />고정</li>
            <li>(*)가맹점ID : <input name="MID" type="text" value="<%=MID%>" /></li>
            <li>(*)인증플래그 : <input name="AuthFlg" type="text" value="10" readonly="readonly" style="background-color: #e2e2e2;" /> 고정</li>
            <li>(*)EdiDate : <input name="EdiDate" type="text" value="<%=EdiDate %>" readonly="readonly" style="background-color: #e2e2e2;"/></li>
            <li>(*)EncryptData : <input name="EncryptData" type="text" value="<%=Hash_String %>" readonly="readonly" style="background-color: #e2e2e2;"/></li>
            <li>구매자 이메일 : <input name="BuyerEmail" type="text" value=""/></li>
            <li>(*)구매자명 : <input name="BuyerName" type="text" value="<%:this.ViewState["good_name"].ToString() %>"/></li>

        </ul>
        <br />

        
        <!-- 인증 파라미터 목록 -->
        <b>인증 변수 목록(매뉴얼 참조)</b><br />
        <ul>
            <li>상품제공기간 플래그 : <input name="offerPeriodFlag" type="text" value="Y"/></li>
            <li>상품제공기간 : <input name="offerPeriod" type="text" value="제품표시일까지"/></li>
            <li>(*)인증구분 : <input type="text" name="certifiedFlag" value="CN" readonly="readonly" style="background-color: #e2e2e2;" /> 고정</li>
            <li>(*)거래통화 : <input type="text" name="currency" value="KRW" readonly="readonly" style="background-color: #e2e2e2;" /> 고정</li>
            <li>(*)가맹점 암호화키 : <input type="text" name="merchantEncKey" value="<%=MerchantEncKey%>" /></li>
            <li>(*)가맹점 해쉬키 : <input type="text" name="merchantHashKey" value="<%=MerchantHashKey%>" /></li>
            <li>(*)TXN_ID 요청UR : <input type="text" name="requestDealApproveUrl" value="<%=RequestDealApproveUrl + RequestDealPage%>" /></li>
            <li>
            (*)결제요청타입 :  
            <select name ="prType">
                <option value="MPM">MPM</option>
                <option value="WPM" selected="selected">WPM</option>
            </select>
            <br />MPM : 모바일결제, WPM : PC결제
            </li>
            <li>
            (*)채널타입 :  
            <select name ="channelType">
                <option value="2">2</option>
                <option value="4" selected="selected">4</option>
            </select>
            <br />2: 모바일결제, 4: PC결제
            </li>
            <li>(*)가맹점 거래번호 : <input type="text" name="merchantTxnNum" value="<%=merchantTxnNum %>" /></li>
        </ul>
        <br />
        
        
        <!-- 인증 파라미터 중 할부결제시 사용하는 파라미터 목록 -->
        <!-- 파라미터 입력형태는 매뉴얼 참조  -->
        <b>할부결제시 선택변수 목록</b><br />
        옳은 값들을 넣지 않으면 무이자를 사용하지 않는것으로 한다.<br />
        
        <b>카드코드(매뉴얼 참조)</b><br />
        비씨:01, 국민:02, 외환:03, 삼성:04, 신한:06, 현대:07, 롯데:08, 한미:11, 씨티:11, <br /> 
        NH채움(농협):12, 수협:13, 신협:13, 우리:15, 하나SK:16, 주택:18, 조흥(강원):19, <br />
        광주:21, 전북:22, 제주:23, 해외비자:25, 해외마스터:26, 해외다이너스:27, <br />
        해외AMX:28, 해외JCB:29, 해외디스커버:30, 은련:34
        <ul>
            <li>카드선택 : <input type="text" name="possiCard" value="" /> ex) 06</li>
            <li>할부개월 : <input type="text" name="fixedInt" value="" /> ex) 03</li>
            <li>최대할부개월 : <input type="text" name="maxInt" value="" /> ex) 24</li>
            <li>
            무이자 사용여부 :
            <select class="require" name="noIntYN" onchange="javascript:noIntYNonChange();">
                <option value="Y">사용</option>
                <option value="N">사용안함</option>
            </select>
            </li>
            <!-- 결제수단코드 + 카드코드 + - + 무이자 개월 ex) CC01-02:03:05:09 -->
            <li>무이자옵션 : <input type="text" name="noIntOpt" value="" /> ex) CC01-02:03:05</li>
            <li>
            카드사포인트사용여부 : 
            <select name ="pointUseYn">
                <option value="N">카드사 포인트 사용안함</option>
                <option value="Y">카드사 포인트 사용</option>
            </select>
            </li>
            <li>금지카드설정 : <input type="text" name="blockCard" value=""/> ex) 01:04:11</li>
            <li>특정제한카드 BIN : <input type="text" name="blockBin" value=""/></li>
            </ul>
            
        <div class="btns">
            <a href="javascript:getTxnId();"><input type="button" value="결제 요청하기"  style="width:150px;height:40px" /></a>
        </div>
        
        <br />
        <br />
        <br />
        ======================================================================<br />
        <b>getTxnId 응답</b><br />
        resultcod<input id="resultCode" type="text" value=""/><br/>
        resultmsg<input id="resultMsg" type="text" value=""/><br/>
        txnId<input id="txnId" type="text" value=""/><br/>
        prDt<input id="prDt" type="text" value=""/><br/>
        <br/>
        <br/>
        <!-- DLP호출에 대한 응답 -->
        <b>DLP 응답</b><br />
        SPU : <input type="text" name="SPU" value=""/><br/>
        SPU_SIGN_TOKEN : <input type="text" name="SPU_SIGN_TOKEN" value=""/><br/>
        MPAY_PUB : <input type="text" name="MPAY_PUB" value=""/><br/>
        NON_REP_TOKEN : <input type="text" name="NON_REP_TOKEN" value=""/><br/>
        
        <!-- TODO :  LayerPopup의 Target DIV 생성 -->
        
        
        
    </form>
<iframe name="txnIdGetterFrame" id="txnIdGetterFrame" src=""  width="0" height="0"></iframe>
</div>
</body>
</html>
