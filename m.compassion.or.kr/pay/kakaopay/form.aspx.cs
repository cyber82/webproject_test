﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;
using LGCNS.CNSPay.Service;

public partial class pay_kakaopay_form : MobileFrontBasePage
{
    protected String RequestDealApproveUrl = String.Empty;
    protected String RequestDealPage = String.Empty;
    protected String Amt = String.Empty;
    protected String MID = String.Empty;
    protected String EncodeKey = String.Empty;
    protected String MerchantEncKey = String.Empty;
    protected String MerchantHashKey = String.Empty;
    protected String TargetUrl = String.Empty;
    protected String EdiDate = String.Empty;
    protected String Hash_String = String.Empty;
    protected String RequestDealPaymentUrl = String.Empty;
    protected String merchantTxnNum = String.Empty;
    protected String channelType = String.Empty;

    protected Boolean cvisible = false;

    public override bool IgnoreProtocol
    {
        get
        {
            return true;
        }
    }


    protected override void Page_Load(object sender, EventArgs e)
    {
        MerchantEncKey = ConfigurationManager.AppSettings["MerchantEncKey"];
        MerchantHashKey = ConfigurationManager.AppSettings["MerchantHashKey"];
        RequestDealApproveUrl = ConfigurationManager.AppSettings["RequestDealApproveUrl"];
        RequestDealPage = ConfigurationManager.AppSettings["RequestDealPage"];
        EncodeKey = ConfigurationManager.AppSettings["EncodeKey"];
        MID = ConfigurationManager.AppSettings["MID"];
        RequestDealPaymentUrl = ConfigurationManager.AppSettings["RequestDealPaymentUrl"];
        channelType = ConfigurationManager.AppSettings["channelType"];
        merchantTxnNum = DateTime.Now.ToString("yyyyMMddHH24mmss");

        //this.ViewState["orderItem"] = Request.Form["order_item"].ToString();


        this.ViewState["month"] = Request.Form["month"] == null ? "" : Request.Form["month"];     // 선물금결제시 사용		
        this.ViewState["good_name"] = Request.Form["good_name"];

        this.ViewState["payItem"] = Request.Form["payItem"];
        var payItem = Request.Form["payItem"].ToString().ToObject<pay_item_session>();
        var payInfo = payItem.data;
        this.ViewState["payInfo"] = payInfo;
        this.ViewState["orderId"] = payItem.orderId;

        //Amt = (payInfo.ToObject<PayItemSession.Entity>()).amount.ToString();
        Amt = Request.Form["good_mny"].ToString();
        this.ViewState["good_mny"] = Request.Form["good_mny"];

        this.ViewState["payAgain"] = Request.Form["payAgain"];

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 페이지별 처리
        this.ViewState["payPageType"] = Request.Form["payPageType"];
        this.ViewState["returnUrl"] = Request.Form["returnUrl"] == null ? "" : Request.Form["returnUrl"];	


        EdiDate = DateTime.Now.ToString("yyyyMMddHHmmss");  //전문생성일시
        String md_src = EdiDate + MID + Amt;  //위변조 처리

        CnsPayCipher chiper = new CnsPayCipher();
        Hash_String = chiper.SHA256Salt(md_src, EncodeKey);

        //Pay Log 생성 (결제모듈 요청)
        string sponsorId = "", userName = "", mobile = "";
        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
            userName = sess.UserName;
            mobile = sess.Mobile;
        }
        string codeId = "", group = "";
        var payInfo2 = payItem.data.ToObject<PayItemSession.Entity>();
        if (payInfo2 != null)
        {
            codeId = payInfo2.codeId.EmptyIfNull();
            group = payInfo2.group.EmptyIfNull();
        }
        string strLog = string.Empty;
        strLog += "sponsorId:" + sponsorId + ", ";
        strLog += "orderId:" + payItem.orderId + ", ";
        strLog += "good_mny:" + Request.Form["good_mny"] + ", ";
        strLog += "codeId:" + codeId + " ";
        strLog += "group:" + group + ", ";
        strLog += "buyr_name:" + Request.Form["buyr_name"].ToString().EmptyIfNull() + ", ";
        strLog += "userName:" + userName + ", ";
        strLog += "mobile:" + mobile + ", ";
        ErrorLog.Write(HttpContext.Current, 600, strLog);
    }
}