﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="kcp_form.ascx.cs" Inherits="pay_kcp_form" %>
<asp:PlaceHolder runat="server" ID="ph_content" Visible="false">

	<script type="text/javascript">

		$(function () {

			if ($("#noname").length > 0 && $("#noname").prop("checked")) {
				$("#user_name").val("무기명");
			}

            $("#buyr_name").val($("#user_name").val());

            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
            if ($("#payPageType").val() == "Temporary") {
                // APP 에서 새창에서 웹뷰를 띄우기위해 get 으로 기본정보를 넘기고 form.aspx 에서 다시 submit 한다.
                $("#ex_frm").attr('method', 'get');
            }
            else {
                $("#ex_frm").attr('method', 'post');
            }
            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분

			$("#ex_frm").submit();
			
		})
	</script>
		<div style="display:none">
			<!-- APP 에서 새창에서 웹뷰를 띄우기위해 get 으로 기본정보를 넘기고 form.aspx 에서 다시 submit 한다. -->
    <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
	<form id="ex_frm" name="ex_frm" method="get" action="/pay/kcp/form">
    <%--<form id="ex_frm" name="ex_frm" method="get" action="/sponsor/pay/temporary/kcp/form">--%>

		<!-- KCP -->
		<input type="hidden" name="ActionResult" value="<%:this.ViewState["ActionResult"].ToString() %>" />
		<input type="hidden" name="ordr_idxx" value="<%:this.ViewState["orderId"].ToString() %>" />
		<input type="hidden" name="good_name" value="<%:this.ViewState["good_name"].ToString() %>"/>
		<input type="hidden" name="good_mny" value="<%:this.ViewState["good_mny"].ToString() %>" />
		<input type="hidden" id="buyr_name" name="buyr_name" value="<%:this.ViewState["buyr_name"].ToString() %>"/>
		<input type="hidden" name="pay_method" value="<%:this.ViewState["pay_method"].ToString() %>" />
		<input type="hidden" name="site_cd" value="<%:this.ViewState["site_cd"].ToString() %>" />
		<input type="hidden"name="used_card_YN" value="<%:this.ViewState["used_card_YN"].ToString() %>"/>
		<input type="hidden"name="used_card" value="<%:this.ViewState["used_card"].ToString() %>"/>
		
	    <input type="hidden" name="param_opt_1" value="<%:this.ViewState["order_item"].ToString() %>"/>

		<input type='hidden' name='action' value='move'>

         <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
        <input type='hidden' name='payPageType' value='<%:this.ViewState["payPageType"].ToString() %>'>
        <input type='hidden' name='param_opt_2' value='<%:this.ViewState["overseas_card"].ToString() %>'>   <%--Option 을 3개 밖에 사용할 수 없어서 overseas_card 와 payPageType 을 묶어서(구분자 | ) 사용함.--%>
		<input type='hidden' name='param_opt_3' value='<%:this.ViewState["returnUrl"].ToString() %>'>

        <input type='hidden' name='payPageType' value='<%:this.ViewState["payPageType"].ToString() %>'>
		
		<!--// KCP -->
	</form>
		</div>
</asp:PlaceHolder>