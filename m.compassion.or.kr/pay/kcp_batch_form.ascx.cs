﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using CommonLib;


public partial class pay_kcp_batch_form : System.Web.UI.UserControl {
	
	public void Show( StateBag state) {
		this.ViewState["site_cd"] = state["site_cd"];
		this.ViewState["user_name"] = state["user_name"];
		
		this.ViewState["payItem"] = state["payItem"];
		var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();
		this.ViewState["payInfo"] = payItem.data;
		this.ViewState["orderId"] = payItem.orderId;
		this.ViewState["good_name"] = "정기결제";
		this.ViewState["good_mny"] = state["good_mny"] == null ? "" : state["good_mny"];

		var extra = payInfo.extra.Decrypt().ToObject<Dictionary<string, object>>();
		this.ViewState["motiveCode"] = extra["motiveCode"].ToString();
		this.ViewState["motiveName"] = extra["motiveName"].ToString();

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = state["payPageType"];

        //이종진 2018-03-12 - 결제실패 시, pay-again 페이지로 가기위한 url을 들고다님
        this.ViewState["againUrl"] = state["againUrl"] == null ? "" : state["againUrl"];

        ph_content.Visible = true;

		// sponsorID가 없는 상태에서 해외회원으로 체크하고 결제하는 경우 sponsorID로 ensure 되지 않기때문에,
		// 다시한번 ensure 한다.
		if(payInfo.type == PayItemSession.Entity.enumType.CDSP) {
			var childAction = new ChildAction();
			childAction.Ensure(payInfo.childMasterId);
		}

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제전 로그 생성
        //Pay Log 생성 (결제모듈 요청)
        string sponsorId = "", userName = "", mobile = "", sponsorUserId = "";
        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
            userName = sess.UserName;
            mobile = sess.Mobile;
            sponsorUserId = sess.UserId;
        }

        string codeId = "", group = "", campaignId = "", good_mny = "", code_name = "", month = "", frequency = "", childKey = "", childMasterId = "", TypeName = "";

        codeId = payInfo.codeId.EmptyIfNull();
        group = payInfo.group.EmptyIfNull();
        campaignId = payInfo.campaignId.EmptyIfNull();
        good_mny = payInfo.amount.ToString().EmptyIfNull();
        code_name = payInfo.codeName.EmptyIfNull();
        month = payInfo.month.ToString().EmptyIfNull();
        frequency = payInfo.frequency.EmptyIfNull();
        childKey = payInfo.childKey.EmptyIfNull();
        childMasterId = payInfo.childMasterId.EmptyIfNull();
        TypeName = payInfo.TypeName.EmptyIfNull();

        string strLog = string.Empty;
        strLog += "SponsorID:" + sponsorId + ", ";
        strLog += "OrderID:" + payItem.orderId + ", ";
        strLog += "UserID:" + sponsorUserId + ", ";
        strLog += "CampaignID:" + campaignId + ", ";
        strLog += "Group:" + group + ", ";
        strLog += "ResultCode:" + "" + ", ";
        strLog += "mid:" + "" + ", ";
        strLog += "tid:" + "" + ", ";
        strLog += "moid:" + "" + ", ";
        strLog += "amt:" + good_mny + ", ";
        strLog += "cardcode:" + "" + ", ";
        strLog += "cardname:" + "" + ", ";
        strLog += "cardbin:" + "" + ", ";
        strLog += "cardpoint:" + "" + ", ";
        strLog += "codeID:" + codeId + ", ";
        strLog += "buyr_name:" + userName + ", ";
        strLog += "UserName:" + userName + ", ";
        strLog += "Mobile:" + mobile + ", ";
        strLog += "IsUser:" + userName + ", ";    // 회원(회원명), 비회원, 무기명
        strLog += "codename:" + code_name + ", ";
        strLog += "Childkey:" + childKey + ", ";
        strLog += "ChildMasterID:" + childMasterId + ", ";
        strLog += "month:" + month + ", ";
        strLog += "frequency:" + frequency + ", ";
        strLog += "TypeName:" + TypeName + ", ";

        //-----------------------------------------------------------------------------
        // USER-AGENT 구분
        //-----------------------------------------------------------------------------
        string WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
        {
            WebMode = "MOBILE";
        }
        else
        {
            WebMode = "PC";
        }
        strLog += "WebMode:" + WebMode + ", ";

        strLog += "payPageType:" + state["payPageType"] + " ";  //[jun.heo] 2017-12 : 결제 모듈 통합 - Log 에 결제 종류 추가
        ErrorLog.Write(HttpContext.Current, 604, strLog);
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제전 로그 생성

    }

    public void Hide() {

		ph_content.Visible = false;
	}

	protected override void OnLoad(EventArgs e) {
		
		base.OnLoad(e);
		
	}


}
