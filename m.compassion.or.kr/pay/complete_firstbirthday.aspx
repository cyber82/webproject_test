﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete_firstbirthday.aspx.cs" Inherits="pay_complete_firstbirthday" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script>

</script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<div class="wrap-sectionsub">
	<!---->
    
		<div class="sponsor-special">
    	
			<div class="msg-applycommit">
        		<strong class="txt-msg1">첫생일 첫나눔 신청이<br />완료되었습니다.</strong>
				첫 생일 첫 나눔을 통해 기부해주신 후원금은  엄마와 아기들의 생명을 살리는 <span class="color1">태아·영아생존 후원</span>에 귀하게 사용됩니다.
				<p class="txt-msg2">생명을 살리는 소중한 일에 동참해주셔서 감사합니다.</p>
			</div>
        
			<div class="wrap-bt"><a class="bt-type6" style="width:100%" href="/">메인</a></div>
        
        
		</div>
    
	<!--//-->
	</div>
</asp:Content>
