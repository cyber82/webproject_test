﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="customer_qna_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script type="text/javascript">
        $(function () {
            var uploader = attachUploader("btn_file_path");
            uploader._settings.data.fileDir = $("#upload_root").val();
            uploader._settings.data.fileType = "file";

            $("#b_content").textCount($("#content_count"), { limit: 2000 });

            $("#phone").mobileFormat();

            $("#qna_type").change(function () {
                if ($(this).val() != "") {
                    $("#msg_qna").hide();
                }
            })

            $("#b_title").keyup(function () {
                if ($("#b_title").val() != "") {
                    $("#msg_title").hide();
                }
            })

            $("#b_content").keyup(function () {
                if ($("#b_content").val() != "") {
                    $("#msg_content").hide();
                }
            })


            $("#email").keyup(function () {
                if ($("#email").val() != "") {
                    $("#msg_email").hide();
                }
            })


            $("#phone").keyup(function () {
                if ($("#phone").val() != "") {
                    $("#msg_phone").hide();
                }
            })
        });

        var attachUploader = function (button) {
            return new AjaxUpload(button, {
                action: '/common/handler/upload',
                responseType: 'json',
                onChange: function () {
                },
                onSubmit: function (file, ext) {
                    this.disable();
                },
                onComplete: function (file, response) {

                    this.enable();

                    if (response.success) {
                        //$("#file_path").val(response.name);
                        $("#file_path").val(response.name.replace(/^.*[\\\/]/, ''));
                        $("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
                        //	$(".temp_file_size").val(response.size);

                    } else
                        alert(response.msg);
                }
            });
        }


        var onSubmit = function () {
            if ($("#qna_type").val() == "") {
                $("#msg_qna").html("문의 유형을 선택하세요").show();
                return false;
            }
            if ($("#b_title").val() == "") {
                $("#msg_title").html("제목을 입력하세요").show();
                return false;
            }
            if ($("#b_title").val().length < 4) {
                $("#msg_title").html("  제목은 최소 4~40자 이내로 입력해주세요").show();
                return false;
            }
            if ($("#b_content").val() == "") {
                $("#msg_content").html("문의내용을 입력하세요").show();
                return false;
            }

            if ($("#b_content").val().length < 10) {
                $("#msg_content").html("문의 내용은 최소 10자 이상 입력해주세요").show();
                return false;
            }
            //if ($("#email").val() == "") {
            //    $("#msg_email").html("이메일을 입력하세요").show();
            //    return false;
            //}

            if (($("#email").val() == "" || $("#email").val() == null) == false
                && !/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($("#email").val())) {
                $("#msg_email").html("이메일 형식이 맞지 않습니다. 다시 확인해주세요.").show();
                $("#email").focus();
                return false;
            }
            //if ($("#phone").val() == "") {
            //    $("#msg_phone").html("휴대폰번호를 입력하세요").show();
            //    return false;
            //}

            if (($("#phone").val() == "" || $("#phone").val() == null) == false
                && !/(01[016789])[-](\d{4}|\d{3})[-]\d{4}$/g.test($("#phone").val())) {
                $("#msg_phone").html("휴대폰 번호 양식이 맞지 않습니다. 10자 이상의 숫자만 입력 가능합니다.").show();
                $("#phone").focus();
                return false;
            }

            // 중복 등록을 막기 위해 Click 시 버튼 숨김
            $("#btnAdd").hide();
            loading.show("글을 등록 중입니다");

        }
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" runat="server" id="upload_root" value="" />
    <input type="hidden" runat="server" id="file_path" value="" />


    <div class="wrap-sectionsub">
<!---->
    
    <div class="etc-consult">
    	
		<div class="wrap-tab3 sectionsub-margin1">
            <a style="width:50%" href="/customer/faq/">FAQ</a>
            <a style="width:50%" class="selected" href="/customer/qna/">1:1 문의</a>
        </div>

        <p class="txt-caption2">문의주신 내용은 빠른 시간 내 답변 드리도록 하겠습니다.</p>
        <fieldset class="frm-input">
            <legend>1:1문의 정보 입력</legend>
            <div class="row-table">
                <div class="col-th" style="width:24%">문의유형</div>
                <div class="col-td" style="width:76%">
                    <asp:DropDownList style="width:100%" runat="server" ID="qna_type"></asp:DropDownList>
                	<p class="txt-error" id="msg_qna" style="display:none">필수 입력 항목입니다.</p>   <!--에러메시지 경우에만 출력-->
                </div>
            </div>
            <div class="row-table">
                <div class="col-th" style="width:24%">이름</div>
                <div class="col-td" style="width:76%"><span class="txt"><asp:Literal runat="server" ID="lbUserName" /></span></div>

            </div>
            <div class="row-table">
                <div class="col-th" style="width:24%">제목</div>
                <div class="col-td" style="width:76%">
                    <asp:TextBox runat="server" ID="b_title" maxlength="40" style="width:100%" placeholder="제목을 입력해 주세요"/>
                    <p class="txt-error" id="msg_title" style="display:none">필수 입력 항목입니다.</p>   <!--에러메시지 경우에만 출력-->
                </div>
            </div>
            <div class="row-table">
                <div class="col-th" style="width:24%">문의내용</div>
                <div class="col-td" style="width:76%">
                	<div class="frm-row box mt0">
                        <asp:TextBox TextMode="MultiLine" runat="server" ID="b_content" style="width:100%;height:100px" placeholder="문의하실 내용을 입력해 주세요" />
                        <p class="txt-byte"><em><span id="content_count">0</span></em>/2000자</p>
                    </div>
                        <p class="txt-error" id="msg_content" style="display:none">필수 입력 항목입니다.</p>   <!--에러메시지 경우에만 출력-->
                </div>
            </div>
            <div class="row-table">
                <div class="col-th" style="width:24%">사진첨부</div>
                <div class="col-td" style="width:76%">
                	 <span class="row pos-relative1">
                         <input type="text" runat="server" id="lb_file_path" readonly="readonly" class="fileInputHidden" style="width:100%;" placeholder="선택된 파일이 없습니다" />
                         <a href="#" class="bt-type10 pos-btn" style="width:50px" id="btn_file_path"><span>찾기</span></a>

					</span>
                    <p class="bu">첨부파일 용량을 2MB이하로 해주세요. 첨부파일은 이미지파일(jpg, jpeg, gif, png)만 가능합니다.</p>
                </div>
            </div>
            <div class="row-table">
                <div class="col-th" style="width:24%">이메일</div>
                <div class="col-td" style="width:76%">
                    <asp:TextBox runat="server" ID="email" MaxLength="100" style="width:100%" />
                    <p class="txt-error" id="msg_email" style="display:none">필수 입력 항목입니다.</p>   <!--에러메시지 경우에만 출력-->
                </div>
            </div>
            <div class="row-table">
                <div class="col-th" style="width:24%">휴대폰</div>
                <div class="col-td" style="width:76%">
                    <asp:TextBox runat="server" ID="phone" MaxLength="13" class="number_only" style="width:100%" />
                    <p class="txt-error" id="msg_phone" style="display:none">필수 입력 항목입니다.</p>   <!--에러메시지 경우에만 출력-->
                </div>
            </div>
		</fieldset>
        
        <div class="wrap-bt">
        	<a class="bt-type7 fl" style="width:49%" href="/customer/faq/">취소</a>
            <asp:LinkButton runat="server" ID="btnAdd" style="width:49%" OnClick="btnAdd_Click" OnClientClick="return onSubmit();" class="bt-type6 fr">확인</asp:LinkButton>
        </div>
        

        
    </div>
    
<!--//-->
</div>

    <!--
     아이디
    <asp:Literal runat="server" ID="lbUserId" />
    -->




</asp:Content>
