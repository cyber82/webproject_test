﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class customer_faq_default : MobileFrontBasePage {

	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}


	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);

		GetKeyword();

		GetCategory();
	}


	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
	}


	protected void GetKeyword()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.keyword.Where(p => p.k_type == "faq").OrderBy(p => p.k_order);
            var list = www6.selectQ<keyword>("k_type", "faq", "k_order");

            repeater_keyword.DataSource = list;
            repeater_keyword.DataBind();
        }
	}


	protected void GetCategory() {
		var list = StaticData.Code.GetList(this.Context).Where(p => p.cd_display == true && p.cd_group == "faq" && p.cd_key != "recruit").OrderBy(p => p.cd_order);

        repeater_category.Value = list.ToJson();


    }
}