﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="customer_faq_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript" src="/customer/faq/default.js"></script>
    <script type="text/javascript">
    </script>

    <script type="text/javascript">
        $(function () {
            // app 에서 새창으로 열린경우 FAQ만 남긴다.
            var app_ex = getParameterByName("app_ex");
            if (app_ex == "2") {
                $(".wrap-tab3").hide();
            }
        })
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="repeater_category" value="" />
    <div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <!---->

        <div class="etc-consult" id="l">

            <div class="wrap-tab3 sectionsub-margin1">
                <a style="width: 50%" class="selected" href="/customer/faq/">FAQ</a>
                <a style="width: 50%" href="/customer/qna/">1:1 문의</a>
            </div>


            <fieldset class="frm-search mt30">
                <legend>검색입력</legend>
                <input type="text" value="" placeholder="키워드를 입력해 주세요" name="k_word" id="k_word" ng-enter="search()" maxlength="30" />
                <span class="search" ng-click="search()">검색</span>
            </fieldset>
            <div class="keyword-hot">
                <asp:Repeater runat="server" ID="repeater_keyword">
                    <ItemTemplate>
                        <a ng-click="keyword({k_word:'<%#Eval("k_word") %>'})"><%#Eval("k_word") %></a><span class="bar">|</span>
                    </ItemTemplate>
                </asp:Repeater>

            </div>

            <p class="txt-caption">문의 유형</p>
            <div>
                <select style="width: 100%" ng-model="select_category" ng-change="getList({top:-1})" data-category="" ng-options="c.cd_key as c.cd_value for c in category">
                </select>


            </div>

            <ul class="list-consult mt20">
                <!--선택 경우 (li 부분에  class="selected"  추가)-->
                <li class="" ng-repeat="item in list" ng-click="toggle($event, item)">
                    <span class="question" onclick=""><em>{{item.f_type_name}}</em>{{item.f_question}}<span class="bu"></span></span>
                    <span class="answer" ng-bind-html="renderHtml(item.f_answer)">
                        <!-- 내용 -->
                    </span>
                </li>


                <!--검색없음-->
                <li class="no-result" ng-hide="list.length">검색 결과가 없습니다.</li>
            </ul>

            <paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>

            <div class="bottom-faq mt30">
                <div class="align-center"><strong>FAQ에서 원하는 답변을 찾지 못하셨나요?</strong></div>
                문의사항을 [1:1 문의하기] 를 통해 남겨주시면 답변드리겠습니다.<br />
                <br />
                더욱 빠른 답변을 원하시면 한국컴패션 대표번호 (02-740-1000/평일 9시~18시)로 연락 주시기 바랍니다.
            <div class="wrap-bt mt20"><a class="bt-type6" style="width: 120px" href="/customer/qna/">1:1 문의하기</a></div>
            </div>



        </div>

        <!--//-->
    </div>



</asp:Content>
