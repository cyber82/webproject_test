﻿(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, $sce, paramService) {
        $scope.requesting = false;
        $scope.total = -1;

        $scope.list = [];
        $scope.params = {
            page: 1,
            rowsPerPage: 10,
            top: -1
        };

        console.log(paramService.getParameterValues().s_type);

        $scope.category = $.parseJSON($("#repeater_category").val());
        $scope.select_category = $scope.params.s_type = paramService.getParameterValues().s_type == "" || !paramService.getParameterValues().s_type ? $scope.category[0].cd_key : paramService.getParameterValues().s_type;
        //console.log($scope.category)
        // 문의유형 선택
        //$scope.select_category = $scope.params.s_type = $scope.category[0].cd_key;

        // 검색
        
        $scope.params = $.extend($scope.params, paramService.getParameterValues());

        $scope.search = function (params) {

            $scope.params = $.extend($scope.params, params);
            $scope.params.s_type = ""
            $scope.params.k_word = $("#k_word").val();
            $("#k_word").blur();
            $scope.getList(params);


        };

        // 키워드
        $scope.keyword = function (params) {
            $scope.params = $.extend($scope.params, params);
            $("#k_word").val(params.k_word);
            
            $scope.getList(params);
            
        }

        // list
        $scope.getList = function (params) {

            // 카테고리 toggle
            if (params) {
                params.s_type = $scope.select_category;
                $scope.params.page = 1;
            }
            
            $scope.params = $.extend($scope.params, params);
            if ($scope.params.k_word) {

                $scope.params.s_type = "";
                $("#k_word").val($scope.params.k_word);
            }


            //$("#category li a[data-category=" + $scope.params.s_type + "]").addClass("on");
            console.log($scope.params)
            $http.get("/api/faq.ashx?t=list", { params: $scope.params }).success(function (result) {
                $scope.list = result.data;
                $scope.total = result.data.length > 0 ? result.data[0].total : 0;
            });
            if (params) scrollTo($("#l"));


        }

        // toggle
        $scope.toggle = function (event, item) {
            if ($(event.currentTarget).hasClass("selected")) {
                $(event.currentTarget).removeClass("selected");
            } else {
                $(event.currentTarget).addClass("selected");
            }
        }

        $scope.renderHtml = function (html) {
            return $sce.trustAsHtml(html);
        };

        $scope.getList();
    });

})();