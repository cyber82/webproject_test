﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="app-info.aspx.cs" Inherits="etc_app_info" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>


<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub">
        <!---->

        <div class="etc-appguide">

            <div class="top-slogon sectionsub-margin1">
                컴패션 후원자님을 위한<strong>특별한 컴패션 앱 !</strong>
            </div>

            <p class="txt-caption">컴패션 앱이 특별한 이유 3가지!</p>

            <p class="txt-caption2">1. 쉽고 빠른 편지 서비스</p>
            <div class="mb15">
                <img src="/common/img/page/etc/img-appguide2.jpg" alt="" width="100%" /></div>
            어린이에게 마음을 전하고 싶지만, 손 편지를 적어 우편으로 보내기엔 절차가 너무 복잡하셨나요? 이젠 앱에서 더욱 강화된 편지 서비스를 누려 보세요!<br />
            <br />
            리뉴얼된 앱에서는 보다 빠르게 사진 한 장, 메시지 한 줄로 편지를  전할 수 있는 ‘사진편지’ 기능도 추가되었어요.<br />
            <br />
            만약 편지를 적다가 급한 일이 생겼을 땐 ‘임시저장’기능으로 소중한 마음까지 저장할 수 있답니다!
        
        <p class="txt-caption2">2. 친절한 푸시 알림</p>
            <div class="mb15">
                <img src="/common/img/page/etc/img-appguide3.jpg" alt="" width="100%" /></div>
            후원 어린이의 새로운 정보가 등록되면 앱 푸시 알림으로 알려드려요. 나의 어린이가 얼만큼 자랐는지, 새로 찍은 사진은 어떤지 등록된 즉시 확인할 수 있어요.<br />
            <br />
            편지를 받은 후 깜빡하고 답장을 쓰지 않은 경우에도 잊지 않게 알림을 드리니, 놓치지 않고 함께할 수 있겠죠?
        
        <p class="txt-caption2">3. 컴패션의 다양한 캠페인/이벤트 참여</p>
            <div class="mb15">
                <img src="/common/img/page/etc/img-appguide4.jpg" alt="" width="100%" /></div>
            컴패션 밴드콘서트, 체험전, 공연 티켓 나눔 이벤트 등등 컴패션에서만 함께할 수 있는 다양한 모임에 참여해 보세요.<br />
            <br />
            새로운 캠페인/이벤트가 등록될 경우 앱 푸시로 알림을 드리니, 앱 푸시 알림은 켜두는 센스!<br />
            <br />
            모임에 참석하면 해당 모임에서만 발급되는 스탬프도 있으니, 차근차근 스탬프 투어도 함께 즐겨 보세요~
        
        <p class="txt-appstore mt30">지금 바로 앱스토어 or 구글플레이스토어에서<br />
            <strong>'컴패션'을 검색해 보세요!</strong></p>
            <ul class="list-appstore">
                <li><a href="/app/download?device=iphone">
                    <img src="/common/img/icon/applestore.png" alt="" />Download on the<br />
                    APP Store</a></li>
                <li><a href="/app/download?device=android">
                    <img src="/common/img/icon/googlestore.png" alt="" />Android app on<br />
                    Google play</a></li>
            </ul>
            

        </div>

        <!--//-->
    </div>

</asp:Content>
