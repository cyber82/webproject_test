﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="store_default" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/store/default.js"></script>
	<script src="/common/js/util/swiper.js"></script>
	<style>
		.1swiper-wrapper{height:290px;}
		.1wrap-thumbil .list-thumbil .photo img {height:190px;}
	</style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

 <div class="wrap-sectionsub"  ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
<!---->
    
    <div class="store-topslider sectionsub-margin1">
        <p class="txt-slogon"><strong>행복한 선물!</strong> 컴패션 스토어의 또 다른 후원의 방법</p>
        <div class="wrap-slider" style="width:100%;overflow:hidden;">
            <div class="swiper-wrapper">

				<asp:Repeater runat="server" ID="repeater_visual">
					<ItemTemplate>
						<div class="swiper-slide">
							<a href="<%#Eval("mp_image_link") %>" target="<%#Eval("mp_is_new_window").ToString() == "True" ? "_blank" : "_self" %>">
								<img src="<%#Eval("mp_image")%>" alt="<%#Eval("mp_title") %>" width="100%" />
							</a>
						</div>
					</ItemTemplate>
                 </asp:Repeater>

			</div>
            
            <div class="page-count">
            </div>
            <div class="page-prev" style="display:none;">이전보기</div>
            <div class="page-next" style="display:none;">다음보기</div>
            
        </div>
    </div>

	 
    <!-- 검색결과가 없을때 -->
    <div ng-show="total == 0">
		<div class="wrap-noresult">
			<strong class="txt-title">검색결과가 없습니다.</strong>
			<p class="txt-note">다른 상품을 검색해주세요.</p>
			<fieldset class="frm-search">
				<legend>상품검색입력</legend>
				<input type="text" id="keyword2" ng-model="params.keyword" ng-enter="searchResultNone()" value="" placeholder="상품을 검색해보세요" />
				<span class="search" ng-click="searchResultNone()">검색</span>
			</fieldset>
    		<div class="wrap-bt"><a class="bt-type6" ng-click="goProductList()">전체상품 보기</a></div>
		</div>    
    
		<!--스토어 다른 상품-->
		<div class="wrap-headline">
    		<h2 class="txt-head">스토어 다른 상품</h2>
		</div>
    
		<div class="wrap-thumbil">
    		<ul class="list-thumbil list-type2">
				<asp:Repeater runat="server" ID="repeater_randomProduct">
                    <ItemTemplate>
                        <li>
							<a ng-click='goView(<%#Eval("idx") %> , $event, \"<%#Eval("name") %>\", <%#Eval("gift_flag") %>)' class="box-block">
            					<span class="photo"><img src="<%# (Uploader.GetRoot(Uploader.FileGroup.image_shop) + Eval("name_img").ToString()).WithFileServerHost() %>" alt="" width="100%" /></span>
								<span class="info">
                					<span class="txt-title"><span class="textcrop-1row prdt-name"><%#Eval("name") %></span></span>
									<span class="txt-price">￦ {{<%#Eval("selling_price") %> | number:N0}}</span>
								</span>
							</a>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
			</ul>
		</div>
	</div>


	 <!-- 검색결과가 있을때  -->
    <div ng-show="total > 0">
		<!--상품검색-->
		<fieldset class="frm-search">
    		<legend>상품검색입력</legend>
    		<input type="text" id="keyword" ng-model="params.keyword" ng-enter="search()" value="" placeholder="상품을 검색해보세요" />
			<span class="search" ng-click="search()">검색</span>
		</fieldset>
    
		<!--tab-->
		<div class="wrap-tab">
    		<ul class="tab-type1">
        		<li class="tab_menu" ng-class="{'selected' : params.gift_flag == -1}" data-index="-1" ng-click="clickTabEvent( -1 , $event)" style="width:33%"><a href="#">전체상품</a></li>
				<li class="tab_menu" ng-class="{'selected' : params.gift_flag == 0}" data-index="0" ng-click="clickTabEvent( 0 , $event)" style="width:33%"><a href="#">일반상품</a></li>
				<li class="tab_menu" ng-class="{'selected' : params.gift_flag == 1}" data-index="1" ng-click="clickTabEvent( 1 , $event)" style="width:34%"><a href="#">어린이 선물</a></li>
			</ul>
		</div>
    
		<!--썸네일 리스트-->
		<div class="wrap-thumbil">
    		<ul class="list-thumbil">
        		<li ng-repeat="item in list">
					<a ng-click="goView(item.idx , $event, item.name, item.gift_flag)" class="box-block">
            			<span class="photo"><img ng-src="{{item.name_img}}" alt="" /></span>
						<span class="info">
                			<span class="txt-title"><span class="textcrop-1row prdt-name">{{item.name}}</span></span>
							<span class="txt-price">{{item.selling_price | number:N0}}원</span>
						</span>
					</a>
        		</li>
			
			</ul>
		</div>
    
		<paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>  

    </div>

    <div class="store-noticetop">
    	<ul>
        	<li><a href="#" ng-click="getPopup.show('manage' , $event)"><span class="txt1">컴패션 스토어는<br />어떻게 운영할까요 ?</span><img src="/common/img/page/store/img-article1.jpg" alt="" /></a></li>
            <li><a href="#" ng-click="getPopup.show('profit' , $event)"><span class="txt1">컴패션 스토어의 수익금은<br />어떻게 쓰이나요?</span><img src="/common/img/page/store/img-article2.jpg" alt="" /></a></li>
            <li><a href="#" ng-click="getPopup.show('gift' , $event)"><span class="txt1">후원어린이에게 선물을<br />어떻게 하나요?</span><img src="/common/img/page/store/img-article3.jpg" alt="" /></a></li>
            <li><a ng-click="notice.goPage($event)"><strong class="txt2">공지사항</strong>
            	<span class="txt3">{{notice.list[0].title}}</span>
            	<time class="txt4">{{notice.list[0].dtreg | date:'yyyy.MM.dd'}}</time>
            <img src="/common/img/page/store/img-article4.png" alt="" /></a></li>
        </ul>
    </div>
    
<!--//-->
</div>

</asp:Content>
