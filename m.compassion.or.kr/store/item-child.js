﻿$(function () {

    $page.init();

});

var $page = {

    processing: false,

    init: function () {



        $("#link").attr("href", "#");
        $("#link").click(function () {
            history.back();
            return false;
        });

        $("#userid").val(common.getUserId());

    },


}

var app = angular.module('cps.page', []);
app.controller("itemCtrl", function ($scope, $http, $filter, popup) {


    // 어린이선물

    $scope.modalChild = {

        total: 0,
        page: 1,
        rowsPerPage: 2,
        data: [],
        list: null,
        container: null,
        processing: false,
        total_ea: 0,
        total_amount: 0,
        child_count: 0,
        item_count: 0,
        title: "",
        image: "",
        instance: null,


        getChildren: function () {

            $http.get("/api/store.ashx?t=get_children", { params: {} }).success(function (r) {

                if (r.success) {
                    $scope.modalChild.container = $(".store-mychild");
                    $scope.modalChild.data = $.extend($scope.modalChild.data, r.data);

                    if (r.data.length < 1) {
                        alert("한국컴패션 후원자만 어린이에게 선물하실 수 있습니다.");
                        return;
                    }

                    $.each($scope.modalChild.data, function () {
                        this.birthdate = new Date(this.birthdate);
                        this.ea = 0;
                    })

                    $scope.modalChild.total = r.data.length;

                    $scope.modalChild.getList({ page: 1 });

                } else {
                    if (r.action == "not_sponsor") {
                        alert("한국컴패션 후원자만 어린이에게 선물하실 수 있습니다.");
                    } else {
                        alert(r.message);
                    }
                }
            });

        },

        getList: function (param) {
            $scope.modalChild.page = param.page;
            var begin = ($scope.modalChild.page - 1) * $scope.modalChild.rowsPerPage;
            var end = ($scope.modalChild.page) * $scope.modalChild.rowsPerPage;

            $scope.modalChild.list = $scope.modalChild.data.slice(begin, end);
        },

        calculate: function () {

            // 선택된 상품수
            $scope.modalChild.item_count = $scope.modalChild.total_ea.format();

            var obj = $scope.modalChild.container;
            var price = parseInt($("#hd_price").val());
            var ea = $scope.modalChild.total_ea;
            var opt_price = obj.find("[data-id=option] option:selected").data("price") || 0;


            $scope.modalChild.total_amount = ((opt_price + price) * ea);
            //console.log($scope.modalChild.total_amount);

            // 선택된 어린이수
            /*
            var count = 0;
            $.each(obj.find("[data-id=ea]"), function () {
                if ($(this).val() != "0") {
                    count++;
                }
            })
			*/

            var count = 0;
            $.each($scope.modalChild.data, function () {
                if (this.ea > 0) {
                    count++;
                }
            });

            $scope.modalChild.child_count = count;

        },

        setBasket: function (action) {

            var obj = $scope.modalChild.container;

            var json = {};
            json.user_id = $("#hd_user_id").val();
            json.item_id = $("#hd_item_id").val();
            json.option_name = obj.find("[data-id=option] option:selected").text();
            json.option = obj.find("[data-id=option]").val();
            json.option_price = obj.find("[data-id=option] option:selected").data("price") || 0;
            json.quantity = $scope.modalChild.total_ea;

            var children = [];

            $.each($scope.modalChild.data, function () {
                if (this.ea > 0) {
                    var child = {};
                    child.childId = this.childkey;
                    child.childName = this.namekr;
                    child.ea = this.ea;
                    children.push(child);
                }
            })

            json.children = children;

            var jsonStr = $.toJSON(json);

            //	console.log(json);
            //	return;
            $http.post("/api/store.ashx", { t: "set_basket", data: jsonStr }).success(function (r) {

                $scope.modalChild.processing = false;
                //	console.log(r);
                if (r.success) {
                    if (action == "cart") {
                        if (!confirm('장바구니로 이동하시겠습니까?')) {
                            //$scope.modalChild.hide();
                            return;
                        }
                    }

                    location.href = '/store/cart';

                } else {
                    alert(r.message);
                }

            });

            return;
        },

        buy: function (action) {

            if ($scope.modalChild.processing) return false;

            var index = $('[data-id=option] option').index($('[data-id=option] option:selected'));
            if ($('[data-id=option] option').size() > 1 && index < 1) {
                alert('옵션을 선택해주세요');
                $("[data-id=option]").focus();
                return false;
            }

            if ($scope.modalChild.total_ea < 1) {
                alert("수량을 선택해주세요");
                return false;
            }

            if ($scope.modalChild.child_count > 50) {
                alert("한번에 50명 이하로만 가능합니다.");
                return false;
            }

            //	$scope.modalChild.processing = true;
            $scope.modalChild.setBasket(action);

        },

        plusEA: function ($event, item) {
            $event.preventDefault();

            var obj = $scope.modalChild.container;

            var group = item.childkey;
            var inventory = parseInt($("#hd_inventory").val());
            var total = $scope.modalChild.total_ea + 1;
            var ea = parseInt(obj.find("[data-id=ea][data-group='" + group + "']").val()) + 1;
            item.ea = ea;
            if (total > inventory) {
                alert('재고량이 부족 합니다.');
                return false;
            }

            $scope.modalChild.total_ea = total;
            obj.find("[data-id=ea][data-group='" + group + "']").val(ea);
            $scope.modalChild.calculate();
        },

        minusEA: function ($event, item) {
            $event.preventDefault();

            var obj = $scope.modalChild.container;
            var group = item.childkey;
            var total = $scope.modalChild.total_ea - 1;
            var ea = parseInt(obj.find("[data-id=ea][data-group='" + group + "']").val()) - 1;

            if (ea < 0) {
                return;
            }

            item.ea = ea;
            $scope.modalChild.total_ea = total;
            obj.find("[data-id=ea][data-group='" + group + "']").val(ea);
            $scope.modalChild.calculate();

        }


    }
    // $scope.modalChild.init();
    $scope.modalChild.getChildren();






});


