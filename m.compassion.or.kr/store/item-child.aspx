﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="item-child.aspx.cs" Inherits="item_child" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script src="/store/item-child.js?v=1.0"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" id="key" runat="server" />
	<input type="hidden" id="hd_user_id" runat="server" />
	<input type="hidden" id="hd_item_id" runat="server" />
    <input type="hidden" id="buy_check" runat="server" />
	<input type="hidden" id="hd_title" runat="server" />
	<input type="hidden" id="hd_price" runat="server" />
	<input type="hidden" id="hd_total_amount" runat="server" />
	<input type="hidden" id="hd_delivery_fee" runat="server" />
	<input type="hidden" id="hd_inventory" runat="server" />

<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="itemCtrl">
<!---->
    <div class="store-mychild">
    	
        <strong class="txt-slogon">나의 어린이에게 선물하고 싶어요.</strong>
    	<div class="box-gift">
        	<span class="photo"><img runat="server" id="image" src="/common/img/temp/temp9.jpg" alt="" /></span>
            <p class="txt-product"><asp:Literal runat="server" ID="title" /></p>
			<asp:DropDownList runat="server" ID="option" style="width:60%" data-id="option">
			</asp:DropDownList>
        </div>
        
        <p class="txt-title">나의 어린이 선택</p>
        <div class="box-child" ng-repeat="item in modalChild.list">
        	<p class="txt-name">{{item.namekr}}</p>
            <p class="txt-info">
            	<em>ID</em>{{item.childkey}}&nbsp;&nbsp;&nbsp;&nbsp;
                <em>성별</em>{{item.gender}}&nbsp;&nbsp;&nbsp;&nbsp;
                <em>생일</em>{{item.birthdate | date:'yyyy.MM.dd'}}
            </p>
            <span class="count">
            	<a class="count-minus" data-group="{{item.childkey}}"  ng-click="modalChild.minusEA($event ,item)">수량 빼기</a>
                <input type="number" id="quantity" value="{{item.ea}}" data-id="ea" ng-value="{{item.ea}}" data-group="{{item.childkey}}" readonly />
                <a class="count-plus" data-group="{{item.childkey}}" ng-click="modalChild.plusEA($event ,item)">수량 더하기</a>
            </span>
        </div>
        
        
        <paging ng-if="modalChild.total > 0" class="small" page="modalChild.page" page-size="modalChild.rowsPerPage" total="modalChild.total" show-prev-next="true" show-first-last="true" paging-action="modalChild.getList({page : page});"></paging>  

        
        <p class="txt-title">선택결과</p>
        <table class="tbl-result">
        	<colgroup><col style="width:36%" /><col style="width:30%" /><col style="width:34%" /></colgroup>
            <caption>선택된 어린이, 총상품갯수, 총금액</caption>
            <thead>
            	<tr>
                	<th>선택된 어린이</th>
                    <th>총 상품 개수</th>
                    <th>총 금액</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td data-id="child_count">{{modalChild.child_count | number:0}}</td>
                    <td data-id="item_count">{{modalChild.item_count | number:0}}</td>
                    <td><span class="color1" data-id="total_amount">{{modalChild.total_amount | number:0}}<em>원</em></span></td>
                </tr>
            </tbody>
        </table>
        
        <div class="wrap-bt">
        	<a style="width:29%" class="bt-type7 fl" href="#" ng-click="modalChild.buy('cart')">장바구니</a>
            <a style="width:69%" class="bt-type6 fr" href="#" ng-click="modalChild.buy('buy')">선택 된 어린이에게 즉시 선물</a>
        </div>
    
    </div>
    
<!--//-->
</div>


</asp:Content>

