﻿$(function () {

    $page.init();

});

var $page = {

    processing: false,

    init: function () {

		// 탭메뉴 이벤트 (상품정보/배송안내/후기/문의) 
		$(".teb_menu").click(function () {
    		$(".teb_menu").removeClass("selected");
    		$(this).addClass("selected");

    		idx = $(this).data("idx");
    		$(".teb_content").hide();
    		$(".teb_content[data-idx='" + idx + "']").show();

    		return false;
		})

    },


}


var app = angular.module('cps.page', []);

app.controller("defaultCtrl", function ($scope, $http, $filter) {

	/* 후기 */
	$scope.review = {
		total: -1,
		list: null,

		params: {
			page: 1,
			rowsPerPage: 6,
			product_idx: -1,
			user_id: '',
			type: "all"
		},

		// getReviwList
		getList: function (params) {

			$scope.review.params = $.extend($scope.review.params, params);
			//console.log($scope.params);

			$http.get("/api/store.ashx?t=review_list", { params: $scope.review.params }).success(function (r) {

				if (r.success) {

					$scope.review.list = r.data;

					$.each($scope.review.list, function () {
						this.reg_date = new Date(this.reg_date);
						// 일주일 안이면 new (1000 * 초 * 분 * 시 * 일)
						this.is_new = (new Date() - this.reg_date) < (1000 * 60 * 60 * 24 * 7) ? "crop-new" : "";

					})

					$scope.review.total = r.data.length > 0 ? r.data[0].total : 0;
					if (params)
						scrollTo($(".wrap-tab"), 5);
				} else {
					alert(r.message);
				}
			});
		},

		// 클릭이벤트
		toggle: function (idx, $event) {
			$event.preventDefault();

			var self = $(".review_title[data-idx='" + idx + "']")
			var target = $(".review_body[data-idx='" + idx + "']");

			if (self.hasClass("selected")) {
				target.slideUp(300);
				self.removeClass("selected");
			} else {
				$(".review_body").slideUp(300);
				target.slideDown(300);
				$(".review_title").removeClass("selected");
				self.addClass("selected");
			}
		}
	}

    /* 문의 */
    $scope.qna = {
        qna_total: -1,
        list: null,

        params: {
            page: 1,
            rowsPerPage: 6,
            product_idx: -1,
            user_id: '',
            type: "all"
        },

        // getList
        getList: function (params) {

            $scope.qna.params = $.extend($scope.qna.params, params);
            //console.log($scope.params);

            $http.get("/api/store.ashx?t=qna_list", { params: $scope.qna.params }).success(function (r) {
            	console.log(r);
                if (r.success) {

                    $scope.qna.list = r.data;
                    $.each($scope.qna.list, function () {
                        if ($("#_userid").val() == this.user_id) {
                            this.isopen = false;
                        }
                        this.reg_date = new Date(this.reg_date);
                        this.show_detail = false;


                        if (this.answer == null || this.answer == "") {
                        	this.answerClass = "state2-ask"
                        	this.answerYN = "진행중";
                        } else {
                        	this.answerClass = "state-ask"
                        	this.answerYN = "답변<br/>완료";
                        }
						/*
                        if (this.answer_date == null) {
                            this.answerClass = "state2-ask"
                            this.answerYN = "처리중";
                        } else {
                            this.answerClass = "state-ask"
                            this.answerYN = "답변<br/>완료";
                        }
						*/
                    })
                    

                    $scope.qna.total = r.data.length > 0 ? r.data[0].total : 0;
                    if (params)
                    	scrollTo($(".wrap-tab"), 5);
                } else {
                    alert(r.message);
                }
            });
        },

    	// 클릭이벤트
        toggle: function (idx, $event) {
        	$event.preventDefault();

        	var self = $(".qna_title[data-idx='" + idx + "']")
        	var target = $(".qna_body[data-idx='" + idx + "']");

        	if (self.hasClass("selected")) {
        		target.slideUp(300);
        		self.removeClass("selected");
        	} else {
        		$(".qna_body").slideUp(300);
        		target.slideDown(300);
        		$(".qna_title").removeClass("selected");
        		self.addClass("selected");
        	}
        }


    }


    $scope.review.getList();
    $scope.qna.getList();
});
