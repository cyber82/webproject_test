﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="order.aspx.cs" Inherits="store_order" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/kcp_no_form.ascx" TagPrefix="uc" TagName="kcp_no_form" %>
<%@ Register Src="/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>
<%--<%@ Register Src="/store/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/store/pay/kcp_no_form.ascx" TagPrefix="uc" TagName="kcp_no_form" %>
<%@ Register Src="/store/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/store/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>--%>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
	<script type="text/javascript" src="/store/order.js?v=2.1"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />
	<input type="hidden" runat="server" id="hd_good_mny" value="" />
	<input type="hidden" runat="server" id="hd_good_name" value="" />
	<input type="hidden" runat="server" id="hd_delivery_fee" value="" />
	<input type="hidden" id="addr_domestic_addr" runat="server" /> 
	<input type="hidden" id="addr_domestic_addr1" runat="server" /> 
	<input type="hidden" id="addr_domestic_addr2" runat="server" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
    <input type="hidden" id="temp_name" runat="server" /> 
	<input type="hidden" id="temp_mobile" runat="server" />
    <input type="hidden" id="temp_zipcode" runat="server" /> 
    <input type="hidden" id="temp_addr1" runat="server" /> 
    <input type="hidden" id="temp_addr2" runat="server" /> 

	
<div class="wrap-sectionsub"  ng-app="cps" ng-cloak ng-controller="defaultCtrl">
    <div class="store-basket">
    	
        <div class="step-flow">
        	<ol>
            	<li><span class="bg"></span><em>STEP 01</em>장바구니</li>
                <li class="recent"><span class="bg"></span><em>STEP 02</em>주문/결제</li>
                <li><span class="bg"></span><em>STEP 03</em>주문완료</li>
            </ol>
        </div>
        
        <!--주문상품리스트(해당부분 click 시  "bt-bu1 부분" +  "해당 서브 레이어 부분"   변경하시면 됩니다)-->
        <div class="wrap-order-click sectionsub-margin1 teb_button" data-idx="order">
        	<p class="ic1">주문상품리스트</p><span class="bt-bu1 bt-bu1-active"></span>  <!--해당영역 활성화 시  "bt-bu1-active"  css  adding/remove-->
        </div>
        <div class="wrap-order-sub" data-idx="order" style="display:block">    <!--해당 부분 display-->
       		
            <ul class="list-basket" id="item_container">
				<!-- 상품정보 스크립트 -->

			</ul>
       		<p class="txt-bottom">3만원 이상 구매 및 어린이 선물 구매 시 무료배송 / 3만원 미만 구매 시 배송비 2,500원 (어린이 선물을 포함한 총 상품 금액 3만원 이상인 경우 무료배송)</p>
            
            <ul class="list-total">
                <li>총 주문 합계<span class="txt-price"><em id="total_amount"></em></span></li>
                <li>주문금액 합계<span class="txt-price"><em id="sub_total"></em></span></li>
                <li>배송비<span class="txt-price"><em id="delivery_fee"></em></span></li>
            </ul>
            
        </div>
        
        <!--배송정보(해당부분 click 시  "bt-bu1 부분" +  "해당 서브 레이어 부분"   변경하시면 됩니다)-->
        <div class="wrap-order-click sectionsub-margin1" data-idx="delivery" id="deliveryTitle">
        	<p class="ic2">배송정보</p><span class="bt-bu1 bt-bu1-active"></span>  <!--해당영역 활성화 시  "bt-bu1-active"  css  adding/remove-->
        </div>
        <div class="wrap-order-sub"  data-idx="delivery" style="display:block" id="deliveryContent">    <!--해당 부분 display-->
        	
            <fieldset class="frm-input">
            	<legend>배송정보 입력</legend>
                <div class="row-table">
                	<div class="col-th col-valign" style="width:27%"><span class="necessary">배송지선택</span></div>
                    <div class="col-td" style="width:73%">
                    	<span class="radio-ui">
                            <input type="radio" id="rdoAddrOld" name="rdoAddr" class="css-radio" checked />
                            <label for="rdoAddrOld" class="css-label">주문고객과 동일정보</label>
                        </span>
                        <span class="radio-ui">
                            <input type="radio" id="rdoAddrNew" name="rdoAddr" class="css-radio" />
                            <label for="rdoAddrNew" class="css-label">새로운 배송지</label>
                        </span>
                    </div>
                </div>
                <div class="row-table">
                	<div class="col-th" style="width:27%"><span class="necessary">수령인</span></div>
                    <div class="col-td" style="width:73%"><input type="text" id="name" runat="server" value="" style="width:100%" /></div>
                </div>
                <div class="row-table">
                	<div class="col-th" style="width:27%"><span class="necessary">휴대폰번호</sapn></div>
                    <div class="col-td" style="width:73%"><input type="text" id="mobile" runat="server" maxlength="11" value="" placeholder="-없이 입력" style="width:100%" /></div>
                </div>
                <div class="row-table">
                	<div class="col-th" style="width:27%"><span class="necessary">배송주소</span></div>
                    <div class="col-td" style="width:73%">
						<span class="row pos-relative2">
							
							<input type="hidden" id="addr1" />
							<input type="hidden" id="addr2" />

							<input type="text" id="addr_domestic_zipcode" placeholder="주소" runat="server" value="" style="width:100%" readonly/>
							<a class="bt-type8 pos-btn" style="width:70px" href="#" runat="server" id="btn_search_addr" ng-click="findAddr($event)">주소찾기</a>

                        </span>
                   
						<span id="addr_road" class="row fs13 mt15"></span>
						<span id="addr_jibun" class="row fs13"></span>

                        <p class="txt-commt mb10">주문 후에는 배송지 변경이 불가능합니다.</p>
                    </div>
                </div>
                <div class="row-table">
                	<div class="col-th" style="width:27%">비상연락처</div>
                    <div class="col-td" style="width:73%"><input type="text" id="phone" runat="server" maxlength="11"  value="" placeholder="-없이 입력" style="width:100%" /></div>
                </div>
                <div class="row-table row-topline">
                    <div class="col-td" style="width:100%">
                    	<span class="row">
                            <select id="memo_template" style="width:100%" >
								<option value="선택해 주세요">배송메모를 선택해 주세요.</option>
								<option value="부재 시 경비실에 맡겨주세요">부재 시 경비실에 맡겨주세요</option>
								<option value="도착 전 연락주세요">도착 전 연락주세요</option>
								<option value="기타">기타</option>
                            </select>
						</span>
                        <span class="row"><textarea id="memo" runat="server" maxlength="100" style="width:100%;height:60px" disabled></textarea></span>   <!--기타 경우-->
                    </div>
                </div>
            </fieldset>
            
        </div>
        
        <!--결제정보(해당부분 click 시  "bt-bu1 부분" +  "해당 서브 레이어 부분"   변경하시면 됩니다)-->
        <div class="wrap-order-click sectionsub-margin1"  data-idx="payment">
        	<p class="ic3">결제정보</p><span class="bt-bu1 bt-bu1-active"></span>  <!--해당영역 활성화 시  "bt-bu1-active"  css  adding/remove-->
        </div>
        <div class="wrap-order-sub"  data-idx="payment" style="display:block">    <!--해당 부분 display-->
        	
			<fieldset class="frm-input" id="frm_paymethod" runat="server">
            	<legend>결제수단 입력</legend>
                <div class="row-table">
                	<div class="col-th col-valign" style="width:27%">결제수단</div>
                    <div class="col-td" style="width:73%">

						<!--
							간편결제와 휴대폰결제 추가 필요 
							<input type="radio" runat="server" id="payment_method_card_" name="payment_method"  checked="true"/> 신용카드 자동이체
							<input type="radio" runat="server" id="payment_method_cms_" name="payment_method"  /> 실시간 계좌이체
							<input type="radio" runat="server" id="payment_method_oversea" name="payment_method"  /> 해외발급카드 즉시결제
						-->
                    	<span class="row">
                        	<span class="radio-ui">
                                <input type="radio" id="payment_method_card" name="payment_method" runat="server" class="css-radio" checked />
                                <label for="payment_method_card" class="css-label">신용카드</label>
                            </span>
                            <span class="radio-ui">
                                <input type="radio" id="payment_method_cms" name="payment_method" runat="server" class="css-radio" />
                                <label for="payment_method_cms" class="css-label">실시간 계좌이체</label>
                            </span>
                        </span>
						
                        <span class="row">
                        	<span class="radio-ui">
                                <input type="radio" id="payment_method_phone" name="payment_method" runat="server" class="css-radio" />
                                <label for="payment_method_phone" class="css-label">휴대폰결제</label>
                            </span>
                        </span>
						<!--
                        <span class="row">
                        	<span class="radio-ui">
                                <input type="radio" id="payment_method_kakao" name="payment_method" runat="server" class="css-radio" />
                                <label for="payment_method_kakao" class="css-label"><img src="/common/img/icon/kakaopay.png" alt="카카오 페이" class="txt-kakaopay" /></label>
                            </span>
                            <span class="radio-ui">
                                <input type="radio" id="payment_method_payco" name="payment_method" runat="server" class="css-radio" />
                                <label for="payment_method_payco" class="css-label"><img src="/common/img/icon/payco.png" alt="페이코" class="txt-payco" /></label>
                            </span>
                        </span>
						-->
                    </div>
                </div>
			</fieldset>
			
            <ul class="list-totalpay">
                <li>주문금액 합계<span class="txt-price"><em id="sub_total2"></em></span></li>
                <li>배송비<span class="txt-price"><em id="delivery_fee2"></em></span></li>
                <li class="total">총 주문 합계<span class="txt-price"><em id="total_amount2"></em></span></li>
            </ul>
            
        </div>
        
        <!---->
        <div class="box-agree">
        	<span class="checkbox-ui">
                <input type="checkbox" class="css-checkbox" id="chk_agree" runat="server"/>
                <label for="chk_agree" class="css-label">동의선택</label>
            </span>
            <label for="buy-agree">위 주문의 상품명, 가격, 할인, 배송 정보를 확인하였으며 이에 동의합니다.<br />
            (전자상거래법 제 8조 2항)</label>
        </div>
        
        <div class="wrap-bt">
        	<asp:LinkButton class="bt-type6" runat="server" ID="btn_submit" OnClick="btn_submit_Click" style="width:100%">결제하기</asp:LinkButton>
        </div>
        
    </div>
    
<!--//-->
</div>

	<!---->
	<textarea class="template" style="display:none">
	   <li class="item_row">
			<span class="header" data-id="type"></span>
			<a href="#"><span class="item">
				<img data-id="img" src="/" alt="" class="photo" />
				<p class="txt-prdname" data-id="title"></p>
				<p class="txt-option" data-id="option"></p>
				<p class="txt-price"><span data-id="price"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="txt-count">수량 <em data-id="ea">2</em></span></p>
			</span></a>
			<span class="total">
				주문금액 <span class="txt-total"><em data-id="total"></em></span>
			</span>
		</li> 	
	</textarea>



</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form" />
	<uc:kcp_no_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_no_form" />
	<uc:payco_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="payco_form" />
	<uc:kakaopay_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kakaopay_form" />
</asp:Content>
