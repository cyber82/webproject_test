﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cart.aspx.cs" Inherits="store_cart" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/store/cart.js?v=1.1"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
<div class="wrap-sectionsub">
<!---->
	<em class="fc_blue" id="total" style="display:none;">0</em>
	
    <div class="store-basket empty" style="display:none;">
    	
        <div class="wrap-msg empty-order sectionsub-margin1">
        	<strong>현재 장바구니에 보관된<br />상품이 없습니다.</strong>
        </div>
        <div class="wrap-bt">
        	<a class="bt-type6" style="width:60%" href="/store/">계속 쇼핑하기</a>
		</div>            
        
    </div>
    
    <div class="store-basket full" style="display:none;">
    	
        <div class="step-flow">
        	<ol>
            	<li class="recent"><span class="bg"></span><em>STEP 01</em>장바구니</li>
                <li><span class="bg"></span><em>STEP 02</em>주문/결제</li>
                <li><span class="bg"></span><em>STEP 03</em>주문완료</li>
            </ol>
        </div>
        
        <div class="wrap-selectall" >
        	<span class="checkbox-ui">
                <input type="checkbox" class="css-checkbox" id="chk_all" checked>
                <label for="chk_all" class="css-label">상품전체선택</label>
            </span>
            <a class="delete" id="btn_remove" href="#">선택삭제</a>
        </div>
        
        <ul class="list-basket"  id="item_container">
        
        </ul>
        <!--
        <div class="wrap-bt"><a class="bt-type4" href="#">더 보기</a></div>
		-->
        
        <ul class="list-total">
        	<li>총 주문 합계<span class="txt-price"><em id="total_amount"></em>원</span></li>
            <li>주문금액 합계<span class="txt-price"><em id="sub_total"></em></span></li>
            <li>배송비<span class="txt-price"><em id="delivery_fee"></em></span></li>
        </ul>
		 
        <div class="wrap-bt">
        	<a style="width:33%" class="bt-type7 fl" href="/store/">계속 쇼핑하기</a>
            <a style="width:31.5%" class="bt-type6" href="#" id="btn_order">선택주문</a>
            <a style="width:31.5%" class="bt-type6 fr" href="#"  id="btn_order_all">전체주문</a>
        </div>
        
    </div>
    
<!--//-->
</div>
	
	<textarea class="template" style="display:none">
		<li class="item_row">
            <span class="header">
                <span data-id=type></span>
                <span class="checkbox-ui">
                    <input type="checkbox" class="css-checkbox item_obj" id="chk1" data-id="chk" checked>
                    <label for="chk1" class="css-label">상품선택</label>
                </span>
                <a class="delete btn_one_remove" href="#">삭제</a>
            </span>
            <a href="#" target="_blank" data-id="title2">
				<span class="item">
					<img data-id="img" alt="" class="photo item_obj" />
					<p class="txt-prdname item_obj" data-id="title"></p>
					<p class="txt-option item_obj"  data-id="option"></p>
					<p class="txt-option item_obj"  data-id="child"></p>
					<p class="txt-price item_obj"  data-id="price"></p>
				</span>
            </a>
            <span class="count" >
                <a class="count-minus item_obj" data-id="btn_minus_ea" href="#">수량 빼기</a>
                <input type="number" class="item_obj" value="0" id="quantity" data-id="ea" readonly  />
                <a class="count-plus item_obj" data-id="btn_plus_ea" href="#">수량 더하기</a>
            </span>
            <span class="total">
                주문금액 <span class="txt-total item_obj" data-id="total" data-group=""><em>원</em></span>
            </span>
        </li>
	</textarea>




</asp:Content>
