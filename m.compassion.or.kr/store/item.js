﻿$(function () {

    $page.init();

    if (cookie.get("cps.app") != null) {
        $(".store-floating-btn").css("height", "107px");
        $(".tabbarH").css("height", "170px");
    }
});


var app = angular.module('cps.page', []);
app.controller("itemCtrl", function ($scope, $http, $filter, popup, paramService) {

    /* Review */
    $scope.review = {
        total: -1,
        list: [],

        params: {
            page: 1,
            rowsPerPage: 3,
            product_idx: $("#key").val(),
            type: 'all'
        },

        // getReviwList
        getList: function (params) {

            params: $.extend($scope.review.params, params);

            $http.get("/api/store.ashx?t=review_list", { params: $scope.review.params }).success(function (r) {

                if (r.success) {

                    $scope.review.list = r.data;

                    $.each($scope.review.list, function () {
                        this.reg_date = new Date(this.reg_date);
                        this.show_detail = false;
                    })

                    $scope.review.total = r.data.length > 0 ? r.data[0].total : 0;

                } else {
                    alert(r.message);
                }
            });
        },

        // 클릭이벤트
        toggle: function (idx) {

            var self = $(".review_title[data-idx='" + idx + "']")
            var target = $(".review_body[data-idx='" + idx + "']");

            if (self.hasClass("selected")) {
                target.slideUp(300);
                self.removeClass("selected");
            } else {
                $(".review_body").slideUp(300);
                target.slideDown(300);
                $(".review_title").removeClass("selected");
                self.addClass("selected");
            }

        },

        // 리뷰 등록 
        add: function () {
            //console.log($scope.review_content);
            if ($page.validReview()) {
                $http.post("/api/store.ashx", { t: 'add_review', id: $("#key").val(), content: $scope.review_content }).success(function (result) {
                    //console.log(result);
                    if (result.success) {
                        $("#review_content").val("");
                        $scope.review.getList({ page: 1 });

                        $(".cancel_review").trigger("click");
                    } else {
                        alert(result.message);

                    }
                })
            }
        }

    },

    /* QnA */
	$scope.qna = {
	    total: -1,
	    list: [],

	    params: {
	        page: 1,
	        rowsPerPage: 3,
	        product_idx: $("#key").val(),
	        type: 'all'
	    },

	    // getList
	    getList: function (params) {
	        $scope.qna.params = $.extend($scope.qna.params, params);
	        $http.get("/api/store.ashx?t=qna_list", { params: $scope.qna.params }).success(function (r) {
	            //console.log(r)
	            if (r.success) {
	                //console.log(r.data);
	                $scope.qna.list = r.data;
	                $.each($scope.qna.list, function () {
	                    if ($("#_userid").val() == this.user_id) {
	                        this.isopen = false;
	                    }
	                    this.reg_date = new Date(this.reg_date);
	                    this.show_detail = false;


	                    if (this.answer == null || this.answer == "") {
	                        this.answerClass = "txt-state2"
	                        this.answerYN = "처리중";
	                    } else {
	                        this.answerClass = "txt-state1"
	                        this.answerYN = "답변<br/>완료";
	                    }
	                    /*
						if (this.answer_date == null) {
							this.answerClass = "txt-state2"
							this.answerYN = '처리중';
						} else {
							this.answerClass = "txt-state1"
							this.answerYN = '답변<br /> 완료';
						}
						*/
	                })
	                $scope.qna.total = r.data.length > 0 ? r.data[0].total : 0;
	            } else {
	                alert(r.message);
	            }
	        });
	    },

	    // 클릭이벤트
	    toggle: function (idx) {

	        var self = $(".qna_title[data-idx='" + idx + "']")
	        var target = $(".qna_body[data-idx='" + idx + "']");

	        if (self.hasClass("selected")) {
	            target.slideUp(300);
	            self.removeClass("selected");

	            target.find(".modify_div").hide();
	            target.find(".answer_view").show();
	        } else {
	            $(".qna_body").slideUp(300);
	            target.slideDown(300);
	            $(".qna_title").removeClass("selected");
	            self.addClass("selected");
	        }

	    },

	    toggleUpdate: function (idx, $event) {

	        var target = $(".qna_body[data-idx='" + idx + "']");

	        target.find(".answer_view").hide();
	        target.find(".modify_div").show();

	        $event.preventDefault();
	    },

	    // 문의 등록 수정 삭제 
	    add: function () {
	        if ($page.validQna()) {
	            $http.post("/api/store.ashx", { t: 'add_qna', id: $("#key").val(), content: $("#qna_content").val(), isOpen: $scope.qna_isOpen }).success(function (result) {
	                if (result.success) {
	                    $("#qna_content").val("");
	                    $scope.qna.getList({ page: 1 });
	                    $(".cancel_qna").trigger("click");
	                } else {
	                    alert(result.message);
	                }
	            })
	        }
	    },

	    update: function (idx, $event) {
	        if (common.checkLogin()) {

	            var content = $(".modify_text[data-idx='" + idx + "']").val();

	            if (content.length < 30) {
	                alert("30자 이상 입력해야 등록가능합니다.")
	                return false;
	            }

	            $http.post("/api/store.ashx", { t: 'update_qna', id: idx, content: content }).success(function (result) {
	                if (result.success) {
	                    $scope.qna.getList({ page: 1 });
	                } else {
	                    alert(result.message);
	                }
	            })
	        }

	        $event.preventDefault();
	    },

	    delete: function (idx) {
	        if (common.checkLogin()) {
	            if (confirm("삭제하시겠습니까?")) {
	                $http.post("/api/store.ashx", { t: 'delete_qna', id: idx }).success(function (result) {
	                    if (result.success) {
	                        alert("삭제 되었습니다.")
	                        $scope.qna.getList({ page: 1 });
	                    } else {
	                        alert(result.message);
	                    }
	                })
	            }
	        }
	    }
	},

    // 공지사항 관련
	$scope.notice = {
	    list: null,
	    index: 0,
	    total: 0,
	    getList: function () {

	        list = null;
	        $http.get("/api/store.ashx?t=notice_list", { page: 1, rowsPerPage: 10, }).success(function (r) {

	            if (r.success) {

	                $scope.notice.list = r.data;

	                $.each($scope.notice.list, function () {
	                    this.dtreg = new Date(this.dtreg);
	                    this.href = "/store/notice/view/" + this.idx;
	                })
	                console.log($scope.notice.list);
	            } else {
	                alert(r.message);
	            }
	        });
	    },

	    goPage: function ($event) {
	        $event.preventDefault()
	        idx = $scope.notice.list[0].idx;

	        $http.get("/api/store.ashx?t=increase_notice_hit", { params: { idx: idx } }).success(function (r) {
	            console.log(r);
	            if (r.success) {
	                location.href = "/store/notice/view/" + idx
	            } else {
	                alert(r.message);
	            }
	        });
	    }

	},

    // 상품이미지 이동
	$scope.image = {

	    page: 1,
	    rowsPerPage: 4,
	    isShow: function (index) {

	        if ((parseInt(index / $scope.image.rowsPerPage + 1)) == $scope.image.page) {

	            return true;
	        }
	        $scope.image.pageCount = (parseInt(index / $scope.image.rowsPerPage));
	        return false;
	    },

	    prev: function ($event) {
	        $event.preventDefault();
	        $scope.image.page--;
	        if ($scope.image.page < 1) {
	            $scope.image.page = 1;
	        }
	    },

	    next: function ($event) {
	        $event.preventDefault();
	        if ($scope.image.pageCount == $scope.image.page) {
	            $scope.image.page++;
	        }

	    }

	},

    //공지사항 가기
    /*
    $scope.goNotice = function ($event) {
        $event.preventDefault();
        location.href = "/store/notice/";
    }
	*/

    // 스토어 다른상품 상세 페이지
    $scope.goView = function (idx, $event) {
        //현 페이지에 $scope.params가 없음.
        location.href = "/store/item/" + idx //+ "?" + $.param($scope.params);
        $event.preventDefault();
    }

    // 어린이에게 선물하기
    $scope.goChild = function ($event) {


        $event.preventDefault();
        if (!common.checkLogin()) {
            return;
        }

        $http.get("/api/store.ashx?t=get_children", { params: {} }).success(function (r) {
            if (r.success) {
                if (r.data.length < 1) {
                    alert("어린이가 있는 경우에만 선물할 수 있습니다.");
                    return;
                }
            } else {
                if (r.action == "not_sponsor") {
                    alert("한국컴패션 후원자만 어린이에게 선물하실 수 있습니다.");
                    return;
                } else if (!r.data) {
                    alert("후원가능한 어린이가 없습니다.");
                    return;
                } else {
                    alert(r.message);
                }
            }

            //현 페이지에 $scope.params가 없음.
            idx = $("#key").val();
            location.href = "/store/item-child/" + idx + "?";

        });
    }

    //팝업
    $scope.getPopup = {

        instance: null,
        item: null,

        show: function (target, $event) {
            popup.init($scope, "/store/popup/" + target, function (modal) {
                $scope.getPopup.instance = modal;

                modal.show();

            });
            if ($event) {
                $event.preventDefault();
            }
        },

        hide: function ($event) {
            $event.preventDefault();
            if (!$scope.getPopup.instance)
                return;
            $scope.getPopup.instance.hide();

        },
    }

    // 실행 
    $scope.review.getList();
    $scope.qna.getList();
    $scope.notice.getList();


    // 마이메뉴에서 이동시 메뉴 선택
    $scope.selectMenu = function (data) {
        $(".teb_menu").removeClass("selected");
        $(".teb_menu[data-idx='" + data + "']").addClass("selected");
        $(".teb_content").hide();
        $(".teb_content[data-idx='" + data + "']").show();
    }

    $scope.data = paramService.getParameterValues();
    if ($scope.data.data != null) {
        $scope.selectMenu($scope.data.data);

    }


});



var $page = {

    processing: false,

    init: function () {

        $("#btn_minus_ea").click(function () {

            var ea = parseInt($("#ea").val()) - 1;
            if (ea < 1) {
                return false;
            }

            $("#ea").val(ea);
            $page.calculate();
            return false;
        })

        $("#btn_plus_ea").click(function () {
            var inventory = parseInt($("#hd_inventory").val());
            var ea = parseInt($("#ea").val()) + 1;
            if (ea > inventory) {
                alert('재고량이 부족 합니다.');
                return false;
            }

            $("#ea").val(ea);
            $page.calculate();
            return false;
        })

        $("#btn_buy , #btn_cart").click(function () {

            if ($page.processing) return false;
            if (!common.checkLogin()) {
                return false;
            }

            var index = $('#option option').index($('#option option:selected'));
            if ($('#option option').size() > 1 && index < 1) {
                alert('옵션을 선택해주세요');
                $("#option").focus();
                return false;
            }

            $page.processing = true;
            var action = $(this).attr("id") == "btn_cart" ? "cart" : "buy";
            $page.setBasket(action);


            return false;
        });

        // 어린이에게 선물하기
        /*
		$("#btn_show_child").click(function () {

			if (!common.checkLogin()) {
				return false;
			}

			modalShow($("#child_form"), function (obj) {

				$page.setGiftLayerEvent(obj);
				
			}, true, false);

			return false;

		});
		*/

        $("#userid").val(common.getUserId());

        // 제품상세 : 후기작성하기 show/hide
        $(".btn_mainReview").click(function ($event) {
            if (!common.checkLogin()) {
                $event.preventDefault();
                return false;
            }
            if ($("#buy_check").val() == "N") {
                alert("구매하신 상품만 후기를 작성할 수 있습니다.");
                return false;
            }

            if ($(".sub-write.review").css("display") == "none") {
                $(this).find("span").addClass("bt-bu1-active");
                $("#review_content").val("후기를 작성해주세요.")
                $(".sub-write.review").show();

            }
        })

        $(".cancel_review").click(function () {
            $("#review_content").val("")
            $(".sub-write.review").hide();
            $(".btn_mainReview").find("span").removeClass("bt-bu1-active");

            return false;
        })

        // 제품상세 : 문의작성하기 show/hide	`
        $(".btn_mainQna").click(function () {
            if ($(".sub-write.qna").css("display") == "none") {
                $(this).find("span").addClass("bt-bu1-active");
                $("#qna_content").val("문의를 작성해주세요.")
                $(".sub-write.qna").show();
            } else {
                $(".cancel_qna").trigger("click");
            }
        })

        $(".cancel_qna").click(function () {
            $(".sub-write.qna").hide();
            $(".btn_mainQna").find("span").removeClass("bt-bu1-active");
        })

        //장바구니담기 확인 레이어
        $(".layer_type1 .btn_close , #go_shopping").click(function () {
            $(".layer_type1.cart").hide();
            return false;
        })

        $("#go_cart").click(function () {
            location.href = '/store/cart';
        })

        // 탭메뉴 이벤트 (상품정보/배송안내/후기/문의) 
        $(".teb_menu").click(function () {
            $(".teb_menu").removeClass("selected");
            $(this).addClass("selected");

            idx = $(this).data("idx");
            $(".teb_content").hide();
            $(".teb_content[data-idx='" + idx + "']").show();

            return false;
        })

        $("#option").change(function () {
            $page.calculate();
        })

        setPlaceholder($("#review_content"), "후기를 작성해주세요.")

        setPlaceholder($("#qna_content"), "문의를 작성해주세요.")

        this.calculate();

        $page.setMainVisual();

    },

    setBasket: function (action) {
        //console.log(action);
        var json = {};
        json.user_id = $("#hd_user_id").val();
        json.item_id = $("#hd_item_id").val();
        json.option_name = $("#option option:selected").text();
        json.option = $("#option").val();
        json.option_price = $("#option option:selected").data("price") || 0;
        json.quantity = $("#ea").val();

        var jsonStr = $.toJSON(json);

        //GA Product Start
        dataLayer.push({
            'event': 'Add',
            'ecommerce': {
                'currencyCode': 'KRW',
                'add': {
                    'products': [{
                        'id': $("#hd_item_id").val(),  // 상품 코드           
                        'name': $("#hd_title").val(), // 상품 이름           
                        'brand': '한국컴패션', // 브랜드           
                        'category': $("#hd_gift_flag_name").val() == 'true' ? '어린이선물' : '일반상품', // 상품 카테고리           
                        'price': $("#hd_price").val(), // 가격(\)           
                        'quantity': $("#ea").val(), // 제품 수량           
                        'variant': $("#option option:selected").text() // 상품 옵션        
                    }]
                }
            }
        });
        //GA Product End

        $.post("/api/store.ashx", { t: "set_basket", data: jsonStr }, function (r) {

            $page.processing = false;
            if (r.success) {
                if (action == "cart") {
                    //console.log(angular.element(document.getElementById('itemCtrl')).scope())
                    angular.element(document.getElementById('itemCtrl')).scope().getPopup.show('cart');
                } else if (action == "buy") {
                    location.href = '/store/cart';
                }

            } else {
                alert(r.message);
            }

        });

        return;


    },

    calculate: function () {

        var price = parseInt($("#hd_price").val());
        var ea = parseInt($("#ea").val());
        var opt_price = $("#option option:selected").data("price") || 0;
        var sub_total = (price + opt_price) * ea;
        //var delivery_fee = 2500;
        var delivery_fee = parseInt($("#hd_delivery_charge").val());
        if (sub_total >= 30000) {
            delivery_fee = 0;
        }

        var total = sub_total + delivery_fee;
        $("#delivery_fee").html(delivery_fee.format());


        $("#total_amount").html(total.format());
        $("#hd_total_amount").val(total);
        $("#hd_delivery_fee").val(delivery_fee);
    },

    validReview: function () {
        if (common.checkLogin()) {
            if ($("#review_content").val().length < 30) {
                alert("30자 이상 입력해야 등록가능합니다.")
                return false;
            }
            return true;
        }
        return false;
    },

    validQna: function () {
        if (common.checkLogin()) {
            if ($("#qna_content").val().length < 30) {
                alert("30자 이상 입력해야 등록가능합니다.")
                return false;
            }
            return true;
        }
        return false;
    },

    setMainVisual: function () {
        new Swiper('.wrap-slider', {
            pagination: '.page-count',
            paginationClickable: true,
            nextButton: '.page-next',
            prevButton: '.page-prev',
            paginationBulletRender: function (index, className) {
                //<span class="recent">1페이지</span>
                //className = "recent " + index;
                return '<span class="' + className + '">' + (index + 1) + '</span>';
            },
            spaceBetween: 30
            //loop : true
        });
    },
}