﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="item.aspx.cs" Inherits="store_item" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/store/item.js?v=1"></script>
	<script src="/common/js/util/swiper.js"></script>
	<style>
		
	</style>
</asp:Content>

<asp:Content ID="head_script_GA" runat="server" ContentPlaceHolderID="head_script_GA">
    <script>
    //GA Product Start
    dataLayer.push({
        'ecommerce': {
            'currencyCode': 'KRW',
            'detail': {
                'products': [{
                    'id': '<%=this.ViewState["hd_item_id"].ToString()%>',  // 상품 코드           
                    'name': '<%=this.ViewState["hd_title"].ToString()%>', // 상품 이름           
                    'brand': '한국컴패션', // 브랜드           
                    'category': '<%=this.ViewState["hd_gift_flag_name"].ToString()%>' == 'true' ? '어린이선물' : '일반상품' // 상품 카테고리             
                }]
            }
        },
    });
    //console.log(dataLayer);
    //GA Product End
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">



<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="itemCtrl" id="itemCtrl">
<!---->
	
	<input type="hidden" id="key" runat="server" />
	<input type="hidden" id="hd_user_id" runat="server" />
	<input type="hidden" id="hd_item_id" runat="server" />
    <input type="hidden" id="buy_check" runat="server" />
	<input type="hidden" id="hd_title" runat="server" />
	<input type="hidden" id="hd_price" runat="server" />
	<input type="hidden" id="hd_total_amount" runat="server" />
	<input type="hidden" id="hd_delivery_fee" runat="server" />
	<input type="hidden" id="hd_inventory" runat="server" />
    <input type="hidden" runat="server" id="hd_delivery_charge" value="" />
    <input type="hidden" id="hd_gift_flag_name" runat="server" />

	<a id="btnList" runat="server" style="width:100%; display:none;" >목록</a>

    <!--제품상세-->
    <div class="store-detail">
    
    	<div class="info-header">
        	<strong class="txt-prd"><asp:Literal runat="server" ID="title" /></strong>
            <p class="txt-price"><em><asp:Literal runat="server" ID="price" /></em>원 <span class="ic-buy"><asp:Literal runat="server" ID="status" /></span></p>
            <span class="sns sns_ani down">
            	<a ng-click="sns.show($event)"><img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
        	
            </span>
        </div>
    	
        <div class="wrap-slider" style="width:100%;overflow:hidden;">
			
            <div class="swiper-wrapper">
				<asp:Repeater ID="repeater_image" runat="server">
					<ItemTemplate>
							<div class="swiper-slide">
								<a href="#">
									<img src="<%# (Uploader.GetRoot(Uploader.FileGroup.image_shop) + Eval("saved_file_name").ToString()).WithFileServerHost()%>" alt="상품썸네일" width="100%" />
							     </a>
							</div>
					</ItemTemplate>
				</asp:Repeater>
			</div>
            
            <div class="page-count"></div>

			<div class="page-prev">이전보기</div>
            <div class="page-next">다음보기</div>
        </div>
        
        <div class="info-option">
        	<span class="row">
				<asp:DropDownList runat="server" ID="option" style="width:100%">
				</asp:DropDownList>
            </span>
            <span class="row count">
            	<a class="count-minus" id="btn_minus_ea" >수량 빼기</a>
                <input type="number" id="ea" value="1" />
                <a class="count-plus" id="btn_plus_ea" >수량 더하기</a>
            </span>
        </div>
        
        <div class="info-price">
        	<ul class="list-price">
            	<li><span class="column-th">판매가</span> <span class="color1"><asp:Literal runat="server" ID="price2" /></span>원</li>
                <li><span class="column-th">판매여부</span> <asp:Literal runat="server" ID="status2" /></li>
                <li><span class="column-th">배송비</span><span id="delivery_fee"></span>원</li>
            </ul>
            <p class="txt-commt">(3만원이상 무료-도서산간지역일 경우<br />추가 배송료가 발생할 수 있습니다.)</p>
            <ul class="list-total">
            	<li><span class="column-th">최종금액</span> <span class="color1" id="total_amount">11,500</span> 원</li>
            </ul>
        </div>
    </div>
    
    <!--상품정보 고정버튼-->
    <div class="store-floating-btn">
    	<a runat="server" ID="btn_buy">구매하기</a>
        <a runat="server" ID="btn_cart" >장바구니</a>
        <a runat="server" ID="btn_show_child" ng-click="goChild($event)">후원어린이에게 선물하기<span class="ic-gift"></span></a>
    </div>
    
    <!--  선물하기 버튼 없을 경우  "no-gift"  class만 추가해주면 됩니다.
    <div class="store-floating-btn no-gift">
    	<a href="#">구매하기</a>
        <a href="#">장바구니</a>
        <a href="#">후원어린이에게 선물하기<span class="ic-gift"></span></a>
    </div>
    -->
    
    <!--//상품정보 고정버튼-->

	<!--제품 상세 탭-->
    <div class="wrap-tab" id="product_review">
    	<ul class="tab-type2">
        	<li class="selected teb_menu" data-idx="0" style="width:25%"><a href="#">상품정보</a></li>
            <li class="teb_menu" data-idx="1" style="width:25%"><a href="#">배송안내</a></li>
            <li class="teb_menu" data-idx="2" style="width:25%"><a href="#">상품후기</a></li>
            <li class="teb_menu" data-idx="3" style="width:25%"><a href="#">상품문의</a></li>
        </ul>
    </div>
    
    <!--제품상세 탭 내용-->
    <div class="store-detail-admin teb_content" data-idx="0">
        <div class="editor-html">
	        <asp:Literal runat="server" ID="product_detail"></asp:Literal>
        </div>
    </div>
    <!--//제품상세 탭 내용-->
    
	<!--배송안내 탭 내용-->
    <div class="store-detail-admin teb_content" data-idx="1" style="display:none;">
        
        <strong class="ic-title1">배송안내</strong>
        <ul class="list-txt">
        	<li><span class="color1">3만원 이상 구매 시 무료배송 / 3만원 미만 구매 시 배송비 2,500원</span><br />
            	도서산간 지역(제주도 포함), 오지 일부 지역은 운임이  추가될 수 있습니다. (해당되신 분들께는 별도 연락을 드립니다.) 
            </li>
            <li>컴패션 커피는 오후 1시 30분 마감입니다. 미리 볶아두지 않고 주문 받은 후 로스팅합니다.
				따라서 평일에만 주문/출고되며, 공휴일 전날 주문하시면 공휴일이 지난 다음 날 로스팅하여 출고됩니다.</li>
            <li>오후 3시 30분 이전 결제가 완료 된 주문은 당일 출고됩니다.</li>
            <li>토요일/공휴일 제외한 평균 배송기간은 출고일로부터 1~2일    소요됩니다.  단, 지역별/업체별 상황에 따라 배송 예정일이    변경될 수 있습니다.</li>
            <li>배송조회는 상품이 출고된 다음날부터 가능합니다.</li>
            <li>택배관련 문의는 담당자(02-3668-3434)에게 연락 주시기 바랍니다.</li>
        </ul>
        
        <strong class="ic-title2">기타안내</strong>
        <ul class="list-txt">
            <li>품절 안내<br />
				주문하신 상품이 품절인 경우 회원님께 문자로 알려 드리고, 경우에 따라 안내 전화를 드린 후 품절 취소 처리가 진행됩니다.
			</li>
            <li>주문 취소 안내<br />
            	상품 취소를 원하시는 경우는 한국컴패션   (연락처:02-3668-3434)로 연락 주시기 바랍니다.<br />
				상품이 출고 된 이후는 주문 취소가 불가능 합니다.
            </li>
        </ul>
        
    </div>
    <!--//배송안내 탭 내용-->

	 <!--상품후기 탭 내용-->
    <div class="store-detail-admin teb_content" data-idx="2"  style="display:none;">
        
        <ul class="list-board" ng-if="review.total > 0">
            <li class="question review_title"  ng-repeat-start="item in review.list"  data-idx="{{item.idx}}" ng-click="review.toggle(item.idx, $event)">   <!--클릭시 "selected" 클래스 추가-->
            	<span class="row-caption">{{item.body}}</span>
                <span class="row-date">{{item.user_id}}<span class="bar">|</span>{{item.reg_date | date:'yyyy.MM.dd'}}</span>
            </li>
            <li class="answer review_body" style="display:none"  data-idx="{{item.idx}}" ng-repeat-end>   <!--해당 답변 레이어 display-->
            	{{item.body}}
            </li>
        </ul>
		<ul class="list-board" ng-if="review.total == 0" style="margin-bottom:60px;">
			<li class="question">
				등록된 상품 후기가 없습니다.
			</li>
		</ul>
        
		<paging ng-if="review.total > 0"  class="small" page="review.params.page" page-size="review.params.rowsPerPage" total="review.total" show-prev-next="true" show-first-last="true" paging-action="review.getList({page : page});"></paging>  

        
        <div class="wrap-postwrite">
        	<a class="write btn_mainReview" ng-click="review.checkBuy()">후기 작성<span class="bt-bu1"></span></a>  <!--활성화 상태 :  "bt-bu1-active"  css 추가-->
            
            <div class="sub-write review" style="display:none">  <!--활성화 상태  :  display :block-->
            	<span class="row">
					<span style="display:inline-block; width:100%; text-align:right;"> <span ng-bind="review_content.length || 0" >11</span>/ 1000자</span>
                	<textarea id="review_content" name="review_content" maxlength="1000" ng-model="review_content" ng-bind="review_content" style="width:100%;height:70px">후기를 작성해주세요.</textarea>
                </span>
                <span class="wrap-bt">
                	<a class="bt-type8 cancel_review" href="#">취소</a>
                    <a class="bt-type6" ng-click="review.add()">완료</a>
                </span>
            </div>
        </div>
        
    </div>
    <!--//상품후기 탭 내용-->

	
    <!--상품문의 탭 내용-->
    <div class="store-detail-admin teb_content" data-idx="3" style="display:none;">
        
        <ul class="list-board" ng-if="qna.total > 0">
            <li class="question qna_title" ng-repeat-start="item in qna.list" data-idx="{{item.idx}}" ng-click="qna.toggle(item.idx, $event)">   <!--클릭시 "selected" 클래스 추가-->
            	<span class="row-width row-caption" ng-bind-html="item.question"></span>
                <span class="row-width row-date">{{item.user_id}}<span class="bar">|</span>{{item.reg_date | date:'yyyy.MM.dd'}}</span>
                <span class="state-ask">
                	<img class="security" src="/common/img/icon/lock.png" alt="비밀글" ng-if="item.isopen"/>
                    <em class="{{item.answerClass}}"  ng-bind-html="item.answerYN"></em>
                </span>
            </li>
            <li class="answer qna_body" data-idx="{{item.idx}}"  style="display:none;" ng-repeat-end>   <!--해당 답변 레이어 display-->
				<div class="answer_view">
					<span class="row-subcaption">
						<span ng-bind-html="item.question"></span>
						<div ng-if="item.is_owner">
							<span class="action"><a class="modify" href="#" ng-click="qna.toggleUpdate(item.idx, $event)" >수정</a> <a class="delete" href="#" ng-click="qna.delete(item.idx)">삭제</a></span>
						</div>
					</span>
					<strong class="color1" ng-if="item.answer">답변</strong>
					<span ng-if="item.answer">{{item.answer}}</span>
				</div>
				<div class="modify_div" style="display:none;"  ng-if="item.is_owner">
					<textarea class="modify_text" data-idx="{{item.idx}}" style="width:100%;height:70px">{{item.question}} </textarea>
					<span class="wrap-bt">
                		<a class="bt-type8" href="#" ng-click="qna.update(item.idx, $event)">수정</a>
					</span>
				</div>
            </li>
		</ul>
		<ul class="list-board" ng-if="qna.total == 0" style="margin-bottom:60px;">
			<li class="question">
				등록된 상품 문의가 없습니다.
			</li>
		</ul>
		
        <paging ng-if="qna.total > 0" class="small" page="qna.params.page" page-size="qna.params.rowsPerPage" total="qna.total" show-prev-next="true" show-first-last="true" paging-action="qna.getList({page : page});"></paging>  

        
        <div class="wrap-postwrite">
        	<a class="write btn_mainQna">문의 작성<span class="bt-bu1"></span></a>  <!--활성화 상태 :  "bt-bu1-active"  css 추가-->
            
            <div class="sub-write qna" style="display:none;">  <!--활성화 상태  :  display :block-->
            	<span class="checkbox-ui">
					<input type="checkbox" class="css-checkbox"  id="qna_isOpen" name="qna_isOpen" ng-model="qna_isOpen" ng-bind="qna_isOpen">
					<label for="qna_isOpen" class="css-label">비밀글</label>
                </span>
                <span class="row">
					<span style="display:inline-block; width:100%; text-align:right;"> <span ng-bind="qna_content.length || 0" >11</span>/ 1000자</span>
                	<textarea style="width:100%;height:70px" id="qna_content" maxlength="1000" name="qna_content" ng-model="qna_content">문의를 작성해주세요</textarea>
                </span>
                <span class="wrap-bt">
                	<a class="bt-type8 cancel_qna">취소</a>
                    <a class="bt-type6" ng-click="qna.add()">완료</a>
                </span>
            </div>
        </div>
    </div>
    <!--//상품문의 탭 내용-->




    
    <!--스토어 다른 상품-->
    <div class="wrap-headline">
    	<h2 class="txt-head">스토어 다른 상품</h2>
    </div>
    
    <div class="wrap-thumbil">
    	<ul class="list-thumbil list-type2">
			<asp:Repeater runat="server" ID="repeater_randomProduct">
				<ItemTemplate>
        			<li>
						<a ng-click='goView(<%#Eval("idx") %> , $event)' class="box-block">
            				<span class="photo"><img src="<%# (Uploader.GetRoot(Uploader.FileGroup.image_shop) + Eval("name_img").ToString()).WithFileServerHost() %>" alt="" /></span>
							<span class="info">
                				<span class="txt-title"><span class="textcrop-1row"><%#Eval("name") %></span></span>
								<span class="txt-price">￦ {{<%#Eval("selling_price") %> | number:N0}} </span>
							</span>
						</a>
        			</li>
				</ItemTemplate>
			</asp:Repeater>
        </ul>
    </div>
    
	<!-- 공지사항 -->
    <div class="store-noticetop">
    	<ul>
        	<li><a href="#" ng-click="getPopup.show('manage' , $event)"><span class="txt1">컴패션 스토어는<br />어떻게 운영할까요 ?</span><img src="/common/img/page/store/img-article1.jpg" alt="" /></a></li>
            <li><a href="#" ng-click="getPopup.show('profit' , $event)"><span class="txt1">컴패션 스토어의 수익금은<br />어떻게 쓰이나요?</span><img src="/common/img/page/store/img-article2.jpg" alt="" /></a></li>
            <li><a href="#" ng-click="getPopup.show('gift' , $event)"><span class="txt1">후원어린이에게 선물을<br />어떻게 하나요?</span><img src="/common/img/page/store/img-article3.jpg" alt="" /></a></li>
            <li><a ng-click="notice.goPage($event)"><strong class="txt2">공지사항</strong>
            	<span class="txt3">{{notice.list[0].title}}</span>
            	<time class="txt4">{{notice.list[0].dtreg | date:'yyyy.MM.dd'}}</time>
            <img src="/common/img/page/store/img-article4.png" alt="" /></a></li>
        </ul>
    </div>
    
<!--//-->
</div>


</asp:Content>
