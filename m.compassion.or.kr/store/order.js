﻿
(function () {

    var app = angular.module('cps.page', []);
    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, $address) {


        // 주소찾기
        // addressApiKey 제거
        $scope.findAddr = function ($event) {
            $event.preventDefault();

            $scope.scrollTop = $(document).scrollTop();
            $(".wrap-sectionsub").hide();

            popup.init($scope, "/common/popup/address", function (modal) {
                initAddress($scope, $address, modal, function (zipcode, addr1, addr2, jibun) {		// callback

                    //hidden

                    $("#addr_domestic_zipcode").val(zipcode);
                    $("#addr_domestic_addr1").val(addr1 + "//" + jibun);
                    $("#addr_domestic_addr2").val(addr2);
                    $("#addr_domestic_addr").val($("#addr_domestic_addr1").val() + " " + $("#addr_domestic_addr2").val());

                    // 화면에 표시
                    $("#addr_domestic_1").val(zipcode);
                    //$("#addr_domestic_2").val(addr2);

                    $("#addr_road").text("[도로명주소] " + addr1 + " " + addr2);
                    $("#addr_jibun").text("[지번주소] " + jibun + " " + addr2);

                }, function () {
                    $(".wrap-sectionsub").show();
                    window.scroll(0, $scope.scrollTop)
                });

            }, { top: 0, iscroll: true, removeWhenClose: true });

        }

    });

})();


$(function () {

    $page.init();
    // 컴파스의 데이타를 불러오는경우 
    if ($("#dspAddrDoro").val() != "") {
        $("#addr_road").text("[도로명주소] (" + $("#temp_zipcode").val() + ") " + $("#dspAddrDoro").val());
        if ($("#dspAddrJibun").val() != "") {
            $("#addr_jibun").text("[지번] (" + $("#temp_zipcode").val() + ") " + $("#dspAddrJibun").val());
        }

        $("#temp_addr1").val($("#dspAddrDoro").val());
        $("#temp_addr2").val($("#dspAddrJibun").val());

    } else {


        addr_array = $("#addr_domestic_addr1").val().split("//");
        $("#addr_road").text("[도로명주소] " + addr_array[0] + " " + $("#addr_domestic_addr2").val());
        if (addr_array[1]) {
            $("#addr_jibun").text("[지번주소] " + addr_array[1] + " " + $("#addr_domestic_addr2").val());
        }
    }


});


var $page = {

    processing: false,

    init: function () {

        $.get("/api/store.ashx", { t: "get_order_temp" }, function (r) {

            $page.processing = false;
            if (r.success) {

                //	console.log(r.data);

                var container = $("#item_container");
                var template = $(".template").text();

                var isGeneralProduct = false;

                $page.orderList = r.data;

                $.each(r.data, function () {
                    var row = $(template);
                    row.attr("data-gift", (this.gift_flag ? "Y" : "N"))
						.attr("data-order-price", (parseInt(this.orderprice)))
                        .attr("data-total-price", (parseInt(this.option1_price) + parseInt(this.selling_price)))
					    .attr("data-delivery-charge", parseInt(this.delivery_charge));

                    row.find("[data-id=img]").attr("src", this.name_img);
                    row.find("[data-id=title]").html(this.name).attr("href", "/store/item/" + this.product_idx);
                    row.find("[data-id=option]").html(this.option1_name == "" ? "없음" : "(옵션) " + this.option1_name);
                    row.find("[data-id=child]").html(this.childname);
                    row.find("[data-id=price]").html(this.selling_price.format() + "원");
                    row.find("[data-id=ea]").html(this.quantity);
                    row.find("[data-id=type]").html(this.gift_flag ? "어린이에게<br />보내는 선물" : "일반");
                    row.find("[data-id=total]").html(this.orderprice.format() + "원");

                    if (this.gift_flag) { }
                    else {
                        isGeneralProduct = true;
                    }

                    container.append(row);
                })

                $('#deliveryTitle').css('display', isGeneralProduct ? 'block' : 'none');
                $('#deliveryContent').css('display', isGeneralProduct ? 'block' : 'none');

                $page.calculate();

            } else {
                alert(r.message);
            }

        });

        $("#memo_template").change(function () {

            val = $(this).val();
            if (val == "기타") {
                $("#memo").val("");
                $("#memo").prop("placeholder", "내용을 입력해 주세요");
                $("#memo").prop("disabled", false);
            } else {
                $("#memo").val(val);
                $("#memo").prop("disabled", true);
            }
        });

        $("input[name=rdoAddr]").change(function () {
            var addrInfo = $(this).attr("id")
            if (addrInfo == "rdoAddrOld") {
                $("#name").val($("#temp_name").val());
                $("#mobile").val($("#temp_mobile").val());
                $("#addr_domestic_addr").val($("#addr_domestic_addr1").val() + " " + $("#addr_domestic_addr2").val());
                $("#addr_domestic_zipcode").val($("#temp_zipcode").val());


                if ($("#dspAddrDoro").val() != "") {
                    $("#addr_road").text("[도로명주소] (" + $("#temp_zipcode").val() + ") " + $("#temp_addr1").val());
                    $("#addr_jibun").text("[지번] (" + $("#temp_zipcode").val() + ") " + $("#temp_addr2").val());

                } else {
                    addr_array = $("#temp_addr1").val().split("//");
                    if (addr_array[0] != "") {
                        $("#addr_road").text("[도로명주소] " + addr_array[0] + " " + $("#temp_addr2").val());
                    }
                    if (addr_array[1]) {
                        $("#addr_jibun").text("[지번주소] " + addr_array[1] + " " + $("#temp_addr2").val());
                    }
                }
            } else if (addrInfo == "rdoAddrNew") {
                $("#name").val("");
                $("#mobile").val("");
                $("#addr_domestic_addr").val("");
                $("#addr_domestic_zipcode").val("");

                $("#addr_road").text("");
                $("#addr_jibun").text("");
            }
        })


        $("#btn_submit").click(function () {

            return $page.checkValid();
        })

        $(".wrap-order-click").click(function () {

            idx = $(this).data("idx")
            target = $(".wrap-order-sub[data-idx=" + idx + "]");

            $(this).find("span").toggleClass("bt-bu1-active");

            target.slideToggle(300);

        })

        $("#chk_agree").click(function () {
            blur();
        })

    },

    calculate: function () {

        var container = $("#item_container");
        var sub_total = 0, delivery_fee = 0, total_amount = 0, gift_item_sub_total = 0;
        //old
        //$.each(container.find(".item_row"), function () {
        //	var row = $(this);

        //	var price = parseInt(row.data("order-price"));
        //	var is_gift = row.data("gift") == "Y";

        //	sub_total += price;
        //	if (is_gift) {
        //		gift_item_sub_total += price;
        //	}

        //});

        //if (sub_total < 30000 && sub_total != gift_item_sub_total) {
        //	delivery_fee = 2500;
        //}

        //new
        var subTotalExceptGift = 0, maxDelFee = 0, nogiftCnt = 0, giftCnt = 0;
        $.each(container.find(".item_row"), function () {
            var row = $(this);
            var price = parseInt(row.data("total-price"));
            var ea = parseInt(row.find("[data-id=ea]").text());
            var is_gift = row.data("gift") == "Y";
            var delCharge = parseInt(row.data("delivery-charge"));
            sub_total += price * ea;
            if (is_gift) {
                gift_item_sub_total += price * ea;
                giftCnt++;
            }
            else {
                subTotalExceptGift += price * ea;
                maxDelFee = delCharge > maxDelFee ? delCharge : maxDelFee;
                nogiftCnt++;
            }

        });
        if (nogiftCnt > 0 && giftCnt >= 0 && sub_total < 30000) {
            delivery_fee = maxDelFee;
        }


        total_amount = sub_total + delivery_fee;
        $("#sub_total").html(sub_total.format());
        $("#delivery_fee").html(delivery_fee.format());
        $("#total_amount").html(total_amount.format());

        $("#sub_total2").html(sub_total.format() + "원");
        $("#delivery_fee2").html(delivery_fee.format() + "원");
        $("#total_amount2").html(total_amount.format() + "원");


        var good_name = $(container.find(".item_row")[0]).find("[data-id=title]").html();
        if (container.find(".item_row").length > 1) {
            good_name += " 외" + container.find(".item_row").length + "개";
        }

        $("#hd_good_mny").val(total_amount);
        $("#hd_good_name").val(good_name);
        $("#hd_delivery_fee").val(delivery_fee);

    },

    checkValid: function () {
        if (!validateForm([
			{ id: "#name", msg: "수령인을 입력해 주세요" },
			{ id: "#addr_domestic_zipcode", msg: "배송지정보를 선택해 주세요" },
            //{ id: "#addr_road", msg: "배송지 상세주소를 입력해주세요" },
			{ id: "#mobile", msg: "휴대폰번호를 입력해 주세요" }
        ])) {
            return false;
        }

        if (!$("#chk_agree").is(":checked")) {
            alert("구매에 동의하여 주세요.")
            return false;
        }

        //GA Product Start
        var paymentMethod = '';
        if ($('#payment_method_card').is(':checked')) paymentMethod = '신용카드';
        else if ($('#payment_method_cms').is(':checked')) paymentMethod = '실시간 계좌이체';
        else if ($('#payment_method_phone').is(':checked')) paymentMethod = '휴대폰 결제';

        var productList = [];
        $.each($page.orderList, function () {
            productList.push({
                'id': this.product_idx,  // 상품 코드           
                'name': this.name, // 상품 이름           
                'brand': '한국컴패션', // 브랜드           
                'category': this.product_gift_flag ? '어린이선물' : '일반상품', // 상품 카테고리           
                'price': this.selling_price, // 가격(\)           
                'quantity': this.quantity, // 제품 수량           
                'variant': this.option1_name // 상품 옵션    
            });
        });

        dataLayer.push({
            'event': 'Checkout',
            'ecommerce': {
                'currencyCode': 'KRW',
                'checkout': {
                    'actionField': { 'step': 2, 'option': paymentMethod }, // 결제 수단         
                    'products': productList
                }
            },
        });
        //GA Product End

        return true;
    },


}
