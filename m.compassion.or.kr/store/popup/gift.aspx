﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="gift.aspx.cs" Inherits="popup_gift"  %>
<div style="background: transparent; width:100%;">  
    
	<!-- 실제 width 지정 -->
	<div class="wrap-layerpop" style="width:80%;left:10%">
		<div class="header">
        	<p class="txt-title">후원어린이에게 선물을 어떻게 하나요?</p>
		</div>
		<div class="contents">
        	<strong>컴패션 스토어에서 ‘후원어린이에게 선물로 보낼 수 있는 상품’을 구매하면, 후원어린이에게 직접 전달됩니다.</strong><br /><br />
			자세한 내용은 공지사항의 안내를 참고바랍니다.
            
			<div class="wrap-bt"><a href="/store/notice/" class="bt-type4">공지사항 바로가기</a></div>
            
			문의 : 02-3668-3434
		</div>
		<div class="close"><span class="ic-close" ng-click="getPopup.hide($event)">레이어 닫기</span></div>
	</div>
    
</div>    
