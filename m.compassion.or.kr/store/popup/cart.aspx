﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cart.aspx.cs" Inherits="popup_cart"  %>

    
<div style="background:transparent; width:100%;" class="fn_pop_container">
	<!--레이어 안내 팝업(장바구니)-->
	
	<div class="wrap-layerpop" style="width:80%;left:10%;">
		<div class="header">
        	<p class="txt-title">상품이 장바구니에 담겼습니다.</p>
		</div>
		<div class="contents">
        	
			<a style="width:80%" href="/store/cart/" class="bt-type6">장바구니 바로가기</a>
			<span class="clear" style="height:10px"></span>
			<a style="width:80%" ng-click="getPopup.hide($event)" class="bt-type7">쇼핑 계속하기</a>
            
		</div>
		<div class="close"><span class="ic-close" ng-click="getPopup.hide($event)">레이어 닫기</span></div>
	</div>
     
	<!--//레이어 안내 팝업(장바구니)-->
</div>