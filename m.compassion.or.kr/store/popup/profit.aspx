﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="profit.aspx.cs" Inherits="popup_profit"  %>

    
<div style="background:transparent;width:100%;" class="fn_pop_container">
	<!--레이어 안내 팝업-->
	<div class="wrap-layerpop" style="width:80%;left:10%;">
		<div class="header">
        	<p class="txt-title">컴패션 스토어의 수익금은<br />어떻게 쓰이나요?</p>
		</div>
		<div class="contents">
        	<strong>컴패션 스토어의 수익금은 한국컴패션에 기부되어 전세계 가난한 어린이들을 위해 쓰여집니다.</strong><br /><br />
			기부된 수익금은 컴패션의 후원금과 같이 국제 컴패션을 통해 어린이가 등록되어 있는 수혜국  현지 컴패션 어린이센터로 전달되며, 후원 어린이의 전인적인 양육을 위해서 사용됩니다.
		</div>
		<div class="close"><span class="ic-close" ng-click="getPopup.hide($event)">레이어 닫기</span></div>
	</div>
	<!--//레이어 안내 팝업(장바구니)-->
</div>