﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="manage.aspx.cs" Inherits="popup_manage"  %>

    
<div style="background:transparent; width:100%;" class="fn_pop_container">
	<!--레이어 안내 팝업-->
	
		<div class="wrap-layerpop" style="width:80%;left:10%;">
			<div class="header">
        		<p class="txt-title">컴패션 스토어는 어떻게 운영 할까요?</p>
			</div>
			<div class="contents">
        		<strong>컴패션 스토어는 한국컴패션에서 운영하는 온라인 쇼핑몰입니다.</strong><br /><br />
				컴패션 스토어는 컴패션과 뜻을 함께하는 기업체, 디자이너, 후원자분의 협력으로 제작됩니다.
			</div>
			<div class="close"><span class="ic-close" ng-click="getPopup.hide($event)">레이어 닫기</span></div>
		</div>
    
<!--//레이어 안내 팝업-->
</div>