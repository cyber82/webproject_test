﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="store_notice" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/store/notice/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
<div class="wrap-sectionsub"  ng-app="cps" ng-cloak  ng-controller="noticeCtrl">
<!---->
    
    <div class="store-notice">
    	
        <div class="wrap-board">

        	<ul class="list-board">
            	<li ng-repeat="item in list" ng-if="total > 0"  ng-click="goView(item.idx, $event)">
					<a class="box-block" href="#">
						<strong class="txt-title {{item.is_new}}">   <!--새로운 글 경우만  "crop-new"  css 사용바랍니다.-->
                    		<span class="textcrop-1row ">{{item.title}}</span><span class="new"></span>
						</strong>
						<span class="txt-date">{{item.dtreg | date:'yyyy.MM.dd'}}<span class="bar">|</span>조회수  {{item.viewcount}}</span>
					</a>
            	</li>
				
				<!-- 게시글 없을때 -->
				<li class="no-content" ng-if="total == 0">등록된 글이 없습니다.</li>
				<!--//  -->

            </ul>
            
        </div>
        
		<paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>  

        
    </div>
    
<!--//-->
</div>



</asp:Content>
