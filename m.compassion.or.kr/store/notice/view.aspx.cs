﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class store_notice_view : MobileFrontBasePage {
	const string listPath = "/store/notice/";

	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();

		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1){
			Response.Redirect( listPath, true );
		}
		

		var isValid = new RequestValidator()
			.Add( "p", RequestValidator.Type.Numeric )
			.Validate( this.Context, listPath );

		base.PrimaryKey = requests[0];


		//Response.Write( PrimaryKey );
	}

	protected override void loadComplete(object sender, EventArgs e){
		ShowData();
	}


	void ShowData()
    {
        using (StoreDataContext dao = new StoreDataContext())
        {
            var exist = www6.selectQStore<notice>("idx", Convert.ToInt32(PrimaryKey));
            //if (!dao.notice.Any(p => p.idx == Convert.ToInt32(PrimaryKey)))
            if(!exist.Any())
            {
                Response.Redirect("/store/notice/");
                return;
            }

            var entity = exist[0]; //dao.notice.First(p => p.idx == Convert.ToInt32(PrimaryKey));

            title.Text = entity.title;
            regDate.Text = entity.dtReg.ToString() != "" ? ((DateTime)entity.dtReg).ToString("yyyy.MM.dd") : "";
            viewCount.Text = entity.viewCount.ToString() == "" ? "0" : entity.viewCount.ToString();
            content.Text = entity.body.IndexOf("src=\"/") == -1 ? entity.body : entity.body.Replace("src=\"/", "src=\"" + ConfigurationManager.AppSettings["domain_file"] + "/");

            //lb_filename.Text = entity.fileName;
            //lnFileName.NavigateUrl = entity.saveFileName.WithFileServerHost();

            var prev_idx = 0;
            var next_idx = 0;
            // 첫글 or 마지막글 인지 확인후 이전/다음글 idx 를 가져옴

            var exist2 = www6.selectQ2Store<notice>("idx < ", Convert.ToInt32(PrimaryKey));
            //if (dao.notice.Any(p => p.idx < Convert.ToInt32(PrimaryKey)))
            if(exist2.Any())
            {
                prev_idx = exist2.OrderByDescending(p => p.idx).ToList()[0].idx;//dao.notice.Where(p => p.idx < Convert.ToInt32(PrimaryKey)).OrderByDescending(p => p.idx).ToList()[0].idx; // 제일큰거
            }

            var exist3 = www6.selectQ2Store<notice>("idx > ", Convert.ToInt32(PrimaryKey));
            //if (dao.notice.Any(p => p.idx > Convert.ToInt32(PrimaryKey)))
            if(exist3.Any())
            {
                next_idx = exist3.OrderBy(p => p.idx).ToList()[0].idx; //dao.notice.Where(p => p.idx > Convert.ToInt32(PrimaryKey)).OrderBy(p => p.idx).ToList()[0].idx; // 제일 작은거 
            }


            if (prev_idx == 0) { btnPrev.Visible = false; } else { btnPrev.Attributes["data-idx"] = prev_idx.ToString(); }
            if (next_idx == 0) { btnNext.Visible = false; } else { btnNext.Attributes["data-idx"] = next_idx.ToString(); }

        }

	}
	
	

}