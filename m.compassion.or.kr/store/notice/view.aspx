﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="store_notice_view" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/store/notice/view.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<div class="wrap-sectionsub"   ng-app="cps" ng-cloak  ng-controller="noticeCtrl">
	<!---->
		<div class="store-notice">
    	
			<div class="wrap-board">

        		<div class="view-board">
            		<div class="txt-title sectionsub-margin1"><asp:Label runat="server" ID="title"/></div>
                
					<div class="txt-date">
                		<asp:Label runat="server" ID="regDate"/><span class="bar">|</span><span class="viewcount"></span><asp:Label runat="server" ID="viewCount"/>
					</div>
					<div class="editor-html">
						<asp:Label runat="server" ID="content"/>
					</div>
                
					<div class="page-indigator">
                		<a class="prev next_prev_btn" id="btnPrev" runat="server"><span class="arrow"></span>이전 글</a>
						<a class="next next_prev_btn" id="btnNext" runat="server">다음 글<span class="arrow"></span></a>
					</div>
                
					<div class="wrap-bt"><a style="width:100%" class="bt-type5"  runat="server" id="btnList">목록</a></div>
				</div>
            
			</div>
        
		</div>
    
	<!--//-->
	</div>
    
</asp:Content>
