﻿/// <reference path="default.js" />
$(function () {

	$page.init();

});

var $page = {

	processing: false,

	init: function () {

	},

}

var app = angular.module('cps.page', []);

app.controller("noticeCtrl", function ($scope, $http, $filter, paramService) {

	$scope.params = {
		page: 1,
		rowsPerPage: 5,
		type: '',
		keyword: ""
	};

	// 파라미터 초기화
	$scope.params = $.extend($scope.params, paramService.getParameterValues());

	$scope.goView = function () {
		$(".next_prev_btn").click(function () {
			idx = $(this).data("idx");

			if (idx != 0) {
				$http.get("/api/store.ashx?t=increase_notice_hit", { params: { idx: idx } }).success(function (r) {
					console.log(r);
					if (r.success) {
						location.href = "/store/notice/view/" + idx + "?" + $.param($scope.params);
					} else {
						alert(r.message);
					}
				});
			}
		})
	}


	$scope.goView();
});
