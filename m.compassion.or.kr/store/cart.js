﻿$(function () {

    $page.init();

});

var $page = {

    processing: false,

    init: function () {

        $("#btn_remove").click(function () {

            var container = $("#item_container");
            var checked = container.find("[data-id=chk]:checked");
            if (checked.length < 1) {
                alert("삭제하실 상품을 선택해주세요");
                return false;
            }

            if ($page.processing) return false;
            $page.processing = true;

            var ids = [], productList = [];
            $.each(checked, function () {
                ids.push($(this).data("group"));

                productList.push({
                    'id': $(this).data("product-idx"),  // 상품 코드           
                    'name': $(this).data("product-name"), // 상품 이름           
                    'brand': '한국컴패션', // 브랜드           
                    'category': $(this).data("product-gift-flag") == 'Y' ? '어린이선물' : '일반상품', // 상품 카테고리           
                    'price': $(this).data("product-price"), // 가격(\)           
                    'quantity': $(this).data("product-quantity"), // 제품 수량           
                    'variant': $(this).data("product-option1_name") // 상품 옵션    
                });
            })

            var jsonStr = $.toJSON(ids);

            $.post("/api/store.ashx", { t: "remove_basket", data: jsonStr }, function (r) {

                $page.processing = false;
                if (r.success) {
                    //GA Product Start
                    dataLayer.push({
                        'event': 'Remove',
                        'ecommerce': {
                            'currencyCode': 'KRW',
                            'remove': {
                                'products': productList
                            }
                        }
                    });
                    //GA Product End

                    $.each(checked, function () {
                        var basket_id = $(this).data("group");
                        $(".item_row[data-group='" + basket_id + "']").remove();
                    })

                    $page.calculate();


                    if ($("#total").text() == "0") {
                        $(".store-basket.empty").show();
                    } else {
                        $(".store-basket.full").show();
                    }

                } else {
                    alert(r.message);
                }

            });

            return false;
        });

        $("#btn_order , #btn_order_all").click(function () {

            var container = $("#item_container");
            var checked = ($(this).attr("id") == "btn_order") ? container.find("[data-id=chk]:checked") : container.find("[data-id=chk]");
            if (checked.length < 1) {
                alert("주문하실 상품을 선택해주세요");
                return false;
            }

            var ids = [], productList = [];
            $.each(checked, function () {
                ids.push($(this).data("group"));

                productList.push({
                    'id': $(this).data("product-idx"),  // 상품 코드           
                    'name': $(this).data("product-name"), // 상품 이름           
                    'brand': '한국컴패션', // 브랜드           
                    'category': $(this).data("product-gift-flag") == 'Y' ? '어린이선물' : '일반상품', // 상품 카테고리           
                    'price': $(this).data("product-price"), // 가격(\)           
                    'quantity': $(this).data("product-quantity"), // 제품 수량           
                    'variant': $(this).data("product-option1_name") // 상품 옵션    
                });
            })

            var jsonStr = $.toJSON(ids);

            if ($page.processing) return false;
            $page.processing = true;

            //GA Product Start
            dataLayer.push({
                'event': 'Checkout',
                'ecommerce': {
                    'currencyCode': 'KRW',
                    'checkout': {
                        'actionField': { 'step': 1 },
                        'products': productList
                    }
                }
            });
            //GA Product End

            $.post("/api/store.ashx", { t: "set_order_temp", data: jsonStr }, function (r) {

                $page.processing = false;
                if (r.success) {

                    location.href = "/store/order/";

                } else {
                    alert(r.message);
                }

            });

            return false;
        });

        $.get("/api/store.ashx", { t: "get_basket" }, function (r) {

            $page.processing = false;
            if (r.success) {

                if (!r.data.length) {
                    $(".store-basket.empty").show();
                } else {
                    $(".store-basket.full").show();
                }

                var container = $("#item_container");
                var template = $(".template").text();

                $.each(r.data, function () {

                    var row = $(template);
                    row.attr("data-group", this.order_idx);
                    row.find(".item_obj").attr("data-group", this.order_idx);

                    row.find(".item_obj").click(function () {
                        $page.calculate();
                    });
                    row.find("[data-id=img]").attr("src", this.name_img);
                    row.find("[data-id=title]").html(this.name).attr("href", "/store/item/" + this.product_idx);
                    row.find("[data-id=title2]").attr("href", "/store/item/" + this.product_idx);
                    row.find("[data-id=option]").html(this.option1_name == "" ? "옵션 없음" : "옵션 : " + this.option1_name);
                    row.find("[data-id=child]").html(this.childname == "" ? "" : "선물받을 어린이 : " + this.childname);
                    row.find("[data-id=price]").html(this.selling_price.format() + "원");
                    row.find("[data-id=ea]").val(this.quantity);
                    row.find("[data-id=type]").html(this.gift_flag ? "어린이에게 보내는 선물" : "일반 상품");
                    row.find("[data-id=total]").html(this.orderprice.format() + "원");

                    row.find("[data-id=chk]")
						.attr("data-gift", (this.gift_flag ? "Y" : "N"))
						.attr("data-total-price", (parseInt(this.option1_price) + parseInt(this.selling_price)))
						.attr("id", "chk_" + this.order_idx)
				        .attr("data-delivery-charge", parseInt(this.delivery_charge))
				        .attr("data-product-gift-flag", (this.product_gift_flag ? "Y" : "N"))
				        .attr("data-product-name", this.name)
                        .attr("data-product-price", parseInt(this.selling_price))
                        .attr("data-product-quantity", this.quantity)
                        .attr("data-product-option1_name", this.option1_name)
				        .attr("data-product-idx", this.product_idx);

                    row.find(".css-label").attr("for", "chk_" + this.order_idx);


                    row.find(".btn_one_remove")
                        .attr("data-group", this.order_idx)
					    .attr("data-product-gift-flag", (this.product_gift_flag ? "Y" : "N"))
                        .attr("data-product-name", this.name)
                        .attr("data-product-price", parseInt(this.selling_price))
                        .attr("data-product-quantity", this.quantity)
                        .attr("data-product-option1_name", this.option1_name)
				        .attr("data-product-idx", this.product_idx);

                    if (this.gift_flag) {
                        row.find("[data-id=btn_plus_ea]").hide();
                        row.find("[data-id=btn_minus_ea]").hide();
                        row.find("[data-id=ea]").addClass("tochild");
                    }

                    row.find("[data-id=btn_minus_ea]").attr("data-inventory", this.inventory).click(function () {
                        var group = $(this).data("group");

                        var ea = parseInt(row.find("[data-id=ea][data-group='" + group + "']").val()) - 1;

                        if (ea < 1) {
                            return false;
                        }

                        $.post("/api/store.ashx", { t: "update_basket", basket_id: group, ea: ea });
                        row.find("[data-id=ea][data-group='" + group + "']").val(ea);

                        var price = parseInt(row.find("[data-id=chk]").data("total-price"));
                        row.find("[data-id=total][data-group='" + group + "']").html((ea * price).format());

                        $page.calculate();
                        return false;
                    })

                    row.find("[data-id=btn_plus_ea]").attr("data-inventory", this.inventory).click(function () {

                        var group = $(this).data("group");
                        var inventory = $(this).data("inventory");

                        var ea = parseInt(row.find("[data-id=ea][data-group='" + group + "']").val()) + 1;

                        if (ea > inventory) {
                            alert('재고량이 부족 합니다.');
                            return false;
                        }

                        $.post("/api/store.ashx", { t: "update_basket", basket_id: group, ea: ea });
                        row.find("[data-id=ea][data-group='" + group + "']").val(ea);

                        var price = parseInt(row.find("[data-id=chk]").data("total-price"));
                        row.find("[data-id=total][data-group='" + group + "']").html((ea * price).format());

                        $page.calculate();
                        return false;
                    })


                    row.find("[data-id=chk]").click(function () {
                        if ($("[data-id=chk]").length != $("[data-id=chk]:checked").length) {
                            $("#chk_all").prop("checked", false);
                        } else {
                            $("#chk_all").prop("checked", true);
                        }
                    })



                    container.append(row);
                })

                $page.calculate();
                $page.afterSet();
            } else {
                alert(r.message);
            }

        });

        $("#chk_all").click(function () {
            if ($("#chk_all").prop("checked")) {
                $("input[type=checkbox]").prop("checked", true);
            } else {
                $("input[type=checkbox]").prop("checked", false);
            }

            $page.calculate();
        })


        $("#link").attr("href", "#");
        $("#link").click(function () {
            history.back();
            return false;
        });

    },

    calculate: function () {

        var container = $("#item_container");
        var sub_total = 0, delivery_fee = 0, total_amount = 0, gift_item_sub_total = 0;

        $("#total").html(container.find(".item_row").length);

        if ($("#total").text() == "0") {
            $(".store-basket.full").hide();
            $(".store-basket.empty").show();
        }

        //old
        //$.each(container.find(".item_row"), function () {
        //	var row = $(this);


        //	console.log(row.find("input[type=checkbox]").is(":checked"));
        //	if (row.find("input[type=checkbox]").is(":checked")) {
        //		var price = parseInt(row.find("[data-id=chk]").data("total-price"));
        //		var ea = parseInt(row.find("[data-id=ea]").val());
        //		var is_gift = row.find("[data-id=chk]").data("gift") == "Y";

        //		sub_total += price * ea;
        //		if (is_gift) {
        //			gift_item_sub_total += price * ea;
        //		}
        //	}

        //});

        //if (sub_total < 30000 && sub_total != gift_item_sub_total) {
        //	delivery_fee = 2500;
        //}

        //new : #12505 요청으로 수정
        //1) 3만원 미만 구매 시 배송비 설정된 금액대로 적용 – 배송비가 2,500원에서 인상 또는 인하될 수 있으며, 0원으로 무료배송 이벤트를 할 수 있도록.
        //2) 3만원 이상 구매 시 배송비 무료 – 배송비 설정된 금액에 관련 없이 총 주문금액이(장바구니에 담긴=결제할 총금액)이 3만원이상이면 무료배송 적용.
        //3) 어린이선물상품-후원어린이에게 선물하기를 통해 주문 할 경우 배송비 적용없음.
        //=> 3만원 미만시 배송비는 배송비중 max값, 3만원의 기준은 어린이선물과 관계없이 총합
        var subTotalExceptGift = 0, maxDelFee = 0, nogiftCnt = 0, giftCnt = 0;
        $.each(container.find(".item_row"), function () {
            var row = $(this);
            if (row.find("input[type=checkbox]").is(":checked")) {
                var price = parseInt(row.find("[data-id=chk]").data("total-price"));
                var ea = parseInt(row.find("[data-id=ea]").val());
                var is_gift = row.find("[data-id=chk]").data("gift") == "Y";
                var delCharge = parseInt(row.find("[data-id=chk]").data("delivery-charge"));
                sub_total += price * ea;
                if (is_gift) {
                    gift_item_sub_total += price * ea;
                    giftCnt++;
                }
                else {
                    subTotalExceptGift += price * ea;
                    maxDelFee = delCharge > maxDelFee ? delCharge : maxDelFee;
                    nogiftCnt++;
                }
            }
        });
        if (nogiftCnt > 0 && giftCnt >= 0 && sub_total < 30000) {
            delivery_fee = maxDelFee;
        }

        total_amount = sub_total + delivery_fee;
        $("#sub_total").html(sub_total.format());
        $("#delivery_fee").html(delivery_fee.format());
        $("#total_amount").html(total_amount.format());

    },

    afterSet: function () {

        $(".btn_one_remove").click(function () {
            var idx = [$(this).data("group")];
            var jsonStr = $.toJSON(idx);

            var productList = [];
            productList.push({
                'id': $(this).data("product-idx"),  // 상품 코드           
                'name': $(this).data("product-name"), // 상품 이름           
                'brand': '한국컴패션', // 브랜드           
                'category': $(this).data("product-gift-flag") == 'Y' ? '어린이선물' : '일반상품', // 상품 카테고리           
                'price': $(this).data("product-price"), // 가격(\)           
                'quantity': $(this).data("product-quantity"), // 제품 수량           
                'variant': $(this).data("product-option1_name") // 상품 옵션    
            });

            $.post("/api/store.ashx", { t: "remove_basket", data: jsonStr }, function (r) {
                //GA Product Start
                dataLayer.push({
                    'event': 'Remove',
                    'ecommerce': {
                        'currencyCode': 'KRW',
                        'remove': {
                            'products': productList
                        }
                    }
                });
                //GA Product End



                $page.processing = false;
                console.log(r)
                if (r.success) {
                    $(".item_row[data-group='" + idx + "']").remove();


                    $page.calculate();
                } else {
                    alert(r.message);
                }

            });

            return false;
        })
    }


}

