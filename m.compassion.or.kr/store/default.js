﻿$(function () {

	$page.init();
	
});

var $page = {

	init: function () {

		// store 안내 레이어 show/hide
	    $(".store_info .si_btn").click(function () {
	    	$(this).parent().find(".layer").fadeIn("fast");
	    	return false;
		})

		$(".btn_back").click(function () {
			$(".store_info .layer").fadeOut("fast");
			return false;
		})

		$page.setMainVisual();

	},

	setMainVisual: function () {
		new Swiper('.wrap-slider', {
			pagination: '.page-count',
			autoplay: 2000,
			loop:true,
			paginationClickable: true,
			nextButton: '.page-next',
			prevButton: '.page-prev',
			paginationBulletRender: function (index, className) {
				//<span class="recent">1페이지</span>
				//className = "recent " + index;
				return '<span class="' + className + '">' + (index + 1) + '</span>';
			},
			spaceBetween: 30
			//loop : true
		});
	},

};

var app = angular.module('cps.page', []);
app.controller("defaultCtrl", function ($scope, $http, popup, $filter, paramService) {

	$scope.total = -1;
	$scope.rowsPerPage = 4;
	$scope.list = null;

	$scope.params = {
		page: 1,
		rowsPerPage: $scope.rowsPerPage,
		gift_flag: -1,
		keyword: ""
	};

	// 파라미터 초기화
	$scope.params = $.extend($scope.params, paramService.getParameterValues());
	
	$scope.$watch('total', function (newValue, oldValue) {
		if (newValue !== oldValue) {
			
			if (oldValue < 0) return;

			setTimeout(function () {
				if (newValue == 0) {
					scrollTo($(".tab-type1"));
				} else {
					scrollTo($(".tab-type1"), 20);
				}
			})
			
		}
	});

	$scope.getList = function (params) {
		$scope.params = $.extend($scope.params, params);

		$http.get("/api/store.ashx?t=get_store_list", { params: $scope.params }).success(function (r) {
			console.log(r);
			if (r.success) {

				$scope.list = r.data;
				$scope.total = r.data.length > 0 ? r.data[0].total : 0;
				
				if ($scope.total == 0) {
					$scope.params.keyword = "";

				}

				if (params)
					scrollTo($(".tab-type1"), 20);
			
			} else {
				alert(r.message);
			}

		});
		
	}
    //검색
	$scope.search = function (params) {
		var keyword = $("#keyword").val();
		$scope.params.keyword = keyword;
		$scope.params.page = 1;
		$scope.getList();
		

	}

    // 검색결과 없을때 검색
	$scope.searchResultNone = function (params) {
	   // var keyword = $("#search").val();
        	    
		$scope.params.keyword = $("#keyword2").val();
	    $scope.params.page = 1;
	    $scope.getList();
	    
	}

	// 탭 이벤트 처리
	$scope.clickTabEvent = function (idx , $event){
		$scope.params.page = 1;
		$scope.getList({ gift_flag: idx });
		
		$event.preventDefault();

	}

	$scope.goView = function (idx, $event, productName, gift_flag) {
	    dataLayer.push({
	        'event': 'Click',
	        'ecommerce': {
	            'currencyCode': 'KRW',
	            'click': {
	                'actionField': { 'list': '스토어' }, // 상품 전시 영역          
	                'products': [{
	                    'id': idx,  // 상품 코드           
	                    'name': productName, // 상품 이름           
	                    'brand': '한국컴패션', // 브랜드           
	                    'category': gift_flag ? '어린이선물' : '일반상품' // 상품 카테고리                
	                }]
	            }
	        },
	    });

	    location.href = "/store/item/" + idx + "?" + $.param($scope.params);
	    $event.preventDefault();

	}

    //제품리스트 가기
	$scope.goProductList = function () {
	    location.href = "/store/";
	    
	}

	$scope.getPopup = {

		instance: null,
		item: null,

		show: function (target, $event) {
			popup.init($scope, "/store/popup/" + target, function (modal) {
				$scope.getPopup.instance = modal;

				modal.show();

			});

			$event.preventDefault();
		},

		hide: function ($event) {
			$event.preventDefault();
			if (!$scope.getPopup.instance)
				return;
			$scope.getPopup.instance.hide();

		},
	}


	// 공지사항 관련
	$scope.notice = {
		list: null,
		index: 0,
		total: 0,
		getList: function () {

			list = null;
			$http.get("/api/store.ashx?t=notice_list", { page: 1, rowsPerPage: 1, }).success(function (r) {
				console.log(r);
				if (r.success) {

					$scope.notice.list = r.data;

					$.each($scope.notice.list, function () {
						this.dtreg = new Date(this.dtreg);
						this.href = "/store/notice/view/" + this.idx;
					})

				} else {
					alert(r.message);
				}
			});
		},

		goPage: function ($event) {
			$event.preventDefault()
			idx = $scope.notice.list[0].idx;

			$http.get("/api/store.ashx?t=increase_notice_hit", { params: { idx: idx } }).success(function (r) {
				console.log(r);
				if (r.success) {
					location.href = "/store/notice/view/" + idx + "?" + $.param($scope.params);
				} else {
					alert(r.message);
				}
			});
		}
		
	},



	$scope.getList();
	$scope.notice.getList();
	//$scope.getPopup.show("manage");

});
