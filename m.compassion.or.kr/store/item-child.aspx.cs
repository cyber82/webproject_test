﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class item_child :MobileFrontBasePage {

	const string listPath = "/store/";

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		if (!UserInfo.IsLogin) {
			Response.ClearContent();
			return;
		}
		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect( listPath, true );
		}

		var isValid = new RequestValidator()
			.Add( "p", RequestValidator.Type.Numeric )
			.Validate( this.Context, listPath );

		base.PrimaryKey = requests[0];
		key.Value = requests[0];


	}

	protected override void loadComplete(object sender, EventArgs e) {

		ShowData();
	}


	void ShowData()
    {
        using (StoreDataContext dao = new StoreDataContext())
        {
            var exist = www6.selectQStore<product>("idx", Convert.ToInt32(PrimaryKey), "display_flag", 1);
            //if (!dao.product.Any(p => p.idx == Convert.ToInt32(PrimaryKey) && p.display_flag == true))
            if(!exist.Any())
            {
                Response.Redirect("/store/");
                return;
            }

            //var entity = dao.product.First(p => p.idx == Convert.ToInt32(PrimaryKey) && p.display_flag == true);
            var entity = www6.selectQFStore<product>("idx", Convert.ToInt32(PrimaryKey), "display_flag", 1);

            //var opts = dao.product_option.Where(p => p.product_idx == Convert.ToInt32(PrimaryKey) && p.option_level == 0 && p.del_flag == false && p.display == true).ToList();
            var opts = www6.selectQStore<product_option>("product_idx", Convert.ToInt32(PrimaryKey), "option_level", 0, "del_flag", 0, "display", 1);

            //var imgs = dao.product_image.Where(p => p.product_idx == Convert.ToInt32(PrimaryKey)).ToList();
            var imgs = www6.selectQStore<product_image>("product_idx", Convert.ToInt32(PrimaryKey));

            //btn_show_child.Visible = entity.gift_flag.Value;
            hd_title.Value = title.Text = entity.name;
            hd_price.Value = entity.selling_price.ToString();
            //price.Text = entity.selling_price.ToString( "N0" );
            //price2.Text = entity.selling_price.ToString( "N0" );
            //status.Text = entity.inventory > 0 ? "판매중" : "품절";
            //status2.Text = entity.inventory > 0 ? "판매중" : "품절";
            hd_inventory.Value = entity.inventory.ToString();
            //product_detail.Text = entity.product_detail;

            if (opts.Count > 0)
            {

                option.Items.Add(new ListItem("옵션을 선택하세요", ""));

                foreach (var opt in opts)
                {
                    var text = opt.option_name + (opt.option_price > 0 ? "(+" + opt.option_price.Value.ToString("N0") + "원)" : "");
                    var item = new ListItem(text, opt.idx.ToString());
                    item.Attributes["data-price"] = opt.option_price.Value.ToString();
                    option.Items.Add(item);
                }
            }
            else
            {
                option.Items.Add(new ListItem("옵션 없음", "0"));
            }

            image.Src = (Uploader.GetRoot(Uploader.FileGroup.image_shop) + imgs[0].saved_file_name).WithFileServerHost();

            if (UserInfo.IsLogin)
            {
                hd_user_id.Value = new UserInfo().UserId;
            }

            hd_item_id.Value = this.PrimaryKey.ToString();

        }
	}




}