﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="review.aspx.cs" Inherits="store_review" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/store/review.js"></script>
	<style>
		.1field-view{word-wrap:break-word;}
	</style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
<div class="wrap-sectionsub">
<!---->
    
    <div class="store-epilogue"  ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
    	
        <!--tab-->
        <div class="wrap-tab">
            <ul class="tab-type1">
                <li class="teb_menu selected" data-idx="0" style="width:50%"><a href="#">후기</a></li>
                <li class="teb_menu"  data-idx="1" style="width:50%"><a href="#">문의</a></li>
            </ul>
        </div>
        

		<!-- 후기 -->
		<div class="teb_content" data-idx="0">
			<div class="wrap-headline">
				<h3 class="txt-head-store-review">전체후기</h3> &nbsp; <span>글쓰기 작성은 해당 상품 페이지에서 가능합니다.</span>
			</div>
        
			<div class="wrap-thumbil">

        		<ul class="list2-thumbil">
				
					<!--Click 선택된 상태
                		li="field-list selected"   부분에 selected 추가해주시고,
						바로 밑 li  부분의 display 하시면 됩니다.
					-->

            		<li class="field-list review_title" data-idx="{{item.idx}}" ng-repeat-start="item in review.list"  ng-click="review.toggle(item.idx, $event)">
						<a class="box-block" href="#" >
                			<span class="photo"><img ng-src="{{item.name_img}}" alt="" /></span>
							<strong class="txt-title {{item.is_new}}">   <!--새로운 글 경우만  "crop-new"  css 사용바랍니다.-->
                    			<span class="textcrop-1row">{{item.name}}</span><span class="new"></span>
							</strong>
							<p class="txt-note">
                    			{{item.body}}
							</p>
							<span class="txt-date">{{item.user_id}} <span class="bar">|</span>{{item.reg_date | date:'yyyy.MM.dd'}}</span>
						</a>
            		</li>
					<li class="field-view review_body" data-idx="{{item.idx}}" ng-repeat-end>
                		<span class="wrap">
							{{item.body}}
						</span>
					</li>
					
					<!-- 게시글 없을때 -->
					<li class="no-content" ng-if="review.total == 0">등록된 글이 없습니다.</li>
					<!--//  -->

				</ul>
            
			</div>
        
        
			<paging ng-if="review.total > 0"  class="small" page="review.params.page" page-size="review.params.rowsPerPage" total="review.total" show-prev-next="true" show-first-last="true" paging-action="review.getList({page : page});"></paging>  

		</div>
		<!-- //후기 -->
		
		<!-- 문의 -->
		<div class="teb_content" data-idx="1" style="display:none;">

			 <div class="wrap-headline">
				<h3 class="txt-head-store-review">전체문의</h3> &nbsp; <span>글쓰기 작성은 해당 상품 페이지에서 가능합니다.</span>
			</div>
        
			<div class="wrap-thumbil">

				<ul class="list2-thumbil reply">    <!--답변상태  있는경우만 "reply"  css추가-->
            		<!--Click 선택된 상태
                		li="field-list selected"   부분에 selected 추가해주시고,
						바로 밑 li  부분의 display 하시면 됩니다.
					-->
					<li class="field-list qna_title" ng-repeat-start="item in qna.list" data-idx="{{item.idx}}" ng-click="qna.toggle(item.idx, $event)">
						<a class="box-block" href="#">
                			<span class="photo"><img ng-src="{{item.name_img}}" alt="" /></span>
							<strong class="txt-title">
                    			<span class="textcrop-1row">{{item.name}}</span>
							</strong>
							<p class="txt-note" ng-bind-html="item.question">
                    			
							</p>
							<span class="txt-date">{{item.user_id}} <span class="bar">|</span>{{item.reg_date | date:'yyyy.MM.dd'}}</span>
							<span class="{{item.answerClass}}" ng-bind-html="item.answerYN"></span>
						</a>
					</li>
					<li class="field-view qna_body"  data-idx="{{item.idx}}" ng-repeat-end>   <!--Click시 열린상태-->
                		<span class="wrap">
                    		<span class="txt-question" ng-bind-html="item.question">{{item.question}}</span>
                        
							<span class="title-reply" ng-if="item.answer">답변</span>
							<span ng-if="item.answer">{{item.answer}}</span>
						</span>
					</li>
					
					<!-- 게시글 없을때 -->
					<li class="no-content" ng-if="qna.qna_total == 0">등록된 글이 없습니다.</li>
					<!--//  -->

				</ul>
            
			</div>
       
			<paging ng-if="qna.total > 0"  class="small" page="qna.params.page" page-size="qna.params.rowsPerPage" total="qna.total" show-prev-next="true" show-first-last="true" paging-action="qna.getList({page : page});"></paging>  

		</div>
		<!-- //문의 -->
		


    </div>
<!--//-->
</div>


</asp:Content>

