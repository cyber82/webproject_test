﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mobile_attendance_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/mobile-attendance/default.js?=1.3"></script>

    <script type="text/javascript">
        $(function () {
            $(".define-stamptour .txt-title").click(function () {
                $(".define-stamptour .define").slideToggle("fast", function () {
                    if ($(this).is(":visible"))
                        $(this).css("display", "block");
                });
                $(".define-stamptour .txt-title").toggleClass("define-open");
            });
        });
    </script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <input type="hidden" runat="server" id="entity" />
        <!---->

        <div class="etc-attend" id="main_list">

            <div class="diagram sectionsub-margin1">
                참석하신 오프라인 모임에 <strong>출석체크</strong>하고,<br />
                <strong>자세한 행사 정보</strong>를 만나 보세요!
            <p class="txt1">( 스탬프 획득은 덤 )</p>
                <div class="bg">
                    <ol>
                        <li><em>하나!</em>이벤트 선택하기</li>
                        <li><em>두울!</em>이벤트 비밀번호 입력하기</li>
                        <li><em>세엣!</em>행사 상세 정보 확인하기</li>
                    </ol>
                    <span class="edge"></span>
                </div>
                <p class="txt2">스탬프 투어<span class="gap"></span>도장 획득!</p>
            </div>



            <div class="define-stamptour mt30">
                <div class="wrap-header">
                    <p class="txt-title">스탬프 투어란<span class="bu"></span></p>
                </div>
                <div class="define">
                    컴패션의 이벤트나 후원자 활동에 참여할 때마다 스탬프를 부여받아, 다채로운 컴패션의 서비스를 만나 가는 투어 활동입니다.<br />
                    <br />
                    후원자님만이 참여할 수 있는 온/오프라인 활동을 통해 즐거운 여행을 시작해 보세요!<br />
					<br />
					<p class="tar"><a href="/my/activity/stamp">스탬프 투어 바로가기</a></p>
                </div>
            </div>

            <ul class="list-event mt20">
                <li ng-repeat="item in list" ng-show="item.ma_display"><a ng-click="goView($event, item)">{{item.ma_title}}<span class="bu"></span></a></li>
                <li class="no_event" ng-hide="total">등록된 이벤트가 없습니다.</li>

            </ul>

            <div class="wrap-bt" ng-if="total > list.length"><a class="bt-type4" style="width: 90px" href="#" ng-click="showMore($event)">더 보기</a></div>


        </div>

        <div class="etc-attend" id="check_password" style="display: none">

            <p class="txt-caption mt20">{{item.ma_title}}</p>
            <div class="box-gray padding1 align-center">
                <p class="txt1">이벤트 비밀번호 입력</p>
                <input type="password" value="" placeholder="비밀번호를 입력해 주세요" style="width: 90%" id="password" class="number_only" maxlength="4" ng-enter="confirm($event)"/>
            </div>
            <div class="wrap-bt"><a class="bt-type6" style="width: 100%" ng-click="confirm($event)">확인</a></div>


        </div>

        <!--//-->
    </div>


</asp:Content>
