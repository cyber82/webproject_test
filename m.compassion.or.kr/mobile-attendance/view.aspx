﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="mobile_attendance_view" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript" src="/mobile-attendance/view.js"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <!---->

        <div class="etc-attend">

            <p class="txt-caption mt20">
                <asp:Literal ID="title" runat="server"></asp:Literal></p>
            <div class="box-gray align-center">

                <!--컨텐츠노출-->
                <div>
                    <asp:Literal ID="content" runat="server"></asp:Literal>
                </div>
                <!--//컨텐츠노출-->

            </div>
            <div class="wrap-bt"><a class="bt-type5" style="width: 100%" ng-click="goList($event)">목록</a></div>


        </div>

        <!--//-->
    </div>

</asp:Content>
