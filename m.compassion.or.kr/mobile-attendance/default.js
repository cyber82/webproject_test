﻿$(function () {
    if (document.referrer.indexOf("mobile-attendance/view/") > 0) {
        $(".title-depth").find("#link").attr("href", "javascript:window.history.go(-3);");
    } else {

        $(".title-depth").find("#link").attr("href", "javascript:window.history.go(-1);");
    }

});

(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, popup, paramService) {

        $scope.total = -1;
        $scope.list = [];
        var rowsPerPage = 10;

        $scope.params = {
            page: 1,
            rowsPerPage: 10,

        };

        // 파라미터 초기화
        $scope.params = $.extend($scope.params, paramService.getParameterValues());

        // list
        $scope.getList = function (params) {


            //console.log("getList", $scope.params);
            $http.get("/api/mobile-attendance.ashx?t=list", { params: $scope.params }).success(function (result) {
                $scope.list = $.merge($scope.list, result.data);
                $scope.total = result.data.length > 0 ? result.data[0].total : 0;
                    
                console.log($scope.list);
            });
            //if (params) scrollTo($(""));
        }

        $scope.showMore = function ($event) {
            $scope.getList();
            $scope.params.page++;
        }

        // 뷰에서 돌아왔을때 처리
        if ($scope.params.page > 1) {
            var page = $scope.params.page;
            $scope.params.rowsPerPage = rowsPerPage * page;
            $scope.params.page = 1;
            $scope.getList({}, function () {
                $scope.params.rowsPerPage = rowsPerPage;
                $scope.params.page = parseInt(page);
            });

        } else {
            $scope.getList({ page: 1 });
        }

        // 상세페이지
        $scope.goView = function ($event, item) {
            $event.preventDefault();

            // 스템프 ID 세션에 추가  
            //$http.post("/api/mobile-attendance.ashx?t=setstemp&idx=" + item.ma_s_id).success(function (result) {
            //})
            cookie.set("e_id", "14", 1);
            cookie.set("e_src", item.ma_s_id, 1);

            if (!common.isLogin()) {
                var r = getDomain() + "/mobile-attendance/default/" + item.ma_id + "?" + $.param($scope.params);
                if (confirm("로그인이 필요합니다. \n로그인 페이지로 이동하시겠습니까?")) {
                    location.href = "/login?" + "e_id=14&e_src=" + item.ma_s_id +(r ? "&mr=" + r : "");
                }
                return false;
            }

            $scope.item = item;


            $http.post("/api/mobile-attendance.ashx?t=check&idx=" + $scope.item.ma_id).success(function (result) {
            	if (result.data) {
            		// ma_url있는 경우 url페이지로 이동
            		if ($scope.item.ma_url == "") {
            			location.href = "/mobile-attendance/view/" + $scope.item.ma_id + "?" + $.param($scope.params);
            		} else {
            			location.href = $scope.item.ma_url;
            		}

                } else {
                    $("#main_list").hide();
                    $("#check_password").show();
                    return false;
                }

            });

        }

        //비밀번호 확인
        $scope.confirm = function ($event) {
            if ($("#password").val() == "") {
                alert("비밀번호를 입력해 주세요.");
                $("#password").focus();
                return false;
            }

            if ($("#password").val() == $scope.item.ma_password) {

                $http.post("/api/mobile-attendance.ashx?t=add&idx=" + $scope.item.ma_id + "&s_id=" + $scope.item.ma_s_id).success(function (result) {
                    if (result.success) {
                    	alert("출석체크가 완료되었습니다.")

                    	// ma_url있는 경우 url페이지로 이동
                    	if ($scope.item.ma_url == "") {
							location.href = "/mobile-attendance/view/" + $scope.item.ma_id + "?" + $.param($scope.params);
                    	} else {
                    		location.href = $scope.item.ma_url;
                    	}

                    } else {
                        alert(result.message)
                    }

                });
            } else {
                alert("비밀번호가 일치하지 않습니다.");
                $("#password").focus();
            }

        }

    	// 로그인 후 비밀번호 입력화면으로 셋팅
        $scope.setItem = function () {

        	if ($("#entity").val()) {
        	    $scope.item = $.parseJSON($("#entity").val());

        	    if (!common.checkLogin(getDomain() + "/mobile-attendance/default/" + $scope.item.ma_id + "?" + $.param($scope.params))) {
        	        return false;
        	    }

        		$("#main_list").hide();
        		$("#check_password").show();
        	}
        }

        $scope.setItem();
    });


})();