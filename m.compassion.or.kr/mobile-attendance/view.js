﻿(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, paramService) {

        $scope.listParams = paramService.getParameterValues();

        $scope.goList = function ($event) {
            $event.preventDefault();
            location.href = "/mobile-attendance/?" + $.param($scope.listParams);
        };

    });

})();
