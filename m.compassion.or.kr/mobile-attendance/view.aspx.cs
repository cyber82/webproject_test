﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class mobile_attendance_view : MobileFrontBasePage {

    protected override void OnBeforePostBack() {

        string listPath = "/mobile-attendance/";


        base.OnBeforePostBack();


        // check param
        var requests = Request.GetFriendlyUrlSegments();
        if(requests.Count < 1) {
            Response.Redirect(listPath, true);
        }
        var isValid = new RequestValidator()
            .Add("p", RequestValidator.Type.Numeric)
            .Validate(this.Context, listPath);

        if(!requests[0].CheckNumeric()) {
            Response.Redirect(listPath, true);
        }

        base.PrimaryKey = requests[0];
    

    }

    protected override void loadComplete( object sender, EventArgs e )
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.mobile_attendance.First(p => p.ma_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<mobile_attendance>("ma_id", Convert.ToInt32(PrimaryKey));

            title.Text = entity.ma_title;
            content.Text = entity.ma_content.WithFileServerHost();

        }
    }
}




