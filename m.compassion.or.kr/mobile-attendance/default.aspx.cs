﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class mobile_attendance_default : MobileFrontBasePage {


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count > 0) {
			base.PrimaryKey = requests[0];
			GetItem();
		}
	}


	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();

	}

	protected void GetItem()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var item = dao.mobile_attendance.FirstOrDefault(p => p.ma_id == Convert.ToInt32(PrimaryKey));
            var item = www6.selectQF<mobile_attendance>("ma_id", Convert.ToInt32(PrimaryKey));

            if (item != null) entity.Value = item.ToJson();
        }
	}


}