﻿<%@ WebHandler Language="C#" Class="popup_list" %>

using System;
using System.Web;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Serialization;
using drawing = System.Drawing;
using NLog;
using System.Net;
using System.Collections;
using CommonLib;

public class popup_list : IHttpHandler {

    public void ProcessRequest(HttpContext context) {

        var type = context.Request["type"].ValueIfNull("layer");
        var device = context.Request["device"].ValueIfNull("mobile");

        JsonWriter.Write(this.getList(type , device), context);
    }

    List<sp_popup_list_fResult> getList(string type , string device)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //return dao.sp_popup_list_f(type, device).Take(1).ToList();
            //var list = dao.sp_popup_list_f(type, device).ToList();
            Object[] op1 = new Object[] { "p_type", "p_device" };
            Object[] op2 = new Object[] { type, device };
            var list = www6.selectSP("sp_popup_list_f", op1, op2).DataTableToList<sp_popup_list_fResult>();

            foreach (var item in list)
            {
                item.p_content = item.p_content.WithFileServerHost();
            }
            return list;
        }

    }

    public bool IsReusable {
        get {
            return false;
        }
    }
}