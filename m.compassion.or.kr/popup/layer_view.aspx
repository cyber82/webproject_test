﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="layer_view.aspx.cs" Inherits="popup_layer_view" %>

<div class="layer_popup" data-id="<%=cookieId %>">

    <script type="text/javascript">
		
        $(function () {
            //스크롤 고정
            $('body').bind('touchmove', function(e){e.preventDefault()});
            // 고정팝업 풋터삭제
            if ($(".layer_popup").data("id").length == 13) {
                $(".lp_footer").remove();
            }
            if ($("head").find("script[src='/popup/popupManager.js']").length < 1) {
                var s = $("script");
                s.attr("type", 'text/javascript');
                s.attr("src", '/popup/popupManager.js');
                $("head").append(s);
            }


            $(".nomore_today").unbind("click");
            $(".nomore_today").click(function () {
                
                var cookieId = $(this).attr("data-id");
                var notToday = $(this).attr("data-set-cookie");
                if (notToday == "true") {
                    popupManager.setCookie(cookieId, "today", <%:remindDays%>);
            }
				
			    $(".layer_popup[data-id='" + cookieId + "']").fadeOut(function(){
				
			        $(".layer_popup[data-id='" + cookieId + "']").remove();

			        // 관리자 팝업
			        // 관리자 등록 팝업이 아닌 custom 팝업은 13자리 아이디를 가진다.
			        if ($(this).data("id").length == 13) {
                        
			            if (!$(".btn_lp_close").hasClass("fix_pop")) {
			                popupManager.openAdminPop(true, "layer", "mobile");
			            }

			        }

			        if ($(".layer_popup").length < 1){
			            $(".layer_pop2").fadeOut();
			        }

			    });
                //스크롤 해제
			    $('body').unbind('touchmove');
			});
            			
            $(".btn_lp_close").unbind("click").bind("click", function () {
                
                var data_id = $(this).data("id");
                console.log(data_id)
                $(".layer_popup[data-id=" + data_id + "]").remove();

                // 관리자 팝업
                // 관리자 등록 팝업이 아닌 custom 팝업은 13자리 아이디를 가진다.
                if (!$(".btn_lp_close").hasClass("fix_pop")) {
                    
                    if (data_id.length == 13) {
                        popupManager.openAdminPop(true, "layer", "mobile");

                        
                    }
                }
                //스크롤 해제
                $('body').unbind('touchmove');
            });

	        //	var popup = $("div[data-role=main_popup]");
	        //	modalShow(popup, null, true, false);
	        //	popup.css({ top: "80px" });


	        var content = $(".lp_wrap[data-id='<%=cookieId %>']");
	        //var left = ($(window).width() - content.width()) / 2;
	        var left = "0";
	        var top = ($(window).height() - content.height()) / 2;
	        //content.css({left : left , top : "10%" , width : "80%" }) 
	        content.css({left : left , top : "0" , width : "100%" }) 
			
	        $(".layer_popup[data-id='<%=cookieId %>']").show();

			if ($(".layer_popup").length < 2){
			    $(".layer_pop2[data-id='<%=cookieId %>']").show();
			    $(".layer_pop2").bind("touchmove" , function(e){
			        e.preventDefault();
			    })
			}else{
			    $(".layer_pop2[data-id='<%=cookieId %>']").remove();
			}

	    });

    </script>

    <div data-id="<%=cookieId %>" style="display: none; position: fixed; margin: auto; left: 0; top: 0; width: 100%; height: 100%; z-index: 100000; background: #fff; opacity: 1" class="layer_pop2"></div>

    <div data-id="<%=cookieId %>" class="lp_wrap admin_html" style="position: fixed; z-index: 100001">
        <%=content %>
        <div class="today_labeltxt1">
        </div>

        <div class="wrap-layerpop lp_footer">
            <div class="contents2">
                <div class="today nomore_today" data-id="<%=cookieId %>" data-set-cookie="true"><span class="txt" onclick=""><%= msg %></span></div>
            </div>

        </div>

    </div>

</div>
