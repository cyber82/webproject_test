﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class popup_layer_view : System.Web.UI.Page {

    public string cookieId;
    public string content;
    public string p_width;
    public string p_height;
    public string p_left;
    public string p_top;
    public string remindDays;
    public string msg = "오늘 하루 이 창을 열지 않습니다.";

    protected void Page_Load( object sender, EventArgs e ) {

        var c = Request["c"];
        remindDays = Request["rd"].ValueIfNull("1");

        if(remindDays == "7") {
            msg = "1주일 동안 이 창을 열지 않습니다.";
        } else if(remindDays == "14") {
            msg = "2주일 동안 이 창을 열지 않습니다.";
        }

        if(c == "temp")
        {
            cookieId = Request["cookieId"];
            content = Request["content"].ToHtml();

            p_width = Request["width"];
            p_height = Request["height"];
            p_left = Request["left"];
            p_top = Request["top"];

        }
        else
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                try
                {
                    //var entity = dao.popup.FirstOrDefault(p => p.p_id == Convert.ToInt32(c));
                    var entity = www6.selectQF<popup>("p_id", Convert.ToInt32(c));
                    if (entity == null)
                    {
                        cookieId = Request["cookieId"];
                        content = Request["content"].ToHtml(false);
                        p_width = Request["width"];
                        p_height = Request["height"];
                        p_left = Request["left"];
                        p_top = Request["top"];

                    }
                    else
                    {
                        content = entity.p_content.WithFileServerHost();
                        cookieId = "popup_" + entity.p_id.ToString();
                        p_width = entity.p_width.ToString();
                        p_height = entity.p_height.ToString();
                        p_top = entity.p_top.ToString();
                        p_left = entity.p_left.ToString();

                    }

                }
                catch
                {

                    Response.Clear();
                    Response.End();
                }
            }

        }


    }
}