﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="my.aspx.cs" Inherits="app_my" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/app/my.js"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
<input type="hidden" id="data" runat="server" />

<div class="wrap-sectionsub-my appContent bg-appmy" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">    <!--마이컴패션 디자인-->

	 <div class="my-appmain">
    	
		<div class="supporter sectionsub-margin2">
        	<p><strong><asp:Literal runat="server" ID="userName" /> </strong> 후원자님</p>
            <asp:PlaceHolder runat="server" ID="ph_conid" Visible="false"><p>후원자번호<span class="no"><asp:Literal runat="server" ID="conId" /> </span></p></asp:PlaceHolder>
        </div>
		
        <div class="my-box padding1 margin1">
			<ul class="dash-board">
				<li><a href="/app/children/"><span class="menu">나의 어린이</span><span class="num">{{data.count_cdsp}}</span></a></li>
				<li><a href="/my/sponsor/"><span class="menu">특별한 나눔</span><span class="num">{{data.count_non_cdsp}}</span></a></li>
				<li><a href="/my/user-funding/join"><span class="menu">참여한 펀딩</span><span class="num">{{data.count_join_funding}}</span></a></li>
				<li><a href="/my/user-funding/create"><span class="menu">개설한 펀딩</span><span class="num">{{data.count_make_funding}}</span></a></li>
			</ul>
		</div>

		<ul class="mainMenu">
			<li class="my-box"><a href="/my/sponsor/">후원 관리</a></li>
			<li class="my-box"><a href="/my/user-funding/join">나의 펀딩 관리</a></li>
			<li class="my-box"><a href="/my/activity/CAD">나의 참여/활동</a></li>
			<li class="my-box"><a href="/my/store/order/">스토어 주문내역</a></li>
			<li class="my-box"><a href="/my/qna/">문의내역</a></li>
		</ul>
		
		<div class="btn-logout">
			<a class="bt-type6" style="width:100%" href="#" ng-click="logout($event)">로그아웃</a>
		</div>

    </div>
   </div>

	
</asp:Content>

