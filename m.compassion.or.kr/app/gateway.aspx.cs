﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using CommonLib;

public partial class app_gateway : MobileFrontBasePage {

	public string v = "";
	public string build = "";
	public string device = "";
	public string statusbar_height = "0";
	
	public device_version deviceInfo = null;
	

	public override bool NoCache {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {

		// 로그오프후 로그인 하는 경우 device , v 는 넘어오지 않는다.
		device = Request.QueryString["device"].EmptyIfNull();
		v = Request.QueryString["v"].ValueIfNull("1.0.0");
		build = WebConfigurationManager.AppSettings["build"].ToString();
		if(build == "auto") {
			build = DateTime.Now.ToString("ddHHmmss");
		}

		AppSession.SetCookie(this.Context, new AppSession.Entity() {
			version = v, build = build, device = device
		});


        // app version 체크
        using (FrontDataContext dao = new FrontDataContext())
        {
            //deviceInfo = dao.device_version.FirstOrDefault(p => p.dv_id == device);
            deviceInfo = www6.selectQF<device_version>("dv_id", device);
            if (v == null)
            {
                deviceInfo = new device_version()
                {
                    dv_min = "0"
                };
            }
        }

		info.Value = deviceInfo.ToJson();

		// intro cookie 가 없으면 인트로로 보낸다.
		HttpCookie cookie = Request.Cookies["cps.app.intro"];
		if(cookie == null || cookie.Value != "1") {
			next_url.Value = "/app/intro/";
			return;
		}

        cookie = this.Context.Request.Cookies[Constants.APP_ROOT_COOKIE_NAME];
        if(cookie == null) {
			next_url.Value = "/app/main";
        } else if(cookie.Value == "child") {
			next_url.Value = "/app/children/";

        } else if(cookie.Value == "letter") {
			next_url.Value = "/my/letter/";

        } else if(cookie.Value == "sponsor") {
			next_url.Value = "/sponsor/children/";

        } else if(cookie.Value == "event") {
			next_url.Value = "/participation/event/";

        } else if(cookie.Value == "more") {
			next_url.Value = "/app/more/";

		}else {
			next_url.Value = "/app/main";
		}

		//next_url.Value += "?app_hide_intro=Y";
	}


}