﻿$(function () {
    $(".appPageNav").find(".back").attr("href", "javascript:history.go(-1);");

});


(function () {

    var app = angular.module('cps.page', []);
    app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

        $scope.data = null;

        // 데이타
        $scope.getData = function () {

            $http.get("/api/my/main.ashx?t=data", { params: {} }).success(function (r) {
                //	console.log(r);
                if (r.success) {

                    $scope.data = r.data;
                    console.log("main", $scope.data);

                    if (r.data.child) {
                        var name = r.data.child.name;
                        var regex = /\(([^)]+)\)/;
                        var matches = regex.exec(name);
                        var en_name = matches[1];
                        var ko_name = name.replace("(" + matches[1] + ")", "");
                        r.data.child.en_name = en_name;
                        r.data.child.ko_name = ko_name;

                    }


                } else {
                    if (r.action == "not_sponsor") {
                        $scope.data = r.data;
                    }
                    //alert(r.message);
                }
            });

        }


        $scope.getData();

    });

})();