﻿
$(function () {
    sympathyVisual.init();
});



var sympathyVisual = {
	init: function () {
		var root = $("#sympathy_container");
	
		// 메인비주얼
		if (root.length < 1)
			return;

		root.find("#sympathy_visual").slidesjs({
		    width: $(".more-main-visual").width(),
		    height: $(".more-main-visual").height(),
		    start: 1,

			play: {

				active: true,
				effect: "slide",
				auto: false,
				swap: true,
				pauseOnHover: false,
				restartDelay: 2500,
			    
			},

			effect: {
				slide: {
					speed: 1000
				},
				fade: {
					speed: 300,
					crossfade: true
				}
			},

			callback: {
				loaded: function () {
					
				},
				start: function (number) {

				},
				complete: function (number) {
				
				}
			},
			navigation: {
			    active: true,
			    // [boolean] Generates next and previous buttons.
			    // You can set to false and use your own buttons.
			    // User defined buttons must have the following:
			    // previous button: class="slidesjs-previous slidesjs-navigation"
			    // next button: class="slidesjs-next slidesjs-navigation"
			    effect: "slide"
			    // [string] Can be either "slide" or "fade".
			},
			pagination: {
			    active: false,
			    // [boolean] Create pagination items.
			    // You cannot use your own pagination. Sorry.
			    effect: "slide"
			    // [string] Can be either "slide" or "fade".
			},
			play: {
			    active: false,
			    effect: "slide",
			    auto: false,
			    swap: false,
			    pauseOnHover: false,
			    restartDelay: 2500
			  
			}


		});
		$(".slidesjs-navigation").hide();
		root.find("#sympathy_visual_prev").click(function () {
		    root.find(".slidesjs-previous").trigger("click");
		    return false;
		});

		root.find("#sympathy_visual_next").click(function () {
		    root.find(".slidesjs-next").trigger("click");
		    return false;
		});

	}
}
