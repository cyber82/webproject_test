﻿
var app = angular.module('cps.page', []);
app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService, bridge) {

	$.get("/api/app?action=version&device=" + $rootScope.appDevice, function (r) {

		var success = true;
		if (r.data.length < 1) {
			alert("버전정보를 가져오는데 실패했습니다.");

			hybrid_api.killApp();

			success = false;

		} else {

			var cur_version = cookie.get("appVersion");
			var info = r.data[0];
			if (versionCompare(info.dv_min, cur_version, { zeroExtend: true }) > 0) {

				//var popup = $rootScope.confirm("앱 실행이 가능한 최소 버젼은 " + info.dv_min + " 입니다.\n고객님의 앱 버젼은 " + cur_version + "입니다.\n최신 앱을 설치 하시겠습니까?");
				var popup = $rootScope.confirm("앱 실행이 가능한 최소 버젼은 " + info.dv_min + " 입니다.\n최신 앱을 설치 하시겠습니까?");
				popup.then(function (res) {
					if (res) {
						hybrid_api.installNewVersion(info);
					} else {
						hybrid_api.killApp();
					}
				});

				/*
			} else if (versionCompare(info.dv_max, val, { zeroExtend: true }) > 0) {
				if (confirm("최신버전은 " + info.dv_max + " 입니다.\n고객님의 앱 버젼은 " + val + "입니다.\n최신 앱을 설치 하시겠습니까?")) {
					hybrid_api.installNewVersion(info);
					success = false;
				}
				*/
			} else {

				hybrid_api.checkStartPage();

			}


		}

	});

});

