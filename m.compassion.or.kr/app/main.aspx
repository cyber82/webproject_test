﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="main.aspx.cs" Inherits="app_main" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/app/main.js"></script>

	<script src="/common/js/util/swiper.min.js"></script>
	<link href="/common/css/swiper.css" rel="stylesheet" />

	<script type="text/javascript" src="/popup/popupManager.js"></script>
	<script type="text/javascript">
		$(function(){
			popupManager.openAll(true , "layer" , "app");
		})
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	
	<!-- 컨텐츠 영역 -->
	<div class="appContent bg-appmain">
    
		<!--가정의달캠페인-->
		<section class="main-field2 appContent-margin1">
			<h2>가정의 달 캠페인</h2>
			<div class="wrap-slider swiper-container">
				<div class="swiper-wrapper">
				<asp:Repeater runat="server" ID="repeater_event">
					<ItemTemplate>
		
    				<div class="swiper-slide"><a href="<%#Eval("mp_image_link") %>" target="<%#Eval("mp_is_new_window").ToString() == "True" ? "_blank" : "_self" %>"><img src="<%#Eval("mp_image").ToString().WithFileServerHost()%>" alt="" width="100%" /></a></div>

					</ItemTemplate>
				</asp:Repeater>
				</div>

				<div class="page-prev">이전보기</div>
				<div class="page-next">다음보기</div>
				<div class="page-count">
					<span class="recent">1페이지</span>
					<span>2페이지</span>
					<span>3페이지</span>
				</div>
			</div>
		</section>
    
		<!--비로그인 사용자가 페이지 진입 시 안내 문구와 [회원가입] 버튼 노출-->
		<section class="unauthorized-user appContent-margin1" runat="server" ID="login_section">
    		<h2>한국컴패션 회원이 되시면 편리한<br />후원 관리와 다양한 캠페인/이벤트 참여가 가능합니다.</h2>
			<div class="wrap-bt mt20">
        		<a class="bt-type7 fl" style="width:49%" href="/login">로그인</a>
				<a class="bt-type6 fr" style="width:49%" href="<%:ConfigurationManager.AppSettings["domain_auth"] + "/m/join/" %>">회원가입</a>
				<!--?app_ex=1-->
			</div>
		</section>
    
		<section class="appmain-sponsor">
    		<h2>후원</h2>
			<div class="box-app">
        		<p class="txt-caption">1:1어린이양육</p>
				<span class="txt-count"><asp:Literal runat="server" ID="children"/></span>
				<strong><asp:Literal runat="server" ID="main_content"></asp:Literal></strong>
				키움과 돌봄의 감동적인 1:1만남 지금 시작하세요.
				<div class="wrap-bt mt20">
					<a class="bt-type4" style="width:60%" href="/sponsor/children/">어린이 만나러 가기</a>
				</div>
			</div>
		</section>

		<div class="appmain-banner">
    		<a href="/sponsor/custom/"><img src="/common/img/app/banner1.jpg" alt="맞춤후원 나와 맞는 어린이를 찾고 싶어요" /></a>
		</div>
    
		<!-- 특별한 나눔 -->
		<asp:PlaceHolder runat="server" ID="ph_specialFunding">
		<section class="appmain-specical sponsor-funding">
			<div class="box-app">
        		<h2>특별한 나눔</h2>
				<a class="link-view" href="/sponsor/special/">전체보기</a>
            
				<div class="wrap-thumbil">
					<ul class="list-thumbil width100">
						<li>
							<span class="photo" style="background:url('<%:sf_entity.sf_image_m%>') no-repeat center;">
								<p class="txt-dday"><asp:Literal runat="server" ID="sf_days" /></p>
								<span class="wrap-graph" style="display:<%:sf_visible %>">
									<span class="txt-flag" style="left:<%:sf_percent %>%"><em><%:sf_percent %>%</em><%:sf_percent >= 100 || sf_remain_days < 0 ? "달성" : "달성중" %></span>   <!--텍스트좌표 left % 수치-->
									<span class="flag"	 style="left:<%:sf_percent %>%"><%:sf_percent %>%</span>   <!--하트좌표 left % 수치-->
									<span class="bg-graph">
										<span class="rate" style="width:<%:sf_percent %>%"></span>      <!--노란그래프 width % 수치-->
									</span>
									<span class="txt-graph">
										<span class="txt1"><%:sf_entity.sf_current_amount.ToString("N0") %>원</span>
										<span class="txt2"><%:sf_entity.sf_goal_amount.ToString("N0") %>원</span>
									</span>
								</span>
							</span>
							<span class="sponsor-fundinginfo">
								<span class="txt-title"><%:sf_entity.CampaignName %></span>
								<span class="txt-note"><%:sf_entity.sf_summary %></span>
							</span>
							<span class="wrap-bt mt0">
								<a class="bt-type4" style="width:60%" href="/sponsor/special/view/<%:sf_entity.CampaignID %>">더 보기</a>
							</span>
						</li>
					</ul>
				</div>
			</div>
		</section>
    </asp:PlaceHolder>
		<!-- 나눔펀딩 -->
		<section class="appmain-specical sponsor-funding">
			<div class="box-app">
        		<h2>나눔펀딩</h2>
				<a class="link-view" href="/sponsor/user-funding/">전체보기</a>
            
				<div class="wrap-thumbil">
					<ul class="list-thumbil width100">
						<li>
							<span class="photo" style="background:url('<%:uf_entity.uf_image%>') no-repeat center;">
								<p class="txt-dday"><asp:Literal runat="server" ID="uf_days" /></p>
								<span class="wrap-graph">
									<span class="txt-flag" style="left:<%:uf_percent %>%"><em><%:uf_percent %>%</em><%:uf_percent >= 100 ? "달성" : "달성중" %> </span>   <!--텍스트좌표 left % 수치-->
									<span class="flag"	 style="left:<%:uf_percent %>%"><%:uf_percent %>%</span>   <!--하트좌표 left % 수치-->
									<span class="bg-graph">
										<span class="rate" style="width:<%:uf_percent %>%"></span>      <!--노란그래프 width % 수치-->
									</span>
									<span class="txt-graph">
										<span class="txt1"><%:uf_entity.uf_current_amount.ToString("N0") %>원</span>
										<span class="txt2"><%:uf_entity.uf_goal_amount.ToString("N0") %>원</span>
									</span>
								</span>
							</span>
							<span class="sponsor-fundinginfo">
								<span class="txt-title"><%:uf_entity.uf_title %> </span>
								<span class="txt-note"><%:uf_entity.uf_summary %><br />나눔 크리에이터 <span class="bar">|</span> <span style="color:#333"><%:uf_entity.SponsorName %></span></span>
							</span>
							<span class="wrap-bt mt0">
								<a class="bt-type4" style="width:60%" href="/sponsor/user-funding/view/<%:uf_entity.uf_id%>">더 보기</a>
							</span>
						</li>
					</ul>
				</div>
			</div>
		</section>
    

		<!-- 공감 -->
		<section class="main-field5 appContent-margin1">
			<h2>공감</h2>
			<div class="wrap-slider swiper-container2">
            	<div class="swiper-wrapper">
				<asp:Repeater runat="server" ID="repeater_sympathy">
					<ItemTemplate>
					<div class="item-field swiper-slide">
						<a href="/sympathy/view/<%#Eval("board_id") %>/<%#Eval("idx") %>?page=1&rowsPerPage=10&s_type=<%#Eval("board_id") %>">
							<div class="photo" style="background:url('<%#Eval("thumb").ToString().WithFileServerHost() %>') no-repeat center top;background-size:cover;"></div>
							<div class="caption textcrop-1row"><%#Eval("board_name") %></div>
							<div class="txt">
								<p class="textcrop-1row"><%#Eval("title") %></p>
								<p class="textcrop-1row"><%#Eval("sub_title") %><p>
							</div>
						</a>
					</div>
					</ItemTemplate>
				</asp:Repeater>
				</div>
				<div class="page-prev">이전보기</div>
				<div class="page-next">다음보기</div>
			</div>
		</section>

		<!-- 기도 나눔 -->
		<div class="appmain-pray appContent-margin1" runat="server" id="pray_div">
    		<strong>기도나눔</strong>
			<asp:Literal runat="server" ID="prayer_content"></asp:Literal>
			<time><asp:Literal runat="server" ID="prayer_date"></asp:Literal></time>
		</div>
		<!-- 편지 쓰는 날 -->
		<div class="appmain-letter" runat="server" id="letter_div" visible="false">
    		<strong>후원 어린이에게 편지 쓰는 날</strong>
			<div class="wrap-bt"><a class="bt-type7" style="width:50%" href="/my/letter/write">바로 가기</a></div>
		</div>
		<!-- 스탬프 -->
		<div class="appmain-banner2 appContent-margin1">
    		<a href="/my/activity/stamp"><img src="/common/img/app/banner2.jpg" alt="스탬프투어 참여하신 캠페인/이벤트나 후원자 활동의 스탬프를 획득하세요!" /></a>
		</div>
		
		<!-- 공지사항 -->
		<div class="appmain-notice appContent-margin1">
    		<h2>공지사항</h2>
			<p class="txt-notice"><a class="textcrop-1row" href="#" runat="server" id="notice_title" style="display: inline-block;width: 100%;white-space: nowrap;overflow: hidden;"></a></p>
			<a class="more" href="/about-us/notice/">더보기</a>
		</div>
	
		<!--Footer-->
		<footer>
    		<div class="etc-menu">
        		<a href="/app/terms?tab=2">이용약관</a>
				<a class="color1" href="/app/terms?tab=1">개인정보 처리방침</a>
			</div>

			<div class="list-sns">
				<a target="_blank" href="https://www.facebook.com/compassion.Korea?app_ex=2"><img src="/common/img/icon/sns1.png" alt="페이스북" /></a>
				<a target="_blank" href="https://www.youtube.com/user/CompassionKR?app_ex=2"><img src="/common/img/icon/sns2.png" alt="유투브" /></a>
				<a target="_blank" href="https://www.instagram.com/compassionkorea/?app_ex=2"><img src="/common/img/icon/sns3.png" alt="인스타그램" /></a>
				<a target="_blank" href="https://twitter.com/compassionkorea?app_ex=2"><img src="/common/img/icon/sns4.png" alt="트위터" /></a>
				<a target="_blank" href="http://plus.kakao.com/home/jsd0z8yd?app_ex=2"><img src="/common/img/icon/sns5.png" alt="옐로아이디" /></a>
				<a target="_blank" href="http://happylog.naver.com/compassion.do?app_ex=2"><img src="/common/img/icon/sns6.png" alt="해피빈" /></a>
			</div>
			<p class="txt-copy">&copy; Compassion Korea All Rights Reserved</p>
		</footer>
    
	</div>
	<!--// 컨텐츠 영역 -->
	
</asp:Content>

