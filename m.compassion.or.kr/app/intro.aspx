﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="intro.aspx.cs" Inherits="app_intro" MasterPageFile="~/top.Master"%>
<%@ MasterType virtualpath="~/top.master" %>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
	<script type="text/javascript" src="/common/js/site/hybrid_api.js"></script>
	<script type="text/javascript" src="/common/js/site/common.js"></script>
	<script type="text/javascript" src="/app/intro.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/cookie.js"></script>
    <script src="/common/js/util/swiper.min.js"></script>

    <link href="/common/css/swiper.css" rel="stylesheet" />
    <link href="/common/css/font.css" rel="stylesheet" media="screen" /> 
    <link href="/common/css/mobile.ui.css" rel="stylesheet" media="screen" />

    <style type="text/css">
        .swiper-container {z-index:1000;position:absolute;left:0;top:0;width:100%;height:100%;background-size:contain !important;text-align:center;}
        .app-intro .splash {background:url('/common/img/page/appintro/splash.jpg') no-repeat center;background-size:cover !important;}
		.app-intro .step1 {background:url('/common/img/page/appintro/bg1.jpg') repeat-x;background-size:contain !important;}
		.app-intro .step2 {background:url('/common/img/page/appintro/bg2.jpg') repeat-x;background-size:contain !important;}
		.app-intro .step3 {background:url('/common/img/page/appintro/bg3.jpg') repeat-x;background-size:contain !important;}
		.app-intro .step4 {background:url('/common/img/page/appintro/bg4.jpg') repeat-x;background-size:contain !important;}
		.app-intro .step .wrap {display:inline-block;width:auto;height:100%;position:relative;}
		.app-intro .step .wrap img {display:inline;width:auto;height:100%;}
		.app-intro .step .wrap .app-btn-start {width:44%;height:7%;text-indent:-9999px;position:absolute;left:28%;bottom:19%;}
		.app-intro-indicator {position:fixed;z-index:1001;left:0;bottom:0;width:100%;height:50px;text-align:center;background:rgba(0,0,0,0.2);padding-top:20px;}
		.app-intro-indicator .swiper-pagination-bullet {display:inline-block;width:10px;height:10px;text-indent:-9999px;margin:0 7px;background:url('/common/img/page/appintro/indicator.png') no-repeat;background-size:cover;}
		.app-intro-indicator .swiper-pagination-bullet.swiper-pagination-bullet-active {background:url('/common/img/page/appintro/indicator_on.png') no-repeat;background-size:cover;}
		.app-intro-indicator .skip {display:inline-block;width:auto;height:100%;font-family:noto_d;font-size:13px;color:#fff;text-decoration:none;line-height:50px;padding:0 18px;position:absolute;right:0;top:0;}
	</style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	    <section class="app-intro" >
		    <!-- 스플래쉬 화면 -->
		    <%--<div class="step splash"></div>--%>
            <script type="text/javascript">
                $(function(){
                	$(".btn_start").click(function () {
                		cookie.set("cps.app.intro", "1", 365 * 10);
						location.href = "/app/main"
                	})
                })
            </script>

		    <!-- 단계별 화면 -->
            <div class="swiper-container">
                <div  class="swiper-wrapper">
		            <div class="step step1 swiper-slide">
						<div class="wrap"><img src="/common/img/page/appintro/intro1.jpg" alt="" /></div>
		            </div>
		            <div class="step step2 swiper-slide">
			            <div class="wrap"><img src="/common/img/page/appintro/intro2.jpg" alt="" /></div>
		            </div>
		            <div class="step step3 swiper-slide">
			            <div class="wrap"><img src="/common/img/page/appintro/intro3.jpg" alt="" /></div>
		            </div>
		            <div class="step step4 swiper-slide">
			            <div class="wrap">
				            <img src="/common/img/page/appintro/intro4.jpg" alt="" />
				            <a href="#" class="app-btn-start btn_start" >시작하기</a>
			            </div>
		            </div>
                </div>
            </div>
		    <!--//-->
	    </section>

	    <!-- 단계별 화면 진입 시 노출 -->
	    <section class="app-intro-indicator">
            <div class="swiper-pagination" style="width:100%">
		        <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
		    <a href="#" class="skip btn_start">건너뛰기</a>
	    </section>
	    <!--//-->

</asp:Content>

