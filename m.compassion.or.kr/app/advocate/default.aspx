﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="app_advocate_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />

    <div class="appContent">
    
    <div class="more-advocate">
 
		<!-- tab -->
		<div class="wrap-tab3 appContent-margin1">
			<a style="width:50%" class="selected" href="/app/advocate">소개</a>
			<a style="width:50%" href="/app/advocate/activity">활동</a>
		</div>
		<!-- //tab -->

		<div class="advocate-intro">
			<p class="txt-caption">컴패션 애드보킷이란?</p>
			<p class="txt-sub">가난으로 고통 당하는 전 세계 어린이들을 대신하여 그들의 필요를 적극 알리고, 컴패션  양육을 통해 꿈을 잃은  어린이들이 꿈을 찾을 수 있도록 자원하여 돕는  후원자들을 가리킵니다.</p>
			<div class="txt-graybox">
				애드보킷(Advocate)은 ‘옹호자’, ‘지지자’라는<br />
				뜻을 가지고 있습니다.
			</div>

			<div class="youtube">
				<iframe src="https://www.youtube.com/embed/pbb8eZn-16E?rel=0&amp;showinfo=0wmode=transparent" frameborder="0" allowfullscreen></iframe>
			</div>

			<div class="img-caption appContent-margin1">
				애드보킷 파트별 소개
			</div>

			<p class="txt-caption2">컴패션 온라인 애드보킷</p>
			<p class="txt-sub">연약한 어린이들의 목소리가 되어 온라인 상에서 캠페인 참여, 컴패션 활동 공유, SNS 홍보 등을 통하여 적극적으로 컴패션 사역을 알리는 후원자를 말합니다.</p>

			<div class="wrap-thumbil">
				<ul class="list-thumbil width100">
					<li>
						<span class="photo"><img src="/common/img/page/advocate/img-intro2.jpg" alt="" /></span>
						<span class="advocate-partguide">
							<span class="txt-title">FOC(Friends of Compassion)</span>
							<span class="txt-note">각 분야의 CEO, 리더로 구성되며 정기모임, FOC 파티, 바자회 등 다양한 네트워크와 전문적인 재능을 통해 컴패션 사역을 알립니다.</span>
						</span>
					</li>
					<li>
						<span class="photo"><img src="/common/img/page/advocate/img-intro3.jpg" alt="" /></span>
						<span class="advocate-partguide">
							<span class="txt-title">VOC(Voice of Compassion)</span>
							<span class="txt-note">컴패션 일반인 홍보대사로서  가난으로 고통 받는 어린이들의 필요를 알리고 더 많은 어린이가 후원자의 손을 잡을 수 있도록 정기모임, 결연행사, 결연캠페인, 조별프로젝트 등 일상의 아이디어를 사용하여 어린이들의 목소리가 되어주는 후원자들입니다.</span>
						</span>
					</li>
					<li>
						<span class="photo"><img src="/common/img/page/advocate/img-intro4.jpg" alt="" /></span>
						<span class="advocate-partguide">
							<span class="txt-title">YVOC(Youth Voice of Compassion)</span>
							<span class="txt-note">
								한국컴패션 청소년홍보대사로서 정기모임, 다양한 재능기부를 이용한 나눔캠페인, 컴패션동아리 등 자발적이고 창의적인 활동을 통해 어린이들의 목소리가 됩니다.
							</span>
						</span>
					</li>
					<li>
						<span class="photo"><img src="/common/img/page/advocate/img-intro5.jpg" alt="" /></span>
						<span class="advocate-partguide">
							<span class="txt-title">컴패션나눔별</span>
							<span class="txt-note">컴패션나눔별은 어린이집, 유치원, 초/중/고등학생, 동아리 등 학생들이 힘을 모아 한 어린이를 후원합니다.</span>
						</span>
					</li>
					<li>
						<span class="photo"><img src="/common/img/page/advocate/img-intro6.jpg" alt="" /></span>
						<span class="advocate-partguide">
							<span class="txt-title">컴패션밴드(Compassion Band)</span>
							<span class="txt-note">한국컴패션의 후원자 중 공연과 관련된 재능을 가진 사람들이 모여 더 많은 어린이들이 사랑으로 양육될 수 있도록, 공연과 여러가지 봉사활동으로 재능을 나누는 자원봉사 모임입니다.</span>
						</span>
					</li>
					<li>
						<span class="photo"><img src="/common/img/page/advocate/img-intro7.jpg" alt="" /></span>
						<span class="advocate-partguide">
							<span class="txt-title">컴패션프렌즈샵(Compassion Friends Shop)</span>
							<span class="txt-note">매장을 운영하는 후원자가 자신의 샵을 활용하여 고객들에게 컴패션을 알리고 어린이들의 꿈을 후원하도록 독려하는 공간입니다.</span>
						</span>
					</li>
				</ul>
			</div>

			<div class="wrap-bt">
				<a class="bt-type6 mb15" style="width:100%" href="/advocate/about/introduce/?app_ex=1" target="_blank">컴패션 애드보킷 소개 자세히 보기</a>
				<a class="bt-type9" style="width:100%" href="/advocate/share/?app_ex=1" target="_blank">컴패션 애드보킷 활동 게시판 바로가기</a>
			</div>

		</div>

	</div>
    
</div>

	

</asp:Content>