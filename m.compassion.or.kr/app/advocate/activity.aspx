﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="activity.aspx.cs" Inherits="app_advocate_activity" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />

    <div class="appContent">
    
    <div class="more-advocate">
 
		<!-- tab -->
		<div class="wrap-tab3 appContent-margin1">
			<a style="width:50%" href="/app/advocate">소개</a>
			<a style="width:50%" class="selected" href="/app/advocate/activity">활동</a>
		</div>
		<!-- //tab -->

		<div class="advocate-activity">
			<p class="txt-caption">컴패션 애드보킷으로서 함께할 수 있는 특별한 활동을 소개합니다.</p>
			<p class="txt-sub">*애드보킷 활동 기능은 PC 버전 홈페이지 및 모바일 홈페이지를 통해 이용하실 수 있습니다.</p>
			
			<ul class="act-list">
				<li>
					<p class="tit">Compassion A Day</p>						
					<div class="youtube">
						<iframe src="https://www.youtube.com/embed/fQjCrHEYV2E?rel=0&amp;showinfo=0wmode=transparent" frameborder="0" allowfullscreen></iframe>
					</div>						
					<div class="channel">
						<span>Mobile Web</span>
					</div>
					<p class="con">
						하루에 한 번, 어린이를 향한 사랑고백<br />
						Compassion A Day란 하루에 한 번, 친구나 지인들에게 컴패션을 소개하고 도움이 필요한 어린이를 알리는 캠페인입니다.<br />
						하루에 한 번 컴패션을 전해 주시면 한 어린이를 향한 놀라운 기적이 시작됩니다.
					</p>
					<div class="wrap-bt align-center">
						<a class="bt-type6" style="width:45%" href="/advocate/together/compassion-a-day" target="_blank">함께하기</a>
					</div>
				</li>
				<li>
					<p class="tit">컴패션블루보드 (Blue Board)</p>
					<div class="youtube">
						<iframe src="https://www.youtube.com/embed/RnLcOyj99vk?rel=0&amp;showinfo=0wmode=transparent" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="channel">
						<span class="bar">PC</span>
						<span>Mobile Web</span>
					</div>
					<p class="con">
						컴패션블루보드란 컴패션 어린이 정보가 담긴 블루보드를 활용하여 후원자님이 속한 곳(회사, 사업장, 교회 등)에서 소중한 사람들에게 자연스럽게 컴패션을 알리고 소개하는 활동입니다.
					</p>
					<div class="wrap-bt align-center">
						<a class="bt-type6" style="width:45%" href="/advocate/together/blueboard" target="_blank">함께하기</a>
					</div>
				</li>
				<li>
					<p class="tit">컴패션 DIY (Donate It Your way)</p>
					<div class="act-img">
						<img src="/common/img/app/more/act_img1.jpg" alt="애드보킷 활동이미지" />
					</div>
					<div class="channel">
						<span class="bar">PC</span>
						<span>Mobile Web</span>
					</div>
					<p class="con">
						컴패션 DIY는 후원자님께서 직접 후원 프로그램을 만들고, 함께 참여할 분들을 모집하는 캠페인입니다. 후원자님만 할 수 있는 특별한 나눔을 실천해 보세요!
					</p>
					<div class="wrap-bt align-center">
						<a class="bt-type6" style="width:45%" href="/advocate/together/diy" target="_blank">함께하기</a>
					</div>
				</li>
				<li>
					<p class="tit">컴패션 애드보킷 캠페인</p>
					<div class="act-img">
						<img src="/common/img/app/more/act_img2.jpg" alt="애드보킷 활동이미지" />
					</div>
					<div class="channel">
						<span class="bar">PC</span>
						<span>Mobile Web</span>
					</div>
					<p class="con">
						컴패션 애드보킷 캠페인이란, 시즌별 컨셉에 맞는 결연 행사를 주최하는 홍보 캠페인입니다. 캠페인별로 제공되는 다양한 프로그램과 패킷을 활용하여 나눔의 기쁨을 느껴 보세요!
					</p>
					<div class="wrap-bt align-center">
						<a class="bt-type6" style="width:45%"  href="/advocate/together/campaign" target="_blank">함께하기</a>
					</div>
				</li>
				<li>
					<p class="tit">컴패션 천만 명에게 알리기</p>
					<div class="act-img">
						<img src="/common/img/app/more/act_img3.jpg" alt="애드보킷 활동이미지" />
					</div>
					<div class="channel">
						<span>Mobile Web</span>
					</div>
					<p class="con">
						컴패션 천만 명에게 알리기란 문자 메시지 및 모바일 메신저(카카오톡, 라인)을 이용하여 지인에게 컴패션의 스토리를 전달하는 프로젝트입니다. 이미지와 짧은 영상으로 구성된 컴패션의 감동스러운 스토리들을 소중한 사람들과 함께 나눠 보세요.
					</p>
					<div class="wrap-bt align-center">
						<a class="bt-type6" style="width:45%" href="/advocate/together/notify" target="_blank">함께하기</a>
					</div>
				</li>
			</ul>

		</div>

	</div>
    
</div>


    	

</asp:Content>