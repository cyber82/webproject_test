﻿$(function () {

	$("header").hide();
	$("footer").hide();

	    var swiper = new Swiper('.swiper-container', {
	        pagination: '.swiper-pagination',
	        paginationClickable: true,
	        onInit: function (swiper) {
	            swiper.lockSwipeToPrev();
	        },

	        onSlideChangeEnd: function (swiper) {

	            $(".skip").show();

	            if (swiper.activeIndex == 3) {
	                swiper.lockSwipeToNext();
	                $(".skip").hide();

	            } else if (swiper.activeIndex == 0) {
	                swiper.lockSwipeToPrev();
	            } else {
	                swiper.unlockSwipeToNext();
	                swiper.unlockSwipeToPrev();
	            }

	        }
	    });

});

function setCookie() {
    console.log("setCookie");
    cookie.set('intro', 1);
}