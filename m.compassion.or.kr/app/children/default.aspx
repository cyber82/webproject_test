﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="app_children_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/app/children/default.js?v=1"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" id="data" runat="server" />

    <div class="appContent bg-appmy" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <div class="my-mychild">

            <!--<div class="wrap-mysupporter sectionsub-margin2">
        	<p><strong>임소연</strong> 후원자님</p>
            <p>123456</p>
        </div>-->

            <fieldset class="search-mychild sectionsub-margin2">
                <legend>후원 중인 어린이 검색</legend>
				<select style="width:40%" id="country" ng-model="country" ng-change="changeCountry(country)" data-ng-options="item.country as item.country for item in countries">
					<option value="">국가</option>
				</select>
				&nbsp;
				<select style="width:56%" id="child" ng-model="child" data-ng-options="item.childkey as item.namekr for item in children">
					<option value="">어린이이름</option>
				</select>
               
				<a class="action" ng-click="clickChild()">검색</a>
            </fieldset>

            <ul class="list-mychild">
                <li ng-repeat="item in list" ng-if="$first">
                    <span class="box-mywhite">
                        <p class="wrap-caption">우리가 만난 날  <span class="color2">♥</span> <span class="color1">{{item.commitmentdate | date:'yyyy.MM.dd'}}</span></p>
                        <div ng-click="goView($event,item)">
                            <span class="pos-photo"><span class="photo" style="background: url('{{item.pic}}') no-repeat"></span></span>
                            <p class="txt-name">{{item.name}}</p>
                            <p class="txt-note">
                                어린이ID : <span class="color1">{{item.childkey}}</span><br />
                                국가 : <span class="color1">{{item.countryname}}</span><br />
                                생일 : <span class="color1">{{item.birthdate | date:'yyyy.MM.dd'}} ({{item.age}}세)</span><br />
                                성별 : <span class="color1">{{item.gender}}</span>
                            </p>
                        </div>
                        <div class="wrap-bt">
                            <a class="bt-type7 fl" style="width: 48%" href="#" ng-click="goLetter($event , item)">편지쓰기</a>
                            <a class="bt-type7 fr" style="width: 48%" href="#" ng-click="goGift($event , item);">선물금 보내기</a>
                            <a class="bt-type4 mt15" style="width: 100%" href="#" ng-click="showAlbum($event,item)">성장앨범 보기</a>
                        </div>
                    </span>
                </li>
            </ul>

            <div class="appmain-pray appContent-margin2"  runat="server" id="pray_div" style="margin-top: 15px !important">
                <strong>기도나눔</strong>
                <asp:Literal runat="server" ID="prayer_content"/>
            <time><asp:Literal runat="server" ID="prayer_date"/></time>
            </div>

            <div class="appmain-letter appContent-margin2 style-add" id="letter_div" runat="server">
                <strong>후원 어린이에게 편지 쓰는 날</strong>
                <div class="wrap-bt"><a class="bt-type7" style="width: 50%" ng-click="goLetterWrite()">바로 가기</a></div>
            </div>

            <ul class="list-mychild">
                <li ng-repeat="item in list" ng-if="!$first">
                    <span class="box-mywhite">
                        <p class="wrap-caption">우리가 만난 날  <span class="color2">♥</span> <span class="color1">{{item.commitmentdate | date:'yyyy.MM.dd'}}</span></p>
                        <div ng-click="goView($event,item)">
                            <span class="pos-photo"><span class="photo" style="background: url('{{item.pic}}') no-repeat"></span></span>
                            <p class="txt-name">{{item.name}}</p>
                            <p class="txt-note">
                                어린이ID : <span class="color1">{{item.childkey}}</span><br />
                                국가 : <span class="color1">{{item.countryname}}</span><br />
                                생일 : <span class="color1">{{item.birthdate | date:'yyyy.MM.dd'}} ({{item.age}}세)</span><br />
                                성별 : <span class="color1">{{item.gender}}</span>
                            </p>
                        </div>
                        <div class="wrap-bt">
                            <a class="bt-type7 fl" style="width: 48%" href="#" ng-click="goLetter($event , item)">편지쓰기</a>
                            <a class="bt-type7 fr" style="width: 48%" href="#" ng-click="goGift($event , item);">선물금 보내기</a>
                            <a class="bt-type4 mt15" style="width: 100%" href="#" ng-click="showAlbum($event,item)">성장앨범 보기</a>
                        </div>
                    </span>
                </li>

            </ul>

            <div class="box-myblue link-childmore"><a href="#" ng-show="total > list.length" ng-click="showMore($event)"><span>더 많은 어린이들 보기</span></a></div>

        </div>


    </div>



</asp:Content>
