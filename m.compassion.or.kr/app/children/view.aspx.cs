﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using System.IO;
using CommonLib;

public partial class app_children_view : MobileFrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var sess = new UserInfo();
	
		var requests = Request.GetFriendlyUrlSegments();
		if(requests.Count < 1) {
			Response.Redirect("/my/", true);
		}
		
		childKey.Value = requests[0];      // childKey
										   //childMasterId.Value = requests[1];      // childMasterId
		childMasterId.Value = Request["childMasterId"];

		googleMapApiKey.Value = ConfigurationManager.AppSettings["googleMapApiKey"];
		this.ViewState["userName"] = new UserInfo().UserName;

        bool hasCDSP = false;

        if(UserInfo.IsLogin) {

            DataTable childData = null;
            var actionData = new SponsorAction().HasCDSP();
            if(actionData.success) {

                childData = (DataTable)actionData.data;
                if(childData.Rows.Count > 0) {
                    hasCDSP = true;

                }
            }
        }

        GetPrayer(hasCDSP);
	}


    //기도나눔

    void GetPrayer( bool hasCDSP )
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.prayer.FirstOrDefault(p => p.p_date == DateTime.Today);
            var entity = www6.selectQF<prayer>("p_date", DateTime.Today);

            if (entity == null)
            {
                pray_div.Visible = letter_div.Visible = false;

                return;
            }

            prayer_content.Text = entity.p_content.ToHtml();
            prayer_date.Text = entity.p_date.ToString("M월 d일 (ddd)");

            if (hasCDSP)
            {
                letter_div.Visible = entity.p_check;

            }
        }
    }

}