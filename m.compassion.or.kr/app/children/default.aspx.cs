﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class app_children_default : MobileFrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var sess = new UserInfo();
	

		var actionResult = new ChildAction().MyChildren("" , 1 , 5);
		if(actionResult.success) {

			data.Value = actionResult.data.ToLowerCaseJson();
			
			var children = (List<ChildAction.MyChildItem>)actionResult.data;
			if(children.Count < 1) {
				//base.AlertWithJavascript("나의 어린이가 없습니다.", "goBack()");
				Response.Redirect("/app/children/nodata");
				return;
			}

			// 1명인경우 상세페이지로 이동
			if(children.Count < 2) {
				Response.Redirect(string.Format("/app/children/view/{0}?childMasterId={1}&header=root", children[0].ChildKey, children[0].ChildMasterId));
				return;
			}

		} else {
			
			if (actionResult.action == "not_sponsor") {
				Response.Redirect("/app/children/nodata");
			}
			base.AlertWithJavascript(actionResult.message , "goBack()");
		}

        bool hasCDSP = false;

        if(UserInfo.IsLogin) {

            DataTable childData = null;
            var actionData= new SponsorAction().HasCDSP();
            if(actionData.success) {

                childData = (DataTable)actionData.data;
                if(childData.Rows.Count > 0) {
                    hasCDSP = true;

                }
            }
        }

        this.GetPrayer(hasCDSP);
    }

    protected override void loadComplete( object sender, EventArgs e ) {

    }

    //기도나눔

    void GetPrayer( bool hasCDSP ) {

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.prayer.FirstOrDefault(p => p.p_date == DateTime.Today);
            var entity = www6.selectQF<prayer>("p_date", DateTime.Today);

            if (entity == null)
            {
                pray_div.Visible = letter_div.Visible = false;

                return;
            }

            prayer_content.Text = entity.p_content.ToHtml();
            prayer_date.Text = entity.p_date.ToString("M월 d일 (ddd)");

            if (hasCDSP)
            {
                letter_div.Visible = entity.p_check;
            }
        }
    }

}