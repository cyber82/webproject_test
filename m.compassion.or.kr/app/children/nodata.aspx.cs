﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using System.IO;


public partial class app_children_nodata : MobileFrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var sess = new UserInfo();
		userName.Text = sess.UserName;
		

		var actionResult = new ChildAction().GetChildren(null, null, null, null, null, 18, null, null, 1, 10, "waiting", -1, -1);
		if(actionResult.success) {

			var list = (List<ChildAction.ChildItem>)actionResult.data;
			Random rnd = new Random();
			var index = rnd.Next(list.Count);
			//Response.Write(list.ToJson());

			var item = list[index];
			data.Value = item.ToLowerCaseJson();

            this.ViewState["meta_url"] = Request.UrlEx();
            this.ViewState["meta_title"] = "[한국컴패션-1:1어린이양육]-" + item.NameKr;

            // optional(설정 안할 경우 주석)
            //this.ViewState["meta_description"] = "";
            this.ViewState["meta_keyword"] = this.ViewState["meta_keyword"].ToString() + ",1:1어린이양육," + item.NameKr + "," + item.NameEn;
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,MobileFrontBasePage)
            this.ViewState["meta_image"] = item.Pic.WithFileServerHost();

        }
	}

}