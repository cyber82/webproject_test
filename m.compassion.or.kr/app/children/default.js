﻿$(function () {


});

(function () {

    var app = angular.module('cps.page', []);
    app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

        $scope.total = -1;
        $scope.list = [];
        $scope.countries = null;
        $scope.children = null;
        $scope.country = "";
        $scope.child = "";

        $scope.params = {
            page: 1,
            rowsPerPage: 5
        };

        // 국가별 어린이 목록
        $scope.getCountries = function () {

            $http.get("/api/my/child.ashx?t=get-child-country", { params: {} }).success(function (r) {
                console.log("getCountries", r);
                if (r.success) {

                    $scope.countries = r.data;


                    $scope.changeCountry();

                } else {
                    alert(r.message);
                }
            });

        }

        // list
        $scope.getList = function (params) {
            $scope.params = $.extend($scope.params, params);

            if ($scope.params.page == 1) {
                showList($.parseJSON($("#data").val()));
            } else {

                $http.get("/api/my/child.ashx?t=list", { params: $scope.params }).success(function (r) {
                    if (r.success) {
                        showList(r.data);
                    } else {
                        alert(r.message);
                    }
                });
            }

        }

        var showList = function (list) {

            $.each(list, function () {
                this.commitmentdate = new Date(this.commitmentdate);
                this.birthdate = new Date(this.birthdate);
            });

            console.log(list);

            $scope.list = $.merge($scope.list, list);
            //	console.log($scope.data, r.data.children);
            $scope.total = $scope.list.length > 0 ? $scope.list[0].total : 0;
            //var countries = data.children;

            //$scope.countries = countries;

        }

        $scope.showMore = function ($event) {
            $event.preventDefault();
            $scope.params.page++;
            $scope.getList({});
        }

        $scope.changeCountry = function (country) {

            $scope.country = country;

            if (country == null) {
                entity = []
                $.each($scope.countries, function (i) {
                    entity.push(this.children[0])
                });
                $scope.children = entity;

            } else {
                var children = $.grep($scope.countries, function (r) {
                    return r.country == country;

                })[0];
                $scope.children = children.children;
            }


        }

        $scope.changeChild = function (childKey) {
            if (!childKey) return;
            $scope.child = childKey;

            console.log(childKey);

            var childmasterid = $.grep($scope.list, function (r) {
                return r.childkey = childKey;
            })[0].childmasterid;


            location.href = "/app/children/view/" + childKey + "?childmasterid=" + childmasterid;
        }

        $scope.clickChild = function () {
            childKey = $("#child").val().replace("string:", "");
            if (childKey == "") return;

            $scope.child = childKey;

            console.log(childKey);

            var childmasterid = $.grep($scope.list, function (r) {
                return r.childkey = childKey;
            })[0].childmasterid;


            location.href = "/app/children/view/" + childKey + "?childmasterid=" + childmasterid;
        }


        // 상세페이지
        $scope.goView = function ($event, item) {
            $event.preventDefault();
            location.href = "/app/children/view/" + item.childkey + "?childMasterId=" + item.childmasterid;
        }


        $scope.getCountries();
        // 첫페이지는 codebehind에서 처리
        $scope.getList();


        var loadingAlbum = false;
        $scope.showAlbum = function ($event, item) {
            loading.show();

            $event.preventDefault();
            if ($("#popAlbum").length > 0 || loadingAlbum) return;
            loadingAlbum = true;
            popup.init($scope, "/common/child/album/" + item.childmasterid, function (modal) {
                loadingAlbum = false;
                modal.show();
                activateAlbum($scope, item.childkey, modal);
            }, { removeWhenClose: true });
        }

        $scope.goLetterWrite = function () {
            if (common.checkLogin) {
                location.href = "/my/letter/write";
            }
            return false;
        }

        // 편지쓰기
        $scope.goLetter = function ($event, item) {

            $event.preventDefault();

            if (!item.paid) {
                alert("첫 후원금 납부 후 어린이에게 편지를 보내실 수 있습니다.\n납부 관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr ");
                return;
            }

            if (!item.canletter) {
                alert("후원자님께서는 어린이와 편지를 주고 받지 않으시는 머니 후원 중이십니다. 감사합니다.");
                return;
            }
            location.href = "/my/letter/write?c=" + item.childmasterid + "|" + item.childkey + "|" + item.name + "|" + item.personalnameeng + "&r=/app/children/";

        }

        // 선물금보내기
        $scope.goGift = function ($event, item) {

            $event.preventDefault();
            if (!item.paid) {
                alert("첫 후원금 납부 후 어린이에게 선물금을 보내실 수 있습니다.\n납부 관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr ");
                return;
            }

            if (!item.cangift) {
                alert("편지후원자의 경우는 선물금보내기 기능이 제한됩니다.");
                return;
            }
            location.href = "/my/sponsor/gift-money/pay/?t=temporary&childMasterId=" + item.childmasterid;

        }

    });

})();