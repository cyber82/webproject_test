﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="app_children_view" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/app/children/view.js?v=1.2"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" id="childKey" runat="server" />
	<input type="hidden" id="childMasterId" runat="server" />
	<input type="hidden" id="googleMapApiKey" runat="server" />
    <style>
        .child-center-info {
            display: block;
            font-family: 'noto_m';
            font-weight: normal;
            font-size: 16px;
            line-height: 20px;
            color: #333;
            padding-bottom: 18px;
        }
    </style>

<div class="appContent bg-appmy" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">    <!--마이컴패션 디자인-->
  
    <div class="my-mychild" style="padding-top:20px">
    	
		<div class="box-mywhite view-mychild">
            <p class="wrap-caption">우리가 만난 날  <span class="color2">♥</span> <span class="color1">{{entity.commitmentdate | date:'yyyy.MM.dd'}}</span></p>
			<span class="pos-photo"><span class="photo" style="background:url('{{entity.pic}}') no-repeat"></span></span>
            <span class="txt-name" style="display:block;"><div style="display:inline-block;">{{entity.name}}</div> <div style="display:inline-block;">{{entity.personalnameeng}}</div></span>
            <div class="wrap-bt">
                <a class="bt-type7 fl" style="width:48%" ng-click="goLetter($event,entity)">편지쓰기</a>
                <a class="bt-type7 fr" style="width:48%" href="#" ng-click="goGift($event , entity);">선물금 보내기</a>
                <a class="bt-type4 mt10" style="width:100%" href="#" ng-click="showAlbum($event , entity);">성장앨범 보기</a>
            </div>
            <div class="description">
            	<strong><%:this.ViewState["userName"].ToString() %> 후원자님, 안녕하세요?</strong>
                우리가 함께한 지 <span class="color1">{{entity.commitmentdays| number:0}}일</span>이 지났어요. 후원금과 기도, 사랑의 편지 항상 감사드려요.
                <div class="box-mygray">
                	<table>
                        <tbody>
                            <tr>
                                <td style="width:65px;">어린이 이름 : </td>
                                <td><span class="color1"><div style="display:inline-block;">{{entity.namekr}}</div> <div style="display:inline-block;">{{entity.nameen}}</div></span></td>
                            </tr>
                            <tr>
                                <td>어린이 애칭 : </td>
                                <td><span class="color1"><div style="display:inline-block;">{{entity.name}}</div> <div style="display:inline-block;">{{entity.personalnameeng}}</div></span></td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                        	<tr>
                            	<td style="width:54%">어린이 ID : <span class="color1">{{entity.childkey}}</span></th>
	                            <td style="width:46%">국가 : <span class="color1">{{entity.countryname}}</span></td>
                            </tr>
                            <tr>
                            	<td>생일 : <span class="color1">{{entity.birthdate | date:'yyyy.MM.dd'}} ({{entity.age}}세)</span></td>
	                            <td>성별 : <span class="color1">{{entity.gender}}</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
               <!--
				<span ng-if="entity.casestudy.family">저희 가족은요, <em>{{entity.casestudy.family}}</em> <em>형제 2명</em>이 있어요.</span>
				<span ng-if="entity.casestudy.familyduties">저는 집에서 가족과 함께 <em>{{entity.casestudy.familyduties}}</em>을(를) 해요.<br /></span>
				<span ng-if="entity.casestudy.christianactivities">제가 참여하는 컴패션 활동은요 <em>{{entity.casestudy.christianactivities}}</em>이예요. </span>
				<span ng-if="entity.casestudy.hobby">제 취미와 운동은 <em>{{entity.casestudy.hobby}}</em> 예요.<br /></span>
				<span ng-if="entity.casestudy.schooling"><em>{{entity.casestudy.schooling}}</em>에 다니고 있고, </span>
				<span ng-if="entity.casestudy.family">좋아하는 과목은 <em>{{entity.casestudy.family}}</em>이예요. <br />-->
				<span ng-bind-html="entity.biokr"></span><br />
				
				<span ng-if="entity.casestudy.health">(<em>{{entity.casestudy.health }}</em>)</span>
				
            </div>
        </div>
        
		<div class="box-mywhite map-weather">
        	<p class="nation">지금 {{countryInfo.c_name}}는(은)요,</p>
			<div id="map" style="width:100%;height:224px"></div>
            <div class="wrap-weather">
                <p class="state">{{countryInfo.c_weather}}</p>
                <p class="temperature">{{countryInfo.c_temperature | number:0}}˚</p>
                <img ng-show="countryInfo" ng-src="/common/img/page/sponsor/weather/{{countryInfo.c_weather_icon}}.png" class="icon" alt="맑음" />
                <p class="time">{{countryInfo.time | date:'HH:mm'}}</p>
            </div>
            <br />
            <div class="graph">
                <img ng-src="/common/img/page/sponsor/gdp/{{entity.countrycode}}.jpg" alt="" width="100%" />
            </div>
        </div>

        <div class="box-mywhite person-gdp" style="overflow: scroll">
                <!-- 센터설명 -->
                <div class="compassionCenter" style="display: none">
                    <%--<p class="txt-caption">{{entity.name}}이(가) 살고 있는 컴패션 센터는</p>--%>
                    <p class="child-center-info">{{entity.name}}이(가) 다니는 어린이센터는,</p>

                    <span ng-bind-html="entity.projectinfo"></span>
                    <br />
                    <span>■ 어린이센터 활동</span>
                    <div class="box-ProjectInfo">
                        <div style="display: block;">
                            <div class="box_type7 cognitive">
                                <span>□ 지적 영역</span>
                                <div class="box_type9" ng-bind-html="entity.cognitive">
                                </div>
                            </div>
                            <div class="box_type8 socioemotional">
                                <span>□ 사회정서적 영역</span>
                                <div class="box_type9" ng-bind-html="entity.socioemotional">
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="box_type7 spiritual">
                                <span>□ 영적 영역</span>
                                <div class="box_type9" ng-bind-html="entity.spiritual">
                                </div>
                            </div>
                            <div class="box_type8 physical">
                                <span>□ 신체적 영역</span>
                                <div class="box_type9" ng-bind-html="entity.physical">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        

        
        <div class="giftmoney" ng-show="entity.cangift">
        	<div class="box-myblue ">
                <div class="wrap-header">
                    <p class="txt-title mt0">선물금 예약 내역</p>
                    <a class="bt-type2 more" href="/my/sponsor/gift-money/pay/?t=regular&childMasterId={{entity.childmasterid}}" ng-if="giftable">선물금 예약하기</a>
                </div>
            </div>
            <div class="box-mywhite">
            	<ul class="list-gift">
                	<li ng-repeat="item in gr_list">
                    	<span class="row txt-name">
                        	{{item.namekr}}<span class="ar">{{item.startdate | date:'yyyy.MM.dd'}}</span>
                        </span>
                        <span class="row txt-period">
                        	{{item.sponsoritem}}<span class="bar">|</span>{{item.fundingfrequency}} {{item.sponsormonth}}월
                        </span>
                        <span class="row txt-price">
                        	<em>{{item.sponsoramount | number:0}}</em> 원<span class="ar"><a href="/my/sponsor/gift-money/update-reservation/?c={{item.commitmentid}}" class="modify">수정</a><a ng-click="deleteGiftReservation($event,item.commitmentid)" class="delete">삭제</a></span>
                        </span>
                    </li>
                  
                    
                    <!--내용없는경우-->
                    <li class="no-result" ng-if="gr_total == 0">
                    	<span class="img"><img src="/common/img/icon/gift2.png" alt="" width="45" /></span>
                        등록된 선물금 예약 정보가 없습니다.
                    </li>
                </ul>
            </div>
            
			<paging class="small" page="gr_page" page-size="gr_rowsPerPage" total="gr_total" show-prev-next="true" show-first-last="true" paging-action="getGiftReservationList(page)"></paging>

		</div>

		
        <div class="giftmoney" ng-show="entity.cangift">
        	<div class="box-myblue ">
                <div class="wrap-header">
                    <p class="txt-title mt0">선물금 결제 내역</p>
                    <a class="bt-type2 more" href="/my/sponsor/gift-money/pay/?t=temporary&childMasterId={{entity.childmasterid}}" ng-if="giftable">선물금 보내기</a>
                </div>
            </div>
            <div class="box-mywhite">
            	<ul class="list-gift">
                	<li ng-repeat="item in gp_list">
                    	<span class="row txt-name">
                        	{{item.namekr}}<span class="ar">{{item.paymentdate | date:'yyyy.MM.dd'}}</span>
                        </span>
                        <span class="row txt-period">
                        	{{item.sponsoritem}}<span class="bar">|</span>{{item.paymentname}}
                        </span>
                        <span class="row txt-price">
                        	<em>{{item.sponsoramount | number:0}}</em> 원
                        </span>
                    </li>
                
                    <!--내용없는경우-->
                    <li class="no-result" ng-if="gp_total == 0">
                    	<span class="img"><img src="/common/img/icon/gift3.png" alt="" width="38" /></span>
                        등록된 선물금 내용이 없습니다.<br />
						어린이에게 선물을 보내 보세요.
                    </li>
                </ul>
            </div>
            
           <paging class="small" page="gp_page" page-size="gp_rowsPerPage" total="gp_total" show-prev-next="true" show-first-last="true" paging-action="getGiftPaymentList(page)"></paging>

		</div>

		 <div class="appmain-pray appContent-margin2" id="pray_div" runat="server" style="margin-top: 30px !important">
            <strong>기도나눔</strong>
            <asp:Literal runat="server" ID="prayer_content"/>
        <time><asp:Literal runat="server" ID="prayer_date"/></time>
        </div>

        <div ng-show="entity.canletter" class="appmain-letter appContent-margin2 style-add" id="letter_div" runat="server">
            <strong>후원 어린이에게 편지 쓰는 날</strong>
            <div class="wrap-bt"><a class="bt-type7" style="width: 50%" ng-href="/my/letter/write/?c={{entity.childmasterid}}|{{entity.childkey}}|{{entity.namekr}}|{{entity.personalnameeng}}">바로 가기</a></div>

        </div>

		<div class="my-hotlink">
        	<p>직접 고른 선물을 하고 싶으신가요?</p>
            <a href="/store/?app_ex=1&gift_flag=1"><img src="/common/img/page/my/banner-store.jpg" alt="스토어 선물 고르러 가기" width="100%" /></a>
        </div>
		
    </div>
    
<!--//-->
</div>

</asp:Content>
