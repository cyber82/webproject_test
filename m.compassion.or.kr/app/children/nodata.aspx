﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="nodata.aspx.cs" Inherits="app_children_nodata" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>



<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/app/children/nodata.js"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" id="data" runat="server" />

    <div class="appContent bg-appmy" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <div class="my-mychild">
            <div class="no-supporter appContent-margin2">
                <span class="color3">후원 중인 어린이가 없습니다.</span><br />
                1:1 어린이 양육을 시작해보세요.
            <div class="linebar"></div>
                <strong><em>
                    <asp:Literal runat="server" ID="userName" /></em>님을<br />
                    기다리고 있는 어린이가 있어요.</strong>
                <span class="color1">1:1 결연이 만드는 사랑의 기적, 한 어린이가 꿈을 찾는 양육비는 매월 4만 5천원입니다. 후원금과 기도, 사랑의 편지로 아이에게 꿈을 주세요.</span>
            </div>


            <div class="box-mywhite recommandchild">
                <div class="wrap-caption">
                    <span class="txt-clock">{{item.waitingdays}}일</span>

                    <span class="sns">
                        <a ng-click="sns.show($event , {url : '/sponsor/children/?c=' +  item.childmasterid, title : '[한국컴패션-1:1어린이양육]' + item.name , desc : '' , picture : item.pic})">
                            <img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
                    </span>
                </div>
                <span class="pos-photo"><span class="photo" style="background: url('{{item.pic}}') no-repeat"></span></span>
                <p class="txt-name">{{item.name}}</p>
                <p class="txt-note">
                    국가 : <span class="color1">{{item.countryname}}</span><br />
                    생일 : <span class="color1">{{item.birthdate | date:'yyyy.MM.dd'}} ({{item.age}}세)</span><br />
                    성별 : <span class="color1">{{item.gender}}</span>
                </p>
                <div class="wrap-bt">
                    <a class="bt-type6" style="width: 100%" ng-click="showChildPop($event , item)">더 알아보기</a>
                </div>
            </div>

        </div>



    </div>



</asp:Content>
