﻿$(function () {

	var entity;
	// 메인설정/푸시알람설정/비전정보
	$.get("/api/app.ashx?t=get-info", function (r) {
		if (!r.success) {
			alert(r.message);
			history.back();
		}

		entity = r.data;
		console.log(entity);

		$("input[name=au_mainpage][value='" + entity.au_mainpage + "']").prop("checked", true);
		
		$("#current_version").text(entity.current_version);
		if (entity.current_version == entity.latest_version) {
			$("#txt_latest_version").show();
		} else {

			$("#btn_install").attr("href", entity.download_link);
			$("#btn_install").show();

		}
	})

	// 자동로그인
	var autoLogin = cookie.get("cps.autologin");
	if (autoLogin == "Y") {
		$("#auto_login_check").addClass("on");
	}

	$("#auto_login_check").click(function () {
		if ($("#auto_login_check").hasClass("on")) {
			$("#auto_login_check").removeClass("on");
			var checked = false;
		} else {
			$("#auto_login_check").addClass("on");
			var checked = true;
		}
		$.get("/api/app.ashx?t=set-auto-login", { "yn": (checked ? "Y" : "N") });
	})

	// 메인페이지
	$("#btn_au_mainpage").click(function () {
		$("#au_mainpage_container").slideToggle();
		return false;
	})

	$("input[name=au_mainpage]").click(function () {
		var val = $(this).val();
		cookie.set("cps.app.root", val, 365 * 10);
		console.log(val);
		$.get("/api/app.ashx?t=set-mainpage", { "val": val }, function (r) {
			if (!r.success) {
				alert(r.message);
			}
		});
	})


});


var app = angular.module('cps.page', []);
app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService, bridge) {

	$scope.pushState = false;
	$scope.togglePush = function () {

		$scope.pushState = !$scope.pushState;
		bridge.updatePushState($scope.pushState);

	};

	// 기기에 설정된 푸시상태를 조회 
	// 기기에서 callback 처리를 setPushState 에서 한다.
	bridge.getPushState($scope, "setPushState");

	/*
	hybrid app 에서 호출하는 method
	*/
	// bridge 에서 호출
	$scope.setPushState = function (state) {
		setTimeout(function () {
			$scope.$apply(function () {

				$scope.pushState = state;
				
			});
		}, 100);
	};

});

