﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="setting.aspx.cs" Inherits="app_setting" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/app/setting.js"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="appContent" ng-app="cps" ng-cloak  ng-controller="defaultCtrl" >
    
    <div class="setting appContent-margin1">
 
		<ul>
			<li>
				<div class="dpth1">
					<span class="txt">자동 로그인 설정</span>
					<span class="pos-r tog-bt" id="auto_login_check"></span>	<!-- 토글버튼 클릭시 "on" 클래스 추가 -->
                    
				</div>
			</li>
			<li>
				<a href="#" class="dpth1 on" id="btn_au_mainpage">
					<span class="txt arr">메인 설정</span>
				</a>

				<!-- 메뉴열림 -->
				<div class="dpth2 main-set" id="au_mainpage_container" style="display:none">
					<p class="con">
						앱의 메인 페이지를 원하는 메뉴로 설정할 수 있어요!<br />
						원하는 메뉴로 설정 후, 앱을 다시 시작하면 변경 내용이 반영됩니다.
					</p>

					<span class="radio-ui">
						<span class="w1">
							<input type="radio" id="main-set1" name="au_mainpage" class="css-radio" value="child"/>
							<label for="main-set1" class="css-label">나의 어린이</label>
						</span>
						<span class="w2">
							<input type="radio" id="main-set2" name="au_mainpage" class="css-radio" value="letter"/>
							<label for="main-set2" class="css-label">편지</label>
						</span>
						<span class="w3">
							<input type="radio" id="main-set3" name="au_mainpage" class="css-radio" value="sponsor"/>
							<label for="main-set3" class="css-label">후원</label>
						</span>
						<span class="w1">
							<input type="radio" id="main-set4" name="au_mainpage" class="css-radio" value="event"/>
							<label for="main-set4" class="css-label">캠페인/이벤트</label>
						</span>
						<span class="w2">
							<input type="radio" id="main-set5" name="au_mainpage" class="css-radio" value="more"/>
							<label for="main-set5" class="css-label">더 보기</label>
						</span>
					</span>
						

					<div class="align-center">
						<a href="#" class="bt-type6" style="width:35%;" onclick="$('#au_mainpage_container').slideUp()">확인</a>
					</div>
				</div>
				<!--// -->
			</li>
			<li>
				<div class="dpth1">
					
					<span class="txt">푸시 알림 설정</span>
					<span class="pos-r tog-bt" ng-class="{'on' : pushState}" ng-click="togglePush()"></span>	<!-- 토글버튼 클릭시 "on" 클래스 추가 -->
				</div>
			</li>
			<li>
				<div class="dpth1">
					<span class="txt">버전 정보<em><span id="current_version"></span></em></span>
					<div style="display:none">
                    <a id="btn_install" target="_blank" class="pos-r bt-type7" style="display:none">업데이트</a>
					</div>
					<span id="txt_latest_version" class="pos-r bt-type7" style="display:none">최신버전</span>
				</div>
			</li>
			<li>
				<a href="/app/terms?tab=2" class="dpth1">
					<span class="txt icon-arr">약관 및 정책</span>
				</a>
			</li>
		</ul>

	</div>
    
</div>
	
</asp:Content>

