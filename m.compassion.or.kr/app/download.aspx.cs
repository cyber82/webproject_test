﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using CommonLib;

public partial class app_download : MobileFrontBasePage {



	protected override void OnBeforePostBack()
    {	
		string device = Request.QueryString["device"].EmptyIfNull();

        // 앱스토어 링크 , iphone , android
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var item = dao.device_version.FirstOrDefault(p => p.dv_id == device);
            var item = www6.selectQF<device_version>("dv_id", device);
            if (item != null)
            {
                Response.Redirect(item.dv_link);
            }
        }
		
	}


}