﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="gateway.aspx.cs" Inherits="app_gateway" MasterPageFile="~/top.Master" %>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript">
		$(function(){
			var info = $.parseJSON($("#info").val());
			console.log(info);

			var cur_version = "<%:v%>";
			if (versionCompare(info.dv_min, cur_version, { zeroExtend: true }) > 0) {

				//var popup = $rootScope.confirm("앱 실행이 가능한 최소 버젼은 " + info.dv_min + " 입니다.\n고객님의 앱 버젼은 " + cur_version + "입니다.\n최신 앱을 설치 하시겠습니까?");
				if (confirm("앱 실행이 가능한 최소 버젼은 " + info.dv_min + " 입니다.\n최신 앱을 설치 하시겠습니까?")) {
					hybrid_api.installNewVersion(info);
				} else {

					hybrid_api.killApp();
				}
				
			} else {

				location.href = $("#next_url").val();

			}

		})

		
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="info" />
	<input type="hidden" runat="server" id="next_url" />



	<script>
		$("header").hide();
	</script>
</asp:Content>

