﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="app_push_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/app/push/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	<a runat="server" id="btnList"></a>

    <div class="appContent bg-alarm" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <div class="app-alarm appContent-margin1">

            <ul>
                <li ng-repeat="item in list">
                    <a href="#" ng-click="goView($event , item)">
                        <p class="tit">{{item.msgText}}</p>
                        <span class="date">{{item.regDate | date:'yyyy.MM.dd'}}</span>
                    </a>
                </li>


                <!-- 알림 없는 경우 -->
                <li class="no-message" ng-if="noResult">
                    받은 알림이 존재하지 않습니다.
                </li>
            </ul>

        </div>

        <a class="bt-type6" style="width: 100%" href="#" ng-show="hasMore" ng-click="showMore($event)">더보기</a>


    </div>





</asp:Content>
