﻿
var app = angular.module('cps.page', []);
app.controller("defaultCtrl", function ($scope, $http, $filter, paramService, bridge) {

	$scope.list = [];
	$scope.hasMore = false;
	$scope.noResult = false;

	$scope.params = {
		page: 1,
		rowsPerPage: 5

	};

	$scope.getList = function (params) {
		$scope.params = $.extend($scope.params, params);
		bridge.getPushData($scope, $scope.params.page, $scope.params.rowsPerPage, "setPushData");
	}

	$scope.setPushData = function (data) {

		$scope.$apply(function () {

			$.each(data, function () {
				this.regDate = new Date(this.regDate.substr(0, 4) + "/" + this.regDate.substr(4, 2) + "/" + this.regDate.substr(6, 2));
			});
			$scope.list = $.merge($scope.list, data);

			if ($scope.list.length < 1)
				$scope.noResult = true;

			if (data.length < $scope.params.rowsPerPage) {
			    $scope.hasMore = false;
			} else {
			    $scope.hasMore = data.length > 0;

			}
		});
	};

	$scope.goView = function ($event, item) {

		// 읽지 않은 상태는 읽은 상태로 변경
		if (item.readYn == "N") {
			bridge.readPush(item.msgId);
		}

		setTimeout(function () {
			hybrid_api.openPushLink(item.appLink);
		}, 500);
		

	};

	$scope.showMore = function ($event) {
		$event.preventDefault();
		$scope.params.page++;
		$scope.getList();
        
	}

	$scope.getList({ page: 1 });

});
