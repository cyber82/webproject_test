﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class app_more : MobileFrontBasePage {
	
	protected override void OnBeforePostBack()
    {
		base.OnBeforePostBack();
        using (FrontDataContext dao = new FrontDataContext())
        {

            //var list = dao.sp_mainpage_list_f(10, "app_more").ToList();
            Object[] op1 = new Object[] { "count", "position" };
            Object[] op2 = new Object[] { 10, "app_more" };
            var list = www6.selectSP("sp_mainpage_list_f", op1, op2).DataTableToList<sp_mainpage_list_fResult>();

            foreach (var entity in list)
            {
                entity.mp_image = entity.mp_image.WithFileServerHost();
            }

            repeater_visual.DataSource = list;
            repeater_visual.DataBind();
        }
            if(UserInfo.IsLogin) {

			ph_login.Visible = true;
		} else {
			ph_logout.Visible = true;
		}


        GetSympathy();

    }


    void GetSympathy()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_sympathy_list_latest_f();
            Object[] op1 = new Object[] {  };
            Object[] op2 = new Object[] {  };
            var list = www6.selectSP("sp_sympathy_list_latest_f", op1, op2).DataTableToList<sp_sympathy_list_latest_fResult>();

            repeater_sympathy.DataSource = list;
            repeater_sympathy.DataBind();
        }
    }
}