﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="more.aspx.cs" Inherits="app_more" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
<script type="text/javascript" src="/app/more.js"></script>
<script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<div class="appContent">
    
    <div class="more-main appContent-margin1">

		<div class="wrap-slider" id="sympathy_container">
            <div id="sympathy_visual">
                <asp:Repeater runat="server" ID="repeater_sympathy">
                    <ItemTemplate>
                        <!-- set -->
                        <div class="list" style="width: 100%">
                            <a href="/sympathy/view/<%#Eval("board_id") %>/<%#Eval("idx") %>?page=1&rowsPerPage=9&s_type=<%#Eval("board_id") %>"">
                                <div class="more-main-visual" style="background: url('<%#Eval("thumb").ToString().WithFileServerHost() %>') no-repeat center; background-size: cover;">
                                    <div class="title-box">
                                        <div class="subject"><span><%#Eval("board_name") %></span></div>
                                        <p class="tit"><%#Eval("title") %></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--// set -->
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="page-prev" id="sympathy_visual_prev">이전보기</div>
			<div class="page-next" id="sympathy_visual_next">다음보기</div>
		</div>

		<ul class="shortcut">
			<li>
				<a href="/sympathy/">
					<span class="icon i-sympathy"></span>
					<span class="txt">공감</span>
				</a>
			</li>
			<li>
				<a href="/my/activity/stamp">
					<span class="icon i-stamp"></span>
					<span class="txt">스탬프 투어</span>
				</a>
			</li>
			<li>
				<a href="/app/advocate/">
					<span class="icon i-advocate"></span>
					<span class="txt">애드보킷</span>
				</a>
			</li>
			<li>
				<a href="/customer/faq/">
					<span class="icon i-faq"></span>
					<span class="txt">FAQ</span>
				</a>
			</li>
			<li>
				<a href="/about-us/notice/">
					<span class="icon i-notice"></span>
					<span class="txt">공지사항</span>
				</a>
			</li>
			<li>
				<a href="/store/?app_ex=1">
					<span class="icon i-store"></span>
					<span class="txt">스토어</span>
				</a>
			</li>
		</ul>

		<!-- 배너 -->
		<div class="more-banner">
            <asp:Repeater runat="server" ID="repeater_visual">
                <ItemTemplate>
                    <a href="<%#Eval("mp_image_link") %>" target="<%#Eval("mp_is_new_window").ToString() == "True" ? "_blank" : "_self" %>">
                        <img src="<%#Eval("mp_image")%>" alt="아프리카, 다시 쓰는 이야기" style="margin-bottom:10px"/>
                    </a>
                </ItemTemplate>
            </asp:Repeater>
		</div>

		<asp:PlaceHolder runat="server" ID="ph_login" Visible="false">
			<div class="more-login">
				<a class="bt-type6" style="width:100%" href="#" ng-click="logout($event)">로그아웃</a>
			</div>

		</asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="ph_logout" Visible="false">
			<div class="more-login">
				<a class="bt-type6" style="width:100%" href="/login">로그인</a>
			</div>
		</asp:PlaceHolder>

		

	</div>
    
</div>


</asp:Content>

