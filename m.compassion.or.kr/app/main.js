﻿$(function () {

	mainVisual.init();

});

var mainVisual = {
	animating: false,

	init: function () {

		var swiper = new Swiper('.swiper-container', {
			pagination: '.swiper-container .page-count',
			paginationClickable: true,
			nextButton: '.swiper-container .page-next',
			prevButton: '.swiper-container .page-prev',
			paginationBulletRender: function (index, className) {
				return '<span class="' + className + '">' + (index + 1) + '</span>';
			},
			spaceBetween: 30
		});

		var swiper2 = new Swiper('.swiper-container2', {
			nextButton: '.swiper-container2 .page-next',
			prevButton: '.swiper-container2 .page-prev',
			spaceBetween: 30
		});

	},

	
};