﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
using Newtonsoft.Json.Linq;

public partial class app_main : MobileFrontBasePage
{
    public WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();
    protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


        // 어린이수
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.content.First(p => p.c_id == "main");
            var entity = www6.selectQF<content>("c_id", "main");
            var data = JObject.Parse(entity.c_text.ToString());

            children.Text = data["child"].ToString();
            main_content.Text = data["content"].ToString();
        }

		// 로그인 영역 Visible
		if (UserInfo.IsLogin) {
			login_section.Visible = false;

		}

        bool hasCDSP = false;

        if(UserInfo.IsLogin) {
            
            UserInfo sess = new UserInfo();

            DataTable childData = null;
            var actionResult = new SponsorAction().HasCDSP();
            if(actionResult.success) {

                childData = (DataTable)actionResult.data;
                if(childData.Rows.Count > 0) {
                    hasCDSP = true;
                       
                }
            }
        }

        this.GetEvent();
		this.GetSpecialFunding();
		this.GetUserFunding();
		this.GetSympathy();
		this.GetPrayer(hasCDSP);
		this.GetNotice();
	}



	//캠페인 
	void GetEvent()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_mainpage_list_f(10, "main_event_mobile");
            Object[] op1 = new Object[] { "count", "position" };
            Object[] op2 = new Object[] { 10, "main_event_mobile" };
            var list = www6.selectSP("sp_mainpage_list_f", op1, op2).DataTableToList<sp_mainpage_list_fResult>();

            repeater_event.DataSource = list;
            repeater_event.DataBind();
        }
	}


	// 특별한 나눔 
	public int sf_percent;
	public int sf_remain_days;
    public string sf_visible = "block";
	public sp_tSpecialFunding_list_fResult sf_entity;
	void GetSpecialFunding()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //sf_entity = dao.sp_tSpecialFunding_list_f("", "", "N", "", "Y").FirstOrDefault();
            Object[] op1 = new Object[] { "AccountClassGroup", "AccountClass", "fixedYN", "ufYN", "pick" };
            Object[] op2 = new Object[] { "", "", "N", "", "Y" };
            sf_entity = www6.selectSP("sp_tSpecialFunding_list_f", op1, op2).DataTableToList<sp_tSpecialFunding_list_fResult>().FirstOrDefault();

            //sf_entity = dao.tSpecialFunding.OrderBy(p => p.sf_id).FirstOrDefault(p => p.sf_id == 26);
            if (sf_entity == null)
            {
                ph_specialFunding.Visible = false;
            }
            else
            {
                Object[] objParam = new object[] { "CampaignID" };
                Object[] objValue = new object[] { sf_entity.CampaignID };
                Object[] objSql = new object[] { "sp_S_Campaign_PaymentTotal" };
                DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["TOTAL"] != DBNull.Value)
                    {
                        long total = Convert.ToInt32(ds.Tables[0].Rows[0]["TOTAL"]);
                        sf_entity.sf_current_amount = total;
                    }
                    else
                    {
                        sf_entity.sf_current_amount = 0;
                    }
                }


                sf_remain_days = Convert.ToInt32(sf_entity.EndDate.Value.Subtract(DateTime.Now).TotalDays);

                // 이미지가 null일경우 대체 이미지 설정 및 있을경우 파일 서버 호스트 변경 
                if (sf_entity.sf_image_m == null)
                {
                    sf_entity.sf_image_m = "/common/img/temp/temp25.jpg";
                }
                else
                {
                    sf_entity.sf_image_m = sf_entity.sf_image_m.WithFileServerHost();
                }

                // days
                if (sf_entity.EndDate.HasValue)
                    sf_days.Text = string.Format("D-{0}", sf_remain_days.ToString("N0"));


                if (sf_entity.sf_goal_amount < 1)
                {
                    sf_percent = -1;

                    sf_visible = "none";
                }
                else
                {
                    if (sf_entity.sf_current_amount < 1)
                        sf_percent = 0;
                    else
                        sf_percent = Convert.ToInt32(sf_entity.sf_current_amount * 1.0 / sf_entity.sf_goal_amount * 100);

                    if (sf_percent > 100)
                        sf_percent = 100;
                }
            }
        }

	}


	// 나눔 펀딩 
	public int uf_percent;
	public sp_tUserFunding_list_fResult uf_entity;

    void GetUserFunding()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //uf_entity = dao.sp_tUserFunding_list_f(1, 1, "", "deadline", "").FirstOrDefault();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "uf_type", "sort", "keyword" };
            Object[] op2 = new Object[] { 1, 1, "", "deadline", "" };
            uf_entity = www6.selectSP("sp_tUserFunding_list_f", op1, op2).DataTableToList<sp_tUserFunding_list_fResult>().FirstOrDefault();

            // 이미지가 null일경우 대체 이미지 설정 및 있을경우 파일 서버 호스트 변경 
            if (uf_entity.uf_image == null)
            {
                uf_entity.uf_image = "/common/img/temp/temp25.jpg";
            }
            else
            {
                uf_entity.uf_image = uf_entity.uf_image.WithFileServerHost();
            }


            // days
            uf_days.Text = string.Format("D-{0}", uf_entity.uf_date_end.Subtract(DateTime.Now).TotalDays.ToString("N0"));

            if (uf_entity.uf_goal_amount < 1)
            {
                uf_percent = -1;
            }
            else
            {

                if (uf_entity.uf_current_amount < 1)
                {
                    uf_percent = 0;
                }
                else
                {
                    uf_percent = Convert.ToInt32(uf_entity.uf_current_amount * 1.0 / uf_entity.uf_goal_amount * 100);
                }

                if (uf_percent > 100) { uf_percent = 100; }

            }


        }
	}


	// 공감
	void GetSympathy()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_sympathy_list_latest_f();
            Object[] op1 = new Object[] {  };
            Object[] op2 = new Object[] {  };
            var list = www6.selectSP("sp_sympathy_list_latest_f", op1, op2).DataTableToList<sp_sympathy_list_latest_fResult>();

            repeater_sympathy.DataSource = list;
            repeater_sympathy.DataBind();
        }
	}

	//기도나눔

    void GetPrayer( bool hasCDSP )
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.prayer.FirstOrDefault(p => p.p_date == DateTime.Today);
            var entity = www6.selectQF<prayer>("p_date", DateTime.Today);
            if (entity == null)
            {
                pray_div.Visible = letter_div.Visible = false;
                return;
            }

            prayer_content.Text = entity.p_content.ToHtml();
            prayer_date.Text = entity.p_date.ToString("M월 d일 (ddd)");

            if (hasCDSP)
            {
                letter_div.Visible = entity.p_check;
            }
        }
    }

    void GetNotice()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.sp_board_list_f(1, 1, "notice", "", "", "", -1, "", "").FirstOrDefault();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "sub_type", "keyword_type", "keyword", "main", "startdate", "enddate" };
            Object[] op2 = new Object[] { 1, 1, "notice", "", "", "", -1, "", "" };
            var entity = www6.selectSP("sp_board_list_f", op1, op2).DataTableToList<sp_board_list_fResult>().FirstOrDefault();

            notice_title.InnerText = entity.b_title;
            notice_title.HRef = "/about-us/notice/view/" + entity.b_id;


        }
	}










}