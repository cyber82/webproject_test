﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="join.aspx.cs" Inherits="my_user_funding_join" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>
	<script type="text/javascript" src="/my/user-funding/join.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

<div class="wrap-sectionsub-my" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">    <!--마이컴패션 디자인-->
<!---->
    
    <div class="wrap-tab4 sectionsub-margin2">
        <a style="width:50%" class="selected" href="#">참여한 펀딩</a>
        <a style="width:50%" href="/my/user-funding/create">개설한 펀딩</a>
    </div>

    <div class="my-funding">
    	
		<div class="box-mywhite funding-profile" ng-if="total > 0">
        	<div class="pos-photo"><span class="photo" style="background:url('{{summary.creator_image}}') no-repeat"></span></div>
            <p class="txt-name">{{summary.creator_name}}</p>
            <div class="row-table">
                <div class="td-field" style="width:49%">참여펀딩<em>{{summary.cnt_join | number:N0}}</em></div>
            	<div class="td-field" style="width:51%">개설펀딩<em>{{summary.cnt_create | number:N0}}</em></div>
            </div>
        </div>

        <div class="box-mywhite funding-profile" ng-if="total == 0">
            <div class="pos-photo"><span class="photo" style="background: url('{{summary.creator_image}}') no-repeat"></span></div>
            <p class="txt-name">{{summary.creator_name}}</p>
            <div class="row-table">
                <div class="td-field" style="width: 49%">참여펀딩<em>{{summary.cnt_join | number:N0}}</em></div>
                <div class="td-field" style="width: 51%">개설펀딩<em>{{summary.cnt_create | number:N0}}</em></div>
            </div>
        </div>
        
        <div class="box-myblue support-amount mt10">
        	<p class="txt1">누적 현황  <span class="stxt1">({{today | date:'yyyy.MM.dd'}} 참여한 펀딩 기준)</span></p>
            <p class="txt-amount"><em>{{summary.amount_join | number:N0}}</em>원</p>
        </div>
		
        <div class="box-mywhite participate-funding" ng-hide="total == 0">
        	<div class="wrap-header">
            	<p class="txt-title">최근 참여한 펀딩 현황</p>
            </div>
			<a ng-href="/sponsor/user-funding/view/{{latest.uf_id}}">
				 <div class="toptop">
					<span class="frame"><span class="photo" style="background:url('{{latest.uf_image}}') no-repeat"></span></span>
					<em class="txt-type">{{latest.uf_type_name}}</em>
					<p class="txt-name" style="word-break:break-all">{{latest.uf_title}}</p>
				</div>
			</a>
			<div class="box-mygray fundingstate-info">
            	펀딩구분 : <span class="color1">{{latest.uf_type_name}}</span><br />
                진행기간 : <span class="color1">{{latest.uf_date_start | date:'yyyy.MM.dd'}} ~ {{latest.uf_date_end | date:'yyyy.MM.dd'}}</span><br />
                목표금액 : <span class="color1">{{latest.uf_goal_amount | number:N0}}원({{latest.uf_current_amount / latest.uf_goal_amount|percentage:0}})</span><br />
                후원금액 : <span class="color1">{{latest.uf_current_amount | number:N0}}원</span><br />
                남은기간 : <span class="color1">{{latest.uf_date_end | amDifference : today : 'days' }}일</span>
            </div>

			<div>
				<a ng-href="/sponsor/user-funding/view/{{latest.uf_id}}"><p class="txt-update">업데이트 소식({{(latest.uf_cnt_notice || 0) | number:N0}})</p></a>
			</div>
            <div class="wrap-bt"><a class="bt-type6" style="width:100%" ng-href="/sponsor/user-funding/view/{{latest.uf_id}}">업데이트  소식 보기</a></div>
            
        </div>
        <div class="box-mywhite participate-funding" ng-if="total == 0">
            <div class="wrap-header">
                <p class="txt-title">최근 참여한 펀딩 현황</p>
            </div>
		  <!--없을때-->
            <div class="no-result" >
            	최근 참여한 나눔펀딩이 없습니다.<br />
				다양한 나눔펀딩에 참여해 보시겠어요?
                <div class="wrap-bt mt15"><a class="bt-type6" style="width:120px" href="/sponsor/user-funding/">나눔펀딩 보기</a></div>
            </div>
		</div>

        <div class="box-mywhite participate-funding list-mysupport style-add1">
        	<div class="wrap-header" style=padding-bottom:15px>
            	<p class="txt-title">참여한 펀딩 내역<em ng-if="total > -1">(총 {{total}}건)</em></p>
				<!--
                 <span class="more nopad"><select style="width:90px">
                	<option>2016년</option>
                </select></span>
				-->
            </div>
             <ul>
            	<li ng-repeat="item in list"><a ng-href="/sponsor/user-funding/view/{{item.uf_id}}">
                	<p class="txt1">{{item.uf_title}}</p>
                	<span class="txt4">{{item.uf_type_name}}<span class="bar">|</span>{{item.status}}<span ng-if="item.uf_current_amount >= item.uf_goal_amount">(후원성공)</span></span>
                    <span class="txt2"><em>{{item.uu_amount | number:N0}}</em> 원</span>
                    <time class="txt3" datetime="{{item.uu_regdate | date:'yyyy.MM.dd'}}">{{item.uu_regdate | date:'yyyy.MM.dd'}}</time>
                </a></li>
               
                
                <!--없는 경우 -->
                <li class="no-result" ng-if="total == 0">
                	등록된 내역이 없습니다.
                </li>
            </ul>

        </div>
		
       <paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
        
    </div>
    
    
<!--//-->
</div>

</asp:Content>

