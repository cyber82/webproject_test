﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="create.aspx.cs" Inherits="my_user_funding_create" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>
	<script type="text/javascript" src="/my/user-funding/create.js"></script>
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" id="hd_userpic_upload_root" runat="server"  />
	<input type="hidden" id="hd_image_domain" runat="server"  />

<div class="wrap-sectionsub-my" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">    <!--마이컴패션 디자인-->

    <div class="wrap-tab4 sectionsub-margin2">
        <a style="width:50%" href="/my/user-funding/join">참여한 펀딩</a>
        <a style="width:50%" class="selected" href="#">개설한 펀딩</a>
    </div>

    <div class="my-funding">
    	
		<div class="box-mywhite funding-profile">
        	<div class="pos-photo"><span id="btn_UserPic" class="pic photo" style="background:url('{{summary.creator_image}}') no-repeat"></span></div>
            <span class="btn_pic reg-photo">사진등록</span>
            <input type="file" accept="image/*" class="hidden_photo_btn" style="width:100px;height:100px;top:17px;left:5%;position:absolute;opacity:0.0;background:red;z-index:10">
            <p class="txt-name">{{summary.creator_name}}</p>
            <div class="row-table">
                <div class="td-field" style="width:49%">참여펀딩<em>{{summary.cnt_join | number:N0}}</em></div>
            	<div class="td-field" style="width:51%">개설펀딩<em>{{summary.cnt_create | number:N0}}</em></div>
            </div>
        </div>
        
        <div class="box-myblue support-amount mt10">
        	<p class="txt1">누적 현황  <span class="stxt1">({{today | date:'yyyy.MM.dd'}} 개설한 펀딩 기준)</span></p>
            <p class="txt-amount"><em>{{summary.amount_create | number:N0}}</em>원</p>
        </div>
		
        <div class="box-mywhite participate-funding" ng-if="ing">
        	<div class="wrap-header">
            	<p class="txt-title">진행 중인 펀딩 현황</p>
            </div>
			<a ng-href="/sponsor/user-funding/view/{{ing.uf_id}}">
				 <div class="toptop">
					<span class="frame"><span class="photo" style="background:url('{{ing.uf_image}}') no-repeat"></span></span>
					<em class="txt-type">{{ing.uf_type_name}}</em>
					<p class="txt-name" style="word-break:break-all">{{ing.uf_title}}</p>
				</div>
			</a>
			<div class="box-mygray fundingstate-info">
            	펀딩구분 : <span class="color1">{{ing.uf_type_name}}</span><br />
                진행기간 : <span class="color1">{{ing.uf_date_start | date:'yyyy.MM.dd'}} ~ {{ing.uf_date_end | date:'yyyy.MM.dd'}}</span><br />
                목표금액 : <span class="color1">{{ing.uf_goal_amount | number:N0}}원({{ing.uf_current_amount / ing.uf_goal_amount|percentage:0}})</span><br />
                후원금액 : <span class="color1">{{ing.uf_current_amount | number:N0}}원</span><br />
                남은기간 : <span class="color1">{{ ing.uf_date_end | amDifference : today : 'days' }}일</span>
            </div>

			<div>
				<a ng-href="/sponsor/user-funding/view/{{ing.uf_id}}?tab=2"><p class="txt-update">업데이트 소식({{(ing.uf_cnt_notice||0) | number:N0}})</p></a> &nbsp;&nbsp;&nbsp;&nbsp;
				<a ng-href="/sponsor/user-funding/view/{{ing.uf_id}}?tab=3"><p class="txt-funder">펀딩 참여자 ({{(ing.uf_cnt_user||0||0) | number:N0}})</p></a> 
			</div>
            
            <div class="wrap-bt">
            	<a class="bt-type7 fl" style="width:49%" href="#" ng-click="showChild($event,ing)"  ng-if="ing.uf_type == 'child'">어린이 상세보기</a>
                <a class="bt-type6 fr" style="width:49%" ng-href="/sponsor/user-funding/update/{{ing.uf_id}}">진행 중인 펀딩 수정</a>
            </div>
            
            <!--없을때-->
            
        </div>
        <div class="box-mywhite participate-funding" ng-if="total != -1 && !ing">
            <div class="wrap-header">
                <p class="txt-title">최근 개설한 펀딩 현황</p>
            </div>
		 <div class="no-result" >
            	새로운 나눔펀딩을 개설해 보시겠어요?<br />
				단계에 따라 손쉽게 생성하실 수 있습니다.
                <div class="wrap-bt mt15"><a class="bt-type6" style="width:180px" href="/sponsor/user-funding/create/">새로운 나눔펀딩 만들기</a></div>
            </div>
		</div>

        <div class="box-mywhite participate-funding list-mysupport style-add1">
        	<div class="wrap-header" style=padding-bottom:15px>
            	<p class="txt-title">개설한 펀딩 내역<em ng-if="total > -1">(총 {{total}}건)</em></p>
				<!--
                 <span class="more nopad"><select style="width:90px">
                	<option>2016년</option>
                </select></span>
				-->
            </div>
             <ul>
            	<li ng-repeat="item in list"><a ng-href="/sponsor/user-funding/view/{{item.uf_id}}">
                	<p class="txt1">{{item.uf_title}}</p>
                	<span class="txt4">{{item.uf_type_name}}<span class="bar">|</span>{{item.status}}</span>
                    <span class="txt2"><em>{{item.uf_current_amount | number:N0}}</em> 원</span>
                    <time class="txt3" datetime="{{item.uf_date_start | date:'yyyy.MM.dd'}}">{{item.uf_date_start | date:'yyyy.MM.dd'}}</time>
                </a>
                <span class="box-mygray">
                    <span class="wrap-graph"> 
                    	<span class="flag" style="left:{{item.ratio|percentage:0 }}">{{item.uf_current_amount / item.uf_goal_amount|percentage:0}}</span>   <!--하트좌표 left % 수치-->
                        <span class="bg-graph">
                        	<span class="rate" style="width:{{item.ratio|percentage:0 }}">{{item.uf_current_amount / item.uf_goal_amount|percentage:0}}</span>      <!--노란그래프 width % 수치-->
                        </span>
                        <span class="txt-graph">
                            <span class="txt1">{{item.uf_current_amount | number:N0}}원</span>
                            <span class="txt2">{{item.uf_goal_amount | number:N0}}원</span>
                        </span>
                    </span>
                </span>
                </li>
               
                <!--없는 경우 -->
                <li class="no-result" ng-if="total == 0">
                	등록된 내역이 없습니다.
                </li>
            </ul>
			
        </div>
        
       <paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 

    </div>
    
    
<!--//-->
</div>

</asp:Content>

