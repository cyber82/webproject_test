﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_default : MobileFrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		
		var sess = new UserInfo();
		userName2.Text = userName.Text = sess.UserName;
		if(!string.IsNullOrEmpty(sess.ConId) && sess.ConId.Length < 7) {
			conId.Text = string.Format("<span class='txt'>후원자 번호</span><span class='no'>{0}</span>" , sess.ConId);
		}
		
        if(Request.UrlReferrer != null && Request["lnb"] != "Y") {
            if(Request.UrlReferrer.AbsolutePath.IndexOf("/my/") > -1) {
                if(AppSession.HasCookie(this.Context))
                    Response.Redirect("/app/my/");

            }
        }

        var actionResult = new JsonWriter();
	
		// 후원어린이 없는 경우 데이타
		userName.Text = new UserInfo().UserName;
		actionResult = new ChildAction().GetChildren(null, null, null, null, null, 18, null, null, 1, 10, "waiting", -1, -1);
		if(actionResult.success) {

			var list = (List<ChildAction.ChildItem>)actionResult.data;
			Random rnd = new Random();
			var index = rnd.Next(list.Count);
			//Response.Write(list.ToJson());

			var item = list[index];
			data.Value = item.ToLowerCaseJson();

		}

	}

}