﻿$(function () {


    $(".teb_menu").click(function () {
        console.log("test");
        $(".box-howto").slideToggle(200);
        $(this).toggleClass("open");
    })

    $(".floating-letterwrite .ic1").attr("href", "/my/letter/temp/?r=" + encodeURIComponent(location.href));
    $(".floating-letterwrite .ic2").attr("href", "/my/letter/write/?r=" + encodeURIComponent(location.href));

    $(".dimmed").click(function () {
        $(".fix_btn").removeClass("on");
        $(".fix_btn img").attr("src", "/common/img/icon/pencil.png");
        $(".sub-floating").hide();
        $(".dimmed").hide();
    })

});

var mSwiper = null;

(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

        $scope.cur_page = 1;
        $scope.tot_page = 1;
        $scope.data = null;

        $scope.listParams = paramService.getParameterValues();

        // 레터내역
        $scope.getData = function () {


            $http.get("/api/my/letter.ashx?t=detail", { params: { corrId: $("#corrId").val(), attach_path: $("#upload_root").val() } }).success(function (r) {

                console.log($("#corrId").val(), $("#upload_root").val());

                if (r.success) {

                    $scope.data = $.extend([], r.data);
                    console.log("data", $scope.data);

                    var screeningname = '';
                    if ($scope.data.translationname != '' && $scope.data.translationstatus == 'END') {
                        screeningname = '<p class="screeningname">이 편지는 Compassion Mate ' + $scope.data.translationname + '님께서 번역해 주셨습니다.</p>';
                    }
                    $scope.data.translatecontent = $scope.data.translatecontent + screeningname;

                    //

                    if ($scope.data.basedata.length > 0) {
                        var childmasterid = $scope.data.basedata[0].childmasterid;
                        var correspondenceid = $scope.data.basedata[0].correspondenceid;

                        // 읽음처리
                        $http.post("/api/my/letter.ashx?t=set-read", { childmasterid: childmasterid, correspondenceid: correspondenceid });

                    }

                    $scope.tot_page = $scope.data.images.length;
                    if ($scope.tot_page > 1) {

                    }
                    $scope.images = $scope.data.images;

                    var obj = $scope.data.basedata[0];

                    $scope.data.sendcontent = $scope.data.sendcontent.replace(/\|/g, ' ');

                    setTimeout(function () {

                        if ($(".swiper-slide").length > 1) {
                            //\common\breadcrumb.ascx 에서 사용
                            $('.headerLetterDirecionCurPage').text(1);
                            $('.headerLetterDirecionTotalPage').text($(".swiper-slide").length);

                            var swiper = new Swiper('.swiper-container', {
                                //pagination: '.swiper-pagination',
                                //paginationClickable: true,
                                nextButton: '.swiper-container .next-post',
                                prevButton: '.swiper-container .prev-post',
                                onReachEnd: function () {
                                    $('.next-post').hide();
                                    $('.prev-post').show();
                                },
                                onReachBeginning: function () {
                                    $('.next-post').show();
                                    $('.prev-post').hide();
                                },
                                onSlideChangeEnd: function (swiper) {
                                    //\common\breadcrumb.ascx 에서 사용
                                    $('.headerLetterDirecionCurPage').text(swiper.activeIndex + 1);
                                }

                            });

                            mSwiper = swiper; //\common\breadcrumb.ascx 에서 사용
                        }
                    }, 500);

                } else {
                    alert(r.message);
                }
            });

        }

        // 다운로드
        $scope.download = function ($event) {
            $event.preventDefault();
            //console.log($scope.images);
            //return;
            var files = [];

            var entity = $.parseJSON(cookie.get("cps.app"));
            if (entity.device == "android") {
                $.each($scope.images, function () {
                    files.push(this);
                })
                location.href = "multi-download:" + files;
            } else {
                $.each($scope.images, function () {
                    //files.push("/api/my/letter.ashx?t=file-download&url=" + encodeURIComponent(this));

                    files.push(this + "?dl-ltr=1");
                    //files.push(encodeURIComponent(this));
                })

                multiDownload(files);
            }
            alert("다운로드가 완료되었습니다. 갤러리를 확인해 주세요")
            return false;
        }

        // 크게보기 팝업
        $scope.showLarge = function ($event, item) {
            //location.href = "/_test/zoom.html";
            $scope.zoom.show($event, item);
        },

		$scope.prev = function ($event) {
		    $event.preventDefault();
		    var val = $scope.cur_page - 1;
		    if (val < 1) {
		        return;
		    }

		    $scope.cur_page--;
		}

        $scope.next = function ($event) {
            $event.preventDefault();
            var val = $scope.cur_page + 1;
            if (val > $scope.tot_page) {
                return;
            }

            $scope.cur_page++;
        }

        //팝업
        $scope.zoom = {

            instance: null,
            item: null,

            show: function ($event, path) {
                $scope.zoom.item = path;
                popup.init($scope, "/my/letter/zoom-image.html", function (modal) {
                    $scope.zoom.instance = modal;
                    modal.show();
                }, { removeWhenClose: true });
                if ($event) {
                    $event.preventDefault();
                }
            },

            hide: function ($event) {
                $event.preventDefault();
                if (!$scope.zoom.instance)
                    return;
                $scope.zoom.instance.hide();

            },
        }

        $scope.getData();

        // 글쓰기 floating
        $scope.floating = {

            temp_count: -1,


            show: function ($event) {
                $event.preventDefault();
                $scope.floating.is_show = !$scope.floating.is_show;

                if ($(".fix_btn").hasClass("on")) {
                    $(".fix_btn").removeClass("on");
                    $(".fix_btn img").attr("src", "/common/img/icon/pencil.png");
                    $(".sub-floating").hide();
                    $(".dimmed").hide();

                } else {
                    $(".fix_btn").addClass("on");
                    $(".fix_btn img").attr("src", "/common/img/icon/close4.png");



                    if ($scope.floating.is_show && $scope.floating.temp_count < 0) {

                        $http.get("/api/my/letter.ashx?t=get-temporary-total", { params: {} }).success(function (r) {
                            if (r.success) {


                                $scope.floating.temp_count = r.data.total_temp;
                                if ($scope.floating.temp_count == 0)
                                    $scope.goReply($scope.data.basedata[0]);
                                else {
                                    $(".sub-floating").show();
                                    $(".dimmed").show();
                                }

                            }
                        });

                    }
                }
            }


        }


        $scope.goReply = function (item) {

            location.href = "/my/letter/write/?c=" + item.childmasterid + "|" + item.childkey + "|" + item.namekr + "&r=" + encodeURIComponent(location.href);

        }
    });

})();