﻿$(function () {
    // common/breadcrumb.ascx 소스중 제목에 부여된 아이디
    $(".teb_menu").click(function () {
        console.log("test");
        $(".box-howto").slideToggle(200);
        $(this).toggleClass("open");
    })

    $(".floating-letterwrite .ic1").attr("href", "/my/letter/temp/?r=" + encodeURIComponent(location.href));
    $(".floating-letterwrite .ic2").attr("href", "/my/letter/write/?r=" + encodeURIComponent(location.href));

    $(".dimmed").click(function () {
        $(".fix_btn").removeClass("on");
        $(".fix_btn img").attr("src", "/common/img/icon/pencil.png");
        $(".sub-floating").hide();
        $(".dimmed").hide();
    })

    $("#floating-btn").remove();

});

(function () {

    var app = angular.module('cps.page', []);
    app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

        $scope.total = -1;
        $scope.list = [];
        $scope.tempList = [];

        $scope.params = {
            letter: 1,
            page: 1,
            rowsPerPage: 5,		// 변경시 codebehind 갯수 수정
            orderby: "name" // name or new
        };

        // list
        $scope.getList = function (params) {
            $scope.params = $.extend($scope.params, params);

            var showList = function (list) {
                //list = list.filter(function (item) { return item.issupported === "Y" || (item.issupported === "N" && item.); })

                //$scope.list = list;
                $scope.tempList = list; //편지 갯수에 대한 정보가 없으므로 `편지삭제처리된 어린이 안보이게 처리`가 getLetterInfo에서 가능

                console.log("children", list)

                var childMasterIds = "";
                $.each($scope.tempList, function () {
                    this.commitmentdate = new Date(this.commitmentdate);
                    this.birthdate = new Date(this.birthdate);
                    this.total_receive = "0";
                    this.total_send = "0";
                    this.unreadcount = 0;
                    childMasterIds += "," + this.childmasterid;
                })

                $scope.total = list.length > 0 ? list[0].total : 0;
                if ($scope.total > 0) {
                    getLetterInfo(childMasterIds.substring(1));
                }
                else {
                    $scope.list = angular.copy($scope.tempList);
                }
            }

            // 어린이별 편지 총갯수 조회
            var getLetterInfo = function (childMasterIds) {

                $http.get("/api/my/letter.ashx?t=get-total", { params: { childMasterIds: childMasterIds } }).success(function (r) {

                    if (r.success) {

                        $.each(r.data, function () {

                            var self = this;
                            var item = $.grep($scope.tempList, function (r) {
                                return r.childmasterid == self.childmasterid;
                            })[0];

                            item.total_receive = self.total_receive;
                            item.total_send = self.total_send;
                            item.unreadcount = self.unreadcount;
                            console.log(item);

                        })

                        var listTemp = angular.copy($scope.tempList);
                        listTemp = listTemp.filter(function (item) { return item.issupported === "Y" || (item.issupported === "N" && (item.total_receive > 0 || item.total_send > 0)); })
                        $scope.list = listTemp;
                        console.log("children", listTemp);

                    } else {
                        alert(r.message);
                    }
                });

            }

            if ($scope.params.page == 1 && $scope.params.orderby == "name") {





                showList($.parseJSON($("#children").val()));
            } else {

                $http.get("/api/my/child.ashx?t=all-list", { params: $scope.params }).success(function (r) {
                    if (r.success) {

                        showList(r.data);

                    } else {
                        alert(r.message);
                    }
                });
            }

        }

        $scope.changeOrder = function () {
            $scope.getList({ page: 1 }, true);
        }

        // 상세페이지
        $scope.goView = function ($event, item) {
            $event.preventDefault();
            location.href = "/my/letter/child/" + item.childmasterid + "?name=" + encodeURIComponent(item.name) + "&childkey=" + item.childkey + "&personalnameeng=" + item.personalnameeng;
        }

        $scope.getList();

        // 글쓰기 floating
        $scope.floating = {

            temp_count: -1,


            show: function ($event) {
                $event.preventDefault();
                $scope.floating.is_show = !$scope.floating.is_show;

                if ($(".fix_btn").hasClass("on")) {
                    $(".fix_btn").removeClass("on");
                    $(".fix_btn img").attr("src", "/common/img/icon/pencil.png");
                    $(".sub-floating").hide();
                    $(".dimmed").hide();

                } else {
                    $(".fix_btn").addClass("on");
                    $(".fix_btn img").attr("src", "/common/img/icon/close4.png");

                    if ($scope.floating.is_show && $scope.floating.temp_count < 0) {

                        $http.get("/api/my/letter.ashx?t=get-temporary-total", { params: {} }).success(function (r) {
                            if (r.success) {

                                $scope.floating.temp_count = r.data.total_temp;
                                if ($scope.floating.temp_count == 0)
                                    location.href = "/my/letter/write/";
                                else {
                                    $(".sub-floating").show();
                                    $(".dimmed").show();
                                }
                            }
                        });

                    } else {
                        $(".sub-floating").show();
                        $(".dimmed").show();
                    }
                }
            }


        }

        // 어린이사진 크게보기
        $scope.childPicClick = function ($event, item) {
            $event.preventDefault();

            //console.log(idx);
            var bView = false;
            if ($('.list-letter .row  .photo .thumb.big').length == 0) {
                bView = true;
            }
            else {
                var prevChildkey = $('.list-letter .row  .photo .thumb.big').attr('childkey');
                $('.list-letter .row  .photo .thumb.big').remove();
                if (prevChildkey != item.childkey) {
                    bView = true;
                }
            }

            if (bView) {
                var obj = $event.currentTarget;
                var bigImg = $(obj).clone();
                $(bigImg).attr('childkey', item.childkey);
                $(bigImg).addClass('big');
                $(bigImg).click(function () { $('.list-letter .row  .photo .thumb.big').remove(); });
                $(bigImg).html('<div style="float:right; background-color:rgba(255,255,255,0.5); color:black; cursor:pointer; margin:5px; border-radius:3px !important; text-indent:12px;">X 닫기</div>');
                $(obj).after(bigImg);
                $(bigImg).animate({
                    position: 'absolute',
                    width: '200px',
                    height: '300px',
                    backgroundSize: '200px 300px'
                }, 300, function () {
                    // Animation complete.
                });
            }

        }

    });

})();