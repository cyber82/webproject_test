﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="child.aspx.cs" Inherits="my_letter_child" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/letter/child.js?v=1.3"></script>
	
	<style>
		.dimmed{position:fixed;top:0;right:0;bottom:0;left:0;background:#000;opacity:.8;filter:alpha(opacity=80);z-index:10}
	</style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	<input type="hidden" id="childMasterId" runat="server" />	
	<input type="hidden" id="childName" runat="server" />	
    <input type="hidden" id="personalNameEng" runat="server" />	

	<!-- 컨텐츠 영역 -->
	<div class="appContent" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		<div class="dimmed" style="display:none;"></div>
    
	
		<select id="type" style="position:absolute;left:30%;width:40%;top:10px;opacity:0.0;" ng-model="params.type" ng-change="changeType()">
			<option value="all">전체</option>
			<option value="receive">받은편지함</option>
			<option value="send">보낸편지함</option>
		</select>

		<div class="howto-letter mt20">
    		<span class="txt-link teb_menu" onClick="">How to<span class="bu"></span></span>     <!--오픈상태: class="txt-link open"   /   기본  :  class="txt-link"-->
			<span class="clear"></span>
			<div class="box-howto" style="display:none;">    <!--how to 레이어-->
        		<a href="/my/letter/letter-video">편지 영상 보기<span class="bu"></span></a>
				<a href="/my/letter/smart-letter/introduce" style="border-top: 1px solid #d9d9d9">스마트레터 서비스 신청<span class="bu"></span></a>
				<div class="wrap-bt"><a class="bt-type6" style="width:100%" href="/customer/faq/?s_type=E">편지 FAQ 바로가기</a></div>
			</div>
		</div>
    
		<a class="more-page mt10" ng-if="total > list.length" ng-click="showMore($event)" href="#">이전 편지 더 보기 <em>{{params.page}}</em> / {{totalPage}}</a>
    
		<div ng-repeat="item in list" ng-class="{'out-letterbox' : item.direction == 'send' || item.direction == 'send_ready' ,'in-letterbox' : item.direction == 'receive'}">

			<div ng-if="item.direction == 'receive'">
    			<div class="photo" style="background:url('{{item.pic}}') no-repeat"><span class="ic-new" ng-if="item.checkstatusyn == 'N'"><img src="/common/img/icon/new.png" alt="New" /></span></div>
				<div class="box">
        			<a href="#" ng-click="goView($event,item)">
						<span class="txt">{{item.display_date | date:'yyyy년 MM월 dd일'}}에 받은 편지입니다.</span>
						<span class="link">편지 보기</span>
        			</a>
					<span class="edge"></span>
				</div>
			</div>

			<div ng-if="item.direction == 'send' || item.direction == 'send_ready'">
				<a href="#" ng-if="item.direction == 'send'"  ng-click="goView($event,item)">
					<span class="txt">{{item.display_date | date:'yyyy년 MM월 dd일'}}에 보낸 편지입니다.</span>
					<span class="link">편지 보기</span>
				</a>
                <a href="#" ng-if="item.direction == 'send_ready'" ng-click="goUpdate($event,item)">
					<span class="txt">{{item.display_date | date:'yyyy년 MM월 dd일'}}에 보낸 편지입니다.</span>
					<span class="link">편지 보기</span>
				</a>
				<span class="edge"></span>

				<div class="row" ng-if="item.direction == 'send'">
        			<div class="fl">
						<%--<span ng-if="item.is_smart == 'True' && item.direction == 'send'" class="type-chk1" style="width:116px">발송완료(스마트레터)</span>
						<span ng-if="item.is_smart == 'False' && item.direction == 'send'" class="type-chk1" style="width:116px">발송완료</span>--%>
                        <span ng-if="item.is_smart == 'True' && item.direction == 'send'" class="type-chk1" style="width:116px"><span ng-bind="item.state"></span>(스마트레터)</span>
						<span ng-if="item.is_smart == 'False' && item.direction == 'send'" class="type-chk1" style="width:116px"><span ng-bind="item.state"></span></span>
        			</div>
					<div class="fr"></div>
				</div>

				<div class="row" ng-if="item.direction == 'send_ready'">
        			<div class="fl"><span class="type-chk2" style="width:116px">등록대기중</span></div>
					<div class="fr">
						<a class="modify" ng-click="goUpdate($event,item)" ng-if="item.direction == 'send_ready' && item.is_editable == 'True'">수정</a>
						<a class="cancel" ng-click="cancel($event,item.correspondenceid)" ng-if="item.is_editable == 'True' && item.correspondencetype != 'SPNPRE'">발송 취소</a>
					</div>
				</div>
			</div>

		</div>

    
		<!--편지가 없는 경우-->
		<div class="no-letterbox mt30" ng-show="total == 0">
    		편지함이 비어 있습니다.<br />어린이에게 후원자님의 따뜻한 마음을 전해 보세요!
		</div>
    
    
    
		<div id="bottom"></div>
		<!--floating-->
		<div class="floating-letterwrite">
    		<span class="fix_btn btn" ng-click="floating.show($event)"><img src="/common/img/icon/pencil.png" alt=""/></span>     <!--클릭 후  "/common/img/icon/close4.png"-->
			<div class="sub-floating">    <!--클릭 후  display:block-->
				<a class="ic1" href="/my/letter/temp" ng-show="floating.temp_count > 0">임시 저장 편지 <em>{{floating.temp_count}}</em>개</a>
				<a class="ic2" href="#" ng-click="goLetter($event)">편지 쓰기</a>
			</div>
		</div>
    
    
    
    
	</div>
	<!--// 컨텐츠 영역 -->




</asp:Content>
