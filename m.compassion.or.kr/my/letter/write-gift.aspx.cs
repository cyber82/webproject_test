﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class my_letter_write_gift : MobileFrontBasePage
{
    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    public string r = "";
    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();

        r = Request["r"].EmptyIfNull();
        if (!string.IsNullOrEmpty(r))
            btnList.HRef = r;

        domain_image.Value = ConfigurationManager.AppSettings["domain_file"];
        upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_letter);

        // 선물편지사용여부
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
        Object[] objSql = new object[1] { string.Format("select MEM_NAME_LANG0 from ETS_TC_CODE_MEMBER where Dim_Code = 'GIFT_LETTER' and MEM_Code = 'USE'") };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);
        if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
        {
            giftLetterUseYN.Value = "N";
        }
        else
        {
            giftLetterUseYN.Value = ds.Tables[0].Rows[0][0].ToString().ValueIfNull("");
        }
        if (giftLetterUseYN.Value == "N")
        {
            base.AlertWithJavascript("선물편지관리에서 사용하지 않도록 설정되어있습니다.", "location.href='/my/letter'");
        }

        //  선물리스트
        Object[] objSql2 = new object[1] { "Corr_S_GiftLetter" };
        Object[] objParam = new object[] { "DIV", "USE" };
        Object[] objValue = new object[] { "", 1 };
        var giftList = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql2, "SP", objParam, objValue).Tables[0];

        giftList.Columns.Add("price", typeof(String));
        giftList.Columns.Add("delivery_charge", typeof(String));
        giftList.Columns.Add("option", typeof(String));
        giftList.Columns.Add("hd_inventory", typeof(String));

        foreach (DataRow dr in giftList.Rows)
        {


            if (dr["IsStoreItem"].ToString().Equals("Y"))
            {
                using (StoreDataContext dao = new StoreDataContext())
                {
                    var idx = Convert.ToInt32(dr["idx"]);
                    //var entity = dao.product.FirstOrDefault(p => p.idx == idx && p.display_flag == true);
                    var entity = www6.selectQFStore<product>("idx", idx, "display_flag", 1);
                    if (entity != null)
                    {
                        //var opts = dao.product_option.Where(p => p.product_idx == idx && p.option_level == 0 && p.del_flag == false && p.display == true).ToList();
                        var opts = www6.selectQStore<product_option>("product_idx", idx, "option_level", 0, "del_flag", 0, "display", 1);

                        //var imgs = dao.product_image.Where(p => p.product_idx == idx).ToList();
                        //btn_show_child.Visible = entity.gift_flag.Value;
                        //hd_title.Value = title.Text = entity.name;
                        //hd_price.Value = entity.selling_price.ToString();
                        dr["price"] = entity.selling_price.ToString();

                        //price.Text = entity.selling_price.ToString("N0");
                        //status.Text = entity.inventory > 0 ? "판매중" : "품절";

                        //hd_inventory.Value = entity.inventory.ToString();
                        dr["hd_inventory"] = entity.inventory.ToString();

                        //product_detail.Text = entity.product_detail;

                        //hd_delivery_charge.Value = entity.delivery_charge.ToString().ValueIfNull("0");
                        dr["delivery_charge"] = entity.delivery_charge.ToString().ValueIfNull("0");

                        string option = "[{\"text\":\"옵션 없음\",\"value\":\"0\",\"price\":\"0\"}]";
                        if (opts.Count > 0)
                        {
                            string optTemp = "{\"text\":\"옵션을 선택하세요\",\"value\":\"\",\"price\":\"0\"}";
                            foreach (var opt in opts)
                            {
                                var text = opt.option_name + (opt.option_price > 0 ? "(+" + opt.option_price.Value.ToString("N0") + "원)" : "");
                                optTemp += ",{\"text\":\"" + text + "\",\"value\":\"" + opt.idx.ToString() + "\",\"price\":\"" + opt.option_price.Value.ToString() + "\"}";
                            }
                            option = "[" + optTemp + "]";
                        }
                        else
                        {
                            //option.Items.Add(new ListItem("옵션 없음", "0"));
                        }
                        dr["option"] = option;

                        //this.ViewState["image"] = "";

                        //if (imgs.Count != 0)
                        //{
                        //    this.ViewState["image"] = (Uploader.GetRoot(Uploader.FileGroup.image_shop) + imgs[0].saved_file_name).WithFileServerHost();
                        //}

                        //repeater_image.DataSource = imgs;
                        //repeater_image.DataBind();

                        //if (UserInfo.IsLogin)
                        //{
                        //    hd_user_id.Value = new UserInfo().UserId;
                        //}

                        //hd_item_id.Value = this.PrimaryKey.ToString();
                    }

                }
            }
        }

        repeater.DataSource = giftList;
        repeater.DataBind();

        if (UserInfo.IsLogin)
        {
            hd_user_id.Value = new UserInfo().UserId;
        }
    }

    protected void ListBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
            return;

        DataRowView drv = (DataRowView)e.Item.DataItem;
        string img = drv["Contents"].ToString();
        ((Image)e.Item.FindControl("imgGift")).ImageUrl = "data:image/png;charset=utf-8;base64," + img;
    }
}