﻿$(function () {

	
	$(".teb_menu").click(function () {
		console.log("test");
		$(".box-howto").slideToggle(200);
		$(this).toggleClass("open");
	})

	
	$(".floating-letterwrite .ic1").attr("href", "/my/letter/temp/?r=" + encodeURIComponent(location.href));
	$(".floating-letterwrite .ic2").attr("href", "/my/letter/write/?r=" + encodeURIComponent(location.href));

	$(".dimmed").click(function () {
		$(".fix_btn").removeClass("on");
		$(".fix_btn img").attr("src", "/common/img/icon/pencil.png");
		$(".sub-floating").hide();
		$(".dimmed").hide();
	})

	if (getParameterByName("header") == "root") {
		$(".howto-letter").show();
	} else {
		$(".howto-letter").hide();
	}

	$("#floating-btn").remove();

});

(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

		$scope.total = -1;
		$scope.totalPage = 0;
		$scope.list = [];
		
		$scope.params = {
			page: 1,
			rowsPerPage: 5,
			childMasterId: $("#childMasterId").val(),
			childKey: paramService.getParameter("childkey"),
			name: $("#childName").val(),
			type: "all",		// all , receive , send
			personalNameEng: $("#personalNameEng").val()
		};

		$scope.params = $.extend($scope.params, paramService.getParameterValues());

		// list
		$scope.getList = function (params , reload) {
			$scope.params = $.extend($scope.params, params);

			var rowsPerPage = $scope.params.rowsPerPage;
			var page = $scope.params.page;
			if (!params) {
				$scope.params.page = 1;
				$scope.params.rowsPerPage = page * rowsPerPage;
			}

			console.log($scope.params);
			$http.get("/api/my/letter.ashx?t=list", { params: $scope.params }).success(function (r) {
				$scope.params.page = page;
				$scope.params.rowsPerPage = rowsPerPage;

				if (r.success) {

					var list = $.extend([], r.data.list).reverse();
					$scope.list = reload ? list : $.merge(list, $scope.list);

					console.log("letter", $scope.list)

					$.each($scope.list, function () {
					    //this.display_date = new Date(this.displaydate);
					    this.display_date = new Date(this.display_date.substring(0, 10).replace(/-/g, '/'));
					})

					$scope.total = $scope.list.length > 0 ? $scope.list[0].total : 0;
					$scope.totalPage = Math.ceil($scope.total / 3);

					if (page < 2) {
						setTimeout(function () {
							location.href = "#bottom";
						}, 300)
					}
					
				} else {
					alert(r.message);
				}
			});
			
		}

		$scope.changeType = function () {
			
			$scope.getList({ page: 1 } , true);
		}

		$scope.showMore = function ($event) {
			$event.preventDefault();
			$scope.getList({page : parseInt($scope.params.page)+1});
		}

		// 상세페이지
		$scope.goView = function ($event, item) {
		    if (item.direction == 'send_ready') { return; }

			$event.preventDefault();
			$scope.params.direction = item.direction;

			location.href = "/my/letter/view/" + item.corrid + "?" + $.param($scope.params);
		}

		// 수정
		$scope.goUpdate = function ($event, item) {
			$event.preventDefault();
			if (item.is_pic_letter == "Y") {
			    //location.href = "/my/letter/update-pic/" + item.correspondenceid + "?" + $.param($scope.params);
                if (item.correspondencetype == 'SPNPRE' || item.correspondencetype == 'SPNLTR' || item.correspondencetype == 'SPNPPR') {
			        //location.href = "/my/letter/update-gift/" + item.correspondenceid + "?" + $.param($scope.params);
                    alert("선물편지는 수정할 수 없습니다.");
                    return false;
			    }
			    else {
				location.href = "/my/letter/update-pic/" + item.correspondenceid + "?" + $.param($scope.params);
			    }
			}
			else {
				location.href = "/my/letter/update/" + item.correspondenceid + "?" + $.param($scope.params);
			}
			return false;
		}

		$scope.getList();


		// 글쓰기 floating
		$scope.floating = {

			temp_count: -1,


			show: function ($event) {
				$event.preventDefault();
				$scope.floating.is_show = !$scope.floating.is_show;

				if ($(".fix_btn").hasClass("on")) {
					$(".fix_btn").removeClass("on");
					$(".fix_btn img").attr("src", "/common/img/icon/pencil.png");
					$(".sub-floating").hide();
					$(".dimmed").hide();

				} else {
					$(".fix_btn").addClass("on");
					$(".fix_btn img").attr("src", "/common/img/icon/close4.png");
					
					if ($scope.floating.is_show && $scope.floating.temp_count < 0) {

						$http.get("/api/my/letter.ashx?t=get-temporary-total", { params: {} }).success(function (r) {
							if (r.success) {

								$scope.floating.temp_count = r.data.total_temp;
								if ($scope.floating.temp_count == 0)
									$scope.goLetter($event);
								else {
									$(".sub-floating").show();
									$(".dimmed").show();
								}
							}
						});

					} else {
						$(".sub-floating").show();
						$(".dimmed").show();
					}
				}
			}


		}

		$scope.goLetter = function ($event) {
			if ($event) $event.preventDefault();
			location.href = "/my/letter/write/?c=" + $scope.params.childMasterId + "|" + $scope.params.childKey + "|" + $scope.params.name + "|" + $scope.params.personalNameEng + "&r=" + encodeURIComponent(location.href);

		}



	});

})();
