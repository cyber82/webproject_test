﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ready.aspx.cs" Inherits="my_letter_ready" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    <script type="text/javascript" src="/my/letter/ready.js?v=1.3"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" id="giftLetterUseYN" runat="server" value="" />

    <!-- 컨텐츠 영역 -->
    <div class="appContent" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <a id="btnList" href="#" runat="server"></a>
        <div class="menu-sympathy appContent-margin1" id="slide">

            <ul style="position: absolute; left: 5px; top: 0; width: 100%" id="slide_menu">
                <!--임시 inline style(수정가능)-->
                <% if (giftLetterUseYN.Value == "N") { %>
                <li style="float: left; display: block; width: 95px"><a href="/my/letter/write/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">글 편지</a></li>
                <li style="float: left; display: block; width: 90px"><a href="/my/letter/write-pic/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">사진 편지</a></li>
                <li style="float: left; display: block; width: 90px"><a href="/my/letter/temp/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">임시 저장</a></li>
                <li style="float: left; display: block; width: 90px"><a class="selected" href="/my/letter/ready/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">발송 준비 중</a></li>
                <li style="float: left; display: block;"></li>
                <% } else { %>
                <li style="float:left;display:block;width:80px"><a href="/my/letter/write/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">편지</a></li>
				<li style="float:left;display:block;width:75px"><a href="/my/letter/write-pic/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">사진 편지</a></li>
                <li style="float:left;display:block;width:75px"><a href="/my/letter/write-gift/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">선물 편지</a></li>
				<li style="float:left;display:block;width:75px"><a href="/my/letter/temp/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">임시 저장</a></li>
				<li style="float:left;display:block;width:90px"><a class="selected" href="/my/letter/ready/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">발송 준비 중</a></li>
                <li style="float:left;display:block;"></li>
                <% } %>
            </ul>

        </div>

        <div class="top-sendready appContent-margin1">
            발송한 편지는 발송 당일<br />
            24시까지 수정 및 발송 취소가 가능합니다.
        </div>

        <p class="result-count mt20" ng-show="total > -1">총 <em>{{total}}</em>개</p>

        <ul class="list-tempsave">
            <li ng-repeat="item in list">
                <strong class="txt-name">{{item.namekr}}</strong>
				<span ng-bind-html="item.content">
				</span>
				<span class="row">
                    <span class="fl">{{item.display_date | date:'yyyy.MM.dd'}}</span>
                    <span class="fr">
                        <span ng-if="item.direction == 'send_ready'"> &nbsp;&nbsp;</span>
                        <a class="modify" href="#" ng-click="goUpdate($event,item)" ng-if="item.direction == 'send_ready' && item.is_editable == 'True'">수정</a>
                        <a class="cancel" href="#" ng-click="cancel($event,item.correspondenceid)" ng-if="item.is_editable == 'True' && item.direction == 'send_ready'">삭제</a>


                        <a class="modify" href="#" ng-click="goUpdate($event,item)" ng-if="item.direction == 'send_temp' ">수정</a>
                        <a class="cancel" href="#" ng-click="cancel($event,item.correspondenceid)" ng-if="item.direction == 'send_temp'">삭제</a>

                    </span>
                </span>
            </li>

			
			<!--등록글 없는 경우-->
            <li class="no-result" ng-show="total == 0">
                
				<span class="align-center box-mywhite" style="padding-bottom:30px">발송 준비 중인 편지가 없습니다.</span>
            </li>
        </ul>



    </div>
    <!--// 컨텐츠 영역 -->

</asp:Content>
