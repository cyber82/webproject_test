﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;

public partial class my_letter_child : MobileFrontBasePage {

	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var requests = Request.GetFriendlyUrlSegments();
		if(requests.Count < 1) {
			Response.Redirect("/my/letter/", true);
		}

		childMasterId.Value = requests[0].ToString();
		childName.Value = Request["name"].EmptyIfNull();
        personalNameEng.Value = Request["personalnameeng"].EmptyIfNull();

    }

}