﻿
$(function () {
    $("#btn_file_remove").hide();
    /*
    {
        var uploader = attachUploader("btn_file_path");
        uploader._settings.data.fileDir = $("#upload_root").val();
        uploader._settings.data.fileType = "image";
        uploader._settings.data.rename = "y";
        uploader._settings.data.limit = 2048;
        setTimeout(function () {
            //$("input[name=userfile]").css("display", "block").css("top", "750px").css("left", "122px").width("800px").height("200px");
            $("input[name=userfile]").css("display", "block");
        }, 300);
    }
	*/


    var getAndroidVersion = function (ua) {
        ua = (ua || navigator.userAgent).toLowerCase();
        var match = ua.match(/android\s([0-9\.]*)/);
        return match ? match[1] : false;
    }

    var hidden_photo_btn = $(".hidden_photo_btn");

    var entity = $.parseJSON(cookie.get("cps.app"));
    var appDevice = null
    if (entity != null) {
        appDevice = entity.device;
    }
    var version = getAndroidVersion();
    hidden_photo_btn.click(function () {
        if (appDevice == "android" && parseFloat(version) == 4.4) {
            JSInterface.takePhoto("takePictureField", "letterwrite", $("#upload_root").val());
            return false;
        }
    });


    hidden_photo_btn.change(function () {
        if (this.files && this.files[0]) {

            if (this.files[0].size > 5242880) {
                setTimeout(function () {
                    alert("5MB 이하의 사진만 올릴 수 있습니다.")
                }, 500)

                return false;
            }

            var fileSize = this.files[0].size;
            var FR = new FileReader();
            FR.onload = function (e) {
                hybrid_api.submitUserImage($("#upload_root").val(), e.target.result, function (r) {
                    
                    if (r.success) {
                        var img = r.data;
                        $("#btn_file_remove").show();
                        alert("첨부 파일에 내용이 있는 경우 번역이 되지 않습니다");

                        $("#file_path").val(img);
                        $("#file_size").val(fileSize);

                        $(".attach_img").attr("src", ($("#domain_image").val() + img)).show();
                        $(".txt-noimg").hide();
                    }
                });
            };
            FR.readAsDataURL(this.files[0]);
        }
    });

    $("#lang_ko").click(function () {
        alert("한글 편지는 번역 과정을 거치기 때문에,\n\r영어로 작성하시면 더 빨리 전달됩니다.");

    })

    if ($("#file_path").val() != "") {
        $("#btn_file_remove").show();
    }

    $("#btn_file_remove").click(function () {

        $.post("/api/my/letter.ashx", { t: "file-delete", file_path: $("#file_path").val() }, function (r) {
            if (r.success) {

                $("#file_path").val("");
                $("#btn_file_remove").hide();
                $(".attach_img").hide();
                $(".txt-noimg").show();

            } else {
                alert(r.message)
            }
        })

        return false;
    })

    $("#letterComment").byteCount($("#comment_count"), { limit: 2000 });

    $("#letterComment").keyup(function () {
        if ($("#comment_count").text() >= 2000) {
            alert("2000byte 까지 입력할 수 있습니다.");
        }
    });


    $(".lang").click(function () {
        $(".lang").removeClass("selected");
        $(this).addClass("selected");
    })

    var sly = new Sly($("#slide"), {    // Call Sly on frame

        horizontal: 1,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 0,
        scrollBy: 1,
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1

    });

    sly.init();
    $.each($("#slide a"), function (i) {
        if ($(this).hasClass("selected")) {
            sly.activate(i);
        }
    })

    setTimeout(function () {
        if ($("#slide_menu").width() < $("#slide").width()) {
            $("#slide_menu").css("left", "5%");
        }

    }, 10);

});

(function () {

    var app = angular.module('cps.page', []);
    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, utils, paramService) {

        $scope.children = null;		// pop-children.html 에서 세팅한 어린이데이타
        $scope.selectedChildren = function () {

            if ($scope.children) {


                var result = $scope.children[0].name + ' (' + $scope.children[0].personalnameeng + ')';

                if ($scope.children.length > 1) {
                    result += " 외" + ($scope.children.length - 1);
                }

                return result;
            }
            return "어린이를 선택해주세요";
        };

        $scope.selectType1 = null;
        $scope.selectTypeName1 = null;
        $scope.selectType2 = null;
        $scope.selectTypeName2 = "편지 예문 (선택 사항)";

        $scope.selectContent = null;

        //$scope.bg_class = null;
        $scope.samplelettercode = "";

        var c = getParameterByName("c");
        if (c) {
            var childMasterId = c.split('|')[0];
            var childKey = c.split('|')[1];
            var childName = c.split('|')[2];
            var personalnameeng = c.split('|')[3];

            $scope.children = [{
                childmasterid: childMasterId, childkey: childKey, name: childName, personalnameeng: personalnameeng
            }];

        }

        // 편지언어클릭
        $scope.languageClick = function (lan) {
            $scope.selectedLang = lan;

            if ($scope.selectedItem != null) {
                $scope.samplelettercode = $scope.selectedItem.samplelettercode;

                if (lan == "ko") {
                    $scope.selectContent = $scope.selectedItem.contextkor;
                } else {
                    $scope.selectContent = $scope.selectedItem.contexteng;
                }
            }
        }


        // type1 : 3자리 , type2 : 6자리
        $scope.selectSampleType = function (type1, type2, title) {
            /*
			if (type1 == "BBI") {
				alert("후원어린이의 성장 사진과 소식이 담긴 보고서에 \r\n대한 답장 편지일 경우 선택해주세요.");
			}
			*/

            //	$scope.bg_class = "bg_" + type1;
            $scope.selectType1 = type1;
            $scope.selectType2 = type2;
            $scope.selectTypeName1 = title;

            // 기존 선택 샘플타입이랑 다르면 초기화
            if ($scope.selectedItem != null && $scope.selectedItem.samplelettertype != type1) {
                $scope.selectedItem = null;
                $scope.selectContent = "";
            }

        }

        $scope.submit = function ($event, status) {
            $event.preventDefault();
            var children = [];
            //var checkChildKey = true;
            if ($scope.children) {
                $.each($scope.children, function () {
                    //if (typeof this.childkey == "undefined" || this.childkey == "undefined" || this.childkey == null || this.childkey == "") {
                    //    checkChildKey = false;
                    //}
                    children.push({ childMasterId: this.childmasterid, childKey: this.childkey });
                });
            }

            //if (!checkChildKey) {
            //    alert("어린이키에 문제가 있습니다. 관리자에게 문의해주세요.");
            //    return false;
            //}

            if (children.length < 1) {
                alert("편지를 보내실 어린이를 선택해주세요");
                return false;
            }

            if ($scope.selectedLang == null) {
                alert("편지에 사용하실 언어를 선택해 주세요.");
                return false;
            }

            if ($("input[name=letterType]:checked").length < 1) {
                alert("편지종류를 선택해 주세요.");
                return false;
            }

            if ($("#letterComment").val().trim().length < 1) {
                alert("편지내용을 입력해 주세요.");
                return false;
            }

            var lang = $scope.selectedLang;
            if (lang == "ko")
                lang = "한글";
            else
                lang = "영어";
            if (status == "N" && !confirm("[언어 : " + lang + " / 편지타입 : " + $scope.selectTypeName1 + "] 로 발송 하시겠습니까?"))
                return false;

            var isTempChange = 0;
            // 한글 선택 예문이 변경되지 않았다면 영문으로 설정할 수 있도록
            if ($scope.selectedItem != undefined && $scope.selectedItem != null && $scope.selectedItem.contextkor == $("#letterComment").val() && $scope.selectedLang == "ko")
                isTempChange = 1

            var param = {
                t: "add",
                samplelettercode: $scope.samplelettercode,
                file_path: $("#file_path").val(),
                file_size: $("#file_size").val(),
                lang: $scope.selectedLang,
                status: status,
                letter_comment: $("#letterComment").val(),
                letter_type: $scope.selectType2,
                children: $.toJSON(children),
                isTempChange: isTempChange,
            };

            //console.log(param);
            //	return;

            $http.post("/api/my/letter.ashx", param).success(function (r) {
                if (r.success) {

                    if (status == "T") {
                        alert("임시 저장이 완료되었습니다.");
                        location.href = "/my/letter/temp/"
                        return;
                    }

                    var ids = "";
                    $.each($scope.children, function () {
                        ids += "," + this.childmasterid;
                    });

                    //		alert("편지발송이 등록되었습니다. 보내신 편지는 당일 24시 내에 수정/취소가 가능합니다");
                    location.href = "/my/letter/complete?ids=" + ids.substring(1);
                } else {
                    alert(r.message);
                }
            });

        }

        // 어린이팝업
        $scope.showChildren = function ($event) {
            $event.preventDefault();
            loading.show();
            popup.init($scope, "/my/letter/pop-children.html?v=2", function (modal) {

                modal.show();
                initPopChildren($http, $scope, modal, utils, paramService);

            }, { top: 0, iscroll: true, removeWhenClose: true });

        }

        // 편지예문팝업
        $scope.showSample = function ($event) {
            $event.preventDefault();
            if ($scope.selectedLang == null) {
                alert("편지 언어를 선택해 주세요");
                return;
            }
            if (!$scope.selectType1) {
                alert("편지 종류를 선택해 주세요");
                return;
            }
            loading.show();
            popup.init($scope, "/my/letter/pop-letter-sample.html", function (modal) {

                modal.show();
                initPopLetterSample($http, $scope, modal, utils, $scope.selectType1, $scope.selectTypeName1);

            }, { top: 0, iscroll: true, removeWhenClose: true });

        }

    });

})();

var attachUploader = function (button) {

    return new AjaxUpload(button, {
        action: '/common/handler/upload',
        responseType: 'json',
        onChange: function () {
        },
        onSubmit: function (file, ext) {
            this.disable();
        },
        onComplete: function (file, response) {

            this.enable();

            if (response.success) {

                $("#btn_file_remove").show();
                alert("첨부 파일에 내용이 있는 경우 번역이 되지 않습니다");

                $("#file_path").val(response.name);
                //$("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
                $("#file_size").val(response.size);

                $(".attach_img").attr("src", ($("#domain_image").val() + response.name)).show();
                $(".txt-noimg").hide();
            } else
                alert(response.msg);
        }
    });
}