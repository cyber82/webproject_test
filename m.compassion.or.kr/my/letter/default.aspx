﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_letter_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/letter/default.js?v=1.4"></script>


	<style>
		.dimmed{position:fixed;top:0;right:0;bottom:0;left:0;background:#000;opacity:.8;filter:alpha(opacity=80);z-index:10}
	</style>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

<input type="hidden" runat="server" id="children" />

	
	<!-- 컨텐츠 영역 -->
	<div class="appContent"  ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		<div class="dimmed" style="display:none;"></div>

		<select id="orderby" style="position:absolute;left:30%;width:40%;top:10px;opacity:0.0;" ng-model="params.orderby" ng-change="changeOrder()">
			<option value="name">어린이 이름 가나다 순</option>
			<option value="new">최신 결연일 순</option>
		</select>


    	<asp:PlaceHolder runat="server" ID="ph_unread" Visible="false">
			<div class="unread-letter appContent-margin1"><a href="#" runat="server" id="btn_unread"></a></div>
		</asp:PlaceHolder>
    
		<div class="howto-letter mt20">
    		<span class="txt-link  teb_menu" onClick="">How to<span class="bu"></span></span>     <!--오픈상태: class="txt-link open"   /   기본  :  class="txt-link"-->
			<span class="clear"></span>
			<div class="box-howto" style="display:none;">    <!--how to 레이어-->
        		<a href="/my/letter/letter-video">편지 영상 보기<span class="bu"></span></a>
                <%--신진희님 요청 스마트레터 서비스 신청하기 어린이 편지 상세 화면이랑 통일  문희원 2017-04-07--%>
                <a href="/my/letter/smart-letter/introduce" style="border-top: 1px solid #d9d9d9">스마트레터 서비스 신청<span class="bu"></span></a>
				<div class="wrap-bt"><a class="bt-type6" style="width:100%" href="/customer/faq/?s_type=E">편지 FAQ 바로가기</a></div>
			</div>
		</div>
    
        <style>
        .list-letter .row >.photo>.thumb { background-size:84px !important; }
        .list-letter .row >.photo>.thumb.big { 
            position: absolute !important;
            z-index: 1;
            border-radius: 10px !important;
            background-Size: 200px 300px !important; 
            border: 1px solid #888888;
            box-shadow:5px 5px 5px rgba(100,100,100,0.8);
            text-indent:initial !important;
            margin-left:90px;
            margin-top:-84px;
            background-color:white !important;
        }
        </style>
		<ul class="list-letter">
    		<li ng-repeat="item in list">
				<span class="row">
					<span class="photo"><span class="thumb" style="background:url('{{item.pic}}') no-repeat; cursor:pointer;" ng-click="childPicClick($event, item)"></span><img ng-show="item.unreadcount > 0" src="/common/img/icon/new.png" alt="New" class="ic-new" /></span>
					<p class="txt-name">{{item.name}}</p>
					<p class="txt-letter">받은 편지 <em>{{item.total_receive}}</em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 보낸 편지 <em>{{item.total_send}}</em></p>
				</span>
				<span class="wrap-bt"><a class="bt-type4" style="width:100%" ng-click="goView($event,item)">편지함 바로가기</a></span>
			</li>
		</ul>
    
		<paging  class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>

        <%--신진희님 요청 스마트레터 서비스 신청하기 어린이 편지 상세 화면이랑 통일 하기 위헤 none 처리  문희원 2017-04-07--%>
		<div class="appmain-banner mt20" style="display:none;">
    		<a href="/my/letter/smart-letter/introduce"><img src="/common/img/app/banner3.jpg" alt="스마트레터 서비스 신청하기 소중한 기념일, 편지 쓰는 걸 깜빡 잊을까 염려되시나요? 컴패션이 후원자님을 대신하여 편지를 보내드립니다!" /></a>
		</div>
    
		<!--floating-->
		<div class="floating-letterwrite">
    		<span class="fix_btn btn" ng-click="floating.show($event)"><img src="/common/img/icon/pencil.png" alt=""/></span>     <!--클릭 후  "/common/img/icon/close4.png"-->
			<div class="sub-floating">    <!--클릭 후  display:block-->
				<a class="ic1" href="/my/letter/temp" ng-show="floating.temp_count > 0">임시 저장 편지 <em>{{floating.temp_count}}</em>개</a>
				<a class="ic2" href="/my/letter/write">편지 쓰기</a>
			</div>
		</div>
    
	</div>
	<!--// 컨텐츠 영역 -->

</asp:Content>
