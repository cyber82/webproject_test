﻿$(function () {

    $("#btn_file_remove").hide();
    /*
    {
        var uploader = attachUploader("btn_file_path");
        uploader._settings.data.fileDir = $("#upload_root").val();
        uploader._settings.data.fileType = "image";
        uploader._settings.data.rename = "y";
        uploader._settings.data.limit = 2048;

        setTimeout(function () {
            //$("input[name=userfile]").css("display", "block").css("top", "380px").css("left", "190px").width("384px").height("139px");
            $("input[name=userfile]").css("display", "block");
        }, 300);
    }
	*/

    var getAndroidVersion = function (ua) {
        ua = (ua || navigator.userAgent).toLowerCase();
        var match = ua.match(/android\s([0-9\.]*)/);
        return match ? match[1] : false;
    }

    var hidden_photo_btn = $(".hidden_photo_btn");

    var entity = $.parseJSON(cookie.get("cps.app"));
    var appDevice = entity.device;
    var version = getAndroidVersion();
    hidden_photo_btn.click(function () {
        if (appDevice == "android" && parseFloat(version) == 4.4) {
            JSInterface.takePhoto("takePictureField", "letterupdatepic", $("#upload_root").val());
            return false;
        }
    });
    hidden_photo_btn.change(function () {
        if (this.files && this.files[0]) {
            if (this.files[0].size > 5242880) {
                alert("5MB 이하의 사진만 올릴 수 있습니다.")
                return false;
            }
            var fileSize = this.files[0].size;
            var FR = new FileReader();
            FR.onload = function (e) {
                hybrid_api.submitUserImage($("#upload_root").val(), e.target.result, function (r) {
                    //console.log(r);
                    if (r.success) {
                        var img = r.data;
                        $("#btn_file_remove").show();
                        alert("사진을 첨부하실 경우 편지는 일반우편과 동일한방식으로 배송됩니다.\n\r첨부 파일이 텍스트인 경우 번역이 되지 않으니 이 점 유의해 주세요.");
                        $("#file_path").val(img);
                        $("#file_size").val(fileSize);
                        $(".attach_img").attr("src", ($("#domain_image").val() + img)).show();
                        $(".txt-noimg").hide();



                    }
                });
            };
            FR.readAsDataURL(this.files[0]);
        }
    });


    $("#lang_ko").click(function () {
        if ($('#editable').val() != 'true') return;
        alert("한글으로는 번역 과정을 거치기 때문에,\n\r영어로 사용하시면 편지가 더 빨리 전달됩니다.");
    })

    if ($("#file_path").val() != "") {
        $("#btn_file_remove").show();
        $(".attach_img").attr("src", ($("#domain_image").val() + $("#file_path").val())).show();
        $(".attach_img").show();
        $(".txt-noimg").hide();

        $(".attach_img").load(function () {
            //alert($(this).height());
            $(".hidden_photo_btn").height($(this).height());
        });
    }

    $("#btn_file_remove").click(function () {

        $.post("/api/my/letter.ashx", { t: "file-delete", file_path: $("#file_path").val() }, function (r) {
            if (r.success) {

                $("#file_path").val("");

                $("#btn_file_remove").hide();
                $(".attach_img").hide();
                $(".txt-noimg").show();

            } else {
                alert(r.message)
            }
        })

        return false;
    })


    $("#letterComment").textCount($("#comment_count"), { limit: 50 });

    $("#letterComment").keyup(function () {
        if ($("#comment_count").text() >= 50) {
            alert("50자 까지 입력할 수 있습니다.");
        }
        //if (getByteLength($(this).val()) >= 100) {
        //    alert("한글 50자(영문일 경우 100자) 까지 입력할 수 있습니다.");
        //}
    });

    $(".lang").click(function () {
        if ($('#editable').val() != 'true') return;
        $(".lang").removeClass("selected");
        $(this).addClass("selected");
    })

    var sly = new Sly($("#slide"), {    // Call Sly on frame

        horizontal: 1,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 0,
        scrollBy: 1,
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1

    });

    sly.init();
    $.each($("#slide a"), function (i) {
        if ($(this).hasClass("selected")) {
            sly.activate(i);
        }
    })

    setTimeout(function () {
        if ($("#slide_menu").width() < $("#slide").width()) {
            $("#slide_menu").css("left", "5%");
        }

    }, 10);

});

(function () {

    var app = angular.module('cps.page', []);
    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService, utils) {

        $scope.requesting = false;
        $scope.total = -1;

        $scope.data = [];
        $scope.list = [];
        $scope.params = {
            page: 1,
            rowsPerPage: 4
        };

        $scope.children = null;		// pop-children.html 에서 세팅한 어린이데이타
        $scope.selectedChildren = function () {

            if ($scope.children) {

                var result = $scope.children[0].namekr + ' (' + $scope.children[0].personalnameeng + ')';
                if ($scope.children.length > 1) {
                    result += " 외" + ($scope.children.length - 1);
                }

                return result;
            }
            return "어린이를 선택해주세요";
        };

        $scope.repCorrespondenceID = "";

        $scope.listParams = paramService.getParameterValues();


        $scope.submit = function ($event, status) {
            $event.preventDefault();
            if ($scope.requesting) return false;

            var children = [];
            //var checkChildKey = true;
            if ($scope.children) {
                $.each($scope.children, function () {
                    //if (typeof this.childkey == "undefined" || this.childkey == "undefined" || this.childkey == null || this.childkey == "") {
                    //    checkChildKey = false;
                    //}
                    children.push({ childMasterId: this.childmasterid, childKey: this.childkey });
                });
            }

            //if (!checkChildKey) {
            //    alert("어린이키에 문제가 있습니다. 관리자에게 문의해주세요.");
            //    return false;
            //}

            if (children.length < 1) {
                alert("편지를 보내실 어린이를 선택해주세요");
                return false;
            }

            if ($(".lang.selected").length < 1) {
                alert("편지에 사용하실 언어를 선택해 주세요.");
                return false;
            }

            if ($("#file_path").val() == "") {
                alert("사진을 선택해주세요.");
                return false;
            }

            if (status == "N" && !confirm("[언어 : " + $(".lang.selected").data("text") + "] 로 등록 하시겠습니까?"))
                return false;

            var param = {
                t: "update",
                repCorrespondenceID: $scope.repCorrespondenceID,
                samplelettercode: $scope.samplelettercode,
                file_path: $("#file_path").val(),
                file_size: $("#file_size").val(),
                lang: $(".lang.selected").data("val"),
                letter_comment: $("#letterComment").val(),
                letter_type: "SPNLTR",
                children: $.toJSON(children),
                status: status

            };

            console.log(param);
            //return;

            $scope.requesting = true;
            $http.post("/api/my/letter.ashx", param).success(function (r) {
                $scope.requesting = false;
                if (r.success) {

                    if (status == "T") {
                        alert("임시 저장이 완료되었습니다.");
                        location.href = "/my/letter/temp/"
                        return;
                    }

                    console.log(r.data);
                    //location.href = "/my/letter/#send"
                    var ids = "";
                    $.each($scope.children, function () {
                        ids += "," + this.childmasterid;
                    });

                    //location.href = $("#btnList").attr("href");
                    location.href = "/my/letter/complete?ids=" + ids.substring(1);

                } else {
                    alert(r.message);
                }
            });

        }

        $scope.loadData = function () {
            var data = $.parseJSON($("#save_data").val());
            console.log("loadData", data);

            $scope.children = data.children;

            $scope.status = data.status;
            $scope.repCorrespondenceID = data.repcorrespondenceid;
            $scope.selectChilden = data.children;

            $("#file_path").val(data.filenamewithpath);
            $("#lb_file_path").val(data.filename);
            if (data.lang == "ko") {
                $(".lang[data-val='ko']").addClass("selected");
            } else {
                $(".lang[data-val='en']").addClass("selected");
            }

            $scope.selectContent = data.lettercomment;

            $scope.editable = $("#editable").val() === 'true';
        }

        $scope.cancel = function ($event, correspondenceId) {
            $event.preventDefault();

            var msg = ($scope.status == "N") ? "편지 발송을 취소하시겠어요?" : "편지를 삭제하시겠습니까?";

            if (!confirm(msg)) return;

            $http.post("/api/my/letter.ashx?t=cancel", { c: correspondenceId }).success(function (r) {

                //alert(r.message);

                if (r.success) {
                    location.href = $("#btnList").attr("href");

                }
            });

            return false;
        }

        $scope.loadData();

    });

})();

var attachUploader = function (button) {
    return new AjaxUpload(button, {
        action: '/common/handler/upload',
        responseType: 'json',
        onChange: function () {
        },
        onSubmit: function (file, ext) {
            this.disable();
        },
        onComplete: function (file, response) {

            this.enable();

            if (response.success) {
                $('#newFile').val('Y');
                $("#btn_file_remove").show();
                alert("사진을 첨부하실 경우 편지는 일반우편과 동일한방식으로 배송됩니다.\n\r첨부 파일이 텍스트인 경우 번역이 되지 않으니 이 점 유의해 주세요.");

                $("#file_path").val(response.name);
                $("#file_size").val(response.size);
                $(".attach_img").attr("src", ($("#domain_image").val() + response.name)).show();
                $(".txt-noimg").hide();

            } else
                alert(response.msg);
        }
    });
}