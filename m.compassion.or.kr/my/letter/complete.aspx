﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete.aspx.cs" Inherits="my_letter_complete" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	
<!-- 컨텐츠 영역 -->
<div class="appContent">

	<div class="letterwrite-sendok">
		<span Id="pic" runat="server" class="pic" style=""></span>
        <strong>편지 등록이 완료되었습니다.<br />
            어린이가 후원자님의 편지를 통해 새 힘을 <br />
            얻을 수 있도록 잠시 기도해주시겠어요?
		</strong>
        등록된 편지는 당일 24시 전까지 수정/취소가 가능합니다.
    </div>
   <div class="wrap-bt mb30" ><a class="bt-type6" style="width:100%" href="/my/letter/ready/">확인</a></div>
    
        
</div>
<!--// 컨텐츠 영역 -->

</asp:Content>

