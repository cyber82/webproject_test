﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;

public partial class my_letter_update_pic : MobileFrontBasePage
{


    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    public string r = "";
    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();

        var requests = Request.GetFriendlyUrlSegments();
        if (requests.Count < 1)
        {
            Response.Redirect("/my/letter/", true);
        }
        base.PrimaryKey = requests[0];      // correspondenceID

        domain_image.Value = ConfigurationManager.AppSettings["domain_file"];
        upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_letter);

        if (Request["type"] == "send_temp")
        {
            btnList.HRef = string.Format("/my/letter/temp?{0}", this.ViewState["q"]);
        }
        else if (Request["type"] == "send_ready")
        {
            btnList.HRef = string.Format("/my/letter/ready?{0}", this.ViewState["q"]);
        }
        else
        {
            btnList.HRef = string.Format("/my/letter/child/{0}?{1}", Request["childmasterid"], this.ViewState["q"]);
        }

        this.LoadData();
    }

    public void LoadData()
    {
        try
        {

            LetterAction.SaveEntity entity = new LetterAction.SaveEntity();

            CommonLib.WWWService.ServiceSoap _wwwService = new CommonLib.WWWService.ServiceSoapClient();
            var sRepCorrespondenceID = this.PrimaryKey.ToString();
            //DataSet
            //DataTable dtChildInfo = _wwwService.getWebMailUpdateList(sRepCorrespondenceID).Tables[0];

            DataTable dtChildInfo = (DataTable)new LetterAction().GetTemporaryDetail(sRepCorrespondenceID).data;
            //sponCount.Text = dtChildInfo.Rows.Count.ToString();

            //	Response.Write(dtChildInfo.ToJson());
            foreach (DataRow dr in dtChildInfo.Rows)
            {
                entity.children.Add(new LetterAction.SaveEntity.child()
                {
                    childKey = dr["childKey"].ToString(),
                    childMasterId = dr["childMasterId"].ToString(),
                    nameKr = dr["ChildName"].ToString(),
                    personalNameEng = dr["PersonalNameEng"].ToString()
                });
            }

            if (dtChildInfo.Rows.Count > 0)
            {
                //변수선언
                string sCorrespondenceWebID = string.Empty;         //CorrespondenceWebID
                string vChildMasterID = string.Empty;               //ChildMasterID
                string sChildKey = string.Empty;                    //ChildKey
                string sCorrespondenceType = string.Empty;          //편지종류
                string sCorrespondenceContext = string.Empty;       //편지내용
                string sAttachYn = string.Empty;                    //첨부파일여부
                string sLanguageType = string.Empty;                //언어타입
                string sOrder = string.Empty;                       //편지종류 순번 2012-10-18 추가



                //바인딩
                sCorrespondenceWebID = dtChildInfo.Rows[0]["CorrespondenceWebID"].ToString();
                vChildMasterID = dtChildInfo.Rows[0]["ChildMasterID"].ToString();
                sChildKey = dtChildInfo.Rows[0]["ChildKey"].ToString();
                sCorrespondenceType = dtChildInfo.Rows[0]["CorrespondenceType"].ToString();
                sCorrespondenceContext = dtChildInfo.Rows[0]["CorrespondenceContext"].ToString();
                sAttachYn = dtChildInfo.Rows[0]["AttachYn"].ToString();
                sLanguageType = dtChildInfo.Rows[0]["LanguageType"].ToString();
                entity.status = dtChildInfo.Rows[0]["corrStatus"].ToString();
                if (sLanguageType == "한글")
                {
                    entity.lang = "ko";
                }
                else
                {
                    entity.lang = "en";
                }
                entity.letterComment = sCorrespondenceContext;
                entity.repCorrespondenceID = this.PrimaryKey.ToString();

                //첨부파일이 있을경우
                if (sAttachYn.Equals("Y"))
                {
                    DataSet dsAttachFileYn = new DataSet();

                    dsAttachFileYn = _wwwService.getWebMailFileName(sRepCorrespondenceID);

                    if (dsAttachFileYn.Tables[0].Rows.Count > 0)
                    {
                        //entity.fileNameWithPath = (upload_root.Value + dsAttachFileYn.Tables[0].Rows[0]["FileName"].ToString()).WithFileServerHost();
                        entity.fileNameWithPath = (upload_root.Value + dsAttachFileYn.Tables[0].Rows[0]["FileName"].ToString());

                    }

                }

                if (sCorrespondenceType.Trim().Equals("XCSLTR") || sCorrespondenceType.Trim().Equals("XCSATM") || sCorrespondenceType.Trim().Equals("SPNATM"))
                {
                    sCorrespondenceType = "SPNLTR";
                }
                //생일카드
                else if (sCorrespondenceType.Trim().Equals("XCSBDC"))
                {
                    sCorrespondenceType = "SPNBDC";
                }
                //어린이날카드
                else if (sCorrespondenceType.Trim().Equals("XCSBBC"))
                {
                    sCorrespondenceType = "SPNBBC";
                }
                //크리스마스카드
                else if (sCorrespondenceType.Trim().Equals("XCSCC"))
                {
                    sCorrespondenceType = "SPNCMS";
                }
                //추가 2012-10-18 편지종류 추가
                //첫편지
                else if (sCorrespondenceType.Trim().Equals("XCSBIO"))
                {
                    sCorrespondenceType = "SPNBIO";
                }
                //성장보고 답장편지
                else if (sCorrespondenceType.Trim().Equals("XCSBBI"))
                {
                    sCorrespondenceType = "SPNBBI";
                }
                entity.samplelettercode = sCorrespondenceType;

                string corrStatus = dtChildInfo.Rows[0]["corrStatus"].ToString();
                var bEditable = corrStatus == "T" || (corrStatus == "N" && dtChildInfo.Rows[0]["RegisterDate"].ToString().Substring(0, 10) == DateTime.Now.ToString("yyyy-MM-dd"));
                editable.Value = bEditable.ToString().ToLower();
            }

            save_data.Value = entity.ToLowerCaseJson();

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
            base.AlertWithJavascript("데이타를 불러오는 도중 에러가 발생했습니다.", "goBack()");
            return;
        }
    }


}