﻿$(function () {
    $("#btn_file_remove").hide();
    /*
    {
        var uploader = attachUploader("btn_file_path");
        uploader._settings.data.fileDir = $("#upload_root").val();
        uploader._settings.data.fileType = "image";
        uploader._settings.data.rename = "y";
        uploader._settings.data.limit = 2048;

        setTimeout(function () {
            //$("input[name=userfile]").css("display", "block").css("top", "380px").css("left", "190px").width("384px").height("139px");
            $("input[name=userfile]").css("display", "block");
        }, 300);
    }
	*/



    var getAndroidVersion = function (ua) {
        ua = (ua || navigator.userAgent).toLowerCase();
        var match = ua.match(/android\s([0-9\.]*)/);
        return match ? match[1] : false;
    }

    var hidden_photo_btn = $(".hidden_photo_btn");

    //var entity = $.parseJSON(cookie.get("cps.app"));
    //var appDevice = entity.device;
    var entity = $.parseJSON(cookie.get("cps.app"));
    var appDevice = null
    if (entity != null) {
        appDevice = entity.device;
    }

    var version = getAndroidVersion();
    hidden_photo_btn.click(function () {
        if (appDevice == "android" && parseFloat(version) == 4.4) {
            JSInterface.takePhoto("takePictureField", "letterwritepic", $("#upload_root").val());
            return false;
        }
    });

    hidden_photo_btn.change(function () {
        if (this.files && this.files[0]) {
            if (this.files[0].size > 5242880) {
                setTimeout(function () {
                    alert("5MB 이하의 사진만 올릴 수 있습니다.")
                }, 500)
                return false;
            }
            var fileSize = this.files[0].size;
            var FR = new FileReader();
            FR.onload = function (e) {
                hybrid_api.submitUserImage($("#upload_root").val(), e.target.result, function (r) {
                    //console.log(r);
                    if (r.success) {
                        var img = r.data;
                        $("#btn_file_remove").show();
                        alert("첨부 파일에 내용이 있는 경우 번역이 되지 않습니다");
                        $("#file_path").val(img);
                        $("#file_size").val(fileSize);
                        $(".attach_img").attr("src", ($("#domain_image").val() + img)).show();
                        $(".txt-noimg").hide();
                    }
                });
            };
            FR.readAsDataURL(this.files[0]);
        }
    });

    $("#lang_ko").click(function () {
        alert("한글으로는 번역 과정을 거치기 때문에,\n\r영어로 사용하시면 편지가 더 빨리 전달됩니다.");

    })

    if ($("#file_path").val() != "") {
        $("#btn_file_remove").show();
        $(".txt-noimg").hide();
    }

    $("#btn_file_remove").click(function () {

        $.post("/api/my/letter.ashx", { t: "file-delete", file_path: $("#file_path").val() }, function (r) {
            if (r.success) {

                $("#file_path").val("");

                $("#btn_file_remove").hide();
                $(".attach_img").hide();
                $(".txt-noimg").show();

            } else {
                alert(r.message)
            }
        })

        return false;
    })

    $("#letterComment").textCount($("#comment_count"), { limit: 50 });

    $("#letterComment").keyup(function () {
        if ($("#comment_count").text() >= 50) {
            alert("50자 까지 입력할 수 있습니다.");
        }
    });

    $(".lang").click(function () {
        $(".lang").removeClass("selected");
        $(this).addClass("selected");
    })

    var sly = new Sly($("#slide"), {    // Call Sly on frame

        horizontal: 1,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 0,
        scrollBy: 1,
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1

    });

    sly.init();
    $.each($("#slide a"), function (i) {
        if ($(this).hasClass("selected")) {
            sly.activate(i);
        }
    })

    setTimeout(function () {
        if ($("#slide_menu").width() < $("#slide").width()) {
            $("#slide_menu").css("left", "5%");
        }

    }, 10);

});

(function () {

    var app = angular.module('cps.page', []);
    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, utils, paramService) {

        $scope.children = null;		// pop-children.html 에서 세팅한 어린이데이타
        $scope.selectedChildren = function () {
            if ($scope.children) {

                var result = $scope.children[0].name + ' (' + $scope.children[0].personalnameeng + ')';
                if ($scope.children.length > 1) {
                    result += " 외" + ($scope.children.length - 1);
                }

                return result;
            }
            return "어린이를 선택해주세요";
        };


        var c = getParameterByName("c");
        if (c) {
            var childMasterId = c.split('|')[0];
            var childKey = c.split('|')[1];
            var childName = c.split('|')[2];
            var personalnameeng = c.split('|')[3];

            $scope.children = [{
                childmasterid: childMasterId, childkey: childKey, name: childName, personalnameeng: personalnameeng
            }];

        }

        $scope.submit = function ($event, status) {
            $event.preventDefault();
            var children = [];
            //var checkChildKey = true;
            if ($scope.children) {
                $.each($scope.children, function () {
                    //if (typeof this.childkey == "undefined" || this.childkey == "undefined" || this.childkey == null || this.childkey == "") {
                    //    checkChildKey = false;
                    //}
                    children.push({ childMasterId: this.childmasterid, childKey: this.childkey });
                });
            }

            //if (!checkChildKey) {
            //    alert("어린이키에 문제가 있습니다. 관리자에게 문의해주세요.");
            //    return false;
            //}

            if (children.length < 1) {
                alert("편지를 보내실 어린이를 선택해주세요");
                return false;
            }

            if ($(".lang.selected").length < 1) {
                alert("편지에 사용하실 언어를 선택해 주세요.");
                return false;
            }

            if ($("#file_path").val() == "") {
                alert("사진을 선택해주세요.");
                location.href = "#l";
                return false;
            }

            if (status == "N" && !confirm("[언어 : " + $(".lang.selected").data("text") + "] 로 발송 하시겠습니까?"))
                return false;

            var param = {
                t: "add",
                samplelettercode: "",
                file_path: $("#file_path").val(),
                file_size: $("#file_size").val(),
                lang: $(".lang.selected").data("val"),
                status: status,
                letter_comment: $("#letterComment").val(),
                letter_type: "SPNPIC",		// 읿반편지  SPNPIC 로 변경 문희원 = 2017-04-24
                children: $.toJSON(children),
                is_pic_letter: "Y"

            };

            console.log(param);
            //return;

            $http.post("/api/my/letter.ashx", param).success(function (r) {
                if (r.success) {

                    if (status == "T") {
                        alert("임시 저장이 완료되었습니다.");
                        location.href = "/my/letter/temp/"
                        return;
                    }

                    var ids = "";
                    $.each($scope.children, function () {
                        ids += "," + this.childmasterid;
                    });

                    //		alert("편지발송이 등록되었습니다. 보내신 편지는 당일 24시 내에 수정/취소가 가능합니다");
                    location.href = "/my/letter/complete?ids=" + ids.substring(1);
                } else {
                    alert(r.message);
                }
            });

        }

        // 어린이팝업
        $scope.showChildren = function ($event) {
            $event.preventDefault();

            popup.init($scope, "/my/letter/pop-children.html?v=2", function (modal) {

                modal.show();
                initPopChildren($http, $scope, modal, utils, paramService);

            }, { top: 0, iscroll: true, removeWhenClose: true });

        }


    });

})();

var attachUploader = function (button) {

    return new AjaxUpload(button, {
        action: '/common/handler/upload',
        responseType: 'json',
        onChange: function () {
        },
        onSubmit: function (file, ext) {
            this.disable();
        },
        onComplete: function (file, response) {

            this.enable();

            if (response.success) {

                $("#btn_file_remove").show();
                alert("첨부 파일에 내용이 있는 경우 번역이 되지 않습니다");

                $("#file_path").val(response.name);
                //$("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
                $("#file_size").val(response.size);

                $(".attach_img").attr("src", ($("#domain_image").val() + response.name)).show();
                $(".txt-noimg").hide();

            } else
                alert(response.msg);
        }
    });
}