﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="my_letter_view" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/multifile-download.js"></script>
	<script type="text/javascript" src="/my/letter/view.js?v=1220"></script>
	<script src="/assets/jquery/jquery.panzoom.js"></script>
	<script src="/common/js/util/swiper.min.js"></script>
	<link href="/common/css/swiper.css" rel="stylesheet" />
		
	<style>
		.dimmed{position:fixed;top:0;right:0;bottom:0;left:0;background:#000;opacity:.8;filter:alpha(opacity=80);z-index:10}
	</style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="upload_root" value="" />
	<input type="hidden" runat="server" id="corrId" value="" />
	
	<!-- 컨텐츠 영역 -->
	<div class="appContent" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		<div class="dimmed" style="display:none;"></div>
    
		<!-- 편지지 -->
        <%--<section class="app-intro-indicator">
            <div class="swiper-pagination" style="width:100%"></div>
	    </section>--%>
		<div class="wrap-postbox appContent-margin1 swiper-container">
			<span class="prev-post" style="display:none">이전보기</span>
			<span class="next-post" style="display:none">다음보기</span>
        
			<div style="width:100%" class="swiper-wrapper">
				<!--이미지-->
				<div class="swiper-slide" ng-repeat="item in images" ng-click="showLarge($event , item)" ng-if="images.length > 0">
					<img ng-src="{{item}}" alt="편지이미지" style="width:100%" />
				</div>

				<!--div class="preview-cad swiper-slide" ng-if="data.translatecontent && data.detaildata[0].translationneed == 'Y'" -->
                <!-- 20170406 번역여부와 관계없이 스크리닝 텍스트가 있으면 표시-->
                <div class="preview-cad swiper-slide" ng-if="data.translatecontent">
                    <!-- 편지 상태 -->
                    
                    <span ng-if="data.basedata[0].direction == 'send'" style="float: right;color: red;">{{data.basedata[0].state}}</span>
                    <br />
					<!--span ng-if="data.translatecontent && data.detaildata[0].translationneed == 'Y'" ng-bind-html="data.translatecontent"></span-->
                    <style>
                        .screeningname { text-align:right; margin-top:30px;} 
                    </style>
                    <span ng-if="data.translatecontent" ng-bind-html="data.translatecontent"></span>
					<br /><br />
					<div class="line1"></div>
					<div class="line2"></div>
				</div>

				<!--보낸 내용  텍스트  -->
				<div class="preview-cad swiper-slide" ng-if="data.sendcontent">
                    <!-- 편지 상태 -->
                    <span class="txt_status mr10" >
                        <span ng-if="data.basedata[0].direction == 'send'" style="float: right;color: red;">{{data.basedata[0].state}}</span>
                    <br />
					<span ng-bind-html="data.sendcontent"></span>
					<!-- 고정 영역 -->
					<br /><br />
					<div class="line1"></div>
					<div class="line2"></div>
					<!--// 고정 영역 -->
				</div>

				<!-- 번역된 내용 -->

				<!-- 첨부 이미지-->
				<div class="wrap-postbox mt20 swiper-slide" ng-if="data.attachment">
					<img ng-src="{{data.attachment}}" alt="" width="100%" />      <!--하단 레이어팝업 명시(display 풀고 사용바랍니다)-->
				</div>
				
			</!--div>
		</div>
		<!--// 편지지 -->
		
	
		<!--보낸 편지의 경우, [답장하기], [다운로드], [공유] 버튼 노출 안 됨-->
		<div class="wrap-bt"  ng-show="data.basedata[0].direction == 'receive'">
    		<a class="bt-type6" style="width:100%" ng-click="goReply(data.basedata[0])">답장하기</a>
			<a href="#" runat="server" id="btnList" style="display:none"></a>
			<a class="bt-type5 mt10" style="width:100%" ng-click="download($event)" ng-if="images.length > 0">다운로드</a>
		</div>
    
    
    
  
		  
		<!--floating-->
		<div class="floating-letterwrite">
    		<span class="fix_btn btn" ng-click="floating.show($event)"><img src="/common/img/icon/pencil.png" alt=""/></span>     <!--클릭 후  "/common/img/icon/close4.png"-->
			<div class="sub-floating">    <!--클릭 후  display:block-->
				<a class="ic1" href="/my/letter/temp" ng-show="floating.temp_count > 0">임시 저장 편지 <em>{{floating.temp_count}}</em>개</a>
				<a class="ic2" href="/my/letter/write">편지 쓰기</a>
			</div>
		</div>
    
    
   
	</div>
	<!--// 컨텐츠 영역 -->



	
</asp:Content>
