﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="write-gift.aspx.cs" Inherits="my_letter_write_gift" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    <script type="text/javascript" src="/my/letter/write-gift.js?v=1.5"></script>
    <script src="/common/js/util/swiper.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" runat="server" id="upload_root" value="" />
    <input type="hidden" runat="server" id="domain_image" value="" />
    <input type="hidden" id="file_path" value="" />
    <input type="hidden" id="file_size" value="" />
    <input type="hidden" id="giftLetterUseYN" runat="server" value="" />
    <input type="hidden" id="hd_user_id" runat="server" />
    <!-- 컨텐츠 영역 -->
    <div class="appContent" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <!-- navigation 를 위한 hidden 값 -->
        <a id="btnList" href="#" runat="server"></a>
        <div class="menu-sympathy appContent-margin1" id="slide">

            <ul style="position: absolute; left: 5px; top: 0; width: 100%" id="slide_menu">
                <!--임시 inline style(수정가능)-->
                <li style="float: left; display: block; width: 80px"><a href="/my/letter/write/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">편지</a></li>
                <li style="float: left; display: block; width: 75px"><a href="/my/letter/write-pic/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">사진 편지</a></li>
                <li style="float: left; display: block; width: 75px"><a class="selected" href="/my/letter/write-pic/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">선물 편지</a></li>
                <li style="float: left; display: block; width: 75px"><a href="/my/letter/temp/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">임시 저장</a></li>
                <li style="float: left; display: block; width: 90px"><a href="/my/letter/ready/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">발송 준비 중</a></li>
                <li style="float: left; display: block;"></li>
            </ul>

        </div>

        <div class="member-join">

            <p class="txt-title">나의 어린이</p>
            <p class="app-sel-child" ng-click="showChildren($event);">{{selectedChildren()}}</p>
            <!--<div class="align-right mt10"><a class="link-child" href="#" ng-click="showChildren($event);">여러 명의 어린이에게 보내실 건가요?</a></div>-->
            <div class="linebar"></div>

            <p class="txt-title mt20">
                선물선택<span class="txt-commt">*보내실 선물을 선택해 주세요.</span>
            </p>
            <asp:Repeater runat=server ID=repeater OnItemDataBound=ListBound>
				<ItemTemplate>
					<div style="width: 190px; height:250px; display:inline-block; margin:2px;">
                        <div style="margin-bottom:5px; text-overflow:ellipsis;">
                            <input type="radio" name="giftSeq" id="rd<%#Eval("SEQ") %>" value="<%#Eval("SEQ") %>" class="css_radio" style="vertical-align:baseline;" data-value="<%#Eval("IsStoreItem") %>" data-text="<%#Eval("Title") %>" ng-click="checkChildSponsorType('<%#Eval("IsStoreItem") %>')" data-price="<%#Eval("price") %>" data-option='<%#Eval("option") %>' data-hd_inventory="<%#Eval("hd_inventory") %>" data-idx="<%#Eval("idx") %>" />
							<label for="rd<%#Eval("SEQ") %>"><div title="<%#Eval("Title") %>" style="white-space: nowrap; text-overflow: ellipsis; width:173px; max-width: 173px; overflow: hidden; display: inline-block;"><%#Eval("Title") %></div></label>
                        </div>
                        <asp:Image ID="imgGift" runat="server" style="width:auto; height:230px;" oncontextmenu="return false;" />
					</div>
				</ItemTemplate>
				<FooterTemplate>
					<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
						<td colspan="5">데이터가 없습니다.</td>
					</tr>
				</FooterTemplate>

			</asp:Repeater>	

            <div class="linebar"></div>

            <div class="wrap-bt mb20">
                <a class="bt-type6 fr" ng-click="submit($event,'N')" style="width: 49%" href="#">편지 보내기</a>
            </div>

        </div>


    </div>
    <!--// 컨텐츠 영역 -->

</asp:Content>

