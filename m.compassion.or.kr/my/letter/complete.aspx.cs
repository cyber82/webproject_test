﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_letter_complete : MobileFrontBasePage {


	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var ids = Request["ids"].Split(',');
		var childMasterId = ids[0];

		var actionResult = new ChildAction().GetChild(childMasterId);
		if(actionResult.success) {
			var data = (ChildAction.ChildItem)actionResult.data;

			//pic.Src = data.Pic;
			pic.Style.Add( "background", "url('"+ data.Pic +"') center top no-repeat;" );
		}


	}
	
	

}