﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_letter_temp : MobileFrontBasePage
{

    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    public string r = "";
    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();

        r = Request["r"].EmptyIfNull();
        if (!string.IsNullOrEmpty(r))
            btnList.HRef = r;


        // 선물편지사용여부
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
        Object[] objSql = new object[1] { string.Format("select MEM_NAME_LANG0 from ETS_TC_CODE_MEMBER where Dim_Code = 'GIFT_LETTER' and MEM_Code = 'USE'") };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);
        if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
        {
            giftLetterUseYN.Value = "N";
        }
        else
        {
            giftLetterUseYN.Value = ds.Tables[0].Rows[0][0].ToString().ValueIfNull("");
        }
        if (giftLetterUseYN.Value == "N")
        {
            //base.AlertWithJavascript("선물편지관리에서 사용하지 않도록 설정되어있습니다.", "location.href='/my/letter'");
        }


    }

}