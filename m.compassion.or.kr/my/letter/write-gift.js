﻿$(function () {
    $("#btn_file_remove").hide();
    /*
    {
        var uploader = attachUploader("btn_file_path");
        uploader._settings.data.fileDir = $("#upload_root").val();
        uploader._settings.data.fileType = "image";
        uploader._settings.data.rename = "y";
        uploader._settings.data.limit = 2048;

        setTimeout(function () {
            //$("input[name=userfile]").css("display", "block").css("top", "380px").css("left", "190px").width("384px").height("139px");
            $("input[name=userfile]").css("display", "block");
        }, 300);
    }
	*/



    var getAndroidVersion = function (ua) {
        ua = (ua || navigator.userAgent).toLowerCase();
        var match = ua.match(/android\s([0-9\.]*)/);
        return match ? match[1] : false;
    }

    var hidden_photo_btn = $(".hidden_photo_btn");

    //var entity = $.parseJSON(cookie.get("cps.app"));
    //var appDevice = entity.device;

    var entity = $.parseJSON(cookie.get("cps.app"));
    var appDevice = null
    if (entity != null) {
        appDevice = entity.device;
    }

    var version = getAndroidVersion();
    hidden_photo_btn.click(function () {
        if (appDevice == "android" && parseFloat(version) == 4.4) {
            JSInterface.takePhoto("takePictureField", "letterwritepic", $("#upload_root").val());
            return false;
        }
    });

    hidden_photo_btn.change(function () {
        if (this.files && this.files[0]) {
            if (this.files[0].size > 5242880) {
                setTimeout(function () {
                    alert("5MB 이하의 사진만 올릴 수 있습니다.")
                }, 500)
                return false;
            }
            var fileSize = this.files[0].size;
            var FR = new FileReader();
            FR.onload = function (e) {
                hybrid_api.submitUserImage($("#upload_root").val(), e.target.result, function (r) {
                    //console.log(r);
                    if (r.success) {
                        var img = r.data;
                        $("#btn_file_remove").show();
                        alert("첨부 파일에 내용이 있는 경우 번역이 되지 않습니다");
                        $("#file_path").val(img);
                        $("#file_size").val(fileSize);
                        $(".attach_img").attr("src", ($("#domain_image").val() + img)).show();
                        $(".txt-noimg").hide();
                    }
                });
            };
            FR.readAsDataURL(this.files[0]);
        }
    });

    $("#lang_ko").click(function () {
        alert("한글으로는 번역 과정을 거치기 때문에,\n\r영어로 사용하시면 편지가 더 빨리 전달됩니다.");

    })

    if ($("#file_path").val() != "") {
        $("#btn_file_remove").show();
        $(".txt-noimg").hide();
    }

    $("#btn_file_remove").click(function () {

        $.post("/api/my/letter.ashx", { t: "file-delete", file_path: $("#file_path").val() }, function (r) {
            if (r.success) {

                $("#file_path").val("");

                $("#btn_file_remove").hide();
                $(".attach_img").hide();
                $(".txt-noimg").show();

            } else {
                alert(r.message)
            }
        })

        return false;
    })

    //$("#letterComment").textCount($("#comment_count"), { limit: 100 });

    $("#letterComment").keyup(function () {
        if ($("#comment_count").text() >= 100) {
            alert("100자 까지 입력할 수 있습니다.");
        }
    });

    $(".lang").click(function () {
        $(".lang").removeClass("selected");
        $(this).addClass("selected");
    })

    var sly = new Sly($("#slide"), {    // Call Sly on frame

        horizontal: 1,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 0,
        scrollBy: 1,
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1

    });

    sly.init();
    $.each($("#slide a"), function (i) {
        if ($(this).hasClass("selected")) {
            sly.activate(i);
        }
    })

    setTimeout(function () {
        if ($("#slide_menu").width() < $("#slide").width()) {
            $("#slide_menu").css("left", "5%");
        }

    }, 10);

});

(function () {

    var app = angular.module('cps.page', []);
    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, utils, paramService) {

        $scope.children = null;		// pop-children.html 에서 세팅한 어린이데이타
        $scope.selectedChildren = function () {
            if ($scope.children) {

                var result = $scope.children[0].name + ' (' + $scope.children[0].personalnameeng + ')';
                if ($scope.children.length > 1) {
                    result += " 외" + ($scope.children.length - 1);
                }

                return result;
            }
            return "어린이를 선택해주세요";
        };


        var c = getParameterByName("c");
        if (c) {
            var childMasterId = c.split('|')[0];
            var childKey = c.split('|')[1];
            var childName = c.split('|')[2];
            var personalnameeng = c.split('|')[3];

            $scope.children = [{
                childmasterid: childMasterId, childkey: childKey, name: childName, personalnameeng: personalnameeng
            }];

        }

        $scope.submit = function ($event, status) {
            $event.preventDefault();
            var children = [];
            //var checkChildKey = true;
            if ($scope.children) {
                $.each($scope.children, function () {
                    //if (typeof this.childkey == "undefined" || this.childkey == "undefined" || this.childkey == null || this.childkey == "") {
                    //    checkChildKey = false;
                    //}
                    children.push({ childMasterId: this.childmasterid, childKey: this.childkey });
                });
            }

            //if (!checkChildKey) {
            //    alert("어린이키에 문제가 있습니다. 관리자에게 문의해주세요.");
            //    return false;
            //}

            if (children.length < 1) {
                alert("편지를 보내실 어린이를 선택해주세요");
                return false;
            }

            if ($("input[name=giftSeq]:checked").length < 1) {
                alert("보내실 선물을 선택해주세요");
                return false;
            }

            var giftSeq = $("input[name=giftSeq]:checked").val();
            var giftIsStoreItem = $("input[name=giftSeq]:checked").attr('data-value');// == 'Y' ? true : false;
            var giftName = $("input[name=giftSeq]:checked").attr('data-text');

            //if (status == "N" && !confirm("[언어 : " + $(".lang.selected").data("text") + "] 로 발송 하시겠습니까?"))
            //    return false;

            if (giftIsStoreItem == 'Y') {
                $scope.modalChild2.reInit();
            }
            if (giftIsStoreItem == 'Y') {
                var isCORRES = false;
                $.each($scope.children, function () {
                    if (this.checked && this.sponsortypeeng == 'CORRES') {
                        isCORRES = true;
                        this.checked = false;
                    }
                });

                if (isCORRES) {
                    alert('유료선물의 경우는 편지결연 어린이는 선택하실 수 없습니다.');
                    return false;
                }
            }

            if (giftIsStoreItem == 'Y') {
                $scope.modalChild2.show($event);
            }
            else {

                $.post("/common/handler/uploadImage.ashx", {
                    giftSeq: giftSeq,
                    path: $('#upload_root').val()
                }, function (res) {
                    if (res.success) {
                        var param = {
                            t: "add",
                            samplelettercode: "",
                            file_path: res.name,
                            file_size: res.size,
                            lang: $("input[name=lang]:checked").val(),
                            status: status,
                            letter_comment: giftIsStoreItem == 'Y' ? giftName : '',
                            letter_type: giftIsStoreItem == 'Y' ? 'SPNPRE' : 'SPNLTR',
                            children: $.toJSON(children),
                            is_pic_letter: "Y",
                            giftSeq: giftSeq,
                            giftIsStoreItem: giftIsStoreItem
                        };

                        $http.post("/api/my/letter.ashx", param).success(function (r) {
                            if (r.success) {
                                alert("편지가 등록되었습니다. 등록된 편지는 당일 24시 전까지 수정/취소가 가능합니다.");
                                location.href = "/my/letter/?type=send";
                            } else {
                                alert(r.message);
                            }
                        });
                    }
                    else {
                        alert(res.msg);
                    }
                });

            }



        }

        // 어린이팝업
        $scope.showChildren = function ($event) {
            $event.preventDefault();

            popup.init($scope, "/my/letter/pop-children.html?v=2", function (modal) {

                modal.show();
                initPopChildren($http, $scope, modal, utils, paramService);

            }, { top: 0, iscroll: true, removeWhenClose: true });

        }

        $scope.checkChildSponsorType = function (isStoreItem) {
            if ($scope.children != null) {
                if (isStoreItem == 'Y') {
                    var isCORRES = false;
                    $.each($scope.children, function () {
                        if (this.checked && this.sponsortypeeng == 'CORRES') {
                            isCORRES = true;
                            this.checked = false;
                        }
                    });

                    if (isCORRES) {
                        alert('유료선물의 경우는 편지결연 어린이는 선택하실 수 없습니다.');
                    }
                }
            }

        }

        // 어린이선물
        $scope.modalChild2 = {

            total: 0,
            page: 1,
            rowsPerPage: 2,
            data: [],
            list: null,
            container: null,
            processing: false,
            total_ea: 0,
            total_amount: 0,
            child_count: 0,
            item_count: 0,
            title: "",
            image: "",
            instance: null,
            hd_inventory: '',
            price: 0,
            idx: 0,

            init: function () {
                // 팝업

                popup.init($scope, "/store/item-child-letter.html", function (modal) {
                    $scope.modalChild2.instance = modal;
                    $scope.modalChild2.container = $("#childWrapper");

                    var obj = $scope.modalChild2.container;
                    $scope.modalChild2.title = '';

                    $scope.modalChild2.image = '';

                    //var option = obj.find("[data-id=option]");
                    //option.empty();
                    //option.append($("#option > option").clone());
                    //option.change(function () {
                    //    $scope.modalChild2.calculate();
                    //})

                    //option.selectbox({

                    //    onOpen: function (inst) {
                    //    },

                    //    onChange: function (val, inst) {
                    //        $scope.modalChild2.calculate();
                    //    }
                    //});

                    $('.btn_ac .btn_type10.fl.mr10').css('display', 'none');

                }, { top: 0, iscroll: true });

            },

            reInit: function () {
                var obj = $scope.modalChild2.container;

                var giftSeq = $("input[name=giftSeq]:checked").val();
                var giftIsStoreItem = $("input[name=giftSeq]:checked").attr('data-value');// == 'Y' ? true : false;
                var giftName = $("input[name=giftSeq]:checked").attr('data-text');

                $scope.modalChild2.title = giftName;
                //$scope.modalChild2.image = $("input[name=giftSeq]:checked").parent().parent().find('img').attr("src");//$("#image").attr("src");

                $('.box-gift .photo img').attr('src', $("input[name=giftSeq]:checked").parent().parent().find('img').attr("src"));

                $scope.modalChild2.idx = $("input[name=giftSeq]:checked").attr('data-idx');

                var optionsData = $.parseJSON($("input[name=giftSeq]:checked").attr('data-option'));

                var option = obj.find("[data-id=option]");
                option.empty();
                //option.append($("#option > option").clone());
                $.each(optionsData, function () {
                    option.append("<option value='" + this.value + "' data-price='" + this.price + "'>" + this.text + "</option>");
                })

                option.change(function () {
                    $scope.modalChild2.calculate();
                })

                //option.selectbox('detach');

                //option.selectbox({

                //    onOpen: function (inst) {
                //    },

                //    onChange: function (val, inst) {
                //        $scope.modalChild2.calculate();
                //    }
                //});

                $scope.modalChild2.hd_inventory = $("input[name=giftSeq]:checked").attr('data-hd_inventory');
                $scope.modalChild2.price = $("input[name=giftSeq]:checked").attr('data-price');
            },

            show: function ($event) {
                $event.preventDefault();
                if (!common.checkLogin()) {
                    return false;
                }


                if (!$scope.modalChild2.instance)
                    return;


                $scope.modalChild2.getChildren();


            },

            hide: function ($event) {
                if ($event) $event.preventDefault();
                if (!$scope.modalChild2.instance)
                    return;
                $scope.modalChild2.instance.hide();

            },

            getChildren: function () {

                $http.get("/api/store.ashx?t=get_children", { params: {} }).success(function (r) {

                    if (r.success) {

                        $scope.modalChild2.data = $.extend($scope.modalChild2.data, r.data);

                        if (r.data.length < 1) {
                            alert("한국컴패션 후원자만 어린이에게 선물하실 수 있습니다.");
                            $scope.modalChild2.hide();
                            return;
                        }

                        if ($scope.modalChild2.EAInit() == false) {
                            alert('재고량이 부족 합니다.');
                            return;
                        }

                        $scope.modalChild2.instance.show();

                        $scope.modalChild2.total = r.data.length;

                        $scope.modalChild2.getList({ page: 1 });

                    } else {

                        if (r.action == "not_sponsor") {
                            alert("한국컴패션 후원자만 어린이에게 선물하실 수 있습니다.");
                        } else if (!r.data) {
                            alert("후원가능한 어린이가 없습니다.");
                            return;
                        } else {
                            alert(r.message);
                        }
                    }
                });

            },

            EAInit: function () {
                $scope.modalChild2.total_ea = 0;
                $.each($scope.modalChild2.data, function () {
                    this.birthdate = new Date(this.birthdate);
                    var childmasterid = this.childmasterid;
                    var isChecked = false;

                    //2018-05-02 이종진 - 선택한 어린이는 수량을 1로 셋팅해줌.
                    if ($scope.children) {
                        $.each($scope.children, function () {
                            if (this.childmasterid == childmasterid) {
                                isChecked = true;
                                $scope.modalChild2.total_ea++;
                            }
                        });
                    }
                    this.ea = isChecked ? 1 : 0;

                });

                var inventory = parseInt($scope.modalChild2.hd_inventory);
                var total = $scope.modalChild2.total_ea;
                if (total > inventory) {
                    return false;
                }

                var price = parseInt($scope.modalChild2.price);
                var ea = $scope.modalChild2.total_ea;
                $scope.modalChild2.total_amount = price * ea;
                var count = 0;
                var i = 0;
                $.each($scope.modalChild2.data, function () {
                    if (this.ea > 0) {
                        count++;
                        i++;
                    }
                });
                $scope.modalChild2.child_count = count;
                $scope.modalChild2.item_count = $scope.modalChild2.total_ea.format();

                return true;
            },

            getList: function (param) {
                $scope.modalChild2.page = param.page;
                var begin = ($scope.modalChild2.page - 1) * $scope.modalChild2.rowsPerPage;
                var end = ($scope.modalChild2.page) * $scope.modalChild2.rowsPerPage;

                $scope.modalChild2.list = $scope.modalChild2.data.slice(begin, end);
            },

            calculate: function () {

                // 선택된 상품수
                $scope.modalChild2.item_count = $scope.modalChild2.total_ea.format();

                var obj = $scope.modalChild2.container;
                var price = parseInt($scope.modalChild2.price);
                var ea = $scope.modalChild2.total_ea;
                var opt_price = obj.find("[data-id=option] option:selected").data("price") || 0;


                $scope.modalChild2.total_amount = ((opt_price + price) * ea);
                //console.log($scope.modalChild2.total_amount);

                // 선택된 어린이수
                /*
                var count = 0;
                $.each(obj.find("[data-id=ea]"), function () {
                    if ($(this).val() != "0") {
                        count++;
                    }
                })
                */
                var count = 0;
                $.each($scope.modalChild2.data, function () {
                    if (this.ea > 0) {
                        count++;
                    }
                });

                $scope.modalChild2.child_count = count;

            },

            setBasket: function (action) {
                var childrenLetter = [];
                if ($scope.children) {
                    $.each($scope.children, function () {
                        //if (this.checked) {
                        //if (typeof this.childkey == "undefined" || this.childkey == "undefined" || this.childkey == null || this.childkey == "") {
                        //    checkChildKey = false;
                        //}
                        childrenLetter.push({ childMasterId: this.childmasterid, childKey: this.childkey });
                        //}
                    });
                }

                var giftSeq = $("input[name=giftSeq]:checked").val();
                var giftIsStoreItem = $("input[name=giftSeq]:checked").attr('data-value');// == 'Y' ? true : false;
                var giftName = $("input[name=giftSeq]:checked").attr('data-text');

                $.post("/common/handler/uploadImage.ashx", {
                    giftSeq: giftSeq,
                    path: $('#upload_root').val()
                }, function (res) {
                    if (res.success) {
                        var param = {
                            t: "add",
                            samplelettercode: "",
                            file_path: res.name,
                            file_size: res.size,
                            lang: $("input[name=lang]:checked").val(),
                            //status: 'N',
                            //[jongjin.lee] 2018-04-30 #CO4-182 
                            //5. 유료선물편지의 경우, kr_compass4.tCorrespondenceWeb.CorrStatus를 최초 'D'로 넣고, 결제되면 'N'으로 변경함
                            status: 'D', 
                            letter_comment: giftIsStoreItem == 'Y' ? giftName : '',
                            letter_type: giftIsStoreItem == 'Y' ? 'SPNPRE' : 'SPNLTR',
                            children: $.toJSON(childrenLetter),
                            is_pic_letter: "Y",
                            giftSeq: giftSeq,
                            giftIsStoreItem: giftIsStoreItem
                        };
                        var repCorrespondenceId = '';
                        var correspondenceWebID = '';
                        $http.post("/api/my/letter.ashx", param).success(function (r) {
                            if (r.success) {
                                repCorrespondenceId = r.data;
                                correspondenceWebID = r.message;

                                var obj = $scope.modalChild2.container;

                                var json = {};
                                json.user_id = $("#hd_user_id").val();
                                json.item_id = $scope.modalChild2.idx;//$("#hd_item_id").val();
                                json.option_name = obj.find("[data-id=option] option:selected").text();
                                json.option = obj.find("[data-id=option]").val();
                                json.option_price = obj.find("[data-id=option] option:selected").data("price") || 0;
                                json.quantity = $scope.modalChild2.total_ea;
                                json.repCorrespondenceId = repCorrespondenceId;
                                json.CorrespondenceWebID = correspondenceWebID;

                                var children = [];

                                $.each($scope.modalChild2.data, function () {
                                    if (this.ea > 0) {
                                        var child = {};
                                        child.childId = this.childkey;
                                        child.childName = this.namekr;
                                        child.ea = this.ea;
                                        children.push(child);
                                    }
                                })

                                json.children = children;

                                var jsonStr = $.toJSON(json);

                                //	console.log(json);
                                //	return;
                                $http.post("/api/store.ashx", { t: "set_basket", data: jsonStr }).success(function (r) {

                                    $scope.modalChild2.processing = false;
                                    //	console.log(r);
                                    if (r.success) {
                                        //if (action == "cart") {
                                        //    if (!confirm('장바구니로 이동하시겠습니까?')) {
                                        //        $scope.modalChild2.hide();
                                        //        return;
                                        //    }
                                        //}

                                        //alert("편지가 등록되었습니다. 결제를 위해 장바구니로 이동하겠습니다.");
                                        alert('결제를 위해 장바구니로 이동하겠습니다.\r\n선물편지로 구매한 상품은 취소가 불가합니다.\r\n주문취소 관련 문의는 담당자(02 - 3668 - 3434)에게 연락 주시기 바랍니다.');

                                        location.href = '/store/cart';

                                    } else {
                                        alert(r.message);
                                    }

                                });

                            } else {
                                alert(r.message);
                            }
                        });
                    }
                    else {
                        alert(res.msg);
                    }
                });

                return;
            },

            buy: function (action) {

                if ($scope.modalChild2.processing) return false;

                var index = $('[data-id=option] option').index($('[data-id=option] option:selected'));
                if ($('[data-id=option] option').size() > 1 && index < 1) {
                    alert('옵션을 선택해주세요');
                    $("[data-id=option]").focus();
                    return false;
                }

                if ($scope.modalChild2.total_ea < 1) {
                    alert("상품갯수를 선택해주세요");
                    return false;
                }

                if ($scope.modalChild2.child_count > 50) {
                    alert("한번에 50명 이하로만 가능합니다.");
                    return false;
                }

                //	$scope.modalChild2.processing = true;
                $scope.modalChild2.setBasket(action);

            },

            plusEA: function ($event, item) {
                $event.preventDefault();

                var obj = $scope.modalChild2.container;

                var group = item.childkey;
                var inventory = parseInt($scope.modalChild2.hd_inventory);
                var total = $scope.modalChild2.total_ea + 1;
                var ea = parseInt(obj.find("[data-id=ea][data-group='" + group + "']").val()) + 1;
                item.ea = ea;
                if (total > inventory) {
                    alert('재고량이 부족 합니다.');
                    return false;
                }

                $scope.modalChild2.total_ea = total;
                obj.find("[data-id=ea][data-group='" + group + "']").val(ea);
                $scope.modalChild2.calculate();
            },

            minusEA: function ($event, item) {
                $event.preventDefault();

                var obj = $scope.modalChild2.container;
                var group = item.childkey;
                var total = $scope.modalChild2.total_ea - 1;
                var ea = parseInt(obj.find("[data-id=ea][data-group='" + group + "']").val()) - 1;

                if (ea < 1) {
                    alert('수량은 1개 이상만 가능합니다.');
                    return;
                }

                item.ea = ea;
                $scope.modalChild2.total_ea = total;
                obj.find("[data-id=ea][data-group='" + group + "']").val(ea);
                $scope.modalChild2.calculate();

            }



        }
        $scope.modalChild2.init();
    });

})();

//var attachUploader = function (button) {

//    return new AjaxUpload(button, {
//        action: '/common/handler/upload',
//        responseType: 'json',
//        onChange: function () {
//        },
//        onSubmit: function (file, ext) {
//            this.disable();
//        },
//        onComplete: function (file, response) {

//            this.enable();

//            if (response.success) {

//                $("#btn_file_remove").show();
//                alert("첨부 파일에 내용이 있는 경우 번역이 되지 않습니다");

//                $("#file_path").val(response.name);
//                //$("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
//                $("#file_size").val(response.size);

//                $(".attach_img").attr("src", ($("#domain_image").val() + response.name)).show();
//                $(".txt-noimg").hide();

//            } else
//                alert(response.msg);
//        }
//    });
//}