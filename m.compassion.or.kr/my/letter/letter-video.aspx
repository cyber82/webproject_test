﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="letter-video.aspx.cs" Inherits="my_letter_video" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	

<div class="appContent">
    
    <div class="youtube">
        <iframe src="https://www.youtube.com/embed/hgVb-T5KPSQ" frameborder="0" allowfullscreen></iframe>
    </div>

    
    <!--floating-->
    <div class="floating-letterwrite">
    	<span class="btn" onClick=""><img src="/common/img/icon/pencil.png" alt="" /></span>     <!--클릭 후  "/common/img/icon/close4.png"-->
        <div class="sub-floating">    <!--클릭 후  display:block-->
			<a class="ic1" href="#">임시 저장 편지 <em>2</em>개</a>
            <a class="ic2" href="#">편지 쓰기</a>
        </div>
    </div>
    
   
    
</div>
<!--// 컨텐츠 영역 -->

</asp:Content>

