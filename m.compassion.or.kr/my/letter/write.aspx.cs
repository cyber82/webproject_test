﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_letter_write : MobileFrontBasePage
{

    //CommonLib.WWW6Service.SoaHelperSoap _www6Service;

    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    public string r = "";
    public string c = "";
    public DataSet dt = null;

    protected override void Page_Load(object sender, EventArgs e)
    {
        c = Request["c"].EmptyIfNull();
        r = Request["r"].EmptyIfNull();

        var childInfo = c.Split('|');
        /*
        if (childInfo == null || childInfo.Length < 2)
        {
            if (!string.IsNullOrEmpty(r))
            {
                base.AlertWithJavascript("어린이 정보가 없습니다.", "location.href='"+ r + "';");
            }
            else
            {
                base.AlertWithJavascript("어린이 정보가 없습니다.", "goBack();");
            }
                
            return;
        }
        */

        if (childInfo != null && childInfo.Length > 1)
        {
            try
            {
                var childAction = new ChildAction();
                var actionResult = childAction.MyAllChildren(childInfo[1], 1, 1, "name", true);
                if (actionResult.success)
                {
                    var myChild = (List<ChildAction.MyChildItem>)actionResult.data;
                    var isSupported = myChild[0].isSupported;
                    if (isSupported == "N")
                    {
                        base.AlertWithJavascript("후원중인 어린이가 아닙니다.", "goBack();");

                    }

                }
                /*
                        var sess = new UserInfo();
                Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId", "childKey", "orderby", "checkLetter" };
                Object[] objValue = new object[] { 1, 1, sess.SponsorID, childInfo[1], "", "1" };
                Object[] objSql = new object[] { "sp_web_myAllchild_list_f" };

            
                dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
            
                if (dt.Tables[0].Rows.Count > 0)
                {
                    string isSupported = dt.Tables[0].Rows[0]["SUPPORTED_YN"].ToString().Trim();

                    ErrorLog.Write(HttpContext.Current, 0, "편지쓰기 어린이 정보 " + dt);
                    ErrorLog.Write(HttpContext.Current, 0, "편지쓰기 어린이 정보 " + isSupported);


                    if (!string.IsNullOrEmpty(isSupported))
                    {
                        base.AlertWithJavascript("후원중인 어린이가 아닙니다.", "goBack();");

                    }
                }
                */
            }
            catch (Exception ex)
            {
                ErrorLog.Write(HttpContext.Current, 0, "Page_Load :::: " + ex.Message);
            }
        }
        this.OnBeforePostBack();
    }

    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();

        r = Request["r"].EmptyIfNull();
        if (!string.IsNullOrEmpty(r))
            btnList.HRef = r;

        domain_image.Value = ConfigurationManager.AppSettings["domain_file"];
        upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_letter);

        var types = new LetterAction().GetSampleLetterTypes();
        if (!types.success)
        {
            base.AlertWithJavascript(types.message, "goBack();");
            return;
        }



        repeater_types.DataSource = types.data;
        repeater_types.DataBind();

        // 선물편지사용여부
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
        Object[] objSql = new object[1] { string.Format("select MEM_NAME_LANG0 from ETS_TC_CODE_MEMBER where Dim_Code = 'GIFT_LETTER' and MEM_Code = 'USE'") };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);
        if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
        {
            giftLetterUseYN.Value = "N";
        }
        else
        {
            giftLetterUseYN.Value = ds.Tables[0].Rows[0][0].ToString().ValueIfNull("");
        }
    }

    public class Item
    {
        string isSupported { get; set; }
    }

}