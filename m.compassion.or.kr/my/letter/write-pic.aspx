﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="write-pic.aspx.cs" Inherits="my_letter_write_pic" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    <script type="text/javascript" src="/my/letter/write-pic.js?v=1.9"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" runat="server" id="upload_root" value="" />
    <input type="hidden" runat="server" id="domain_image" value="" />
    <input type="hidden" id="file_path" value="" />
    <input type="hidden" id="file_size" value="" />
    <input type="hidden" id="giftLetterUseYN" runat="server" value="" />

    <!-- 컨텐츠 영역 -->
    <div class="appContent" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <!-- navigation 를 위한 hidden 값 -->
        <a id="btnList" href="#" runat="server"></a>
        <div class="menu-sympathy appContent-margin1" id="slide">

            <ul style="position: absolute; left: 5px; top: 0; width: 100%" id="slide_menu">
                <!--임시 inline style(수정가능)-->
                <% if (giftLetterUseYN.Value == "N") { %>
                <li style="float: left; display: block; width: 95px"><a href="/my/letter/write/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">글 편지</a></li>
                <li style="float: left; display: block; width: 90px"><a class="selected" href="/my/letter/write-pic/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">사진 편지</a></li>
                <li style="float: left; display: block; width: 90px"><a href="/my/letter/temp/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">임시 저장</a></li>
                <li style="float: left; display: block; width: 90px"><a href="/my/letter/ready/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">발송 준비 중</a></li>
                <li style="float: left; display: block;"></li>
                <% } else { %>
                <li style="float: left; display: block; width: 80px"><a href="/my/letter/write/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">편지</a></li>
                <li style="float: left; display: block; width: 75px"><a class="selected" href="/my/letter/write-pic/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">사진 편지</a></li>
                <li style="float: left; display: block; width: 75px"><a href="/my/letter/write-gift/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">선물 편지</a></li>
                <li style="float: left; display: block; width: 75px"><a href="/my/letter/temp/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">임시 저장</a></li>
                <li style="float: left; display: block; width: 90px"><a href="/my/letter/ready/?c=<%:Request["c"].EmptyIfNull() %>&r=<%:HttpUtility.UrlEncode(r) %>">발송 준비 중</a></li>
                <li style="float: left; display: block;"></li>
                <% } %>
            </ul>

        </div>

        <div class="member-join">

            <p class="txt-title">나의 어린이</p>
            <p class="app-sel-child" ng-click="showChildren($event);">{{selectedChildren()}}</p>
            <!--<div class="align-right mt10"><a class="link-child" href="#" ng-click="showChildren($event);">여러 명의 어린이에게 보내실 건가요?</a></div>-->
            <div class="linebar"></div>

            <p class="txt-title mt20">언어 선택<span class="txt-commt">*사용하실 언어를 선택해 주세요</span></p>
            <div class="tab-column-2 mt15">
                <a class="lang" href="#" id="lang_ko" data-val="ko" data-text="한글">한글</a>
                <a class="lang" href="#" id="lang_en" data-val="en" data-text="영어">영어</a>
            </div>

            <!--사진첨부 오른쪽 버튼   삭제,추가 두가지 경우  <img src="/common/img/icon/delete2.png" alt="삭제" />-->
            <p class="txt-title mt20">
                사진 첨부
				<a id="btn_file_remove" class="action" href="#">
                    <img src="/common/img/icon/delete2.png" alt="삭제" /></a>
            </p>
            <!--이미지 등록 전-->
            <input type="file" accept="image/*" class="hidden_photo_btn" style="width:280px;height:150px;top:365px;left:5%;position:absolute;opacity:0.0;background:red;z-index:10">
            <div class="reg-letterimg" id="btn_file_path">
                <p class="txt-noimg">이미지를 등록해 주세요.</p>
            </div>

            <!--이미지 등록 후-->
            <div class="reg-letterimg">
                <img class="attach_img" alt="" width="100%" />
            </div>


            <fieldset class="frm-input mt20">
                <legend>메시지 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <div class="frm-row box mt0">
                            <textarea name="letterComment" id="letterComment" ng-model="selectContent" style="width: 100%; height: 100px" placeholder="사진과 함께 보낼 짧은 메시지를 입력해 주세요 (선택)"></textarea>
                            <p class="txt-byte"><em id="comment_count">0</em>/50</p>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="linebar"></div>

            <div class="wrap-bt mb20">
                <a class="bt-type7 fl" ng-click="submit($event,'T')" style="width: 49%" href="#">임시 저장</a>
                <a class="bt-type6 fr" ng-click="submit($event,'N')" style="width: 49%" href="#">편지 보내기</a>
            </div>

        </div>


    </div>
    <!--// 컨텐츠 영역 -->

</asp:Content>

