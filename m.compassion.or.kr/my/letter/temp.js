﻿$(function () {
    var sly = new Sly($("#slide"), {    // Call Sly on frame

        horizontal: 1,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 0,
        scrollBy: 1,
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1

    });

    sly.init();
    $.each($("#slide a"), function (i) {
        if ($(this).hasClass("selected")) {
            sly.activate(i);
        }
    })

    setTimeout(function () {
        if ($("#slide_menu").width() < $("#slide").width()) {
            $("#slide_menu").css("left", "5%");
        }

    }, 10);

});

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

		$scope.total = -1;
		$scope.list = [];
		$scope.total_ready = 0;
		$scope.total_temp = 0;
		

		var rowsPerPage = 5;
		$scope.params = {
			page: 1,
			rowsPerPage: rowsPerPage,
			type: "send_temp"
			
		};

		// 뷰에서 돌아올때 넘겨준 파람을 다시 넘겨받는다.
		//$scope.params = $.extend($scope.params, paramService.getParameterValues());

		// 레터내역
		$scope.getList = function (params, cb) {

			$scope.params = $.extend($scope.params, params);
			console.log($scope.params);
			$http.get("/api/my/letter.ashx?t=list", { params: $scope.params }).success(function (r) {
				console.log(r);
				if (r.success) {

					var list = r.data.list;
					console.log(r.data);
					// 전체편지 1페이지인경우 타입별 갯수 가져온다.
					// 전체편지에서 임시저장데이타를 제외할경우 임시저장 데이타는 갯수조회가 안된다. 
					if ($scope.params.page == 1 && list.length > 0) {
						$scope.total_ready = parseInt(r.data.total_ready);
						$scope.total_temp = parseInt(r.data.total_temp);

					};

					$.each(list, function () {
						this.display_date = new Date(this.display_date.replace(/-/g, '/'));
						this.content = this.content.replace(/<br\/>/g, ' ');
					});

					$scope.list = list;
					$scope.total = list.length > 0 ? list[0].total : 0;
					
				} else {

					$scope.total = 0;
					if (r.action == "not_sponsor") {


					}
				}
			});

		}


		$scope.cancel = function ($event, correspondenceId) {
			$event.preventDefault();
			if (!confirm("편지를 삭제하시겠습니까?")) return;

			$http.post("/api/my/letter.ashx?t=cancel", { c: correspondenceId }).success(function (r) {

				alert(r.message);

				if (r.success) {
					$scope.list = [];
					$scope.getList({ page: 1 });

				}
			});

			return false;
		}

	

		$scope.goUpdate = function ($event, item) {
			$event.preventDefault();

			$scope.params.r = decodeURIComponent(getParameterByName("r"));
		
			if (item.is_pic_letter == "Y")
				location.href = "/my/letter/update-pic/" + item.correspondenceid + "?" + $.param($scope.params);
			else
				location.href = "/my/letter/update/" + item.correspondenceid + "?" + $.param($scope.params);
			return false;
		}


		// 뷰에서 돌아왔을때 처리
		if ($scope.params.page > 1) {
			var page = $scope.params.page;
			$scope.params.rowsPerPage = rowsPerPage * page;
			$scope.params.page = 1;
			$scope.getList({}, function () {
				$scope.params.rowsPerPage = rowsPerPage;
				$scope.params.page = parseInt(page);
			});

		} else {
			$scope.getList({ page: 1 });
		}


	});

})();