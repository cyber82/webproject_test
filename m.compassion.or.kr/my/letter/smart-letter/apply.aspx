﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="apply.aspx.cs" Inherits="my_letter_smart_letter_apply" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript">
		$(function () {
			/*
			$("input:checkbox").click(function () {
				if ($(this).prop("checked")) {
					$("input:checkbox").prop("checked", false);
					$(this).prop("checked", true);
				}
			})
			*/


		});

		function Validate5() {
			return confirm("스마트레터 서비스를 신청하시겠습니까?");
		}

		function Validate6() {
			if (!document.getElementById("select2").checked && !document.getElementById("select3").checked) {
				alert('신청하실 서비스를 선택해 주세요.');
				return false;
			}

			return confirm("스마트레터 서비스를 신청하시겠습니까?");
		}

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	
	<!-- 컨텐츠 영역 -->
	<div class="appContent">
    
		<div class="wrap-tab3 sectionsub-margin3">
			<a style="width:50%" href="/my/letter/smart-letter/introduce">소개</a>
			<a style="width:50%" class="selected" href="/my/letter/smart-letter/apply">신청</a>
		</div>


        <!--신청 alert 확인 후 or 이미 서비스 사용 중인 사용자에게 노출되는 안내 메시지와 버튼-->
		<div id="exist" runat="server">
            <ul class="list-checkboxchild">
				<li visible="false" id="exist6_letter" runat="server">
					<span class="field">
						<span class="field-th">
							<span class="checkbox3-ui">
                                <input type="checkbox" name="select" class="css-checkbox3" id="cb_MultiUseLetter" runat="server" />
                                <label for="cb_MultiUseLetter" class="css-label3">&nbsp;</label>
                            </span>
                        </span>
                        <span class="field-td">
							<span class="txt-name">편지 서비스</span>
							<span class="txt-info">
								<label for="cb_MultiUseLetter" class="css_label">후원자님께서는 현재 스마트레터 편지 서비스를 이용중입니.</label>
							</span>
						</span>
                    </span>
                </li>

                <li visible="false" id="exist6_card" runat="server">
					<span class="field">
						<span class="field-th">
							<span class="checkbox3-ui">
                                <input type="checkbox" name="select" class="css-checkbox3" id="cb_MultiUseCard" runat="server" />
                                <label for="cb_MultiUseCard" class="css-label3">&nbsp;</label>
                            </span>
                        </span>
                        <span class="field-td">
							<span class="txt-name">카드 서비스</span>
							<span class="txt-info">
								<label for="cb_MultiUseCard" class="css_label">후원자님께서는 현재 스마트레터 카드 서비스를 이용중입니.</label>
							</span>
						</span>
                    </span>
                </li>

                <li visible="false" id="exist5_card" runat="server">
					<span class="field">
						<span class="field-th">
							<span class="checkbox3-ui">
                                <input type="checkbox" name="select" class="css-checkbox3" id="cb_SingleUseCard" runat="server" />
                                <label for="cb_SingleUseCard" class="css-label3">&nbsp;</label>
                            </span>
                        </span>
                        <span class="field-td">
							<span class="txt-name">카드 서비스</span>
							<span class="txt-info">
								<label for="cb_SingleUseCard" class="css_label">후원자님께서는 현재 스마트레터 카드 서비스를 이용중입니.</label>
							</span>
						</span>
                    </span>
                </li>
            </ul>

            <div class="wrap-bt mb30">
				<asp:LinkButton runat="server" cssclass="bt-type4" style="width:100%" ID="btn_cancel" OnClick="btn_cancel_Click">스마트레터 서비스 취소하기</asp:LinkButton>
			</div>
            
		</div>
		<!--//-->

		
		<div id="submit5" runat="server">
			<ul class="list-checkboxchild">
				<li>
					<span class="field">
						<span class="field-th">
							<span class="checkbox3-ui">
								<input type="checkbox" id="card_1" name="select" class="css-checkbox3" checked />
								<label for="smart-service1" class="css-label3">&nbsp;</label>
							</span>
						</span>
						<span class="field-td">
							<span class="txt-name">카드 서비스</span>
							<span class="txt-info">
								<span class="color1">매년 크리스마스 카드와 어린이날 축하 카드를 어린이에게 대신 보내드려요.</span>
							</span>
						</span>
					</span>
				</li>
			</ul>
    
			<!--스마트레터 서비스를 신청하지 않은 사용자에게만 노출.-->
			<div class="wrap-bt mb30">
				<asp:LinkButton runat="server" CssClass="bt-type6" style="width:100%" ID="btnSave5" runat="server" OnClientClick="return Validate5()"  onclick="btnSave5_Click">스마트레터 서비스 신청하기</asp:LinkButton>
			</div>
			<!--//-->
		</div>


		<div id="submit6" runat="server">
			
			<ul class="list-checkboxchild">
				<li runat="server" id="submit6_card">
					<span class="field">
						<span class="field-th">
							<span class="checkbox3-ui">
								<input type="checkbox" name="select" id="select3" runat="server" class="css-checkbox3"  />
								<label for="select3" class="css-label3">&nbsp;</label>
							</span>
						</span>
						<span class="field-td">
							<span class="txt-name">카드 서비스</span>
							<span class="txt-info">
								<span class="color1">매년 생일 카드, 크리스마스 카드, 어린이날 카드를 어린이에게 대신 보내드려요.</span>
							</span>
						</span>
					</span>
				</li>
				<li runat="server" id="submit6_letter">
					<span class="field">
						<span class="field-th">
							<span class="checkbox3-ui">
								<input type="checkbox" name="select" id="select2" runat="server" class="css-checkbox3" />
								<label for="select2" class="css-label3">&nbsp;</label>
							</span>
						</span>
						<span class="field-td">
							<span class="txt-name">편지 서비스</span>
							<span class="txt-info">
								<span class="color1">일년에 두 번, 후원자님을 대신해 사랑이 듬뿍 담긴 정성스러운 편지를 어린이들에게 전해드려요.</span>
							</span>
						</span>
					</span>
				</li>
			</ul>
    
			<!--스마트레터 서비스를 신청하지 않은 사용자에게만 노출.-->
			<div class="wrap-bt mb30">					
				<asp:LinkButton runat="server" CssClass="bt-type6" ID="btnSave6" runat="server" style="width:100%" OnClientClick="return Validate6()"  onclick="btnSave6_Click">스마트레터 서비스 신청하기</asp:LinkButton>
			</div>
			<!--//-->
    
		</div>

	</div>
	<!--// 컨텐츠 영역 -->

</asp:Content>
