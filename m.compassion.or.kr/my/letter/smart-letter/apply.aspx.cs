﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;

public partial class my_letter_smart_letter_apply : MobileFrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}

	WWWService.Service _wwwService = new WWWService.Service();
	WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

	protected override void OnBeforePostBack() {

		UserInfo sess = new UserInfo();

		#region 다카스 검증
		string sSponsorID = sess.SponsorID.ToString();

		//Sponsor Counting
		int nSponCount = 0;
		bool isSinchYn = false;
        bool isSinchYn_C = false;
		string sLetterType = "";
		string sCardType = "";
		DataSet dsSponCount = _wwwService.CheckedSponCount( sSponsorID );

		//수정 20120327
		if (dsSponCount.Tables[0].Rows.Count > 0) {
			nSponCount = int.Parse( dsSponCount.Tables[0].Rows[0]["ChildCount"].ToString() );
		} else {
			nSponCount = 0;
		}

		//다카스 사용여부
		DataSet dsSponList = _wwwService.getCopyServiceList( sSponsorID );

		if (dsSponList.Tables[0].Rows.Count > 0) {
			//다카스 신청여부
            isSinchYn = true;
            isSinchYn_C = true;

			//편지타입
			sLetterType = dsSponList.Tables[0].Rows[0]["LetterRequestType"].ToString();

			switch (sLetterType) {
				case "GN":
				sLetterType = "일반";
				break;
				case "CP":
				sLetterType = "카피";
				break;
				case "WS":
				sLetterType = "대필";
				break;
				case "CC":
				sLetterType = "크카";
				break;
			}


			//카드타입
			sCardType = dsSponList.Tables[0].Rows[0]["CardRequestType"].ToString();

			switch (sCardType) {
				case "GN":
				sCardType = "일반";
				break;
				case "CP":
				sCardType = "카피";
				break;
				case "WS":
				sCardType = "대필";
				break;
				case "CC":
				sCardType = "크카";
				break;
			}

			
            if (sLetterType == "일반")
            {
                //다카스 신청여부
                isSinchYn = false;
            }

            if (sCardType == "일반")
            {
                isSinchYn_C = false;
            }

        }
        else if (dsSponList.Tables[0].Rows.Count == 0)
        {
            isSinchYn = false;
            isSinchYn_C = false;
        }

        exist.Visible = false;
        if (nSponCount > 5)
        {
            submit5.Visible = false;
            // 편지/카드 전체 신청중일 시
            if (isSinchYn && isSinchYn_C)
            {
                submit6.Visible = false;
                exist.Visible = true;
                exist6_letter.Visible = true;
                exist6_card.Visible = true;
            }
            else
            {
                submit6.Visible = true;
                // 편지 신청중이 아닐 시
                if (!isSinchYn)
                    submit6_letter.Visible = true;
                else
                {
                    submit6_letter.Visible = false;
                    exist6_letter.Visible = true;
                    exist.Visible = true;
                }

                // 카드 신청중이 아닐 시 
                if (!isSinchYn_C)
                    submit6_card.Visible = true;
                else
                {
                    submit6_card.Visible = false;
                    exist6_card.Visible = true;
                    exist.Visible = true;
                }
            }
        }
        else
        {
            submit6.Visible = false;
            exist6_card.Visible = false;
            exist6_letter.Visible = false;

            if (!isSinchYn_C)
                submit5.Visible = true;
            else
            {
                submit5.Visible = false;
                exist5_card.Visible = true;
                exist.Visible = true;
            }
        }

        btn_cancel.Visible = exist.Visible;


        ////신청불가능
        //if (isSinchYn) {
        //    submit5.Visible = false;
        //    submit6.Visible = false;
			
        //    exist.Visible = true;
		
        //}
        ////신청가능
        //else {
        //    exist.Visible = false;
			
        //    if (nSponCount > 5)
        //        submit5.Visible = false;
        //    else
        //        submit6.Visible = false;
        //}

		

		#endregion


		#region 후원어린이가 없거나 후원신청중일때 20120327
		if (nSponCount < 1) {
			base.AlertWithJavascript("현재 후원 신청 중이거나 후원하는 어린이가 없습니다. \\r\\n먼저 1:1 어린이 양육을 신청해 주세요. \\r\\n감사합니다." , "goBack()");
			return;
		}
		#endregion

	}
	
	protected void btnSave5_Click( object sender, EventArgs e ) {
		if(base.IsRefresh) {
			return;
		}

		#region 변수선언
		string sLetter = "GN";
		string sCard = "CC";
		string sUserID = string.Empty;
		string sUserName = string.Empty;
		string sChannel = string.Empty;
		#endregion

		//시스템아이디
		UserInfo sess1 = new UserInfo();
		sUserID = sess1.UserId.ToString();
		sUserName = sess1.UserName.ToString();


		//가입채널
		sChannel = CodeAction.ChannelType;


		#region 결과처리
		string sResult = string.Empty;
		DataSet dsSponCount = _wwwService.CheckedSponCount(sess1.SponsorID);
		sResult = _wwwService.setCopychange(dsSponCount, sLetter, sCard, sUserID, sUserName, sChannel);

		if(sResult.Equals("10")) {
			base.AlertWithJavascript("서비스 신청이 완료되었습니다." , "location.href='/my/letter/smart-letter/introduce'" );
			return;
		} else {

			base.AlertWithJavascript("서비스 신청이 정상적으로 이루어지지 않았습니다.");
			return;
		}

		#endregion
	}
	protected void btnSave6_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}

		#region 변수선언
		string sLetter = string.Empty;
		string sCard = string.Empty;
		string sUserID = string.Empty;
		string sUserName = string.Empty;
		string sChannel = string.Empty;
		#endregion

		//시스템아이디
		UserInfo sess1 = new UserInfo();
		sUserID = sess1.UserId.ToString();
		sUserName = sess1.UserName.ToString();


		//가입채널
		sChannel = CodeAction.ChannelType;


		string sResult = string.Empty;
		/*
		if(select1.Checked) {
			sLetter = "CP";
			sCard = "GN";
		} else 
		
		
		if(select2.Checked) {
			sLetter = "WS";
			sCard = "GN";
		} else {
			sLetter = "GN";
			sCard = "WS";
		}
		*/

        sLetter = "GN";
        sCard = "GN";
        if (select2.Checked)
        {
            sLetter = "WS";
        }
        if (select3.Checked)
        {
            sCard = "WS";
        }

        if (submit6_letter.Visible == false)
            sLetter = "WS";
        if (submit6_card.Visible == false)
            sCard = "WS";

		DataSet dsSponCount = _wwwService.CheckedSponCount(sess1.SponsorID);
		sResult = _wwwService.setCopychange(dsSponCount, sLetter, sCard, sUserID, sUserName, sChannel);

		if(sResult.Equals("10")) {
			base.AlertWithJavascript("서비스 신청이 완료되었습니다.", "location.href='/my/letter/smart-letter/apply'");
			return;
		} else {

			base.AlertWithJavascript("서비스 신청이 정상적으로 이루어지지 않았습니다.");
			return;
		}

	
	}


	protected void btn_cancel_Click( object sender, EventArgs e ) {

        string letterType = "WS";
        string cardType = "WS";

        // 6명 미만
        if (cb_SingleUseCard.Checked)
        {
            letterType = "GN";
            cardType = "GN";
        }
        else
        {
            // 6명 이상
            if (submit6_letter.Visible)
                letterType = "GN";
            else
                letterType = "WS";

            if (submit6_card.Visible)
                cardType = "GN";
            else
                cardType = "WS";

            if (cb_MultiUseLetter.Checked)
                letterType = "GN";
            if (cb_MultiUseCard.Checked)
                cardType = "GN";
        }

        var sess = new UserInfo();

        Object[] objParam = new object[] { "sponsorId", "UserID", "sponsorName", "LetterType", "CardType" };
        Object[] objValue = new object[] { sess.SponsorID, sess.UserId, sess.UserName, letterType, cardType };
        Object[] objSql = new object[] { "sp_web_smart_letter_cancel_f_New" };
        _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

		base.AlertWithJavascript("서비스 해지가 완료되었습니다.", "location.href='/my/letter/smart-letter/apply'");

	}
}