﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;

public partial class my_letter_smart_letter_introduce : MobileFrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}

	WWWService.Service _wwwService = new WWWService.Service();
	WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

	protected override void OnBeforePostBack() {

		UserInfo sess = new UserInfo();

		string sSponsorID = sess.SponsorID.ToString();

		//Sponsor Counting
		int nSponCount = 0;
		DataSet dsSponCount = _wwwService.CheckedSponCount( sSponsorID );

		//수정 20120327
		if (dsSponCount.Tables[0].Rows.Count > 0) {
			nSponCount = int.Parse( dsSponCount.Tables[0].Rows[0]["ChildCount"].ToString() );
		} else {
			nSponCount = 0;
		}


		if (nSponCount > 5) {
			exist6.Visible = true;
		} else {
			exist5.Visible = true;
		}
	}

}