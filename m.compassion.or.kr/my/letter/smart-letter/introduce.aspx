﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="introduce.aspx.cs" Inherits="my_letter_smart_letter_introduce" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript">
	
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	

	
	<!-- 컨텐츠 영역 -->

	<div class="appContent">
    
		<div class="wrap-tab3 sectionsub-margin3">
			<a style="width:50%" class="selected" href="/my/letter/smart-letter/introduce">소개</a>
			<a style="width:50%" href="/my/letter/smart-letter/apply">신청</a>
		</div>

		<div class="service-smartletter">
    		<div><img src="/common/img/app/img-smart1.jpg" alt="" width="100%" /></div>
			<p class="txt-caption mt30">스마트레터 서비스란?</p>
			<p class="txt-sub mt20">
        		어린이에게 편지 한 장이라도 써주고 싶지만 미루고 미루시다 깜빡 잊어버리실까 염려되세요? <br />
        	
				<!--후원 어린이 5명 이하인 경우 소개 컨텐츠-->
				<span id="exist5" runat="server" visible="false">
				스마트레터 서비스를 신청해주시면 컴패션에서 매년 후원자님을 대신하여 후원 어린이에게 크리스마스 카드와 어린이날 카드를 전해드립니다. 
				</span>
			
				<!--후원 어린이 6명 이상인 경우 소개 컨텐츠 일부 문구 다름-->
				<span id="exist6" runat="server" visible="false">
					스마트레터 서비스를 신청해주시면 컴패션에서 매년 후원자님을 대신하여 후원 어린이에게 생일 카드, 크리스마스 카드, 어린이날 카드 및 편지를 전해드립니다.
				</span>
			</p>
		</div>
		<div class="box-guide mt20">
			<strong class="txt-note">더 알고 싶어요!</strong>
			<ul class="list-txt">
				<li>1. 매년 새로운 내용의 카드를 후원 어린이에게 전해드립니다. (크리스마스/어린이날이 지난 후에 신청하시면 그 다음해부터 발송됩니다.</li>
				<li>2. 어린이날 카드는 만 13세 이하 어린이에게만 보내지는 카드입니다. </li>
				<li>3. 여러 명의 어린이들을 후원하고 있어도 염려 마세요. 정성 듬뿍 담은 카드를 후원 어린이 모두에게 보내드립니다.</li>
				<li>4. 스마트레터 서비스를 통해 어린이에게 보낸 카드는 편지함에서 확인하실 수 있습니다.</li>
				<li>5. 스마트레터 서비스를 이용하시더라도, 어린이에게 직접 편지와 카드를 작성하실 수 있습니다.</li>
			</ul>
		</div>
		<div class="wrap-bt mb30"><a class="bt-type6" style="width:100%" href="/my/letter/smart-letter/apply">신청</a></div>
        

	</div>
	<!--// 컨텐츠 영역 -->

</asp:Content>
