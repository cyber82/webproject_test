﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_letter_default : MobileFrontBasePage {

	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		
		{
			var actionResult = new ChildAction().MyAllChildren("", 1, 5 , "name" , true);
			if(actionResult.success) {
				List<ChildAction.MyChildItem> data = ((List<ChildAction.MyChildItem>)actionResult.data);

				children.Value = data.ToLowerCaseJson();

			//	Response.Redirect(string.Format("/my/letter/child/{0}?name={1}&childKey={2}&header=root", data[0].ChildMasterId, data[0].Name, data[0].ChildKey));
				if(data.Count == 1) {
					Response.Redirect(string.Format("/my/letter/child/{0}?name={1}&childKey={2}&header=root" , data[0].ChildMasterId , data[0].Name , data[0].ChildKey));
				}else if (data.Count < 1) {
					Response.Redirect("/app/children/nodata?from=letter");
					return;
				}
			} else {
				if (actionResult.action == "not_sponsor") {
					Response.Redirect("/app/children/nodata?from=letter");
					return;
				}
				base.AlertWithJavascript("편지정보를 불러오지 못했습니다." , "goBack();");
			}
		}

		
		{
			var actionResult = new LetterAction().GetUnreadCount();
			if(actionResult.success) {
				var entity = ((LetterAction.UnreadEntity)actionResult.data);
				var count = entity.cnt;
				ph_unread.Visible = count > 0;

				if(count > 0) {
					btn_unread.HRef = "/my/letter/child/" + entity.childMasterId + "?name=" + entity.childName;
					btn_unread.InnerHtml = string.Format("<span class='icon'></span>아직 읽지 않은 <em>{0}</em>통의 편지가 있어요!", count);
				}

			} else {
				ph_unread.Visible = false;
			}
		}

	}

}