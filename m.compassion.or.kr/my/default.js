﻿

(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		$scope.thismonth = new Date();
		$scope.commitment_list = null;
		$scope.commitment_total = -1;
		$scope.payment_total = 0;
		$scope.order_total = -1;
		$scope.order_list = null;
		$scope.data = null;
		$scope.select_orderNo = "";

		// 데이타
		$scope.getData = function () {

			$http.get("/api/my/main.ashx?t=data", { params: {} }).success(function (r) {
			//	console.log(r);
				if (r.success) {

					$scope.data = r.data;
					console.log("main", $scope.data);

					if (r.data.child) {
						var name = r.data.child.name;
						var regex = /\(([^)]+)\)/;
						var matches = regex.exec(name);
						var en_name = matches[1];
						var ko_name = name.replace("(" + matches[1] + ")", "");
						r.data.child.en_name = en_name;
						r.data.child.ko_name = ko_name;
						
					}


				} else {
					if (r.action == "not_sponsor") {
						$scope.data = r.data;
					}
					//alert(r.message);
				}
			});

		}

		// 정기후원내역
		$scope.getCommitmentList = function () {

			$http.get("/api/my/commitment.ashx?t=list", { params: {page : 1 , rowsPerPage : 3} }).success(function (r) {
				
				if (r.success) {
					
					var list = r.data;
					
					$.each(list, function () {
						this.startdate = new Date(this.startdate);
					});
					$scope.commitment_list = list;
					$scope.commitment_total = list.length;
				} else {

					
				}
			});

		}

		// 이번달 결제금액
		$scope.getPaymentList = function () {

			var date_begin = $filter('date')(new Date(), 'yyyy-MM-01');
			var date_end = $filter('date')(new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0), 'yyyy-MM-dd');
			//console.log(date_begin, date_end);

			$http.get("/api/my/payment.ashx?t=history", { params: { page: 1, rowsPerPage: 1, date_begin: date_begin, date_end: date_end } }).success(function (r) {

				//console.log(r);
				if (r.success) {

					if (r.data.length > 0) {
						$scope.payment_total = r.data[0].totalamount;
					}

				} else {
					//	alert(r.message);
					if (r.action == "not_sponsor") {
						$scope.payment_total = 0;
					}

				}
			});
		}

		// 이번달 주문 내역 
		$scope.getOrderList = function () {

			$http.get("/api/store.ashx?t=order_list", { params: { page: 1, rowsPerPage: 5} }).success(function (r) {

				if (r.success) {
				    var list = r.data;

                    
					if (r.data.length > 0) {
						$scope.order_total = r.data[0].totalamount;
					}

					$.each(list, function () {
						this.dtreg = new Date(this.dtreg);


						if (this.statename == "출고완료") {
							if (this.deliveryco == 1) {
								this.href = 'https://trace.epost.go.kr/xtts/servlet/kpl.tts.common.svl.SttSVL?ems_gubun=E&sid1=' + this.deliverynum + '&POST_CODE=&mgbn=trace&traceselect=1&target_command=kpl.tts.tt.epost.cmd.RetrieveOrderConvEpostPoCMD&JspURI=%2Fxtts%2Ftt%2Fepost%2Ftrace%2FTrace_list.jsp&postNum=' + this.deliverynum + "&app_ex=1";
							} else if (this.deliveryco == 2) {
								this.href = "http://d2d.ilogen.com/d2d/delivery/invoice_tracesearch_quick.jsp?slipno=" + this.deliverynum + "&app_ex=1";
							} else if (this.deliveryco == 3) {
								this.href = 'http://www.hlc.co.kr/hydex/jsp/tracking/trackingViewCus.jsp?InvNo=' + this.deliverynum + "&app_ex=1";
							} else if (this.deliveryco == 5) {
								this.href = 'http://nplus.doortodoor.co.kr/web/detail.jsp?slipno=' + this.deliverynum + "&app_ex=1";
							} else {
								this.href = "";
							}
						}
					});
				    //console.log(list);


					$scope.order_list = list;

				} else {
					alert(r.message);
				}
			});
		}

		// 상세페이지
		$scope.goChildView = function ($event, item) {
			$event.preventDefault();
			location.href = "/my/children/view/" + item.childkey + "?childMasterId=" + item.childmasterid;
		}

		$scope.getData();

		$scope.getCommitmentList();

		$scope.getPaymentList();

		$scope.getOrderList();

		// 후원어린이가 없는 경우
		$scope.item = $.parseJSON($("#data").val());
		$scope.item.birthdate = new Date($scope.item.birthdate);

		$scope.showChildPop = function ($event, item) {
			loading.show();
			if ($event) $event.preventDefault();
			popup.init($scope, "/common/child/pop/" + item.countrycode + "/" + item.childmasterid + "/" + item.childkey, function (modal) {
				modal.show();

				initChildPop($http, $scope, modal, item);

			}, { top: 0, iscroll: true, removeWhenClose: true });
		}

		$scope.showAlbum = function ($event, item) {
			loading.show();
			$event.preventDefault();
			popup.init($scope, "/common/child/album/" + item.childmasterid, function (modal) {
				modal.show();
				activateAlbum($scope, item.childkey, modal);
			}, { removeWhenClose: true });
		}


		$scope.modal = {
		    instance: null,
		    payments: [],
		    paymentsByMonth: [],
		    totalPayment: 0,
		    years: [],

		    init: function () {

		        for (var i = 0 ; i < 3 ; i++) {
		            this.years.push(new Date().getFullYear() - i);
		        }
		    },

		    show: function (item) {

		        $scope.entity = $.extend({}, item);
		        $scope.entity.birthdate = new Date($scope.entity.birthdate);
		        $scope.entity.ptd = new Date($scope.entity.ptd);
		        if ($scope.entity.ptd.getFullYear() < 2000) {
		            $scope.entity.ptd = null;
		        }

		        $scope.selectedYear = new Date().getFullYear();
		        $("#view_year").val("number:" + $scope.selectedYear);

		        popup.init($scope, "/my/sponsor/commitment/view", function (modal) {
		            $scope.modal.instance = modal;
		            $scope.modal.changeYear($scope.entity.fundingfrequency == '1회' ? "" : $scope.selectedYear);
		            $scope.modal.instance.show();
		        }, { top: 0, iscroll: true, removeWhenClose: true });

		    },

		    hide: function ($event) {
		        $event.preventDefault();
		        $scope.modal.instance.hide();

		    },

		    changeYear: function (val) {
		        $scope.modal.payments = [];
		        $scope.modal.paymentsByMonth = [];
		        $scope.modal.totalPayment = 0;
		        // 월별결제내역
		        $http.get("/api/my/payment.ashx?t=get-monthly-history-by-commitmentId", { params: { c: $scope.entity.commitmentid, year: val } }).success(function (r) {
		            console.log(r);
		            if (r.success) {

		                $scope.modal.payments = r.data;

		                // 정기는 월별로 데이타세팅
		                if ($scope.entity.fundingfrequency != '1회') {

		                    $scope.modal.paymentsByMonth = [];
		                    for (var i = 0; i < 12; i++) {
		                        $scope.modal.paymentsByMonth[i] = "-";

		                        $.each(r.data, function () {
		                            if (parseInt(this.paymentdate) == (i + 1)) {

		                                $scope.modal.totalPayment += this.totalamount;
		                                $scope.modal.paymentsByMonth[i] = this.totalamount.format() + " 원";
		                            }
		                        });
		                    }

		                }

		            } else {
		                alert(r.message);
		            }
		        });
		    }
		};

		$scope.modal.init();

		$scope.showDetail = function ($event, item) {
		    // childmasterid : 0000000000
		    console.log(item);
		    $event.preventDefault();
		    $scope.modal.show(item);


		}

		$scope.storeModal = {
		    instance: null,
		    banks: [],
		    detail_list: [],

		    show: function (orderNo) {
		        popup.init($scope, "/my/store/order/detail-order-main", function (storeModal) {
		            $scope.storeModal.instance = storeModal;
		            $scope.storeModal.instance.show();
		        }, { top: 0, iscroll: true, removeWhenClose: true });
		        // 상세 주문 내역 

		        $http.get("/api/store.ashx?t=order_detail_list&orderno=" + orderNo, {}).success(function (r) {

		            if (r.success) {

		                var list = r.data;

		                $.each(list, function () {
		                    this.dtreg = new Date(this.dtreg);
		                    if (this.childname == "") {
		                        this.product_type = "일반상품";
		                    } else {
		                        this.product_type = "어린이에게 보내는 선물";
		                        this.childname = "선물받을 어린이 :6 " + this.childname;
		                    }


		                });
		                $scope.storeModal.detail_list = list;
		            } else {
		                alert(r.message);
		            }
		        });
		    },
		    close: function ($event) {
		        $event.preventDefault();
		        $scope.storeModal.instance.hide();

		    }

		};

		$scope.goView = function (idx, $event) {
		    location.href = "/store/item/" + idx;
		    $event.preventDefault();

		}

		// 선물금보내기
		$scope.goGift = function ($event, item) {

			$event.preventDefault();
			if (!item.paid) {
				alert("첫 후원금 납부 후 어린이에게 선물금을 보내실 수 있습니다.\n납부 관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr ");
				return;
			}

			if (!item.cangift) {
				alert("편지후원자의 경우는 선물금보내기 기능이 제한됩니다.");
				return;
			}
			location.href = "/my/sponsor/gift-money/pay/?t=temporary&childMasterId=" + item.childmasterid;

		}

	});

})();