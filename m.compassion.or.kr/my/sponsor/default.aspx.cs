﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_sponsor_default : MobileFrontBasePage {
	
	protected override void OnBeforePostBack() {
		if(Request.UrlReferrer != null && Request["lnb"] != "Y") {
			if(Request.UrlReferrer.AbsolutePath.IndexOf("/my/sponsor/") > -1) {
				if (AppSession.HasCookie(this.Context))
					Response.Redirect("/app/my");
				else
					Response.Redirect("/my/");
			}
		}
		Response.Redirect("/my/sponsor/commitment/");

	}

}