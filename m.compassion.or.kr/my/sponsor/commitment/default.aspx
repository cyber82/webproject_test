﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_commitment_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
    <script type="text/javascript" src="/my/sponsor/commitment/default.js?v=1.0"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    <style>
        .menu-my {
                display: block;
                position: relative;
                background: url(/common/img/page/my/bg-menu.gif) 0 bottom repeat-x #fff;
                background-size: 100% 6px;
                overflow: hidden;
                height: 70px;
            }
    </style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" id="hd_auth_domain" runat="server" />
    <input type="hidden" id="hdSponsorId" runat="server" />
    <input type="hidden" id="hdBirthDate" runat="server" />
    <input type="hidden" id="hdUserName" runat="server" />

    <div class="wrap-sectionsub-my" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <!--마이컴패션 디자인-->
        <!---->

        <div class="menu-my sectionsub-margin2">

            <ul style="position: absolute; top: 0;">
                <!--임시 inline style(수정가능)-->
                <li style="display: inline-block; width: 100px"><a class="selected" href="/my/sponsor/commitment/">정기후원<br />신청내역</a></li>
                <li style="display: inline-block; width: 100px"><a href="/my/sponsor/payment/">후원금<br />내역</a></li>
                <li style="display: inline-block; width: 100px"><a href="/my/sponsor/pay-delay/">지연된<br />후원금</a></li>
                <li style="display: inline-block; width: 100px"><a href="/my/sponsor/gift-money/">어린이<br />선물금</a></li>
                <li style="display: inline-block;"></li>
            </ul>
        </div>

        <div class="my-sponsor">
            <asp:PlaceHolder runat="server" ID="ph_change_notice" Visible="true">
                <!--본인인증이 완료된 사용자에게 해당 메시지 출력해외/기업 회원의 경우 비노출 -->
                <div class="self-account sectionsub-margin2">
                    <strong>후원자님의 정기후원 신청 내역을 확인하실 수 있습니다. <br />
                                일시후원 내역은 '후원금 내역' 메뉴에서 확인해주세요.</strong>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="ph_before_auth" Visible="false">

                <div class="self-account sectionsub-margin2 cert">
                    <strong>혹시 기존 후원내역이 안보이세요?</strong>
                    후원내역을 연결하여 하나의 아이디로 회원님의<br />
                    후원내역을 관리해보세요.
			        <div class="link-account">
                        <a href="#" id="btn_cert_by_phone"><span>휴대폰 인증</span></a>
                        <a href="#" id="btn_cert_by_ipin"><span>아이핀(i-PIN) 인증</span></a>
                    </div>
                </div>

            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="ph_after_auth" Visible="false">
                <!--본인인증이 완료된 사용자에게 해당 메시지 출력해외/기업 회원의 경우 비노출 -->
                <div class="self-account sectionsub-margin2">
                    <strong>본인인증이 완료되었습니다.</strong>
                    본인인증 후에도 기존 후원내역이 안보이시는 경우<br />
                    한국컴패션 후원지원팀으로 연락 주시기 바랍니다.
				<div class="linebar"></div>
                    후원지원팀 (<a href="tel:02-740-1000">02-740-1000</a> 평일 9시~18시/공휴일제외) / <a class="color1" href="mailto:info@compassion.or.kr">info@compassion.or.kr</a>
                </div>
            </asp:PlaceHolder>


            <div class="box-mywhite list-mysupport" id="l">
                <div class="wrap-header">
                    <p class="txt-title">정기후원 신청내역<em ng-if="total > -1">(총 {{total}}건)</em></p>
                    <span class="more nopad">
                        <select style="width: 150px" ng-model="params.sponsorItemEng" ng-change="changeSponsorItemEng()">
                            <option value="">전체</option>
                            <option value="commitment">1:1어린이양육 후원</option>
							<option value="gift">어린이 선물금</option>
                            <option value="funding">특별한 나눔</option>
                        </select></span>
                </div>
                <ul>
                    <li class="{{item.icon}}" ng-repeat="item in list"><a href="#" ng-click="showDetail($event,item)">
                        <p class="txt1">{{item.accountclassdetail}}</p>
                        <span class="txt4">{{item.accountclassname}}<span class="bar">|</span>{{item.commitstatusText}}</span>
                        <span class="txt2">{{item.fundingfrequency == '1회' ? "일시" : "정기" }} <em>{{item.amount | number:0}}</em> 원</span>
                        <span class="bar">|</span>
                        <time class="txt3" datetime="{{item.startdate | date:'yyyy.MM.dd'}}">{{item.startdate | date:'yyyy.MM.dd'}}</time>
                    </a></li>

                    <!--후원 내역이 없는 경우 -->
                    <li class="no-result" ng-if="total == 0">신청하신 후원 내역이 없습니다.<br />
                        첫 후원을 하러 가보시겠어요?
					<div class="wrap-bt"><a class="bt-type6" id="sponsorItemEngLinkURL" style="width: 180px" href="/sponsor/children/">1:1 어린이 양육 시작하기</a></div>
                    </li>
                </ul>
            </div>

            <paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>

            <div class="box-mywhite bottom-info">
				후원자님께서 보내주시는 후원금은 어린이들이 지적, 사회·정서적, 신체적, 영적으로 온전하게 자라가는 데 큰 도움이 되고 있습니다.<br />
                <br />

                후원 신청 내역에 대한 문의사항이 있으시면 한국컴패션으로 연락 주시기 바랍니다.<br />
                <br />
                후원지원팀 (<a href="tel:02-740-1000">02-740-1000</a> 평일 9시~18시/공휴일제외) / <a class="color1" href="mailto:info@compassion.or.kr">info@compassion.or.kr</a>
            </div>


        </div>

        <!--//-->
    </div>

</asp:Content>

