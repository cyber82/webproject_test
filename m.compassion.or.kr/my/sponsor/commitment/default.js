﻿$(function () {

	var sly = new Sly($(".menu-my"), {    // Call Sly on frame

		horizontal: 1,
		itemNav: 'centered',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 3,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1

	});

	sly.init();

	$.each($(".menu-my a"), function (i) {
	    if ($(this).hasClass("selected")) {
	        sly.activate(i);
	    }
	});

	if (cookie.get("cps.app") != null) {
	    $(".appPageNav").find(".back").attr("href", "/app/my/");
	} else {
	    $(".title-depth").find(".backgo").attr("href", "/my/");
	}
	
});

$(function () {
	// 휴대폰 인증
	$("#btn_cert_by_phone").click(function () {
		cert_openPopup("phone", $("#hd_auth_domain").val());

		return false;
	})

	// 아이핀 인증
	$("#btn_cert_by_ipin").click(function () {
		cert_openPopup("ipin", $("#hd_auth_domain").val());
		
		return false;
	})
});

// 본인인증 결과 응답
// result = Y or N , birth = yyyyMMdd
// gender = M or F
// method = ipin or phone
var cert_setCertResult = function (method, result, ci, di, name, birth, gender, phone) {
	
	console.log(method, result, ci, di, name, birth, gender, phone);

	if (result != "Y") {
		alert("본인인증에 실패했습니다. 다시 시도해 주세요.");
		return;
	}
	// 테스트를 위해 임시로 막음
	if ($("#hdUserName").val() != name || $("#hdBirthDate").val() != birth) {
		alert("로그인정보와 본인인증정보가 일치하지 않습니다.");
		return;
	}
	
	checkCI(method, ci, di, gender);

};

var checkCI = function (method, ci, di, gender) {

	$.post("/api/sponsor.ashx", { t: "sync", ci: ci, di: di, gender: gender }, function (r) {

		if (r.success) {
			
			if (r.action == "login") {
			    alert("본인인증된 계정이 이미 존재합니다. \n해당 계정으로 로그인하셔서 후원내역을 확인해주십시요. \n(아이디 : " + r.data.user_id + ")");
				// 로그인창오픈

			} else if (r.action == "reload") {

				location.reload();
				// offline 가입페이지로 이동

			}
		} else {
			alert(r.message);
		}

	});

	return false;

};

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		$scope.requesting = false;
		$scope.total = -1;
		$scope.rowsPerPage = 5;
		$scope.list = null;
		
		$scope.params = {
			page: 1,
			rowsPerPage: $scope.rowsPerPage,
            fundingfrequency: "",
            sponsorItemEng: ""
		};
		$scope.selectedYear = new Date().getFullYear();

		// 후원상세
		$scope.modal = {
			instance: null,
			payments: [],
			paymentsByMonth: [],
			totalPayment : 0 , 
			years : [] ,

			init: function () {

				for (var i = 0 ; i < 9 ; i++) {
					this.years.push(new Date().getFullYear() - i);
				}
			},

			show: function (item) {
			    
				$scope.entity = $.extend({} , item);
				$scope.entity.birthdate = new Date($scope.entity.birthdate);
				$scope.entity.ptd = new Date($scope.entity.ptd);
				if ($scope.entity.pic == null || $scope.entity.pic == "")
					$scope.entity.pic = "/common/img/common/noimg.jpg";
				if ($scope.entity.ptd.getFullYear() < 2000) {
					$scope.entity.ptd = null;
				}

				$scope.selectedYear = new Date().getFullYear();
				$("#view_year").val("number:"+$scope.selectedYear);

				popup.init($scope, "/my/sponsor/commitment/view", function (modal) {
					$scope.modal.instance = modal;
					//$scope.modal.changeYear($scope.entity.fundingfrequency == '1회' ? "" : $scope.selectedYear);
					$scope.modal.changeYear(item.fundingfrequency == '1회' ? $.datepicker.formatDate('yy', item.startdate) : $scope.selectedYear);

					$scope.modal.instance.show();
				}, { top: 0, iscroll: true, removeWhenClose: true });
				
			},

			hide: function ($event) {
				$event.preventDefault();
				$scope.modal.instance.hide();

			},

			changeYear: function (val) {
				$scope.modal.payments = [];
				$scope.modal.paymentsByMonth = [];
				$scope.modal.totalPayment = 0;
			    // 월별결제내역
				
				$http.get("/api/my/payment.ashx?t=get-monthly-history-by-commitmentId", { params: { c: $scope.entity.commitmentid , year : val } }).success(function (r) {
					console.log(r);
					if (r.success) {

						$scope.modal.payments = r.data;
						
						// 정기는 월별로 데이타세팅
						if ($scope.entity.fundingfrequency != '1회') {

							$scope.modal.paymentsByMonth = [];
							for (var i = 0; i < 12; i++) {
								$scope.modal.paymentsByMonth[i] = "-";

								$.each(r.data , function(){
									if (parseInt(this.paymentdate) == (i + 1)) {

										$scope.modal.totalPayment += this.totalamount;
										$scope.modal.paymentsByMonth[i] = this.totalamount.format() + " 원";
									}
								});
							}

						}

					} else {
						alert(r.message);
					}
				});
			}
		};
		
		// 정기후원내역
		$scope.getList = function (params) {
            //2018-05-23 이종진 - #WO-55 후원신청내역 -> 정시후원신청내역으로 변경함에 따라 정기후원만 조회하도록 파라미터 고정
            $scope.params.fundingfrequency = "정기후원";
			$scope.params = $.extend($scope.params, params);
			
			$http.get("/api/my/commitment.ashx?t=list", { params: $scope.params }).success(function (r) {
				
				if (r.success) {
					
					var list = r.data;
					
					$.each(list, function () {
						this.startdate = new Date(this.startdate);
						this.commitstatusText = this.commitstatus.replace("후원완료", "후원완료").replace("후원취소", "취소");
						this.icon = getIcon(this.accountclass);
						console.log(this.icon);
					});

					$scope.list = list;
					$scope.total = list.length > 0 ? list[0].total : 0;
					console.log(list , $scope.total);

					if (params)
						scrollTo($("#l"), 30);

				} else {
					alert(r.message);
				}
			});

			var getIcon = function (accountclass) {
				switch (accountclass) {
					default: return "ic-gift3";	// 특별한후원
					case "DS": case "DSADD": case "LS": case "LSADD": return "ic-gift1";
					case "DF": return "ic-gift2";
					case "BG": case "PG": case "GG": case "FG": return "ic-gift4";
				}
			};
		}

        $scope.changeSponsorItemEng = function () {
            $scope.params.page = 1;
            //선택한 값에 따라, 링크 변경
            switch ($scope.params.sponsorItemEng) {
                case "gift":
                    $("#sponsorItemEngLinkURL").attr("href", "/my/sponsor/gift-money/");
                    $("#sponsorItemEngLinkURL").text("어린이 선물금 보내기");
                    break;
                case "funding":
                    $("#sponsorItemEngLinkURL").attr("href", "/sponsor/special/");
                    $("#sponsorItemEngLinkURL").text("특별한 나눔 후원하기");
                    break;
                default:
                    $("#sponsorItemEngLinkURL").attr("href", "/sponsor/children/");
                    $("#sponsorItemEngLinkURL").text("1:1 어린이 양육 시작하기");
                    break;
            }

			$scope.getList();
		}

		$scope.getList();

		$scope.modal.init();

		$scope.showDetail = function ($event , item) {
			// childmasterid : 0000000000
			console.log(item);
			$event.preventDefault();
			$scope.modal.show(item);


		}

	});

})();