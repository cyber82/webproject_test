﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="my_commitment_view"  %>


<div style="background:transparent;" class="fn_pop_container" id="childModalview">

	<div class="wrap-layerpop fn_pop_content" style="width:100%;left:0;">
        <div class="header2">
        	<p class="txt-title">후원 상세 정보</p>
        </div>
        <div class="contents">
            
            <div class="my-sponsor">
            
                <div class="sponsordetail-title">
                	후원정보
                    <span class="ar"><span ng-if="entity.fundingfrequency == '1회'">후원일</span>
		<span ng-if="entity.fundingfrequency != '1회'">후원 시작일</span>
				 : {{entity.startdate |  date:'yyyy-MM-dd'}}</span>
                </div>

				 <div class="sponsordetail-child" ng-if="entity.childmasterid != '0000000000'">
                	<span class="frame"><span class="photo" style="background:url('{{entity.pic}}') no-repeat"></span></span>    <!--어린이사진 inline style-->
                    <em class="txt-type">{{entity.accountclassname}}</em>
                    <p class="txt-name">{{entity.accountclassdetail}}</p>
                    <p class="txt-info"><span class="color1">어린이 ID : </span> {{entity.childkey}}</p>
                    <p class="txt-info"><span class="color1">국가 : </span> {{entity.countryname}}</p>
                    <p class="txt-info"><span class="color1">생일 : </span> {{entity.birthdate | date:'yyyy-MM-dd'}} ({{entity.age}}세)</p>
                </div>
                

                <div class="sponsordetail-child2" ng-if="entity.childmasterid == '0000000000'">
                	<span class="frame"><span class="photo" style="background:url('{{entity.pic}}') no-repeat"></span></span>    <!--어린이사진 inline style-->
                    <em class="txt-type">{{entity.accountclassname}}</em>
                    <p class="txt-name">{{entity.accountclassdetail}}</p>
                </div>
                
                <div class="wrap-tableW">
                	<table class="tableW">
                        <colgroup><col width="50%" /><col width="50%" /></colgroup>
                    	<caption>후원 유형,후원 금액</caption>
                        <thead>
                        	<tr>
                            	<th>후원 유형</th>
                                <th>후원 금액</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
                            	<td>{{entity.fundingfrequency == '1회' ? "일시후원" : "정기후원" }}<span ng-if="entity.fundingfrequency == '1회'">/ {{entity.paymentname}}</span></td>
                                <td><span class="color1">{{entity.amount | number:0}}원<span ng-if="entity.fundingfrequency != '1회'">/{{entity.fundingfrequency.replace("매" , "")}}</span></td>
                            </tr>
                        </tbody>
                    </table>
					 <p class="txt-bottom" ng-if="entity.fundingfrequency != '1회' && entity.ptd">후원금 최종 적용월 {{entity.ptd | date:'yyyy.MM'}}월</p>
                </div>
                
				<div class="sponsordetail-title" ng-if="entity.fundingfrequency == '1회'">
                	납부 내역
                </div>
                <table class="list-payment" ng-if="entity.fundingfrequency == '1회'">
                    <colgroup><col style="width:50%" /><col style="width:50%" /></colgroup>
                	<caption></caption>
                    <tbody>
                       <tr>
						   <th>{{modal.payments[0].paymentdate }}월</th>
						   <td>{{modal.payments[0].totalamount | number:0}} 원</td>
					   </tr>
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<th>총 납부 금액</th>
                            <td><em>{{modal.payments[0].totalamount | number:0}}</em> 원</td>
                        </tr>
                    </tfoot>
                </table>

                <div class="sponsordetail-title" ng-if="entity.fundingfrequency != '1회'">
                	월별 납부 내역
                    <span class="ar"><select style="width:90px" ng-model="selectedYear" ng-change="modal.changeYear(selectedYear)" data-ng-options="item as item+'년' for item in modal.years">
						</select></span>
                </div>
                <table class="list-payment" ng-if="entity.fundingfrequency != '1회'">
                    <colgroup><col style="width:50%" /><col style="width:50%" /></colgroup>
                	<caption></caption>
                    <tbody>
                       <tr>
                        	<th>1월</th>
	                        <td>{{modal.paymentsByMonth[0]}}</td>
                        </tr>
                        <tr>
                        	<th>2월</th>
	                        <td>{{modal.paymentsByMonth[1]}}</td>
                        </tr>
                        <tr>
                        	<th>3월</th>
	                        <td>{{modal.paymentsByMonth[2]}}</td>
                        </tr>
                        <tr>
                        	<th>4월</th>
	                        <td>{{modal.paymentsByMonth[3]}}</td>
                        </tr>
                        <tr>
                        	<th>5월</th>
	                        <td>{{modal.paymentsByMonth[4]}}</td>
                        </tr>
                        <tr>
                        	<th>6월</th>
	                        <td>{{modal.paymentsByMonth[5]}}</td>
                        </tr>
                        <tr>
                        	<th>7월</th>
	                        <td>{{modal.paymentsByMonth[6]}}</td>
                        </tr>
                        <tr>
                        	<th>8월</th>
	                        <td>{{modal.paymentsByMonth[7]}}</td>
                        </tr>
                        <tr>
                        	<th>9월</th>
	                        <td>{{modal.paymentsByMonth[8]}}</td>
                        </tr>
                        <tr>
                        	<th>10월</th>
	                        <td>{{modal.paymentsByMonth[9]}}</td>
                        </tr>
                        <tr>
                        	<th>11월</th>
	                        <td>{{modal.paymentsByMonth[10]}}</td>
                        </tr>
                        <tr>
                        	<th>12월</th>
	                        <td>{{modal.paymentsByMonth[11]}}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<th>총 납부 금액</th>
                            <td><em>{{modal.totalPayment | number:0}}</em> 원</td>
                        </tr>
                    </tfoot>
                </table>
                
                <div class="wrap-bt"><a class="bt-type8" style="width:80px" ng-click="modal.hide($event)">닫기</a></div>
                
                
            <div>
            
        </div>
        <div class="close2"><a class="ic-close"  ng-click="modal.hide($event)">레이어 닫기</a></div>
    </div>
    
</div>    
<!--//레이어 안내 팝업-->



