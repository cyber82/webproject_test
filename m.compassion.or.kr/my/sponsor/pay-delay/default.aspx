﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_pay_delay_default" MasterPageFile="~/main.Master" ValidateRequest="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>
<%--<%@ Register Src="/my/sponsor/pay-delay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/my/sponsor/pay-delay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/my/sponsor/pay-delay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>--%>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/my/sponsor/pay-delay/default.js"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    <script>
    	$(function () {

            if ($("section").hasClass("appPageNav")) {
                var pageH = $(".fullHeight").height();
                var deviceH = $(window).height() - 62;

                if (pageH < deviceH) {
                    $(".fullHeight").css({ "height": deviceH });
                }
            }
        });
    </script>
    <style>
    .menu-my {
            display: block;
            position: relative;
            background: url(/common/img/page/my/bg-menu.gif) 0 bottom repeat-x #fff;
            background-size: 100% 6px;
            overflow: hidden;
            height: 70px;
        }
</style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" id="amount" runat="server" />

    <input type="hidden" runat="server" id="jumin" value="" />
    <input type="hidden" runat="server" id="ci" value="" />
    <!-- 본인인증 CI -->
    <input type="hidden" runat="server" id="di" value="" />
    <!-- 본인인증 DI -->
    <asp:HiddenField ID="hidConfirm" runat="server" />

    <div class="wrap-sectionsub-my fullHeight">
        <!--마이컴패션 디자인-->
        <!---->

        <div class="menu-my sectionsub-margin2">

            <ul style="position: absolute; top: 0;">
                <!--임시 inline style(수정가능)-->
                <li style="display: inline-block; width: 100px"><a href="/my/sponsor/commitment/">정기후원<br />신청내역</a></li>
                <li style="display: inline-block; width: 100px"><a href="/my/sponsor/payment/">후원금<br />내역</a></li>
                <li style="display: inline-block; width: 100px"><a class="selected" href="/my/sponsor/pay-delay/">지연된<br />후원금</a></li>
                <li style="display: inline-block; width: 100px"><a href="/my/sponsor/gift-money/">어린이<br />선물금</a></li>
                <li style="display: inline-block;"></li>
            </ul>

        </div>

        <div class="my-sponsor">
            <div id="exists" runat="server">
                <div class="box-myblue support-amount" style="margin-top: -5px">
                    <p class="txt1">납부가 지연된 1:1어린이<br />
                        양육 후원금은</p>
                    <p class="txt-amount"><em>
                        <asp:Literal runat="server" ID="lblAmountTotal" /></em>원</p>
                </div>

                <div class="box-mywhite delay-c​ontribution">
                    <p class="wrap-caption">후원금이 지연된 어린이는 총 <span class="color2">
                        <asp:Literal runat="server" ID="lblChildTotal" /></span>명입니다.</p>

                    <div class="wrap-tableW mt20">
                        <table class="tableW">
                            <caption>후원 중인 어린이 수, 지연된 개월 수, 지연된 금액 정보</caption>
                            <colgroup>
                                <col width="27%" />
                                <col width="32%" />
                                <col width="41%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>어린이 수</th>
                                    <th>지연된 개월 수</th>
                                    <th>지연 후원금액(원)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="grvDeliquent" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("ChildCnt")%></td>
                                            <td><%#Eval("DeliquentMonth")%>개월</td>
                                            <td><span class="color2"><%#Convert.ToDecimal(Eval("Amount")).ToString("N0")%></span></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="box-myblue support-amount" style="margin-top: -5px" id="nonexists" runat="server">
                <p class="txt1">납부가 지연된 1:1어린이양육 후원금이 없습니다.</p>

            </div>

            <div id="pn_payment" runat="server">
                <div class="box-mywhite delay-c​ontribution">
                    <p class="wrap-caption">결제하실 어린이(들)의 <span class="color2" style="color: #333">지연된 후원금</span>을 선택해 주세요.</p>
                    <div class="row">
                        <asp:DropDownList ID="ddlSelectMonth" Style="width: 120px" runat="server" OnSelectedIndexChanged="ddlSelectMonth_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>

                    </div>
                    <div class="row" runat="server" id="divAddPay">
                        <span class="checkbox-ui">
                            <asp:CheckBox ID="cbxAdd" runat="server" Text="" OnCheckedChanged="cbxAdd_CheckedChanged" AutoPostBack="True" />
                            <label for="cbxAdd" class="css-label" style="padding-left:25px">당월 후원금도 함께 결제</label>
                        </span>
                    </div>
                    <div class="row" runat="server" id="divAddPay2">
                        당월 후원금은 지정된 이체일에 자동으로 결제될 예정이므로 홈페이지에서는 지연된 후원금만 결제하실 수 있습니다.
                    </div>
                    <div class="row">
                        <table class="list-payment">
                            <colgroup>
                                <col style="width: 53%" />
                                <col style="width: 47%" />
                            </colgroup>
                            <tbody>
                                <tr style="display:none;">
                                    <th class="align-left">결제할 지연된 후원금</th>
                                    <td>
                                        <asp:Label ID="lblDeliquentAmount" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr style="display:none;">
                                    <th class="align-left">결제할 당월 후원금</th>
                                    <td>
                                        <asp:Label ID="lblNowAmount" runat="server" Text=""></asp:Label></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>결제 후원금</th>
                                    <td><em>
                                        <asp:Label ID="lblTotalAmount" runat="server" Text=""></asp:Label></em></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <div class="box-mywhite delay-c​ontribution">
                    <div class="wrap-header">
                        <p class="txt-title">결제방법</p>
                    </div>
                    <fieldset class="frm-input">
                        <legend>결제수단 입력</legend>
                        <span class="row">
                            <span class="radio-ui">
                                <input type="radio" id="payment_method_card" runat="server" name="payment" class="css-radio" checked />
                                <label for="payment_method_card" class="css-label">신용카드</label>
                            </span>
                            <span class="radio-ui">
                                <input type="radio" id="payment_method_cms" runat="server" name="payment" class="css-radio" />
                                <label for="payment_method_cms" class="css-label">실시간 계좌이체</label>
                            </span>
                            <span class="radio-ui">
                                <input type="radio" id="payment_method_kakao" runat="server" name="payment" class="css-radio" />
                                <label for="payment_method_kakao" class="css-label">
                                    <img src="/common/img/icon/kakaopay.png" alt="카카오 페이" class="txt-kakaopay" /></label>
                            </span>

                            <span class="radio-ui">
                                <input type="radio" id="payment_method_payco" runat="server" name="payment" class="css-radio" />
                                <label for="payment_method_payco" class="css-label">
                                    <img src="/common/img/icon/payco.png" alt="페이코" class="txt-payco" /></label>
                            </span>
							<!--
                            <span class="radio-ui">
                                <input type="radio" id="payment_method_oversea" runat="server" name="payment" class="css-radio" />
                                <label for="payment_method_oversea" class="css-label">해외발급 카드</label>
                            </span>
							-->
                            <span class="radio-ui">
                                <input type="radio" id="payment_method_phone" runat="server" name="payment" class="css-radio" />
                                <label for="payment_method_phone" class="css-label">휴대폰결제</label>
                            </span>
                        </span>
                    </fieldset>
                </div>
                <div class="wrap-bt mt10">
                    <asp:LinkButton runat="server" ID="btn_submit" class="bt-type6" Style="width: 100%" OnClick="btnPay_Click">바로 결제</asp:LinkButton></div>

                <div class="box-mywhite bottom-info">
                    <p class="txt-title">지연된 후원금 결제 안내</p>
                    <ul class="list-bu">
                        <li>후원금 결제 시 일부 카드사의 경우 결제 대행업체인 [NHN KCP 한국 사이버결제]로 승인 SMS가 전송됩니다.</li>
                        <li>지연된 후원금을 모두 결제하셔야 당월 후원금 결제가 가능합니다.</li>
                        <li>1:1리더십 결연 후원자님의 지연된 후원금 정보는 이 페이지에서 확인되지 않습니다. 
                		1:1리더십 결연 관련 문의는 한국컴패션으로 연락 주시기 바랍니다.<br />
                            후원지원팀 (<a href="tel:02-740-1000">02-740-1000</a> 평일 9시~18시/공휴일제외) / <a class="color1" href="mailto:info@compassion.or.kr">info@compassion.or.kr</a>
                        </li>
                    </ul>

                    <p class="txt-title mt30">지연된 후원금을 쉽게 납부하시는 방법</p>
                    <ul class="list-bu">
                        <li><strong>한국컴패션 홈페이지 이용</strong>
                            로그인&gt;마이컴패션&gt;후원 관리&gt;지연된 후원금 메뉴에서 결제 방법을 선택하시면 바로 결제하실 수 있습니다.</li>
                        <li><strong>전화 ARS 이용</strong>
                            <a href="tel:02-740-1000">02-740-1000</a>로 전화 걸기▶2번(후원금 결제 및 납부 내역 문의)▶1번(지연된 후원금 신용카드 결제)을 통해 지연된 후원금을 바로 결제하실 수 있습니다.</li>
                        <li><strong>전용 가상계좌 이용</strong>
                            홈페이지 또는 전화를 통해 후원자님만의 전용 가상계좌를 발급 받으셔서 지연된 후원금을 납부하실 수 있습니다.</li>
                    </ul>


                </div>
            </div>

        </div>

        <!--//-->
    </div>

</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">

    <uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" ID="kcp_form" />
    <uc:payco_form runat="server" EnableViewState="true" ViewStateMode="Enabled" ID="payco_form" />
    <uc:kakaopay_form runat="server" EnableViewState="true" ViewStateMode="Enabled" ID="kakaopay_form" />

</asp:Content>
