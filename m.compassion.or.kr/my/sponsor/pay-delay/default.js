﻿$(function () {

	var sly = new Sly($(".menu-my"), {    // Call Sly on frame

		horizontal: 1,
		itemNav: 'centered',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 3,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1

	});

	sly.init();

	$.each($(".menu-my a"), function (i) {
		if ($(this).hasClass("selected")) {
			sly.activate(i);
		}
	})

});

$(function () {

	$page.init();
	
})

var $page = {

	init : function(){
	

	} 

}
