﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_payment_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>
    <script type="text/javascript" src="/my/sponsor/payment/default.js"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    <style>
        #ui-datepicker-div {
            display: none;
        }
        .menu-my {
                display: block;
                position: relative;
                background: url(/common/img/page/my/bg-menu.gif) 0 bottom repeat-x #fff;
                background-size: 100% 6px;
                overflow: hidden;
                height: 70px;
            }
    </style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <div class="wrap-sectionsub-my" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <!--마이컴패션 디자인-->
        <!---->
        <div class="menu-my sectionsub-margin2">

            <ul style="position: absolute; top: 0;">
                <!--임시 inline style(수정가능)-->
                <li style="display: inline-block; width: 100px"><a href="/my/sponsor/commitment/">정기후원<br />신청내역</a></li>
                <li style="display: inline-block; width: 100px"><a class="selected" href="/my/sponsor/payment/">후원금<br />내역</a></li>
                <li style="display: inline-block; width: 100px"><a href="/my/sponsor/pay-delay/">지연된<br />후원금</a></li>
                <li style="display: inline-block; width: 100px"><a href="/my/sponsor/gift-money/">어린이<br />선물금</a></li>
                <li style="display: inline-block;"></li>
            </ul>

        </div>

        <div class="my-sponsor">

            <div class="box-mywhite search-month">
                <span class="radio-ui">
                    <input type="radio" id="period-month1" name="period-month" class="css-radio dateRange" checked data-day="0" data-month="1" />
                    <label for="period-month1" class="css-label">1개월</label>
                </span>
                <span class="radio-ui">
                    <input type="radio" id="period-month2" name="period-month" class="css-radio dateRange" data-day="0" data-month="2" />
                    <label for="period-month2" class="css-label">2개월</label>
                </span>
                <span class="radio-ui">
                    <input type="radio" id="period-month3" name="period-month" class="css-radio dateRange" data-day="0" data-month="3" />
                    <label for="period-month3" class="css-label">3개월</label>
                </span>
                <span style="display: none">
                    <input type="text" id="date_begin" class="input_type2 date begin" value="{{date_begin| date:'yyyy-MM-dd'}}" style="width: 130px" />
                    <button class="calendar btn_calendar" onclick="$('#date_begin').trigger('focus');return false;"></button>
                    ~&nbsp;&nbsp;
				<input type="text" id="date_end" class="input_type2 date end" value="{{date_end| date:'yyyy-MM-dd'}}" style="width: 130px" />
                    <button class="calendar btn_calendar " onclick="$('#date_end').trigger('focus');return false;"></button>
                </span>
                <a ng-click="search($event)" class="bt-type8">조회</a>
            </div>

            <div class="box-myblue support-amount">
                <p class="txt1">후원금 합계</p>
                <p class="txt-amount"><em>{{totalAmount | number:0}}</em>원</p>
            </div>

            <div class="box-mywhite list-mysupport style-add1">
                <div class="wrap-header">
                    <p class="txt-title">후원금 내역<em ng-if="total > -1">(총 {{total}}건)</em></p>
                </div>
                <ul id="l">
                    <li ng-repeat="item in list">
                        <p class="txt1">{{item.accountclassname}}</p>
                        <span class="txt4">{{item.accountclassdetail}}<span class="bar">|</span>{{item.paymethod}}<span ng-if="item.paymethoddetail">({{item.paymethoddetail}})</span></span>
                        <span class="txt2"><em>{{item.amount | number:0}}</em> 원</span>
                        <time class="txt3" datetime="{{item.paymentdate | date:'yyyy.MM.dd'}}">{{item.paymentdate | date:'yyyy.MM.dd'}}</time>
                    </li>


                    <!--후원 내역이 없는 경우 -->
                    <li class="no-result" ng-if="total == 0">검색하신 후원금 내역이 없습니다.
                    </li>
                </ul>
            </div>

            <paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>

            <div class="box-mywhite bottom-info">
                CMS계좌 자동이체의 경우 출금일로부터 1일 후, 지로의 경우 납부일로부터 2-5일 후 납부 내역이 확인됩니다. 후원금 납부 내역에 대한 문의사항이 있으시면 한국컴패션으로 연락 주시기 바랍니다.<br />
                <br />

                후원지원팀 (<a href="tel:02-740-1000">02-740-1000</a> 평일 9시~18시/공휴일제외) / <a class="color1" href="mailto:info@compassion.or.kr">info@compassion.or.kr</a>
            </div>


        </div>

        <!--//-->
    </div>

</asp:Content>
