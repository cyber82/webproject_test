﻿$(function () {

	var sly = new Sly($(".menu-my"), {    // Call Sly on frame

		horizontal: 1,
		itemNav: 'centered',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 3,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1

	});

	sly.init();

	$.each($(".menu-my a"), function (i) {
		if ($(this).hasClass("selected")) {
			sly.activate(i);
		}
	})

});

$(function () {

	setDatePicker($(".date"), function () {
		// dateValidate 가 있으면 호출안됨
	});

	$("#date_begin").dateRange({
		buttons: ".dateRange",	// preset range
		end: "#date_end",
		eventBubble : true ,
		onClick: function (r) {
		}
	});

	$("#date_begin").dateValidate({
		end: "#date_end",
		onSelect: function () {

		}
	});

});

(function () {
var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter) {

		$scope.total = null;
		
		$scope.list = [];
		$scope.params = {
			page: 1,
			rowsPerPage: 4,
			date_begin: "",
			date_end: ""
		};

		// 기본 날짜 세팅
		var date_begin = new Date();
		date_begin.setMonth(date_begin.getMonth() - 1);
		$scope.date_begin = date_begin;

		$scope.date_end = new Date();
		$("#date_begin").val($filter('date')($scope.date_begin, "yyyy-MM-dd"));
		$("#date_end").val($filter('date')($scope.date_end, "yyyy-MM-dd"));

		// list
		$scope.getList = function (params) {

			$scope.params.date_begin = $("#date_begin").val();
			$scope.params.date_end = $("#date_end").val();
			$scope.params = $.extend($scope.params, params);
			console.log($scope.params);

			$http.get("/api/my/payment.ashx?t=history", { params: $scope.params }).success(function (r) {
				console.log(r);
				if (r.success) {

					$scope.totalAmount = 0;
					var list = r.data;
					if (list.length > 0) {
						$scope.totalAmount = r.data[0].totalamount;
					}

					$.each(list, function () {
						this.paymentdate = new Date(this.paymentdate);
					});

					$scope.list = list;
					$scope.total = r.data.length > 0 ? r.data[0].total : 0;

				//	if (params)
				//		scrollTo($("#l"), 30);
				} else {

					if (r.action == "not_sponsor") {
						$scope.totalAmount = 0;
						$scope.total = 0;
					}
					//alert(r.message);
				}
			});


		}

		$scope.search = function ($event) {
			$event.preventDefault();
			$scope.params.page = 1;
			$scope.getList();
		}

		$scope.getList();

	});

})();