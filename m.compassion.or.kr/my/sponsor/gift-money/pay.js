﻿$(function () {

	var sly = new Sly($(".menu-my"), {    // Call Sly on frame

		horizontal: 1,
		itemNav: 'centered',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 3,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1

	});

	sly.init();

	$.each($(".menu-my a"), function (i) {
		if ($(this).hasClass("selected")) {
			sly.activate(i);
		}
	})

	$page.init();

});

var goBack = function () {
	location.href = "/my/sponsor/gift-money/";
}

var $page = {

	init: function () {

		if ($("#hash").val() != "") {
			location.href = "#" + $("#hash").val();
			//	scrollTo($("#l"));
		}
		
		/*
		$("#chkAll").click(function () {
			var checked = $(this).prop("checked");
			$(".chk").prop("checked", checked);
			$.each($(".chk") , function(){
				var checked = $(this).prop("checked");
				var index = $(this).data("index");
				if (checked)
					$("#li" + index).addClass("selected")
				else
					$("#li" + index).removeClass("selected")
			})
		});
		
		$(".chk").click(function () {
			var checked = $(this).prop("checked");
			var exist_year = $(this).data("exist-year");		// 예약여부 , Y or N
			var commit_count = $(this).data("commit-count");		// 결제여부 , tcommitmentMaster 에 등록된 횟수
			var index = $(this).data("index");
			
			if ($("#rdoOnce").prop("checked")) {
				if (commit_count >= 2) {
					alert("예약을 포함하여 2건만 결제가능합니다.");
					return false;
				}
			} else if ($("#rdoYear").prop("checked")) {
				if (exist_year == "Y") {
					alert("이미 예약된 어린이 입니다.");
					return false;
				}
			}

			if (checked)
				$("#li" + index).addClass("selected")
			else
				$("#li" + index).removeClass("selected")


		})
		
		*/

		$(".raGubun").change(function () {
			var id = $(".raGubun:checked").attr("id");
			if (id == "rdoOnce") {
				$(".pn_payYear").hide();
				$("#pn_payment").show();
				$("#btn_submit").html("결제");

				$("#month_container").hide();

			} else {
				$(".pn_payYear").show();
				$("#pn_payment").hide();
				$("#btn_submit").html("예약");

				if ($("#ddlMonth").length > 0)
					$("#month_container").show();

			}

			// postback 시 이미 예약됬다는 문구가 노출되는것 방지
			
			if ($("#hdCompleted").val() == "N") {
				$(".chk").each(function () {

					var checked = $(this).prop("checked");
					var exist_year = $(this).data("exist-year");		// 예약여부 , Y or N
					var commit_count = $(this).data("commit-count");		// 결제여부 , tcommitmentMaster 에 등록된 횟수
					var index = $(this).data("index");

					if (checked && $("#gift_type").val() != "CF") {
						if ($("#rdoOnce").prop("checked")) {
							if (commit_count >= 2) {
								alert("예약을 포함하여 2건만 결제가능합니다.");
								$(this).prop("checked", false);
								$("#li" + index).removeClass("selected")
								return false;
							}
						} else if ($("#rdoYear").prop("checked")) {
							if (exist_year == "Y") {
								alert("이미 예약된 어린이 입니다.");
								$(this).prop("checked", false);
								$("#li" + index).removeClass("selected")
								return false;
							}
						}

					}
				});
			}

		})

		$(".raGubun").trigger("change");


		if ($("#rdoOnce").prop("checked")) {
			$("#pn_payment").show();
		}

		$("#pay_amount").change(function () {

			if ($(this).val() == "custom") {
				$(".pn_pay_amount_custom").show();
				$("#pay_amount_custom").focus();
			} else {
				$(".pn_pay_amount_custom").hide();
				$("#pay_amount_custom").val("");
				
			}
		});

		if ($("#gift_type").val() == "BG") {
		    $(".pn_BG").show();
		} else if ($("#gift_type").val() == "GG") {
		    $(".pn_GG").show();
		} else if ($("#gift_type").val() == "FG") {
		    $(".pn_FG").show();
		} else if ($("#gift_type").val() == "PG") {
		    $(".pn_PG").show();
		} else if ($("#gift_type").val() == "CF") {
		    $(".pn_CF").show();
		}


		$("#pay_amount").trigger("change");

	},

	// 확인
	onSubmit: function () {

		if ($(".chk.selected").length < 1 && $("#gift_type").val() != "CF") {
			alert("선물금을 보내실 어린이를 선택해주세요");
			return false;
		}

		if ($("#pay_amount").val() == "") {
			alert("금액을 선택해주세요.");
			return false;
		}

		if ($("#pay_amount").val() == "custom") {
			var obj = $("#pay_amount_custom");
			var val = obj.val();
			var min = $("#pay_amount option:eq(1)").val();
			var min_text = $("#pay_amount option:eq(1)").text();

			if (val == "") {
				alert("금액을 입력해주세요");
				obj.focus();
				return false;
			}

			if (isNaN(val) || parseInt(val) < min) {
				alert(min_text + " 이상 입력해주세요");
				obj.val("");
				obj.focus();
				return false;
			}

			if (parseInt(val) % 1000 > 0) {
				alert("천원단위로 입력해 주세요");
				obj.val("");
				obj.focus();
				return false;
			}

		}

		// 정기 조건 
		var childMasterIds = "";
		if ($("#rdoYear").prop("checked")) {

			var targets = "";
			$.each($(".chk.selected"), function () {

				var reservation = $(this).data("exist-year");
				if (reservation == "Y") {
					targets += "," + $(this).data("name");
				} else {
					childMasterIds += "," + $(this).data("childmasterid");
				}
				
			})

			if (targets != "") {
				alert(targets.substring(1) + " 어린이는 이미 선물금이 예약되어 있습니다.");
				return false;
			}
		}

		// 일시 조건
		if ($("#rdoOnce").prop("checked")) {

			var targets = "";
			$.each($(".chk.selected"), function () {
				var cnt = $(this).data("commit-count");
				if (cnt && cnt >= 2) {
					targets += ","+$(this).data("name");
				} else {
					childMasterIds += ","+$(this).data("childmasterid");
				}
			})

			if (targets != "") {
				alert(targets.substring(1) + " 어린이는 선물금 결제횟수를 초과했습니다.(예약포함)");
				return false;
			}
		}

		if ($("#gift_type").val() == "CF") {
			$("#hdChildMasterIds").val("0000000000");
		} else {
			$("#hdChildMasterIds").val(childMasterIds.substring(1));
		}
		
		
		return true;
	}

};

(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		$scope.total = -1;
		
		$scope.data = [];
		$scope.list = null;
		$scope.countries = null;
		$scope.children = null;
		$scope.country = "";
		$scope.child = "";

		$scope.params = {
			page: 1,
			rowsPerPage: 4
		};

		// list
		$scope.getList = function (params) {
			
			var data = $.extend($scope.data, $.parseJSON($("#hdChildren").val()));

			// 어린이가 지정된 경우 , 선물금 가능한 어린이만 선택되기 때문에, 선물금이 불가한 경우는 전체 목록 노출
			var childMasterId = getParameterByName("childMasterId");
			
			if (childMasterId) {
				var data2 = $.grep(data, function (r) {
					return r.ChildMasterID == childMasterId;
				});

				if (data2.length > 0) {
					data2[0].checked = true;
					data = data2;
				}
			}

			$scope.data = data;
			console.log($scope.data);
			$scope.total = $scope.data.length > 0 ? $scope.data[0].total : 0;

			$.each($scope.data, function () {
					/*
				var yyyy = this.CommitmentID.substr(0, 4);
				var mm = this.CommitmentID.substr(4, 2);
				var dd = this.CommitmentID.substr(6, 2);
				this.commit_date = new Date(mm + "/" + dd + "/" + yyyy);
					*/
				this.commit_date = new Date(this.startdate.replace(/-/g, '/').substring(0, 10));
			});

			var countries = $.parseJSON($("#hdCountries").val());
			$scope.countries = countries;
			console.log("countries", $scope.countries);

			$scope.showList(true);

		}

		$scope.showList = function (reload) {
			var begin = ($scope.params.page - 1) * $scope.params.rowsPerPage;
			var end = ($scope.params.page) * $scope.params.rowsPerPage;
			var list = $scope.data.slice(begin, end);
			if (reload) {
				$scope.list = [];
				$scope.total = $scope.data.length;
			}

			$scope.list = $.merge($scope.list, list);

		};

		$scope.more = function ($event) {
			$event.preventDefault();
			$scope.params.page++;
			$scope.showList(false);
		};

		$scope.chkAll = function () {
			var checked = $("#chkAll").prop("checked");
			$.each($scope.data, function () {
				this.checked = checked;
			})

		}

		$scope.chk = function (item) {
			
			var checked = !item.checked;
			if (checked) {
				var exist_year = item.exist_year;
				var commit_count = item.commit_count;

				if ($("#rdoOnce").prop("checked")) {
					if (commit_count >= 2) {
						alert("예약을 포함하여 2건만 결제가능합니다.");
						return false;
					}
				} else if ($("#rdoYear").prop("checked")) {
					if (exist_year == "Y") {
						alert("이미 예약된 어린이 입니다.");
						return false;
					}
				}
			}

			item.checked = !item.checked;

		}

		$scope.onSubmit = function ($event) {

			var list = $.grep($scope.data, function (r) {
				return r.checked;
			});

			if ($("#gift_type").val() == "") {
				alert("선물금 종류를 선택해 주세요");
				$event.preventDefault();
				return;
			}
			if (list.length < 1 && $("#gift_type").val() != "CF") {
				alert("선물금을 보내실 어린이를 선택해주세요");
				$event.preventDefault();
				return;
			}

			if ($("#pay_amount").val() == "") {
				alert("금액을 선택해주세요.");
				$event.preventDefault();
				return;
			}

			if ($("#pay_amount").val() == "custom") {
				var obj = $("#pay_amount_custom");
				var val = obj.val();
				var min = $("#pay_amount option:eq(1)").val();
				var min_text = $("#pay_amount option:eq(1)").text();
				var max = $("#pay_amount option:eq(" + ($("#pay_amount option").size() - 2) + ")").val();
				var max_text = $("#pay_amount option:eq(" + ($("#pay_amount option").size() - 2) + ")").text();
				var gift_name = $("#gift_type option:selected").text();

				if (val == "") {
					alert("금액을 입력해주세요");
					obj.focus();
					$event.preventDefault();
					return;
				}

				if (isNaN(val) || parseInt(val) < min) {
					alert(gift_name + "은 최소 " + min_text + " 이상 입력해주세요");
					obj.val("");
					obj.focus();
					$event.preventDefault();
					return;
				}

				if (parseInt(val) > max && $("#gift_type").val() != "CF") {
					alert(gift_name + "은 최대 " + max_text + " 이하 입력해주세요");
					obj.val("");
					obj.focus();
					$event.preventDefault();
					return;
				}

				if (parseInt(val) % 1000 > 0) {
					alert("천원단위로 입력해 주세요");
					obj.val("");
					obj.focus();
					$event.preventDefault();
					return;
				}

			}

			// 정기 조건 
			var childMasterIds = "";
			if ($("#rdoYear").prop("checked")) {

				var targets = "";
				$.each(list, function () {
					var reservation = this.exist_year;
					if (reservation == "Y") {
						targets += "," + this.NameKr;
					} else {
						childMasterIds += "," + this.ChildMasterID;

					}

				})



				if (targets != "") {
					alert(targets.substring(1) + " 어린이는 이미 선물금이 예약되어 있습니다.");
					$event.preventDefault();
					return;
				}
			}


			// 일시 조건
			if ($("#rdoOnce").prop("checked")) {

				var targets = "";
				$.each(list, function () {

					var cnt = this.commit_count;
					if (cnt && cnt >= 2) {
						targets += "," + this.NameKr;
					} else {
						childMasterIds += "," + this.ChildMasterID;
					}
				})

				if (targets != "") {
					alert(targets.substring(1) + " 어린이는 선물금 결제횟수를 초과했습니다.(예약포함)");
					$event.preventDefault();
					return;
				}
			}

			if ($("#gift_type").val() == "CF") {
				$("#hdChildMasterIds").val("0000000000");
			} else {
				$("#hdChildMasterIds").val(childMasterIds.substring(1));
			}



		}

		$scope.getList();

	});

})();
