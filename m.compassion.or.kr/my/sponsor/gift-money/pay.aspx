﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pay.aspx.cs" Inherits="my_gift_money_pay" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>
<%--<%@ Register Src="/sponsor/pay/temporary/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/sponsor/pay/temporary/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/sponsor/pay/temporary/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>--%>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/sponsor/gift-money/pay.js?v=2"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
	<style>
        .menu-my {
                display: block;
                position: relative;
                background: url(/common/img/page/my/bg-menu.gif) 0 bottom repeat-x #fff;
                background-size: 100% 6px;
                overflow: hidden;
                height: 70px;
            }
    </style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	<input type="hidden" id="hdCompleted" runat="server" value="N" />
	<input type="hidden" id="hdChildren" runat="server" />
	<input type="hidden" id="hdCountries" runat="server" />
	<input type="hidden" id="hdChildMasterIds" runat="server" value="" />
	

	<input type="hidden" runat="server" id="hash" value="" />
	<input type="hidden" runat="server" id="gubun" value="" />
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />
	<input type="hidden" runat="server" id="baby_gender" value="" />

	<input type="hidden" id="gender" runat="server"  />
	<input type="hidden" id="hd_auth_domain" runat="server"  />
	<input type="hidden" id="hd_age" runat="server"  />

	<input type="hidden" runat="server" id="jumin" value="" />		
	<input type="hidden" runat="server" id="ci" value="" />		<!-- 본인인증 CI -->
	<input type="hidden" runat="server" id="di" value="" />		<!-- 본인인증 DI -->
	<input type="hidden" runat="server" id="cert_gb" value="" />		<!-- 본인인증 수단 -->
	
	<input type="hidden" runat="server" id="user_class" value="20세이상" />
	<input type="hidden" runat="server" id="hdState" value="" />
	<input type="hidden" runat="server" id="hdLocation" value="" />
	<input type="hidden" runat="server" id="hdManageType" value="" />
	<input type="hidden" runat="server" id="hfAddressType" value="" />		<!-- 회원 기본주소와 동기화 하는 기능이 있는경우 hfAddressType 가 필요함 -->
	<input type="hidden" runat="server" id="hdCI" value="" />	<!-- 기존 등록된 본인인증 CI -->
	
	
	<input type="hidden" runat="server" id="exist_account" value="N" />		<!-- 정기결제 계좌가 있는지 여부 -->
	<input type="hidden" runat="server" id="amount" value="0" />		<!-- 선택된 금액 -->
	<input type="hidden" runat="server" id="child_count" value="0" />		
	<input type="hidden" runat="server" id="ready_count" value="0" />		
	<input type="text" runat="server" id="user_name" style="display:none" />		<!-- 결제시 사용 -->

	<asp:PlaceHolder runat="server" Visible="false">
		<input type="checkbox" runat="server" id="addr_overseas" />해외에 거주하고 있어요
		연말정산영수증 신청 <input type="checkbox" id="p_receipt_pub_ok" name="p_receipt_pub" runat="server" checked="true" /> 

	</asp:PlaceHolder>

<div class="wrap-sectionsub-my" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">    <!--마이컴패션 디자인-->

    <div class="menu-my sectionsub-margin2">


        <ul style="position: absolute; top: 0;">
            <!--임시 inline style(수정가능)-->
            <li style="display: inline-block; width: 100px"><a href="/my/sponsor/commitment/">정기후원<br />신청내역</a></li>
            <li style="display: inline-block; width: 100px"><a href="/my/sponsor/payment/">후원금<br />내역</a></li>
            <li style="display: inline-block; width: 100px"><a href="/my/sponsor/pay-delay/">지연된<br />후원금</a></li>
            <li style="display: inline-block; width: 100px"><a class="selected" href="/my/sponsor/gift-money/">어린이<br />선물금</a></li>
            <li style="display: inline-block;"></li>
        </ul>
        
    </div>
    
     <div class="submenu-my sectionsub-margin2">
    	
        <ul style="position:absolute;left:60px;top:0;">  <!--임시 inline style(수정가능)-->
            <li style="display:inline-block"><a href="/my/sponsor/gift-money/">어린이 선물금</a></li>
            <li style="display:inline-block"><a class="selected" href="/my/sponsor/gift-money/pay/">선물금 보내기</a></li>
        </ul>
        
    </div>

    <div class="my-sponsor">
    	
         <div class="toptxt-giftsend sectionsub-margin2" ID="lblTypeStr" runat="server" >
         	<strong>생일선물금의 경우 </strong> <span class="color2">어린이</span>
            <span class="color2">생일 1개월</span> 전에 보내주시기를 권해 드립니다.<br /> 권장시기
            이후에 납부하시더라도 ‘생일 선물금’으로 어린이에게<br />
            정확히 전해드립니다. 
         </div>

		<div class="toptxt-giftsend sectionsub-margin2" ID="lblTypeStr2" runat="server"  >
         	<strong>크리스마스 선물금의 경우 </strong>
            <span class="color2">9월</span>에 예약해 주시면 어린이가 크리스마스에 맞추어<br />
			선물을 전달받아 행복한 크리스마스를 보낼 수 있습니다. 
         </div>

		<div class="toptxt-giftsend sectionsub-margin2" ID="lblFirstNo" runat="server" visible="false" >
         	<strong>첫 후원금이 납부된 이후에 결제하실 수 있습니다</strong>
            
         </div>


        <div class="box-mywhite mt20">
        	<div class="wrap-header">
            	<p class="txt-title">선물금 종류 선택</p>
            </div>
			<asp:DropDownList runat="server" ID="gift_type" style="width:100%" class="mt20" AutoPostBack="true" OnSelectedIndexChanged="gift_type_SelectedIndexChanged">
				<asp:ListItem Text="생일 선물금" Value="BG"></asp:ListItem>
				<asp:ListItem Text="어린이 선물금" Value="GG"></asp:ListItem>
				<asp:ListItem Text="가족 선물금" Value="FG"></asp:ListItem>
				<asp:ListItem Text="어린이센터 선물금" Value="PG"></asp:ListItem>
				<asp:ListItem Text="크리스마스 선물금" Value="CF"></asp:ListItem>
			</asp:DropDownList>
			
			<div class="member-join " style="margin-bottom:0">
				 <ul class="list-bullet2">
        			<li><asp:Literal runat="server" ID="lb_range" /></li>
				</ul>
			</div>
        </div>
        
		<div runat="server" id="pn_children">
        <div class="box-mywhite mt20">
        	<div class="wrap-header">
            	<p class="txt-title">어린이 선택</p>
                <span class="more fr checkbox-ui" style="padding:5px 0 0 0;font-size:13px;letter-spacing:-1px">
                    <input type="checkbox" id="chkAll" ng-click="chkAll()" class="css-checkbox" />
                    <label for="chkAll" class="css-label">후원 중인 모든 어린이 선택</label>
                </span>
            </div>
        </div>
        <ul class="list-giftchild">
        	<li ng-repeat="item in list" id="li{{$index}}" ng-class="item.checked ? 'selected' : ''">       <!--선택 row Css-->				
            	<span class="box-mywhite">
                    <span class="row">
                        <p class="txt-id fl">어린이 ID : {{item.ChildKey}}</p>
                        <p class="txt-state fr"  ng-if="item.exist_year == 'Y'">선물금 예약중</p>
                    </span>
                    <span class="row align-padding">
                        <span class="pos-photo"><span class="photo" style="background:url('{{item.pic}}') no-repeat"></span></span>
                        <p class="txt-name">{{item.NameKr}}</p>
                        <p class="txt-nation">국가 : {{item.CountryCode}}</p>
                    </span>
                    <span class="checkbox2-ui">
						<input type="checkbox" id="chk{{$index}}" ng-click="chk(item)" ng-checked="{{item.checked}}" data-index="{{$index}}" ng-class="item.checked ? 'selected' : ''" class="css-checkbox2 chk" data-exist-year="{{item.exist_year}}" data-commit-count="{{item.commit_count}}" data-childMasterId="{{item.ChildMasterID}}"  data-name="{{item.NameKr}}" />
                        <label for="chk{{$index}}" class="css-label2">선택 </label>
                    </span>
                </span>
            </li>
          
        </ul>
        
         <div class="box-myblue link-childmore"><a href="#" ng-show="params.page * params.rowsPerPage < total" ng-click="more($event);"><span>더 많은 어린이들 보기</span></a></div>
		</div>
        
<!--옵션에 따른 자세한 화면 IA는 기획자분한테 문의바랍니다.-->        
        <div class="box-mywhite period-giftsend">
            <div class="wrap-caption">
            	<p class="txt-title">선물금 결제 주기 </p>
            </div>
            <div class="frm-row">
            	<span class="radio-ui">
                    <input type="radio" runat="server" id="rdoOnce" class="raGubun css-radio"  name ="raGubun" />
                    <label for="rdoOnce" class="css-label">바로결제</label>
                </span>&nbsp;&nbsp;&nbsp;&nbsp;
				
                <span class="radio-ui" runat="server" id="pn_regular">
					<input type="radio" runat="server" id="rdoYear" class="raGubun css-radio" checked="true"  name ="raGubun"/>
                    <label for="rdoYear" class="css-label">매년결제예약</label>
                </span>
            </div>
            <p class="txt-guide">
            	<strong>선물금 바로결제</strong>
                선물금 예약은 정기 결제수단을 등록한 후 이용하실 수 있습니다.
				
                <!--위에 "매년결제예약"  옵션 선택시-->
                <!--예약하신 선물금은 후원금 자동이체 계좌, 신용카드에서 출금됩니다. -->
            </p>
		</div>
        
        <div class="box-mywhite period-giftsend mt10">
        	<span class="row-table" runat="server" id="month_container">
            	<span class="th-field" style="width:20%">매년</span>
                <span class="td-field" style="width:80%"><asp:DropDownList ID="ddlMonth" runat="server" Enabled="true" Visible="false" style="width:100%">
						<asp:ListItem Selected="True" Value="01">1월</asp:ListItem>     
						<asp:ListItem Value="02">2월</asp:ListItem>
						<asp:ListItem Value="03">3월</asp:ListItem>      
						<asp:ListItem Value="04">4월</asp:ListItem>
						<asp:ListItem Value="05">5월</asp:ListItem>
						<asp:ListItem Value="06">6월</asp:ListItem>
						<asp:ListItem Value="07">7월</asp:ListItem>
						<asp:ListItem Value="08">8월</asp:ListItem>
						<asp:ListItem Value="09">9월</asp:ListItem>
						<asp:ListItem Value="10">10월</asp:ListItem>
						<asp:ListItem Value="11">11월</asp:ListItem>
						<asp:ListItem Value="12">12월</asp:ListItem>
					</asp:DropDownList>
					</span>
            </span>
            <span class="row-table mt20">
            	<span class="th-field" style="width:20%">금액</span>
                <span class="td-field" style="width:80%">
                	<span class="row"><asp:DropDownList runat="server" ID="pay_amount" style="width:100%"><asp:ListItem Value="" Text="선택하세요"></asp:ListItem></asp:DropDownList></span>
                    <span class="row pn_pay_amount_custom" style="display:none;"><input type="text" runat="server" id="pay_amount_custom" maxlength="8" class="fc_money number_only" style="width:100%"  value="" placeholder="직접입력" /></span>
			    </span>
            </span>
			<p class="txt-guide mt10 pn_payYear" style="display:none">예약하신 선물금은 후원금 자동이체 계좌, 신용카드에서 출금됩니다.</p>
            <p class="txt-guide mt10 pn_payYear" style="display:none">생일 선물금은 어린이 생일 1개월 전, 크리스마스 선물금은 9월로 자동예약됩니다.</p>
            <p class="txt-guide mt10 pn_BG" style="display:none;">생일선물금은 최소 1만 5천원~ 최대 10만원까지를 추천합니다.</p>
			<p class="txt-guide mt10 pn_GG" style="display:none;">어린이에게 전하는 특별한 선물금은 최소 1만 5천원~ 최대 10만원까지 연에 1~2회를 추천합니다.</p>
			<p class="txt-guide mt10 pn_FG" style="display:none;">어린이 가족에게 전하는 마음의 선물금은 최소 3만 5천원~ 최대 100만원까지 권장합니다. (연에 100만원까지 가능합니다.)</p>
			<p class="txt-guide mt10 pn_PG" style="display:none;">어린이가 등록된 어린이센터에 전하는 모두를 위한 선물금은 최소 15만원~ 최대 200만원까지 권장합니다. (연에 200만원까지 가능합니다.)</p>
            <p class="txt-guide mt10 pn_CF" style="display:none;">크리스마스 선물금은 컴패션에 등록된 모든 어린이를 위한 선물금으로, 한 어린이당 2만원의 선물금을 추천합니다.</p>

        </div>
        
        <!--결제방법 (바로결제 시)-->
        <div class="box-mywhite delay-c​ontribution mt10" id="pn_payment" runat="server" style="display:none">
        	<div class="wrap-header">
            	<p class="txt-title">결제방법</p>
            </div>
            <fieldset class="frm-input">
            	<legend>결제수단 입력</legend>
                <span class="row">
					<span class="radio-ui">
						<input type="radio" id="payment_method_card" runat="server" name="payment" class="css-radio" checked />
						<label for="payment_method_card" class="css-label">신용카드</label>
					</span>
					<span class="radio-ui">
						<input type="radio" id="payment_method_cms" runat="server" name="payment" class="css-radio" />
						<label for="payment_method_cms" class="css-label">실시간 계좌이체</label>
					</span>
					<span class="radio-ui">
						<input type="radio" id="payment_method_kakao" runat="server" name="payment" class="css-radio" />
						<label for="payment_method_kakao" class="css-label"><img src="/common/img/icon/kakaopay.png" alt="카카오 페이" class="txt-kakaopay" /></label>
					</span>
                
					<span class="radio-ui">
						<input type="radio" id="payment_method_payco" runat="server" name="payment" class="css-radio" />
						<label for="payment_method_payco" class="css-label"><img src="/common/img/icon/payco.png" alt="페이코" class="txt-payco" /></label>
					</span>
					<!--
					<span class="radio-ui">
						<input type="radio" id="payment_method_oversea" runat="server" name="payment" class="css-radio" />
						<label for="payment_method_oversea" class="css-label">해외발급 카드</label>
					</span>
					-->
					<span class="radio-ui">
						<input type="radio" id="payment_method_phone" runat="server" name="payment" class="css-radio" />
						<label for="payment_method_phone" class="css-label">휴대폰결제</label>
					</span>
                </span>
			</fieldset>
		</div>
        
        <!--바로결제 시-->
        <div class="wrap-bt mt20"><asp:LinkButton runat="server" ID="btn_submit" ng-click="onSubmit($event);" OnClick="btn_submit_Click" class="bt-type6" style="width:100%">결제</asp:LinkButton></div>
        
        
    </div>
    
<!--//-->
</div>

</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form" />
	<uc:payco_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="payco_form" />
	<uc:kakaopay_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kakaopay_form" />
</asp:Content>