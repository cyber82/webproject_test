﻿$(function () {

	var sly = new Sly($(".menu-my"), {    // Call Sly on frame

		horizontal: 1,
		itemNav: 'centered',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 3,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1

	});

	sly.init();

	$.each($(".menu-my a"), function (i) {
		if ($(this).hasClass("selected")) {
			sly.activate(i);
		}
	})

});

(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		$scope.giftable = false;		// 선물금가능한지 여부

		$scope.entity = null;
		$scope.countries = null;
		$scope.children = null;
		$scope.country = "";
		$scope.child = "";

		$scope.params = {
			page: $scope.page,
			rowsPerPage: $scope.rowsPerPage
		};

		// 선물금예약/결제 가능한지 여부 확인
		$scope.checkGiftable = function () {

			$http.get("/api/my/payment.ashx?t=gift-giftable", { params: {} }).success(function (r) {

				if (r.success) {
					if (r.data > 0)
						$scope.giftable = true;

				} else {
					
				}
			});

		}

		// 선물금예약내역
		$scope.gr_total = -1;
		$scope.gr_page = 1;
		$scope.gr_rowsPerPage = 3;
		$scope.gr_list = [];

		$scope.getGiftReservationList = function (page) {

			var params = {  page: page, rowsPerPage: $scope.gr_rowsPerPage };

			$http.get("/api/my/payment.ashx?t=gift-reservation", { params: params }).success(function (r) {

				if (r.success) {
					console.log("선물금예약내역" , r.data);
					var list = $.extend([], r.data);

					$.each(r.data, function () {
						this.startdate = new Date(this.startdate);
					});

					// 더보기인경우 merge
					$scope.gr_list = list;
					$scope.gr_total = list.length > 0 ? list[0].total : 0;

					$scope.gr_page = params.page;		// 현재 페이지 갱신해야 함

				} else {
					if (r.action == "not_sponsor") {
						
						$scope.gr_total = 0;
					}
				}
			});

		}

		// 선물금 결제내역
		$scope.gp_total = -1;
		$scope.gp_page = 1;
		$scope.gp_rowsPerPage = 3;
		$scope.gp_list = [];
		$scope.getGiftPaymentList = function (page) {
			
			var params = {  page: page, rowsPerPage: $scope.gp_rowsPerPage };

			$http.get("/api/my/payment.ashx?t=gift-payment", { params: params }).success(function (r) {
				
				if (r.success) {

					var list = $.extend([], r.data);
					console.log("payment", list);

					$.each(r.data, function () {
						this.paymentdate = new Date(this.paymentdate);
					});

					// 더보기인경우 merge
					$scope.gp_list = list;
					$scope.gp_total = list.length > 0 ? list[0].total : 0;

					$scope.gp_page = params.page;		// 현재 페이지 갱신해야 함

				} else {
					if (r.action == "not_sponsor") {

						$scope.gp_total = 0;
					}
				}
			});

		}

		$scope.deleteGiftReservation = function (commitmentid) {

			if (!confirm("선물금 예약을 취소하고 예약정보를 삭제하시겠어요?")) {
				return false;
			}
	
			$http.post("/api/my/payment.ashx?t=gift-delete-reservation", { c: commitmentid }).success(function (r) {

				if (r.success) {

					$scope.getGiftReservationList(1);

				} else {
					alert(r.message);
				}
			});

		}


		$scope.goUpdate = function ($event, item) {
			$event.preventDefault();
			location.href = "/my/sponsor/gift-money/update-reservation/?c=" + item.commitmentid;
		}

		$scope.checkGiftable();

		$scope.getGiftReservationList(1);

		$scope.getGiftPaymentList(1);

		
	});

})();