﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_gift_money_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/my/sponsor/gift-money/default.js"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    <style>
        .menu-my {
                display: block;
                position: relative;
                background: url(/common/img/page/my/bg-menu.gif) 0 bottom repeat-x #fff;
                background-size: 100% 6px;
                overflow: hidden;
                height: 70px;
            }
    </style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub-my" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <!--마이컴패션 디자인-->

        <div class="menu-my sectionsub-margin2">

            <ul style="position: absolute; top: 0;">
                <!--임시 inline style(수정가능)-->
                <li style="display: inline-block; width: 100px"><a href="/my/sponsor/commitment/">정기후원<br />신청내역</a></li>
                <li style="display: inline-block; width: 100px"><a href="/my/sponsor/payment/">후원금<br />내역</a></li>
                <li style="display: inline-block; width: 100px"><a href="/my/sponsor/pay-delay/">지연된<br />후원금</a></li>
                <li style="display: inline-block; width: 100px"><a class="selected" href="/my/sponsor/gift-money/">어린이<br />선물금</a></li>
                <li style="display: inline-block;"></li>
            </ul>

        </div>

        <div class="submenu-my sectionsub-margin2">

            <ul style="text-align:center;">
                <!--임시 inline style(수정가능)-->
                <li style="display: inline-block"><a class="selected" href="/my/sponsor/gift-money/">어린이 선물금</a></li>
                <li style="display: inline-block"><a href="/my/sponsor/gift-money/pay/">선물금 보내기</a></li>
            </ul>

        </div>

        <div class="my-sponsor">

            <div class="gift-child sectionsub-margin2">
                <strong>선물금이 뭔가요?</strong>
                후원자님의 마음을 담아 선물금을 보내주세요. 보내주신 선물금으로 어린이와 가족, 어린이센터 교사가 함께 의논해 어린이가 가장 원하고 가장 필요한 선물을 준비하게 됩니다.
            </div>

            <div class="giftmoney">
                <div class="box-myblue ">
                    <div class="wrap-header">
                        <p class="txt-title">선물금 예약 내역</p>
                        <a class="bt-type2 more" href="/my/sponsor/gift-money/pay/?t=regular" ng-if="giftable" runat="server" id="btn_reservation">선물금 예약하기</a>
                    </div>
                </div>
                <div class="box-mywhite">
                    <ul class="list-gift">
                        <li ng-repeat="item in gr_list">
                            <span class="row txt-name">{{item.namekr}}<span class="ar">{{item.startdate | date:'yyyy.MM.dd'}}</span>
                            </span>
                            <span class="row txt-period">{{item.sponsoritem}}<span class="bar">|</span>{{item.fundingfrequency}} {{item.sponsormonth}}월
                            </span>
                            <span class="row txt-price">
                                <em>{{item.sponsoramount | number:0}}</em> 원<span class="ar"><a ng-click="goUpdate($event , item);" class="modify">수정</a><a ng-click="deleteGiftReservation(item.commitmentid)" class="delete">삭제</a></span>
                            </span>
                        </li>


                        <!--내용없는경우-->
                        <li class="no-result" ng-if="gr_total == 0">
                            <span class="img">
                                <img src="/common/img/icon/gift2.png" alt="" width="45" /></span>
                            등록된 선물금 예약 정보가 없습니다.
                        </li>
                    </ul>
                </div>

                <paging class="small" page="gr_page" page-size="gr_rowsPerPage" total="gr_total" show-prev-next="true" show-first-last="true" paging-action="getGiftReservationList(page)"></paging>
            </div>


            <div class="giftmoney">
                <div class="box-myblue ">
                    <div class="wrap-header">
                        <p class="txt-title">선물금 결제 내역</p>
                        <a class="bt-type2 more" href="/my/sponsor/gift-money/pay/?t=temporary">선물금 보내기</a>
                    </div>
                </div>
                <div class="box-mywhite">
                    <ul class="list-gift">
                        <li ng-repeat="item in gp_list">
                            <span class="row txt-name">{{item.namekr}}<span class="ar">{{item.paymentdate | date:'yyyy.MM.dd'}}</span>
                            </span>
                            <span class="row txt-period">{{item.sponsoritem}}
                            </span>
                            <span class="row txt-price">
                                <em>{{item.sponsoramount | number:0}}</em> 원
                            </span>
                        </li>


                        <!--내용없는경우-->
                        <li class="no-result" ng-if="gp_total == 0">
                            <span class="img">
                                <img src="/common/img/icon/gift3.png" alt="" width="38" /></span>
                            등록된 선물금 내용이 없습니다.<br />
                            어린이에게 선물을 보내 보세요.
                        </li>
                    </ul>
                </div>

                <paging class="small" page="gp_page" page-size="gp_rowsPerPage" total="gp_total" show-prev-next="true" show-first-last="true" paging-action="getGiftPaymentList(page)"></paging>
            </div>


            <div class="box-mywhite bottom-info">
                기타 문의는 한국컴패션으로 연락 주시기 바랍니다.<br />
                Tel : <a href="tel:02-740-1000">02-740-1000</a><br />
                후원지원팀 (<a href="tel:02-740-1000">02-740-1000</a> 평일 9시~18시/공휴일제외) / <a class="color1" href="mailto:info@compassion.or.kr">info@compassion.or.kr</a>
            </div>


        </div>

        <!--//-->
    </div>

</asp:Content>
