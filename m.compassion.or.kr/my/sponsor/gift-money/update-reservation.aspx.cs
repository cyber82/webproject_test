﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class my_gift_money_update_reservation : MobileFrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	public override bool RequireLogin {
		get {
			return true;
		}
	}
	WWWService.Service _wwwService = new WWWService.Service();
	public WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		//this.ViewState["listUrl"] = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();
		
		if(string.IsNullOrEmpty(Request.QueryString["c"])) {
			Response.Redirect("/");
			return;
		}

		this.ViewState["commitmentId"] = Request.QueryString["c"].EmptyIfNull();
		//Response.Write(this.ViewState["commitmentId"].ToString());
		this.GetReservation();

	}


	void GetReservation() {

		try {

			Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId" , "commitmentId" };
			Object[] objValue = new object[] { 1, 1, new UserInfo().SponsorID , ViewState["commitmentId"].ToString() };
			Object[] objSql = new object[] { "sp_web_present_reservation_list_f" };
			DataTable list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];
			
			if(list.Rows.Count < 1) {
				Response.Redirect("/");
				return;
			}

			var childMasterId = list.Rows[0]["ChildMasterID"].ToString();
			var SponsorAmount = list.Rows[0]["SponsorAmount"].ToString();
			var SponsorMonth = list.Rows[0]["SponsorMonth"].ToString();
			var sponsorItemEng = list.Rows[0]["sponsorItemEng"].ToString();
			var sponsorItem = list.Rows[0]["sponsorItem"].ToString();
			var StartDate = list.Rows[0]["StartDate"].ToString().Substring(0,10);

			this.ViewState["giftType"] = sponsorItemEng;
			this.ViewState["childMasterId"] = childMasterId;
			gift_type.Value = sponsorItemEng;

			lb_type.Text = sponsorItem;
			//lb_startdate.Text = StartDate;
			if(sponsorItemEng == "CF") {
				pn_child.Style["display"] = "none";
			} else {
				this.GetChildInfo(childMasterId);
			}

			ddlMonth.SelectedValue = SponsorMonth.PadLeft(2, '0');
			if (sponsorItemEng == "BG" || sponsorItemEng == "CF" ) {
				ph_month.Visible = false;
			} else {
				ph_month.Visible = true;
			}
			
			this.BindAmount(sponsorItemEng , SponsorAmount);

		} catch(Exception ex) {
		//	Response.Write(ex.ToString());
			ErrorLog.Write(this.Context, 0, ex.Message);
			base.AlertWithJavascript("예약정보를 가져오는중에 에러가 발생했습니다." , "goBack()");

		}

	}
	

	void GetChildInfo(string childMasterId ) {
	
		DataSet ds = _wwwService.GetChildDetail(childMasterId);
		if(ds.Tables["ChildDetailT"] != null) {
			
			//레이블에 바인딩
			c_key.Text = ds.Tables["ChildDetailT"].Rows[0]["ChildKey"].ToString().Trim();
			//c_name.Text = ds.Tables["ChildDetailT"].Rows[0]["Name"].ToString().Trim();
			c_namekr.Text = ds.Tables["ChildDetailT"].Rows[0]["NameKr"].ToString().Trim();
		//	c_birth.Text = ds.Tables["ChildDetailT"].Rows[0]["BirthDate"].ToString().Trim();
		//	c_age.Text = Convert.ToDateTime(c_birth.Text).GetAge().ToString();
		//	c_gender.Text = ds.Tables["ChildDetailT"].Rows[0]["Gender"].ToString().Trim();
			c_country.Text = ds.Tables["ChildDetailT"].Rows[0]["CountryCode"].ToString().Trim();
			img.Text = ChildAction.GetPicByChildKey(c_key.Text);
			
			
		}

	}

	void BindAmount(string type , string val) {

		List<int> price = null;
		if(type == "BG") {
			price = new List<int> { 15000, 30000, 50000, 70000, 100000 };
		} else if(type == "GG") {   // 어린이
			price = new List<int> { 15000, 30000, 50000, 70000, 100000 };
		} else if(type == "FG") {   // 가족
			price = new List<int> { 35000, 50000, 100000, 300000, 500000, 1000000 };
		} else if(type == "PG") {   // 어린이센터
			price = new List<int> { 150000, 200000, 300000, 500000, 1000000, 2000000 };
		} else if(type == "CF") {   // 크리스마스
			price = new List<int> { 20000, 30000, 40000, 50000 };
		}

		var selected = false;
		pay_amount.Items.Clear();
		pay_amount.Items.Add(new ListItem("선택하세요", ""));
		
		foreach(var p in price) {
			var item = new ListItem(p.ToString("N0") + "원", p.ToString());
			if (p.ToString() == val) {
				item.Selected = true;
				selected = true;
			}
			pay_amount.Items.Add(item);
		}

		pay_amount.Items.Add(new ListItem("직접입력", "custom"));
		if(!selected) {
			
			pay_amount.SelectedIndex = pay_amount.Items.Count - 1;
			pay_amount_custom.Value = val;
		}

	}

	// 수정
	protected void btn_submit_Click( object sender, EventArgs e ) {
		var commitmentId = this.ViewState["commitmentId"].ToString();

		if(base.IsRefresh) {
			return;
		}

		UserInfo sess = new UserInfo();
		var amount = string.IsNullOrEmpty(pay_amount_custom.Value) ? pay_amount.SelectedValue : pay_amount_custom.Value;
		if (amount == "") {
			base.AlertWithJavascript("금액을 선택해주세요.");
			return;
		}

		{
			Object[] objParam = new object[] { "commitmentId", "Amount", "Remark", "Month", "ModUserID", "ModUserName" };
			Object[] objValue = new object[] { commitmentId, amount, "", ddlMonth.SelectedValue, sess.SponsorID, sess.UserName };
			Object[] objSql = new object[] { "sp_U_CommitmentAmount" };
			_www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
		}

		{
			Object[] objParam = new object[] { "commitmentId", "SponsorID", "Amount", "Remark", "Month", "RegUserID", "RegUserName", "RegDate" };
			Object[] objValue = new object[] { commitmentId, sess.SponsorID, amount, "", ddlMonth.SelectedValue, sess.SponsorID, sess.UserName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") };
			Object[] objSql = new object[] { "sp_I_CommitmentAmountHistory" };
			_www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
		}

		base.AlertWithJavascript("수정되었습니다.", "goBack()");

		/*
		Object[] objParam = new object[] { "sponsorId", "commitmentId" };
		Object[] objValue = new object[] { sess.SponsorID, commitmentId };
		Object[] objSql = new object[] { "sp_web_present_reservation_cancel_f" };
		var success = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0].Rows[0]["result"].ToString() == "1";

		if(success) {

			var result = new CommitmentAction().DoGIFT(sess.SponsorID, this.ViewState["childMasterId"].ToString() , amount, "", ddlMonth.SelectedValue, "정기", this.ViewState["giftType"].ToString());
			if(result.success) {
				//Response.Redirect(this.ViewState["listUrl"].ToString());
				base.AlertWithJavascript("수정되었습니다." , "goBack()");
			} else {
				base.AlertWithJavascript("선물금 수정에 실패했습니다.");
			}
				
		}
		*/
	}
}