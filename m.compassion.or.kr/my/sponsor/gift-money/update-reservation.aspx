﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update-reservation.aspx.cs" Inherits="my_gift_money_update_reservation" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/sponsor/gift-money/update-reservation.js"></script>
	<script type="text/javascript" src="/assets/sly/sly.min.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="gift_type" />
	
<div class="wrap-sectionsub-my">    <!--마이컴패션 디자인-->
<!---->
  
    
    <div class="menu-my sectionsub-margin2">
    	
        <ul style="position: absolute; top: 0;">
            <!--임시 inline style(수정가능)-->
            <li style="display: inline-block; width: 100px"><a href="/my/sponsor/commitment/">후원신청내역</a></li>
            <li style="display: inline-block; width: 100px"><a href="/my/sponsor/payment/">후원금 내역</a></li>
            <li style="display: inline-block; width: 100px"><a href="/my/sponsor/pay-delay/">지연된 후원금</a></li>
            <li style="display: inline-block; width: 100px"><a class="selected" href="/my/sponsor/gift-money/">어린이 선물금</a></li>
            <li style="display: inline-block;"></li>
        </ul>
        
        
    </div>
    
     <div class="submenu-my sectionsub-margin2">
    	
        <ul style="position:absolute;left:60px;top:0;">  <!--임시 inline style(수정가능)-->
            <li style="display:inline-block"><a href="/my/sponsor/gift-money/">어린이 선물금</a></li>
            <li style="display:inline-block"><a class="selected"  href="/my/sponsor/gift-money/pay/">선물금 보내기</a></li>
        </ul>
        
    </div>

    <div class="my-sponsor">
    	
        <div class="box-myblue txt-gifttype">
        	<span class="ic"><asp:Literal runat="server" ID="lb_type"></asp:Literal></span>
        </div>
        
		<div runat="server" id="pn_child">
        <ul class="list-giftchild">
        	<li>
            	<span class="box-mywhite">
                    <span class="row">
                        <p class="txt-id fl">어린이 ID : <asp:Literal runat="server" ID="c_key" /></p>
                    </span>
                    <div class="linebar"></div>
                    <span class="row align-padding">
                        <span class="pos-photo"><span class="photo" style="background:url('<asp:Literal runat="server" ID="img" />') no-repeat"></span></span>
                        <p class="txt-name"><asp:Literal runat="server" ID="c_namekr" /></p>
                        <p class="txt-nation">국가 : <asp:Literal runat="server" ID="c_country" /></p>
                    </span>
                </span>
            </li>
        </ul>
		</div>
        
		<asp:PlaceHolder runat="server" ID="ph_month" Visible="false">
        <div class="box-mywhite period-giftsend mt10">
            <div class="wrap-caption">
            	<p class="txt-title">선물금 예약월</p>
            </div>
            <div class="frm-row" style="padding-bottom:0">
            	<span class="row-table">
                    <span class="th-field" style="width:20%">매년</span>
                    <span class="td-field" style="width:80%"><asp:DropDownList ID="ddlMonth" style="width:100%" runat="server" Enabled="true" >
							<asp:ListItem Selected="True" Value="01">1월</asp:ListItem>     
							<asp:ListItem Value="02">2월</asp:ListItem>
							<asp:ListItem Value="03">3월</asp:ListItem>      
							<asp:ListItem Value="04">4월</asp:ListItem>
							<asp:ListItem Value="05">5월</asp:ListItem>
							<asp:ListItem Value="06">6월</asp:ListItem>
							<asp:ListItem Value="07">7월</asp:ListItem>
							<asp:ListItem Value="08">8월</asp:ListItem>
							<asp:ListItem Value="09">9월</asp:ListItem>
							<asp:ListItem Value="10">10월</asp:ListItem>
							<asp:ListItem Value="11">11월</asp:ListItem>
							<asp:ListItem Value="12">12월</asp:ListItem>
						</asp:DropDownList></span>
                </span>	
				 <p class="txt-guide mt10" >예약월 변경 시 중복 출금이 될 수 있습니다. 선물금 결제 내역을 확인하시고, 변경해주세요.</p>
            </div>
		</div>
		</asp:PlaceHolder>
        
        <div class="box-mywhite period-giftsend mt10">
            <span class="row-table">
            	<span class="th-field" style="width:20%">금액</span>
                <span class="td-field" style="width:80%">
                	<span class="row">
						<asp:DropDownList runat="server" ID="pay_amount" style="width:100%"></asp:DropDownList>
                    </span>
                    <span class="row pn_pay_amount_custom" style="display:none;">
						<input type="text" runat="server" id="pay_amount_custom" class="number_only fc_money" style="width:100%" placeholder="금액을 입력해 주세요" />
                    </span>
                </span>
            </span>
            
            <p class="txt-guide mt10 pn_BG" style="display:none;">생일선물금은 최소 1만 5천원~ 최대 10만원까지를 추천합니다.</p>
			<p class="txt-guide mt10 pn_GG" style="display:none;">어린이에게 전하는 특별한 선물금은 최소 1만 5천원~ 최대 10만원까지 연에 1~2회를 추천합니다.</p>
			<p class="txt-guide mt10 pn_FG" style="display:none;">어린이 가족에게 전하는 마음의 선물금은 최소 3만 5천원~ 최대 100만원까지 권장합니다. (연에 100만원까지 가능합니다.)</p>
			<p class="txt-guide mt10 pn_PG" style="display:none;">어린이가 등록된 어린이센터에 전하는 모두를 위한 선물금은 최소 15만원~ 최대 200만원까지 권장합니다. (연에 200만원까지 가능합니다.)</p>
            <p class="txt-guide mt10 pn_CF" style="display:none;">크리스마스 선물금은 컴패션에 등록된 모든 어린이를 위한 선물금으로, 한 어린이당 2만원의 선물금을 추천합니다.</p>
        </div>
        
        <div class="wrap-bt mt20"><asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="bt-type6" style="width:100%">수정</asp:LinkButton></div>
        <!--
        <div class="box-mywhite list-guidetxt">
        	<ul>
            	<li>선물금은 최소 15,000원 이상부터 보내실 수 있습니다.</li>
                <li>예약하신 선물금은 후원금 자동이체 계좌, 신용카드에서 출금됩니다.</li>
            </ul>
        </div>
		-->
        
    </div>
    
<!--//-->
</div>

</asp:Content>
