﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class my_qna_view : MobileFrontBasePage {

    const string listPath = "/my/qna/";

    public override bool RequireSSL
    {
        get
        {
            return false;
        }
    }

    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();

        // check param
        var requests = Request.GetFriendlyUrlSegments();
        if(requests.Count < 1) {
            Response.Redirect(listPath, true);
        }

        var isValid = new RequestValidator()
            .Add("p", RequestValidator.Type.Numeric)
            .Validate(this.Context, listPath);

        base.PrimaryKey = requests[0];
        key.Value = requests[0];


        btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();


    }

    protected override void loadComplete( object sender, EventArgs e ) {

        ShowData();
    }

    void ShowData() {

        var sess = new UserInfo();
        //userName.InnerText = sess.UserName ;
        //sponsorID.InnerText = sess.SponsorID;


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.upboard.First(p => p.board_idx == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<upboard>("board_idx", Convert.ToInt32(PrimaryKey));

            //var type = dao.code.First(p => p.cd_group == "qna_type" && p.cd_key == entity.type).cd_value;
            var type = www6.selectQF<code>("cd_group", "qna_type", "cd_key", entity.type).cd_value;

            b_type.Text = type;
            b_title.Text = entity.b_title;
            b_content.Text = entity.b_content;
            b_regDate.Text = Convert.ToDateTime(entity.b_writeday).ToString("yyyy.MM.dd");
            b_img.Src = entity.b_filename1 == ConfigurationManager.AppSettings["image_qna"] ? "" : entity.b_filename1.WithFileServerHost();
            s_confirm.Text = entity.s_confirm.EmptyIfNull().ToString() == "" ? "처리중" : "답변완료";

            // 답변내역
            //var answer_entity = dao.upboard_comment.FirstOrDefault(p => p.seqno == entity.no_idx && p.@ref == entity.@ref);
            var answer_entity = www6.selectQF<upboard_comment>("seqno", entity.no_idx, "ref", entity.@ref);

            if (answer_entity != null)
            {
                s_content.Text = answer_entity.comment;
            }
            else
            {
                answer_holder.Visible = false;
            }


        }
	}

}