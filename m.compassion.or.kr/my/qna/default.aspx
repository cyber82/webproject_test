﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_qna_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/my/qna/default.js?v=1.0"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub-my fullHeight" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <!--마이컴패션 디자인-->
        <!---->

        <div class="my-epilogue">

            <ul class="list-board">
                <li ng-repeat="item in list"><a href="#" ng-click="goView(item.board_idx, $event)">
                    <span class="box-mywhite">
                        <p class="txt-title align-padding">{{item.type}}</p>
                        <p class="txt-note textcrop-1row align-padding">{{item.b_title}}</p>
                        <time class="txt-time align-padding" datetime="2016-11-30">{{item.b_writeday | date:'yyyy.MM.dd'}}</time>
                        <span ng-class="{'pos-state1' : item.s_confirm == '처리중' , 'pos-state2' : item.s_confirm == '답변<br>완료'}" ng-bind-html="item.s_confirm"></span>
                    </span>
                </a></li>
                <!--
                    <span class="pos-state2">답변<br />완료</span>
            -->

                <!--등록글 없는 경우-->
                <li class="no-result" ng-if="total == 0">
                    <span class="box-mywhite">등록된 내역이 없습니다.</span>
                </li>
            </ul>

            <paging ng-hide="total == 0" class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>

            <div class="wrap-bt sectionsub-margin2 bottom-white-bt">
                <a href="/customer/faq/" class="bt-type7 fl" style="width: 49%">FAQ</a>
                <a href="/customer/qna/" class="bt-type6 fr" style="width: 49%">1:1 문의하기</a>
            </div>

        </div>


        <!--//-->
    </div>







</asp:Content>

