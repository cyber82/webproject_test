﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="my_qna_view" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/my/qna/default.js"></script>
	<script>
		$(function () {
			setFullHeight();
		})
		
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" id="key" runat="server" />
    <div class="wrap-sectionsub-my fullHeight">
        <!--마이컴패션 디자인-->
        <!---->

        <div class="my-epilogue">

            <div class="box-mywhite">
                <p class="wrap-caption consult-title" style="word-break:break-all">
                    <strong>
                       <asp:Literal runat="server" id="b_type"></asp:Literal>
                    </strong>
                    <asp:Literal runat="server" id="b_title"></asp:Literal>
                </p>
                <div class="consult-info">
                    <asp:Literal runat="server" id="b_regDate"></asp:Literal><span class="bar">|</span><em><asp:Literal runat="server" id="s_confirm"> </asp:Literal></em>
                </div>
                <div class="consult-answer" style="white-space:pre-line;word-break:break-all">
                    <p class="txt-caption">회원 문의</p>
                    <asp:Literal runat="server" id="b_content"></asp:Literal>
                        <img runat="server" id="b_img" style="max-width:100%;"/>
                    <br />
                    <br />
					<asp:PlaceHolder runat="server" ID="answer_holder">
                    <p class="txt-caption">한국컴패션 답변</p>
                    <asp:Literal runat="server" id="s_content"></asp:Literal>
					</asp:PlaceHolder>
                </div>

                <div class="wrap-bt"><a href="#" class="bt-type5" style="width: 100%" runat="server" id="btnList">목록</a></div>

                <div class="wrap-bt mt20">
                    <a href="/customer/faq/" class="bt-type7 fl" style="width: 49%">FAQ</a>
                    <a href="/customer/qna/" class="bt-type6 fr" style="width: 49%">1:1 문의하기</a>
                </div>

            </div>

        </div>


        <!--//-->
    </div>
</asp:Content>

