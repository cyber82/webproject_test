﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using System.IO;


public partial class my_children_view : MobileFrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var sess = new UserInfo();
	
		var requests = Request.GetFriendlyUrlSegments();
		if(requests.Count < 1) {
			Response.Redirect("/my/", true);
		}
		
		childKey.Value = requests[0];      // childKey
										   //childMasterId.Value = requests[1];      // childMasterId
		childMasterId.Value = Request["childMasterId"];

		googleMapApiKey.Value = ConfigurationManager.AppSettings["googleMapApiKey"];
		this.ViewState["userName"] = new UserInfo().UserName;

	}

}