﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_children_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/children/default.js"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
<input type="hidden" id="data" runat="server" />

<div class="wrap-sectionsub-my" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">    <!--마이컴패션 디자인-->
    
    <div class="my-mychild">
    	
        <fieldset class="search-mychild sectionsub-margin2">
			<legend>후원 중인 어린이 검색</legend>
			<select style="width:40%" id="country" ng-model="country" ng-change="changeCountry(country)" data-ng-options="item.country as item.country for item in countries">
				<option value="">국가</option>
			</select>
            &nbsp;
			<select style="width:56%" id="child" ng-model="child" ng-change="changeChild(child)" data-ng-options="item.childkey as item.namekr for item in children">
				<option value="">어린이이름</option>
			</select>

            <!--<a class="action" href="#">검색</a>-->
        </fieldset>
        
		<ul class="list-mychild"> 
        	<li ng-repeat="item in list">
                <span class="box-mywhite">
                    <p class="wrap-caption">우리가 만난 날  <span class="color2">♥</span> <span class="color1">{{item.commitmentdate | date:'yyyy.MM.dd'}}</span></p>
					<a href="#" ng-click="goView($event,item)">
						<span class="pos-photo"><span class="photo" style="background:url('{{item.pic}}') no-repeat"></span></span>
						<p class="txt-name">{{item.name == '' ? item.personalnameeng : item.name}}</p>
						<p class="txt-note">
                    		어린이ID : <span class="color1">{{item.childkey}}</span><br />
							국가 : <span class="color1">{{item.countryname}}</span><br />
							생일 : <span class="color1">{{item.birthdate | date:'yyyy.MM.dd'}} ({{item.age}}세)</span><br />
							성별 : <span class="color1">{{item.gender}}</span>
						</p>
					</a>
                    <div class="wrap-bt">
                        <a class="bt-type7 fl" style="width:48%" href="#" ng-click="goGift($event , item);">선물금 보내기</a>
                        <a class="bt-type6 fr" style="width:48%" href="#" ng-click="showAlbum($event,item)">성장앨범 보기</a>
                    </div>
                </span>
            </li>
           
        </ul>

		<div class="box-myblue link-childmore"><a href="#" ng-show="total > list.length" ng-click="showMore($event)"><span>더 많은 어린이들 보기</span></a></div>


    </div>
    
<!--//-->
</div>

</asp:Content>
