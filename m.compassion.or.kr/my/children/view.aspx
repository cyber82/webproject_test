﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="my_children_view" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/my/children/view.js?v=1.1"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <style>
        .child-center-info {
            display: block;
            font-family: 'noto_m';
            font-weight: normal;
            font-size: 16px;
            line-height: 20px;
            color: #333;
            padding-bottom: 18px;
        }
    </style>
    <input type="hidden" id="childKey" runat="server" />
    <input type="hidden" id="childMasterId" runat="server" />
    <input type="hidden" id="googleMapApiKey" runat="server" />

    <div class="wrap-sectionsub-my" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <!--마이컴패션 디자인-->

        <div class="my-mychild" style="padding-top: 20px">

            <div class="box-mywhite view-mychild">
                <p class="wrap-caption">우리가 만난 날  <span class="color2">♥</span> <span class="color1">{{entity.commitmentdate | date:'yyyy.MM.dd'}}</span></p>
                <span class="pos-photo"><span class="photo" style="background: url('{{entity.pic}}') no-repeat"></span></span>
               <%-- <p class="txt-name">--%>
                    <span class="txt-name" style="display:block;"><div style="display:inline-block;">{{entity.name}}</div> <div style="display:inline-block;">{{entity.personalnameeng}}</div></span>
                <%--</p>--%>
                <div class="wrap-bt">
                    <a class="bt-type7 fl" style="width: 48%" href="#" ng-click="goGift($event , entity);">선물금 보내기</a>
                    <a class="bt-type6 fr" style="width: 48%" href="#" ng-click="showAlbum($event , entity);">성장앨범 보기</a>
                </div>
                <div class="description">
                    <strong><%:this.ViewState["userName"].ToString() %> 후원자님, 안녕하세요?</strong>
                    우리가 함께한 지 <span class="color1">{{entity.commitmentdays| number:0}}일</span>이 지났어요. 후원금과 기도, 사랑의 편지 항상 감사드려요.
                <div class="box-mygray">
                    <table>
                        <tbody>
                            <tr>
                                <td style="width:65px;">어린이 이름 : </td>
                                <td><span class="color1"><div style="display:inline-block;">{{entity.namekr}}</div> <div style="display:inline-block;">{{entity.nameen}}</div></span></td>
                            </tr>
                            <tr>
                                <td>어린이 애칭 : </td>
                                <td><span class="color1"><div style="display:inline-block;">{{entity.name}}</div> <div style="display:inline-block;">{{entity.personalnameeng}}</div></span></td>
                            </tr>
                        </tbody>
                    </table>

                    <table>
                        <tbody>
                            <tr>
                                <td style="width: 54%">어린이 ID : <span class="color1">{{entity.childkey}}</span>
                                </th>
	                            <td style="width: 46%">국가 : <span class="color1">{{entity.countryname}}</span></td>
                            </tr>
                            <tr>
                                <td>생일 : <span class="color1">{{entity.birthdate | date:'yyyy.MM.dd'}} ({{entity.age}}세)</span></td>
                                <td>성별 : <span class="color1">{{entity.gender}}</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                    <!--
				<span ng-if="entity.casestudy.family">저희 가족은요, <em>{{entity.casestudy.family}}</em> <em>형제 2명</em>이 있어요.</span>
				<span ng-if="entity.casestudy.familyduties">저는 집에서 가족과 함께 <em>{{entity.casestudy.familyduties}}</em>을(를) 해요.<br /></span>
				<span ng-if="entity.casestudy.christianactivities">제가 참여하는 컴패션 활동은요 <em>{{entity.casestudy.christianactivities}}</em>이예요. </span>
				<span ng-if="entity.casestudy.hobby">제 취미와 운동은 <em>{{entity.casestudy.hobby}}</em> 예요.<br /></span>
				<span ng-if="entity.casestudy.schooling"><em>{{entity.casestudy.schooling}}</em>에 다니고 있고, </span>
				<span ng-if="entity.casestudy.family">좋아하는 과목은 <em>{{entity.casestudy.family}}</em>이예요. <br />-->
                    <span ng-bind-html="entity.biokr"></span>
                    <br />

                    <span ng-if="entity.casestudy.health">(<em>{{entity.casestudy.health}}</em>)</span>

                </div>
            </div>

            <div class="box-mywhite map-weather">
                <p class="nation">지금 {{countryInfo.c_name}}는(은)요,</p>
                <div id="map" style="width: 100%; height: 200px"></div>
                <div class="wrap-weather">
                    <p class="state">{{countryInfo.c_weather}}</p>
                    <p class="temperature">{{countryInfo.c_temperature | number:0}}˚</p>
                    <img ng-show="countryInfo" ng-src="/common/img/page/sponsor/weather/{{countryInfo.c_weather_icon}}.png" class="icon" alt="맑음" />
                    <p class="time">{{countryInfo.time | date:'HH:mm'}}</p>
                </div>
                <br />
                <div class="graph">
                    <img ng-src="/common/img/page/sponsor/gdp/{{entity.countrycode}}.jpg" alt="" width="100%" />
                </div>
            </div>

            <div class="box-mywhite person-gdp" style="overflow: scroll">
                <!-- 센터설명 -->
                <div class="compassionCenter" style="display: none">
                    <%--<p class="txt-caption">{{entity.name}}이(가) 살고 있는 컴패션 센터는</p>--%>
                    <p class="child-center-info">{{entity.name}}이(가) 다니는 어린이센터는,</p>

                    <span ng-bind-html="entity.projectinfo"></span>
                    <br />
                    <span>■ 어린이센터 활동</span>
                    <div class="box-ProjectInfo">
                        <div style="display: block;">
                            <div class="box_type7 cognitive">
                                <span>□ 지적 영역</span>
                                <div class="box_type9" ng-bind-html="entity.cognitive">
                                </div>
                            </div>
                            <div class="box_type8 socioemotional">
                                <span>□ 사회정서적 영역</span>
                                <div class="box_type9" ng-bind-html="entity.socioemotional">
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="box_type7 spiritual">
                                <span>□ 영적 영역</span>
                                <div class="box_type9" ng-bind-html="entity.spiritual">
                                </div>
                            </div>
                            <div class="box_type8 physical">
                                <span>□ 신체적 영역</span>
                                <div class="box_type9" ng-bind-html="entity.physical">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="box-mywhite ad-banner" ng-show="entity.canletter">
                <p class="wrap-caption">
                    <strong>어린이에게 편지를 보내보세요.</strong>
                    컴패션 앱을 다운받으셔서 후원 중인<br />
                    나의 어린이에게 편지를  쓰실 수 있습니다.
                </p>
                <p class="txt-appstore">지금 바로 앱스토어 or 구글플레이스토어에서<br />
                    <strong>‘컴패션’을 검색해 보세요!</strong></p>
                <ul class="list-appstore">
                    <li><a href="/app/download?device=iphone" target="_blank">
                        <img src="/common/img/icon/applestore.png" alt="" />Download on the<br />
                        APP Store</a></li>
                    <li><a href="/app/download?device=android" target="_blank">
                        <img src="/common/img/icon/googlestore.png" alt="" />Android app on<br />
                        Google play</a></li>
                </ul>
            </div>

            <div ng-show="entity.cangift">
                <div class="giftmoney">
                    <div class="box-myblue ">
                        <div class="wrap-header">
                            <p class="txt-title">선물금 예약 내역</p>
                            <a class="bt-type2 more" href="/my/sponsor/gift-money/pay/?t=regular" ng-if="giftable">선물금 예약하기</a>
                        </div>
                    </div>
                    <div class="box-mywhite">
                        <ul class="list-gift">
                            <li ng-repeat="item in gr_list">
                                <span class="row txt-name">{{item.namekr}}<span class="ar">{{item.startdate | date:'yyyy.MM.dd'}}</span>
                                </span>
                                <span class="row txt-period">{{item.sponsoritem}}<span class="bar">|</span>{{item.fundingfrequency}} {{item.sponsormonth}}월
                                </span>
                                <span class="row txt-price">
                                    <em>{{item.sponsoramount | number:0}}</em> 원<span class="ar"><a href="/my/sponsor/gift-money/update-reservation/?c={{item.commitmentid}}" class="modify">수정</a><a ng-click="deleteGiftReservation($event,item.commitmentid)" class="delete">삭제</a></span>
                                </span>
                            </li>


                            <!--내용없는경우-->
                            <li class="no-result" ng-if="gr_total == 0">
                                <span class="img">
                                    <img src="/common/img/icon/gift2.png" alt="" width="45" /></span>
                                등록된 선물금 예약 정보가 없습니다.
                            </li>
                        </ul>
                    </div>

                    <paging class="small" page="gr_page" page-size="gr_rowsPerPage" total="gr_total" show-prev-next="true" show-first-last="true" paging-action="getGiftReservationList(page)"></paging>

                </div>


                <div class="giftmoney">
                    <div class="box-myblue ">
                        <div class="wrap-header">
                            <p class="txt-title">선물금 결제 내역</p>
                            <a class="bt-type2 more" href="/my/sponsor/gift-money/pay/?t=temporary" ng-if="giftable">선물금 보내기</a>
                        </div>
                    </div>
                    <div class="box-mywhite">
                        <ul class="list-gift">
                            <li ng-repeat="item in gp_list">
                                <span class="row txt-name">{{item.namekr}}<span class="ar">{{item.paymentdate | date:'yyyy.MM.dd'}}</span>
                                </span>
                                <span class="row txt-period">{{item.sponsoritem}}<span class="bar">|</span>{{item.paymentname}}
                                </span>
                                <span class="row txt-price">
                                    <em>{{item.sponsoramount | number:0}}</em> 원
                                </span>
                            </li>

                            <!--내용없는경우-->
                            <li class="no-result" ng-if="gp_total == 0">
                                <span class="img">
                                    <img src="/common/img/icon/gift3.png" alt="" width="38" /></span>
                                등록된 선물금 내용이 없습니다.<br />
                                어린이에게 선물을 보내 보세요.
                            </li>
                        </ul>
                    </div>

                    <paging class="small" page="gp_page" page-size="gp_rowsPerPage" total="gp_total" show-prev-next="true" show-first-last="true" paging-action="getGiftPaymentList(page)"></paging>

                </div>
            </div>

            <div class="my-hotlink">
                <p>직접 고른 선물을 하고 싶으신가요?</p>
                <a href="/store/?gift_flag=1">
                    <img src="/common/img/page/my/banner-store.jpg" alt="스토어 선물 고르러 가기" width="100%" /></a>
            </div>

        </div>

        <!--//-->
    </div>

</asp:Content>
