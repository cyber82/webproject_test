﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_children_default : MobileFrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var sess = new UserInfo();
	

		var actionResult = new ChildAction().MyChildren("" , 1 , 4);
		if(actionResult.success) {

			data.Value = actionResult.data.ToLowerCaseJson();

			var children = (List<ChildAction.MyChildItem>)actionResult.data;
			if(children.Count < 1) {
				//base.AlertWithJavascript("나의 어린이가 없습니다.", "goBack()");
				Response.Redirect("/my/children/nodata");
				return;
			}

			// 1명인경우 상세페이지로 이동
			if(children.Count < 2) {
				Response.Redirect(string.Format("/my/children/view/{0}?childMasterId={1}" , children[0].ChildKey, children[0].ChildMasterId));
				return;
			}

		} else {
			if(actionResult.action == "not_sponsor") {
				Response.Redirect("/my/children/nodata");
			}
			base.AlertWithJavascript(actionResult.message , "goBack()");
		}

	}
	
	protected override void loadComplete( object sender, EventArgs e ) {
		
	}

}