﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cad.aspx.cs" Inherits="my_activity_cad" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/my/activity/cad.js"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub-my" class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">     <!--마이컴패션 디자인-->
<!---->
    
    
    <div class="menu-my sectionsub-margin2" id="slide">
    	
        <ul style="position: absolute; top: 0;" id="slide_menu">
            <li style="display:inline-block;width:130px"><a class="selected" href="/my/activity/cad/">CAD 추천 관리</a></li>
            <li style="display:inline-block;width:100px"><a href="/my/activity/stamp/">스탬프 투어</a></li>
            <li style="display:inline-block;width:160px"><a href="/my/activity/event/">캠페인/이벤트 참여내역</a></li>
            <li style="display:inline-block;"></li>
        </ul>
        
    </div>
    
    <div class="my-activity">
    	
        <div class="box-mywhite list-cad">
            <ul>
                <li ng-repeat="item in list">
                    <span class="row txt-id">
                        추천 어린이 ID : {{item.childkey}}<span class="ar">{{item.status}} <span ng-show="item.status == '진행중'">D-{{item.d_day}}</span><span ng-show="item.status == '결연'">♥{{item.startdate| date:'yyyy.MM.dd' }}</span></span>
                    </span>
                    <span class="row txt-name">{{item.namekr }}</span>
                    <span class="row txt-nation">
                        국가 : {{item.country}}<span class="ar">{{item.regdate | date:'yyyy.MM.dd'}}</span>
                    </span>
                </li>
                
                <!--등록글 없는 경우-->
                <li class="no-result" ng-if="total == 0">
                	등록된 CAD 추천 관리 내역이 없습니다.
                </li>
			</ul> 
		</div>

       <paging ng-hide="total == 0" class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
        
		<div class="box-mywhite bottomtxt-eventjoin">
			CAD 추천 서비스는 컴패션 모바일 홈페이지를 통해 이용하실 수 있습니다.
		</div>

        <div class="my-hotlink">
        	<p>하루 한 명, 소중한 사람들에게 컴패션 어린이를 추천해 보세요</p>
            <a href="/advocate/together/compassion-a-day/"><img src="/common/img/page/my/banner-cad.jpg" alt="CAD 소개 페이지 가기" width="100%" /></a>
        </div>
        
    </div>
    
<!--//-->
</div>
	

</asp:Content>


