﻿$(function () {
    var sly = new Sly($("#slide"), {    // Call Sly on frame

        horizontal: 1,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 0,
        scrollBy: 1,
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1

    });

    sly.init();
    sly.activate(0);
    /*
    setTimeout(function () {

        if ($("#slide_menu").width() < $("#slide").width()) {
            $("#slide_menu").css("left", '7%');
        }
    }, 300);
    */
});

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

	
		$scope.total = -1;
		$scope.list = null;
	
		$scope.params = {
			page: 1,
			rowsPerPage: 5
		};

		// 이번달 주문 내역 
		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);
			$http.get("/api/cad.ashx?t=list", { params: $scope.params }).success(function (r) {
				console.log(r);
				if (r.success) {
					//var list = $.parseJSON(r.data);
					var list = r.data;
					var today = new Date();
                    console.log(today)
					if (list.length > 0) {
						$.each(list, function () {
							this.regdate = new Date(this.regdate);
							this.startdate = new Date(this.startdate);
							this.birthdate = new Date(this.birthdate);
							this.d_day = 7 - Math.floor((today - this.regdate) / (1000 * 60 * 60 * 24));
							if (this.status == "대기") {
							    this.status = "진행중";
							}
						});


						$scope.list = list;
						$scope.total = r.data.length > 0 ? r.data[0].total : 0;
					} else {
						$scope.total = 0;
					}
				} else {
					alert(r.message);
				}
			});
		}

		$scope.getList();

	});

})();