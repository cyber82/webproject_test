﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="event.aspx.cs" Inherits="my_activity_event" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/my/activity/event.js?v=1.1"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub-my" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">    <!--마이컴패션 디자인-->
<!---->
    
    <div class="menu-my sectionsub-margin2" id="slide">
    	
        <ul style="position: absolute; top: 0;" id="slide_menu">
            <li style="display:inline-block; width:130px"><a href="/my/activity/cad/">CAD 추천 관리</a></li>
            <li style="display:inline-block; width:100px"><a href="/my/activity/stamp/">스탬프 투어</a></li>
            <li style="display:inline-block; width:160px"><a class="selected" href="/my/activity/event/">캠페인/이벤트 참여내역</a></li>
            <li style="display:inline-block"></li>
        </ul>
        
    </div>
    
    <div class="my-activity">
    	
        <ul class="list-eventjoin">
            <li ng-class="{ongoing:item.state == '진행중'}" ng-repeat="item in list" ng-click = "goView(item.er_e_id)">
                <span class="box-mywhite">
                    <span class="row txt-title">{{item.e_title}}</span>
                    <span class="row txt-day" ng-show="item.e_announce">당첨자 발표 : {{item.e_announce | date:'yyyy.MM.dd'}}</span>
                    <span class="row txt-day" ng-hide="item.e_announce">&nbsp</span>
                    <span class="row txt-time">{{item.er_regdate | date:'yyyy.MM.dd'}}</span>
                    <span class="pos-state">
                        <span class="txt1">{{item.state}}</span>
                        <em class="txt2" ng-show="item.state == '종료'">{{item.winner}}</em>
                    </span>
                </span>
            </li>

            
            <!--등록글 없는 경우-->
            <li class="no-result" ng-if="total == 0">
                <span class="box-mywhite">등록된 캠페인/이벤트 참여내역이 없습니다.</span>
            </li>
        </ul> 

       <paging ng-hide="total == 0" class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
        
        <div class="box-mywhite bottomtxt-eventjoin">
            신청하신 캠페인/이벤트의 신청(예약)정보 변경 및 취소는 해당 이벤트의 상세 페이지에서 가능합니다.
        </div>
        
        
    </div>
    
<!--//-->
</div>

</asp:Content>

