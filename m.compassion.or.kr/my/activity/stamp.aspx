﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="stamp.aspx.cs" Inherits="my_activity_stamp" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript" src="/my/activity/stamp.js?ver=1.2"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub-my" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <!--마이컴패션 디자인-->

        <div class="menu-my sectionsub-margin2" id="slide">

            <ul style="position: absolute; top: 0;" id="slide_menu">
                <li style="display: inline-block; width:130px"><a href="/my/activity/cad/">CAD 추천 관리</a></li>
                <li style="display: inline-block; width:100px"><a class="selected" href="/my/activity/stamp/">스탬프 투어</a></li>
                <li style="display: inline-block; width:160px"><a href="/my/activity/event/">캠페인/이벤트 참여내역</a></li>
                <li style="display: inline-block"></li>
            </ul>

        </div>

        <div class="my-activity">
            <!--
            <script type="text/javascript">
                $(function () {
                    $(".stamp-top .txt-title").click(function () {
                        $(".stamp-top .define").slideToggle("fast", function () {
                            if ($(this).is(":visible"))
                                $(this).css("display", "block");
                        });
                        $(".stamp-top .txt-title").toggleClass("define-open");
                    });
                });
            </script>
			-->
            <div class="box-mywhite stamp-top">
                <div class="wrap-header">
                    <p class="txt-title">스탬프 투어란<span class="bu"></span></p>

                    <span class="more nopad">
                        <select id="year" ng-model="year" ng-change="changeYear(year)" data-ng-options="item.s_year as item.s_year for item in years"></select>
                    </span>
                </div>
                <div class="define">
                    컴패션의 이벤트나 후원자 활동에 참여할 때마다 스탬프를 부여받아, 다채로운 컴패션의 서비스를 만나 가는 투어 활동입니다. 후원자님만이 참여할 수 있는 온/오프라인 활동을 통해 즐거운 여행을 시작해 보세요!
                </div>
            </div>

            <!--스탬프투어-->
            <div class="box-mywhite stamp-tour mt20">
                <div class="wrap-header">
                    <p class="txt-title">스탬프투어</p>

                </div>
                <p class="txt">스탬프를 클릭하시면 해당 활동에 참여하실 수 있습니다.</p>
                <ul class="list-stamp">

                    <li ng-repeat="item in online_list | orderBy : $index : true">
                        <a ng-click="appAlert(item.alert)" href="{{item.url}}"><span class="stamp {{item.s_id}} {{item.addClass}}"></span>
                            <!--css규칙 : "stamp {스템프코드} on"    :  on 경우 -->
                            <span class="textcrop-2row {{item.addClass}}">{{item.s_name}}</span></a>
                        <!--on-->
                    </li>

                    <!--// 온라인 스탬프투어 -->
                </ul>
            </div>

            <!--출석체크-->
            <div class="box-mywhite stamp-tour mt20" ng-show="view">
                <div class="wrap-header">
                    <p class="txt-title">출석체크 스탬프</p>
                </div>
                <ul class="list-stamp">
                    <li ng-if="item.is_earning == 'Y'" ng-repeat="item in offline_list">
                        <span class="stamp common {{item.addClass}}"></span>
                        <!--css규칙 : "stamp common on"    :  on 경우 -->
                        <span class="textcrop-2row {{item.addClass}}" style="height: 36px">{{item.s_name}}</span>
                        <!--on-->
                    </li>

                </ul>
            </div>

            <div class="my-hotlink">
                <p>참석하신 오프라인 모임에 출석체크하고, 자세한 행사 정보도 만나 보세요!</p>
                <a href="/mobile-attendance/">
                    <img src="/common/img/page/my/banner-check.jpg" alt="모바일 출석체크 하러 가기" width="100%" /></a>
            </div>

        </div>

        <!--//-->
    </div>

</asp:Content>

