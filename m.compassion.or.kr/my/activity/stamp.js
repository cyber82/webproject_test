﻿$(function () {
    var sly = new Sly($("#slide"), {    // Call Sly on frame

        horizontal: 1,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 0,
        scrollBy: 1,
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1

    });

    sly.init();
    sly.activate(1);
    if (document.referrer.indexOf("/my/activity/") > 0) {
        $(".appPageNav").find(".back").attr("href", "/app/more/");
    } else {

    $(".appPageNav").find(".back").attr("href", "javascript:history.go(-1);");
    }
    /*
    setTimeout(function () {

        if ($("#slide_menu").width() < $("#slide").width()) {
            $("#slide_menu").css("left", '7%');
        }
    }, 300);
    */
});

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		$scope.isApp = cookie.get("cps.app");
		$scope.years = null;
		$scope.year = (new Date).getFullYear() + '';
		$scope.online_list = null;
		$scope.offline_list = null;
	
		// 년도정보
		$scope.getYears = function () {

			$scope.online_list = null;
			$scope.offline_list = null;

			$http.get("/api/my/stamp.ashx?t=years", { params: [] }).success(function (r) {

				if (r.success) {
					var list = r.data;
					$scope.years = list;

					setTimeout(function () {
						$(".custom_sel").selectbox("detach");
						$(".custom_sel").selectbox();
					},500)
					

					//console.log(list);

				} else {
					alert(r.message);
				}
			});
		}

		// 리스트
		$scope.getList = function () {

			$scope.online_list = null;
			$scope.offline_list = null;
			
			$http.get("/api/my/stamp.ashx?t=list", { params: {year : $scope.year} }).success(function (r) {
	
				if (r.success) {
					var list = r.data;
					
					$.each(list, function () {
						if (this.su_regdate)
						    this.su_regdate = new Date(this.su_regdate);

						if (this.is_earning == "Y") {
						    this.addClass = "on";
						}

						if (this.s_id == "store") {
							this.url = "/store/?app_ex=1";
						//	if ($scope.isApp) this.url = "#";
						    this.do = "스토어 상품 보기";
						}

						if (this.s_id == "ufj") {
						    this.url = "/sponsor/user-funding/";
						    this.do = "나눔펀딩 만나기";
						}

						if (this.s_id == "ufc") {
						    this.url = "/sponsor/user-funding/create/";
						    this.do = "나눔펀딩 만들기";
						}
						if (this.s_id == "sf") {
						    this.url = "/sponsor/special/";
						    this.do = "특별한 나눔 보기";
						}
						if (this.s_id == "cad") {
						    this.url = "/my/activity/cad/";
						    this.do = "CAD 추천가기";
						}
						if (this.s_id == "mail") {
							this.url = "/my/letter/write/";
							if (!$scope.isApp ) {
							    this.url = "#";
							    this.alert = true;
							    //this.url = "/my/letter/write/?app_ex=1";
							}
						    this.do = "편지쓰러 가기";
						}
						if (this.s_id == "cdsp") {
						    this.url = "/sponsor/children/";
						    this.do = "결연하러 가기";
						}

					});

					$scope.online_list = $.grep(list, function (r) {
						return r.is_online == 'Y';
					})

					$scope.offline_list = $.grep(list, function (r) {
						return r.is_online == 'N';
					})

					

					$.each($scope.offline_list, function () {
					    if (this.is_earning == "Y") {
					        $scope.view = true;
					    }
					})
					//console.log($scope.view);
					//console.log(list);
				} else {
					alert(r.message);
				}
			});
		}

		$scope.changeYear = function (year) {
			$scope.year = year;

			$scope.getList();
		}

		$scope.getYears();

		$scope.getList();

        // 편지쓰기 앱이 아닌경우 앱 다운로드 페이지로 이동
		$scope.appAlert = function (bAlert) {
		    if (bAlert) {
		        if (confirm("모바일 편지쓰기는 APP에서만 가능합니다.\n\rAPP다운로드 페이지로 이동하시겠습니까? "))
		            location.href = "/etc/app-info/";
		    }
		}
	});

})();