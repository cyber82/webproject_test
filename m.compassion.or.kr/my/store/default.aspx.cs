﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_store_default : MobileFrontBasePage {
	
	protected override void OnBeforePostBack() {
		if(Request.UrlReferrer != null && Request["lnb"] != "Y") {
			if(Request.UrlReferrer.AbsolutePath.IndexOf("/my/store/") > -1) {
				Response.Redirect("/my/");
			}
		}
		Response.Redirect("/my/store/order/");

	}

}