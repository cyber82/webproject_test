﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="detail-order-main.aspx.cs" Inherits="my_store_order_detail_order_main"  %>

<div style="background:transparent;" class="fn_pop_container" id="childModalview">

	<div class="wrap-layerpop fn_pop_content" style="width:100%;left:0;">
        <div class="header2">
        	<p class="txt-title">주문 상세 정보</p>
        </div>
        <div class="contents">
            
            <div class="my-order-detailinfo store-basket">
            
                <ul class="list-basket">
                    <li ng-repeat="item in storeModal.detail_list">
                        <span class="header" ng-bind-html="item.product_type"></span>
                        <a href="#" ng-click="goView(item.product_idx, $event)" target="_blank"><span class="item">
                            <img ng-src="{{item.name_img}}" alt="상품 썸네일 이미지" class="photo" />
                            <p class="txt-prdname">{{item.producttitle}}</p>
                            <p class="txt-option">(옵션)  {{item.optiondetail}}</p>
                                <p class="txt-option"ng-show="item.childname != ''" ng-bind-html="item.childname"></p>
                            </p>
                            <p class="txt-price">{{item.price | currency : "" : 0 }}원&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="txt-count">수량 <em>{{item.qty}}</em></span></p>
                        </span></a>
                        <span class="total">
                            주문금액 <span class="txt-total">{{item.totalprice | currency : "" : 0 }}<em>원</em></span>
                        </span>
                    </li> 	
                  
                </ul>
                
                <div class="wrap-bt"><a class="bt-type6 bt-original" style="width:100%" href="#" ng-click="storeModal.close($event)">확인</a></div>
            
            <div>
            
        </div>
        <div class="close2"><span class="ic-close" ng-click="storeModal.close($event)">레이어 닫기</span></div>
    </div>
    
</div>    
<!--//레이어 안내 팝업-->
    
</asp:Content>















				
