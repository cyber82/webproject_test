﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="detail-delivery.aspx.cs" Inherits="my_store_order_detail_delivery" %>

<div style="background:transparent;" class="fn_pop_container" id="childModalview">

	<div class="wrap-layerpop fn_pop_content" style="width:100%;left:0;">
        <div class="header2">
            <p class="txt-title">배송 안내</p>
        </div>
        <div class="contents">

            <div class="my-order-detailinfo">

                <strong class="ic-title1">배송안내</strong>
                <ul class="list-txt">
                    <li>3만원 이상 구매 시 무료배송 / 3만원 미만 구매 시 배송비 2,500원</li>
                    <li>오후 3시 30분 이전 결제가 완료된 주문은 당일 출고됩니다.</li>
                    <li>컴패션 커피는 오후 1시 30분 마감입니다. 미리 볶아두지 않고 주문 받은 후 로스팅하여 출고됩니다.
                    	따라서 평일에만 주문/출고되며, 공휴일 전날 주문하시면 공휴일이 지난 다음 날 로스팅하여 출고됩니다.</li>
                    <li>토요일/공휴일 제외한 평균 배송기간은 출고일로부터 1~2일 소요됩니다.<br />
                        (단, 지역별/업체별 상황에 따라 배송 예정일이 변경될 수 있습니다.)</li>
                    <li>도서산간 지역(제주도 포함), 오지 일부 지역은 운임이 추가될 수 있습니다.<br />
                        (해당되신 분들께는 별도 연락을 드립니다.) </li>
                    <li>택배 관련 문제/사고 발생 시 담당자(02-3668-3434)에게 연락 주시기 바랍니다.</li>
                </ul>

                <strong class="ic-title2">반품/교환 안내</strong>
                <p class="txt-emp">상품수령 후 7일 이내에 신청하실 수 있으며, 반품/교환 신청은 담당자(02-3668-3434)에게 연락 주시기 바랍니다. 단, 다음의 경우 해당하는 반품/교환 신청이 불가능 할 수 있습니다.</p>
                <ul class="list-txt">
                    <li>소비자의 책임 있는 사유로 상품 등이 멸실 또는 훼손된 경우(단지, 상품확인을 위한 포장 훼손 제외)</li>
                    <li>소비자의 사용 또는 소비에 의해 상품 등의 가치가 현저히 감소한 경우</li>
                    <li>시간의 경과에 의해 재판매가 곤란할 정도로 상품 등의 가치가 현저히 감소한 경우</li>
                    <li>복제가 가능한 상품 등의 포장을 훼손한 경우</li>
                    <li>소비자의 주문에 따라 개별적으로 생산되는 상품이 제작에 들어간 경우</li>
                </ul>

                <strong class="ic-title3">환불 안내</strong>
                <ul class="list-txt">
                    <li>주문 취소 등의 사유로 환불이 발생될 경우 담당자(02-3668-3434)에게 연락 주시기 바랍니다.</li>
                </ul>

                <div>
                </div>
                <div class="close2"><span class="ic-close" ng-click="modalDetail.close($event)">레이어 닫기</span></div>
            </div>

        </div>
        <!--//레이어 안내 팝업-->
