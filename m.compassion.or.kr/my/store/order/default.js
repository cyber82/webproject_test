﻿$(function () {




    
});

(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

        $scope.total = -1;
        $scope.list = null;

        $scope.params = {
            page: 1,
            rowsPerPage: 3,
            date_begin: "",
            date_end: ""
        };

        // 기본 날짜 세팅
        var date_begin = new Date();
        date_begin.setMonth(date_begin.getMonth() - 1);
        $scope.params.date_begin = date_begin;

        $scope.params.date_end = new Date();
        $("#date_begin").val($filter('date')($scope.params.date_begin, "yyyy-MM-dd"));
        $("#date_end").val($filter('date')($scope.params.date_end, "yyyy-MM-dd"));

        // 이번달 주문 내역 

        $scope.getList = function (params) {

            $scope.params.date_begin = $("#date_begin").val();
            $scope.params.date_end = $("#date_end").val();
            $scope.params = $.extend($scope.params, params);

            $http.get("/api/store.ashx?t=order_list", { params: $scope.params }).success(function (r) {

                if (r.success) {
                    var list = r.data;

                    if (r.data.length > 0) {
                        $scope.total = r.data[0].total;
                    }

                    //console.log($scope.total,list);
                    $.each(list, function () {
                        this.dtreg = new Date(this.dtreg);

                        if (this.statename == "출고완료") {
                        	if (this.deliveryco == 1) {
                        		this.href = 'https://trace.epost.go.kr/xtts/servlet/kpl.tts.common.svl.SttSVL?ems_gubun=E&sid1=' + this.deliverynum + '&POST_CODE=&mgbn=trace&traceselect=1&target_command=kpl.tts.tt.epost.cmd.RetrieveOrderConvEpostPoCMD&JspURI=%2Fxtts%2Ftt%2Fepost%2Ftrace%2FTrace_list.jsp&postNum=' + this.deliverynum + "&app_ex=1";
                        	} else if (this.deliveryco == 2) {
                        		this.href = "http://d2d.ilogen.com/d2d/delivery/invoice_tracesearch_quick.jsp?slipno=" + this.deliverynum + "&app_ex=1";
                        	} else if (this.deliveryco == 3) {
                        		this.href = 'http://www.hlc.co.kr/hydex/jsp/tracking/trackingViewCus.jsp?InvNo=' + this.deliverynum + "&app_ex=1";
                        	} else if (this.deliveryco == 5) {
                        		this.href = 'http://nplus.doortodoor.co.kr/web/detail.jsp?slipno=' + this.deliverynum + "&app_ex=1";
                        	} else {
                        		this.href = "";
                        	}
                        }

                    });

                    $scope.list = list;
                    $scope.total = r.data.length > 0 ? r.data[0].total : 0;

                } else {
                    alert(r.message);
                }


                if (params)
                    scrollTo($("#l"));

            });
        }

        $scope.goView = function (idx, $event) {
            location.href = "/store/item/" + idx;
            $event.preventDefault();

        }



        // 상세 주문 내역 팝업 
        $scope.modal = {
            instance: null,
            banks: [],
            detail_list: [],

            show: function (orderNo) {
                popup.init($scope, "/my/store/order/detail-order", function (modal) {
                    $scope.modal.instance = modal;
                    $scope.modal.instance.show();
                }, { top: 0, iscroll: true, removeWhenClose: true });
                // 상세 주문 내역 

                $http.get("/api/store.ashx?t=order_detail_list&orderno=" + orderNo, {}).success(function (r) {

                    if (r.success) {

                        var list = r.data;

                        $.each(list, function () {
                            this.dtreg = new Date(this.dtreg);
                            if (this.childname == "") {
                                this.product_type = "일반상품";
                            } else {
                                this.product_type = "어린이에게 보내는 선물";
                                this.childname = "선물받을 어린이 : " + this.childname;
                            }

                        });
                        $scope.modal.detail_list = list;
                    } else {
                        alert(r.message);
                    }
                });
            },
            close: function ($event) {
                $event.preventDefault();
                $scope.modal.instance.hide();

            }

        };

        // 자세히보기 팝업
        $scope.modalDetail = {
            instance: null,

            show: function ($event) {
                popup.init($scope, "/my/store/order/detail-delivery", function (modalDetail) {
                    $scope.modalDetail.instance = modalDetail;
                    $scope.modalDetail.instance.show();
                }, { top: 0, iscroll: true, removeWhenClose: true });

                $event.preventDefault();

            },

            close: function ($event) {
                $event.preventDefault();
                $scope.modalDetail.instance.hide();

            },
        }

        $scope.getList();

        $scope.getDate = function () {
            $("#date_begin").dateRange({
                buttons: ".dateRange",	// preset range
                end: "#date_end",
                eventBubble: true,
                onClick: function () {
                    $scope.params.page = 1;

                }
            });

            $("#date_begin").dateValidate({
                end: "#date_end",
                onSelect: function () {
                    
                }
            });
        }

        $scope.getDate();
    });

})();