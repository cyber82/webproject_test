﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_default" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>
	<script type="text/javascript" src="/my/store/order/default.js"></script>
    

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" id="hdSponsorId" runat="server"/>
	<input type="hidden" id="hdBirthDate" runat="server"/>
	<input type="hidden" id="hdUserName" runat="server"/>
    <input type="hidden" id="date_begin" class="date begin" value="{{params.date_begin| date:'yyyy-MM-dd'}}"/>
    <input type="hidden" id="date_end" class="date end" value="{{params.date_end| date:'yyyy-MM-dd'}}"/>
	
    <div class="wrap-sectionsub-my fullHeight" ng-app="cps" ng-cloak  ng-controller="defaultCtrl" id="l">    <!--마이컴패션 디자인-->
<!---->
    

    
    <div class="wrap-tab4 sectionsub-margin2">
        <a style="width:50%" class="selected" href="/my/store/order/">주문/배송 내역</a>
        <a style="width:50%" href="/my/store/review/">나의 후기/문의</a>
    </div>

    <div class="my-storeorder">
    	
		<div class="box-mywhite search-month">
        	<span class="radio-ui">
                <input type="radio" id="period_1" class="css-radio dateRange" name="chk_radio" data-day="0" data-month="1" checked="checked">
                <label for="period_1" class="css-label">1개월</label>
            </span>
            <span class="radio-ui">
                <input type="radio" id="period_2" class="css-radio dateRange" name="chk_radio"  data-day="0" data-month="2"/>
                <label for="period_2" class="css-label">2개월</label>
            </span>
            <span class="radio-ui">
                <input type="radio" id="period_3"class="css-radio dateRange" name="chk_radio"  data-day="0" data-month="3"/>
                <label for="period_3" class="css-label">3개월</label>
            </span>
			<span class="bt-type8" data-from="date_begin" data-end="date_end" ng-click="getList()">조회</span>
        </div>
        
        <ul class="list-order">
           
            <li ng-repeat="item in list">
				<div class="box-mywhite">
					<a href="#" ng-click="modal.show(item.orderno)" class="wrap-caption txt-no">주문일자/번호 : <em>{{item.dtreg | date:'yyyy.MM.dd'}} / {{item.orderno}} {{item.repcorrespondenceid == null ? '' : '(선물편지)'}}</em></a>
					<a href="#" ng-click="modal.show(item.orderno)">
						<p class="txt-prdname">{{item.producttitle}}</p>
						<p class="txt-option">(옵션) {{item.optiondetail}}</p>
					</a>					
					<p class="txt-price"><em>{{item.settleprice  | currency : "" : 0}}</em>원</p>
					<p class="order-state2">{{item.statename}}</p>
					<a ng-href="{{item.href}}" target="_blank" class="bt-type4" ng-show="item.statename == '출고완료' && (item.deliveryco < 4 || item.deliveryco == 5)">배송조회</a>
				</div>
            </li>

            <!--등록글 없는 경우-->
            <li class="no-result" ng-show="total == 0">
                <span class="box-mywhite">주문하신 내역이 없습니다.</span>
            </li>
        </ul>

        <paging ng-hide="total == 0" class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>

        <div class="box-mywhite guide-ordericon" ng-hide="total == 0">
            <!--주문내역 없을 때 display:none 처리-->
            <div class="row-table">
                <div class="td-field" style="width: 85px">
                    <img src="/common/img/icon/orderstate1.png" alt="" /><em>주문완료</em></div>
                <div class="td-field">
                    <p>회원님의 주문 및 NHN KCP 결제내역이 확인 되었습니다.</p>
                    <p>주문확인 문자발송/결제 영수증 메일 발송<br /> 오후 3시 30분 당일출고 (커피 오후 1시30분 마감)</p>
                </div>
            </div>
            <div class="row-table">
            	<div class="td-field" style="width:85px"><img src="/common/img/icon/orderstate2.png" alt="" /><em>출고완료</em></div>
                <div class="td-field">
                	<p>상품이 출고되어 택배사로 전달하였습니다.</p>
                    <p>출고 다음 날 배송조회를 통해 배송상황을 확인하실 수 있습니다.</p>
                </div>
            </div>
            <div class="row-table">
            	<div class="td-field" style="width:85px"><img src="/common/img/icon/orderstate3.png" alt="" /><em>주문취소</em></div>
                <div class="td-field">
                	<p>회원님의 요청으로 주문 및 결제가 취소된 경우입니다.</p>
                    <p>주문취소 관련 문의는 담당자 (02-3668-3434)에게 연락 주시기 바랍니다.</p>
                </div>
            </div>
        </div>
        
        <div class="box-mywhite list-guidetxt">
        	<ul>
            	<li>6개월 이전 구매내역은 PC버전에서 확인하실 수 있습니다.</li>
                <li>주문이 정상적으로 완료되지 않으면 내역 조회가 되지 않습니다.</li>
				<li>배송조회는 상품이 출고된 다음날부터 가능합니다.</li>
                <li>NHN KCP는 신용카드 및 계좌이체 결제대행 서비스를  제공하는 회사 이름입니다</li>
                <li><span class="fl">배송 / 반품 / 교환 / 환불 안내<br />
					담당자 전화 : 02-3668-3434</span>
                	<span class="fl"><a class="bt-type8" href="#" ng-click="modalDetail.show($event)">자세히 보기</a></span>
                    <span class="clear"></span>
                </li>
            </ul>
        </div>
        
        
    </div>
    
    
<!--//-->
</div>

    
</asp:Content>

