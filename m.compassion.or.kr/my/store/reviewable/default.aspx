﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_store_reviewable_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>
    <script type="text/javascript" src="/my/store/reviewable/default.js"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub-my fullHeight" ng-app="cps" ng-cloak ng-controller="defaultCtrl" id="l">
        <!--마이컴패션 디자인-->
        <!---->


        <div class="wrap-tab4 sectionsub-margin2">
            <a style="width: 50%" href="/my/store/order/">주문/배송 내역</a>
            <a style="width: 50%" class="selected" href="/my/store/review/">나의 후기/문의</a>
        </div>
        <div class="submenu-my sectionsub-margin2" id="slide">

            <ul style="position: absolute; top: 0; left: 3%">
                <li style="display: inline-block; width: 110px"><a href="/my/store/review/">나의 구매후기</a></li>
                <li style="display: inline-block; width: 110px"><a href="/my/store/qna/">나의 상품문의</a></li>
                <li style="display: inline-block; width: 180px"><a class="selected" href="/my/store/reviewable/">후기 작성 가능한 상품내역</a></li>
                <li style="display: inline-block"></li>
            </ul>

        </div>

        <div class="my-epilogue">

            <ul class="list-board">
                <li ng-repeat="item in list"><a href="/store/item/{{item.productidx}}?data=2#product_review">
                    <span class="box-mywhite">
                        <p class="wrap-caption txt-no">주문일자/번호 : <em>{{item.dtreg  | date:'yyyy.MM.dd'}} / {{item.orderno}}</em></p>
                        <p class="txt-prdname">{{item.name}}</p>
                        <p class="txt-option">(옵션) 아이보리+파우치포함</p>
                        <p class="txt-price"><em>{{item.totalprice | currency : "" : 0}}</em>원</p>
                    </span>
                </a><span class="bt-type6 postwrite" ng-click="write(item.productidx)">후기 작성하기</span></li>

                <!--등록글 없는 경우-->
                <li class="no-result" ng-show="total == 0">
                    <span class="box-mywhite">후기 작성 가능한 상품내역이 없습니다.</span>
                </li>
            </ul>

            <paging ng-hide="total == 0" class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getlist({page : page})"></paging>


        </div>
    </div>

    <!--//-->
</asp:Content>
