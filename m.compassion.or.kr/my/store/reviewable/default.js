﻿$(function () {
    var sly = new Sly($("#slide"), {    // Call Sly on frame

        horizontal: 1,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 1,
        scrollBy: 1,
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1

    });

    sly.init();
    $.each($("#slide a"), function (i) {
        if ($(this).hasClass("selected")) {
            sly.activate(i);
        }
    })

    
})
var app = angular.module('cps.page', []);

app.controller("defaultCtrl", function ($scope, $http, $filter) {

    $scope.total = -1;
    $scope.list = [];

    $scope.params = {
        page: 1,
        rowsPerPage: 10,
        
    };

    // getReviwList
    $scope.getlist = function (params) {

        $scope.params = $.extend($scope.params, params);
        //console.log($scope.params);

        $http.get("/api/store.ashx?t=reviewable_list", { params: $scope.params }).success(function (r) {
            
            if (r.success) {

                $scope.list = r.data;
                console.log(r.data)
                $.each($scope.list, function () {
                    this.dtreg = new Date(this.dtreg);
                })

                $scope.total = r.data.length > 0 ? r.data[0].total : 0;

                setTimeout(function () {

                	setFullHeight();

                }, 300);

            } else {
                alert(r.message);
            }
            if (params)
                scrollTo($("#l"));
        });
    }

    $scope.write = function (idx) {
        location.href = "/store/item/" + idx;
    }
    
    $scope.getlist();

});
