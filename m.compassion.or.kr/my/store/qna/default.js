﻿$(function () {
    var sly = new Sly($("#slide"), {    // Call Sly on frame

        horizontal: 1,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 0,
        scrollBy: 1,
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1

    });

    sly.init();
    $.each($("#slide a"), function (i) {
        if ($(this).hasClass("selected")) {
            sly.activate(i);
        }
    })
	
})

var app = angular.module('cps.page', []);
app.controller("defaultCtrl", function ($scope, $http, $filter) {

	$scope.total = -1;
	$scope.list = null;

	$scope.params = {
		page: 1,
		rowsPerPage: 10
	};

	// getReviwList
	$scope.getlist = function (params) {

		$scope.params = $.extend($scope.params, params);
		//console.log($scope.params);

		$http.get("/api/store.ashx?t=qna_list", { params: $scope.params }).success(function (r) {
			
			if (r.success) {
				//r.data = [];
				$scope.list = r.data;
				
				$.each($scope.list, function () {
				    this.reg_date = new Date(this.reg_date);

				    if (this.answer_date == null) {
				        this.answerClass = "pos-state1"
				        this.answerYN = '진행중';
				    } else {
				        this.answerClass = "pos-state2"
				        this.answerYN = '답변<br /> 완료';
				    }
				})

				$scope.total = r.data.length > 0 ? r.data[0].total : 0;

				setTimeout(function () {

					setFullHeight();

				}, 300);

			} else {
				alert(r.message);
			}
			if (params)
			    scrollTo($("#l"));
		});
	}


	$scope.getlist();

});
