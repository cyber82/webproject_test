﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_default" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/my/default.js"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
<input type="hidden" id="data" runat="server" />

<div class="wrap-sectionsub-my" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">    <!--마이컴패션 디자인-->
<!---->
    <div class="my-main">
    	
		<div class="wrap-mysupporter sectionsub-margin2">
        	<p><span class="sponsor-name"><asp:Literal runat="server" ID="userName" /></span> 후원자님</p>
            <p><asp:Literal runat="server" ID="conId" /></p>
        </div>
        
        <div class="box-mywhite recent-mychild" ng-if="data && data.child">

			<!-- 나의 어린이 상세 페이지 이동 -->
			<a href="#" ng-click="goChildView($event,data.child)">
				<span class="pos-photo"><span class="photo" style="background:url('{{data.child.pic}}') no-repeat"></span></span>

				<p class="txt-name">{{data.child.ko_name}}</p>
				<p class="txt-note"><span class="color1"><asp:Literal runat="server" ID="userName2" /> 후원자님, 안녕하세요?</span><br />
					우리가 함께한 지  <span class="color1">{{data.child.days }}일</span>이 지났어요.
					후원금과 기도, 사랑의 편지 항상 감사드려요.
				</p>
			</a>
			<!--//  -->
			
       		<div class="wrap-bt">
            	<a class="bt-type7 fl" style="width:48%" href="#" ng-click="goGift($event , data.child);">선물금 보내기</a>
                <a class="bt-type6 fr" style="width:48%" href="#" ng-click="showAlbum($event,data.child)">성장앨범 보기</a>
            </div>
        </div>
        
        <div class="box-myblue link-childmore" ng-if="data.count_cdsp > 1" ><a href="/my/children/"><span>나의 후원어린이 더 보기</span></a></div>
        
        <div class="box-mywhite recent-dash">
        	<span><a href="/my/children">나의 어린이<em>{{data.count_cdsp}}</em></a></span>
            <span><a href="/my/sponsor">특별한 나눔<em>{{data.count_non_cdsp}}</em></a></span>
            <span><a href="/my/user-funding/join">참여한 펀딩<em>{{data.count_join_funding}}</em></a></span>
            <span><a href="/my/user-funding/create">개설한 펀딩<em>{{data.count_make_funding}}</em></a></span>
        </div>
        
        <ul class="list-quicklink">
        	<li><a class="box-mywhite" href="/my/children">나의 어린이</a></li>
            <li><a class="box-mywhite" href="/my/sponsor">후원 관리</a></li>
            <li><a class="box-mywhite" href="/my/user-funding/join">나의 펀딩 관리</a></li>
            <li><a class="box-mywhite" href="/my/activity/CAD">나의 참여/활동</a></li>
            <li><a class="box-mywhite" href="/my/store/order/">스토어 주문내역</a></li>
            <li><a class="box-mywhite" href="/my/qna/">문의내역</a></li>            
        </ul>
        
        <div class="box-myblue support-amount">
        	<p class="txt1">이번 달 후원금 내역</p>
            <time datetime="2016-09">{{thismonth | date:'yyyy.MM'}}</time>
            <p class="txt-amount"><em>{{payment_total | number:0}}</em>원</p>
        </div>
        
        <div class="box-mywhite list-mysupport" ng-if="commitment_total > 0">
        	<div class="wrap-header">
            	<p class="txt-title">후원 신청 내역</p>
                <a class="bt-type4 more" href="/my/sponsor/commitment/">더 보기</a>
            </div>
            <ul>
            	<li class="{{item.icon}}" ng-repeat="item in commitment_list">

					<!---- 후원 상세정보 팝업 링크 ---->
					<a href="#" ng-click="showDetail($event,item)">
						<p class="txt1">{{item.accountclassname}} <span ng-if="item.accountclassname != item.accountclassdetail">- {{item.accountclassdetail}}</span></p>
                		<span class="txt2">{{item.fundingfrequency == '1회' ? "일시" : "정기" }} <em>{{item.amount | number:0}}</em> 원</span>
						<span class="bar">|</span>
						<time class="txt3" datetime="{{item.startdate | date:'yyyy.MM.dd'}}">{{item.startdate | date:'yyyy.MM.dd'}}</time>
					</a>
                </li>
            </ul>
        </div>
        
        <div class="box-mywhite band-count">
        	<div class="wrap-header">
            	<p class="txt-title">추천 관리</p>
                <a class="bt-type4 more" href="/my/activity/CAD">더 보기</a>
            </div>
            <div class="row-table">
            	<p class="td-txt" style="width:60%">내가 추천한 어린이의<br />결연 현황을 확인해보세요.</p>
	            <p class="td-count" style="width:40%">추천 어린이 <em>{{data.count_cad}}</em> 명</p>
            </div>
        </div>
        
        <div class="box-mywhite band-count">
        	<div class="wrap-header">
            	<p class="txt-title">스탬프 투어</p>
                <a class="bt-type4 more" href="/my/activity/stamp">더 보기</a>
            </div>
            <div class="row-table">
            	<p class="td-txt" style="width:60%">내가 획득한<br />스탬프 현황을 확인해보세요.</p>
	            <p class="td-count" style="width:40%">획득 스탬프 <em>{{data.count_stamp}}</em> 개</p>
            </div>
        </div>
        
        <div class="box-mywhite band-count">
        	<div class="wrap-header">
            	<p class="txt-title">이벤트 신청내역</p>
                <a class="bt-type4 more" href="/my/activity/event">더 보기</a>
            </div>
            <div class="row-table">
            	<p class="td-txt" style="width:60%">내가 참여한 캠페인/이벤트<br />내역을 확인해보세요.</p>
	            <p class="td-count" style="width:40%">참여 현황 <em>{{data.count_event}}</em> 개</p>
            </div>
        </div>
        
        <div class="order-storedetail">
            <div class="box-myblue ">
                <div class="wrap-header">
                    <p class="txt-title">스토어 주문 내역</p>
                    <a class="bt-type2 more" href="/my/store/order">더 보기</a>
                </div>
            </div>
            <ul class="list-order">
            	<li ng-repeat="item in order_list" class="box-mywhite">

					<!---- 주문상세정보 팝업 링크 ----->
					<a href="#" ng-click="storeModal.show(item.orderno)">
						<p class="txt-no">주문일자/번호 : <em>{{item.dtreg | date:'yyyy-MM-dd'}} / {{item.orderno}}</em></p>
					</a>

					<!------ 상품 상세페이지 이동 ------->	
					<a href="/store/item/{{item.productidx}}">	
						<p class="txt-prdname">{{item.producttitle}} </p>
						<p class="txt-option">옵션 - {{item.optiondetail}}</p>
						<p class="txt-price"><em>{{item.settleprice | number:0}}</em>원</p>
						<p class="order-state1">{{item.statename}}</p>
					</a>

					<a ng-href="{{item.href}}" target="_blank" class="bt-type4 bt-delivery" ng-show="item.statename == '출고완료' && (item.deliveryco < 4 || item.deliveryco == 5)">배송조회</a>
            	</li>

                <li class="no-result" ng-if="order_total==0 || order_total == -1">
                    <div class="box-mywhite">
                    	주문하신 내역이 없습니다.
	                    <div class="wrap-bt"><a class="bt-type6" style="width:140px" href="/store/">스토어 바로가기</a></div>
                    </div>
  	            </li>
              
            </ul>
        </div>
        
    </div>
    
<!--//-->
</div>

	
</asp:Content>

