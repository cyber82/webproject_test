﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using NLog;
using CommonLib;

public class UploadFileAuthHandler : IHttpHandler {

	public static readonly List<string> ImageExtensions = new List<string> { ".jpg", ".jpeg", ".bmp", ".gif", ".png" };

	public void ProcessRequest(HttpContext context) {

		Download(context, context.Request.Path, "application/octet-stream");
		return;
		

	}

	bool Download(HttpContext context, string path, string contentType) {

		path = context.Server.MapPath(path);
		if (!File.Exists(path)) {
			HttpContext.Current.Response.Write("<script>alert(\"File Not Found\");history.back();</script>");
			return false;
		}

		// whitelist
		if(!new List<string> { ".pdf", ".zip", ".jpg", ".jpeg", ".gif", ".png", ".exe", ".gsd", ".dwg", ".igs", ".dxf", ".docx", ".doc", ".xlsx", ".xls", ".pptx", ".ppt" }.Contains(Path.GetExtension(path).ToLower())) {
			HttpContext.Current.Response.Write("<script>alert(\"Not Allowed.(501)\");history.back();</script>");
			return false;
		}

		
		FileStream inStr = null;
		byte[] buffer = new byte[1024];
		long byteCount;
		try {
			HttpContext.Current.Response.Clear();
			HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
			HttpContext.Current.Response.Buffer = false;
			HttpContext.Current.Response.ContentType = contentType;
			if (contentType == "application/octet-stream") {
				HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(Path.GetFileName(path)));
			}

			//	throw new Exception();
			inStr = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 1024, false);
			while ((byteCount = inStr.Read(buffer, 0, buffer.Length)) > 0) {

				if (HttpContext.Current.Response.IsClientConnected) {
					HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length);
					HttpContext.Current.Response.Flush();
				} else {
					break;
				}
			}

			return true;

		} catch {
			try {
				HttpContext.Current.Response.Clear();
				HttpContext.Current.Response.ContentType = "text/html";
				HttpContext.Current.Response.ClearHeaders();
				HttpContext.Current.Response.Write("<script>alert(\"Download Error\");history.back();</script>");
			} catch {
			}
			return false;

		} finally {
			if (inStr != null)
				inStr.Close();
		}

	}

	public bool IsReusable
	{
		get
		{
			return false;
		}
	}

}