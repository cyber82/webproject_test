﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;
using NLog;
using System.Net;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Drawing;



public class Uploader
{
    CommonLib.WWW6Service.SoaHelperSoap _www6Service;

    public Uploader()
    {
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
    }

    public enum FileGroup
    {
        image_cpswedding,
        image_cpsdol,
        image_user_funding,
        image_user_pic,
        file_board,
        image_board,
        image_shop,
        image_banner,
        image_popup,
        file_recruit_notice,
        recruit,
        image_mainpage,
        image_sympathy,
        image_letter,
        image_participation,
        file_participation,
        image_company,
        image_qna,
        file_nk,
        image_nk,
        file_advocate,
        image_advocate,
        file_special_funding
    }

    public static string GetRoot(FileGroup group)
    {

        return GetRoot(group, null);
    }

    public static string GetRoot(FileGroup group, string arg)
    {
        switch (group)
        {
            default:
                return "";
            case FileGroup.image_mainpage:
                return ConfigurationManager.AppSettings["image_mainpage"];
            case FileGroup.image_cpswedding:
                return ConfigurationManager.AppSettings["image_cpswedding"];
            case FileGroup.image_cpsdol:
                return ConfigurationManager.AppSettings["image_cpsdol"];
            case FileGroup.image_user_funding:
                return ConfigurationManager.AppSettings["image_user_funding"];
            case FileGroup.file_special_funding:
                return ConfigurationManager.AppSettings["file_special_funding"];
            case FileGroup.image_user_pic:
                return ConfigurationManager.AppSettings["image_user_pic"];
            case FileGroup.file_board:
                return ConfigurationManager.AppSettings["file_board"];
            case FileGroup.image_board:
                return ConfigurationManager.AppSettings["image_board"];
            case FileGroup.image_banner:
                return ConfigurationManager.AppSettings["image_banner"];
            case FileGroup.image_shop:
                return ConfigurationManager.AppSettings["image_shop"];
            case FileGroup.image_popup:
                return ConfigurationManager.AppSettings["image_popup"];
            case FileGroup.file_recruit_notice:
                return ConfigurationManager.AppSettings["file_recruit_notice"];
            case FileGroup.image_letter:
                return ConfigurationManager.AppSettings["image_letter"];
            case FileGroup.recruit:
                return string.Format(ConfigurationManager.AppSettings["recruit"], arg);
            case FileGroup.image_participation:
                return ConfigurationManager.AppSettings["image_participation"];
            case FileGroup.file_participation:
                return ConfigurationManager.AppSettings["file_participation"];

            case FileGroup.image_sympathy:
                return ConfigurationManager.AppSettings["image_sympathy"];
            case FileGroup.image_company:
                return ConfigurationManager.AppSettings["image_company"];
            case FileGroup.image_qna:
                return ConfigurationManager.AppSettings["image_qna"];
            case FileGroup.file_nk:
                return ConfigurationManager.AppSettings["file_nk"];
            case FileGroup.image_nk:
                return ConfigurationManager.AppSettings["image_nk"];
            case FileGroup.file_advocate:
                return ConfigurationManager.AppSettings["file_advocate"];
            case FileGroup.image_advocate:
                return ConfigurationManager.AppSettings["image_advocate"];



        }
    }

    public class UploadResult
    {
        public bool success
        {
            get;
            set;
        }
        public string name
        {
            get;
            set;
        }
        public Int64 size
        {
            get;
            set;
        }
        public string msg
        {
            get;
            set;
        }
        public string domain
        {
            get;
            set;
        }
    }

    public UploadResult saveToFileServer(HttpContext context, string temp_file, string path, string fileName, string rename, string delete_filename)
    {

        string domain = ConfigurationManager.AppSettings["domain_file"];
        if (domain == "")
        {
            domain = context.Request.Domain();
        }


        using (WebClient client = new WebClient())
        {
            byte[] res = client.UploadFile(string.Format("{0}/common/handler/upload.common.ashx?fileDir={1}&rename={2}&fname={3}&dfile={4}", domain, path, rename, fileName, delete_filename), "post", temp_file);
            var resText = Encoding.UTF8.GetString(res);
            UploadResult result = JsonConvert.DeserializeObject<UploadResult>(resText);
            result.domain = domain;
            return result;
        }

    }

    public UploadResult Upload(HttpContext context, bool rename, string path, string fileName, string temp_file, string delete_filename)
    {

        UploadResult result = new UploadResult()
        {
            success = false
        };

        result = saveToFileServer(context, temp_file, path, fileName, rename ? "y" : "n", delete_filename);

        File.Delete(temp_file);

        return result;
    }

    public UploadResult Upload(HttpContext context, bool rename, string path, HttpPostedFile file)
    {

        UploadResult result = null;

        if (file != null && file.ContentLength > 0)
        {

            string tempDir = ConfigurationManager.AppSettings["temp"];

            if (!Directory.Exists(context.Server.MapPath(tempDir)))
            {
                Directory.CreateDirectory(context.Server.MapPath(tempDir));
            }

            string fname = file.FileName.Split('\\')[file.FileName.Split('\\').Length - 1];
            string fileName = fname.GetUniqueName(context, tempDir);
            string temp_file = context.Server.MapPath(tempDir) + fileName;

            file.SaveAs(temp_file);

            result = this.Upload(context, rename, path, fileName, temp_file, "");

        }

        return result;
    }

    public UploadResult Upload(HttpContext context, bool rename, string path, string fileName, Image image, string delete_filename)
    {

        UploadResult result = null;

        string tempDir = ConfigurationManager.AppSettings["temp"];

        if (!Directory.Exists(context.Server.MapPath(tempDir)))
        {
            Directory.CreateDirectory(context.Server.MapPath(tempDir));
        }

        fileName = fileName.GetUniqueName(context, tempDir);
        string temp_file = context.Server.MapPath(tempDir) + fileName;

        image.Save(temp_file);
        result = this.Upload(context, rename, path, fileName, temp_file, delete_filename);

        return result;
    }

    public UploadResult UploadLetter(HttpContext context, bool rename, string path, int giftSeq)
    {
        // 선물편지 보내기할때 이미지 저장 (db의 byte이미지를 파일 생성하여 path에 저장)
        UploadResult result = new UploadResult();
        result.success = false;

        try
        {
            string tempDir = ConfigurationManager.AppSettings["temp"];
            if (!Directory.Exists(context.Server.MapPath(tempDir)))
            {
                Directory.CreateDirectory(context.Server.MapPath(tempDir));
            }

            Object[] objSqlGift = new object[1] { string.Format("select top 1 Contents, Title, IsStoreItem from CORR_Gift where SEQ='{0}'", giftSeq) };
            var giftData = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSqlGift, "TEXT", null, null).Tables[0].Rows[0];
            var giftContents = giftData["Contents"].ToString();
            var giftTitle = giftData["Title"].ToString();
            var isStoreItem = Convert.ToBoolean(giftData["IsStoreItem"]);

            if (!isStoreItem)
            {
                string fname = giftTitle + ".png";
                string fileName = fname.GetUniqueName(context, tempDir);
                string temp_file = context.Server.MapPath(tempDir) + fileName;

                System.Drawing.Image image;
                byte[] bytes = Convert.FromBase64String(giftContents);
                ErrorLog.Write(HttpContext.Current, 0, "UploadLetter 222:");
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = System.Drawing.Image.FromStream(ms);
                    image.Save(temp_file);

                    result = this.Upload(context, rename, path, fileName, temp_file, "");
                }
            }
            else
            {
                //유료선물일 경우
                Object[] objSqlGift2 = new object[1] { "SELECT MEM_NAME_LANG0 FROM ETS_TC_CODE_MEMBER where MEM_CODE = 'IMG_SPNPRE'" };
                var giftData2 = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSqlGift2, "TEXT", null, null).Tables[0].Rows[0];
                var giftUrl = giftData2["MEM_NAME_LANG0"].ToString();
                var url2 = new Uri(giftUrl);

                var giftTitle2 = url2.Segments.Last();
                string fname2 = giftTitle2 + ".png";
                string fileName2 = fname2.GetUniqueName(context, tempDir);
                string temp_file2 = context.Server.MapPath(tempDir) + fileName2;

                using (WebClient webClient = new WebClient())
                {
                    byte[] data = webClient.DownloadData(giftUrl);

                    using (MemoryStream mem = new MemoryStream(data))
                    {
                        using (var letterImage = Image.FromStream(mem))
                        {
                            letterImage.Save(temp_file2);
                            result = this.Upload(context, rename, path, fileName2, temp_file2, "");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.success = false;
            result.msg = ex.Message;
        }

        return result;
    }
}