﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class login : MobileFrontBasePage {


	protected override void OnBeforePostBack() {

//
		var url = "";
		var r = Request["r"].EmptyIfNull();
		if(!string.IsNullOrEmpty(r))
			Response.Redirect(ConfigurationManager.AppSettings["domain_auth"] + "/login/?r=" + HttpUtility.UrlEncode(r.Replace("default.aspx", "").Replace(".aspx", "")));


		// mobile return url
		var mr = Request["mr"].EmptyIfNull();
        if (!string.IsNullOrEmpty(mr))
        {
            string param = "";
            var e_id = Request["e_id"].EmptyIfNull();

            if (!string.IsNullOrEmpty(e_id))
            {
                var e_src = Request["e_src"].EmptyIfNull();
                param = "e_id=" + e_id + "&e_src=" + e_src + "&";
            }

            Response.Redirect(ConfigurationManager.AppSettings["domain_auth"] + "/m/login/?"+param+"r=" + HttpUtility.UrlEncode(mr.Replace("default.aspx", "").Replace(".aspx", "")));
        }

		if (Request.UrlReferrer != null)
			url = Request.UrlReferrer.AbsoluteUri;
		Response.Redirect(ConfigurationManager.AppSettings["domain_auth"] + "/m/login/?r=" + HttpUtility.UrlEncode(url.Replace("default.aspx", "").Replace(".aspx", "")));
	}

}