﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class logout : MobileFrontBasePage {


	protected override void OnBeforePostBack() {

		var url = "";
		if(Request.UrlReferrer != null)
			url = Request.UrlReferrer.AbsoluteUri;
		Response.Redirect(ConfigurationManager.AppSettings["domain_auth"] + "/m/logout/?r=" + HttpUtility.UrlEncode(url.Replace("default.aspx", "").Replace(".aspx", "")));
	}

}